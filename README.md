# SUMARiS Angular components

Based components, for Angular App.

Features :
- Angular 11+: 
  - v1.x for Angular 11
  - v2.6.x for Angular 14
  - v2.7.x for Angular 17
  - v18.x for Angular 18
- Angular Material design
- Ionic framework
- i18n
- New form field components:
  * Authentication forms
  * Extensible menu
  * UI components (mobile or desktop) :
    * Autocomplete field, with search and highlight
    * Latitude and longitude field, with 3 entry format
    * Date + time, Date only, Date short (year selector)

## Contribute

See the [developer guide](https://github.com/sumaris-net/ngx-sumaris-components/blob/master/doc/build.md)

## Change log

See the [change log](https://github.com/sumaris-net/ngx-sumaris-components/blob/master/doc/changelog.md)

## License

Free software, distributed under a [AGPL v3 license](https://github.com/sumaris-net/ngx-sumaris-components/blob/master/LICENSE).
