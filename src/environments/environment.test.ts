// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { StorageDrivers } from '../app/shared/storage/storage.utils';

// Environment to use only with unit tests
import { Environment } from './environment.class';

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
//import 'zone.js/plugins/zone-error';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pkg = require('../../package.json');

export const environment = Object.freeze(<Environment>{
  name: pkg.name as string,
  version: pkg.version as string,
  production: false,
  externalEnvironmentUrl: 'assets/environments/environment-test.json',
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',

  // FIXME: enable cache
  persistCache: false,

  peerMinVersion: '1.8.0',
  peerDefaultPrefix: 'http://',
  enableSelectPeerByFeature: true,

  defaultPeer: {
    host: 'localhost',
    port: 8080,
  },

  defaultPeers: [
    {
      host: 'localhost',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8081,
    },
    {
      host: '192.168.0.45',
      port: 8080,
    },
    {
      host: 'sih.sfa.sc',
      port: 80,
    },
  ],

  defaultAppName: 'SUMARiS',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=net.sumaris.app',
  defaultIOSInstallUrl: 'https://apps.apple.com/us/app/sumaris/id6736747523',
  defaultDesktopInstallUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/releases',

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  // About
  sourceUrl: pkg.repository && (pkg.repository.url as string),
  reportIssueUrl: pkg.bugs && (pkg.bugs.url as string),

  // Development
  defaultAuthValues: {
    // Basic auth (using Person.username)
    // username: 'admq2', password: 'q22006'
    // Token auth (using Person.pubkey)
    //username: 'admin@sumaris.net', password: 'admin',
  },

  account: {
    enableListenChanges: true,
    listenIntervalInSeconds: 60,
  },

  entityEditor: {
    enableListenChanges: false,
    listenIntervalInSeconds: 60,
  },
});
