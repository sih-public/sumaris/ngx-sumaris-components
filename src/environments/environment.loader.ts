import { Directive } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom, of, shareReplay, tap } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Environment } from './environment.class';
import { isNotNilOrBlank } from '../app/shared/functions';

@Directive()
export abstract class EnvironmentLoader<T extends Environment = Environment> {
  abstract load(): Promise<T>;

  abstract get(): T;
}

export class EnvironmentHttpLoader<T extends Environment = Environment> extends EnvironmentLoader<T> {
  private readonly _logPrefix = '[environment-loader] ';

  protected _debug: boolean;
  protected _environment: T;
  protected _loaded = false;
  protected readonly _externalEnvironmentUrl: string;

  constructor(
    protected http: HttpClient,
    defaultEnvironment?: T
  ) {
    super();
    // Copy default environment (because can be readonly)
    this._environment = Object.assign({}, defaultEnvironment || <T>{});
    this._externalEnvironmentUrl = this._environment.externalEnvironmentUrl;
    this._debug = !this._environment.production;
  }

  /**
   * Get the loaded environment. Should be call after loaded
   */
  get(): T {
    if (!this._loaded && isNotNilOrBlank(this._externalEnvironmentUrl)) {
      console.warn(`${this._logPrefix}EnvironmentHttpLoader.get() should be called AFTER load() finished.`);
    }
    return this._environment;
  }

  async load(): Promise<T> {
    if (this._loaded) return this._environment; // Already loaded

    try {
      // If it has an external url
      if (isNotNilOrBlank(this._externalEnvironmentUrl)) {
        // DEBUG
        //await sleep(4000);

        // Load external environment file
        const json = await this.fetch(this._externalEnvironmentUrl);

        // Patch the existing environment
        if (json) {
          Object.assign(this._environment, json);
        }
      }

      return this._environment;
    } finally {
      this._loaded = true;
    }
  }

  /* -- protected function -- */

  protected async fetch(url: string): Promise<Partial<T>> {
    if (this._debug) console.debug(`${this._logPrefix}Loading external environment from {${url}}...`);

    return firstValueFrom(
      this.http.get<Partial<T>>(url).pipe(
        shareReplay(1),
        tap((json) => {
          if (this._debug) console.debug(`${this._logPrefix}External environment loaded`, json);
        }),
        catchError((error) => {
          if (error.status === 404) {
            if (this._debug) console.error(`${this._logPrefix}External environment not found (404)`);
          } else {
            console.error(`${this._logPrefix}Failed to load external environment: ${error.message}`, error);
          }
          return of(null);
        })
      )
    );
  }
}
