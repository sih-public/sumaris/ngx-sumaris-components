// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { StorageDrivers } from '../app/shared/storage/storage.utils';

import { Environment } from './environment.class';
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
//import 'zone.js/plugins/zone-error';

export const environment: Environment = Object.freeze(<Environment>{
  name: '@sumaris-net/ngx-components', // overridden by ENVIRONMENT token
  version: '1.0', // overridden by ENVIRONMENT token

  // Do NOT set to false - because this value will be used by releases
  production: true,

  // Do NOT set - because this value will be used by releases
  //externalEnvironmentUrl: null,

  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  useHash: false,
  allowDarkMode: true,

  // FIXME: enable cache
  persistCache: false,

  // TODO: make this works
  //offline: true,

  peerMinVersion: '1.8.0',

  enableSelectPeerByFeature: true,

  defaultPeer: {
    host: 'localhost',
    port: 8081,
  },

  defaultPeers: [
    {
      host: 'localhost',
      port: 8080,
    },
    {
      host: 'localhost',
      port: 8081,
    },
    {
      host: '192.168.0.45',
      port: 8080,
    },
    {
      host: '192.168.0.24',
      port: 8080,
    },
    {
      host: '192.168.0.29',
      port: 8080,
    },
    {
      host: '192.168.0.107',
      port: 8080,
    },
    {
      host: 'sih.sfa.sc',
      port: 443,
    },
    {
      host: 'test.sumaris.net',
      port: 443,
    },
    {
      host: 'open.sumaris.net',
      port: 443,
    },
    {
      host: 'server.e-is.pro',
      port: 443,
    },
  ],
  defaultAppName: 'SUMARiS',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=net.sumaris.app',
  defaultIOSInstallUrl: 'https://apps.apple.com/us/app/sumaris/id6736747523',
  defaultDesktopInstallUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/releases',

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  // About
  sourceUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/ngx-sumaris-components',
  reportIssueUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/ngx-sumaris-components/-/issues',
  forumUrl: null, //'https://forum.sumaris.net',
  helpUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/blob/master/user-manual/index_fr.md',
  privacyPolicyUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/raw/master/doc/privacy_policy_fr.md',
  termsOfUseUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/raw/master/doc/terms_of_use_fr.md?ref_type=heads',

  // Development
  defaultAuthValues: {
    // Token auth (using Person.pubkey)
    username: 'admin@sumaris.net',
    password: 'admin',
  },

  account: {
    enableListenChanges: true,
    listenIntervalInSeconds: 60,
  },

  entityEditor: {
    enableListenChanges: true,
    listenIntervalInSeconds: 60,
  },
});
