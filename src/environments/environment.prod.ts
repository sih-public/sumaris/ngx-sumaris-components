import { Environment } from './environment.class';
import { StorageDrivers } from '../app/shared/storage/storage.utils';

/* eslint-disable */
export const environment = Object.freeze(<Environment>{
  name: '@sumaris-net/ngx-components',
  version: undefined, // overridden by ENVIRONMENT token
  production: true,
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',

  peerMinVersion: '1.0.0',
  peerDefaultPrefix: 'https://',
  enableSelectPeerByFeature: false,
  persistCache: false,

  defaultPeer: undefined,
  // TODO: remove this
  /*defaultPeer: {
    host: '192.168.0.45',
    port: 8080
  },*/
  // Production and public peers
  defaultPeers: [
    // No default peer in production
  ],
  defaultAppName: 'SUMARiS',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=net.sumaris.app',

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  // About
  sourceUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/ngx-sumaris-components',
  reportIssueUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/ngx-sumaris-components/-/issues',

  account: {
    enableListenChanges: true,
  },

  entityEditor: {
    enableListenChanges: false,
  },
});
/* eslint-enable */
