import { FetchPolicy } from '@apollo/client/core';
import { StorageConfig } from '@ionic/storage';
import { InjectionToken } from '@angular/core';
import { CacheConfig } from 'ionic-cache/lib/interfaces/cache-config.interface';
import { LoggingServiceConfig } from '../app/shared/logging/logging-service.config';

export const ENVIRONMENT = new InjectionToken<Environment>('ENV');

export class Environment {
  name: string;
  version: string;
  production: boolean;

  // An URL to load external environment file (e.g. 'assets/environments/environment.json'
  externalEnvironmentUrl?: string;
  baseUrl: string;
  connectionTimeout?: number;

  sameUrlPeer?: boolean;

  // The peer to use (if defined)
  peer?: { host: string; port: number; useSsl?: boolean; path?: string } | undefined | null;

  // A peer to use at startup (useful on a web site deployment)
  defaultPeer?: { host: string; port: number; useSsl?: boolean; path?: string } | undefined | null;

  // A list of peers, to select as peer, in settings
  defaultPeers?: { host: string; port: number; useSsl?: boolean; path?: string }[];

  // Min compatible version for the peer
  peerMinVersion?: string;

  // Default peer prefix, when fix peer url manually (default: 'https://')
  peerDefaultPrefix?: string;

  // Allow to select a peer by feature
  enableSelectPeerByFeature?: boolean;

  // Interval to check if web page must be reload, in millis
  checkAppVersionIntervalInSeconds?: number;

  // Enable cache persistence ?
  persistCache?: boolean;

  // Force offline mode ? /!\ For DEV only
  offline?: boolean;

  // Apollo (graphQL) config
  apolloFetchPolicy?: FetchPolicy;

  // Default values
  defaultLocale: string;
  defaultLatLongFormat?: 'DD' | 'DDMM' | 'DDMMSS';
  defaultDepartmentId?: number;
  defaultAppName?: string;
  defaultAndroidInstallUrl?: string;
  defaultIOSInstallUrl?: string;
  defaultDesktopInstallUrl?: string;

  // Route and style
  useHash?: boolean; // Configure Angular router to use '#' in URL (need by web-ext artifact)
  allowDarkMode?: boolean;

  // Logging
  logging?: Partial<LoggingServiceConfig>;

  // Storage
  cache?: Partial<CacheConfig>;
  storage?: Partial<StorageConfig>;
  storageSavePeriodMs?: number;

  // About
  sourceUrl?: string;
  reportIssueUrl?: string;

  forumUrl?: string; // TODO allow a map by locale ? ex: { [lang: string]: string };
  helpUrl?: string; // TODO allow a map by locale ?
  privacyPolicyUrl?: string; // TODO allow a map by locale ?
  termsOfUseUrl?: string; // TODO allow a map by locale ?

  // Default authentication /!\ For DEV only
  // (see values in the test database - XML files in Pod's module)
  defaultAuthValues?: {
    username?: string;
    password?: string;
    token?: string;
  };

  // Account
  account?: {
    showAccountButton?: boolean; // Show account button in home page (if undefined = true)
    enableListenChanges?: boolean;
    // If undefined or 0, then listen only changes made by the pod
    // If greater than 0 : will check concurrent changes in the DB (e.g. changes mde by another software) AND changes made the pod
    listenIntervalInSeconds?: number;
  };

  entityEditor?: {
    enableListenChanges?: boolean;
    // If undefined or 0, then listen only changes made by the pod
    // If greater than 0 : will check concurrent changes in the DB (e.g. changes mde by another software) AND changes made the pod
    listenIntervalInSeconds?: number;
  };
}
