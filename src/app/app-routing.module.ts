import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { HomePage } from './core/home/home';
import { RegisterConfirmPage } from './core/register/register-confirm.page';
import { AccountPage } from './core/account/account.page';
import { SettingsPage } from './core/settings/settings.page';
import { AuthGuardService } from './core/services/auth-guard.service';
import { SharedRoutingModule } from './shared/shared-routing.module';
import { APP_MENU_ITEMS } from './core/menu/menu.service';
import { IMenuItem } from './core/menu/menu.model';
import { ComponentDirtyGuard } from './shared/guard/component-dirty.guard';
import { AppAuthModule } from './core/auth/auth.module';
import { AppChangePasswordPage } from './core/account/password/change-password.page';

const routes: Routes = [
  // Core path
  {
    path: '',
    component: HomePage,
  },

  {
    path: 'home/:action',
    component: HomePage,
  },
  {
    path: 'confirm/:email/:code',
    component: RegisterConfirmPage,
  },
  {
    path: 'password/:email/:token',
    component: AppChangePasswordPage,
  },
  {
    path: 'account',
    pathMatch: 'full',
    component: AccountPage,
    canActivate: [AuthGuardService],
    canDeactivate: [ComponentDirtyGuard],
  },
  {
    path: 'account/password',
    pathMatch: 'full',
    canActivate: [AuthGuardService],
    component: AppChangePasswordPage,
  },
  {
    path: 'settings',
    pathMatch: 'full',
    component: SettingsPage,
    canDeactivate: [ComponentDirtyGuard],
  },

  // Admin
  {
    path: 'admin',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./admin/admin-routing.module').then((m) => m.AdminRoutingModule),
  },

  // Test module (disable in menu, by default - can be enabled by the Pod configuration page)
  {
    path: 'testing',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'shared',
      },
      // Shared module
      {
        path: 'shared',
        loadChildren: () => import('./shared/shared.testing.module').then((m) => m.SharedTestingModule),
      },
      // Core module
      {
        path: 'core',
        loadChildren: () => import('./core/core.testing.module').then((m) => m.CoreTestingModule),
      },
      // Social module
      {
        path: 'social',
        loadChildren: () => import('./social/social.testing.module').then((m) => m.SocialTestingModule),
      },
    ],
  },

  // Other route redirection (should at the end of the array)
  {
    path: '**',
    redirectTo: '/',
  },
];

// Menu items
const MENU_ITEMS: IMenuItem[] = [
  { title: 'MENU.HOME', path: '/', icon: 'home' },

  // Data entry (FAKE route - used for menu testing)
  { title: 'MENU.DATA_ENTRY_DIVIDER', profile: 'USER' },
  {
    title: 'MENU.TRIPS',
    path: '/trips',
    matIcon: 'explore',
    profile: 'USER',
    ifProperty: 'sumaris.trip.enable',
    titleProperty: 'sumaris.trip.name',
  },
  {
    title: 'MENU.OCCASIONS',
    path: '/observations',
    icon: 'location',
    profile: 'USER',
    ifProperty: 'sumaris.observedLocation.enable',
    titleProperty: 'sumaris.observedLocation.name',
  },

  // Admin
  { title: 'MENU.ADMINISTRATION_DIVIDER', profile: 'ADMIN' },
  {
    title: 'MENU.USERS',
    path: '/admin/users',
    icon: 'people',
    profile: 'ADMIN',
    // children: [
    //   {title: 'USER.LIST.BTN_ACTIONS', matIcon: 'expand_more', profile: 'ADMIN', action: 'expand',
    //     children: [
    //       {
    //         title: 'USER.LIST.BTN_SEND_MESSAGE', icon: 'mail',  action: 'send', static: false
    //       },
    //     ]
    //   }
    // ]
  },

  // Settings
  { title: '' /*empty divider*/, cssClass: 'flex-spacer' },
  { title: 'MENU.TESTING', path: '/testing', icon: 'code', color: 'danger' },
  { title: 'MENU.LOCAL_SETTINGS', path: '/settings', icon: 'settings', color: 'medium' },
  { title: 'MENU.ABOUT', action: 'about', matIcon: 'help_outline', color: 'medium', cssClass: 'visible-mobile' },

  // Logout
  { title: 'MENU.LOGOUT', action: 'logout', icon: 'log-out', profile: 'GUEST', color: 'medium hidden-mobile' },
  { title: 'MENU.LOGOUT', action: 'logout', icon: 'log-out', profile: 'GUEST', color: 'danger visible-mobile' },
];

export const ROUTE_OPTIONS: ExtraOptions = {
  enableTracing: false,
  //enableTracing: !environment.production,
  useHash: false,
  onSameUrlNavigation: 'reload',
};

@NgModule({
  imports: [SharedRoutingModule, RouterModule.forRoot(routes, ROUTE_OPTIONS), AppAuthModule],
  exports: [RouterModule],
  providers: [
    { provide: APP_MENU_ITEMS, useValue: MENU_ITEMS },
    { provide: ComponentDirtyGuard, useClass: ComponentDirtyGuard },
  ],
})
export class AppRoutingModule {}
