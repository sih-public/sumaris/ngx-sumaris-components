import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, OnInit } from '@angular/core';
import { ConfigService } from './core/services/config.service';
import { DOCUMENT } from '@angular/common';
import { Configuration } from './core/services/model/config.model';
import { PlatformService } from './core/services/platform.service';
import { throttleTime } from 'rxjs/operators';
import { FormFieldDefinition } from './shared/form/field.model';
import { getColorContrast, getColorShade, getColorTint, hexToRgbArray, mixHex } from './shared/graph/colors.utils';
import { AccountService } from './core/services/account.service';
import { LocalSettingsService } from './core/services/local-settings.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { isNotNil, isNotNilOrBlank, joinPropertiesPath } from './shared/functions';
import { Department } from './core/services/model/department.model';

import { CORE_CONFIG_OPTIONS } from './core/services/config/core.config';
import { PersonService } from './admin/services/person.service';
import { LoadResult } from './shared/services/entity-service.class';
import { Person } from './core/services/model/person.model';
import { TEST_LOCAL_SETTINGS_OPTIONS } from './core/services/testing/local-settings.config';
import { PersonFilter } from './admin/services/filter/person.filter';
import { SortDirection } from '@angular/material/sort';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  logo: string;
  appName: string;
  private _document = inject(DOCUMENT);

  constructor(
    protected platform: PlatformService,
    private accountService: AccountService,
    private configService: ConfigService,
    private personService: PersonService,
    private settings: LocalSettingsService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private cd: ChangeDetectorRef
  ) {}

  async ngOnInit() {
    console.info('[app] Starting...');
    await this.platform.start();

    // Listen for config changed
    this.configService.config.subscribe((config) => this.onConfigChanged(config));

    // Add local settings options
    this.addLocalSettingsOptions();

    // Add additional account fields
    this.addAccountFields();

    // Add custom icons
    this.addCustomSVGIcons();

    console.info('[app] Starting [OK]');
  }

  onActivate(event) {
    // Make sure to scroll on top before changing state
    // See https://stackoverflow.com/questions/48048299/angular-5-scroll-to-top-on-every-route-click
    const scrollToTop = window.setInterval(() => {
      const pos = window.scrollY;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

  protected onConfigChanged(config: Configuration) {
    this.logo = config.smallLogo || config.largeLogo;
    this.appName = config.label;

    // Set document title
    const title = isNotNil(config.name) ? `${config.label} - ${config.name}` : config.label;
    this._document.getElementById('appTitle').textContent = title;

    if (config.properties) {
      // Set document favicon
      const favicon = config.getProperty(CORE_CONFIG_OPTIONS.FAVICON);
      if (isNotNil(favicon)) {
        this._document.getElementById('appFavicon').setAttribute('href', favicon);
      }

      this.updateTheme({
        colors: {
          primary: config.properties['sumaris.color.primary'],
          secondary: config.properties['sumaris.color.secondary'],
          tertiary: config.properties['sumaris.color.tertiary'],
          success: config.properties['sumaris.color.success'],
          warning: config.properties['sumaris.color.warning'],
          accent: config.properties['sumaris.color.accent'],
          danger: config.properties['sumaris.color.danger'],
        },
        vars: {
          '--app-form-field-background-color': config.getProperty(CORE_CONFIG_OPTIONS.FORM_FIELD_BACKGROUND_COLOR),
          '--app-form-field-focus-background-color': config.getProperty(CORE_CONFIG_OPTIONS.FORM_FIELD_FOCUS_BACKGROUND_COLOR),
          '--app-form-field-disabled-background-color': config.getProperty(CORE_CONFIG_OPTIONS.FORM_FIELD_DISABLED_BACKGROUND_COLOR),
        },
      });

      this.cd.markForCheck();
    }
  }

  protected updateTheme(options: { darkMode?: boolean; colors?: { [color: string]: string }; vars?: { [color: string]: string } }) {
    if (!options) return;

    console.info('[app] Changing theme...', options);

    // Set colors
    const style = document.documentElement.style;
    if (options.colors) {
      // Add 100 & 900 color for primary and secondary color
      ['primary', 'secondary'].forEach((colorName) => {
        const color = options.colors[colorName];
        options.colors[colorName + '100'] = (color && mixHex('#ffffff', color, 10)) || undefined;
        options.colors[colorName + '900'] = (color && mixHex('#000000', color, 12)) || undefined;
      });

      Object.getOwnPropertyNames(options.colors).forEach((colorName) => {
        // Remove existing value
        style.removeProperty(`--ion-color-${colorName}`);
        style.removeProperty(`--ion-color-${colorName}-rgb`);
        style.removeProperty(`--ion-color-${colorName}-contrast`);
        style.removeProperty(`--ion-color-${colorName}-contrast-rgb`);
        style.removeProperty(`--ion-color-${colorName}-shade`);
        style.removeProperty(`--ion-color-${colorName}-tint`);

        // Set new value, if any
        const color = options.colors[colorName];
        if (isNotNil(color)) {
          // Base color
          style.setProperty(`--ion-color-${colorName}`, color);
          style.setProperty(`--ion-color-${colorName}-rgb`, hexToRgbArray(color).join(', '));

          // Contrast color
          const contrastColor = getColorContrast(color, true);
          style.setProperty(`--ion-color-${colorName}-contrast`, contrastColor);
          style.setProperty(`--ion-color-${colorName}-contrast-rgb`, hexToRgbArray(contrastColor).join(', '));

          // Shade color
          style.setProperty(`--ion-color-${colorName}-shade`, getColorShade(color));

          // Tint color
          style.setProperty(`--ion-color-${colorName}-tint`, getColorTint(color));
        }
      });
    }

    // Set CSS vars
    if (options.vars) {
      Object.getOwnPropertyNames(options.vars).forEach((varName) => {
        // Set new value, if any
        const value = options.vars[varName];
        if (isNotNilOrBlank(value)) {
          // Base color
          style.setProperty(varName, value);
        }
      });
    }
  }

  protected addLocalSettingsOptions() {
    console.debug('[app] [test] Add local settings options...');

    const suggestPersonFn = async (value: any, filter: Partial<PersonFilter>, sortBy: keyof Person, sortDirection: SortDirection) => {
      await this.personService.ready();
      if (!this.accountService.isLogin()) return <LoadResult<Person>>{ data: [] };
      return this.personService.suggest(value, filter, sortBy, sortDirection);
    };

    // Add option with type 'entity'
    {
      const optionDef = <FormFieldDefinition>{ ...TEST_LOCAL_SETTINGS_OPTIONS.DUMMY_OPTION_ENTITY };
      optionDef.autocomplete.suggestFn = suggestPersonFn;
      this.settings.registerOption(optionDef);
    }

    // Add option with type 'entities'
    {
      const optionDef = <FormFieldDefinition>{ ...TEST_LOCAL_SETTINGS_OPTIONS.DUMMY_OPTION_ENTITIES };
      optionDef.autocomplete.suggestFn = suggestPersonFn;
      this.settings.registerOption(optionDef);
    }
  }

  protected addAccountFields() {
    console.debug('[app] Add additional account fields...');

    const attributes = this.settings.getFieldDisplayAttributes('department');
    const departmentDefinition = <FormFieldDefinition>{
      key: 'department',
      label: 'USER.DEPARTMENT.TITLE',
      type: 'entity',
      autocomplete: {
        items: [
          Department.fromObject({ id: 1, label: 'E-IS', name: 'Environmental Information Systems' }),
          Department.fromObject({ id: 2, label: 'UNK', name: 'Other' }),
        ],
        filter: { entityName: 'Department' },
        displayWith: (value) => value && joinPropertiesPath(value, attributes),
        attributes,
      },
      extra: {
        registration: {
          required: true,
        },
        account: {
          required: true,
          disabled: true,
        },
      },
    };

    // Add account field: department
    this.accountService.registerAdditionalField(departmentDefinition);

    // When settings changed
    this.settings.onChange.pipe(throttleTime(400)).subscribe(() => {
      // Update the display fn
      const attributes = this.settings.getFieldDisplayAttributes('department');
      departmentDefinition.autocomplete.attributes = attributes;
      departmentDefinition.autocomplete.displayWith = (value) => (value && joinPropertiesPath(value, attributes)) || undefined;
    });
  }

  protected addCustomSVGIcons() {
    ['fish'] // Example
      .forEach((filename) =>
        this.matIconRegistry.addSvgIcon(filename, this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/icons/${filename}.svg`))
      );
  }
}
