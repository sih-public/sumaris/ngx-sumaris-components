import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SettingsPage } from './settings.page';
import { TranslateModule } from '@ngx-translate/core';
import { AppFormButtonsBarModule } from '../form/buttons/form-buttons-bar.module';
import { AppPropertiesFormModule } from '../form/properties/properties.module';
import { RxPush } from '@rx-angular/template/push';

@NgModule({
  imports: [SharedModule, AppFormButtonsBarModule, TranslateModule.forChild(), AppPropertiesFormModule, RxPush],

  declarations: [SettingsPage],
  exports: [TranslateModule, SettingsPage],
})
export class AppSettingsPageModule {}
