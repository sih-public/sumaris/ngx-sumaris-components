import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Inject,
  InjectionToken,
  Injector,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import { AccountService } from '../services/account.service';
import { APP_LOCALES, LocaleConfig, LocalSettings, UsageMode } from '../services/model/settings.model';
import { Peer } from '../services/model/peer.model';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormControl } from '@angular/forms';
import { AppForm } from '../form/form.class';
import { AppFormUtils, CanLeave } from '../form/form.utils';
import { TranslateService } from '@ngx-translate/core';
import { ValidatorService } from '@e-is/ngx-material-table';
import { LocalSettingsValidatorService } from '../services/validator/local-settings.validator';
import { PlatformService } from '../services/platform.service';
import { NetworkService } from '../services/network.service';
import { isNilOrBlank, isNotNilOrBlank, removeDuplicatesFromArray, toBoolean } from '../../shared/functions';
import { LocalSettingsService } from '../services/local-settings.service';
import { FormFieldDefinition } from '../../shared/form/field.model';
import { merge } from 'rxjs';
import { AlertController, NavController } from '@ionic/angular';
import { Alerts } from '../../shared/alerts';
import { ISelectPeerModalOptions } from '../peer/select-peer.modal';
import { IMenuItem } from '../menu/menu.model';

import { AppPropertiesForm } from '../form/properties/properties.form';
import { UserProfileLabel } from '../services/model/person.model';

export const APP_SETTINGS_MENU_ITEMS = new InjectionToken<IMenuItem[]>('settingsMenuItems');

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.page.html',
  providers: [{ provide: ValidatorService, useExisting: LocalSettingsValidatorService }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsPage extends AppForm<LocalSettings> implements OnInit, OnDestroy, CanLeave {
  private data: LocalSettings;

  protected mobile: boolean;
  protected isAdmin: boolean;
  protected saving = false;
  protected usageModes: UsageMode[] = ['FIELD', 'DESK'];
  protected menuItems: IMenuItem[];
  protected showOptionKeys = false;

  protected propertyDefinitions: FormFieldDefinition[];

  protected latLongFormats = ['DDMMSS', 'DDMM', 'DD'];

  get accountInheritance(): boolean {
    return this.form.controls['accountInheritance'].value;
  }

  get isLogin(): boolean {
    return this.accountService.isLogin();
  }

  get propertiesFormArray(): UntypedFormArray {
    return this.form.get('properties') as UntypedFormArray;
  }

  get darkMode(): boolean {
    return this.form.controls['darkMode'].value;
  }

  set darkMode(value: boolean) {
    this.form.controls['darkMode'].setValue(value);
  }

  @ViewChild('propertiesForm', { static: true }) propertiesForm: AppPropertiesForm;

  constructor(
    injector: Injector,
    protected platform: PlatformService,
    protected navController: NavController,
    protected validatorService: LocalSettingsValidatorService,
    protected alertCtrl: AlertController,
    protected translate: TranslateService,
    protected formBuilder: UntypedFormBuilder,
    protected accountService: AccountService,
    protected settings: LocalSettingsService,
    protected cd: ChangeDetectorRef,
    public network: NetworkService,
    @Inject(APP_LOCALES) public locales: LocaleConfig[],
    @Optional() @Inject(APP_SETTINGS_MENU_ITEMS) menuItems?: IMenuItem[]
  ) {
    super(injector, validatorService.getFormGroup());

    this.menuItems = menuItems || [];
    this.mobile = this.platform.mobile;

    // By default, disable the form
    this._enabled = false;
  }

  async ngOnInit() {
    super.ngOnInit();

    // Make sure account/settings are ready
    await Promise.all([this.settings.ready(), this.accountService.ready()]);
    this.isAdmin = this.accountService.isAdmin();

    // Load options definitions
    this.loadPropertyDefinitions();

    // Load settings
    await this.load();

    this.registerSubscription(
      merge(this.accountService.onLogin, this.accountService.onLogout).subscribe(() => {
        this.setAccountInheritance(this.accountInheritance);

        // Reload options definitions
        this.loadPropertyDefinitions();
      })
    );
  }

  protected loadPropertyDefinitions() {
    let definitions = [
      // Add account field (BEFORE local settings, because of removeDuplicatesFromArray() - see bellow)
      ...this.accountService.optionDefs.map(
        (def) =>
          <FormFieldDefinition>{
            ...def,
            minProfile: def?.minProfile || <UserProfileLabel>'GUEST',
          }
      ),
      // Add local settings options
      ...this.settings.optionDefs,
    ].map((definition) => {
      // Disabled items that should not be visible
      if (!definition.disabled && definition.minProfile && !this.accountService.hasMinProfile(definition.minProfile)) {
        return {
          ...definition,
          disabled: true,
        };
      }
      return definition;
    });

    // Remove duplicated key
    this.propertyDefinitions = removeDuplicatesFromArray(definitions, 'key');

    if (this.loaded) this.markForCheck();
  }

  async load() {
    this.markAsLoading();
    console.debug('[settings] Loading settings...');

    const data = this.settings.settings;

    // Set defaults
    data.accountInheritance = toBoolean(data.accountInheritance, true);
    data.locale = data.locale || this.translate.currentLang || this.translate.defaultLang;
    data.latLongFormat = data.latLongFormat || 'DDMMSS';
    data.usageMode = data.usageMode || 'DESK';

    // Set peer
    if (isNilOrBlank(data.peerUrl)) {
      await this.network.ready();
      const peer = this.network.peer;
      data.peerUrl = peer && peer.url;
    }

    // Remember data
    await this.updateView(data);
  }

  async updateView(data: LocalSettings) {
    if (!data) return; //skip
    this.data = data;

    const json = {
      ...data,
      properties: undefined,
    };

    this.form.patchValue(json, { emitEvent: true });
    this.propertiesForm.setValue(data.properties);

    this.markAsPristine();

    this.enable({ emitEvent: false });

    // Apply inheritance
    this.setAccountInheritance(data.accountInheritance, { emitEvent: false });

    this.markAsLoaded();
    this.error = null;
    this.markForCheck();
  }

  async save(event?: Event, options?: any): Promise<boolean> {
    if (this.saving) return; // Skip

    if (event) event.preventDefault();

    if (!this.form.valid) {
      await AppFormUtils.waitWhilePending(this.form);

      if (this.form.invalid) {
        AppFormUtils.logFormErrors(this.form);
        this.form.markAllAsTouched();
        return;
      }
    }

    console.debug('[settings] Saving local settings...');

    this.saving = true;
    this.error = undefined;
    this.markForCheck();

    const data = await this.getValue();

    // Check peer alive, before saving
    const peerChanged = this.form.get('peerUrl').dirty;

    // Clean page history, when peer changed
    if (peerChanged) data.pageHistory = [];

    try {
      this.disable();

      await this.settings.apply(data);
      this.data = data;
      this.markAsPristine();

      // If peer changed
      if (peerChanged) {
        // Restart the network
        this.network.peer = Peer.parseUrl(data.peerUrl);
      }

      return true;
    } catch (err) {
      console.error(err);
      this.error = (err && err.message) || err;
      return false;
    } finally {
      this.enable({ emitEvent: false });

      // Apply inheritance
      this.setAccountInheritance(data.accountInheritance, { emitEvent: false });

      // Add a delay, because of option menu that is always opened after save!!
      setTimeout(() => {
        this.saving = false;
        this.markForCheck();
      }, 400);
    }
  }

  setAccountInheritance(enable: boolean, opts?: { emitEvent?: boolean }) {
    // Make sure to update the value in control
    this.form.controls['accountInheritance'].setValue(enable, opts);
    if (this.data.accountInheritance !== enable) {
      this.form.controls['accountInheritance'].markAsDirty({ onlySelf: false });
    }

    if (enable) {
      if (this.isLogin) {
        // Force using account settings
        const account = this.accountService.account;

        // Copy values
        if (account.settings) {
          if (account.settings.locale) this.form.get('locale').setValue(account.settings.locale, opts);
          if (account.settings.latLongFormat) this.form.get('latLongFormat').setValue(account.settings.latLongFormat, opts);
        }
        // Disable fields
        this.form.get('locale').disable(opts);
        this.form.get('latLongFormat').disable(opts);
      } else {
        // Enable fields
        this.form.get('locale').enable(opts);
        this.form.get('latLongFormat').enable(opts);
      }
    } else {
      // Restore previous values
      this.form.get('locale').setValue(this.data.locale, opts);
      this.form.get('latLongFormat').setValue(this.data.latLongFormat, opts);

      // Enable fields
      this.form.get('locale').enable(opts);
      this.form.get('latLongFormat').enable(opts);
    }

    // Mark for check, if need
    if (!opts || opts.emitEvent) {
      this.markForCheck();
    }
  }

  async showSelectPeerModal(opts?: Partial<ISelectPeerModalOptions>) {
    const selectedPeer = this._form.get('peerUrl').value;
    const peer = await this.network.showSelectPeerModal({
      showSetManuallyButton: false, // Make no sense here, because same as cancel (already inside the settings page)
      selectedPeer: isNotNilOrBlank(selectedPeer) ? selectedPeer : null,
      ...opts,
    });
    if (peer?.url) {
      const control = this.form.get('peerUrl') as UntypedFormControl;
      control.setValue(peer.url, { emitEvent: true, onlySelf: false });
      control.markAsDirty({ onlySelf: false });
      this.markAsDirty();
    }
  }

  protected setForceOffline(value: boolean) {
    this.network.setForceOffline(value);
    this.form.get('peerUrl').markAsPending(); // Force revalidation
    this.form.markAsDirty();
    this.form.updateValueAndValidity();
    this.form.markAsTouched();
  }

  async cancel(event?: Event) {
    await this.load();
  }

  async close(_?: Event) {
    if (this.saving) return;

    // Back to home
    return this.navController.navigateRoot('/'); // back to home
  }

  async clearCache(event?: Event) {
    const confirm = await Alerts.askActionConfirmation(this.alertCtrl, this.translate, true, event);
    if (confirm) {
      await this.network.clearCache();
      this.settings.removeOfflineFeatures();
    }
  }

  async executeAction(event: Event, item: IMenuItem) {
    if (!item) return;
    if (event?.defaultPrevented) return;

    if (item.path) {
      await this.navController.navigateForward(item.path);
    } else {
      console.error('[settings] Unknown item type (no path)', item);
    }
  }

  async getValue(): Promise<LocalSettings> {
    const json = this.form.value;

    // Serialize properties to map (from array)
    json.properties = this.propertiesForm.getValueAsJson();

    // Override properties from account
    if (json.accountInheritance && this.isLogin) {
      const account = this.accountService.account;
      const userSettings = account && account.settings;
      console.debug(`[settings] Applying account inheritance {locale: '${userSettings.locale}', latLongFormat: '${userSettings.latLongFormat}'}...`);
      json.locale = userSettings.locale || json.locale;
      json.latLongFormat = userSettings.latLongFormat || json.latLongFormat;
    }

    return json;
  }

  /* -- protected functions -- */

  markAsLoading() {
    super.markAsLoading();
    this.propertiesForm.markAsLoading();
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    super.markAsLoaded(opts);
    this.propertiesForm.markAsLoaded(opts);
  }

  get loading(): boolean {
    return super.loading || this.propertiesForm.loading;
  }

  get loaded(): boolean {
    return super.loaded || this.propertiesForm.loaded;
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.propertiesForm.enable(opts);
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    this.propertiesForm.disable(opts);
  }

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.markAsPristine(opts);
    this.propertiesForm.markAsPristine(opts);
  }

  @HostListener('window:beforeunload', ['$event'])
  handleRefresh(event: BeforeUnloadEvent): void {
    // Avoid to quit web browser page, without saving
    if (!this.mobile && this.dirty) {
      event.preventDefault();
    }
  }

  protected openAccountPage() {
    return this.navController.navigateForward('account');
  }

  protected async toggleDarkMode(enable?: boolean) {
    const oldValue = this.form.get('darkMode').value;
    enable = toBoolean(enable, !oldValue);
    console.info('[settings] Changing darkMode=' + enable);
    this.platform.toggleDarkTheme(enable);

    if (oldValue !== enable) {
      this.form.get('darkMode').setValue(enable, {
        emitEvent: false,
      });
      // Disable auto dark theme
      this.form.get('autoDarkMode').setValue(false, {
        emitEvent: false,
      });
    }
    await this.save();
  }

  protected toggleShowOptionKeys() {
    this.showOptionKeys = !this.showOptionKeys;
    this.markForCheck();
  }

  protected devToggleDebug() {
    this.debug = !this.debug;
    this.markForCheck();
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
