import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { SelectPeerModal } from './select-peer.modal';
import { TranslateModule } from '@ngx-translate/core';
import { AppTextPopoverModule } from '../form/text-popover/text-popover.module';
import { CorePipesModule } from '../services/pipes/pipes.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    // Network
    SelectPeerModal,
  ],
  exports: [
    TranslateModule,
    // Components
    SelectPeerModal,
  ],
  imports: [SharedModule, TranslateModule.forChild(), AppTextPopoverModule, CorePipesModule, FormsModule],
  providers: [provideHttpClient(withInterceptorsFromDi())],
})
export class AppSelectPeerModule {}
