import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  numberAttribute,
  OnDestroy,
  Optional,
  ViewChild,
} from '@angular/core';
import { IonSearchbar, ModalController, PopoverController } from '@ionic/angular';
import { Peer } from '../services/model/peer.model';
import { Subscription } from 'rxjs';
import { fadeInAnimation } from '../../shared/material/material.animations';
import { HttpClient } from '@angular/common/http';
import { NetworkUtils, NodeInfo } from '../services/network.utils';
import { VersionUtils } from '../../shared/version/versions';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { TextPopover, TextPopoverOptions } from '../form/text-popover/text-popover.component';
import { TranslateService } from '@ngx-translate/core';
import { isMobile } from '../../shared/platforms';
import { Validators } from '@angular/forms';
import { PEER_URL_REGEXP } from '../services/network.service';
import { APP_LOGGING_SERVICE } from '../../shared/logging/logging-service.class';
import { ILogger, ILoggingService } from '../../shared/logging/logger.model';
import { map } from 'rxjs/operators';
import { EntityUtils } from '../services/model/entity.model';
import { RxState } from '@rx-angular/state';
import { suggestFromArray } from '../../shared/services';
import { isNilOrBlank, isNotEmptyArray, isNotNil } from '../../shared/functions';
import { NodeFeature } from '../services/model/node-feature.model';

export interface ISelectPeerModalOptions {
  canCancel: boolean;
  allowSelectDownPeer: boolean;
  onRefresh: EventEmitter<Event>;
  showSetManuallyButton: boolean;
  selectedPeer?: string;
  title?: string;
  defaultPeers: Peer[];
  showSearchBar?: boolean;
}

export interface ISelectPeerModalState {
  defaultPeers: Peer[];
  peers: Peer[];
  filteredPeers: Peer[];
  filteredPeersByFeatures: Peer[];
  filteredPeerCount: number;
  filteredFeaturesCount: number;
  searchText: string;
  selectedTabIndex: number;
}

@Component({
  selector: 'select-peer-modal',
  templateUrl: 'select-peer.modal.html',
  styleUrls: ['./select-peer.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeInAnimation],
})
export class SelectPeerModal extends RxState<ISelectPeerModalState> implements OnDestroy, ISelectPeerModalOptions {
  private _subscription = new Subscription();
  private readonly _logger: ILogger;

  protected defaultPeers$ = this.select('defaultPeers');
  protected peers$ = this.select('peers');
  protected filteredPeers$ = this.select('filteredPeers');
  protected filteredPeersByFeatures$ = this.select('filteredPeersByFeatures');
  protected filteredPeerCount$ = this.select('filteredPeerCount');
  protected filteredFeaturesCount$ = this.select('filteredFeaturesCount');

  protected loading = true;
  protected mobile: boolean;
  protected peerMinVersion: string;
  protected peerDefaultPrefix: string;
  protected enableSelectPeerByFeature: boolean;
  protected collapsedPeers: { [key: string]: boolean } = {};

  set peers(value: Peer[]) {
    this.set('peers', () => value);
  }
  get peers(): Peer[] {
    return this.get('peers');
  }
  set searchText(value: string) {
    this.set('searchText', () => value);
  }
  get searchText(): string {
    return this.get('searchText');
  }
  set filteredPeers(value: Peer[]) {
    this.set('filteredPeers', () => value);
  }
  get filteredPeers(): Peer[] {
    return this.get('filteredPeers');
  }

  @Input({ transform: numberAttribute }) set selectedTabIndex(value: number) {
    this.set('selectedTabIndex', () => value);
  }
  get selectedTabIndex(): number {
    return this.get('selectedTabIndex');
  }

  @Input() selectedPeer: string;
  @Input() title = 'NETWORK.PEER.SELECT_MODAL.TITLE';
  @Input({ transform: booleanAttribute }) canCancel = true;
  @Input({ transform: booleanAttribute }) allowSelectDownPeer = true;
  @Input({ transform: booleanAttribute }) showSetManuallyButton = true;
  @Input({ transform: booleanAttribute }) showSearchBar: boolean;
  @Input() tabGroupAnimationDuration: '200ms';

  @Input() onRefresh = new EventEmitter<Event>();

  @Input() set defaultPeers(value: Peer[]) {
    this.set('defaultPeers', () => value);
  }

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;

  constructor(
    private modalCtrl: ModalController,
    private popoverController: PopoverController,
    private translate: TranslateService,
    private http: HttpClient,
    private cd: ChangeDetectorRef,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_LOGGING_SERVICE) protected loggingService?: ILoggingService
  ) {
    super();
    this.set({ searchText: null, selectedTabIndex: 0 });
    this.mobile = isMobile(window);
    this._logger = loggingService?.getLogger('network-modal');
    this.peerMinVersion = environment.peerMinVersion;
    this.peerDefaultPrefix = environment.peerDefaultPrefix || 'https://';
    this.enableSelectPeerByFeature = environment.enableSelectPeerByFeature || false;
    this.showSearchBar = this.enableSelectPeerByFeature; // Show search bar by default, if two tabs

    // Refreshing peers
    this.hold(this.defaultPeers$, (res) => this.refreshPeers(res));

    // Search on host
    this.connect(
      'filteredPeers',
      this.select(['peers', 'searchText'], (res) => res).pipe(
        map(({ peers, searchText }) => {
          if (isNilOrBlank(searchText)) return peers; // All

          console.debug('[select-peer-modal] Searching on hosts: ' + searchText);
          const { data } = suggestFromArray(peers, searchText, {
            anySearch: true,
            searchAttributes: ['label', 'hostAndPort'],
          });
          return data;
        })
      )
    );
    this.connect('filteredPeerCount', this.filteredPeers$.pipe(map((peers) => peers?.length ?? 0)));

    // Search on features
    if (this.enableSelectPeerByFeature) {
      this.connect(
        'filteredPeersByFeatures',
        this.select(['peers', 'searchText'], (res) => res).pipe(
          map(({ peers, searchText }) => {
            if (isNilOrBlank(searchText)) {
              return (peers || []).filter((p) => isNotEmptyArray(p?.features));
            }

            console.debug('[select-peer-modal] Searching on features: ' + searchText);
            return peers
              .map((peer) => {
                const { data: features } = suggestFromArray(peer.features || [], searchText, {
                  anySearch: true,
                  searchAttributes: ['label', 'name', 'description'],
                });
                if (isNotEmptyArray(features)) {
                  const filteredPeer = peer.clone();
                  filteredPeer.features = features;
                  return filteredPeer;
                }
                return null;
              })
              .filter(isNotNil);
          })
        )
      );
      this.connect('filteredFeaturesCount', this.filteredPeersByFeatures$.pipe(map(Peer.countAllFeatures)));
    }
  }

  cancel() {
    this.modalCtrl.dismiss();
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  selectPeer(peer: Peer) {
    if (this.allowSelectDownPeer || (peer.reachable && this.isCompatible(peer))) {
      console.debug(`[select-peer-modal] Selected peer: {url: '${peer.url}'}`);
      this.selectedPeer = peer.url;
      this.modalCtrl.dismiss(peer);
    }
  }

  async toggleSearchBar() {
    this.showSearchBar = !this.showSearchBar;
    if (this.showSearchBar && this.searchbar) {
      setTimeout(() => {
        this.searchbar.setFocus();
        this.markForCheck();
      }, 300);
    }
  }

  togglePeerFeatures(peerUrl: string) {
    if (!peerUrl) return;
    this.collapsedPeers[peerUrl] = isNotNil(this.collapsedPeers[peerUrl]) ? !this.collapsedPeers[peerUrl] : true;
    this.markForCheck();
  }

  /**
   *  Check the min pod version, defined by the app
   *
   * @param peer
   */
  isCompatible(peer: Peer): boolean {
    return !this.peerMinVersion || (peer && peer.softwareVersion && VersionUtils.isCompatible(this.peerMinVersion, peer.softwareVersion));
  }

  refresh(event: Event) {
    this.loading = true;
    this.onRefresh.emit(event);
  }

  async clickSetManually(event?: Event) {
    const peerUrl = await this.openPeerUrlPopover(event);
    if (peerUrl) {
      const peer = Peer.parseUrl(peerUrl);
      await this.refreshPeer(peer);
      this.selectPeer(peer);
    }
  }

  /* -- protected methods -- */
  protected clearLogo(peer: Peer | NodeFeature) {
    if (peer instanceof Peer) {
      peer.favicon = 'skip';
    } else {
      peer.logo = 'skip';
    }
  }
  async refreshPeers(peers: Peer[]) {
    peers = peers || [];

    const data: Peer[] = [];
    const jobs = Promise.all(
      peers.map(async (peer) => {
        await this.refreshPeer(peer);

        if (this._subscription.closed) return; // component destroyed

        data.push(peer);

        // Sort (by reachable, then host)
        data.sort((a, b) => {
          if (a.reachable && !b.reachable) return -1;
          if (!a.reachable && b.reachable) return 1;
          if (a.hostAndPort < b.hostAndPort) return -1;
          if (a.hostAndPort > b.hostAndPort) return 1;
          return 0;
        });

        this.peers = data.slice(); // Copy to force update

        return peer;
      })
    );

    try {
      await jobs;
    } catch (err) {
      if (!this._subscription.closed) console.error(err);
    }
    this.loading = false;
    this.cd.markForCheck();
  }

  protected async refreshPeer(peer: Peer): Promise<Peer> {
    try {
      // DEBUG
      //await sleep(Math.random() * 100);

      const summary: NodeInfo = await NetworkUtils.getNodeInfo(this.http, peer.url);
      peer.status = 'UP';
      peer.softwareName = summary.softwareName;
      peer.softwareVersion = summary.softwareVersion;
      peer.label = summary.nodeLabel;
      peer.name = summary.nodeName;
      peer.features = summary.features?.map(NodeFeature.fromObject) || [];
    } catch (err) {
      // If modal was not closed (e.g. response arrived too late => ignore)
      if (!this._subscription.closed) {
        console.error(`[select-peer] Error while calling '${peer.url}/api/node/info': ${err?.message || err}`, err);
        this._logger?.error('refreshPeer', `Error while calling '${peer.url}/api/node/info': ${err?.message || JSON.stringify(err)}`);
      }
      peer.status = 'DOWN';
    }
    return peer;
  }

  protected async openPeerUrlPopover(event: Event, opts?: Partial<TextPopoverOptions>) {
    const modal = await this.popoverController.create({
      component: TextPopover,
      componentProps: <TextPopoverOptions>{
        placeholder: this.translate.instant('SETTINGS.PEER_URL'),
        multiline: false,
        autofocus: !this.mobile,
        autocomplete: false,
        validator: Validators.pattern(PEER_URL_REGEXP),
        maxLength: null,
        text: this.selectedPeer || this.peerDefaultPrefix,
        ...opts,
      },
      backdropDismiss: false,
      keyboardClose: false,
      event: null, // IMPORTANT: null event will center the popover, and this will fix keyboard overlap on mobile
      translucent: true,
      cssClass: 'popover-large',
    });

    await modal.present();
    const { data, role } = await modal.onDidDismiss();

    if (data) {
      console.info('Popover result: ', data);
      return data;
    }
  }

  protected updateFilteredPeer(event: any) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    const value = event.target.value;

    const test = this.filteredPeers$.pipe(
      map((peers) => peers.filter(EntityUtils.searchTextFilter(['features.label', 'features.name', 'features.description'], value)))
    );

    console.log(test);
    // (event.target.value);
    this.markForCheck();
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
