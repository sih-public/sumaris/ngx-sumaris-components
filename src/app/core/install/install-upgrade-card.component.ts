import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { firstValueFrom, from, Observable, Subscription } from 'rxjs';
import { Configuration } from '../services/model/config.model';
import { ConfigService } from '../services/config.service';
import { PlatformService } from '../services/platform.service';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { NetworkService } from '../services/network.service';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { VersionUtils } from '../../shared/version/versions';
import { slideUpDownAnimation } from '../../shared/material/material.animations';
import { isNilOrBlank, isNotEmptyArray, isNotNil, isNotNilOrBlank, sleep } from '../../shared/functions';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { TranslateService } from '@ngx-translate/core';
import { Peer } from '../services/model/peer.model';
import { MimeTypes } from '../../shared/file/file.utils';
import { UriUtils } from '../../shared/file/uri.utils';
import { isAndroid, isIOS } from '../../shared/platforms';

export declare interface InstallAppLink {
  name: string;
  url: string;
  platform?: 'android' | 'ios' | 'electron';
  version?: string;
  filename?: string;
  title?: string;
  description?: string;
  mimeType?: string;
  downloading?: boolean;
  location?: string;
}

@Component({
  selector: 'app-install-upgrade-card',
  templateUrl: 'install-upgrade-card.component.html',
  styleUrls: ['./install-upgrade-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation],
})
export class AppInstallUpgradeCard implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  loading = true;
  hasDownloader = false;
  downloading = false;
  waitingNetwork = false;
  installLinks: InstallAppLink[];
  upgradeLinks: InstallAppLink[];
  offline: boolean;

  @Input() isLogin: boolean;
  @Input() showUpgradeWarning = true;
  @Input() showOfflineWarning = true;
  @Input() showInstallButton = false;
  @Input() debug: boolean;

  constructor(
    private configService: ConfigService,
    private toastController: ToastController,
    private alertController: AlertController,
    private translate: TranslateService,
    private cd: ChangeDetectorRef,
    public platform: PlatformService,
    public network: NetworkService,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    this.debug = !environment.production;
  }

  ngOnInit() {
    this.offline = this.network.offline;

    // Listen pod config
    this._subscription.add(
      from(this.network.ready())
        .pipe(
          debounceTime(1000),
          switchMap(() => this.configService.config),
          filter(isNotNil)
        )
        .subscribe((config) => this.checkNeedInstallOrUpdate(config))
    );

    // Listen network changes
    this._subscription.add(
      this.network.onNetworkStatusChanges
        .pipe(
          //debounceTime(450),
          //tap(() => this.waitingNetwork = false),
          map((connectionType) => connectionType === 'none'),
          distinctUntilChanged()
        )
        .subscribe((offline) => {
          if (this.offline !== offline) {
            this.offline = offline;
            this.markForCheck();
          }
        })
    );
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._subscription = null;
    this.installLinks = null;
    this.upgradeLinks = null;
  }

  async download(event: Event, link: InstallAppLink) {
    if (!link || !link.url) {
      console.error("[install-upgrade-card] Missing required argument 'link.url'");
      return; // Skip
    }

    // Mark link as downloading
    link.downloading = true;
    this.markForCheck();

    this.listenDownloadJob(
      link.url,
      this.platform.download({
        uri: link.url,
        title: link.title,
        filename: link.filename,
        mimeType: link.mimeType,
      })
    )
      .then((location) => {
        if (this.platform.canOpenFile) link.location = location;
        link.downloading = false;
      })
      .catch(() => (link.downloading = false))
      .then(() => this.markForCheck());
  }

  async openFile(event: Event, link: InstallAppLink) {
    if (!link || !link.location || !link.mimeType) {
      console.error("[install-upgrade-card] Missing required argument 'link.location' or 'link.mimeType'");
      return; // Skip
    }
    try {
      await this.platform.open(link.location);
    } catch (err) {
      console.error('[install-upgrade-card] Cannot open file: ' + ((err && err.message) || err), err);
    }
  }

  async tryOnline() {
    this.waitingNetwork = true;
    this.markForCheck();

    try {
      await this.network.tryOnline({
        showLoadingToast: false,
        showOnlineToast: true,
        showOfflineToast: false,
      });
    } finally {
      this.waitingNetwork = false;
      this.markForCheck();
    }
  }

  getPlatformName(platform: 'android' | 'ios' | 'electron') {
    switch (platform) {
      case 'android':
        return 'Android';
      case 'ios':
        return 'iOS';
      case 'electron':
        return 'desktop';
      default:
        return '';
    }
  }

  asLink(value: any): InstallAppLink {
    return value as InstallAppLink;
  }

  /* -- private method  -- */

  private async checkNeedInstallOrUpdate(config: Configuration) {
    if (!config) return; // Skip

    console.info('[install-upgrade-card] Check if need to install or upgrade...');

    this.hasDownloader = this.platform.canDownload;

    try {
      const links = this.getLinks(config);

      // Check for upgrade
      this.upgradeLinks = this.getCompatibleUpgradeLinks(links, config);

      if (isNotEmptyArray(this.upgradeLinks)) {
        console.info(`[install-upgrade-card] Find upgrade links: ${this.upgradeLinks.map((l) => l.url).join(',')}`);
        this.installLinks = null;
        this.listenDownloadJobs();
      } else {
        // Check for install links
        this.installLinks = this.getCompatibleInstallLinks(links);
        if (isNotEmptyArray(this.installLinks)) {
          console.info(`[install-upgrade-card] Find installation links ${this.installLinks.map((l) => l.url).join(',')}`);
          await sleep(1000); // Add a delay, for animation
        } else {
          console.info(`[install-upgrade-card] No installation or upgrade links to display.`);
        }
      }
    } finally {
      this.loading = false;
      this.markForCheck();
    }
  }

  private getCompatibleInstallLinks(installLinks: InstallAppLink[]): InstallAppLink[] {
    // Already an native App: not need to install
    if (this.platform.isApp()) return undefined;

    // If mobile web: return all compatible, depending on the platform
    if (this.platform.isMobileWeb()) {
      return installLinks.filter(
        (link) =>
          !link.platform ||
          this.platform.is(link.platform) ||
          (link.platform === 'android' && isAndroid(window)) ||
          (link.platform === 'ios' && isIOS(window))
      );
    }

    return undefined;
  }

  private getCompatibleUpgradeLinks(installLinks: InstallAppLink[], config: Configuration): InstallAppLink[] {
    const appVersion = this.environment.version;
    const appMinVersionFromPod = config.getProperty(CORE_CONFIG_OPTIONS.APP_MIN_VERSION);

    if (!appVersion) {
      console.error("Missing value for 'environment.version': cannot check app compatibility!");
      return undefined;
    }

    const needUpgrade = appMinVersionFromPod && !VersionUtils.isCompatible(appMinVersionFromPod, appVersion);
    if (!needUpgrade) return undefined;

    // Filter on platform, if mobile App
    const upgradeLinks = (installLinks || []).filter((link) => link.platform && this.platform.is(link.platform));

    // Use min version as default version
    upgradeLinks.forEach((link) => {
      link.version = link.version || appMinVersionFromPod;
    });

    return isNotEmptyArray(upgradeLinks) ? upgradeLinks : undefined;
  }

  private getLinks(config: Configuration): InstallAppLink[] {
    const result: InstallAppLink[] = [];

    // Compute App name
    const minVersion = config.getProperty(CORE_CONFIG_OPTIONS.APP_MIN_VERSION);
    const defaultAppName = this.environment.defaultAppName || 'SUMARiS';

    // Android
    {
      // Get URL (from config, or environment)
      let url = config.getProperty(CORE_CONFIG_OPTIONS.ANDROID_INSTALL_URL);
      if (isNilOrBlank(url)) url = this.environment.defaultAndroidInstallUrl || null;

      // Resolve relative URL
      const peer = this.network.peer;
      if (peer && url && (url.startsWith('./') || url.startsWith('/'))) {
        url = Peer.path(peer, url);
      }

      // Compute App name (use config if URL is specific)
      const name: string = isNotNilOrBlank(url) ? config.label : defaultAppName;

      if (url) {
        let title: string;
        let mimeType: string;

        // Get file name
        let filename = UriUtils.getFilename(url);
        let version = minVersion || 'latest';

        // OK, this is a downloadable APK file (e.g. NOT a link to a playstore)
        if (filename?.toLowerCase().endsWith('.apk')) {
          // Define mime type
          mimeType = MimeTypes.ANDROID_APK;

          // Get the file version (if any)
          const versionMatches = /-v([1-9][0-9]*\.[0-9]+\.(:?(:?alpha|beta|rc)?[0-9]*))/i.exec(filename);
          version = (versionMatches && versionMatches[1]) || version;

          // Compute a new file name, with the version
          if (isNotNilOrBlank(name) && version !== 'latest') {
            filename = `${name}-v${version}.apk`.toLowerCase();
            title = `${name} v${version}`;
          }

          // Replace 'latest' with the app version, if present in the filename
          else if (filename.indexOf('latest') !== -1 && version !== 'latest') {
            filename = filename.replace('latest', version);
          }
        }

        result.push({ name, url, platform: 'android', version, filename, title, mimeType });
      }
    }

    // iOS
    {
      // Get URL (from config, or environment)
      let url = config.getProperty(CORE_CONFIG_OPTIONS.IOS_INSTALL_URL);
      if (isNilOrBlank(url)) url = this.environment.defaultIOSInstallUrl || null;

      // Resolve relative URL
      const peer = this.network.peer;
      if (peer && url && (url.startsWith('./') || url.startsWith('/'))) {
        url = Peer.path(peer, url);
      }

      // Compute App name (use config if URL is specific)
      const name: string = isNotNilOrBlank(url) ? config.label : defaultAppName;

      if (url) {
        const version = minVersion || 'latest';
        const title = `${name} v${version}`;
        result.push({ name, url, platform: 'ios', version, title });
      }
    }

    // Desktop (Electron)
    {
      // Get URL (from config, or environment)
      let url = config.getProperty(CORE_CONFIG_OPTIONS.DESKTOP_INSTALL_URL);
      if (isNilOrBlank(url)) url = this.environment.defaultDesktopInstallUrl || null;

      // Resolve relative URL
      const peer = this.network.peer;
      if (peer && url && (url.startsWith('./') || url.startsWith('/'))) {
        url = Peer.path(peer, url);
      }

      // Compute App name (use config if URL is specific)
      const name: string = isNotNilOrBlank(url) ? config.label : defaultAppName;

      if (url) {
        const version = minVersion || 'latest';
        const title = `${name} v${version}`;
        result.push({ name, url, platform: 'electron', version, title });
      }
    }

    return result;
  }

  private listenDownloadJobs() {
    // Listening downloading promise, if exists
    (this.upgradeLinks || []).forEach((link) => {
      const job = this.platform.getDownloadingJob(link.url);
      if (job) {
        link.downloading = true;
        this.listenDownloadJob(link.url, job)
          .then((location) => {
            link.location = location;
            link.downloading = false;
          })
          .catch(() => (link.downloading = false))
          .then(() => this.markForCheck());
      }
    });
  }

  private async listenDownloadJob(url: string, job: Observable<string>): Promise<string> {
    if (!url || !job) return; // Skip

    try {
      // Mark as downloading
      if (!this.downloading) {
        console.info(`[install-upgrade-card] Asking platform to download '${url}'...`);
        this.downloading = true;
        this.markForCheck();
      } else {
        console.info(`[install-upgrade-card] Platform is already downloading!`);
      }

      const location = await firstValueFrom(job);
      if (location) {
        console.info('[install-upgrade-card] File downloaded at: ' + location);
        this.showDownloadCompleteDialog();
      }
      return location;
    } catch (err) {
      // Failed: display a toast
      console.error('[install-upgrade-card] Download failed: ' + ((err && err.message) || err));
      this.showDownloadFailedToast(err);
      throw err;
    } finally {
      this.downloading = false;
      this.markForCheck();
    }
  }

  private showDownloadFailedToast(err) {
    return this.showToast({
      message: 'ERROR.DOWNLOAD_FAILED',
      messageParams: { error: err },
      type: 'error',
      showCloseButton: true,
    });
  }

  private async showDownloadCompleteDialog() {
    if (!this._subscription) return; // Skip if component has been destroyed

    const translations = await this.translate.instant(['INFO.ALERT_HEADER', 'INFO.UPDATE_APP_DOWNLOADED', 'COMMON.BTN_CLOSE']);
    const alert = await this.alertController.create({
      header: translations['INFO.ALERT_HEADER'],
      message: translations['INFO.UPDATE_APP_DOWNLOADED'],
      buttons: [
        {
          text: translations['COMMON.BTN_CLOSE'],
          role: 'cancel',
          cssClass: 'secondary',
        },
      ],
    });

    await alert.present();
    await alert.onDidDismiss();
  }

  private markForCheck() {
    if (!this._subscription) return; // Skip if destroyed
    this.cd.markForCheck();
  }

  private async showToast(opts: ShowToastOptions) {
    if (!this._subscription) return; // Skip if component has been destroyed
    await Toasts.show(this.toastController, this.translate, opts);
  }
}
