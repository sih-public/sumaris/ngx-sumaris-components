import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AppInstallUpgradeCard } from './install-upgrade-card.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],
  declarations: [
    // Components
    AppInstallUpgradeCard,
  ],
  exports: [
    TranslateModule,
    // Components
    AppInstallUpgradeCard,
  ],
})
export class AppInstallUpgradeCardModule {}
