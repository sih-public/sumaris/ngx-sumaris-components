import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { AppFormProvider, AppFormUtils, IAppForm, IAppFormGetter, OnReady } from './form.utils';
import { waitForFalse, WaitForOptions, waitForTrue } from '../../shared/observables';
import { AppTable } from '../table/table.class';
import { AppForm } from './form.class';
import { AppEditor } from './entity/editor.class';
import { AbstractControl } from '@angular/forms';
import { AppAsyncTable } from '../table/async-table.class';

export interface IAppFormContainer<T = any> extends IAppForm<T> {
  children: IAppForm[];
  tables: AppTable<any>[];
  asyncTables: AppAsyncTable<any>[];
  forms: AppForm<any>[];
  editors: AppEditor<any>[];

  addForm(path: string, form?: IAppForm | IAppFormGetter): void;
  addForms(forms: (IAppForm | IAppFormGetter)[]): void;
  removeForm(form: string | IAppForm | IAppFormGetter): IAppForm | IAppFormGetter;
}

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppFormContainer<T = any> implements IAppFormContainer<T>, OnInit, OnDestroy {
  private _children: IAppForm[];
  private _childrenPath: string[];
  private _subscription = new Subscription();
  protected _logPrefix = '[form-container] ';
  protected _enabled = false;

  protected readonly destroySubject = new Subject<void>();

  // TODO use RxState instead ?
  readonly readySubject = new BehaviorSubject<boolean>(false);
  readonly loadingSubject = new BehaviorSubject<boolean>(true);
  readonly dirtySubject = new BehaviorSubject<boolean>(false);
  readonly touchedSubject = new BehaviorSubject<boolean>(false);
  readonly errorSubject = new BehaviorSubject<string>(undefined);

  get children(): IAppForm[] {
    return this._children;
  }

  get tables(): AppTable<any>[] {
    return this._children && (this._children.filter((c) => c instanceof AppTable) as AppTable<any>[]);
  }

  get asyncTables(): AppAsyncTable<any>[] {
    return this._children && (this._children.filter((c) => c instanceof AppAsyncTable) as AppAsyncTable<any>[]);
  }

  get forms(): AppForm<any>[] {
    return this._children && (this._children.filter((c) => c instanceof AppForm) as AppForm<any>[]);
  }

  get editors(): AppEditor<any>[] {
    return this._children && (this._children.filter((c) => c instanceof AppEditor) as AppEditor[]);
  }

  get error(): string {
    return this.errorSubject.value;
  }

  set error(error: string) {
    this.setError(error);
  }

  get loading(): boolean {
    return this.loadingSubject.value || (this._children && this._children.findIndex((c) => c.enabled && c.loading) !== -1) || false;
  }

  get loaded(): boolean {
    return (!this.loadingSubject.value && (!this._children || this._children.findIndex((c) => c.enabled && c.loading) === -1)) || false;
  }

  get dirty(): boolean {
    return this.dirtySubject.value || (this._children && this._children.findIndex((c) => c.enabled && c.dirty) !== -1) || false;
  }

  get valid(): boolean {
    // Important: Should be not invalid AND not pending, so use '!valid' (DO NOT use 'invalid')
    return !this._children || this._children.findIndex((c) => c.enabled && !c.valid) === -1;
  }

  get invalid(): boolean {
    return (this._children && this._children.findIndex((c) => c.enabled && c.invalid) !== -1) || false;
  }

  get pending(): boolean {
    return (this._children && this._children.findIndex((c) => c.enabled && c.pending) !== -1) || false;
  }

  get empty(): boolean {
    return !this._enabled;
  }

  get touched(): boolean {
    return this.touchedSubject.value || (this._children && this._children.findIndex((c) => c.enabled && c.touched) !== -1) || false;
  }

  get untouched(): boolean {
    return (!this.touchedSubject.value && (!this._children || this._children.findIndex((c) => c.enabled && c.touched) === -1)) || false;
  }

  get enabled(): boolean {
    return this._enabled;
  }

  get disabled(): boolean {
    return !this._enabled;
  }

  get destroyed(): boolean {
    return this.destroySubject?.closed !== false;
  }

  @Input() debug = false;
  protected constructor() {}

  ngOnInit() {
    // Can be override by sub classes
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();

    this.readySubject.unsubscribe();
    this.loadingSubject.unsubscribe();
    this.touchedSubject.unsubscribe();
    this.errorSubject.unsubscribe();

    this.destroySubject.next();
    this.destroySubject.unsubscribe();
  }

  addForm(path: string, form?: IAppForm | IAppFormGetter) {
    if (!form) throw new Error('Trying to register an undefined child form');
    this._children = this._children || [];
    if (typeof form === 'function') {
      this._children.push(new AppFormProvider(form));
    } else {
      this._children.push(form);
    }
    this._childrenPath = this._childrenPath || [];
    this._childrenPath.push(path || null);
  }

  addForms(forms: (IAppForm | IAppFormGetter)[]) {
    (forms || []).forEach((form) => this.addForm(null, form));
  }

  removeForm(form: string | IAppForm | IAppFormGetter): IAppForm | IAppFormGetter {
    if (!form) throw new Error('Trying to remove an undefined child form');
    const index =
      typeof form == 'string' ? (this._childrenPath || []).findIndex((c) => c === form) : (this._children || []).findIndex((c) => c === form);
    if (index !== -1) {
      this._childrenPath.splice(index, 1);
      return this._children.splice(index, 1)?.[0];
    }
    return undefined;
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this._enabled = false;
    this._children?.forEach((c) => c.disable(opts));
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this._enabled = true;
    this._children?.forEach((c) => c.enable(opts));
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.error = null;
    if (this.dirtySubject.value !== false) {
      this.dirtySubject.next(false);
    }
    this._children?.forEach((c) => c.markAsPristine(opts));
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.touchedSubject.value !== false) {
      this.touchedSubject.next(false);
    }
    this._children?.forEach((c) => c.markAsUntouched());
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  /**
   * @deprecated prefer to use markAllAsTouched()
   * @param opts
   */
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (!this.touchedSubject.value !== true) {
      this.touchedSubject.next(true);
    }
    this._children?.forEach((c) => c.markAsTouched(opts));
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  markAllAsTouched(opts?: { emitEvent?: boolean }) {
    if (this.touchedSubject.value !== true) {
      this.touchedSubject.next(true);
    }
    this._children?.forEach((c) => c.markAllAsTouched(opts));
    if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.dirtySubject.value !== true) {
      this.dirtySubject.next(true);
      if (!this.loading && (!opts || opts.emitEvent !== false)) this.markForCheck();
    }
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    if (!this.loadingSubject.closed && this.loadingSubject.value !== true) {
      this.loadingSubject.next(true);

      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  markAsLoaded(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (!this.loadingSubject.closed && this.loadingSubject.value !== false) {
      this.loadingSubject.next(false);
      if (!opts || opts.onlySelf !== true) {
        // Emit to children forms and editors
        // note: tables should be changed by parent
        this.forms?.forEach((f) => f.markAsLoaded(opts));
        this.editors?.forEach((e) => e.markAsLoaded(opts));
      }
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  markAsReady(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this._children?.forEach((c) => c.markAsReady(opts));
    if (!this.readySubject.closed && this.readySubject.value !== true) {
      this.readySubject.next(true);

      // If subclasses implements OnReady
      if (typeof this['ngOnReady'] === 'function') {
        (this as any as OnReady).ngOnReady();
      }
    }
  }

  /**
   * Wait form is ready
   *
   * @param opts
   */
  ready(opts?: WaitForOptions): Promise<void> {
    if (this.readySubject.value) return Promise.resolve();
    return waitForTrue(this.readySubject, { stop: this.destroySubject, ...opts });
  }

  /**
   * Wait form is not busy (=loaded)
   *
   * @param opts
   */
  waitIdle(opts?: WaitForOptions): Promise<void> {
    if (!this.loadingSubject.value) return Promise.resolve();
    return waitForFalse(this.loadingSubject, { stop: this.destroySubject, ...opts });
  }

  logFormErrors(prefix?: string) {
    if (this.debug) console.debug('[editor] Editor not valid. Checking where (forms, tables)...');
    this._children?.forEach((c, index) => {
      const path = this._childrenPath[index];
      this.logChildrenErrors(c, prefix, path);
    });
  }

  /* -- protected methods -- */

  protected registerSubscription(sub: Subscription) {
    this._subscription.add(sub);
  }

  protected unregisterSubscription(sub: Subscription): void {
    this._subscription.remove(sub);
  }

  protected setError(value: string, opts?: { emitEvent?: boolean }) {
    // Compare using '!=' to detect null and undefined as same value
    if (this.errorSubject.value != value) {
      this.errorSubject.next(value);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  protected resetError(opts?: { emitEvent?: boolean }) {
    this.setError(undefined, opts);
  }

  protected markForCheck() {
    // Should be overwritten by subclasses
    if (this.debug) console.warn('markForCheck() has been called, but is not overwritten by component ' + this.constructor.name);
  }

  protected logChildrenErrors(c: IAppForm | AbstractControl, prefix?: string, path?: string) {
    prefix = prefix || this._logPrefix;
    // If editor
    if (c instanceof AppEditor) {
      if (!c.empty && !c.valid) {
        if (c.pending) {
          const path = c.constructor.name.toLowerCase();
          console.warn(`${prefix} -> ${path}: waiting while pending...`);
          AppFormUtils.waitWhilePending(c, { timeout: 1000 }).then(() => c.logFormErrors(prefix));
        } else {
          c.logFormErrors(prefix);
        }
      }
    }
    // Angular form control
    else if (c instanceof AbstractControl) {
      AppFormUtils.logFormErrors(c, prefix, path);
    }
    // If form provider: get delegate
    else if (c instanceof AppFormProvider) {
      if (!c.empty && !c.valid) {
        c.waitDelegate({ timeout: 1000 }).then((delegate) => this.logChildrenErrors(delegate, prefix, path));
      }
    }
    // If table
    else if (c instanceof AppTable) {
      if (c.invalid && c.editedRow && c.editedRow.validator) {
        const path = c.constructor.name.toLowerCase();
        AppFormUtils.logFormErrors(c.editedRow.validator, prefix, path);
      }
    } else if (c instanceof AppAsyncTable) {
      if (c.invalid && c.dataSource?.hasSomeEditingRow()) {
        const path = c.constructor.name.toLowerCase();
        c.dataSource.getEditingRows().forEach((editedRow) => {
          AppFormUtils.logFormErrors(editedRow.validator, prefix, path);
        });
      }
    }
    // If form
    else if (c instanceof AppForm) {
      if (!c.empty && !c.valid) {
        if (c.pending) {
          const path = c.constructor.name.toLowerCase();
          console.warn(`${prefix} -> ${path}: waiting while pending...`);
          AppFormUtils.waitWhilePending(c, { timeout: 1000 }).then(() => this.logChildrenErrors(c.form, prefix, path));
        } else {
          AppFormUtils.logFormErrors(c.form, prefix);
        }
      }
    } else {
      console.warn(`${prefix} Unknown children sub-form: `, c);
    }
  }
}
