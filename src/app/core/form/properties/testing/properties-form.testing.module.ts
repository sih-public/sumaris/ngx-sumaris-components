import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PropertiesFormTestPage } from './properties-form.test';
import { SharedModule } from '../../../../shared/shared.module';
import { CoreModule } from '../../../core.module';

@NgModule({
  imports: [CommonModule, SharedModule, CoreModule, TranslateModule.forChild()],
  declarations: [PropertiesFormTestPage],
  exports: [RouterModule, PropertiesFormTestPage],
})
export class PropertiesFormTestingModule {}
