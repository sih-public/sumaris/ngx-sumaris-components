import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormFieldDefinition } from '../../../../shared/form/field.model';
import { AppPropertiesForm } from '../properties.form';
import { StatusIds } from '../../../services/model/model.enum';
import { suggestFromArray } from '../../../../shared/services';
import { LoadResult, SuggestFn } from '../../../../shared/services/entity-service.class';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PropertyValidator } from '../property.validator';
import { PropertyMap } from '../../../../shared/types';
import { SortDirection } from '@angular/material/sort';

const TEST_PROPERTIES = Object.freeze({
  // Entity
  ENTITY: <FormFieldDefinition>{
    key: 'sumaris.properties.entity',
    label: 'Select one entity',
    type: 'entity',
    autocomplete: {
      showAllOnFocus: true,
      filter: {
        entityName: 'LocationLevel',
        statusIds: [StatusIds.DISABLE, StatusIds.ENABLE],
      },
      attributes: ['label', 'name'],
    },
    defaultValue: null,
  },

  // Entities
  ENTITIES: <FormFieldDefinition>{
    key: 'sumaris.properties.entities',
    label: 'Select many entities',
    type: 'entities',
    autocomplete: {
      showAllOnFocus: true,
      filter: {
        entityName: 'LocationLevel',
        statusIds: [StatusIds.DISABLE, StatusIds.ENABLE],
      },
      attributes: ['label', 'name'],
    },
    defaultValue: null,
  },
});

const FAKE_ENTITIES = [
  { id: 1, label: 'AAA', name: 'Item A', description: 'Very long description A', comments: 'Very very long comments... again for A' },
  { id: 2, label: 'BBB', name: 'Item B', description: 'Very long description B', comments: 'Very very long comments... again for B' },
  { id: 3, label: 'CCC', name: 'Item C', description: 'Very long description C', comments: 'Very very long comments... again for C' },
];

@Component({
  selector: 'app-properties-form-test',
  templateUrl: './properties-form.test.html',
})
export class PropertiesFormTestPage implements OnInit, AfterViewInit {
  propertyDefinitions: FormFieldDefinition[];
  i18nFieldPrefix: string;
  form: FormGroup;

  @ViewChild('propertiesForm', { read: AppPropertiesForm, static: true }) propertiesForm: AppPropertiesForm;

  constructor(
    protected formBuilder: FormBuilder,
    protected pv: PropertyValidator
  ) {
    this.i18nFieldPrefix = '';
    this.form = formBuilder.group({
      properties: formBuilder.array([
        this.formBuilder.group({
          key: [null, Validators.compose([Validators.required, Validators.max(50)])],
          value: [null, Validators.required],
        }),
      ]),
    });

    const suggestFn: SuggestFn<any, any> = (value, filter, sortBy, sortDirection, options) =>
      this.suggestEntity(value, filter, sortBy, sortDirection, options);

    this.propertyDefinitions = Object.values(TEST_PROPERTIES).map((def) => {
      if (def.type === 'entity' || def.type === 'entities') {
        def = { ...def }; // Copy
        def.autocomplete.suggestFn = suggestFn;
      }
      return def;
    });
  }

  ngOnInit() {
    this.propertiesForm.markAsReady();
    this.propertiesForm.enable();
  }

  async ngAfterViewInit() {
    // Create the map, as it comes from the pod
    const json = <PropertyMap>{
      // Simple entity
      [TEST_PROPERTIES.ENTITY.key]: FAKE_ENTITIES[0].id.toString(),

      // Multiples entities
      [TEST_PROPERTIES.ENTITIES.key]: [FAKE_ENTITIES[0], FAKE_ENTITIES[1]].map((entity) => entity.id).join(','),
    };
    await this.propertiesForm.setValue(json);
  }

  protected async suggestEntity(value: any, filter: any, sortBy: any | string, sortDirection: SortDirection, options: any): Promise<LoadResult<any>> {
    return suggestFromArray(FAKE_ENTITIES, value, filter, sortBy, sortDirection, options);
  }
}
