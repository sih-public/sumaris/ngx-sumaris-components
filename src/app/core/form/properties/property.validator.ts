import { Injectable } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Property } from '../../../shared/types';
import { AppValidatorService } from '../../services/validator/base.validator.class';
import { isNil, isNotEmptyArray } from '../../../shared/functions';
import { FormFieldDefinition } from '../../../shared/form/field.model';
import { AppFormArray } from '../array/form-array';
import { AppPropertiesUtils } from './properties.utils';

export interface PropertyValidatorOptions {
  definition?: FormFieldDefinition;
  valueMaxLength?: number;
}

export interface PropertiesValidatorOptions {
  definitions?: FormFieldDefinition[];
  valueMaxLength?: number;
}

@Injectable({ providedIn: 'root' })
export class PropertyValidator extends AppValidatorService<Property> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder, null);
  }

  getFormArray(data?: any, opts?: PropertiesValidatorOptions) {
    const fromArray = new AppFormArray<Property, UntypedFormGroup>(
      (value) => this.getFormGroup(value, { valueMaxLength: 2000 }),
      (v1, v2) => this.equals(v1, v2),
      (value) => this.isEmpty(value),
      {
        allowReuseControls: false,
        allowEmptyArray: true,
      }
    );

    // Set data, if any
    if (data) {
      const dataArrayOrPromise = AppPropertiesUtils.arrayFromObject(data, opts?.definitions);
      if (Array.isArray(dataArrayOrPromise)) {
        if (isNotEmptyArray(dataArrayOrPromise)) fromArray.patchValue(dataArrayOrPromise);
      } else {
        // If promise: wait and set after
        dataArrayOrPromise
          .catch((err) => {
            console.error('[property-validator] Error during conversion from map to array conversion: ' + (err?.message || err), data, err);
            return null;
          })
          .then((values) => {
            if (isNotEmptyArray(values)) fromArray.patchValue(values);
            fromArray.updateValueAndValidity();
          });

        // Wait value to be set
        fromArray.markAsPending();
      }
    }
    return fromArray;
  }

  getFormGroup(data?: Property, opts?: PropertyValidatorOptions): UntypedFormGroup {
    return this.formBuilder.group(this.getFormGroupConfig(data, opts));
  }

  getFormGroupConfig(data?: Property, opts?: PropertyValidatorOptions): { [p: string]: any } {
    return {
      key: [data?.key ?? null, Validators.compose([Validators.required, Validators.max(50)])],
      value: [
        data?.value ?? null,
        opts?.valueMaxLength > 0 ? Validators.compose([Validators.required, Validators.max(opts.valueMaxLength)]) : Validators.required,
      ],
    };
  }

  equals(v1: Property, v2: Property): boolean {
    return (!v1 && !v2) || v1.key === v2.key;
  }

  isEmpty(obj: Property): boolean {
    return isNil(obj?.key) || isNil(obj.value);
  }
}
