import { AppInMemoryTable } from '../../table/memory-table.class';
import { ChangeDetectionStrategy, Component, Directive, Inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '../../table/table.model';
import { InMemoryEntitiesService } from '../../../shared/services/memory-entity-service.class';
import { Entity, EntityAsObjectOptions, EntityUtils } from '../../services/model/entity.model';
import { AppValidatorService } from '../../services/validator/base.validator.class';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { FormFieldDefinition, FormFieldDefinitionMap } from '../../../shared/form/field.model';
import { isNotEmptyArray, isNotNil } from '../../../shared/functions';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';
import { TableElement } from '@e-is/ngx-material-table';
import { EntityClass } from '../../services/model/entity.decorators';
import { EntityFilter } from '../../services/model/filter.model';
import { FilterFn, ObjectMap } from '../../../shared/types';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { AppFormUtils } from '../form.utils';
import { MatExpansionPanel } from '@angular/material/expansion';

interface TranslatedFormFieldDefinition extends FormFieldDefinition {
  translatedName?: string;
}

@EntityClass({ typename: 'PropertyEntity' })
export class PropertyEntity extends Entity<PropertyEntity, string> {
  static fromObject: (source: any, opts?: any) => PropertyEntity;

  definition: TranslatedFormFieldDefinition = null;
  value: any = null;

  fromObject(source: any) {
    super.fromObject(source);
    this.id = source.definition?.key || source.id;
    this.definition = source.definition;
    this.value = source.value;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.id = this.definition.key;
    delete target.definition;
    return target;
  }
}

export class PropertyEntityFilter extends EntityFilter<PropertyEntityFilter, PropertyEntity> {
  static fromObject: (source, opts?: any) => PropertyEntityFilter;

  searchText: string;
  notDefaultOnly = false;

  fromObject(source: any) {
    super.fromObject(source);
    this.searchText = source.searchText;
    this.notDefaultOnly = source.notDefaultOnly;
  }

  asObject(opts?: EntityAsObjectOptions): any {
    return super.asObject(opts);
  }

  protected buildFilter(): FilterFn<PropertyEntity>[] {
    const filterFns = super.buildFilter();

    const searchTextFilter = EntityUtils.searchTextFilter('definition.translatedName', this.searchText);
    if (searchTextFilter) filterFns.push(searchTextFilter);

    if (this.notDefaultOnly) {
      filterFns.push((data) => isNotNil(data.value) && data.definition.defaultValue !== data.value);
    }

    return filterFns;
  }

  protected isCriteriaNotEmpty(key: string, value: any): boolean {
    if (key === 'notDefaultOnly') return value === true;
    return super.isCriteriaNotEmpty(key, value);
  }
}

@Directive()
export class PropertyEntityValidator extends AppValidatorService<PropertyEntity> {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder, null);
  }

  getFormGroupConfig(data?: PropertyEntity): { [p: string]: any } {
    return {
      id: [data?.id || null],
      definition: [data?.definition || null, Validators.required],
      value: [data?.value || null],
    };
  }
}

@Component({
  selector: 'app-properties-table',
  templateUrl: 'properties.table.html',
  styleUrls: ['properties.table.scss'],
  providers: [{ provide: PropertyEntityValidator, useClass: PropertyEntityValidator }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppPropertiesTable extends AppInMemoryTable<PropertyEntity, PropertyEntityFilter, string> implements OnInit {
  @Input() definitions: FormFieldDefinition[];
  @Input() showToolbar = true;
  @ViewChild(MatExpansionPanel, { static: true }) filterExpansionPanel: MatExpansionPanel;

  // availableDefinitions$ = new BehaviorSubject<TranslatedFormFieldDefinition[]>([]);
  definitionsMapByKey: FormFieldDefinitionMap = {};
  translationsByKey: ObjectMap<string> = {};
  filterForm: UntypedFormGroup;
  filterCriteriaCount = 0;
  filterPanelFloating = true;

  constructor(
    protected injector: Injector,
    protected formBuilder: UntypedFormBuilder,
    protected validatorService: PropertyEntityValidator,
    @Inject(ENVIRONMENT) environment: Environment
  ) {
    super(
      injector,
      [...RESERVED_START_COLUMNS, 'definition', 'value', ...RESERVED_END_COLUMNS],
      PropertyEntity,
      new InMemoryEntitiesService(PropertyEntity, PropertyEntityFilter, {
        sortByReplacement: { definition: 'definition.translatedName' },
      }),
      validatorService
    );

    this.inlineEdition = true;
    this.defaultSortBy = 'definition';
    this.defaultSortDirection = 'asc';
    this.debug = !environment?.production;

    this.filterForm = formBuilder.group({
      searchText: [],
      notDefaultOnly: [null],
    });

    this.autoLoad = false;
  }

  ngOnInit() {
    super.ngOnInit();

    // Fill options map
    this.definitionsMapByKey = {};
    this.definitions?.forEach((o) => {
      this.definitionsMapByKey[o.key] = o;
    });

    this.registerCellValueChanges('definition').subscribe((definition) => {
      if (isNotNil(definition) && this.editedRow?.id === -1) {
        this.editedRow.validator.patchValue({ id: definition.key, value: definition.defaultValue });
      }
    });

    this.registerCellValueChanges('value').subscribe(() => {
      this.markAsDirty();
    });

    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter((_) => {
            const valid = this.filterForm.valid;
            if (!valid && this.debug) AppFormUtils.logFormErrors(this.filterForm);
            return valid;
          }),
          // Update the filter
          tap((json) => this.setFilter(json))
        )
        .subscribe()
    );

    this.resetFilter();

    this.markAsReady();
  }

  get value(): PropertyEntity[] {
    let values = super.value || [];

    // remove unset options
    values = values.filter((property) => isNotNil(property.value !== property.definition.defaultValue));

    return values;
  }

  set value(data: PropertyEntity[]) {
    super.value = data;
  }

  definitionByRow = (row: TableElement<PropertyEntity>): FormFieldDefinition => this.definitionsMapByKey[row.currentData.id];

  setValue(value: PropertyEntity[], opts?: { emitEvent?: boolean }) {
    // Update translations
    this.translationsByKey = isNotEmptyArray(this.definitions) ? this.translate.instant(this.definitions.map((def) => def.label)) : {};

    // Build entities
    const source = (value || []).slice();
    const target: PropertyEntity[] = [];
    target.push(
      ...this.definitions.map((definition) =>
        PropertyEntity.fromObject({
          id: definition.key,
          // translate definition label
          definition: this.toTranslatedFormFieldDefinition(definition),
          // get user value or default value
          value: this.toValue(source, definition),
        })
      )
    );

    super.setValue(target, opts);
  }

  clearControlValue(event: Event, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  resetFilter(value?: any, opts?: { emitEvent: boolean }) {
    const f = <PropertyEntityFilter>{ searchText: null, notDefaultOnly: false, ...value };
    this.filterForm.reset(f, opts);
    this.setFilter(f || null, opts);
    this.filterCriteriaCount = 0;
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
    this.resetError();
  }

  setFilter(filter: PropertyEntityFilter, opts?: { emitEvent?: boolean }) {
    filter = this.dataSource.dataService.asFilter(filter);

    // Update criteria count
    const criteriaCount = filter.countNotEmptyCriteria();
    if (criteriaCount !== this.filterCriteriaCount) {
      this.filterCriteriaCount = criteriaCount;
      this.markForCheck();
    }

    // Update the form content
    if (this.filterForm && (!opts || opts.emitEvent !== false)) {
      this.filterForm.patchValue(filter.asObject(), { emitEvent: false });
    }

    super.setFilter(filter as PropertyEntityFilter, opts);
  }

  get filterIsEmpty(): boolean {
    return this.filterCriteriaCount === 0;
  }

  toggleFilterPanelFloating() {
    this.filterPanelFloating = !this.filterPanelFloating;
    this.markForCheck();
  }

  closeFilterPanel() {
    if (this.filterExpansionPanel) this.filterExpansionPanel.close();
    this.filterPanelFloating = true;
  }

  applyFilterAndClosePanel(event?: Event) {
    const filter = this.filterForm.value;
    this.setFilter(filter, { emitEvent: false });
    this.emitRefresh(event);
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  toTranslatedFormFieldDefinition = (definition: FormFieldDefinition): TranslatedFormFieldDefinition => ({
    ...definition,
    translatedName: this.translationsByKey[definition.label],
  });

  resetProperty(event: UIEvent, row: TableElement<PropertyEntity>) {
    event?.preventDefault();
    row.validator.patchValue({ value: row.currentData.definition.defaultValue });
    row.confirmEditCreate();
    this.markRowAsDirty();
  }

  protected toValue(values: PropertyEntity[], definition: FormFieldDefinition): any {
    const value = values.find((v) => v.id === definition.key)?.value;
    return isNotNil(value) ? value : definition.defaultValue;
  }
}
