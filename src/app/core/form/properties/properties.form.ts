import { booleanAttribute, ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Injector, Input, OnInit, Optional } from '@angular/core';
import { FormGroupDirective, UntypedFormArray, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { FormFieldDefinition, FormFieldDefinitionMap, FormFieldDefinitionUtils } from '../../../shared/form/field.model';
import { isEmptyArray, isNil, isNotNil } from '../../../shared/functions';
import { DateAdapter, ThemePalette } from '@angular/material/core';
import { Moment } from 'moment';
import { AppForm } from '../form.class';
import { Property, PropertyMap } from '../../../shared/types';
import { PropertyValidator } from './property.validator';
import { PredefinedColors } from '@ionic/core';
import { FormArrayHelper, FormArrayHelperOptions } from '../array/form-array';
import { isMobile } from '../../../shared/platforms';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';
import { MatAutocompleteFieldConfig } from '../../../shared/material/autocomplete/material.autocomplete.config';
import { suggestFromArray } from '../../../shared/services';
import { LoadResult } from '../../../shared/services/entity-service.class';
import { AppPropertiesUtils, AppPropertyUtils } from './properties.utils';
import { RxState } from '@rx-angular/state';
import { firstNotNilPromise } from '../../../shared/observables';
import { SortDirection } from '@angular/material/sort';
import { RegExpUtils } from '../../../shared/regexps';

export interface AppPropertiesFormState {
  definitions: FormFieldDefinition[];
  definitionKeys: Property[];
}

@Component({
  selector: 'app-properties-form',
  templateUrl: 'properties.form.html',
  styleUrls: ['properties.form.scss'],
  providers: [{ provide: PropertyValidator, useClass: PropertyValidator }, RxState],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppPropertiesForm<O = any, T extends Property<O> = Property<O>, S extends AppPropertiesFormState = AppPropertiesFormState>
  extends AppForm<T[]>
  implements OnInit
{
  protected readonly _state: RxState<S> = inject(RxState);

  protected _definitions$ = this._state.select('definitions');
  protected _definitionKeys$ = this._state.select('definitionKeys');

  protected set _definitionKeys(value: Property[]) {
    this._state.set('definitionKeys', () => value);
  }
  protected get _definitionKeys(): Property[] {
    return this._state.get('definitionKeys');
  }

  protected _definitionByKey: FormFieldDefinitionMap = {};
  protected _definitionsByIndex: { [index: number]: FormFieldDefinition } = {};
  protected _helper: FormArrayHelper<Property>;

  protected _autocompleteConfig: MatAutocompleteFieldConfig;
  protected _focusedControlIndex: number;

  @Input() formArrayName: string;
  @Input() formArray: UntypedFormArray;
  @Input() options: FormArrayHelperOptions;
  @Input() chipColor: ThemePalette | PredefinedColors = 'accent';
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input({ transform: booleanAttribute }) showHintKey = false;
  @Input() hintKeyPrefix = '';
  @Input({ transform: booleanAttribute }) showMoreButton = true;
  @Input() addButtonText = 'COMMON.BTN_ADD';
  @Input() addButtonTitle = '';
  @Input({ transform: booleanAttribute }) showAddButton = true;
  @Input() panelClass: string;
  @Input() panelWidth: string;

  /**
   * @deprecated Use addButtonTitle instead
   * @param value
   */
  @Input() set showMoreButtonTitle(value: string) {
    this.addButtonTitle = value;
  }
  get showMoreButtonTitle(): string {
    return this.addButtonTitle;
  }

  @Input() set definitions(value: FormFieldDefinition[]) {
    this._state.set('definitions', () => value);
  }
  get definitions(): FormFieldDefinition[] {
    return this._state.get('definitions');
  }

  set value(data: T[]) {
    this.setValue(data);
  }

  get value(): T[] {
    return this.formArray.value as T[];
  }

  get length(): number {
    return this.formArray?.length || 0;
  }

  get fieldForms(): UntypedFormGroup[] {
    return this.formArray.controls as UntypedFormGroup[];
  }

  constructor(
    protected injector: Injector,
    protected formBuilder: UntypedFormBuilder,
    protected dateAdapter: DateAdapter<Moment>,
    protected cd: ChangeDetectorRef,
    protected validator: PropertyValidator,
    @Optional() private formGroupDir: FormGroupDirective
  ) {
    super(injector, null);

    this.tabindex = 20; // For compatibility - TODO remove later
    //this.debug = !environment.production;
  }

  ngOnInit() {
    if (this.options?.allowEmptyArray === false && isEmptyArray(this.definitions)) {
      throw new Error("Missing or invalid attribute 'definitions'");
    }

    // Retrieve the form
    const form =
      (this.formArray && (this.formArray.parent as UntypedFormGroup)) || (this.formGroupDir && this.formGroupDir.form) || this.formBuilder.group({});
    this.setForm(form);

    this.formArray = this.formArray || (this.formArrayName && (form.get(this.formArrayName) as UntypedFormArray));
    this.formArrayName =
      this.formArrayName || (this.formArray && Object.keys(form.controls).find((key) => form.get(key) === this.formArray)) || 'properties';
    if (!this.formArray) {
      console.warn(`Missing array control '${this.formArrayName}'. Will create it!`);
      this.formArray = this.formBuilder.array([]);
      this.form.addControl(this.formArrayName, this.formArray);
    }

    this._helper = new FormArrayHelper<Property>(
      this.formArray,
      (value) => this.validator.getFormGroup(value),
      (v1, v2) => (!v1 && !v2) || v1.key === v2.key,
      (value) => this.isEmptyProperty(value),
      this.options
    );

    super.ngOnInit();

    // Default values
    this.mobile = this.mobile ?? isMobile(window);

    // Combo key
    this._autocompleteConfig = {
      suggestFn: (value, filter, sortBy, sortDirection, opts) => this.suggestDefinitionKeys(value, filter, 'value', sortDirection, opts),
      equals: (v1, v2) => (v1?.key || v1) === (v2?.key || v2),
      displayWith: (value: any) => {
        // DEBUG
        if (this.debug) console.debug('[app-properties-form] Displaying value', value);

        // Value is an object (a Property)
        if (value && typeof value === 'object') {
          return (value as Property).value;
        }

        // Value is a key
        const def = typeof value === 'string' && this._definitionByKey[value];
        if (!def) return value;
        return this.translate.instant(def.label);
      },
      attributes: ['value'],
      columnNames: [this.translate.instant('SETTINGS.PROPERTY_KEY')],
      splitSearchText: false,
      showAllOnFocus: true,
      selectInputContentOnFocus: true,
    };

    this._state.hold(this._state.select('definitions'), (definitions) => {
      if (this.loaded) this.processDefinitions(definitions);
    });

    this.processDefinitions(this.definitions);
  }

  protected processDefinitions(definitions: FormFieldDefinition[]) {
    if (!definitions) return [];

    // DEBUG
    if (this.debug) console.debug('[app-properties-form] Processing definitions...', definitions);

    // Prepare definition (e.g. resolve tokens if any)
    definitions = FormFieldDefinitionUtils.prepareDefinitions(this.injector, definitions || []);

    // Fill options map
    const definitionsByKey = <FormFieldDefinitionMap>{};
    this._definitionKeys = definitions
      .map((def) => {
        definitionsByKey[def.key] = def;
        if (def.disabled) return null;
        return { key: def.key, value: this.translate.instant(def.label) };
      })
      .filter(isNotNil);
    this._definitionByKey = definitionsByKey;
  }

  getDefinitionAt(index: number): FormFieldDefinition {
    let definition = this._definitionsByIndex[index];
    if (!definition) {
      definition = this.updateDefinitionAt(index, { emitEvent: false });
    }
    return definition;
  }

  updateDefinitionAt(index: number, opts?: { emitEvent?: boolean }): FormFieldDefinition {
    let value = (this.formArray.at(index) as UntypedFormGroup).controls.key.value;
    const key = value?.key || value;
    const definition = (key && this._definitionByKey[key]) || null;
    this._definitionsByIndex[index] = definition; // update map by index
    if (opts?.emitEvent !== false) {
      this.markForCheck();
    }
    return definition;
  }

  removeAt(index: number) {
    this._helper.removeAt(index);
    this.clearTemplateCache();
    this.markForCheck();
  }

  async setValue(data: PropertyMap | T[]) {
    if (!data) return; // Skip

    const [definitions, definitionKeys] = await Promise.all([
      firstNotNilPromise(this._definitions$, { stop: this.destroySubject }),
      firstNotNilPromise(this._definitionKeys$, { stop: this.destroySubject }),
    ]);

    // Transform properties map into array
    const values = await AppPropertiesUtils.arrayFromObject(data, definitions);

    // Workaround because when autocomplete use mat-select (when mobile), it needs to display the value - fix issue ngx-sumaris-components#16
    if (this.mobile) {
      values.forEach((property) => {
        property.key = (definitionKeys.find((defKey) => defKey.key === property.key) || <Property>{ key: property.key, value: property.key }) as any;
      });
    }

    this.clearTemplateCache();
    this._helper.resize(values.length);
    this._helper.formArray.patchValue(values, { emitEvent: false });

    this.markAsPristine();
    this.markAsLoaded();
  }

  getValueAsJson(): PropertyMap {
    const data = this.value;
    const json = AppPropertiesUtils.arrayAsObject(data, this.definitions, {
      omitNilOrBlank: this.options?.allowDuplicatedValues !== true,
    });
    console.debug('[app-properties-form] Converting data as object', data, json);
    return json;
  }

  clear() {
    if (this._helper.formArray.length) {
      this._helper.resize(0);
    }
    this.clearTemplateCache();
    this.markForCheck();
  }

  /* -- protected methods -- */

  protected clearTemplateCache() {
    this._definitionsByIndex = {}; // clear map by index
  }

  protected async suggestDefinitionKeys(
    value: any,
    filter?: any,
    sortBy?: string | keyof Property,
    sortDirection?: SortDirection,
    opts?: {
      offset?: number;
      size?: number;
      [key: string]: any;
    }
  ): Promise<LoadResult<Property>> {
    let items = await firstNotNilPromise(this._definitionKeys$, { stop: this.destroySubject });

    // DEBUG
    if (this.debug) console.debug('[app-properties-form] Suggesting definition keys', value, items);

    // Get as key
    if (typeof value === 'string' && !RegExpUtils.hasWildcardCharacter(value)) {
      const obj = items.find((o) => o.key === value);
      if (obj) return { data: [obj] } as LoadResult<Property>;
    }

    // Get as object
    if (value && typeof value === 'object') {
      return { data: [value] } as LoadResult<Property>;
    }

    // Remove already used keys
    if (this.options?.allowDuplicatedValues !== true) {
      const existingKeys = (this.formArray.value || [])
        .filter((_, index) => index !== this._focusedControlIndex)
        .map(AppPropertyUtils.getPropertyKey);
      items = items.filter((item) => !existingKeys.includes(item.key));
    }

    value = value ?? '*';

    // Any match
    if (!value.startsWith('*')) value = '*' + value;

    return suggestFromArray(items, value, { searchAttribute: 'value', ...filter }, sortBy, sortDirection, opts);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected isEmptyProperty(obj: Property): boolean {
    return isNil(obj?.key) || isNil(obj.value);
  }
}
