import { Property, PropertyMap } from '../../../shared/types';
import { getPropertyByPath, isNil, isNilOrBlank, isNotNil, isNotNilOrBlank, isPromise, toBoolean, toNumber } from '../../../shared/functions';
import { FormFieldDefinition } from '../../../shared/form/field.model';
import { EntitiesServiceLoadOptions, SuggestFn } from '../../../shared/services/entity-service.class';

export class AppPropertiesUtils {
  /**
   * Converts an array of `Property` objects into a `PropertiesMap` object.
   *
   * @param {Property[]} [sources] - An optional array of properties to be converted.
   * @param {FormFieldDefinition[]} [definitions] - An optional array of form field definitions to help with the conversion.
   * @param {Object} [opts] - Optional configurations.
   * @param {boolean} [opts.omitNilOrBlank] - If true, properties with nil or blank values will be omitted. Defaults to true.
   * @return {PropertyMap} - The resulting `PropertiesMap` object.
   */
  static arrayAsObject<T = any>(sources?: Property<T>[], definitions?: FormFieldDefinition[], opts?: { omitNilOrBlank: boolean }): PropertyMap {
    return (
      (sources || [])
        // Serialize each item
        .map((source) => {
          if (!source) return undefined;
          const key = AppPropertyUtils.getPropertyKey(source);
          if (!key) return undefined;
          const def = definitions?.find((d) => d.key === key);
          return AppPropertyUtils.asObject(source, def);
        })
        // Fill a JSON object
        .reduce(
          (res, item) => {
            if (isNilOrBlank(item?.key)) return res;
            // Skip if nil or blank value
            if (opts?.omitNilOrBlank !== false && isNilOrBlank(item.value)) return res;
            res[item.key] = item.value;
            return res;
          },
          <PropertyMap>{}
        )
    );
  }

  /**
   * Generates an array of properties from the given object, optionally applying definitions to the properties.
   *
   * @param {PropertyMap} [sources] - The source object from which properties are to be extracted.
   * @param {FormFieldDefinition[]} [definitions] - An optional array of definitions to be applied to the extracted properties.
   * @param opts
   * @return {Property[] | Promise<Property[]>} An array of properties generated from the source object, or a promise resolving to that array.
   */
  static arrayFromObject<T>(
    sources?: PropertyMap | Property<T>[],
    definitions?: FormFieldDefinition[],
    opts?: {
      keepOrphan?: boolean; // true by default
    }
  ): Property<T>[] | Promise<Property<T>[]> {
    // DEBUG
    //console.debug(`[properties-utils] Convert property map into array of Property...`);

    let targets = Array.isArray(sources)
      ? sources.slice()
      : Object.getOwnPropertyNames(sources || {}).map(
          (key) =>
            <Property<any>>{
              key,
              value: sources[key],
            }
        );
    const jobs = [];

    if (!!definitions) {
      targets = targets
        .map((target: Property<any>) => {
          // Find definition
          const def = definitions.find((def) => def.key === target.key);

          // Exclude no definition (if expected by user options)
          if (!def && opts?.keepOrphan === false) return null;

          // Deserialize from string
          let propertyOrPromise = AppPropertyUtils.fromObject(target, def);
          if (isPromise(propertyOrPromise)) {
            jobs.push(propertyOrPromise.then((property) => (target.value = property.value)));
          } else {
            // Update the target value
            target.value = propertyOrPromise.value;
          }

          return target;
        })
        // Clean orphan
        .filter(isNotNil);

      // Return as promise, when need
      if (jobs.length) return Promise.all(jobs).then(() => targets as Property<T>[]);
    }

    return targets as Property<T>[];
  }
}

export class AppPropertyUtils {
  static key(key: any): string {
    return key?.key ?? key;
  }

  static getPropertyKey(source: Property | any): string {
    if (!source) return undefined;
    return (source.key as any)?.key ?? source.key;
  }

  /**
   * Converts the given source to an object that contains a key-value pair.
   *
   * @param {Property<any> | any} source - The source property or any value to be converted.
   * @param {FormFieldDefinition} [def] - The form field definition used for serialization.
   * @param {Object} [opts] - Additional options for serialization.
   * @param {string} [opts.defaultSerializeAttribute='id'] - The attribute to be used for default serialization.
   * @return {{ key: string, value: string } | undefined} An object containing the serialized key-value pair, or undefined if the source is not provided.
   */
  static asObject(
    source: Property<any> | any,
    def?: FormFieldDefinition,
    opts?: {
      defaultSerializeAttribute?: string;
    }
  ): { key: string; value: string } | undefined {
    if (!source) return undefined;
    // Serialize key (WARNING: can be an object, when using autocomplete field to edit a key - see AppPropertiesForm)
    const key = AppPropertyUtils.getPropertyKey(source);

    // Prepare a deserialize function
    let serializeFn = def?.serialize;
    if (!serializeFn) {
      switch (def?.type || 'other') {
        case 'enum':
        case 'enums':
          serializeFn = AppPropertyUtils.key;
          break;
        case 'entity':
        case 'entities': {
          const serializeAttribute = def?.serializeAttribute ?? opts?.defaultSerializeAttribute ?? 'id';
          serializeFn = (value) => {
            const serializeValue = getPropertyByPath(value, serializeAttribute, value /*default value*/);
            if (isNil(serializeValue)) return serializeValue;
            return serializeValue.toString();
          };
          break;
        }
        default:
          serializeFn = AppPropertyUtils.toString;
      }
    }

    let valueStr: string;

    // Value is an array
    if (Array.isArray(source.value)) {
      valueStr = source.value.map(serializeFn).filter(isNotNilOrBlank).join(',');
    }
    // Any other case
    else {
      valueStr = serializeFn(source.value);
    }
    return { key, value: valueStr };
  }

  static fromObject<T = string>(
    source: Property | Property<T>,
    def: FormFieldDefinition,
    opts?: {
      defaultSuggestFn: SuggestFn<any, any>;
      defaultSuggestOptions?: EntitiesServiceLoadOptions;
      defaultSerializeAttribute?: string;
    }
  ): Property<T> | Promise<Property<T>> {
    if (!source?.key || isNil(source.value) || !def) return source as Property<T>; // Skip if no key

    // Clone the source
    const target: Property<any> = { key: source.key, value: source.value };

    const jobs: Promise<any>[] = [];
    // Resolve value
    switch (def.type) {
      case 'boolean':
        target.value = toBoolean(source.value as any, toBoolean(def.defaultValue));
        break;
      case 'integer':
      case 'double':
        target.value = toNumber(source.value as any, toNumber(def.defaultValue));
        break;
      case 'enum':
        const valueKey = AppPropertyUtils.key(target.value);
        if (isNotNilOrBlank(valueKey)) {
          const enumValue = (def.values as (string | Property)[])?.find((defValue) => defValue && valueKey === (defValue['key'] || defValue)) ?? null;
          target.value = enumValue?.['key'] ?? enumValue ?? null;
        } else {
          target.value = null;
        }
        break;
      case 'enums':
        const valueKeys = Array.isArray(source.value)
          ? source.value.map(AppPropertyUtils.key) // Copy
          : typeof source.value === 'string'
            ? source.value.trim().split(/,+/)
            : [AppPropertyUtils.key(source.value)];
        target.value = valueKeys
          .filter(isNotNil)
          .map(
            (key: string) =>
              (def.values as (string | Property)[])?.find((defValue) => defValue && key === (defValue['key'] || defValue)) || { key, value: '?' }
          );
        break;
      case 'entity':
        // Deserialize entity (e.g. load entity from the id)
        const valueOrPromise = AppPropertyUtils.resolveEntity(target.value, def, opts);
        if (isPromise(valueOrPromise)) {
          jobs.push(
            valueOrPromise.then((value) => {
              target.value = value;
            })
          );
        } else {
          target.value = valueOrPromise as any;
        }
        break;
      case 'entities':
        // Convert value to array
        target.value = Array.isArray(source.value)
          ? source.value.slice() // Copy
          : typeof source.value === 'string'
            ? source.value
                .split(',')
                .map((v) => v.trim())
                .filter(isNotNilOrBlank)
            : [];

        // Deserialize each sub-value
        let index = 0;
        for (let subValue of target.value) {
          // Deserialize sub-value
          const subValueOrPromise = AppPropertyUtils.resolveEntity(subValue, def, opts);

          // Add promise to jobs
          if (isPromise(subValueOrPromise)) {
            const finalIndex = index;
            jobs.push(
              subValueOrPromise.then((value) => {
                // Update sub value
                target.value[finalIndex] = value;
              })
            );
          } else {
            // Update sub value
            target.value[index] = subValueOrPromise as any;
          }
          index++;
        }
    }

    // /!\ Return as promise only if need (let function sync when possible)
    if (jobs.length) return Promise.all(jobs).then(() => target);

    return target;
  }

  private static resolveEntity(
    value: any,
    def: FormFieldDefinition,
    opts?: {
      defaultSuggestFn: SuggestFn<any, any>;
      defaultSuggestOptions?: EntitiesServiceLoadOptions;
      defaultSerializeAttribute?: string;
    }
  ): any | Promise<any> {
    // Use deserialize function, if any
    if (def.deserialize) {
      return def.deserialize(value);
    }

    // Or try using suggest function
    return this.resolveEntityBySuggest(value, def, opts);
  }

  private static async resolveEntityBySuggest(
    value: any,
    def: FormFieldDefinition,
    opts?: {
      defaultSuggestFn: SuggestFn<any, any>;
      defaultSuggestOptions?: EntitiesServiceLoadOptions;
      defaultSerializeAttribute?: string;
    }
  ): Promise<any> {
    const serializeAttribute = def.serializeAttribute ?? opts?.defaultSerializeAttribute ?? 'id';
    if (typeof value === 'object' && isNotNil(value[serializeAttribute])) {
      return value; // Already resolve
    }

    const suggestFn: SuggestFn<any, any> = def.autocomplete?.suggestFn || opts?.defaultSuggestFn;
    if (!suggestFn) {
      console.warn(
        `Missing 'deserialize' or 'autocomplete.suggestFn' in the field definition '${def.key}'. Cannot deserialize value: ${JSON.stringify(value)}`
      );
      return value; // Skip
    }
    let searchValue = value;
    const filter = { ...def.autocomplete?.filter }; // Cope filter
    if (serializeAttribute === 'id' || serializeAttribute === 'label') {
      filter[serializeAttribute] = serializeAttribute === 'id' ? +value : value;
      searchValue = '*';
      // DEBUG
      //console.debug(`[properties-utils] Resolving entity where id=${value}...`);
    } else {
      filter.searchAttribute = serializeAttribute;
      // DEBUG
      //console.debug(`[properties-utils] Resolving entity where ${serializeAttribute}=${searchValue}...`);
    }

    try {
      // Fetch entity, using the suggest function
      const res = await suggestFn(searchValue, filter, serializeAttribute, 'asc', {
        withTotal: false,
        offset: 0,
        size: 1,
        ...opts?.defaultSuggestOptions,
      });

      // Get result array
      const data = Array.isArray(res) ? res : res.data;

      if (data?.length > 0) {
        if (data?.length > 1) {
          console.warn(
            `[properties-utils] More than one entity found, for option ${def.key}=${value}! Please check option's definition and the 'autocomplete.suggestFn' function`,
            data
          );
        }

        // DEBUG
        //console.debug(`[properties-utils] ${data.length} entities found: `, data);

        // Ok, we find it => stop here
        return data[0];
      }

      // Warn, but continue
      console.warn(
        `[properties-utils] Unable to resolve entity for option: ${def.key}=${value}. Please check option's definition and the 'autocomplete.suggestFn' function`
      );
    } catch (err) {
      console.error(`[properties-utils] Cannot resolve entity for option: ${def.key}=${value}`, err);
      // Continue
    }

    // Create the missing result
    const unresolvedEntity = <any>{
      [serializeAttribute]: value,
    };

    // Fill attributes with '??'
    def.autocomplete?.attributes?.forEach((key) => {
      unresolvedEntity[key] = unresolvedEntity[key] ?? '';
    });

    return unresolvedEntity;
  }

  private static toString(value?: any): string {
    return value?.toString();
  }
}
