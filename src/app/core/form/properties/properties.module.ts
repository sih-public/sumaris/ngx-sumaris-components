import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { AppPropertiesForm } from './properties.form';
import { TranslateModule } from '@ngx-translate/core';
import { AppPropertiesTable } from './properties.table';
import { AppTableModule } from '../../table/table.module';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild(), AppTableModule],

  declarations: [
    // Other components
    AppPropertiesForm,
    AppPropertiesTable,
  ],
  exports: [
    TranslateModule,

    // Components
    AppPropertiesForm,
    AppPropertiesTable,
  ],
})
export class AppPropertiesFormModule {}
