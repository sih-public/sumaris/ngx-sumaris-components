import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppUsernameForm } from './username.form';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],

  declarations: [AppUsernameForm],
  exports: [TranslateModule, AppUsernameForm],
})
export class AppUsernameFormModule {}
