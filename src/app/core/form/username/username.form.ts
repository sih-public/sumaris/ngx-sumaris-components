import { booleanAttribute, ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { AppForm } from '../form.class';
import { isMobile } from '../../../shared/platforms';

@Component({
  selector: 'app-username-form',
  templateUrl: 'username.form.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppUsernameForm extends AppForm<{ username: string }> {
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input() usernamePlaceHolder: string = 'USER.EMAIL';
  @Input({ transform: booleanAttribute }) emailValidator = true;

  constructor(
    injector: Injector,
    formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    super(
      injector,
      formBuilder.group({
        username: new UntypedFormControl(null, Validators.required),
      })
    );
  }

  ngOnInit() {
    super.ngOnInit();
    this.mobile = this.mobile ?? isMobile(window);

    const userNameControl = this.form.get('username');
    if (!this.emailValidator && userNameControl.hasValidator(Validators.email)) {
      userNameControl.removeValidators(Validators.email);
    } else if (this.emailValidator && !userNameControl.hasValidator(Validators.email)) {
      userNameControl.addValidators(Validators.email);
    }
  }

  markForCheck() {
    this.cd.markForCheck();
  }
}
