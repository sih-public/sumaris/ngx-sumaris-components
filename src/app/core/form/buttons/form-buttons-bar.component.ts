import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Hotkeys } from '../../../shared/hotkeys/hotkeys.service';
import { Subscription } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { MenuService } from '../../menu/menu.service';
import { PredefinedColors } from '@ionic/core';

export abstract class FormButtonsBarToken {
  abstract onCancel: EventEmitter<Event>;
  abstract onSave: EventEmitter<Event>;
  abstract onNext: EventEmitter<Event>;
  abstract onBack: EventEmitter<Event>;
}

@Component({
  selector: 'app-form-buttons-bar',
  templateUrl: './form-buttons-bar.component.html',
  styleUrls: ['./form-buttons-bar.component.scss'],
  providers: [{ provide: FormButtonsBarToken, useExisting: FormButtonsBarComponent }],
})
export class FormButtonsBarComponent implements FormButtonsBarToken, OnDestroy {
  private _subscription = new Subscription();

  @Input() disabled = false;
  @Input() disabledCancel = false;
  @Input() disabledEscape = false;
  @Input() classList: string;

  @Input() saveButtonColor: PredefinedColors = 'danger';

  @Input() backText = 'COMMON.BTN_CLOSE';
  @Input() cancelText = 'COMMON.BTN_RESET';
  @Input() nextText = 'COMMON.BTN_NEXT';

  @Input() showBack = true;
  @Input() showCancel = true;
  @Input() showNext = true;
  @Input() showSave = true;

  @Output() onCancel = new EventEmitter<Event>();
  @Output() onSave = new EventEmitter<Event>();
  @Output() onNext = new EventEmitter<Event>();
  @Output() onBack = new EventEmitter<Event>();
  @Output() onSaveAndClose = new EventEmitter<Event>();
  @Output() onSaveAndNext = new EventEmitter<Event>();

  constructor(
    hotkeys: Hotkeys,
    protected menu: MenuService
  ) {
    // Ctrl+S
    this._subscription.add(
      hotkeys
        .addShortcut({ keys: `${hotkeys.defaultControlKey}.s`, description: 'COMMON.BTN_SAVE' })
        .pipe(filter((_) => !this.disabled))
        .subscribe((event) => this.onSave.emit(event))
    );

    // Ctrl+Z
    this._subscription.add(
      hotkeys
        .addShortcut({ keys: `${hotkeys.defaultControlKey}.z`, description: 'COMMON.BTN_RESET' })
        .pipe(filter((_) => !this.disabled && !this.disabledCancel))
        .subscribe((event) => this.onCancel.emit(event))
    );

    // Escape
    this._subscription.add(
      hotkeys
        .addShortcut({ keys: 'escape', description: 'COMMON.BTN_CLOSE', preventDefault: false })
        .pipe(
          debounceTime(100), // Allow other components to manage the event
          filter((e) => this.showBack && !e.defaultPrevented && !this.disabledEscape)
          //tap(() => console.debug('[form-buttons-bar] Emit back event (keydown.escape)'))
        )
        .subscribe((event) => this.onBack.emit(event))
    );
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    this.onCancel.complete();
    this.onSave.complete();
    this.onNext.complete();
    this.onBack.complete();
    this.onSaveAndNext.complete();
    this.onSaveAndClose.complete();
  }
}
