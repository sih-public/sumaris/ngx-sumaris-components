import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { FormButtonsBarComponent } from './form-buttons-bar.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],

  declarations: [
    // Components
    FormButtonsBarComponent,
  ],
  exports: [
    TranslateModule,

    // Components
    FormButtonsBarComponent,
  ],
})
export class AppFormButtonsBarModule {}
