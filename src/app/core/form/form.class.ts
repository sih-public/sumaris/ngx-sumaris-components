import { booleanAttribute, Directive, EventEmitter, Injector, Input, numberAttribute, OnDestroy, OnInit, Output } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { AppFormUtils, IAppForm, OnReady } from './form.utils';
import { LocalSettingsService } from '../services/local-settings.service';
import { waitForFalse, WaitForOptions, waitForTrue } from '../../shared/observables';
import { FormErrorTranslateOptions, FormErrorTranslator, IFormPathTranslator } from '../../shared/validator/form-error-adapter.class';
import { changeCaseToUnderscore } from '../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import {
  MatAutocompleteConfigHolder,
  MatAutocompleteFieldAddOptions,
  MatAutocompleteFieldConfig,
} from '../../shared/material/autocomplete/material.autocomplete.config';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export class AppForm<T> implements IAppForm<T>, OnInit, OnDestroy, IFormPathTranslator {
  private _subscription = new Subscription();
  protected _form: UntypedFormGroup;
  protected _enabled = false;
  protected errorTranslator: FormErrorTranslator;
  protected i18nFieldPrefix: string = null;
  protected readonly translate: TranslateService;
  protected readonly settings: LocalSettingsService;
  protected readonly destroySubject = new Subject<void>();

  // TODO use RxState instead ?
  readonly readySubject = new BehaviorSubject<boolean>(false);
  readonly loadingSubject = new BehaviorSubject<boolean>(true);
  readonly errorSubject = new BehaviorSubject<string>(undefined);

  readonly autocompleteHelper: MatAutocompleteConfigHolder;
  readonly autocompleteFields: { [key: string]: MatAutocompleteFieldConfig };

  get error(): string {
    return this.errorSubject.value;
  }

  set error(error: string) {
    this.setError(error);
  }

  get loading(): boolean {
    return this.loadingSubject.value;
  }

  get loaded(): boolean {
    return !this.loadingSubject.value;
  }

  get value(): any {
    return this.form.value;
  }

  set value(data: any) {
    this.setValue(data);
  }

  get dirty(): boolean {
    return this.form?.dirty || false;
  }

  get invalid(): boolean {
    return !this.form || this.form.invalid;
  }

  get pending(): boolean {
    return !this.form || (this.form.dirty && this.form.pending);
  }

  get valid(): boolean {
    return this.form?.valid || false;
  }

  get empty(): boolean {
    return !this.form || (!this.form.dirty && !this.form.touched);
  }

  get touched(): boolean {
    return this.form?.touched || false;
  }

  get untouched(): boolean {
    return !this.form || this.form.untouched;
  }

  get enabled(): boolean {
    return this._enabled;
  }

  get disabled(): boolean {
    return !this._enabled;
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void {
    this.form?.disable(opts);
    if (this._enabled) {
      this._enabled = false;
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void {
    this.form?.enable(opts);
    if (!this._enabled) {
      this._enabled = true;
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  get destroyed(): boolean {
    return this.destroySubject?.closed !== false;
  }

  @Input({ transform: booleanAttribute }) debug = false;
  @Input({ transform: numberAttribute }) tabindex: number;
  @Input() errorTranslateOptions: FormErrorTranslateOptions;

  @Input()
  set form(value: UntypedFormGroup) {
    this.setForm(value);
  }

  get form(): UntypedFormGroup {
    return this._form;
  }

  @Output() onCancel = new EventEmitter<any>();
  @Output() onSubmit = new EventEmitter<any>();

  constructor(injector: Injector, form?: UntypedFormGroup) {
    if (form) this.setForm(form);
    this.translate = injector.get(TranslateService);
    this.settings = injector.get(LocalSettingsService);
    this.errorTranslator = injector.get(FormErrorTranslator);
    this.autocompleteHelper = new MatAutocompleteConfigHolder(
      this.settings && {
        getUserAttributes: (a, b) => this.settings.getFieldDisplayAttributes(a, b),
      }
    );
    this.autocompleteFields = this.autocompleteHelper.fields;
  }

  ngOnInit() {
    // Set defaults
    this.errorTranslateOptions = this.errorTranslateOptions || { separator: ', ', pathTranslator: this }; // Can be override in subclasses constructors

    // Enable/disable delegate form
    if (this._enabled) this.enable();
    else this.disable();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();

    this.readySubject.unsubscribe();
    this.loadingSubject.unsubscribe();
    this.errorSubject.unsubscribe();
    this.onCancel.unsubscribe();
    this.onSubmit.unsubscribe();

    this.destroySubject.next();
    this.destroySubject.unsubscribe();
  }

  async cancel() {
    this.onCancel.emit();
  }

  /**
   *
   * @param event
   * @param opts allow to skip validation check, using {checkValid: false}
   */
  async doSubmit(event: any, opts?: { checkValid?: boolean }) {
    if (!this._form) {
      this.markAllAsTouched({ emitEvent: true });
      return;
    }

    // Check if valid (if not disabled)
    if ((!opts || opts.checkValid !== false) && !this._form.valid) {
      // Wait validation end
      await AppFormUtils.waitWhilePending(this._form);

      // Form is invalid: exit (+ log if debug)
      if (this._form.invalid) {
        this.markAllAsTouched({ emitEvent: true });
        if (this.debug) AppFormUtils.logFormErrors(this._form);
        return;
      }
    }

    // Emit event
    this.onSubmit.emit(event);
  }

  setForm(form: UntypedFormGroup) {
    if (this._form !== form) {
      this._form = form;
      this._subscription.add(this._form.statusChanges.subscribe((_) => this.markForCheck()));
    }
  }

  getValue(): Promise<T> | T {
    return this.form.value as T;
  }

  setValue(data: T, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> | void {
    if (!data) {
      console.warn('[form] Trying to set an empty value to form. Skipping');
      return;
    }

    // DEBUG
    //if (this.debug) console.debug('[form] Updating form (using entity)', data);

    // Convert object to json, then apply it to form (e.g. convert 'undefined' into 'null')
    AppFormUtils.copyEntity2Form(data, this.form, { emitEvent: false, onlySelf: true, ...opts });

    if (!opts || opts.emitEvent !== true) {
      this.markForCheck();
    }
  }

  reset(data?: T, opts?: { emitEvent?: boolean; onlySelf?: boolean }) {
    if (data) {
      if (this.debug) console.debug('[form] Resetting form (using entity)', data);

      // Convert object to json, then apply it to form (e.g. convert 'undefined' into 'null')
      const json = AppFormUtils.getFormValueFromEntity(data, this.form);
      this.form.reset(json, { emitEvent: false, onlySelf: true });
    } else {
      this.form.reset(undefined, { emitEvent: false, onlySelf: true });
    }

    if (!opts || opts.emitEvent !== true) this.markForCheck();
  }

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.form.markAsPristine(opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.form.markAsUntouched(opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  /**
   * @deprecated prefer to use markAllAsTouched()
   * @param opts
   */
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    // this.form.markAllAsTouched() // This is not working well (e.g. in TripForm)
    if (this.debug) console.warn('TODO: Replace this call by markAllAsTouched() - because of changes in ngx-components >= 0.16.0');
    AppFormUtils.markAsTouched(this.form, opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  markAllAsTouched(opts?: { emitEvent?: boolean }) {
    // this.form.markAllAsTouched() // This is not working well (e.g. in TripForm)
    AppFormUtils.markAllAsTouched(this.form, opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this.form.markAsDirty(opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    this.setLoading(true, opts);
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    this.setLoading(false, opts);
  }

  markAsReady(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (!this.readySubject.closed && this.readySubject.value !== true) {
      this.readySubject.next(true);

      // If subclasses implements OnReady
      if (typeof this['ngOnReady'] === 'function') {
        (this as any as OnReady).ngOnReady();
      }
    }
  }

  /**
   * Wait form is ready
   *
   * @param opts
   */
  ready(opts?: WaitForOptions): Promise<void> {
    if (this.readySubject.value) return Promise.resolve();
    return waitForTrue(this.readySubject, { stop: this.destroySubject, ...opts });
  }

  /**
   * Wait form is not busy (=loaded)
   *
   * @param opts
   */
  waitIdle(opts?: WaitForOptions): Promise<void> {
    if (!this.loadingSubject.value) return Promise.resolve();
    return waitForFalse(this.loadingSubject, { stop: this.destroySubject, ...opts });
  }

  updateValueAndValidity(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    AppFormUtils.updateValueAndValidity(this.form, opts);
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  translateFormPath(path: string) {
    const i18nFieldName = this.getI18nFieldName(path);
    return this.translate?.instant(i18nFieldName) || i18nFieldName;
  }

  /* -- protected methods -- */

  protected getFormError(form: UntypedFormGroup, opts?: FormErrorTranslateOptions): string {
    if (!form || !this.errorTranslator) return undefined;
    return this.errorTranslator.translateFormErrors(this.form, {
      pathTranslator: this,
      ...opts,
    });
  }

  protected getI18nFieldName(path: string) {
    return (this.i18nFieldPrefix || '') + changeCaseToUnderscore(path).toUpperCase();
  }

  protected registerSubscription(sub: Subscription) {
    this._subscription.add(sub);
  }

  protected unregisterSubscription(sub: Subscription): void {
    this._subscription.remove(sub);
  }

  protected registerAutocompleteField<E = any, EF = any>(
    fieldName: string,
    opts?: MatAutocompleteFieldAddOptions<E, EF>
  ): MatAutocompleteFieldConfig<E, EF> {
    return this.autocompleteHelper.add(fieldName, opts);
  }

  protected setError(value: string, opts?: { emitEvent?: boolean }) {
    if (this.errorSubject.value !== value) {
      this.errorSubject.next(value);
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  protected markForCheck() {
    // Should be overwritten by subclasses
    console.warn('markForCheck() has been called, but is not overwritten by component ' + this.constructor.name);
  }

  /* -- private function -- */

  private setLoading(value: boolean, opts?: { emitEvent?: boolean }) {
    if (!this.loadingSubject.closed && this.loadingSubject.value !== value) {
      this.loadingSubject.next(value);

      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }
}
