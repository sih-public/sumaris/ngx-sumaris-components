import { Entity } from '../../services/model/entity.model';
import { AbstractControl, AbstractControlOptions, UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, ValidatorFn } from '@angular/forms';
import { toBoolean } from '../../../shared/functions';
import { addValueInArray, clearValueInArray, copyEntity2Form, removeValueInArray, resizeArray } from '../../../shared/forms';
import { SharedFormArrayValidators } from '../../../shared/validator/validators';

export interface FormArrayHelperOptions {
  allowEmptyArray: boolean; // false by default /!\ TRUE in the AppFormArray
  allowManyNullValues?: boolean; // false by default
  allowDuplicatedValues?: boolean; // false by default
  validators?: ValidatorFn[];
}

export class FormArrayHelper<T = Entity<any>, C extends AbstractControl = AbstractControl> {
  static getOrCreateArray(formBuilder: UntypedFormBuilder, form: UntypedFormGroup, arrayName: string): UntypedFormArray {
    const disabled = form.disabled;
    let arrayControl = form.get(arrayName) as UntypedFormArray;
    if (!arrayControl) {
      arrayControl = formBuilder.array([]);

      // Apply parent disabled state, before to push it into the array
      // This is need to avoid parent form to be enabled, after calling resizeArray()
      if (disabled && arrayControl.enabled) arrayControl.disable({ emitEvent: false });

      form.addControl(arrayName, arrayControl);
    }
    return arrayControl;
  }

  private _allowEmptyArray: boolean;
  private _allowManyNullValues: boolean;
  private _allowDuplicatedValues: boolean;

  private readonly _validators: ValidatorFn[];

  get allowEmptyArray(): boolean {
    return this._allowEmptyArray;
  }

  set allowEmptyArray(value: boolean) {
    this.setAllowEmptyArray(value);
  }

  get allowManyNullValues(): boolean {
    return this._allowManyNullValues;
  }

  set allowManyNullValues(value: boolean) {
    this._allowManyNullValues = value;
  }

  get allowDuplicatedValues(): boolean {
    return this._allowDuplicatedValues;
  }

  set allowDuplicatedValues(value: boolean) {
    this._allowDuplicatedValues = value;
  }

  get formArray(): UntypedFormArray {
    return this._formArray;
  }

  constructor(
    private readonly _formArray: UntypedFormArray,
    private createControl: (value?: T) => C,
    private equals: (v1: T, v2: T) => boolean,
    private isEmpty: (value: T) => boolean,
    options?: FormArrayHelperOptions
  ) {
    this._validators = options && options.validators;

    // empty array not allow by default
    this.setAllowEmptyArray(toBoolean(options?.allowEmptyArray, false));
    this._allowManyNullValues = toBoolean(options?.allowManyNullValues, false);
    this._allowDuplicatedValues = toBoolean(options?.allowDuplicatedValues, false);
  }

  /**
   * @param value
   * @param options
   */
  add(value?: T, options?: { emitEvent: boolean; insertAt?: number }): boolean {
    return addValueInArray(this._formArray, this.createControl, this.equals, this.isEmpty, value, {
      allowManyNullValues: this.allowManyNullValues,
      allowDuplicateValue: this.allowDuplicatedValues,
      ...options,
    });
  }

  removeAt(index: number) {
    // Do not remove if last criterion
    if (!this._allowEmptyArray && this._formArray.length === 1) {
      return clearValueInArray(this._formArray, this.isEmpty, index);
    } else {
      return removeValueInArray(this._formArray, this.isEmpty, index);
    }
  }

  resize(length: number, options?: { emitEvent?: boolean }): boolean {
    return resizeArray(this._formArray, this.createControl, length, options);
  }

  clearAt(index: number): boolean {
    return clearValueInArray(this._formArray, this.isEmpty, index);
  }

  isLast(index: number): boolean {
    return this._formArray.length - 1 === index;
  }

  removeAllEmpty() {
    let index = this._formArray.controls.findIndex((c) => this.isEmpty(c.value));
    while (index !== -1) {
      this.removeAt(index);
      index = this._formArray.controls.findIndex((c) => this.isEmpty(c.value));
    }
  }

  size(): number {
    return this._formArray.length;
  }

  at(index: number): C {
    return this._formArray.at(index) as C;
  }

  /**
   * Resize the FormArray, then set values
   *
   * @param values
   * @param options
   */
  setValue(
    values: T[],
    options?: {
      onlySelf?: boolean;
      emitEvent?: boolean;
    }
  ): void {
    this.resize(values?.length || 0);
    this._formArray.setValue(values, options);
  }

  /**
   * Resize the FormArray, then patch values
   *
   * @param values
   * @param options
   */
  patchValue(
    values: T[],
    options?: {
      onlySelf?: boolean;
      emitEvent?: boolean;
    }
  ): void {
    this.resize(values?.length || 0);
    this._formArray.patchValue(values, options);
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this._formArray.controls.forEach((c) => c.disable(opts));
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    this._formArray.controls.forEach((c) => c.enable(opts));
  }

  forEach(ite: (control: C) => void) {
    const size = this.size();
    for (let i = 0; i < size; i++) {
      const control = this._formArray.at(i) as C;
      if (control) ite(control);
    }
  }

  /* -- internal methods -- */

  protected setAllowEmptyArray(value: boolean) {
    if (this._allowEmptyArray === value) return; // Skip if same

    this._allowEmptyArray = value;

    // Set required (or reste) min length validator
    if (this._allowEmptyArray) {
      this._formArray.setValidators(this._validators || null);
    } else {
      this._formArray.setValidators((this._validators || []).concat(SharedFormArrayValidators.requiredArrayMinLength(1)));
    }
  }
}

export declare interface AppFormArrayOptions extends AbstractControlOptions {
  allowEmptyArray?: boolean; // true by default
  allowReuseControls?: boolean; // true by default
  allowManyNullValues?: boolean; // false by default
  allowDuplicateValue?: boolean; // false by default
}

export class AppFormArray<T = Entity<any>, C extends AbstractControl = AbstractControl> extends UntypedFormArray {
  private readonly options: AppFormArrayOptions;

  get allowEmptyArray(): boolean {
    return this.options.allowEmptyArray;
  }

  set allowEmptyArray(value: boolean) {
    this.setAllowEmptyArray(value);
  }

  get allowManyNullValues(): boolean {
    return this.options.allowManyNullValues;
  }

  set allowManyNullValues(value: boolean) {
    this.options.allowManyNullValues = value;
  }

  get allowDuplicateValue(): boolean {
    return this.options.allowDuplicateValue;
  }

  set allowDuplicateValue(value: boolean) {
    this.options.allowDuplicateValue = value;
  }

  constructor(
    public createControl: (value?: T) => C,
    private equals: (v1: T, v2: T) => boolean,
    private isEmpty: (value: T) => boolean,
    options?: AppFormArrayOptions | undefined | null
  ) {
    super([], options);
    this.options = {
      allowEmptyArray: true,
      allowReuseControls: true,
      allowManyNullValues: false,
      allowDuplicateValue: false,
      ...options,
    };
    this.setAllowEmptyArray(this.options.allowEmptyArray);
  }

  /**
   * WIll rebuild the array, using the given values
   *
   * @param values
   * @param options
   */
  setValue(
    values: any[],
    options?: {
      onlySelf?: boolean;
      emitEvent?: boolean;
    }
  ): void {
    if (values === undefined) throw new Error("'undefined' value not allowed in AppFormArray.setValue(). Use 'null' or '[]' to clear the array");

    if (this.options.allowReuseControls === false) {
      const disabled = this.disabled;

      // Clean all
      this.resize(0, { emitEvent: options?.emitEvent });

      // Recreate each control, with a default value
      (values || []).forEach((value) => {
        const control = this.createControl(value);

        // Apply parent disabled state, before to push it into the array
        // This is need to avoid parent form to be enabled, after calling AppFormArray.patchValue() (e.g. in table's row validator)
        if (disabled && control.enabled) control.disable({ emitEvent: false });
        else if (!disabled && control.disabled) control.enable({ emitEvent: false });

        this.push(control, options);
      });
    } else {
      this.resize(values?.length || 0, { emitEvent: false });
      super.setValue(values, options);
    }
  }

  patchValue(
    values: any[] | null | undefined,
    options?: {
      onlySelf?: boolean;
      emitEvent?: boolean;
    }
  ): void {
    // --- /!\ From official Angular doc of 'FormGroup.patchValue()' :
    //   Even though the `value` argument type doesn't allow `null` and `undefined` values, the
    //   `patchValue` can be called recursively and inner data structures might have these values, so
    //   we just ignore such cases when a field containing FormGroup instance receives `null` or
    //   `undefined` as a value.
    // ---
    if (values == null) return; // Ignore

    if (this.options.allowReuseControls === false) {
      const disabled = this.disabled;

      // Clean all
      this.resize(0, { emitEvent: options?.emitEvent });

      // Recreate each control, with a default value
      (values || []).forEach((value) => {
        const control = this.createControl(value);

        // Apply parent disabled state, before to push it into the array
        // This is need to avoid parent form to be enabled, after calling AppFormArray.patchValue() (e.g. in table's row validator)
        if (disabled && control.enabled) control.disable({ emitEvent: false });
        else if (!disabled && control.disabled) control.enable({ emitEvent: false });

        this.push(control, options);
      });
    } else {
      this.resize(values?.length || 0, { emitEvent: false });
      super.patchValue(values, options);
    }
  }

  resize(length: number, options?: { emitEvent?: boolean }): boolean {
    return resizeArray(this, this.createControl, length, options);
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    this.controls.forEach((c) => c.disable(opts));
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);
    this.controls.forEach((c) => c.enable(opts));
  }

  forEach(ite: (control: C) => void) {
    const size = this.length;
    for (let i = 0; i < size; i++) {
      const control = this.at(i) as C;
      if (control) ite(control);
    }
  }

  /**
   * @param value
   * @param options
   */
  add(value?: T, options?: { emitEvent: boolean; insertAt?: number }) {
    addValueInArray(this, this.createControl, this.equals, this.isEmpty, value, { ...this.options, ...options });
  }

  removeAt(index: number, options?: { emitEvent: boolean }) {
    // Do not remove if last criterion
    if (this.options.allowEmptyArray === false && this.length === 1) {
      this.clearAt(index, options);
      return false;
    } else if (index < this.length) {
      super.removeAt(index, options);
      this.markAsDirty();
      return true;
    }
    return false;
  }

  clearAt(index: number, options?: { emitEvent: boolean }) {
    const control = this.at(index);
    if (this.isEmpty(control.value)) return; // skip (not need to clear)

    if (control instanceof UntypedFormGroup) {
      copyEntity2Form({}, control, options);
    } else if (control instanceof UntypedFormArray) {
      control.setValue([], options);
    } else {
      control.setValue(null, options);
    }
    this.markAsDirty();
  }

  isLast(index: number): boolean {
    return this.length - 1 === index;
  }

  removeAllEmpty() {
    let index = this.controls.findIndex((c) => this.isEmpty(c.value));
    while (index !== -1) {
      this.removeAt(index);
      index = this.controls.findIndex((c) => this.isEmpty(c.value));
    }
  }

  clone(): AppFormArray<T, C> {
    return new AppFormArray<T, C>(this.createControl, this.equals, this.isEmpty, { ...this.options });
  }

  /* -- internal -- */

  protected setAllowEmptyArray(value: boolean) {
    if (this.options.allowEmptyArray === value) return; // Skip if same

    this.options.allowEmptyArray = value;

    // Set required (or reset) min length validator
    if (this.options.allowEmptyArray) {
      this.setValidators(this.options.validators || null);
    } else {
      const validators = this.options?.validators || [];
      this.setValidators((Array.isArray(validators) ? validators : [validators]).concat(SharedFormArrayValidators.requiredArrayMinLength(1)));
    }
  }
}
