import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { CoreModule } from '../../../core.module';
import { ArrayFormTestPage } from './form-array.test';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, SharedModule, CoreModule, TranslateModule.forChild()],

  declarations: [ArrayFormTestPage],
  exports: [ArrayFormTestPage, RouterModule],
})
export class FormArrayTestModule {}
