import { ChangeDetectorRef, Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl } from '@angular/forms';

import { PlatformService } from '../../../services/platform.service';
import { Subscription } from 'rxjs';
import { isNilOrBlank, trimEmptyToNull } from '../../../../shared/functions';
import { AppForm } from '../../form.class';
import { AppFormArray } from '../form-array';

@Component({
  selector: 'app-array-test',
  templateUrl: './form-array.test.html',
})
export class ArrayFormTestPage extends AppForm<any> implements OnInit, OnDestroy {
  private subscription = new Subscription();

  get defaultFormArray(): AppFormArray<any, any> {
    return this.form.get('default') as AppFormArray<any, any>;
  }

  get notEmptyFormArray(): AppFormArray<any, any> {
    return this.form.get('notEmpty') as AppFormArray<any, any>;
  }

  get nullValuesFormArray(): AppFormArray<any, any> {
    return this.form.get('nullValues') as AppFormArray<any, any>;
  }

  get duplicatedValuesFormArray(): AppFormArray<any, any> {
    return this.form.get('duplicatedValues') as AppFormArray<any, any>;
  }

  get valueToAddControl(): UntypedFormControl {
    return this.form?.get('valueToAdd') as UntypedFormControl;
  }

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected platform: PlatformService,
    protected injection: Injector,
    protected cd: ChangeDetectorRef
  ) {
    super(
      injection,
      formBuilder.group({
        default: new AppFormArray(
          (value) => this.formBuilder.control(value),
          (v1, v2) => v1 === v2,
          (value) => isNilOrBlank(value)
        ),

        notEmpty: new AppFormArray(
          (value) => this.formBuilder.control(value),
          (v1, v2) => v1 === v2,
          (value) => isNilOrBlank(value),
          {
            allowEmptyArray: false,
          }
        ),

        nullValues: new AppFormArray(
          (value) => this.formBuilder.control(value),
          (v1, v2) => v1 === v2,
          (value) => isNilOrBlank(value),
          {
            allowEmptyArray: false,
            allowManyNullValues: true,
          }
        ),

        duplicatedValues: new AppFormArray(
          (value) => this.formBuilder.control(value),
          (v1, v2) => v1 === v2,
          (value) => isNilOrBlank(value),
          {
            allowEmptyArray: false,
            allowManyNullValues: true,
            allowDuplicateValue: true,
          }
        ),

        // Value to add
        valueToAdd: formBuilder.control(null, []),
      })
    );
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // Load the form with data
  async loadData() {
    this.setValue({
      default: ['TESTING_1', 'TESTING_2'],
      notEmpty: ['TESTING_1', 'TESTING_2'],
      nullValues: ['TESTING_1', null, 'TESTING_4'],
      duplicatedValues: ['TESTING_1', null, 'TESTING_1'],
      // Add with value
      valueToAdd: null,
    });
  }

  protected setFormArrayValue(formArray: AppFormArray<any, any>, values: string | string[]) {
    const arrayValues = Array.isArray(values) ? values : values?.split(',').map(trimEmptyToNull);
    formArray.patchValue(arrayValues);
    this.markAsDirty();
  }

  /* -- protected methods -- */

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
