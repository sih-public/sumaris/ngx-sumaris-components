import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { AppListForm } from './list.form';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],

  declarations: [
    // Other components
    AppListForm,
  ],
  exports: [
    TranslateModule,

    // Components
    AppListForm,
  ],
})
export class AppListFormModule {}
