import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AppPropertiesFormModule } from './properties/properties.module';
import { AppListFormModule } from './list/list.module';
import { AppTextPopoverModule } from './text-popover/text-popover.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppEntityFormModule } from './entity/entity.module';
import { AppFormButtonsBarModule } from './buttons/form-buttons-bar.module';
import { AppUsernameFormModule } from './username/username.module';

@NgModule({
  imports: [
    SharedModule,
    TranslateModule.forChild(),

    // Sub modules
    AppEntityFormModule,
    AppListFormModule,
    AppPropertiesFormModule,
    AppTextPopoverModule,
    AppFormButtonsBarModule,
    AppUsernameFormModule,
  ],

  declarations: [],

  exports: [
    TranslateModule,

    // Sub modules
    AppEntityFormModule,
    AppListFormModule,
    AppPropertiesFormModule,
    AppTextPopoverModule,
    AppFormButtonsBarModule,
  ],
})
export class AppFormModule {}
