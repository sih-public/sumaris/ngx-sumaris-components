import { AbstractControl } from '@angular/forms';
import { round, toBoolean } from '../../shared/functions';
import { filterNumberInput, selectInputContent, selectInputContentFromEvent } from '../../shared/inputs';
import { waitFor, WaitForOptions } from '../../shared/observables';
import {
  addValueInArray,
  clearValueInArray,
  copyEntity2Form,
  disableAndClearControl,
  disableAndClearControls,
  disableControl,
  disableControls,
  enableControl,
  enableControls,
  filterFormErrors,
  filterFormErrorsByPath,
  filterFormErrorsByPrefix,
  getControlFromPath,
  getFormErrors,
  getFormValueFromEntity,
  logFormErrors,
  markAllAsTouched,
  markAsUntouched,
  removeValueInArray,
  resizeArray,
  setControlEnabled,
  setControlsEnabled,
  updateValueAndValidity,
  waitIdle,
  waitWhilePending,
} from '../../shared/forms';

export declare type IAppFormGetter = () => IAppForm;

export declare interface OnReady {
  ngOnReady();
}

export interface IAppForm<T = any> {
  // From Angular form
  invalid: boolean;
  valid: boolean;
  dirty: boolean;
  pending: boolean;
  error: string;
  enabled: boolean;
  disabled: boolean;
  touched: boolean;
  untouched: boolean;
  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;

  // Specific to IAppForm
  empty?: boolean;
  loading?: boolean;
  loaded?: boolean;
  destroyed?: boolean;
  ready(): Promise<void>;
  waitIdle(opts?: WaitForOptions): Promise<any>;

  // Optional functions
  setValue?: (data: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }) => Promise<void> | void;
  patchValue?: (data: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }) => Promise<void> | void;
  reset?: (data?: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }) => void;
  getValue?: () => Promise<T> | T;

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAllAsTouched(opts?: { emitEvent?: boolean }): void;
  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAsLoading(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAsLoaded(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
  markAsReady(opts?: { onlySelf?: boolean; emitEvent?: boolean }): void;
}

export interface CanSave {
  dirty: boolean;
  valid: boolean;
  save(): Promise<boolean>;
}

export interface CanLeave extends CanSave {
  cancel(event?: Event): Promise<void>;
}

/**
 * A form that do nothing
 */
export class AppNullForm implements IAppForm {
  readonly invalid = false;
  readonly valid = false;
  readonly dirty = false;
  readonly empty = true;
  readonly pending = false;
  readonly error = null;
  readonly enabled = false;
  readonly loading = false;
  readonly touched = false;
  get disabled(): boolean {
    return !this.enabled;
  }
  get untouched(): boolean {
    return !this.touched;
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAllAsTouched(opts?: { emitEvent?: boolean }) {}
  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAsLoading(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAsLoaded(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}
  markAsReady(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {}

  waitIdle(opts?: WaitForOptions): Promise<any> {
    return Promise.reject(false);
  }
  ready(): Promise<any> {
    return Promise.reject(true);
  }
}

export class AppFormProvider<F extends IAppForm = IAppForm> implements IAppForm {
  static NULL_FORM = new AppNullForm();

  constructor(private getterFn: () => F) {}

  private get delegate(): IAppForm {
    return this.getterFn() || AppFormProvider.NULL_FORM;
  }

  waitDelegate(opts?: WaitForOptions): Promise<F> {
    let content: F;
    return waitFor(() => {
      content = this.getterFn();
      return !!content;
    }, opts).then((_) => content);
  }

  /* -- delegated methods -- */

  get enabled(): boolean {
    return this.delegate.enabled;
  }
  get disabled(): boolean {
    return this.delegate.disabled;
  }
  get error(): string {
    return this.delegate.error;
  }
  get invalid(): boolean {
    return this.delegate.invalid;
  }
  get valid(): boolean {
    return this.delegate.valid;
  }
  get dirty(): boolean {
    return this.delegate.dirty;
  }
  get empty(): boolean {
    return this.delegate.empty;
  }
  get pending(): boolean {
    return this.delegate.pending;
  }
  get loading(): boolean {
    return this.delegate.loading;
  }
  get touched(): boolean {
    return this.delegate.touched;
  }
  get untouched(): boolean {
    return this.delegate.untouched;
  }
  ready(): Promise<any> {
    return this.delegate.ready();
  }
  waitIdle(opts?: WaitForOptions): Promise<any> {
    return this.delegate.waitIdle();
  }
  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.disable(opts);
  }
  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.enable(opts);
  }
  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsPristine(opts);
  }
  markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsUntouched(opts);
  }
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsTouched(opts);
  }
  markAllAsTouched(opts?: { emitEvent?: boolean }) {
    return this.delegate.markAllAsTouched(opts);
  }
  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsDirty(opts);
  }
  markAsLoading(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsLoading(opts);
  }
  markAsLoaded(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsLoaded(opts);
  }
  markAsReady(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    return this.delegate.markAsReady(opts);
  }

  async save(): Promise<boolean> {
    if (AppFormUtils.canSave(this.delegate)) {
      return this.delegate.save();
    }
    return true;
  }
}

/**
 * Helper class, for angular forms
 */
export class AppFormUtils {
  static copyEntity2Form = copyEntity2Form;
  static getFormValueFromEntity = getFormValueFromEntity;
  static logFormErrors = logFormErrors;
  static getControlFromPath = getControlFromPath;
  static filterNumberInput = filterNumberInput;
  static enableControls = enableControls;
  static enableControl = enableControl;
  static disableControl = disableControl;
  static disableControls = disableControls;
  static disableAndClearControl = disableAndClearControl;
  static disableAndClearControls = disableAndClearControls;
  static setControlEnabled = setControlEnabled;
  static setControlsEnabled = setControlsEnabled;
  static selectInputContentFromEvent = selectInputContentFromEvent;
  static selectInputContent = selectInputContent;
  /**
   * @deprecated use markAllAsTouched() instead (naming was wrong)
   */
  static markAsTouched = markAllAsTouched;

  /**
   * Marks the control and all its descendant controls as touched.
   * See also:
   * - `markAsTouched()`
   */
  static markAllAsTouched = markAllAsTouched;
  static markAsUntouched = markAsUntouched;
  static waitWhilePending = waitWhilePending;
  static waitIdle = waitIdle;
  static updateValueAndValidity = updateValueAndValidity;

  static getFormErrors = getFormErrors;

  static isForm(object: any): object is IAppForm {
    return object instanceof AppFormProvider || ('dirty' in object && 'enabled' in object && 'valid' in object);
  }

  static canSave(object: any): object is IAppForm & CanSave {
    return 'save' in object && AppFormUtils.isForm(object);
  }

  // ArrayForm
  static addValueInArray = addValueInArray;
  static removeValueInArray = removeValueInArray;
  static resizeArray = resizeArray;
  static clearValueInArray = clearValueInArray;

  // Calculated fields
  static calculatedSuffix = 'Calculated';
  static isControlHasInput = isControlHasInput;
  static setCalculatedValue = setCalculatedValue;
  static resetCalculatedValue = resetCalculatedValue;

  // Form errors
  static filterErrorsByPrefix = filterFormErrorsByPrefix;
  static filterErrorsByPath = filterFormErrorsByPath;
  static filterErrors = filterFormErrors;
}

export function isControlHasInput(controls: { [key: string]: AbstractControl }, controlName: string): boolean {
  // true if the control has a value and its 'calculated' control has the value 'false'
  return controls[controlName].value && !toBoolean(controls[controlName + AppFormUtils.calculatedSuffix].value, false);
}

export function setCalculatedValue(controls: { [key: string]: AbstractControl }, controlName: string, value: number | undefined) {
  // set value to control
  controls[controlName].setValue(round(value));
  // set 'calculated' control to 'true'
  controls[controlName + AppFormUtils.calculatedSuffix].setValue(true);
}

export function resetCalculatedValue(controls: { [key: string]: AbstractControl }, controlName: string) {
  if (!isControlHasInput(controls, controlName)) {
    // set undefined only if control already calculated
    AppFormUtils.setCalculatedValue(controls, controlName, undefined);
  }
}
