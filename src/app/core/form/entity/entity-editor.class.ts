import { AfterViewInit, ChangeDetectorRef, Directive, EventEmitter, Injector, OnDestroy, OnInit, Optional } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Params, Router } from '@angular/router';
import { AlertController, IonRouterOutlet, NavController, ToastController } from '@ionic/angular';

import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable, of, Subject, Subscription } from 'rxjs';
import { isMoment } from 'moment';
import { AddToPageHistoryOptions, LocalSettingsService } from '../../services/local-settings.service';
import { debounceTime, filter, map, mergeMap } from 'rxjs/operators';
import { Entity } from '../../services/model/entity.model';
import { UsageMode } from '../../services/model/settings.model';
import { FormGroup } from '@angular/forms';
import { AppTabEditor, AppTabEditorOptions, IAppTabEditor } from './tab-editor.class';
import { AppFormUtils } from '../form.utils';
import { Alerts } from '../../../shared/alerts';
import { AppErrorWithDetails, ErrorCodes, ServerErrorCodes } from '../../services/errors';
import { equals, isInt, isNil, isNilOrBlank, isNotEmptyArray, isNotNil, isNotNilOrBlank, toBoolean, toNumber } from '../../../shared/functions';
import { EntityServiceLoadOptions, IEntityService } from '../../../shared/services/entity-service.class';
import { DateFormatService } from '../../../shared/pipes/date-format.pipe';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';
import { HistoryPageReference } from '../../services/model/history.model';
import { firstFalse, firstNotNilPromise, waitForFalse, WaitForOptions } from '../../../shared/observables';
import { FormErrorTranslator } from '../../../shared/validator/form-error-adapter.class';
import { isStartableService } from '../../../shared/services/startable-service.class';

export interface IAppEntityEditor<T> extends IAppTabEditor<T> {
  usageMode: UsageMode;
  isNewData: boolean;
}

export class AppEditorOptions extends AppTabEditorOptions {
  autoLoad?: boolean;
  autoLoadDelay?: number;
  pathIdAttribute?: string;
  enableListenChanges?: boolean;
  listenIntervalInSeconds?: number;

  /**
   * Change page route (window URL) when saving for the first time
   */
  autoUpdateRoute?: boolean; // Default to true

  /**
   * Open the next tab, after saving for the first time
   */
  autoOpenNextTab?: boolean; // Default to true

  i18nPrefix?: string;
}

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppEntityEditor<
    T extends Entity<T, ID>,
    S extends IEntityService<T, ID> = IEntityService<T, any>,
    ID = number,
    LO extends EntityServiceLoadOptions = EntityServiceLoadOptions,
  >
  extends AppTabEditor<T, ID, LO>
  implements OnInit, OnDestroy, AfterViewInit, IAppEntityEditor<T>
{
  data: T;
  previousDataId: ID;

  defaultBackHref: string;
  historyIcon: { icon?: string; matIcon?: string };

  readonly titleSubject = new BehaviorSubject<string>(undefined);
  readonly onUpdateView = new EventEmitter<T>();

  /**
   *  @deprecated
   */
  protected get $title(): Subject<string> {
    return this.titleSubject;
  }

  protected readonly cd: ChangeDetectorRef;
  protected readonly dateFormat: DateFormatService;
  protected readonly settings: LocalSettingsService;
  protected readonly environment: Environment;
  protected readonly routerOutlet: IonRouterOutlet;
  protected errorTranslator: FormErrorTranslator;

  private _usageMode: UsageMode;
  private readonly _enableListenChanges: boolean;
  private readonly _listenIntervalInSeconds: number;
  private readonly _pathIdAttribute: string;
  private readonly _autoLoad: boolean;
  private readonly _autoLoadDelay: number;
  private readonly _autoUpdateRoute: boolean;
  private _autoOpenNextTab: boolean;
  private _listenChangesSubscription: Subscription;

  get usageMode(): UsageMode {
    return this._usageMode;
  }

  set usageMode(value: UsageMode) {
    if (this._usageMode !== value) {
      this._usageMode = value;
      this.markForCheck();
    }
  }

  get isOnFieldMode(): boolean {
    return this.settings.isOnFieldMode(this._usageMode);
  }

  get isNewData(): boolean {
    return isNil(this.data?.id);
  }

  get service(): S {
    return this.dataService;
  }

  get autoOpenNextTab(): boolean {
    return this._autoOpenNextTab;
  }

  set autoOpenNextTab(value: boolean) {
    this._autoOpenNextTab = value;
  }

  protected get pathIdAttribute(): string {
    return this._pathIdAttribute;
  }

  protected constructor(
    injector: Injector,
    protected dataType: new () => T,
    protected dataService?: S,
    @Optional() options?: AppEditorOptions
  ) {
    super(
      injector.get(ActivatedRoute),
      injector.get(Router),
      injector.get(NavController),
      injector.get(AlertController),
      injector.get(TranslateService),
      options
    );
    this.environment = injector.get(ENVIRONMENT);
    options = <AppEditorOptions>{
      // Default options
      enableListenChanges: this.environment.entityEditor?.enableListenChanges || false, // False  by default
      listenIntervalInSeconds: toNumber(this.environment.entityEditor?.listenIntervalInSeconds, 0), // no timer by default
      pathIdAttribute: 'id',
      autoLoad: true,
      autoUpdateRoute: true,
      i18nPrefix: '',

      // Following options are override inside ngOnInit()
      // autoOpenNextTab: ...,

      // Override defaults
      ...options,
    };

    this.cd = injector.get(ChangeDetectorRef);
    this.settings = injector.get(LocalSettingsService);
    this.errorTranslator = injector.get(FormErrorTranslator);
    this.routerOutlet = injector.get(IonRouterOutlet);
    this.dateFormat = injector.get(DateFormatService);
    this.toastController = injector.get(ToastController);
    this._enableListenChanges = options.enableListenChanges;
    this._listenIntervalInSeconds = options.listenIntervalInSeconds;
    this._pathIdAttribute = options.pathIdAttribute;
    this._autoLoad = options.autoLoad;
    this._autoLoadDelay = options.autoLoadDelay;
    this._autoUpdateRoute = options.autoUpdateRoute;
    this._autoOpenNextTab = options.autoOpenNextTab;
    this.i18nContext = {
      prefix: options.i18nPrefix,
      suffix: '',
    };

    // FOR DEV ONLY ----
    //this.debug = !environment.production;
    this._logPrefix = '[entity-editor] ';
  }

  ngOnInit() {
    super.ngOnInit();

    // Defaults
    this._autoOpenNextTab = toBoolean(this._autoOpenNextTab, !this.isOnFieldMode);
    this.historyIcon = this.historyIcon || { icon: 'list' };

    // Register forms
    this.registerForms();

    // Disable page, during load
    this.disable();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    // Load data
    if (this._autoLoad) {
      setTimeout(() => this.loadFromRoute(), this._autoLoadDelay);
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.titleSubject.complete();
    this.titleSubject.unsubscribe();
    this.onUpdateView.complete();
    this.onUpdateView.unsubscribe();
  }

  waitIdle(opts?: WaitForOptions): Promise<void> {
    // Wait end of saving
    if (this.saving) return waitForFalse(this.savingSubject, { stop: this.destroySubject });

    return super.waitIdle(opts);
  }

  /**
   * Load data from id, using the dataService
   *
   * @param id
   * @param opts
   */
  async load(
    id?: ID,
    opts?: EntityServiceLoadOptions & {
      emitEvent?: boolean;
      openTabIndex?: number;
      updateRoute?: boolean;
      [key: string]: any;
    }
  ) {
    if (!this.dataService) throw new Error("Cannot load data: missing 'dataService'!");

    this.resetError();

    // New data
    if (isNil(id)) {
      try {
        // Create using default values
        const data = new this.dataType();
        this._usageMode = this.computeUsageMode(data);
        await this.onNewEntity(data, opts);
        await this.updateView(data, {
          openTabIndex: 0,
          ...opts,
        });
      } catch (err) {
        this.setError(err, { emitEvent: false });
        this.selectedTabIndex = 0;
      } finally {
        this.markAsLoaded();
      }
    }

    // Load existing data
    else {
      try {
        const data = await this.dataService.load(id, opts);
        if (!data) throw { code: ErrorCodes.DATA_NOT_FOUND_ERROR, message: 'ERROR.DATA_NO_FOUND' };
        this._usageMode = this.computeUsageMode(data);
        await this.onEntityLoaded(data, opts);
        await this.updateView(data, opts);
        this.startListenChanges();
      } catch (err) {
        this.setError(err);
        this.selectedTabIndex = 0;
      } finally {
        this.markAsLoaded();
      }
    }
  }

  canUserWrite(data: T, opts?: any): boolean {
    return this.dataService.canUserWrite(data, opts);
  }

  startListenChanges() {
    // Skip if disabled, or already listening
    if (this._listenChangesSubscription || !this._enableListenChanges) return;
    // Skip if new (nothing to listen)
    if (this.isNewData) return;

    // Listen for changes on server
    const subscription = this.listenChanges(this.data.id, {
      interval: this._listenIntervalInSeconds,
    })
      ?.pipe(
        filter(isNotNil),
        mergeMap((data) =>
          this.saving
            ? // If saving, wait end, to avoid to detect self changes
              firstFalse(this.savingSubject).pipe(
                map(() => data),
                debounceTime(500)
              )
            : of(data)
        )
      )
      .subscribe((data: T) => {
        const isNewer = isMoment(data.updateDate) && (!this.data.updateDate || data.updateDate.isAfter(this.data.updateDate));
        if (!isNewer) return; // Skip

        // Editor has no changes: reload
        if (!this.dirty) {
          if (this.debug) console.debug(`[data-editor] Changes detected on server, at {${data.updateDate}}: reloading page...`);
          this.reload();
        }
        // Cannot reload: alert the user
        else {
          if (this.debug) console.debug(`[data-editor] Changes detected on server, at {${data.updateDate}}, but page is dirty: skip reloading.`);
          this.showToast({ message: 'ERROR.DATA_UPDATE_DATE_CHANGED', type: 'warning', showCloseButton: true });
        }
      });

    // Register subscription
    if (subscription) {
      this._listenChangesSubscription = subscription;
      this.registerSubscription(this._listenChangesSubscription);
      subscription.add(() => {
        this.unregisterSubscription(subscription);
        this._listenChangesSubscription = null;
      });
    }
  }

  stopListenChanges() {
    this._listenChangesSubscription?.unsubscribe();
  }

  async ready(opts?: WaitForOptions): Promise<void> {
    await super.ready(opts);

    // Wait service to be ready
    if (isStartableService(this.dataService)) {
      await this.dataService.ready();
    }
  }

  async updateView(
    data: T | null,
    opts?: {
      emitEvent?: boolean;
      openTabIndex?: number;
      updateRoute?: boolean;
    }
  ): Promise<void> {
    const idChanged =
      isNotNil(data.id) &&
      this.previousDataId !== undefined && // Ignore if first loading (=undefined)
      this.previousDataId !== data.id;

    opts = {
      updateRoute: this._autoUpdateRoute && idChanged && !this.loading,
      ...opts,
    };

    this.data = data;
    this.previousDataId = data.id || null;

    await this.ready();
    await this.setValue(data);

    if (!opts || opts.emitEvent !== false) {
      this.markAsPristine();
      this.markAsUntouched();
      this.updateViewState(data);

      // Need to update route
      if (opts.updateRoute === true) {
        this.updateTabAndRoute(data, opts)
          // Update the title - should be executed AFTER updateRoute because of path change - fix #185
          .then(() => this.updateTitle(data));
      } else {
        // Update the next tab
        this.updateTabIndex(opts.openTabIndex);

        // Update the title.
        this.updateTitle(data);
      }

      this.onUpdateView.emit(data);
    }
  }

  /**
   * Enable or disable state
   */
  updateViewState(data: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.isNewData || this.canUserWrite(data)) {
      this.enable(opts);
    } else {
      this.disable(opts);

      // Allow to sort tables
      this.tables?.forEach((t) => t.enableSort());
      this.asyncTables?.forEach((t) => t.enableSort());
    }
  }

  /**
   * Update the route location, and open the next tab
   */
  async updateTabAndRoute(
    data: T,
    opts?: {
      openTabIndex?: number;
    }
  ): Promise<void> {
    const queryParams = { ...this.queryParams };

    // Open the next tab
    this.updateTabIndex(opts && opts.openTabIndex);

    if (!this.queryTabIndexParamName || !this.route) return; // Skip route update

    // Update route location
    queryParams[this.queryTabIndexParamName] = this.selectedTabIndex !== 0 ? this.selectedTabIndex : undefined;
    await this.updateRoute(data, queryParams);
  }

  /**
   * Update the route location, and open the next tab
   */
  updateTabIndex(tabIndex?: number) {
    if (isNotNil(tabIndex) && tabIndex >= 0 && tabIndex !== this.selectedTabIndex) {
      this.selectedTabIndex = tabIndex;
      this.markForCheck();
    }
  }

  async saveAndClose(event: Event, options?: any): Promise<boolean> {
    const saved = await this.save(event);
    if (saved) {
      await this.close(event);
    }
    return saved;
  }

  async close(event: Event) {
    if (event) {
      if (event.defaultPrevented) return;
      event.preventDefault();
      event.stopPropagation();
    }
    return this.goBack();
  }

  /**
   * Save the editor, by calling the dataService.save().
   * Ensure that editor if valid. If not, display an error
   *
   * @param event
   * @param opts
   */
  async save(event?: Event, opts?: any): Promise<boolean> {
    if (this.loading || this.saving) {
      console.debug('[data-editor] Skip save: editor is busy (loading or saving)');
      return false;
    }
    if (!this.dirty && !this.isNewData) {
      console.debug('[data-editor] Skip save: editor not dirty');
      return true;
    }

    // Wait end of async validation
    await this.waitWhilePending();

    // If invalid
    if (this.invalid) {
      this.logFormErrors();
      this.openFirstInvalidTab();
      this.scrollToTop();
      this.markAllAsTouched();

      this.submitted = true;
      return false;
    }

    this.markAsSaving();
    this.resetError();

    if (this.debug) console.debug('[data-editor] Saving data...');

    try {
      // Compute next tab index (if enable)
      const nextTabIndex = this._autoOpenNextTab && isNil(this.previousDataId) && this.computeNextTabIndex();

      // Save all dirty tables
      const saved = await this.saveDirtyChildren();
      if (!saved) return false;

      // Get data
      const data = await this.getValue();

      this.disable();

      // Save form
      const updatedData = await this.dataService.save(data, opts);

      await this.onEntitySaved(updatedData);

      // Update the view (e.g metadata)
      await this.updateView(updatedData, {
        // Open next tab only if user has not changed it
        openTabIndex: nextTabIndex > this._selectedTabIndex ? nextTabIndex : undefined,
        ...opts,
      });

      // Subscribe to remote changes
      if (!this._listenChangesSubscription) this.startListenChanges();

      this.submitted = false;

      return true;
    } catch (err) {
      this.submitted = true;
      this.selectedTabIndex = 0; // Can be override by subclasses, using setError()
      this.setError(err);
      this.scrollToTop(); // Scroll to top (to show error)
      this.markAsDirty();
      this.enable();

      // Concurrent change on pod
      if (err.code === ServerErrorCodes.BAD_UPDATE_DATE && isNotNil(this.data.id)) {
        // Call a data reload (in background), to update the GraphQL cache, and allow to cancel changes
        this.dataService.load(this.data.id, { fetchPolicy: 'network-only' }).then(() => {
          console.debug('[data-editor] Data cache reloaded. User can reload page');
        });
      }

      return false;
    } finally {
      this.markAsSaved();
    }
  }

  /**
   * Save data (if dirty and valid), and return it. Otherwise, return nil value.
   */
  async saveAndGetDataIfValid(): Promise<T | undefined> {
    // Form is not valid
    if (!this.valid) {
      // Make sure validation is finished
      await AppFormUtils.waitWhilePending(this);

      // If invalid: Open the first tab in error
      if (this.invalid) {
        this.openFirstInvalidTab();
        return undefined;
      }

      // Continue (valid)
    }

    // Form is valid, but not saved
    if (this.dirty) {
      const saved = await this.save(new Event('save'));
      if (!saved) return undefined;
    }

    // Valid and saved data
    return this.data;
  }

  async delete(event?: Event): Promise<boolean> {
    if (this.loading || this.saving) return false;

    // Ask user confirmation
    const confirmation = await Alerts.askDeleteConfirmation(this.alertCtrl, this.translate);
    if (!confirmation) return;

    console.debug('[data-editor] Asking to delete...');

    this.markAsSaving();
    this.resetError();

    try {
      // Get data
      const data = await this.getValue();
      const isNew = this.isNewData;

      this.disable();

      if (!isNew) {
        await this.dataService.delete(data);
      }

      await this.onEntityDeleted(data);

      // Remove page history
      this.removePageHistory();
    } catch (err) {
      this.submitted = true;
      this.selectedTabIndex = 0;
      this.setError(err);
      this.markAsSaved();
      this.enable();
      return false;
    }

    // Wait, then go back (wait is need in order to update back href is need)
    await setTimeout(() => this.goBack(), 500);

    return true;
  }

  async reload() {
    this.markAsLoading();
    await this.load(this.data?.id, { fetchPolicy: 'network-only' });
  }

  async unload(opts?: { emitEvent?: boolean }): Promise<void> {
    await super.unload(opts);
    this.stopListenChanges();
    this.form.reset();
    this.registerForms();
    this.data = null;
    // TODO: find a way to remove current page from the navigation history
  }

  async goBack(event?: Event): Promise<void | boolean> {
    // Go back
    if (this.toolbar?.canGoBack) {
      return this.toolbar.goBack();
    } else if (this.defaultBackHref) {
      return this.navController.navigateBack(this.defaultBackHref);
    } else {
      // Back to home
      return this.navController.navigateBack('/');
    }
  }

  resetError(opts?: { emitEvent?: boolean }) {
    super.resetError(opts);
  }

  setError(err: string | AppErrorWithDetails, opts?: { emitEvent?: boolean; detailsCssClass?: string }) {
    if (!err) {
      super.setError(undefined, opts);
    } else if (typeof err === 'string') {
      console.error('[entity-editor] Error: ' + (err || ''));
      super.setError(err as string, opts);
    } else {
      console.error('[entity-editor] Error: ' + err.message || '', err);
      let userMessage = (err.message && this.translate.instant(err.message)) || err;

      // Add details error (if any) under the main message
      const detailMessage = !err.details || typeof err.details === 'string' ? (err.details as string) : err.details.message;
      if (isNotNilOrBlank(detailMessage)) {
        const cssClass = opts?.detailsCssClass || 'error-details';
        userMessage += `<br/><small class="${cssClass}" title="${detailMessage}">`;
        userMessage += detailMessage.length < 70 ? detailMessage : detailMessage.substring(0, 67) + '...';
        userMessage += '</small>';
      }
      super.setError(userMessage, opts);
    }
  }

  /**
   * Load data from the snapshot route
   *
   * @protected
   */
  protected loadFromRoute(): Promise<void> {
    const route = this.route.snapshot;
    if (!route || isNilOrBlank(this._pathIdAttribute)) {
      throw new Error("Unable to load from route: missing 'route' or 'options.pathIdAttribute'.");
    }
    let id = route.params[this._pathIdAttribute];
    if (isNil(id) || id === 'new') {
      return this.load(undefined, route.params);
    } else {
      // Convert as number, if need
      if (isInt(id)) {
        id = parseInt(id);
      }
      return this.load(id, route.params);
    }
  }

  /**
   * Return undefined to keep same tab
   *
   * @protected
   */
  protected computeNextTabIndex(): number | undefined {
    const nextTabIndex = this.selectedTabIndex + 1;
    return nextTabIndex < this.tabCount ? nextTabIndex : undefined;
  }

  protected async onBeforeEntityLoad(): Promise<void> {
    // can be overwritten by subclasses
  }

  protected async onNewEntity(data: T, options?: EntityServiceLoadOptions): Promise<void> {
    // can be overwritten by subclasses
  }

  protected async onEntityLoaded(data: T, options?: EntityServiceLoadOptions): Promise<void> {
    // can be overwritten by subclasses
  }

  protected async onEntitySaved(data: T): Promise<void> {
    // can be overwritten by subclasses
  }

  protected async onEntityDeleted(data: T): Promise<void> {
    // can be overwritten by subclasses
  }

  protected computeUsageMode(data: T): UsageMode {
    return this.settings.isUsageMode('FIELD') ? 'FIELD' : 'DESK';
  }

  protected waitWhilePending(opts?: WaitForOptions): Promise<void> {
    return AppFormUtils.waitWhilePending(this, opts);
  }

  async getValue(): Promise<T> {
    const json = await this.getJsonValueToSave();

    const res = new this.dataType();
    res.fromObject(json);

    return res;
  }

  protected getJsonValueToSave(): Promise<any> {
    return Promise.resolve(this.form.value);
  }

  /**
   * Compute the title
   *
   * @param data
   */
  protected async updateTitle(data?: T) {
    data = data || this.data;
    const title = await this.computeTitle(data);
    this.titleSubject.next(title);

    // If NOT data, then add to page history
    if (!this.isNewData) {
      const page = await this.computePageHistory(title);
      if (page) return this.addToPageHistory(page);
    }
  }

  protected async addToPageHistory(page: HistoryPageReference, opts?: AddToPageHistoryOptions) {
    if (!page) return; // Skip

    return this.settings.addToPageHistory(page, {
      removePathQueryParams: true,
      removeTitleSmallTag: true,
      emitEvent: false,
      ...opts,
    });
  }

  protected async computePageHistory(title: string): Promise<HistoryPageReference> {
    if (this.debug) console.debug('[entity-editor] Computing page history, using url: ' + this.router.url);
    return {
      title,
      path: this.router.url,
    };
  }

  protected async removePageHistory(opts?: { emitEvent?: boolean }) {
    return this.settings.removePageHistory(this.router.url, opts);
  }

  protected computePageUrl(id: ID | 'new'): string | any[] {
    const parentUrl = this.getParentPageUrl();
    return parentUrl && `${parentUrl}/${id}`;
  }

  protected getParentPageUrl(withQueryParams?: boolean) {
    let parentUrl = this.defaultBackHref;

    // Remove query params
    if (withQueryParams !== true && parentUrl && parentUrl.indexOf('?') !== -1) {
      parentUrl = parentUrl.substring(0, parentUrl.indexOf('?'));
    }

    return parentUrl;
  }

  protected listenChanges(id: ID, opts?: any): Observable<T | undefined> {
    return this.dataService.listenChanges(this.data.id, {
      // Make sure to skip cache, because NOT the full graph will be fetched by subscription
      // When a changes occur, we call reload() to force refetching the full graph
      fetchPolicy: 'no-cache',
      ...opts,
    });
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  /**
   * Update the route, without reloading the component
   *
   * @param data
   * @param queryParams
   * @protected
   */
  protected async updateRoute(data?: T, queryParams?: any): Promise<boolean> {
    data = data || this.data;
    if (!data || !this.route) return false;

    const currId = this.route.snapshot.paramMap.get(this.pathIdAttribute);
    const futureId = isNotNil(data.id) ? data.id.toString() : 'new';
    if (queryParams) {
      this.queryParams = {
        ...this.queryParams,
        ...queryParams,
      };
    }
    if (currId === futureId) {
      // Update queryParam only (not the path)
      // /!\ We should get query Params from the updated route, and not from snapshot that can be outdated
      const actualQueryParams = await firstNotNilPromise(this.route.queryParams);
      if (!equals(actualQueryParams, this.queryParams)) {
        if (this.debug) console.debug(`${this._logPrefix}Updating route using queryParams: `, this.queryParams);
        return await this.router.navigate(['.'], {
          relativeTo: this.route,
          replaceUrl: false,
          queryParams: this.queryParams,
          state: { animated: false },
        });
      }
    } else {
      const path = this.computePageUrl(isNotNil(data.id) ? data.id : 'new');
      const commands: any[] = path && typeof path === 'string' ? path.split('/').slice(1) : (path as any[]);
      if (isNotEmptyArray(commands)) {
        // Current route was '/new' => change to '/new?id=:id' (to allow CustomReuseStrategy to detect that route can be reused)
        if (currId === 'new' && this.queryParams[this.pathIdAttribute] !== futureId) {
          if (this.debug) console.debug(`${this._logPrefix}Updating route /new into /:id, using queryParams: `, queryParams);
          this.queryParams[this.pathIdAttribute] = futureId;
          const res = await this.router.navigate(['.'], {
            relativeTo: this.route,
            replaceUrl: true,
            queryParams: this.queryParams,
            state: { animated: false },
          });
          if (!res) return false;
        }

        if (this.debug) console.debug(`${this._logPrefix}Updating route to: ` + path);
        return await this.router.navigate(commands, {
          replaceUrl: true,
          queryParams: this.queryParams,
          state: { animated: false },
        });
      }
      return Promise.reject('Missing page URL');
    }
  }

  /* -- private functions -- */

  /**
   * Open the first tab that is invalid
   */
  private openFirstInvalidTab() {
    const invalidTabIndex = this.getFirstInvalidTabIndex();
    if (invalidTabIndex !== -1 && this.selectedTabIndex !== invalidTabIndex) {
      this.selectedTabIndex = invalidTabIndex;
      this.markForCheck();
    }
  }

  /* -- protected abstract methods (to override) -- */

  protected abstract computeTitle(data: T): Promise<string>;

  protected abstract get form(): FormGroup;

  protected abstract getFirstInvalidTabIndex(): number;

  protected abstract registerForms(): void;

  abstract setValue(data: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }): Promise<void> | void;
}
