import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { EntityMetadataComponent } from './entity-metadata.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxJdenticonModule } from 'ngx-jdenticon';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild(), NgxJdenticonModule],

  declarations: [
    // Other components
    EntityMetadataComponent,
  ],
  exports: [
    TranslateModule,
    // Components
    EntityMetadataComponent,
  ],
})
export class AppEntityFormModule {}
