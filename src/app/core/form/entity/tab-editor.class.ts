import { Directive, EventEmitter, Input, OnDestroy, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { AlertController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { HammerSwipeEvent } from '../../../shared/gesture/hammer.utils';
import { isNotNil, toNumber } from '../../../shared/functions';
import { AppEditor, IAppEditor } from './editor.class';

export interface IAppTabEditor<T = any> extends IAppEditor<T> {
  selectedTabIndex: number;
  setSelectedTabIndex(value: number, opts?: { emitEvent?: boolean; realignInkBar?: boolean }): void;
  unload(opts?: { emitEvent?: boolean }): Promise<void>;
  realignInkBar(): void;
}

export class AppTabEditorOptions {
  /**
   * Number of tab. 1 by default
   */
  tabCount?: number;

  /**
   * '200ms' by default
   */
  tabGroupAnimationDuration?: string;

  /**
   * Should enable swipe between tab, on touch screen
   */
  enableSwipe?: boolean; // true by default
}

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppTabEditor<T = any, ID = number, O = any> extends AppEditor<T, ID, O> implements IAppTabEditor<T>, OnInit, OnDestroy {
  protected _selectedTabIndex = 0;

  // Editor options
  protected tabCount: number;
  protected enableSwipe: boolean;
  protected tabGroupAnimationDuration: string;

  @Input() queryTabIndexParamName: string;

  @Input() set selectedTabIndex(value: number) {
    this.setSelectedTabIndex(value);
  }
  get selectedTabIndex(): number {
    return this._selectedTabIndex;
  }

  @Output() readonly selectedTabIndexChange = new EventEmitter<number>();

  @ViewChild('tabGroup', { static: true }) tabGroup: MatTabGroup;

  protected constructor(
    @Optional() route: ActivatedRoute, // Modal editor give 'null'
    router: Router,
    navController: NavController,
    alertCtrl: AlertController,
    translate: TranslateService,
    @Optional() options?: AppTabEditorOptions
  ) {
    super(route, router, navController, alertCtrl, translate);
    options = {
      tabCount: 1,
      enableSwipe: true,
      tabGroupAnimationDuration: '200ms',
      ...options,
    };
    this.tabCount = options.tabCount;
    this.enableSwipe = options.enableSwipe;
    this.tabGroupAnimationDuration = options.tabGroupAnimationDuration;
    this.queryTabIndexParamName = 'tab';
    this._logPrefix = '[tab-editor] ';
  }

  ngOnInit() {
    super.ngOnInit();

    // Read the selected tab index, from path query params
    if (this.tabGroup && this.route && this.queryTabIndexParamName) {
      this.registerSubscription(
        this.route.queryParams.subscribe((queryParams) => {
          this.queryParams = { ...queryParams };

          // Parse tab param
          if (this.tabCount > 1 && this.queryTabIndexParamName) {
            const tabIndex = toNumber(queryParams[this.queryTabIndexParamName], 0);
            this.queryParams[this.queryTabIndexParamName] = tabIndex !== 0 ? tabIndex : undefined;
            if (this.selectedTabIndex !== tabIndex) {
              this.selectedTabIndex = tabIndex;
            }
          }

          // Realign tab, after a delay because the tab can be disabled when component is created
          setTimeout(() => this.tabGroup.realignInkBar(), 500);
        })
      );
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.selectedTabIndexChange.complete();
  }

  setSelectedTabIndex(value: number, opts?: { emitEvent?: boolean; realignInkBar?: boolean }) {
    // Fix value
    if (value < 0) {
      value = 0;
    } else if (value > this.tabCount - 1) {
      console.warn(`[tab-editor] Invalid tab index ${value}. Max value is ${this.tabCount - 1}`);
      value = this._selectedTabIndex;
    }

    this._selectedTabIndex = value;
    this.selectedTabIndexChange.next(value);

    if (this.tabGroup && (!opts || opts.realignInkBar !== false)) {
      this.tabGroup.selectedIndex = value;
      this.markForCheck();
      // Wait tab change in UI, before realign ink tab
      setTimeout(() => this.tabGroup.realignInkBar(), 200);
    } else if (!opts || opts.emitEvent !== false) {
      this.markForCheck();
    }
  }

  realignInkBar() {
    this.tabGroup?.realignInkBar();
  }

  onTabChange(event: MatTabChangeEvent, queryTabIndexParamName?: string): boolean {
    queryTabIndexParamName = queryTabIndexParamName || this.queryTabIndexParamName;

    if (!queryTabIndexParamName || !this.route) return true; // Skip if tab query param not set, or no route

    if (this.debug) console.debug(this._logPrefix + 'onTabChange()', event);

    if (!this.queryParams || +this.queryParams[queryTabIndexParamName] !== event.index) {
      this.queryParams = this.queryParams || {};
      this.queryParams[queryTabIndexParamName] = event.index !== 0 ? event.index : undefined;

      if (queryTabIndexParamName === 'tab' && isNotNil(this.queryParams['subtab'])) {
        delete this.queryParams.subtab; // clean subtab
      }
      this.updateRoute();
      return true;
    }
    return false;
  }

  /**
   * Action triggered when user swipes
   */
  onSwipeTab(event: HammerSwipeEvent): boolean {
    // DEBUG
    // if (this.debug) console.debug("[tab-page] onSwipeTab()");

    // Skip, if not a valid swipe event
    if (
      !this.enableSwipe ||
      !event ||
      event.defaultPrevented ||
      (event.srcEvent && event.srcEvent.defaultPrevented) ||
      event.pointerType !== 'touch'
    ) {
      return false;
    }

    // DEBUG
    //if (this.debug)
    //console.debug("[tab-page] Detected swipe: " + event.type, event);

    let selectTabIndex = this.selectedTabIndex;
    switch (event.type) {
      // Open next tab
      case 'swipeleft':
        selectTabIndex = selectTabIndex >= this.tabCount - 1 ? 0 : selectTabIndex + 1;
        break;

      // Open previous tab
      case 'swiperight':
        selectTabIndex = selectTabIndex <= 0 ? this.tabCount : selectTabIndex - 1;
        break;

      // Other case
      default:
        console.error('[tab-page] Unknown swipe action: ' + event.type);
        return false;
    }

    setTimeout(() => {
      this.selectedTabIndex = selectTabIndex;
      this.markForCheck();
    });

    return true;
  }

  onSubTabChange(event: MatTabChangeEvent) {
    this.onTabChange(event, 'subtab');
  }

  async unload(opts?: { emitEvent?: boolean }): Promise<void> {
    this.selectedTabIndex = 0;
    await super.unload(opts);
  }

  /* -- protected functions -- */

  protected abstract getFirstInvalidTabIndex(): number;

  protected async updateRoute(): Promise<boolean> {
    if (!this.route) return false; // Skip if no route (e.g. in modal)
    return this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: this.queryParams,
      state: { animated: false },
    });
  }
}
