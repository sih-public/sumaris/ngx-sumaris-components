import { AfterViewInit, ChangeDetectorRef, Directive, Injector, Input, OnDestroy, OnInit, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';

import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { LocalSettingsService } from '../../services/local-settings.service';
import { Entity } from '../../services/model/entity.model';
import { UsageMode } from '../../services/model/settings.model';
import { FormGroup } from '@angular/forms';
import { AppTabEditor, AppTabEditorOptions } from './tab-editor.class';
import { AppFormUtils } from '../form.utils';
import { Alerts } from '../../../shared/alerts';
import { AppErrorWithDetails, ErrorCodes } from '../../services/errors';
import { isNil, isNotNil, isNotNilOrBlank } from '../../../shared/functions';
import { DateFormatService } from '../../../shared/pipes/date-format.pipe';
import { waitForFalse, WaitForOptions } from '../../../shared/observables';
import { FormErrorTranslator } from '../../../shared/validator/form-error-adapter.class';
import { debounceTime } from 'rxjs/operators';

export interface IEntityEditorModalOptions<T extends Entity<T, ID> = Entity<any, any>, ID = number> {
  // Data
  isNew: boolean;
  data: T;
  disabled: boolean;

  onAfterModalInit?: <M extends AppEntityEditorModal<T, ID>>(modal: M) => void | Promise<void>;
  onDelete?: (event: Event, data: T) => Promise<boolean>;

  usageMode: UsageMode;
  mobile: boolean;
  i18nSuffix?: string;
}

export class AppEntityEditorModalOptions extends AppTabEditorOptions {
  i18nPrefix?: string;
}

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppEntityEditorModal<T extends Entity<T, ID>, ID = number, O extends AppEntityEditorModalOptions = AppEntityEditorModalOptions>
  extends AppTabEditor<T, ID>
  implements OnInit, OnDestroy, AfterViewInit, IEntityEditorModalOptions<T, ID>
{
  protected _logPrefix: string = '';
  protected _isNew: boolean;

  protected readonly dateFormat: DateFormatService;
  protected readonly cd: ChangeDetectorRef;
  protected readonly settings: LocalSettingsService;
  protected readonly modalCtrl: ModalController;
  protected errorTranslator: FormErrorTranslator;

  readonly savingSubject = new BehaviorSubject<boolean>(false);

  readonly titleSubject = new BehaviorSubject<string>(undefined);

  @Input() data: T;
  @Input() mobile: boolean;
  @Input() usageMode: UsageMode;
  @Input() onAfterModalInit: <M extends AppEntityEditorModal<T, ID>>(modal: M) => void | Promise<void>;
  @Input() onDelete: (event: Event, data: T) => Promise<boolean>;
  @Input() i18nSuffix: string = null;

  /**
   * @deprecated
   */
  protected get $title(): Subject<string> {
    return this.titleSubject;
  }

  @Input() set isNew(value: boolean) {
    this._isNew = value;
  }

  get isNew(): boolean {
    return isNotNil(this._isNew) ? this._isNew : isNil(this.data?.id);
  }

  /**
   * @deprecated use isNew instead
   */
  get isNewData(): boolean {
    return this.isNew;
  }
  /**
   * @deprecated use isNew instead
   */
  @Input() set isNewData(value: boolean) {
    this.isNew = value;
  }

  @Input() set disabled(value: boolean) {
    this._enabled = !value;
  }

  get disabled(): boolean {
    return !this._enabled;
  }

  get isOnFieldMode(): boolean {
    return this.settings.isOnFieldMode(this.usageMode);
  }

  protected constructor(
    injector: Injector,
    protected dataType: new () => T,
    @Optional() options?: O
  ) {
    super(null, injector.get(Router), injector.get(NavController), injector.get(AlertController), injector.get(TranslateService), options);
    options = <O>{
      // Default options
      i18nPrefix: '',

      // Override defaults
      ...options,
    };

    this.queryTabIndexParamName = undefined; // Important: avoid query parameter changed
    this.settings = injector.get(LocalSettingsService);
    this.errorTranslator = injector.get(FormErrorTranslator);
    this.cd = injector.get(ChangeDetectorRef);
    this.dateFormat = injector.get(DateFormatService);
    this.modalCtrl = injector.get(ModalController);
    this.i18nContext = {
      prefix: options.i18nPrefix,
      suffix: this.i18nSuffix || '',
    };

    // FOR DEV ONLY ----
    //this.debug = !environment.production;
  }

  ngOnInit() {
    // Default values
    this.mobile = isNotNil(this.mobile) ? this.mobile : this.settings.mobile;
    this.usageMode = this.usageMode || this.settings.usageMode;

    super.ngOnInit();

    // Register forms
    this.registerForms();

    // Disable
    if (this.disabled) this.disable();

    if (this.isNew) {
      // Update the title.
      this.updateTitle();
    } else {
      // Update title each time value changes
      this.registerSubscription(this.form.valueChanges.pipe(debounceTime(250)).subscribe((json) => this.updateTitle(json)));
    }
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    if (this.onAfterModalInit) {
      Promise.resolve(this.onAfterModalInit(this)).then(() => this.load());
    } else {
      // Use a timeout to avoid error 'NG0100: ExpressionChangedAfterItHasBeenCheckedError'
      setTimeout(() => this.load());
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.titleSubject.complete();
    this.titleSubject.unsubscribe();
  }

  waitIdle(opts?: WaitForOptions): Promise<void> {
    // Wait end of saving
    if (this.saving) return waitForFalse(this.savingSubject);

    return super.waitIdle(opts);
  }

  async load() {
    this.markAsReady();
    this.markAsLoading();

    try {
      await this.updateView(this.data);
    } catch (err) {
      if (err === 'CANCELLED') return;
      this.setError(err);
      this.selectedTabIndex = 0;
    } finally {
      this.markAsLoaded();
    }
  }

  reload(): Promise<void> {
    if (this.dirty) {
      return this.load();
    }
  }

  unload(opts?: { emitEvent?: boolean }): Promise<void> {
    throw new Error('No implemented on modal');
  }

  async updateView(
    data: T,
    opts?: {
      emitEvent?: boolean;
      openTabIndex?: number;
    }
  ): Promise<void> {
    this.resetError();

    if (!data) throw { code: ErrorCodes.DATA_NOT_FOUND_ERROR, message: 'ERROR.DATA_NO_FOUND' };

    this.data = data;

    await this.setValue(data);

    if (!opts || opts.emitEvent !== false) {
      this.markAsPristine();
      this.markAsUntouched();
      this.updateViewState(data);

      // Update the title.
      this.updateTitle(data);
    }
  }

  /**
   * Enable or disable state
   */
  updateViewState(data: T, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.isNew || this.enabled) {
      this.enable(opts);
    } else {
      this.disable(opts);

      // Allow to sort table
      this.tables?.forEach((t) => t.enableSort());
      this.asyncTables?.forEach((t) => t.enableSort());
    }
  }

  async saveAndClose(event?: Event | Event): Promise<boolean> {
    const data = await this.saveAndGetDataIfValid();
    if (data) {
      this.modalCtrl.dismiss(data);
      return true;
    }
    return false;
  }

  async close(event: Event | Event): Promise<void> {
    if (this.dirty) {
      const saveBeforeLeave = await Alerts.askSaveBeforeLeave(this.alertCtrl, this.translate, event);

      // User cancelled
      if (isNil(saveBeforeLeave) || (event && event.defaultPrevented)) {
        return;
      }

      // Is user confirm: close normally
      if (saveBeforeLeave === true) {
        await this.saveAndClose(event);
        return;
      }
    }

    await this.modalCtrl.dismiss();
  }

  /**
   * Save the editor, by calling the dataService.save().
   * Ensure that editor if valid. If not, display an error
   *
   * @param event
   * @param opts
   */
  async save(event?: Event | Event, opts?: any): Promise<boolean> {
    if (this.loading || this.saving || this.disabled) {
      console.debug(this._logPrefix + 'Skip save: modal is busy (loading or saving)');
      return false;
    }
    if (!this.dirty) {
      console.debug(this._logPrefix + 'Skip save: modal not dirty');
      return true;
    }

    // Wait end of async validation
    await this.waitWhilePending();

    // If invalid
    if (this.invalid) {
      this.logFormErrors();
      this.setError('COMMON.FORM.HAS_ERROR');
      this.openFirstInvalidTab();
      this.scrollToTop();
      this.markAllAsTouched();

      this.submitted = true;
      return false;
    }

    this.markAsSaving();
    this.resetError();

    if (this.debug) console.debug(this._logPrefix + 'Saving data...');

    try {
      // Save all dirty children
      const saved = await this.saveDirtyChildren();
      if (!saved) return false;

      // Fill data
      this.data = await this.getValue();

      // Mark as submitted
      this.submitted = true;

      return true;
    } catch (err) {
      this.submitted = true;
      this.selectedTabIndex = 0; // Can by override by subclasses, using setError()
      this.setError(err);
      this.scrollToTop(); // Scroll to top (to show error)
      this.markAsDirty();
      this.enable();

      return false;
    } finally {
      this.markAsSaved();
    }
  }

  /**
   * Save data (if dirty and valid), and return it. Otherwise, return nil value.
   */
  async saveAndGetDataIfValid(): Promise<T | undefined> {
    // Form is not valid
    if (!this.valid) {
      // Make sure validation is finished
      await AppFormUtils.waitWhilePending(this);

      // If invalid: Open the first tab in error
      if (this.invalid) {
        this.openFirstInvalidTab();
        return undefined;
      }

      // Continue (valid)
    }

    // Form is valid, but not saved
    if (this.dirty) {
      const saved = await this.save(new Event('save'));
      if (!saved) return undefined;
    }

    // Valid and saved data
    return this.data;
  }

  async delete(event?: Event | Event) {
    if (this.loading || this.saving) return false;

    this.markAsSaving();
    this.resetError();
    try {
      // Delegate deletion to modal caller
      if (this.onDelete) {
        const result = await this.onDelete(event, this.data);

        // User cancelled
        if (isNil(result) || (event && event.defaultPrevented)) return;

        if (result) {
          await this.modalCtrl.dismiss(this.data, 'delete');
        }
      }

      // Or dismiss, with the role 'delete'
      else {
        if (this.isNew) {
          console.error(this._logPrefix + 'Trying to delete a new data. Make no sense!');
        }

        await this.modalCtrl.dismiss(this.data, 'delete');
      }
    } catch (err) {
      this.submitted = true;
      this.setError(err);
      this.selectedTabIndex = 0;
      this.markAsSaved();
      if (this.enabled) this.enable();
      return false;
    }
  }

  resetError(opts?: { emitEvent?: boolean }) {
    if (isNotNilOrBlank(this.error)) {
      this.error = null;
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  setError(err: string | AppErrorWithDetails, opts?: { emitEvent?: boolean; detailsCssClass?: string }) {
    if (!err) {
      super.setError(undefined, opts);
    } else if (typeof err === 'string') {
      console.error('[entity-editor] Error: ' + (err || ''));
      super.setError(err as string, opts);
    } else {
      console.error('[entity-editor] Error: ' + err.message || '', err);
      let userMessage = (err.message && this.translate.instant(err.message)) || err;

      // Add details error (if any) under the main message
      const detailMessage = !err.details || typeof err.details === 'string' ? (err.details as string) : err.details.message;
      if (isNotNilOrBlank(detailMessage)) {
        const cssClass = opts?.detailsCssClass || 'error-details';
        userMessage += `<br/><small class="${cssClass}" title="${detailMessage}">`;
        userMessage += detailMessage.length < 70 ? detailMessage : detailMessage.substring(0, 67) + '...';
        userMessage += '</small>';
      }
      super.setError(userMessage, opts);
    }
  }

  /* -- abstract methods (to override) -- */

  abstract setValue(data: T): Promise<void> | void;

  protected abstract registerForms(): void;

  protected abstract computeTitle(data: T): Promise<string>;

  protected abstract get form(): FormGroup;

  protected abstract getFirstInvalidTabIndex(): number;

  /* -- protected methods -- */

  protected waitWhilePending(opts?: WaitForOptions): Promise<void> {
    return AppFormUtils.waitWhilePending(this, opts);
  }

  protected async getValue(): Promise<T> {
    const json = await this.getJsonValueToSave();

    const res = new this.dataType();
    res.fromObject(json);

    return res;
  }

  protected getJsonValueToSave(): Promise<any> {
    return Promise.resolve(this.form.value);
  }

  /**
   * Compute the title
   *
   * @param data
   */
  protected async updateTitle(data?: T) {
    data = data || this.data;
    const title = await this.computeTitle(data);
    this.titleSubject.next(title);
  }

  protected async scrollToTop(duration?: number) {
    if (this.content) {
      return this.content.scrollToTop(duration);
    }
  }

  protected markForCheck() {
    this.cd?.markForCheck();
  }

  /* -- private functions -- */

  /**
   * Open the first tab that is invalid
   */
  private openFirstInvalidTab() {
    const invalidTabIndex = this.getFirstInvalidTabIndex();
    if (invalidTabIndex !== -1 && this.selectedTabIndex !== invalidTabIndex) {
      this.selectedTabIndex = invalidTabIndex;
      this.markForCheck();
    }
  }
}
