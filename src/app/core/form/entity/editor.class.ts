import { AfterViewInit, Directive, OnDestroy, OnInit, Optional, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AlertController, IonContent, NavController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { AppTable } from '../../table/table.class';
import { AppForm } from '../form.class';
import { FormButtonsBarToken } from '../buttons/form-buttons-bar.component';
import { AppFormUtils, CanLeave, CanSave, IAppForm, IAppFormGetter } from '../form.utils';
import { ShowToastOptions, Toasts } from '../../../shared/toast/toasts';
import { ToolbarToken } from '../../../shared/toolbar/toolbar';
import { toNumber } from '../../../shared/functions';
import { throttleTime } from 'rxjs/operators';
import { AppFormContainer, IAppFormContainer } from '../form-container.class';

export interface IAppEditor<T> extends IAppFormContainer<T>, CanSave, CanLeave {
  save(event?: Event, options?: any): Promise<boolean>;
  cancel(event?: Event): Promise<void>;
}

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppEditor<T = any, ID = number, O = any>
  extends AppFormContainer<T>
  implements IAppEditor<T>, OnInit, OnDestroy, AfterViewInit
{
  protected toastController: ToastController;

  readonly savingSubject = new BehaviorSubject<boolean>(false);

  submitted = false;
  queryParams: Params;
  i18nContext: {
    prefix: string;
    suffix: string;
  };

  get saving(): boolean {
    return this.savingSubject.value || this.editors?.some((e) => e.saving);
  }

  @ViewChild(ToolbarToken) toolbar: ToolbarToken | null = null;
  @ViewChild(FormButtonsBarToken, { static: true }) formButtonsBar: FormButtonsBarToken | null = null;
  @ViewChild(IonContent, { static: true }) content: IonContent;

  abstract get isNewData(): boolean;

  protected constructor(
    @Optional() protected route: ActivatedRoute, // Modal editor give 'null'
    protected router: Router,
    protected navController: NavController,
    protected alertCtrl: AlertController,
    protected translate: TranslateService
  ) {
    super();
    this._logPrefix = '[editor] ';
  }

  ngOnInit() {
    super.ngOnInit();

    if (this.formButtonsBar) {
      this.registerSubscription(
        this.formButtonsBar.onBack
          .pipe(throttleTime(200)) // Avoid to be called twice
          .subscribe((event) => this.goBack(event))
      );
    }
  }

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method
  ngAfterViewInit() {
    // Can be overridden by subclasses
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.savingSubject.unsubscribe();
  }

  /**
   * @deprecated Use addForm(path, form)
   * @param form
   */
  addChildForm(form: IAppForm | IAppFormGetter) {
    this.addForm(null, form);
  }

  /**
   * @deprecated Use addForms()
   * @param forms
   */
  addChildForms(forms: (IAppForm | IAppFormGetter)[]) {
    this.addForms(forms);
  }

  /**
   * @deprecated Use removeForm()
   * @param form
   */
  removeChildForm(form: IAppForm | IAppFormGetter): IAppForm | IAppFormGetter {
    return this.removeForm(form);
  }

  markAsSaving(opts?: { emitEvent?: boolean }) {
    if (this.savingSubject.value !== true) {
      this.savingSubject.next(true);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  markAsSaved(opts?: { emitEvent?: boolean }) {
    if (!this.savingSubject.closed && this.savingSubject.value !== false) {
      this.savingSubject.next(false);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  async cancel(event?: Event): Promise<void> {
    if (this.isNewData) {
      await this.unload(); // async reset
    } else if (this.dirty) {
      await this.reload(); // async reload
    }
  }

  /**
   * Unload the page (remove all data). Useful when reusing angular cache a cancelled page
   *
   * @param opts
   */
  async unload(opts?: { emitEvent?: boolean }): Promise<void> {
    console.debug(this._logPrefix + 'Unloading data...');
    this.markAsLoading();
    this.children?.forEach((f) => {
      if (f instanceof AppForm) {
        // Fix error when 'null' is pass (default value = {} was not applied)
        f.reset(undefined, opts);
      } else if (f instanceof AppTable) {
        f.dataSource.disconnect();
      } else if (f instanceof AppEditor) {
        f.unload(opts);
      }
    });

    this.markAsPristine(opts);
    this.markAsSaved(opts);
  }

  async reloadWithConfirmation(confirm?: boolean) {
    const needConfirm = this.dirty;
    // if not confirm yet: ask confirmation
    if (!confirm && needConfirm) {
      const translations = this.translate.instant([
        'COMMON.BTN_ABORT_CHANGES',
        'COMMON.BTN_CANCEL',
        'CONFIRM.CANCEL_CHANGES',
        'CONFIRM.ALERT_HEADER',
      ]);
      const alert = await this.alertCtrl.create({
        header: translations['CONFIRM.ALERT_HEADER'],
        message: translations['CONFIRM.CANCEL_CHANGES'],
        buttons: [
          {
            text: translations['COMMON.BTN_ABORT_CHANGES'],
            cssClass: 'secondary',
            handler: () => {
              confirm = true; // update upper value
            },
          },
          {
            text: translations['COMMON.BTN_CANCEL'],
            role: 'cancel',
            handler: () => {},
          },
        ],
      });
      await alert.present();
      await alert.onDidDismiss();
    }

    // If confirm: execute the reload
    if (confirm || !needConfirm) {
      this.scrollToTop();
      this.disable();
      return await this.reload();
    }
  }

  async goBack(event?: Event): Promise<void | boolean> {
    // Go back
    if (this.toolbar?.canGoBack) {
      return this.toolbar.goBack();
    } else {
      // Back to home
      return this.navController.navigateBack('/');
    }
  }

  abstract load(id?: ID, options?: O): Promise<void>;

  abstract save(event?: Event, options?: any): Promise<boolean>;

  abstract reload(): Promise<void>;

  /* -- protected methods -- */

  protected async scrollToTop(duration?: number) {
    duration = toNumber(duration, 500);

    if (!this.content) {
      console.warn(`[tab-editor] Cannot scroll to top. (no 'ion-content' tag found ${this.constructor.name}`);

      // TODO: FIXME (not working as the page is not the window)
      const scrollToTop = window.setInterval(() => {
        const pos = window.scrollY;
        if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
        } else {
          window.clearInterval(scrollToTop);
        }
      }, 16);
      return;
    }

    return this.content.scrollToTop(duration);
  }

  protected async saveIfDirtyAndConfirm(
    event?: Event,
    opts?: {
      emitEvent?: boolean;
      confirmed?: boolean;
    }
  ): Promise<boolean> {
    if (!this.dirty) return true;

    let confirmed = opts?.confirmed || false;
    if (!confirmed) {
      let cancel = false;
      const translations = this.translate.instant([
        'COMMON.BTN_SAVE',
        'COMMON.BTN_CANCEL',
        'COMMON.BTN_ABORT_CHANGES',
        'CONFIRM.SAVE',
        'CONFIRM.ALERT_HEADER',
      ]);
      const alert = await this.alertCtrl.create({
        header: translations['CONFIRM.ALERT_HEADER'],
        message: translations['CONFIRM.SAVE'],
        buttons: [
          {
            text: translations['COMMON.BTN_CANCEL'],
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              cancel = true;
            },
          },
          {
            text: translations['COMMON.BTN_ABORT_CHANGES'],
            cssClass: 'secondary',
            handler: () => {},
          },
          {
            text: translations['COMMON.BTN_SAVE'],
            handler: () => {
              confirmed = true; // update upper value
            },
          },
        ],
      });
      await alert.present();
      await alert.onDidDismiss();

      if (!confirmed) {
        return !cancel;
      }
    }

    return await this.save(event, opts);
  }

  protected async saveDirtyChildren(): Promise<boolean> {
    // Save all dirty tables
    const dirtyChildren = this.children.filter((c) => c.dirty).filter(AppFormUtils.canSave);
    const results = await Promise.all(dirtyChildren.map((c) => c.save()));
    return !results.some((saved) => saved !== true);
  }

  protected async showToast(opts: ShowToastOptions) {
    if (!this.toastController) throw new Error("Missing toastController in component's constructor");
    await Toasts.show(this.toastController, this.translate, opts);
  }
}
