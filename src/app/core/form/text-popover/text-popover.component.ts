import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PopoverController, PopoverOptions } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { TextForm } from '../../../shared/material/text/text-form.component';
import { logFormErrors, waitWhilePending } from '../../../shared/forms';
import { ValidatorFn } from '@angular/forms';
import { Color, OverlayEventDetail } from '@ionic/core';
import { isNotEmptyArray, isNotNilOrBlank, toBoolean } from '../../../shared/functions';

export interface TextPopoverOptions {
  title?: string;
  text?: string;
  placeholder?: string;
  editing?: boolean;
  multiline?: boolean;
  autoHeight?: boolean;
  maxLength?: number;
  autofocus?: boolean;
  validator?: ValidatorFn;
  // Header stuff
  showHeader?: boolean;
  headerColor?: Color;
  headerButtons?: TextPopoverButton[];
  // Footer stuff
  showFooter?: boolean;
}

export interface TextPopoverButton {
  text?: string;
  icon?: string;
  color?: Color;
  fill?: 'outline' | 'clear' | 'solid';
  side?: 'start' | 'end';
  role?: 'cancel' | string;
  cssClass?: string | string[];
  handler?: (value: string) => boolean | void | Promise<boolean | void>;
}

@Component({
  selector: 'app-text-popover',
  templateUrl: './text-popover.component.html',
  styleUrls: ['./text-popover.component.scss'],
})
export class TextPopover implements OnInit, AfterViewInit, OnDestroy, TextPopoverOptions {
  static async show(
    ctrl: PopoverController,
    event: Event,
    opts: TextPopoverOptions,
    popoverOpts?: Partial<PopoverOptions>
  ): Promise<OverlayEventDetail> {
    const modal = await ctrl.create({
      event,
      component: TextPopover,
      componentProps: opts,
      ...popoverOpts,
    });

    await modal.present();
    return await modal.onDidDismiss();
  }

  @ViewChild('form', { static: true }) form: TextForm;

  @Input() showHeader: boolean;
  @Input() headerColor: Color = null;
  @Input() showFooter: boolean;
  @Input() title: string = null;
  @Input() text: string;
  @Input() placeholder = '';
  @Input() editing = true;
  @Input() multiline = true;
  @Input() autoHeight = false;
  @Input() maxLength = 2000;
  @Input() autofocus = false;
  @Input() validator: ValidatorFn = null;
  @Input() headerButtons: TextPopoverButton[] = null;
  @Input() mobile = false;

  subscription = new Subscription();

  get disabled() {
    return this.form.disabled;
  }

  get enabled() {
    return this.form.enabled;
  }

  get valid() {
    return this.form?.valid || false;
  }

  get value() {
    return this.form?.value.text;
  }

  constructor(
    protected popoverController: PopoverController,
    protected translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.showHeader = toBoolean(this.showHeader, isNotNilOrBlank(this.title) || isNotEmptyArray(this.headerButtons));
    this.showFooter = toBoolean(this.showFooter, !this.mobile);
    this.enable();
  }

  ngAfterViewInit() {
    setTimeout(() => this.form.setValue({ text: this.text }), 0);
  }

  async onValidate(event: any): Promise<any> {
    await waitWhilePending(this.form);

    if (!this.valid) {
      logFormErrors(this.form.textControl);
      return;
    }

    try {
      const value = this.value;
      this.disable();
      this.form.error = null;
      await this.popoverController.dismiss(value);
    } catch (err) {
      this.form.error = (err && err.message) || err;
      this.enable();
    }
  }

  disable() {
    this.form.disable();
  }

  enable() {
    if (this.editing) this.form.enable();
  }

  cancel() {
    this.popoverController.dismiss();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  protected async clickButton(button: TextPopoverButton) {
    if (typeof button.handler === 'function') {
      const result = await button.handler(this.value);
      // TODO comment interpreter le true ?? => fermer ou annuler l'action ?
      if (result === true) {
        return this.popoverController.dismiss(this.value, button.role);
      }
    } else {
      return this.popoverController.dismiss(this.value, button.role);
    }
  }

  protected isStartSide(button: TextPopoverButton) {
    return button?.side === 'start';
  }

  protected isEndSide(button: TextPopoverButton) {
    return button?.side !== 'start';
  }
}
