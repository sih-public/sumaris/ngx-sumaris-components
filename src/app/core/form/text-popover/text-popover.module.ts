import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { TextPopover } from './text-popover.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],

  declarations: [
    // Components
    TextPopover,
  ],
  exports: [
    TranslateModule,
    // Components
    TextPopover,
  ],
})
export class AppTextPopoverModule {}
