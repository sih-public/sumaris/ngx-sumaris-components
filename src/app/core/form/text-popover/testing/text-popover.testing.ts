import { Component } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { TextPopover, TextPopoverOptions } from '../text-popover.component';
import { TranslateService } from '@ngx-translate/core';
import { PlatformService } from '../../../services/platform.service';

@Component({
  selector: 'text-popover-testing',
  templateUrl: 'text-popover.testing.html',
})
export class TextPopoverTestingPage {
  constructor(
    private popoverController: PopoverController,
    private toastController: ToastController,
    private translate: TranslateService,
    private platform: PlatformService
  ) {}

  async showPopover(event: Event, opts?: Partial<TextPopoverOptions>) {
    const modal = await this.popoverController.create({
      component: TextPopover,
      componentProps: <TextPopoverOptions>{
        title: 'modalTitle',
        headerColor: 'primary',
        placeholder: 'your text',
        ...opts,
      },
      backdropDismiss: false,
      keyboardClose: false,
      event,
      translucent: true,
      cssClass: 'popover-large',
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (data) {
      console.info('Popover result: ', data);
    }
  }

  protected async showSharePopoverWithButtons(event: UIEvent, opts?: Partial<TextPopoverOptions>) {
    opts = {
      ...opts,
      text:
        opts?.multiline !== false
          ? 'Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit'
          : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      editing: false,
      headerButtons: [
        {
          icon: 'copy',
          text: 'Copy',
          fill: 'outline',
          side: 'end',
          handler: async (value: string) => {
            await this.platform.copyToClipboard(
              {
                label: opts?.title,
                string: value,
              },
              {
                message: 'INFO.COPY_SUCCEED',
              }
            );

            return false; // Avoid dismiss
          },
        },
      ],
    };

    return this.showPopover(event, opts);
  }
}
