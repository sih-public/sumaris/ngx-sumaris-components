import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TextPopoverTestingPage } from './text-popover.testing';

const routes: Routes = [
  {
    path: 'text-popover',
    pathMatch: 'full',
    component: TextPopoverTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [TextPopoverTestingPage],
  exports: [RouterModule, TextPopoverTestingPage],
})
export class TextPopoverTestingModule {}
