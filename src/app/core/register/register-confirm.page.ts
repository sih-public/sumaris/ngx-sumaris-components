import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../services/account.service';
import { getRandomImageWithCredit } from '../home/home';
import { ConfigService } from '../services/config.service';
import { PlatformService } from '../services/platform.service';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { isNotEmptyArray } from '../../shared/functions';

@Component({
  selector: 'app-register-confirm-page',
  templateUrl: 'register-confirm.page.html',
  styleUrls: ['./register-confirm.page.scss'],
})
export class RegisterConfirmPage implements OnDestroy {
  subscriptions: Subscription[] = [];

  isLogin: boolean;
  loading = true;
  error: string;
  email: string;
  contentStyle = {};

  constructor(
    private platform: PlatformService,
    private accountService: AccountService,
    private activatedRoute: ActivatedRoute,
    private configService: ConfigService
  ) {
    this.platform.ready().then(() => {
      this.isLogin = accountService.isLogin();

      // Subscriptions
      this.subscriptions.push(this.accountService.onLogin.subscribe((_) => (this.isLogin = true)));
      this.subscriptions.push(this.accountService.onLogout.subscribe(() => (this.isLogin = false)));
      this.subscriptions.push(this.activatedRoute.paramMap.subscribe((params) => this.doConfirm(params.get('email'), params.get('code'))));

      this.configService.config.subscribe((config) => {
        if (isNotEmptyArray(config?.backgroundImages)) {
          const { image } = getRandomImageWithCredit(config.backgroundImages);
          this.contentStyle = { 'background-image': `url(${image})` };
        } else {
          const primaryColor = config.getProperty(CORE_CONFIG_OPTIONS.COLOR_PRIMARY) || 'var(--ion-color-primary)';
          this.contentStyle = { 'background-color': primaryColor };
        }

        this.loading = false;
      });
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
    this.subscriptions = [];
  }

  async doConfirm(email: string, code: string) {
    this.email = email;

    if (!code) {
      this.loading = false;
      this.error = undefined;
      return;
    }

    try {
      if (this.accountService.isLogin()) {
        const account = this.accountService.account;
        const emailAccount = account && account.email;
        if (email !== emailAccount) {
          // Not same email => logout, then retry
          await this.accountService.logout();
          return this.doConfirm(email, code);
        }
      }

      // Send the confirmation code
      const confirmed = await this.accountService.confirmEmail(email, code);
      if (confirmed && this.isLogin) {
        await this.accountService.reload();
      }
      this.loading = false;
      //this.location.replaceState("/confirm/" + email + "/");
    } catch (err) {
      this.error = (err && err.message) || err;
      this.loading = false;
    }
  }
}
