import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RegisterConfirmPage } from './register-confirm.page';
import { RegisterForm } from './register.form';
import { RegisterModal } from './register.modal';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, RouterModule, TranslateModule.forChild()],

  declarations: [RegisterForm, RegisterModal, RegisterConfirmPage],
  exports: [TranslateModule, RegisterForm, RegisterModal, RegisterConfirmPage],
})
export class AppRegisterModule {}
