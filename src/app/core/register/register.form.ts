import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { AccountService, RegisterData } from '../services/account.service';
import { Account } from '../services/model/account.model';
import { referentialToString } from '../services/model/referential.model';
import { MatStepper } from '@angular/material/stepper';
import { delay, Observable, of, Subscription } from 'rxjs';
import { AccountValidatorService } from '../services/validator/account.validator';
import { FormFieldDefinition } from '../../shared/form/field.model';
import { mergeMap } from 'rxjs/operators';
import { LocalSettingsService } from '../services/local-settings.service';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { MatAutocompleteConfigHolder } from '../../shared/material/autocomplete/material.autocomplete.config';

@Component({
  selector: 'app-register-form',
  templateUrl: 'register.form.html',
  styleUrls: ['./register.form.scss'],
})
export class RegisterForm implements OnInit {
  protected debug = false;

  autocompleteHelper: MatAutocompleteConfigHolder;
  additionalFields: FormFieldDefinition[];
  form: UntypedFormGroup;
  forms: UntypedFormGroup[];
  subscriptions: Subscription[] = [];
  error: string;
  sending = false;

  showPwd = false;
  showConfirmPwd = false;

  @ViewChild('stepper', { static: true }) private stepper: MatStepper;

  @Output()
  onCancel = new EventEmitter<any>();

  @Output()
  onSubmit = new EventEmitter<RegisterData>();

  constructor(
    private accountService: AccountService,
    private accountValidatorService: AccountValidatorService,
    public formBuilder: UntypedFormBuilder,
    @Inject(ENVIRONMENT) protected environment: Environment,
    protected settings?: LocalSettingsService
  ) {
    this.forms = [];
    // Email form
    this.forms.push(
      formBuilder.group({
        email: new UntypedFormControl(null, Validators.compose([Validators.required, Validators.email]), this.emailAvailability(this.accountService)),
        confirmEmail: new UntypedFormControl(null, Validators.compose([Validators.required, this.equalsValidator('email')])),
      })
    );

    // Password form
    this.forms.push(
      formBuilder.group({
        password: new UntypedFormControl(null, Validators.compose([Validators.required, Validators.minLength(8)])),
        confirmPassword: new UntypedFormControl(null, Validators.compose([Validators.required, this.equalsValidator('password')])),
      })
    );

    // Detail form
    const formDetailDef = {
      lastName: new UntypedFormControl(null, Validators.compose([Validators.required, Validators.minLength(2)])),
      firstName: new UntypedFormControl(null, Validators.compose([Validators.required, Validators.minLength(2)])),
    };

    // Prepare autocomplete settings
    this.autocompleteHelper = new MatAutocompleteConfigHolder(
      settings && {
        getUserAttributes: (a, b) => settings.getFieldDisplayAttributes(a, b),
      }
    );

    // Add additional fields to details form
    this.additionalFields = this.accountService.additionalFields
      // Keep only required fields
      .filter((field) => field.extra && field.extra.registration && field.extra.registration.required);
    this.additionalFields.forEach((field) => {
      //if (this.debug) console.debug("[register-form] Add additional field {" + field.name + "} to form", field);
      formDetailDef[field.key] = new UntypedFormControl(null, this.accountValidatorService.getValidators(field));

      if (field.type === 'entity') {
        field.autocomplete = this.autocompleteHelper.add(field.key, field.autocomplete);
      }
    });

    this.forms.push(formBuilder.group(formDetailDef));

    this.form = formBuilder.group({
      emailStep: this.forms[0],
      passwordStep: this.forms[1],
      detailsStep: this.forms[2],
    });
  }

  ngOnInit() {
    // For DEV only ------------------------
    if (!this.environment.production) {
      this.form.setValue({
        emailStep: {
          email: 'contact@e-is.pro',
          confirmEmail: 'contact@e-is.pro',
        },
        passwordStep: {
          password: 'contactera',
          confirmPassword: 'contactera',
        },
        detailsStep: {
          lastName: 'Lavenier 2',
          firstName: 'Benoit',
          department: null,
        },
      });
    }
  }

  get value(): RegisterData {
    const result: RegisterData = {
      username: this.form.value.emailStep.email,
      password: this.form.value.passwordStep.password,
      account: new Account(),
    };
    result.account.fromObject(this.form.value.detailsStep);
    result.account.email = result.username;

    return result;
  }

  get valid(): boolean {
    return this.form.valid;
  }

  isEnd(): boolean {
    return this.stepper.selectedIndex === 2;
  }

  isBeginning(): boolean {
    return this.stepper.selectedIndex === 0;
  }

  slidePrev() {
    return this.stepper.previous();
  }

  slideNext(event?: Event) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }
    return this.stepper.next();
  }

  equalsValidator(otherControlName: string): ValidatorFn {
    return function (c: AbstractControl): ValidationErrors | null {
      if (c.parent && c.value !== c.parent.value[otherControlName]) {
        return {
          equals: true,
        };
      }
      return null;
    };
  }

  emailAvailability(accountService: AccountService): AsyncValidatorFn {
    return function (control: AbstractControl): Observable<ValidationErrors | null> {
      return of(null).pipe(
        delay(500),
        mergeMap(() =>
          accountService
            .checkEmailAvailable(control.value)
            .then((res) => null)
            .catch((err) => {
              console.error(err);
              return { availability: true };
            })
        )
      );
    };
  }

  cancel() {
    this.onCancel.emit();
  }

  doSubmit(event?: any) {
    if (this.form.invalid) return;
    this.sending = true;
    this.onSubmit.emit(this.value);
  }

  referentialToString = referentialToString;

  markAsTouched() {
    this.form.markAsTouched();
  }

  disable() {
    this.form.disable();
  }

  enable() {
    this.form.enable();
  }
}
