import { Component, Inject, InjectionToken, OnDestroy, Optional } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ENVIRONMENT } from '../../../environments/environment.class';
import { capitalizeFirstLetter } from '../../shared/functions';
import { Department } from '../services/model/department.model';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '../services/config.service';
import { Configuration } from '../services/model/config.model';
import { Observable, Subscription } from 'rxjs';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';

export const APP_ABOUT_DEVELOPERS = new InjectionToken<Partial<Department>[]>('aboutDevelopers');
export const APP_ABOUT_PARTNERS = new InjectionToken<Partial<Department>[]>('aboutPartners');

@Component({
  selector: 'app-about-modal',
  templateUrl: './about.modal.html',
  styleUrls: ['./about.modal.scss'],
})
export class AboutModal implements OnDestroy {
  readonly name: string;
  readonly version: string;
  readonly sourceUrl: string;
  readonly config$: Observable<Configuration>;
  reportIssueUrl: string;
  forumUrl: string;
  helpUrl: string;

  private readonly _subscription = new Subscription();

  constructor(
    protected translate: TranslateService,
    protected modalController: ModalController,
    protected configService: ConfigService,
    @Optional() @Inject(ENVIRONMENT) protected environment,
    @Optional() @Inject(APP_ABOUT_DEVELOPERS) public developers: Partial<Department>[],
    @Optional() @Inject(APP_ABOUT_PARTNERS) public partners: Partial<Department>[]
  ) {
    this.name = translate.instant('APP_NAME');
    if (this.name === 'APP_NAME') {
      // Not translated: use name from the environnement
      this.name = (environment && environment.name && capitalizeFirstLetter(environment.name)) || undefined;
    }
    this.version = environment && environment.version;
    this.sourceUrl = environment && environment.sourceUrl;
    this.reportIssueUrl = environment && environment.reportIssueUrl;
    this.forumUrl = environment && environment.forumUrl;
    this.helpUrl = environment && environment.helpUrl;
    this.config$ = configService.config;

    this._subscription.add(this.config$.subscribe((config) => this.setConfig(config)));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  async close() {
    await this.modalController.dismiss();
  }

  /* -- protected functions -- */

  protected setConfig(config?: Configuration) {
    this.forumUrl = config?.getProperty(CORE_CONFIG_OPTIONS.FORUM_URL) || this.environment.forumUrl;
    this.helpUrl = config?.getProperty(CORE_CONFIG_OPTIONS.HELP_URL) || this.environment.helpUrl;
    this.reportIssueUrl = config?.getProperty(CORE_CONFIG_OPTIONS.REPORT_ISSUE_URL) || this.environment.reportIssueUrl;
  }
}
