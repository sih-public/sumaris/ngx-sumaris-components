import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { AboutModal } from './about.modal';
import { TranslateModule } from '@ngx-translate/core';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
  imports: [SharedModule, IonicModule, TranslateModule.forChild(), MarkdownModule],

  declarations: [AboutModal],
  exports: [TranslateModule, AboutModal],
})
export class AppAboutModalModule {}
