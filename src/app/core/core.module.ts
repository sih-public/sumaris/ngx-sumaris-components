import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { IonicStorageModule } from '@ionic/storage-angular';
import { AppGraphQLModule } from './graphql/graphql.module';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { CacheModule } from 'ionic-cache';
import { PlatformService } from './services/platform.service';
import { AppMenuModule } from './menu/menu.module';
import { AppIconModule } from './icon/icon.module';
import { AppSelectPeerModule } from './peer/select-peer.module';
import { AppInstallUpgradeCardModule } from './install/install-upgrade-card.module';
import { AppRegisterModule } from './register/register.module';
import { AppAuthModule } from './auth/auth.module';
import { AppHomePageModule } from './home/home.module';
import { AppAboutModalModule } from './about/about.module';
import { AppSettingsPageModule } from './settings/settings.module';
import { AppFormModule } from './form/form.module';
import { AppTableModule } from './table/table.module';
import { AppUpdateOfflineModeCardModule } from './offline/update-offline-mode-card.module';
import { AppAccountModule } from './account/account.module';
import { CorePipesModule } from './services/pipes/pipes.module';
import { AppChangePasswordModule } from './account/password/change-password.module';

@NgModule({ exports: [
        SharedModule,
        RouterModule,
        // Sub modules
        AppGraphQLModule,
        AppMenuModule,
        AppRegisterModule,
        AppChangePasswordModule,
        AppAuthModule,
        AppHomePageModule,
        AppAboutModalModule,
        AppSettingsPageModule,
        AppIconModule,
        AppSelectPeerModule,
        AppFormModule,
        AppTableModule,
        AppInstallUpgradeCardModule,
        AppUpdateOfflineModeCardModule,
        CorePipesModule,
    ], imports: [SharedModule,
        RouterModule,
        CacheModule,
        IonicStorageModule,
        // Sub modules
        AppGraphQLModule,
        AppMenuModule,
        AppIconModule,
        AppAccountModule,
        AppRegisterModule,
        AppAuthModule,
        AppHomePageModule,
        AppAboutModalModule,
        AppSettingsPageModule,
        AppSelectPeerModule,
        AppFormModule,
        AppTableModule,
        AppInstallUpgradeCardModule,
        AppUpdateOfflineModeCardModule,
        CorePipesModule], providers: [provideHttpClient(withInterceptorsFromDi())] })
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    console.info('[core] Creating module (root)');
    return {
      ngModule: CoreModule,
      providers: [PlatformService],
    };
  }
}
