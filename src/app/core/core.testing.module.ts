import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule, Routes } from '@angular/router';
import { TableTestingModule } from './table/testing/table.testing.module';
import { TableTestPage } from './table/testing/table.testing';
import { TextPopoverTestingModule } from './form/text-popover/testing/text-popover.testing.module';
import { TextPopoverTestingPage } from './form/text-popover/testing/text-popover.testing';
import { TestingPage } from '../shared/testing/tests.page';
import { PropertiesFormTestingModule } from './form/properties/testing/properties-form.testing.module';
import { PropertiesFormTestPage } from './form/properties/testing/properties-form.test';
import { Table2TestPage } from './table/testing/table2.testing';
import { ArrayFormTestPage } from './form/array/testing/form-array.test';
import { FormArrayTestModule } from './form/array/testing/form-array-test.module';

export const CORE_TESTING_PAGES: TestingPage[] = [
  { label: 'Core components', divider: true },
  { label: 'Table (click to edit)', page: '/testing/core/table' },
  { label: 'Table 2 (click to select)', page: '/testing/core/table2' },
  { label: 'Text popover', page: '/testing/core/text-popover' },
  { label: 'Properties form', page: '/testing/core/properties-form' },
  { label: 'Array form', page: '/testing/core/array-form' },
];

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'table',
  },
  {
    path: 'table',
    pathMatch: 'full',
    component: TableTestPage,
  },
  {
    path: 'table2',
    pathMatch: 'full',
    component: Table2TestPage,
  },
  {
    path: 'text-popover',
    pathMatch: 'full',
    component: TextPopoverTestingPage,
  },
  {
    path: 'properties-form',
    pathMatch: 'full',
    component: PropertiesFormTestPage,
  },
  {
    path: 'array-form',
    pathMatch: 'full',
    component: ArrayFormTestPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),

    // Sub modules
    TableTestingModule,
    TextPopoverTestingModule,
    PropertiesFormTestingModule,
    FormArrayTestModule,
  ],
  exports: [
    RouterModule,

    // Sub modules
    TableTestingModule,
    TextPopoverTestingModule,
    PropertiesFormTestingModule,
    FormArrayTestModule,
  ],
})
export class CoreTestingModule {}
