import { NgModule } from '@angular/core';
import { AppAuthForm } from './auth.form';
import { AppAuthModal } from './auth.modal';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AppRegisterModule } from '../register/register.module';
import { AppResetPasswordModal } from './reset-password.modal';
import { AppUsernameFormModule } from '../form/username/username.module';

@NgModule({
  imports: [
    SharedModule,
    TranslateModule.forChild(),

    // App modules
    AppRegisterModule,
    AppUsernameFormModule,
  ],

  declarations: [
    // Components
    AppAuthForm,
    AppAuthModal,
    AppResetPasswordModal,
  ],
  exports: [
    TranslateModule,

    // Components
    AppAuthForm,
    AppAuthModal,
    AppResetPasswordModal,
  ],
})
export class AppAuthModule {}
