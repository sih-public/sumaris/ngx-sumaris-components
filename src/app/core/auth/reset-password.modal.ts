import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AccountService } from '../services/account.service';
import { AppUsernameForm } from '../form/username/username.form';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { TranslateService } from '@ngx-translate/core';
import { slideUpDownAnimation } from '../../shared/material/material.animations';
import { filter } from 'rxjs/operators';
import { Subscription, TeardownLogic } from 'rxjs';

@Component({
  selector: 'app-reset-password-modal',
  templateUrl: 'reset-password.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation],
})
export class AppResetPasswordModal implements OnInit {
  protected readonly subscription = new Subscription();
  protected sending = false;

  @Input() username: string;
  @Input() usernamePlaceholder: string;
  @Input() emailValidator = true;

  @ViewChild('form', { static: true }) private form: AppUsernameForm;

  constructor(
    protected accountService: AccountService,
    protected viewCtrl: ModalController,
    protected toastController: ToastController,
    protected translate: TranslateService,
    protected cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.form.setValue({ username: this.username });
    this.form.enable();

    // Reset error if form changed
    this.registerSubscription(this.form.form.valueChanges.pipe(filter(() => !this.sending)).subscribe(() => (this.form.error = null)));
  }

  async cancel() {
    console.debug('[reset-password] cancelled');
    await this.viewCtrl.dismiss();
  }

  async doSubmit(event?: any) {
    if (this.sending) return;

    this.sending = true;
    const data = this.form.value;

    this.form.error = null;
    this.form.disable();

    try {
      console.debug(`[reset-password] Asking password reset for user '${data.username}' ...`);
      await this.accountService.sendResetPasswordEmail(data.username);
      await this.viewCtrl.dismiss();

      await this.showToast({ message: 'AUTH.CONFIRM.RESET_PASSWORD_SENT', type: 'info', showCloseButton: true });
    } catch (err) {
      console.error('[reset-password] Error while sending password reset email', err);
      this.form.error = err?.message || err;
      this.form.enable();
    } finally {
      this.sending = false;
      this.markForCheck();
    }
  }

  protected async showToast(opts: ShowToastOptions) {
    await Toasts.show(this.toastController, this.translate, opts);
  }

  protected registerSubscription(teardown: TeardownLogic) {
    this.subscription.add(teardown);
  }
  protected markForCheck() {
    this.cd.markForCheck();
  }
}
