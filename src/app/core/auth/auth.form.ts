import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Injector, OnInit, Optional, Output } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { RegisterModal } from '../register/register.modal';
import { AuthData } from '../services/account.service';
import { NetworkService } from '../services/network.service';
import { AuthTokenType } from '../services/network.types';
import { LocalSettingsService } from '../services/local-settings.service';
import { slideUpDownAnimation } from '../../shared/material/material.animations';
import { AppForm } from '../form/form.class';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { ConfigService } from '../services/config.service';
import { AppFormUtils } from '../form/form.utils';
import { AppResetPasswordModal } from './reset-password.modal';

@Component({
  selector: 'app-auth-form',
  templateUrl: 'auth.form.html',
  styleUrls: ['./auth.form.scss'],
  animations: [slideUpDownAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppAuthForm extends AppForm<AuthData> implements OnInit {
  protected readonly mobile: boolean;
  protected canWorkOffline = false;
  protected showPwd = false;
  protected canRegister: boolean;
  protected canResetPassword: boolean;
  protected usernamePlaceholder: string;

  @Output() onCancel = new EventEmitter<any>();
  @Output() onSubmit = new EventEmitter<AuthData>();

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.disable(opts);
    this.showPwd = false; // Hide pwd when disable (e.g. when submitted)
  }

  constructor(
    injector: Injector,
    formBuilder: UntypedFormBuilder,
    settings: LocalSettingsService,
    private configService: ConfigService,
    private modalCtrl: ModalController,
    public network: NetworkService,
    private cd: ChangeDetectorRef,
    @Optional() @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(
      injector,
      formBuilder.group({
        username: [null, Validators.required],
        password: [null, Validators.required],
        offline: [network.offline],
      })
    );

    this.mobile = settings.mobile;
    this.canWorkOffline = this.settings.hasOfflineFeature();
    this._enabled = true;
  }

  ngOnInit() {
    super.ngOnInit();

    // Load config, to set username's label and validator
    this.registerSubscription(
      this.configService.config.subscribe((config) => {
        // Set the username placeholder, with email or username
        const tokenType = config.getProperty(CORE_CONFIG_OPTIONS.AUTH_TOKEN_TYPE) as AuthTokenType;
        if (tokenType === 'token') {
          this.usernamePlaceholder = 'USER.EMAIL';
          this.form.get('username').setValidators(Validators.compose([Validators.required, Validators.email]));
        } else {
          this.usernamePlaceholder = 'USER.USERNAME';
          this.form.get('username').setValidators(Validators.required);
        }

        // Enable/disable registration link
        this.canRegister = config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.REGISTRATION_ENABLE);

        // Enable/Disable reset password link
        this.canResetPassword = tokenType == 'token' && config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.AUTH_RESET_PASSWORD_ENABLE);

        this.markForCheck();
      })
    );

    // For DEV only
    if (this.environment && this.environment.production === false && this.environment.defaultAuthValues) {
      // Set the default user, for testing.
      // Override using environment, if any
      this.form.patchValue(this.environment.defaultAuthValues);
    }
  }

  async cancel() {
    this.onCancel.emit();
  }

  async doSubmit(event?: Event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    if (this.loading) return;

    if (!this.form.valid) {
      await AppFormUtils.waitWhilePending(this.form);
      if (this.form.invalid) {
        AppFormUtils.logFormErrors(this.form);
        return; // Skip if invalid
      }
    }

    this.markAsLoading();
    const data = this.form.value;
    this.showPwd = false; // Hide password
    this.error = null; // Reset error

    setTimeout(() =>
      this.onSubmit.emit({
        username: data.username,
        password: data.password,
        offline: data.offline,
      })
    );
  }

  register() {
    this.onCancel.emit();
    setTimeout(async () => {
      const modal = await this.modalCtrl.create({
        component: RegisterModal,
        backdropDismiss: false,
      });
      return modal.present();
    }, 200);
  }

  resetPassword() {
    this.onCancel.emit();
    const data = this.form.value;

    setTimeout(async () => {
      const modal = await this.modalCtrl.create({
        component: AppResetPasswordModal,
        componentProps: {
          username: data.username || null,
          usernamePlaceholder: this.usernamePlaceholder,
        },
        backdropDismiss: false,
      });
      return modal.present();
    }, 200);
  }

  /* -- protected functions -- */

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
