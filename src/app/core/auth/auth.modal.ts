import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AccountService } from '../services/account.service';
import { firstNotNilPromise } from '../../shared/observables';
import { AppAuthForm } from './auth.form';

@Component({
  templateUrl: 'auth.modal.html',
  styleUrls: ['./auth.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppAuthModal implements OnInit {
  get loading() {
    return this.form?.loading;
  }

  @ViewChild('form', { static: true }) private form: AppAuthForm;

  constructor(
    private accountService: AccountService,
    private viewCtrl: ModalController,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.form.markAsReady({ emitEvent: false });
    this.form.markAsLoaded();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  async doSubmit(): Promise<any> {
    if (this.form.disabled) return;
    if (!this.form.valid) {
      this.form.markAllAsTouched();
      return;
    }
    this.markAsLoading();

    try {
      const data = this.form.value;

      // Disable the form
      this.form.disable();

      const account = await this.accountService.login(data);
      return this.viewCtrl.dismiss(account);
    } catch (err) {
      this.form.error = (err && err.message) || err;
      this.markAsLoaded();

      // Enable the form
      this.form.enable();

      // Reset form error on next changes
      firstNotNilPromise(this.form.form.valueChanges).then(() => {
        this.form.error = null;
        this.markForCheck();
      });

      return;
    }
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected markAsLoading(opts?: { emitEvent?: boolean }) {
    this.form.markAsLoading(opts);
  }

  protected markAsLoaded(opts?: { emitEvent?: boolean }) {
    this.form.markAsLoaded(opts);
  }
}
