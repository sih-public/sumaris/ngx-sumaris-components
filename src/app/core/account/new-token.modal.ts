import { AppEntityEditorModal, IEntityEditorModalOptions } from '../form/entity/entity-editor-modal.class';
import { TokenScope, UserToken } from '../services/model/token.model';
import { UntypedFormGroup } from '@angular/forms';
import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { AccountService } from '../services/account.service';
import { now } from 'moment';
import { fromUnixMsTimestamp } from '../../shared/dates';
import { NewTokenForm } from './new-token.form';

export interface INewTokenOptions extends IEntityEditorModalOptions<UserToken> {
  showScopes: boolean;
  tokenScopes: TokenScope[];
  existingNames: string[];
}

@Component({
  selector: 'app-new-token-modal',
  templateUrl: './new-token.modal.html',
})
export class NewTokenModal extends AppEntityEditorModal<UserToken> implements OnInit, INewTokenOptions {
  @Input() showScopes: boolean;
  @Input() tokenScopes: TokenScope[];
  @Input() existingNames: string[];
  @ViewChild(NewTokenForm, { static: true }) newTokenForm: NewTokenForm;

  constructor(
    injector: Injector,
    protected accountService: AccountService
  ) {
    super(injector, UserToken, { tabCount: 1 });
  }

  ngOnInit() {
    super.ngOnInit();

    this.enable();
  }

  protected computeTitle(data: UserToken): Promise<string> {
    return this.translate.instant('ACCOUNT.TOKENS.CREATE.TITLE');
  }

  protected get form(): UntypedFormGroup {
    return this.newTokenForm.form;
  }

  protected getFirstInvalidTabIndex(): number {
    return 0;
  }

  protected registerForms() {
    this.addForm(null, this.newTokenForm);
  }

  setValue(data: UserToken): Promise<void> | void {
    // Affect pubkey from current account
    data.pubkey = this.accountService.account.pubkey;

    // Default expiration date
    data.expirationDate = fromUnixMsTimestamp(now()).add(1, 'month');

    this.newTokenForm.setValue(data);
  }

  protected async getValue(): Promise<UserToken> {
    return this.newTokenForm.getValue();
  }
}
