import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Inject, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { combineLatest, Subscription } from 'rxjs';
import { AccountService } from '../services/account.service';
import { Account } from '../services/model/account.model';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AccountValidatorService } from '../services/validator/account.validator';
import { AppForm } from '../form/form.class';
import { AppFormUtils, CanLeave } from '../form/form.utils';
import { FormFieldDefinition } from '../../shared/form/field.model';
import { LAT_LONG_PATTERNS } from '../../shared/material/latlong/latlong.utils';
import { StatusIds } from '../services/model/model.enum';
import { APP_LOCALES, LocaleConfig, LocalSettings } from '../services/model/settings.model';
import { NetworkService } from '../services/network.service';
import { filter, map } from 'rxjs/operators';
import { MatTabGroup } from '@angular/material/tabs';
import { ConfigService } from '../services/config.service';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { AppPropertiesTable, PropertyEntity } from '../form/properties/properties.table';
import { PropertyMap } from '../../shared/types';
import { isNotEmptyArray, isNotNil } from '../../shared/functions';
import { UserTokenTable } from './token.table';
import { NavController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { Configuration } from '../services/model/config.model';

import { AuthTokenType } from '../services/network.types';

@Component({
  selector: 'app-account-page',
  templateUrl: 'account.page.html',
  styleUrls: ['./account.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountPage extends AppForm<Account> implements OnInit, OnDestroy, CanLeave {
  @ViewChild('tabGroup', { static: true }) tabGroup: MatTabGroup;
  @ViewChild('propertiesTable', { static: true }) propertiesTable: AppPropertiesTable;
  @ViewChild('tokensTable', { static: true }) tokensTable: UserTokenTable;

  protected selectedTabIndex = 0;
  protected isLogin: boolean;
  protected changesSubscription: Subscription;
  protected networkSubscription: Subscription;
  protected email = {
    confirmed: false,
    notConfirmed: false,
    sending: false,
    error: undefined,
  };
  protected additionalFields: FormFieldDefinition[];
  protected optionDefinitions: FormFieldDefinition[];

  get settingsForm(): UntypedFormGroup {
    return this.form.get('settings') as UntypedFormGroup;
  }

  // settingsContentForm: UntypedFormGroup;
  protected locales: LocaleConfig[];
  protected latLongFormats = LAT_LONG_PATTERNS;
  protected saving = false;
  protected submitted = false;
  protected accountReadOnly = false;
  protected readonly mobile: boolean;

  get valid(): boolean {
    return super.valid && (this.propertiesTable?.valid || true);
  }

  protected showLatLonFormat = true;
  protected canChangePassword = false;
  protected showSecurityDetails = false;
  protected showTechnicalDetails = true;
  protected showApiTokens = false;

  constructor(
    injector: Injector,
    public formBuilder: UntypedFormBuilder,
    public accountService: AccountService,
    public network: NetworkService,
    protected navController: NavController,
    protected validatorService: AccountValidatorService,
    // protected settingsValidatorService: UserSettingsValidatorService,
    protected configService: ConfigService,
    protected cd: ChangeDetectorRef,
    @Inject(APP_LOCALES) locales: LocaleConfig[]
  ) {
    super(injector, validatorService.getFormGroup(accountService.account));

    this.locales = locales;

    // Add settings fo form
    // this.settingsForm = settingsValidatorService.getFormGroup(accountService.account && accountService.account.settings);
    // this.settingsContentForm = (this.settingsForm.controls['content'] as UntypedFormGroup);
    // this.form.addControl('settings', this.settingsForm);

    this.mobile = this.settings.mobile;
    this.debug = !environment.production;

    // By default, disable the form
    this.disable();
  }

  ngOnInit() {
    super.ngOnInit();

    // Get user options
    this.optionDefinitions = (this.accountService.optionDefs || []).filter(
      (definition) => !definition.disabled && (!definition.minProfile || this.accountService.hasMinProfile(definition.minProfile))
    );

    // Observed some events
    this.registerSubscription(this.accountService.onLogin.pipe(filter(() => !this.saving)).subscribe((account) => this.onLogin(account)));
    this.registerSubscription(this.accountService.onLogout.subscribe(() => this.onLogout()));
    this.registerSubscription(
      this.onCancel.subscribe(() => {
        this.setValue(this.accountService.account);
        this.markAsPristine();
      })
    );

    this.registerSubscription(
      this.accountService.$additionalFields.subscribe((additionalFields) => {
        this.additionalFields = additionalFields.slice(); // Copy
        if (this.accountService.isLogin()) {
          this.onLogin(this.accountService.account);
        } else {
          this.markAsLoaded();
        }
      })
    );

    this.registerSubscription(this.configService.config.subscribe((config) => this.onConfigLoaded(config)));

    // Realign tab, after a delay because the tab can be disabled when component is created
    setTimeout(() => this.tabGroup.realignInkBar(), 500);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.stopListenChanges();
    this.networkSubscription?.unsubscribe();
  }

  onLogin(account: Account) {
    console.debug('[account] Logged account: ', account);
    this.isLogin = true;

    this.setValue(account);

    this.email.confirmed = account && account.email && account.statusId !== StatusIds.TEMPORARY;
    this.email.notConfirmed = account && account.email && (!account.statusId || account.statusId === StatusIds.TEMPORARY);

    this.networkSubscription?.unsubscribe();
    this.networkSubscription = combineLatest([
      this.configService.config,
      this.network.onNetworkStatusChanges.pipe(map((connectionType) => connectionType !== 'none')),
    ]).subscribe(([config, online]) => {
      this.onConfigLoaded(config);

      if (online) {
        this.enable();
        this.markAsPristine();
        this.startListenChanges();
      } else {
        this.disable();
        this.markAsPristine();
        this.stopListenChanges();
      }
      this.markAsLoaded();
    });

    this.markForCheck();
  }

  onLogout() {
    this.isLogin = false;
    this.email.confirmed = false;
    this.email.notConfirmed = false;
    this.email.sending = false;
    this.email.error = undefined;
    this.form.reset();
    this.disable();

    this.networkSubscription?.unsubscribe();

    this.stopListenChanges();

    // Back to home
    this.navController.navigateRoot('/');
  }

  async setValue(data: Account, opts?: { emitEvent?: boolean; onlySelf?: boolean }): Promise<void> {
    super.setValue(data, opts);

    // Set options
    if (isNotEmptyArray(this.optionDefinitions) && this.propertiesTable) {
      const properties = data?.settings?.asLocalSettings()?.properties || {};
      const propertiesEntities: PropertyEntity[] = [];
      this.optionDefinitions
        .map((definition) => definition.key)
        .forEach((key) => {
          if (isNotNil(properties[key])) {
            propertiesEntities.push(PropertyEntity.fromObject({ id: key, value: properties[key] }));
          }
        });
      await this.propertiesTable.ready();
      this.propertiesTable.value = propertiesEntities;
    }

    if (this.tokensTable) {
      await this.tokensTable.ready();
      this.tokensTable.value = data.tokens;
    }
  }

  async refresh(_?: Event) {
    this.disable();
    this.markAsLoading();

    return this.accountService.reload();
  }

  startListenChanges() {
    if (this.changesSubscription) return; // already started
    this.changesSubscription = this.accountService.listenChanges();
  }

  stopListenChanges() {
    if (!this.changesSubscription) return;
    this.changesSubscription.unsubscribe();
    this.changesSubscription = undefined;
  }

  async sendConfirmationEmail(event: MouseEvent) {
    const json = this.form.value;
    json.email = this.form.controls['email'].value;
    if (!json.email || !this.email.notConfirmed) {
      event.preventDefault();
      return false;
    }

    this.email.sending = true;
    console.debug('[account] Sending confirmation email...');
    try {
      await this.accountService.sendConfirmationEmail(json.email, (json.settings && json.settings.locale) || this.translate.currentLang);
      console.debug('[account] Confirmation email sent.');
      this.email.sending = false;
    } catch (err) {
      this.email.sending = false;
      this.email.error = (err && err.message) || err;
    }
  }

  async save(event?: Event): Promise<boolean> {
    if (this.saving) return false;

    if (!this.form.valid) {
      await AppFormUtils.waitWhilePending(this.form);

      if (this.form.invalid) {
        AppFormUtils.logFormErrors(this.form);
        this.form.markAllAsTouched();
        return false;
      }
    }

    this.submitted = true;
    this.saving = true;
    this.error = undefined;

    const newAccount = Account.fromObject({
      ...this.accountService.account.asObject(),
      ...this.form.value,
    });

    if (isNotEmptyArray(this.optionDefinitions) && this.propertiesTable) {
      // merge properties
      await this.propertiesTable.save();
      const propertyEntities = this.propertiesTable.value || [];
      const properties: PropertyMap = propertyEntities.reduce((res, item) => {
        res[item.id] = item.value;
        return res;
      }, {});
      const newProperties = { ...newAccount.settings.content['properties'], ...properties };
      newAccount.settings.merge(<LocalSettings>{ properties: newProperties }, ['properties']);
    } else if (newAccount.settings.content['properties']) {
      // Clean properties
      delete newAccount.settings.content['properties'];
    }

    if (this.tokensTable) {
      await this.tokensTable.save();
      newAccount.tokens = this.tokensTable.value;
    }

    console.debug('[account] Saving account...', newAccount);
    try {
      this.disable();

      await this.accountService.save(newAccount);

      // Reload
      setTimeout(() => this.onLogin(this.accountService.account), 100);

      this.markAsPristine();
      return true;
    } catch (err) {
      console.error(err);
      this.error = (err && err.message) || err;
      return false;
    } finally {
      this.saving = false;
      this.enable();
    }
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    super.enable(opts);

    // Some fields are always disable
    this.form.controls.email.disable();
    this.form.controls.mainProfile.disable();
    this.form.controls.pubkey.disable();

    // Disable account fields if readonly by configuration
    const disabled = this.accountReadOnly;
    if (disabled) {
      this.form.controls.lastName.disable();
      this.form.controls.firstName.disable();
    }

    // Always disable some additional fields
    this.additionalFields.forEach((field) => {
      const control = this.form.controls[field.key];
      if (!control) return; // Skip

      const fieldDisabled = disabled || (field.extra?.account && field.extra.account.disabled) || false;
      if (fieldDisabled && control.enabled) {
        control.disable();
      }
      const required = (field.extra?.account && field.extra.account.required) || false;
      if (!field.required && required) {
        control.setValidators(control.validator ? Validators.compose([control.validator, Validators.required]) : Validators.required);
      }
    });
    this.markForCheck();
  }

  async cancel() {
    await super.cancel();
  }

  async close(_?: Event) {
    if (this.saving) return;

    // Back to home
    return this.navController.navigateRoot('/'); // back to home
  }

  @HostListener('window:beforeunload', ['$event'])
  handleRefresh(event: BeforeUnloadEvent): void {
    // Avoid to quit web browser page, without saving
    if (!this.mobile && this.dirty) {
      event.preventDefault(); // Avoid to quit without saving
    }
  }

  protected onConfigLoaded(config: Configuration) {
    console.debug('[account] Config loaded: ', config);
    this.accountReadOnly = config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.ACCOUNT_READONLY);
    this.showLatLonFormat =
      config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.ACCOUNT_LAT_LONG_FORMAT_ENABLE) &&
      // For compatibility with deprecated key
      config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.DEFAULT_LAT_LONG_FORMAT_ENABLED);

    const authTokenType = config.getProperty(CORE_CONFIG_OPTIONS.AUTH_TOKEN_TYPE) as AuthTokenType;
    this.canChangePassword = authTokenType === 'token';
    this.showSecurityDetails = this.canChangePassword;
    this.showApiTokens = config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.AUTH_API_TOKEN_ENABLED);
    this.markForCheck();
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected openChangePasswordPage() {
    this.navController.navigateForward('account/password');
  }
}
