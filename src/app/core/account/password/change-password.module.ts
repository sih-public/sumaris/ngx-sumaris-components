import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ChangePasswordForm } from './change-password.form';
import { AppChangePasswordPage } from './change-password.page';

@NgModule({
  imports: [SharedModule, RouterModule, TranslateModule.forChild()],

  declarations: [ChangePasswordForm, AppChangePasswordPage],
  exports: [TranslateModule, ChangePasswordForm, AppChangePasswordPage],
})
export class AppChangePasswordModule {}
