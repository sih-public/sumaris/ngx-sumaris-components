import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AccountService, AuthData } from '../../services/account.service';
import { ChangePasswordForm } from './change-password.form';
import { ActivatedRoute } from '@angular/router';
import { getRandomImageWithCredit } from '../../home/home';
import { ConfigService } from '../../services/config.service';
import { NavController, ToastController } from '@ionic/angular';
import { Configuration } from '../../services/model/config.model';
import { isNotEmptyArray, isNotNilOrBlank, nullIfNilOrBlank } from '../../../shared/functions';
import { CORE_CONFIG_OPTIONS } from '../../services/config/core.config';
import { AppFormContainer } from '../../form/form-container.class';
import { environment } from '../../../../environments/environment';
import { AuthTokenType } from '../../services/network.types';
import { ShowToastOptions, Toasts } from '../../../shared/toast/toasts';
import { TranslateService } from '@ngx-translate/core';
import { slideUpDownAnimation } from '../../../shared/material/material.animations';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-change-password-page',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation],
})
export class AppChangePasswordPage extends AppFormContainer implements OnInit, OnDestroy {
  protected token: string;
  protected username: string;
  protected contentStyle = {};
  protected sending = false;
  protected isLogin = false;

  protected usernamePlaceholder: string;
  protected emailValidator = false;

  @ViewChild('form', { static: true }) private form: ChangePasswordForm;

  constructor(
    protected configService: ConfigService,
    protected accountService: AccountService,
    protected toastController: ToastController,
    protected translate: TranslateService,
    protected navController: NavController,
    protected activatedRoute: ActivatedRoute,
    protected cd: ChangeDetectorRef
  ) {
    super();
    this.debug = !environment.production;

    // Get username  + token (from the route)
    this.username =
      nullIfNilOrBlank(this.activatedRoute.snapshot.params['email']) || nullIfNilOrBlank(this.activatedRoute.snapshot.params['username']);
    if (isNotNilOrBlank(this.username)) {
      this.token =
        nullIfNilOrBlank(this.activatedRoute.snapshot.params['token']) || nullIfNilOrBlank(this.activatedRoute.snapshot.queryParams['token']);
    }
    console.debug(`[password] username: ${this.username}, token: ${this.token}`);
  }

  async ngOnInit() {
    super.ngOnInit();

    // Wait account service to be ready
    await this.accountService.ready();

    this.isLogin = this.accountService.isLogin();
    if (this.isLogin) {
      this.username = this.username || this.accountService.account.email;
      this.markForCheck();
    }
    this.form.username = this.username;

    this.addForms([this.form]);

    this.registerSubscription(this.configService.config.subscribe((config) => this.onConfigReady(config)));

    // Reset error when form changes
    this.registerSubscription(this.form.form.valueChanges.pipe(filter(() => !this.sending)).subscribe(() => (this.form.error = null)));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  async onConfigReady(config: Configuration) {
    console.debug('[password] Config loaded', config);
    if (isNotEmptyArray(config?.backgroundImages)) {
      const { image } = getRandomImageWithCredit(config.backgroundImages);
      this.contentStyle = { '--ion-background-color': 'transparent', 'background-image': `url(${image})` };
    } else {
      const primaryColor = config.getProperty(CORE_CONFIG_OPTIONS.COLOR_PRIMARY) || 'var(--ion-color-primary)';
      this.contentStyle = { '--ion-background-color': primaryColor + ' !important' };
    }

    const tokenType = config.getProperty(CORE_CONFIG_OPTIONS.AUTH_TOKEN_TYPE) as AuthTokenType;
    if (tokenType === 'token') {
      this.usernamePlaceholder = 'USER.EMAIL';
      this.emailValidator = true;
    } else {
      this.usernamePlaceholder = 'USER.USERNAME';
      this.emailValidator = false;
    }

    this.markAsLoaded();
    this.enable();
  }

  async doSubmit(event?: Event) {
    if (event?.defaultPrevented) return;

    if (this.form.disabled || this.sending) return; // Skip

    this.sending = true;
    const { username, password } = this.form.value;

    this.form.error = null;
    this.form.disable();

    try {
      const authData: AuthData = { username, password };

      // Token exists : send confirmation
      if (isNotNilOrBlank(this.token)) {
        // Update pubkey, using the token
        const done = await this.accountService.resetPubkey(authData, this.token);
        if (done) {
          await this.navController.navigateRoot('/');

          await this.showToast({ message: 'AUTH.CONFIRM.PASSWORD_CHANGED', type: 'info' });
        }
      } else {
        // Update pubkey on the current account
        const done = await this.accountService.updatePubkey(authData);

        if (done) {
          await this.navController.navigateRoot('/account');

          await this.showToast({ message: 'AUTH.CONFIRM.PASSWORD_CHANGED', type: 'info' });
        }
      }
    } catch (err) {
      console.error('[password] Error while changing password', err);
      this.form.error = (err && err.message) || err;
      this.form.enable();
    } finally {
      this.sending = false;
      this.markForCheck();
    }
  }

  protected async showToast(opts: ShowToastOptions) {
    await Toasts.show(this.toastController, this.translate, opts);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
