import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { AppForm } from '../../form/form.class';
import { SharedValidators } from '../../../shared/validator/validators';
import { AuthData } from '../../services/account.service';

@Component({
  selector: 'app-change-password-form',
  templateUrl: 'change-password.form.html',
  styleUrls: ['./change-password.form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangePasswordForm extends AppForm<AuthData> implements OnInit {
  protected showPwd = false;
  protected showConfirmPwd = false;

  @Input() usernamePlaceholder: string;
  @Input() emailValidator = false;

  @Input() set username(value: string) {
    this.form.get('username').setValue(value);
  }

  get username(): string {
    return this.form.get('username').value;
  }

  constructor(
    injector: Injector,
    formBuilder: UntypedFormBuilder,
    private cd: ChangeDetectorRef
  ) {
    super(
      injector,
      formBuilder.group({
        username: new UntypedFormControl(null, Validators.required),
        password: new UntypedFormControl(null, Validators.compose([Validators.required, Validators.minLength(8)])),
        confirmPassword: new UntypedFormControl(null, Validators.compose([Validators.required, SharedValidators.equals('password')])),
      })
    );
  }

  ngOnInit() {
    super.ngOnInit();

    // Add/remove email validator
    const usernNameControl = this.form.get('username');
    if (this.emailValidator && !usernNameControl.hasValidator(Validators.email)) {
      usernNameControl.addValidators(Validators.email);
    } else if (!this.emailValidator && usernNameControl.hasValidator(Validators.email)) {
      usernNameControl.removeValidators(Validators.email);
    }
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
