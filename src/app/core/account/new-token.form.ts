import { TokenScope, UserToken } from '../services/model/token.model';
import { ChangeDetectorRef, Component, Injector, Input, OnInit } from '@angular/core';
import { UserTokenValidatorService } from '../services/validator/user-token.validator';
import { MatAutocompleteFieldConfig } from '../../shared/material/autocomplete/material.autocomplete.config';
import { AppForm } from '../form/form.class';
import { AccountService } from '../services/account.service';
import { PlatformService } from '../services/platform.service';
import { isNilOrBlank, isNotNilOrBlank } from '../../shared/functions';

@Component({
  selector: 'app-new-token-form',
  templateUrl: './new-token.form.html',
})
export class NewTokenForm extends AppForm<UserToken> implements OnInit {
  protected tokenConfig: MatAutocompleteFieldConfig;
  @Input() showScopes: boolean;
  @Input() tokenScopes: TokenScope[];
  @Input() existingNames: string[];
  @Input() mobile: boolean;

  private readonly platform: PlatformService;

  constructor(
    injector: Injector,
    protected validator: UserTokenValidatorService,
    protected accountService: AccountService,
    protected cd: ChangeDetectorRef
  ) {
    super(injector, validator.getFormGroup());
    this.platform = injector.get(PlatformService);
  }

  get valid(): boolean {
    return super.valid && isNotNilOrBlank(this.value?.token);
  }

  get invalid(): boolean {
    return super.invalid || isNilOrBlank(this.value?.token);
  }

  ngOnInit() {
    super.ngOnInit();

    this.tokenConfig = {
      items: this.tokenScopes.slice().map((value) => {
        value.name = this.translate.instant(value.name);
        return value;
      }),
      attributes: ['name'],
    };

    this.validator.updateFormGroup(this.form, this.existingNames);
  }

  async generate(event: UIEvent) {
    event?.stopPropagation();

    try {
      const token = await this.accountService.generateToken(this.getFlags(this.value.scopes));
      if (!token) {
        this.setError('No token generated !');
      }

      this.form.patchValue({ token });

      this.markForCheck();
    } catch (e) {
      this.setError(e);
    }
  }

  async copy(event: UIEvent) {
    event?.stopPropagation();
    await this.platform.copyToClipboard(this.value.token, {
      message: 'ACCOUNT.TOKENS.CREATE.COPIED',
    });
  }

  async getValue(): Promise<UserToken> {
    const value = await super.getValue();

    // Set flags from scopes
    value.flags = this.getFlags(value.scopes);

    return value;
  }

  protected getFlags(scopes: TokenScope[]): number {
    let flags = 0;
    scopes.forEach((scope) => {
      flags += scope.flag;
    });
    return flags;
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
