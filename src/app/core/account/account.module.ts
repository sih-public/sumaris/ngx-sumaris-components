import { NgModule } from '@angular/core';
import { AccountPage } from './account.page';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { AppFormButtonsBarModule } from '../form/buttons/form-buttons-bar.module';
import { AppPropertiesFormModule } from '../form/properties/properties.module';
import { AppTableModule } from '../table/table.module';
import { UserTokenTable } from './token.table';
import { NewTokenModal } from './new-token.modal';
import { AppChangePasswordModule } from './password/change-password.module';
import { NewTokenForm } from './new-token.form';
import { AppInstallUpgradeCardModule } from '../install/install-upgrade-card.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule.forChild(),

    // App modules
    AppFormButtonsBarModule,
    AppPropertiesFormModule,
    AppTableModule,

    // Sub modules
    AppChangePasswordModule,
    AppInstallUpgradeCardModule,
  ],
  declarations: [AccountPage, UserTokenTable, NewTokenForm, NewTokenModal],
  exports: [
    TranslateModule,
    // Components
    AccountPage,
    UserTokenTable,
    NewTokenForm,
    NewTokenModal,

    // Sub modules
    AppChangePasswordModule,
  ],
})
export class AppAccountModule {}
