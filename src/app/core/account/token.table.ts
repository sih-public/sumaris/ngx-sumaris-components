import { ChangeDetectionStrategy, Component, Inject, Injector, Input, OnInit, Optional } from '@angular/core';
import { ValidatorService } from '@e-is/ngx-material-table';
import { UserTokenValidatorService } from '../services/validator/user-token.validator';
import { TokenScope, UserToken } from '../services/model/token.model';
import { AppInMemoryTable } from '../table/memory-table.class';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '../table/table.model';
import { InMemoryEntitiesService } from '../../shared/services/memory-entity-service.class';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { APP_USER_TOKEN_SCOPES } from '../services/account.service';
import { INewTokenOptions, NewTokenModal } from './new-token.modal';
import { ConfigService } from '../services/config.service';
import { Configuration } from '../services/model/config.model';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { isNotEmptyArray, isNotNilOrBlank } from '../../shared/functions';

@Component({
  selector: 'app-user-token-table',
  templateUrl: 'token.table.html',
  styleUrls: ['token.table.scss'],
  providers: [{ provide: ValidatorService, useClass: UserTokenValidatorService }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserTokenTable extends AppInMemoryTable<UserToken> implements OnInit {
  @Input() useSticky = false;

  protected showScopes = false;
  protected defaultScope: string;
  protected tokenScopes: TokenScope[];

  constructor(
    injector: Injector,
    validatorService: ValidatorService,
    protected configService: ConfigService,
    @Inject(ENVIRONMENT) environment: Environment,
    @Optional() @Inject(APP_USER_TOKEN_SCOPES) tokenScopes: TokenScope[]
  ) {
    super(
      injector,
      [...RESERVED_START_COLUMNS, 'name', 'scopes', 'lastUsedDate', 'expirationDate', 'creationDate', ...RESERVED_END_COLUMNS],
      UserToken,
      new InMemoryEntitiesService(UserToken, undefined, {
        filterFnFactory: () => () => true,
        equals: UserToken.equals,
      }),
      validatorService
    );

    this.tokenScopes = tokenScopes ?? [];
    this.inlineEdition = false;
    this.allowRowDetail = false;
    this.defaultSortBy = 'creationDate';
    this.defaultSortDirection = 'asc';
    this.i18nColumnPrefix = 'ACCOUNT.TOKENS.TABLE.';
    this.debug = !environment?.production;

    if (!tokenScopes) {
      console.warn('No TokenScopes provided. Please provide the token APP_USER_TOKEN_SCOPES with valid TokenScope array');
    }
  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(this.configService.config.subscribe((config) => this.onConfigLoaded(config)));

    this.markAsReady();
  }

  get value(): UserToken[] {
    return super.value || [];
  }

  set value(data: UserToken[]) {
    super.value = data;
  }

  setValue(value: UserToken[], opts?: { emitEvent?: boolean }) {
    // Set scopes from flags
    value?.forEach((userToken) => {
      const scopes: TokenScope[] = [];
      if (userToken.flags) {
        this.tokenScopes.forEach((scope) => {
          // eslint-disable-next-line no-bitwise
          if (userToken.flags & scope.flag) {
            scopes.push(scope);
          }
        });
      }
      userToken.scopes = scopes;
    });

    super.setValue(value, opts);
  }

  async addToken(event: UIEvent) {
    if (event?.defaultPrevented) return; // Avoid multiple call

    event?.preventDefault();
    event?.stopPropagation();
    const newToken = new UserToken();
    if (isNotNilOrBlank(this.defaultScope) && isNotEmptyArray(this.tokenScopes)) {
      const scope = this.tokenScopes.find((scope) => scope.id === this.defaultScope);
      if (!scope) {
        console.warn("[token-table] Cannot found the default scope '${this.defaultScope}' in values of APP_USER_TOKEN_SCOPES!");
      }
      newToken.scopes = scope ? [scope] : [];
    }

    const modal = await this.modalCtrl.create({
      component: NewTokenModal,
      componentProps: <INewTokenOptions>{
        mobile: this.mobile,
        isNew: true,
        data: newToken,
        showScopes: this.showScopes,
        tokenScopes: this.tokenScopes || [],
        existingNames: this.value?.map((v) => v.name),
      },
      backdropDismiss: false,
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();

    if (data?.name) {
      // Add this token in the list
      const row = await this.addRowToTable(undefined, { editing: false });
      row.validator.patchValue(data);

      this.markAsDirty({ emitEvent: false });
      this.markForCheck();
    }
  }

  // async revocate(event: UIEvent) {
  //   if (this.selection.isEmpty()) return;
  //
  //   const confirmed = await Alerts.askConfirmation(this.i18nColumnPrefix + 'CONFIRM.REVOCATE', this.alertCtrl, this.translate);
  //   if (!confirmed) return;
  //
  //   const now = moment();
  //   this.selection.selected.forEach(row => {
  //     row.validator.patchValue({revocationDate: now}, {emitEvent:true});
  //     // row.validator.updateValueAndValidity();
  //   });
  //
  //   this.markForCheck();
  //   // this.markAsDirty();
  // }

  display(scopes: TokenScope[]) {
    return scopes?.map((value) => this.translate.instant(value.name)).join(', ');
  }

  protected onConfigLoaded(config: Configuration) {
    this.showScopes = config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.AUTH_API_TOKEN_SCOPE_ENABLED);
    this.setShowColumn('scopes', this.showScopes);
    this.defaultScope = config.getProperty(CORE_CONFIG_OPTIONS.DEFAULT_AUTH_API_TOKEN_SCOPE);
  }
}
