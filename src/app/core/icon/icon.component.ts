import { Component, Input } from '@angular/core';
import { AppColors, IconRef } from '../../shared/types';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
})
export class AppIconComponent {
  @Input() icon: string = null;
  @Input() matIcon: string = null;
  @Input() matSvgIcon: string = null;
  @Input() color: AppColors = null;
  @Input() height: number | string;
  @Input() width: number | string;

  @Input() set ref(value: IconRef) {
    this.icon = value.icon;
    this.matIcon = value.matIcon;
    this.matSvgIcon = value.matSvgIcon;
    this.color = value.color || this.color; // Keep existing, if not defined in the reference
  }
}
