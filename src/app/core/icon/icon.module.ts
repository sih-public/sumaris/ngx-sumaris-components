import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
// import ngx-translate and the http loader
import { TranslateModule } from '@ngx-translate/core';
import { AppIconComponent } from './icon.component';

@NgModule({
  imports: [SharedModule, TranslateModule],

  declarations: [AppIconComponent],
  exports: [AppIconComponent],
})
export class AppIconModule {}
