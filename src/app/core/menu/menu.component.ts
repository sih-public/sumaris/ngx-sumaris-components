import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnInit, Optional, ViewChild } from '@angular/core';
import { IonSplitPane, MenuController, ModalController, NavController } from '@ionic/angular';

import { Account } from '../services/model/account.model';
import { AccountService } from '../services/account.service';

import { fadeInSlowAnimation } from '../../shared/material/material.animations';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { DEFAULT_MENU_SHOW_WHEN, MenuService, SplitPaneShowWhen } from './menu.service';
import { IMenuItem, MenuItem } from './menu.model';
import { firstNotNilPromise } from '../../shared/observables';
import { ActivatedRoute, Router } from '@angular/router';
import { RxStrategyNames } from '@rx-angular/cdk/render-strategies';

@Component({
  selector: 'app-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [fadeInSlowAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent implements OnInit {
  @Input() id = 'menu';
  @Input() menuId = 'left';
  @Input() side = 'left';
  @Input() contentId = 'menu-content';
  @Input() logo: string;
  @Input() appName: string;
  @Input() rxStrategy: RxStrategyNames = 'normal';
  @Input() appVersion: string;

  @ViewChild('splitPane', { static: true }) splitPane: IonSplitPane;

  loading = true;
  isLogin = false;
  accountName: string;
  accountAvatar: string;
  accountEmail: string;

  private readonly _debug: boolean;
  private _subscription = new Subscription();

  constructor(
    protected accountService: AccountService,
    protected navController: NavController,
    protected menu: MenuController,
    protected modalCtrl: ModalController,
    protected translate: TranslateService,
    protected cd: ChangeDetectorRef,
    protected menuService: MenuService,
    protected router: Router,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() protected route: ActivatedRoute // Modal editor give 'null'
  ) {
    console.debug('[menu-component] Create');
    this.appVersion = environment.version;
    this._debug = !environment.production;
  }

  async ngOnInit() {
    this.splitPane.when = await firstNotNilPromise(this.menuService.splitPaneWhen);

    this._subscription.add(
      this.menuService.accountChanges.subscribe((account) => {
        if (account) this.onLogin(account);
        else this.onLogout(true);
      })
    );

    // Listen to menu service event
    this._subscription.add(this.menuService.splitPaneWhen.subscribe((value) => this.setSplitPaneWhen(value)));
    this._subscription.add(this.menuService.enabled.subscribe((value) => this.enable(value)));
    this._subscription.add(
      this.menuService.opened
        // Avoid duplicated events
        .pipe(distinctUntilChanged())
        .subscribe((value) => (value ? this.open({ emitEvent: false }) : this.close({ emitEvent: false })))
    );

    // TODO : Find a more reliable way to do this :
    //    Must be donne after menuService subscription :
    //    - In MenuService.ngOnStart accountChanges change will be emitted before the service is ready
    //    - This is required because subjectData be filled after get account change
    //      and MenuService await for subjectData be filled to be ready
    await this.menuService.ready();
  }

  async onLogin(account: Account) {
    console.info('[menu] Update (on login)');
    this.accountAvatar = account.avatar;
    this.accountName = account.displayName;
    this.accountEmail = account.email;
    this.isLogin = true;
    this.markAsLoaded({ emitEvent: false });
    this.markForCheck();
  }

  async onLogout(skipRedirect?: boolean) {
    if (!this.isLogin) {
      this.markAsLoaded();
      return; // Skip if already logout
    }

    if (!skipRedirect) {
      console.debug('[menu] Logout');
    } else {
      console.debug('[menu] Update (on logout)');
    }
    this.accountAvatar = null;
    this.accountEmail = null;
    this.accountName = null;

    // Wait the end of fadeout, to reset the account
    if (!skipRedirect) {
      await this.navController.navigateRoot('/');
    }

    this.isLogin = false;
    this.markAsLoaded({ emitEvent: false });
    this.markForCheck();
  }

  trackByFn(index: number, item: IMenuItem) {
    return item.id.toString();
  }

  async enable(value: boolean): Promise<void> {
    const enabled = await this.menu.isEnabled(this.menuId);
    if (enabled !== value) {
      await this.menu.enable(value, this.menuId);
      if (!value) await this.close();
    }
  }

  async open(opts?: { emitEvent?: boolean }): Promise<boolean> {
    console.debug('[menu] Checking open event...');
    const opened = await this.menu.isOpen(this.menuId);
    if (!opened) {
      console.debug('[menu] Opening menu');
      await this.menu.open(this.menuId);

      // Propagate to service
      if (!opts || opts.emitEvent !== false) {
        this.menuService._markAsOpened();
      }
    }
    return true;
  }

  async close(opts?: { emitEvent?: boolean }): Promise<boolean> {
    console.debug('[menu] Checking close event...');
    const opened = await this.menu.isOpen(this.menuId);
    if (opened) {
      console.debug('[menu] Closing menu');
      await this.menu.close(this.menuId);

      // Propagate to service
      if (!opts || opts.emitEvent !== false) {
        this.menuService._markAsClosed();
      }
    }
    return true;
  }

  async setSplitPaneWhen(value: SplitPaneShowWhen) {
    console.debug('[menu] Checking splitPane when...');
    if (this.splitPane.when !== value) {
      console.debug('[menu] Set splitPane when to ' + value);
      this.splitPane.when = value;
      this.detectChanges();

      // Propagate to the menu service
      setTimeout(() => this.menuService.setSplitPaneShowWhen(value), 200);
    }
  }

  toggleSplitPaneShow(event?: Event) {
    if (event && event.defaultPrevented) return;
    if (event) event.preventDefault();

    if (this.splitPane.when === false) {
      return this.setSplitPaneWhen(DEFAULT_MENU_SHOW_WHEN);
    } else {
      return this.setSplitPaneWhen(false);
    }
  }

  executeAction(event: Event, item: MenuItem) {
    this.close();

    // Delegate execution to service
    return this.menuService.executeAction(event, item);
  }

  onSwipeRight(event) {
    // Skip, if not a valid swipe event
    if (!event || event.pointerType !== 'touch' || event.velocity < 0.4) {
      //event.preventDefault();
      return false;
    }

    // Will open the left menu, so cancelled this swipe event
    const startX = event.center.x - event.distance;
    if (startX <= 50) {
      // DEBUG
      //console.debug("[menu] Cancel swipe right, because near the left menu {x: " + startX + ", velocity: " + event.velocity + "}");
      event.preventDefault();
      return false;
    }

    // OK: continue

    // DEBUG
    //console.debug("[menu] Received swipe right {x: " + startX + ", velocity: " + event.velocity + "}");
  }

  protected markAsLoaded(opts?: { emitEvent?: boolean }) {
    if (this.loading) {
      setTimeout(() => {
        this.loading = false;

        if (!opts || opts.emitEvent !== false) this.markForCheck();
      }, 450);
    }
  }

  protected markAsLoading(opts?: { emitEvent?: boolean }) {
    if (!this.loading) {
      this.loading = true;

      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  protected detectChanges() {
    this.cd.detectChanges();
  }

  protected markForCheck() {
    this.cd.detectChanges();
  }

  protected togglePinned(event: Event, item: IMenuItem) {
    event.preventDefault();
    event.stopPropagation();
    this.menuService.togglePinned(item);
  }
}
