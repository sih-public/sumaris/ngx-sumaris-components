import { EntityAsObjectOptions } from '../services/model/entity.model';
import { EntityClass } from '../services/model/entity.decorators';
import { EntityFilter } from '../services/model/filter.model';
import { MenuItem } from './menu.model';

import { FilterFn } from '../../shared/types';

// @dynamic
@EntityClass({ typename: 'MenuItemFilterVO' })
export class MenuItemFilter extends EntityFilter<MenuItemFilter, MenuItem> {
  static fromObject: (source: any, opts?: any) => MenuItemFilter;

  fromObject(source: any, opts?: any) {}

  asObject(opts?: EntityAsObjectOptions): any {
    const target = Object.assign({}, this);

    return target;
  }

  protected buildFilter(): FilterFn<MenuItem>[] {
    const filterFns = super.buildFilter();

    //

    return filterFns;
  }
}
