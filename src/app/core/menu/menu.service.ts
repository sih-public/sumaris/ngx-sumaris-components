import { EventEmitter, Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, combineLatest, from, merge, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, throttleTime } from 'rxjs/operators';
import { ENVIRONMENT, Environment } from '../../../environments/environment.class';
import { DateUtils } from '../../shared/dates';
import { isEmptyArray, isNil, isNotEmptyArray, isNotNil, isNotNilOrBlank, toBoolean } from '../../shared/functions';
import { firstNotNilPromise } from '../../shared/observables';
import { StartableObservableService } from '../../shared/services/startable-observable-service.class';
import { IconRef } from '../../shared/types';
import { AboutModal } from '../about/about.modal';
import { AccountService } from '../services/account.service';
import { ConfigService } from '../services/config.service';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { LocalSettingsService } from '../services/local-settings.service';
import { Account } from '../services/model/account.model';
import { Configuration } from '../services/model/config.model';
import { TreeItemEntityUtils } from '../services/model/tree-item-entity.model';
import { IMenuItem, MenuItem, MenuItems, MenuOptions } from './menu.model';

export type SplitPaneShowWhen = boolean | 'lg';
export const DEFAULT_MENU_SHOW_WHEN: SplitPaneShowWhen = 'lg';
export const APP_MENU_ITEMS = new InjectionToken<IMenuItem[]>('menuItems');
export const APP_MENU_OPTIONS = new InjectionToken<MenuOptions>('menuOptions');

export interface MenuPathParam {
  path: string;
  params: { [key: string]: string };
}

@Injectable({
  providedIn: 'root',
  deps: [ENVIRONMENT],
})
export class MenuService extends StartableObservableService<MenuItem[]> {
  readonly accountChanges = new BehaviorSubject<Account>(undefined);
  readonly onPinned = new EventEmitter<MenuItem>(true);

  private _staticItems: MenuItem[];
  private _detachedSubItems: MenuItem[] = [];
  private _itemCounter = 0;
  private readonly _options: MenuOptions;
  private readonly _logPrefix = '[menu-service] ';
  private readonly _$opened = new Subject<boolean>();
  private readonly _$splitPaneWhen = new BehaviorSubject<SplitPaneShowWhen>(DEFAULT_MENU_SHOW_WHEN);
  private readonly _$enabled = new BehaviorSubject<boolean>(true);
  private readonly _$enableSubMenu: BehaviorSubject<boolean>;

  constructor(
    protected settings: LocalSettingsService,
    protected configService: ConfigService,
    protected accountService: AccountService,
    protected router: Router,
    protected modalCtrl: ModalController,
    protected alertController: AlertController,
    protected navController: NavController,
    protected translate: TranslateService,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_MENU_OPTIONS) options?: MenuOptions,
    @Optional() @Inject(APP_MENU_ITEMS) staticItems?: IMenuItem[]
  ) {
    super(settings);
    this._options = {
      ...options,
      subMenu: {
        disableIcon: false,
        savePinned: true,
        ...(options?.subMenu || {}),
        // Force disable on mobile
        enable: options?.subMenu?.enable !== false && !this.settings.mobile,
      },
    };
    this._$enableSubMenu = new BehaviorSubject<boolean>(this._options.subMenu.enable);
    this._debug = !environment.production;

    this._detachedSubItems = [];
    this._staticItems = (staticItems || []).map((source) => {
      const target = this.fromObject({ static: true, ...source }, { computeId: true, fillParent: true, fillDefaultFromParent: true });

      const detachedSubItems = TreeItemEntityUtils.deleteRecursively(target, (item) => item.static !== true);
      if (detachedSubItems?.length) {
        detachedSubItems.forEach((item) => this._detachToParent(item));
        console.debug(this._logPrefix + 'Initial detached sub items', detachedSubItems);
        this._detachedSubItems.push(...detachedSubItems);
      }
      return target;
    });
  }

  get opened(): Observable<boolean> {
    return this._$opened.asObservable();
  }

  get splitPaneWhen(): Observable<SplitPaneShowWhen> {
    return this._$splitPaneWhen.asObservable();
  }

  get enabled(): Observable<boolean> {
    return this._$enabled.asObservable();
  }

  get subMenuEnabled(): boolean {
    return this._$enableSubMenu.value;
  }

  get subItems(): MenuItem[] {
    return (this.data || []).reduce((res, rootElement) => res.concat(rootElement.children), []);
  }

  private get _staticSubItems(): MenuItem[] {
    return (this._staticItems || []).reduce((res, rootElement) => res.concat(rootElement.children), []);
  }

  enable(value: boolean) {
    this._$enabled.next(value);
  }

  enableSubMenu(value: boolean) {
    this._$enableSubMenu.next(value);
  }

  setSplitPaneShowWhen(value: SplitPaneShowWhen) {
    if (this._$splitPaneWhen.value !== value) {
      this._$splitPaneWhen.next(value);
    }
  }

  toggleSplitPaneWhen() {
    if (this._$splitPaneWhen.value !== false) {
      this._$splitPaneWhen.next(false);
    } else {
      this._$splitPaneWhen.next(DEFAULT_MENU_SHOW_WHEN);
    }
  }

  fromObject(source: any, opts?: { withChildren?: boolean; computeId?: boolean; fillParent?: boolean; fillDefaultFromParent?: boolean }): MenuItem {
    const target = MenuItem.fromObject(source, { withChildren: false });
    // Compute id if need
    if (opts?.computeId && isNil(target.id)) target.id = this._computeNewId();

    // Remove icon if disabled
    if (this._options.subMenu.disableIcon) {
      Object.assign(target, <IconRef>{ icon: undefined, matIcon: undefined, matSvgIcon: undefined });
    }

    // Recursive
    target.children = (source.children || []).map((json: any) => this.fromObject(json, { ...opts, fillParent: false }));

    // Fill parent (once, when the full tree is ready)
    if (opts?.fillParent) {
      TreeItemEntityUtils.treeFillParent(target);

      // Fill defaults from the parent (once filled)
      if (opts?.fillDefaultFromParent) {
        TreeItemEntityUtils.visit(target, (item) => {
          if (!item.parent) return; // Skip if no parent
          item.parentPath = item.parentPath || item.parent.path;
          item.parentPathParams = item.parentPathParams || item.parent.pathParams;
          item.static = toBoolean(item.static, item.parent.static);
          // Compute path, from parent path + action
          if (!item.path && !!item.action) {
            item.path = item.parent.path;
            item.pathParams = typeof item.action === 'string' ? { action: item.action } : { actionId: item.id.toString(), actionTitle: item.title };
          }
        });
      }
    }

    return target;
  }

  createSubMenu(source: Partial<IMenuItem>): MenuItem {
    if (!this.subMenuEnabled) return null; // Skip if disabled

    const target = this.fromObject(source);

    // Check if exists
    if (this.started) {
      const existingItem = this._findExistingItem(target);
      if (existingItem) return existingItem;
    }

    // Create
    if (isNil(target.id)) target.id = this._computeNewId();

    return target;
  }

  async addSubMenuItems(sources: IMenuItem[], opts?: { skipIfExists?: boolean }) {
    if (!this.started) await this.ready();
    if (!this.subMenuEnabled) return; // Skip
    return sources.map((source) => this._addSubMenuItem(this.fromObject(source), opts));
  }

  async addSubMenuItem(item: IMenuItem, opts?: { skipIfExists?: boolean }) {
    if (!this.started) await this.ready();
    if (!this.subMenuEnabled) return; // Skip
    return this._addSubMenuItem(this.fromObject(item), opts);
  }

  async removeSubMenuItems(items: IMenuItem[]) {
    if (!this.started) await this.ready();
    return (items || []).map((item) => this._removeSubMenuItem(this.fromObject(item)));
  }

  async removeSubMenuItem(item: IMenuItem) {
    if (!this.started) await this.ready();
    return this._removeSubMenuItem(this.fromObject(item));
  }

  togglePinned(source: IMenuItem) {
    const item = source instanceof MenuItem ? source : this._findExistingItem(this.fromObject(source));

    // Skip if not found
    if (!item) {
      if (this._debug) console.warn(this._logPrefix + `Sub menu {${source.path}} cannot be found - unable to toggle pinned!`);
      return;
    }

    // Skip if static
    if (item.static) return;

    // Unpinned
    if (item.pinned) {
      item.pinned = false;
      if (!item.detached) {
        this._detachedSubItems = [...this._detachedSubItems, ...this._detachSubMenuItems()];
      }
    }

    // Pinned
    else {
      item.pinned = true;
    }

    this.onPinned.emit(item);
  }

  async executeAction(event: Event, item: MenuItem) {
    const action = item?.action;
    if (!action) return; // skip
    if (typeof action === 'string') {
      switch (action as string) {
        case 'logout':
          await this.logout();
          break;
        case 'about':
          await this.openAboutModal();
          break;
        case 'expand':
          await this.toggleExpandableItem(item);
          if (item.path) {
            await this.navController.navigateRoot(item.path, {
              queryParams: item.pathParams,
            });
          }
          break;
        default: {
          if (item.path) {
            await this.navController.navigateRoot(item.path, {
              queryParams: item.pathParams,
            });
          } else {
            throw new Error('Unknown action: ' + action);
          }
        }
      }
    } else if (typeof action === 'function') {
      (action as unknown as any)(event);
    }
  }

  async logout() {
    const translations = await this.translate.instant([
      'AUTH.LOGOUT.CONFIRM_TITLE',
      'AUTH.LOGOUT.CONFIRM_MESSAGE',
      'COMMON.BTN_CANCEL',
      'AUTH.LOGOUT.BTN_CONFIRM',
    ]);
    const alert = await this.alertController.create({
      header: translations['AUTH.LOGOUT.CONFIRM_TITLE'],
      message: translations['AUTH.LOGOUT.CONFIRM_MESSAGE'],
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: translations['AUTH.LOGOUT.BTN_CONFIRM'],
          cssClass: 'ion-color-primary',
          handler: async () => {
            // Back to home
            await this.navController.navigateRoot('/');

            // WARNING: logout should be done AFTER home redirection, because of cache clearing - see issue #5
            this.accountService.logout();
          },
        },
      ],
    });

    await alert.present();
  }

  async openAboutModal() {
    const modal = await this.modalCtrl.create({ component: AboutModal });
    return modal.present();
  }

  async toggleExpandableItem(item: MenuItem) {
    if (!item.expanded) {
      if (this._debug) console.debug(this._logPrefix + 'Expand item', item);

      item.matIcon = 'expand_less';

      // Try to attach some item
      this._detachedSubItems
        .filter((childItem) => !!this._findParent(childItem, [item]))
        .forEach((childItem) => this._attachMenuToParent(item, childItem));
    } else {
      item.matIcon = 'expand_more';

      this._detachedSubItems = [...this._detachedSubItems, ...this._detachSubMenuItems({ path: '/', params: {} }, item.children)];
    }
  }

  _markAsOpened() {
    this._$opened.next(true);
  }

  _markAsClosed() {
    this._$opened.next(false);
  }

  async close() {
    this._$opened.next(false);
  }

  async open() {
    this._$opened.next(true);
  }

  protected async ngOnStart() {
    console.info(`${this._logPrefix}Starting...`);

    // Restore sub items from settings
    await this.settings.ready();
    const settingsSubItems: IMenuItem[] = this.settings.getPageSettings('menu', 'sub-items');
    if (isNotEmptyArray(settingsSubItems)) {
      const detachedSubItems = settingsSubItems.map((source) => this.fromObject(source, { computeId: true, fillParent: true }));
      console.debug(this._logPrefix + 'Restoring pinned sub items from settings', detachedSubItems);

      // TODO check if previous sub menu (from menu token) are kept
      //this._detachedSubItems.push(...detachedSubItems);
      this._detachedSubItems = detachedSubItems;
    }

    const accountEvent$ = merge(
      from(this.accountService.ready()),
      this.accountService.onLogin,
      this.accountService.onLogout.pipe(map((_) => null))
    ).pipe(
      map((account) => (isNotNil(account?.id) ? account : null)),
      distinctUntilChanged((a1, a2) => DateUtils.isSame(a1?.updateDate, a2?.updateDate))
    );

    this.registerSubscription(
      // Combine config and account events
      combineLatest([this.configService.config, accountEvent$]).subscribe(([config, account]) => {
        const isLogin = this.accountService.isLogin();
        if (this._debug) console.debug(`${this._logPrefix}Received config or account event. Refreshing items... isLogin? `, isLogin);

        // Make sure to clear account, if not login
        account = isLogin ? account : null;

        // Load all items
        const items = this._loadMenuItems(config, account);

        // Emit new root items
        this.dataSubject.next(items);

        // Emit account changes event
        this.accountChanges.next(account);
      })
    );

    this.registerSubscription(
      merge(
        // Force refresh when login/logout
        this.accountChanges.pipe(map(() => MenuItems.parsePathWithQuery(this.router.routerState.snapshot.url))),
        // Start listening route
        this.startSubject.pipe(
          switchMap((_) => this.router.events),
          filter((event) => {
            return event instanceof NavigationEnd;
          }),
          map((event: NavigationEnd) => MenuItems.parsePathWithQuery(event.url))
        )
      )
        .pipe(throttleTime(250))
        .subscribe((pathParam) => {
          const detachedSubItems = this._detachSubMenuItems(pathParam);
          if (detachedSubItems?.length) {
            this._detachedSubItems = [...this._detachedSubItems, ...detachedSubItems];
            if (this._debug) console.debug(`${this._logPrefix}Newly detached items: `, detachedSubItems);
          }
        })
    );

    // Save pinned items
    this.registerSubscription(
      this.onPinned.subscribe((item) => {
        // Collect pinned elements, then serialize it (without id)
        const pinnedSubItems = this.subItems.filter((i) => i.isPinned()).map((item) => item.asObject({ withChildren: true, keepLocalId: false }));

        if (pinnedSubItems.length && this._debug)
          console.debug(this._logPrefix + `Saving ${pinnedSubItems.length} pinned items, in local settings`, pinnedSubItems);
        this.settings.savePageSetting('menu', pinnedSubItems, 'sub-items');
      })
    );

    return firstNotNilPromise(this.dataSubject);
  }

  private _loadMenuItems(config: Configuration | null, account: Account | null) {
    if (this._debug) console.debug(`${this._logPrefix}Loading menu items...`);

    // Save previous children of root items
    const detachedSubItems = (this.subItems || []).concat(this._staticSubItems || []).reduce((res, rootItem) => {
      const children = rootItem.children;
      if (isEmptyArray(children)) return res;
      // Detach all root children
      rootItem.children = [];
      children.forEach((child) => (child.parent = null));
      // Return root children
      return res.concat(children);
    }, this._detachedSubItems || []);

    // Reset base root items (clean items added by config)
    let items: MenuItem[] = this._staticItems.slice();

    // Concat config's items
    const configValue: string = config?.getProperty(CORE_CONFIG_OPTIONS.MENU_ITEMS);
    if (isNotNilOrBlank(configValue)) {
      try {
        const configItems = JSON.parse(configValue) as IMenuItem[];
        items = (configItems || []).reduce((res: MenuItem[], source) => {
          // Normalize the item
          const item = this.fromObject({ static: true, ...source }, { computeId: true });

          // Skip is already loaded
          if (res.find((i) => MenuItems.isSame(i, item))) return res;

          // Reuse existing id, if possible
          const existingItem = this.data?.find((i) => MenuItems.isSame(i, item));
          if (existingItem) {
            item.id = existingItem.id;
          }

          if (item.after) {
            const index = res.findIndex((i) => i.title === item.after);
            if (index !== -1) {
              return res
                .slice(0, index + 1)
                .concat(item)
                .concat(res.slice(index + 1));
            }
          } else if (item.before) {
            const index = res.findIndex((i) => i.title === item.before);
            if (index !== -1) {
              return res.slice(0, index).concat(item).concat(res.slice(index));
            }
          }
          return res.concat(item);
        }, items);
      } catch (err) {
        console.error(`${this._logPrefix}Invalid value for option '${CORE_CONFIG_OPTIONS.MENU_ITEMS.key}'. Expected an array of menu item`, err);
      }
    }

    // Filter item using account rights
    const opts = { debug: this._debug };
    items = items
      .filter((item) => MenuItems.checkIfVisible(item, account, config, opts))
      .map((item) => {
        // Replace title using a config property
        if (isNotNilOrBlank(item.titleProperty) && config) {
          item.title = config.properties[item.titleProperty] || item.title;
        }

        return item;
      });

    if (this._debug) console.debug(`${this._logPrefix}Found ${items.length} visible root items...`);

    // Re-attach sub menu items
    if (this.subMenuEnabled && detachedSubItems.length) {
      if (account) {
        this._addSubMenuItems(detachedSubItems, { skipIfExists: false, availableParents: detachedSubItems.concat(items) });
      } else {
        this._detachedSubItems = detachedSubItems;
      }
    } else {
      this._detachedSubItems = [];
    }

    return items;
  }

  private _addSubMenuItems(sources: MenuItem[], opts?: { skipIfExists?: boolean; availableParents?: MenuItem[] }) {
    return sources.map((source) => this._addSubMenuItem(source, opts));
  }

  private _addSubMenuItem(newItem: MenuItem, opts?: { skipIfExists?: boolean; availableParents?: MenuItem[] }) {
    opts = {
      skipIfExists: true,
      ...opts,
    };

    const pathParam = MenuItems.parsePathWithQuery(this.router.routerState.snapshot.url);

    // Find if item already exists
    const existingItem = this._findExistingItem(newItem, opts?.availableParents);
    if (existingItem) {
      // No changes
      if (opts.skipIfExists && MenuItems.isSame(existingItem, newItem) && existingItem.visible) {
        // DEBUG
        if (this._debug) console.debug(`${this._logPrefix}Skipping sub item (same visible item already exist)`, newItem);
        return existingItem;
      }

      if (this._debug) console.debug(`${this._logPrefix}Updating existing sub item #`, existingItem.id);

      // Replace existing item, but keep the existing id
      if (newItem !== existingItem) {
        newItem.id = existingItem.id;
        newItem.$children = existingItem.$children;
        newItem.pinned = existingItem.pinned;
        newItem.static = existingItem.static;
        if (newItem.pinned || newItem.static || existingItem.expanded) {
          // If loading, keep existing title if pinned (avoid to show skeleton)
          newItem.title = newItem.title || existingItem.title;
        } else {
          newItem.title = isNotNil(newItem.title) ? newItem.title : existingItem.title;
        }
        // Auto set icon, for expand action
        if (existingItem.action === 'expand') {
          newItem.icon = null;
          newItem.matIcon = existingItem.expanded ? 'expand_less' : 'expand_more';
        }
      }
    }

    // Generate item's id (if need)
    else if (isNil(newItem.id)) {
      newItem.id = this._computeNewId();
    }

    // Default icon for expand action
    if (newItem.action === 'expand' && !newItem.matIcon) {
      newItem.matIcon = 'expand_more';
    }

    // If visible, get the parent where to attach the item
    const parent = this._findParent(newItem, opts?.availableParents);
    if (parent && parent.expanded) {
      // Attach item to parent
      this._attachMenuToParent(parent, newItem);

      // Update detached items :
      // - excluded new item
      // - attach and exclude those than can be attached to the new item
      // Search in detached items one have newItem as parent
      if (newItem.expanded) {
        const ancestor = newItem.ancestor;
        this._detachedSubItems = this._detachedSubItems.concat(...newItem.children).reduce((res, item) => {
          // Excluded newly add item
          if (item.id === newItem.id) return res;

          // If some detached can be attached to the new item: do it
          if (MenuItems.checkIfSubMenuVisible(item, pathParam)) {
            const parent = this._findParent(item, [ancestor]);
            if (parent) {
              // Update item's children
              const detachedChildren = this._detachSubMenuItems(pathParam, item.children || []);

              // Attach item
              this._attachMenuToParent(parent, item);

              // Remember detached children
              return res.concat(...detachedChildren);
            }
          }

          // Detach (if need)
          this._detachToParent(item);

          // Add to detached items
          return res.concat(item);
        }, []);
      } else {
        // Remove from detached elements
        this._detachedSubItems = this._detachedSubItems.filter((item) => item.id !== newItem.id);

        // Detach children
        newItem.children.forEach((item) => this._detachToParent(item));
      }
    } else {
      // Add or update detached item
      const detachedIndex = isNotNil(newItem.id) ? this._detachedSubItems.findIndex((item) => item.id === newItem.id) : -1;
      if (detachedIndex !== -1) {
        this._detachedSubItems[detachedIndex] = newItem;
      } else {
        this._detachedSubItems.push(newItem);
      }
    }

    return newItem;
  }

  private _computeNewId() {
    return this._itemCounter++;
  }

  private _removeSubMenuItem(item: MenuItem) {
    const existingItem = this._findExistingItem(item);
    if (existingItem) {
      this._detachToParent(existingItem);
      return existingItem;
    }
  }

  private _detachSubMenuItems(currentPathParam?: MenuPathParam, subItems?: MenuItem[]) {
    currentPathParam = currentPathParam ? currentPathParam : MenuItems.parsePathWithQuery(this.router.routerState.snapshot.url.toString());
    return (subItems || this.subItems).reduce((res, subItem) => {
      // Not visible => should be detach
      if (!MenuItems.checkIfSubMenuVisible(subItem, currentPathParam)) {
        this._detachToParent(subItem);

        return res.concat(subItem);
      }
      // Visible: check children
      return res.concat(this._detachSubMenuItems(currentPathParam, subItem.children));
    }, []);
  }

  private _detachToParent(item: MenuItem) {
    if (!item?.parent) return; // Skip if already detached
    let children = item.parent.children || [];

    // Remove from parent children
    const indexInParent = children.indexOf(item);
    if (indexInParent !== -1) {
      children = children.slice(); // Copy original array, because it can be used in a loop
      children.splice(indexInParent, 1);
      item.parent.children = children;
    }

    item.parent = null;
  }

  private _findExistingItem(searchItem: IMenuItem, items?: MenuItem[], recursive = true): MenuItem {
    items = items || [...this._detachedSubItems, ...this.data];
    if (!items.length) return undefined; // Not found
    for (const item of items) {
      if (MenuItems.isSameIdOrPathAndParams(searchItem, item)) return item;
      if (recursive && item.children) {
        const child = this._findExistingItem(searchItem, item.children);
        if (child) return child;
      }
    }
    return null;
  }

  private _findParent(childItem: MenuItem, availableParents?: MenuItem[]): MenuItem {
    if (!childItem?.path) return undefined;
    if (childItem.parent) return childItem.parent;

    availableParents = availableParents || [...this._detachedSubItems, ...this.data];

    return availableParents.reduce((res, item) => {
      if (!item?.path) return res;

      // A found item already match with the menuItem.parentPath : keep it as it
      // There is a found candidate that already match the current candidate : keep it
      if (MenuItems.isParent(childItem, res)) return res;

      // If the menu item has explicit parentPath and its path and path param are the same as the candidate  : use it as parent
      if (MenuItems.isParent(childItem, item)) return item;

      if (!res && item.children?.length) {
        return this._findParent(childItem, item.children);
      }

      return res;
    }, undefined);
  }

  private _attachMenuToParent(parent: MenuItem, child: MenuItem) {
    if (this._debug) console.debug(`${this._logPrefix}Attach sub-menu {${MenuItems.getUrl(child)}} to parent {${MenuItems.getUrl(parent)}}`);

    // Avoid to keep identical menu item (we replace it with the new child menu item)
    //if (!parent?.$children) parent.$children = new BehaviorSubject<MenuItem[]>([]);
    const children = parent.children || [];
    const existingIndex = children.findIndex((item) => MenuItems.isSameIdOrPathAndParams(item, child));

    // Update existing element
    if (existingIndex !== -1) {
      if (children[existingIndex] !== child) {
        children[existingIndex] = child;
        parent.children = children;
        child.parent = parent;
      }
    } else {
      children.push(child);

      // Sort by tab (or id)
      parent.children = children.sort(MenuItems.sortByTabOrId);
      child.parent = parent;
    }
  }
}
