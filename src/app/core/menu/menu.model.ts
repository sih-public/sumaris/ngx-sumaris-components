import { UserProfileLabel } from '../services/model/person.model';
import { Configuration } from '../services/model/config.model';
import { AppColors, IconRef } from '../../shared/types';
import { isNilOrBlank, isNotNil, isNotNilOrBlank, toBoolean, toInt } from '../../shared/functions';
import { BehaviorSubject } from 'rxjs';
import { MenuPathParam } from './menu.service';
import { Account, AccountUtils } from '../services/model/account.model';
import { Entity, EntityAsObjectOptions } from '../services/model/entity.model';
import { ITreeItemEntity } from '../services/model/tree-item-entity.model';
import { EntityClass } from '../services/model/entity.decorators';

export interface IMenuItem extends IconRef {
  id?: number;
  parent?: IMenuItem;
  children?: IMenuItem[];

  title: string;
  path?: string;

  // Injection point
  after?: string;
  before?: string;
  static?: boolean;

  action?: string | any;
  profile?: UserProfileLabel;
  exactProfile?: UserProfileLabel;
  color?: string | AppColors;
  cssClass?: string;
  // A config property, to enable the menu item
  ifProperty?: string;
  // A config property, to override the title
  titleProperty?: string;
  titleArgs?: { [key: string]: string };
  pathParams?: { [key: string]: string };
  parentPath?: string;
  parentPathParams?: { [key: string]: string };
  pinned?: boolean;
}

// @dynamic
@EntityClass({ typename: 'MenuItemVO' })
export class MenuItem extends Entity<MenuItem> implements IMenuItem, ITreeItemEntity<MenuItem> {
  static fromObject: (source: any, opts?: { withChildren?: boolean }) => MenuItem;

  parent: MenuItem;
  parentId: number;
  $children = new BehaviorSubject<MenuItem[]>([]);
  static: boolean;

  $title = new BehaviorSubject<string>(undefined);
  titleProperty?: string; // A config property, to override the title
  titleArgs?: { [key: string]: string };

  path?: string;
  pathParams?: { [key: string]: string };
  parentPath?: string;
  parentPathParams?: { [key: string]: string };

  // Injection point
  after?: string;
  before?: string;

  action?: string | any;
  profile?: UserProfileLabel;
  exactProfile?: UserProfileLabel;
  color?: string | AppColors;
  cssClass?: string;
  // A config property, to enable the menu item
  ifProperty?: string;
  pinned?: boolean;

  // Icon
  icon?: string;
  matIcon?: string;
  matSvgIcon?: string;

  fromObject(source: any, opts?: { withChildren?: boolean }) {
    this.id = source.id;

    this.title = source.title;
    this.titleProperty = source.titleProperty;
    this.titleArgs = source.titleArgs;

    // Clean path
    if (source.path) {
      const { path, params } = MenuItems.parsePathWithQuery(source.path);
      this.path = path;
      this.pathParams = { ...source.pathParams, ...params };
    }
    // Clean parent path
    if (source.parentPath) {
      const { path, params } = MenuItems.parsePathWithQuery(source.parentPath);
      this.parentPath = path;
      this.parentPathParams = { ...source.parentPathParams, ...params };
    }

    this.after = source.after;
    this.before = source.before;

    this.action = source.action;
    this.profile = source.profile;
    this.exactProfile = source.exactProfile;
    this.color = source.color;
    this.cssClass = source.cssClass;
    this.ifProperty = source.ifProperty;
    this.pinned = toBoolean(source.pinned, false);
    this.static = isNotNil(source.static) ? source.static : undefined; // If undefined, will be set by service

    // Icon
    this.icon = source.icon;
    this.matIcon = source.matIcon;
    this.matSvgIcon = source.matSvgIcon;

    if (!opts || opts.withChildren === false) {
      this.children = (source.children || []).map((json: any) => MenuItem.fromObject(json, opts)) || [];
    }
  }

  asObject(opts?: { withChildren?: boolean } & EntityAsObjectOptions): any {
    const target = Object.assign({}, this);
    target.title = this.title;

    if (opts?.keepLocalId === false) {
      delete target.id;
    }

    if (opts?.withChildren !== false) {
      target.children = (this.children || []).map((c) => c.asObject(opts));
    }

    delete target.$children;
    delete target.$title;
    delete target.parent;

    return target;
  }

  // addChild(child: MenuItem) {
  //   this.$children.next(this.$children.value)
  // }

  get children(): MenuItem[] {
    return this.$children.value;
  }
  set children(value: MenuItem[]) {
    this.$children.next(value);
  }

  get title(): string {
    return this.$title.value;
  }
  set title(value: string) {
    if (this.pinned) {
      // If loading, keep existing title if pinned (avoid to show skeleton)
      if (isNotNilOrBlank(value)) this.$title.next(value);
    } else if (isNotNil(value)) {
      this.$title.next(value);
    }
  }

  get detached(): boolean {
    return !this.parentPath || !this.parent;
  }

  get visible(): boolean {
    return this.static || !this.detached;
  }

  get expanded(): boolean {
    return this.action !== 'expand' || this.matIcon === 'expand_less';
  }

  get ancestor(): MenuItem {
    if (!this.parent) return null; // root = not ancestor
    if (!this.parent.parent) return this; // first sub menu = ancestor
    return this.parent?.ancestor; // loop
  }

  get divider(): boolean {
    return !this.action && !this.path;
  }

  get pinnable(): boolean {
    return !this.static && typeof this.action !== 'function';
  }

  isPinned() {
    if (this.pinned) return true;
    return (this.children || []).some((child) => child.isPinned());
  }
}

export class MenuItems {
  /**
   * Compare, without checking the children
   *
   * @param mainItem
   * @param otherItem
   */
  static isSame(mainItem: IMenuItem, otherItem: IMenuItem) {
    return this.isSameIdOrPathAndParams(mainItem, otherItem) && mainItem.title === otherItem.title;
  }

  /**
   * Compare, without checking the children and the title
   *
   * @param mainItem
   * @param otherItem
   */
  static isSameIdOrPathAndParams(mainItem: IMenuItem, otherItem: IMenuItem) {
    return (isNotNil(mainItem?.id) && mainItem.id === otherItem?.id) || this.isSamePathAndParams(mainItem, otherItem);
  }

  /**
   * Compare, without checking the children
   *
   * @param mainItem
   * @param otherItem
   */
  static isSamePathAndParams(mainItem: IMenuItem, otherItem: IMenuItem) {
    return (
      mainItem &&
      mainItem.path === otherItem?.path &&
      mainItem.parentPath === otherItem.parentPath &&
      this.isSameParams(mainItem.pathParams, otherItem.pathParams)
    );
  }

  /**
   * All key/value of the mainParams should exist (and be equals) in the other params
   *
   * @param mainParams
   * @param otherParams
   */
  static isSameParams(mainParams: { [key: string]: string }, otherParams: { [key: string]: string }) {
    const mainEntries = Object.entries(mainParams);
    const otherEntries = Object.entries(otherParams);
    // TODO: est-ce que un item ne pourrait pas avoir plus de params dans l'item existant ?
    return mainEntries.length === otherEntries.length && mainEntries.every(([key, value]) => value === otherParams[key]);
  }

  static isParent(child: IMenuItem, parent: IMenuItem | MenuPathParam) {
    return (
      (child.parentPath &&
        child.parentPath === parent?.path &&
        this.isSameParams(child.parentPathParams, parent['pathParams'] || parent['params'])) ||
      false
    );
  }

  static checkIfVisible(
    item: IMenuItem,
    account: Account,
    config: Configuration,
    opts?: {
      isLogin?: boolean;
      debug?: boolean;
      logPrefix?: string;
    }
  ): boolean {
    opts = opts || {};
    if (item.profile) {
      const hasProfile = AccountUtils.hasMinProfile(account, item.profile);
      if (!hasProfile) {
        if (opts.debug)
          console.debug(
            `${(opts && opts.logPrefix) || '[menu-service]'} Hide item '${item.title}': need the min profile '${item.profile}' to access path '${item.path}'`
          );
        return false;
      }
    } else if (item.exactProfile) {
      const hasExactProfile = AccountUtils.hasExactProfile(account, item.exactProfile);
      if (!hasExactProfile) {
        if (opts.debug)
          console.debug(
            `${(opts && opts.logPrefix) || '[menu-service]'} Hide item '${item.title}': need exact profile '${item.exactProfile}' to access path '${item.path}'`
          );
        return false;
      }
    }

    // If enable by config
    if (item.ifProperty) {
      //console.debug("[menu] Checking if property enable ? " + item.ifProperty, config && config.properties);
      const isEnableByConfig = config && config.properties[item.ifProperty] === 'true';
      if (!isEnableByConfig) {
        if (opts.debug) console.debug("[menu-service] Config property '" + item.ifProperty + "' not 'true' for ", item.path);
        return false;
      }
    }

    return true;
  }

  static checkIfSubMenuVisible(item: MenuItem, currentPathParam: MenuPathParam) {
    if (!item.parentPath) return false; // Should have a parent

    if (item.static) return true; // Always keep static items

    if (item.isPinned()) return true; // Always keep pinned items

    // Keep each menu items in the scope of the current url path
    if (currentPathParam?.path.startsWith(item.path)) return true;

    // Keep if parent is an expanded item
    if (item.parent?.action === 'expand' && item.parent.expanded) return true;

    // Not visible
    return false;
  }

  static parsePathWithQuery(pathWithQuery: string): MenuPathParam {
    const index = pathWithQuery.lastIndexOf('?');
    if (index < 0) return { path: pathWithQuery, params: {} };
    const path = pathWithQuery.substring(0, index);
    const queryString = pathWithQuery.substring(index);
    const params = Object.fromEntries(new URLSearchParams(queryString));
    return { path, params };
  }

  static getUrl(item: IMenuItem) {
    return this.getUrlFromPathAndParams(item?.path, item?.pathParams);
  }

  static getUrlFromPathAndParams(path: string, params?: any) {
    const queryString = Object.entries(params || {})
      .map(([key, value]) => `${key}=${value}`)
      .join('&');
    if (isNilOrBlank(queryString)) return path || '';
    return `${path || ''}?${queryString}`;
  }

  static sortByParamOrIdComparator(paramName?: string): (n1: IMenuItem, n2: IMenuItem) => number {
    paramName = paramName || 'tab';
    return (n1: IMenuItem, n2: IMenuItem) => {
      const d1 = toInt(n1.pathParams?.[paramName], n1.id);
      const d2 = toInt(n2.pathParams?.[paramName], n2.id);
      return d1 === d2 ? 0 : d1 > d2 ? 1 : -1;
    };
  }

  static sortByTabOrId = this.sortByParamOrIdComparator('tab');
}

/**
 * Options to configure specific behavior on projects
 */
export class MenuOptions {
  subMenu?: {
    enable?: boolean;
    showPinButton?: boolean;
    disableIcon?: boolean;
    savePinned?: boolean;
  };
}
