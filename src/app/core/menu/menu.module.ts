import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MenuComponent } from './menu.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { SubMenuTabDirective } from './sub-menu-tab.directive';
import { RxStateModule } from '../../shared/rx-state/rx-state.module';

@NgModule({
  imports: [SharedModule, RouterModule, RxStateModule, TranslateModule.forChild()],

  declarations: [
    // Components
    MenuComponent,
    // Directive
    SubMenuTabDirective,
  ],
  exports: [
    // Modules
    TranslateModule,
    // Components
    MenuComponent,
    // Directive
    SubMenuTabDirective,
  ],
})
export class AppMenuModule {}
