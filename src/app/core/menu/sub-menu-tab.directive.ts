import { AfterViewInit, Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatTab, MatTabGroup } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { MenuService } from './menu.service';
import { MenuItem, MenuItems } from './menu.model';
import { isNotNil } from '../../shared/functions';
import { IconRef } from '../../shared/types';

@Directive({
  selector: '[appSubMenuTab]',
})
export class SubMenuTabDirective implements OnChanges, AfterViewInit {
  @Input() label: string;
  @Input() disabled: boolean;
  @Input() parentPath: string;
  @Input() path: string;
  @Input() subMenuTitle: string;
  @Input() subMenuIcon: IconRef = null; // { icon: 'none' };

  private _menuItem: MenuItem;
  private readonly _enable: boolean;

  constructor(
    private menuService: MenuService,
    private tabGroup: MatTabGroup,
    private tab: MatTab,
    private router: Router
  ) {
    this.path = MenuItems.parsePathWithQuery(this.router.routerState.snapshot.url).path;
  }

  ngAfterViewInit() {
    if (!this.tab.disabled) this._addToMenu();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled || changes.label || changes.subMenuTitle || changes.parentPath || changes.path) {
      if (this.disabled) {
        this._removeToMenu();
      } else {
        this._addToMenu();
      }
    }
  }

  private async _removeToMenu() {
    const menuItem = this.menuItem;
    if (menuItem) {
      await this.menuService.removeSubMenuItem(menuItem);
    }
  }

  private async _addToMenu() {
    const menuItem = this.menuItem;
    if (!menuItem?.parentPath) return;

    // DEBUG
    //if (this._debug) console.debug(`[sub-menu-tab] Refreshing sub menu {${menuItem.path}?tab=${menuItem.pathParams?.tab || 0} on parent {${menuItem.parentPath}}`);

    this._menuItem = await this.menuService.addSubMenuItem(menuItem, { skipIfExists: false });
  }

  private get menuItem(): MenuItem {
    const index = this.tabGroup.selectedIndex + this.tab.position;
    const parentPath = index === 0 ? this.parentPath : this.path;
    if (!parentPath) return null;
    const tabParams = index === 0 ? {} : { tab: index.toString() };

    // Create or load
    if (!this._menuItem) {
      this._menuItem = this.menuService.createSubMenu({
        path: this.path,
        pathParams: tabParams,
        parentPath,
      });
    }

    // Update existing
    else {
      const { path, params } = MenuItems.parsePathWithQuery(this.path);
      this._menuItem.path = path;
      this._menuItem.pathParams = { ...params, ...tabParams };
    }

    // Update item (title and icon)
    if (this._menuItem) {
      this._menuItem.title = this.title;
      if (this.subMenuIcon) {
        Object.assign(this._menuItem, this.subMenuIcon);
      }
    }

    return this._menuItem;
  }

  private get title(): string {
    return isNotNil(this.subMenuTitle) ? this.subMenuTitle : this.label;
  }
}
