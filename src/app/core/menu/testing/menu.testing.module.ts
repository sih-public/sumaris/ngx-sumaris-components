import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MenuTestingPage } from './menu.testing';
import { AppMenuModule } from '../menu.module';
import { SharedModule } from '../../../shared/shared.module';
import { OtherMenuTestingPage } from './menu-other.testing';
import { AuthGuardService } from '../../services/auth-guard.service';

const routes: Routes = [
  {
    path: 'menu',
    data: {
      test: 'menu',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MenuTestingPage,
        data: {
          test: 'empty',
        },
      },
      {
        path: 'others',
        canActivate: [AuthGuardService],
        data: {
          profile: 'USER',
        },
        children: [
          {
            path: ':otherId',
            pathMatch: 'full',
            component: OtherMenuTestingPage,
            canActivate: [AuthGuardService],
            data: {
              profile: 'ADMIN',
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), AppMenuModule, SharedModule],
  declarations: [MenuTestingPage, OtherMenuTestingPage],
  exports: [RouterModule, MenuTestingPage, OtherMenuTestingPage],
})
export class MenuTestingModule {}
