import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertController, NavController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {sleep} from '../../../shared/functions';
import {MenuService} from '../menu.service';
import {MenuTestingPage} from './menu.testing';

@Component({
  selector: 'app-testing-menu-other',
  templateUrl: './menu.testing.html',
  styleUrls: ['./menu.testing.scss'],
})
export class OtherMenuTestingPage extends MenuTestingPage {
  constructor(
    route: ActivatedRoute, // Modal editor give 'null'
    router: Router,

    navController: NavController,
    alertCtrl: AlertController,
    translate: TranslateService,
    menuService: MenuService
  ) {
    super(route, router, navController, alertCtrl, translate, menuService);
    this.logPrefix = '[menu-testing-other] ';
    this.showOtherLinks = false;
    this.showThirdTab = true;
    this.subTabCount = 2;
  }

  async initSubMenu() {
    this.secondTabTitle = 'Second';
    this.parentPath = '/testing/shared/menu?tab=1';
    this.childPath = '';
    this.path = '/testing/shared/menu/others/' + this.route.snapshot.paramMap.get('otherId');
    console.debug(`${this.logPrefix} Setup with`, { path: this.path, parentPath: this.parentPath });
    this.thirdTabTitle = 'Third';
    this.markForCheck();

    this.title = await this.computeTitle();
    this.markForCheck();
  }

  protected getDefaultTitle(): string {
    return 'Other';
  }

  protected async computeTitle(): Promise<string> {
    // Load title (with a delay (e.g. when loading data)
    await sleep(2000);

    const otherId = this.route.snapshot.paramMap.get('otherId');

    return this.getDefaultTitle() + ' ' + otherId;
  }

}
