import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MenuService } from '../menu.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { DateUtils } from '../../../shared/dates';
import { toBoolean } from '../../../shared/functions';
import { MatTabGroup } from '@angular/material/tabs';
import { AppTabEditor } from '../../form/entity/tab-editor.class';

@Component({
  selector: 'app-testing-menu',
  templateUrl: './menu.testing.html',
  styleUrls: ['./menu.testing.scss'],
})
export class MenuTestingPage extends AppTabEditor implements OnInit, OnDestroy, AfterViewInit {
  protected logPrefix = '[menu-testing] ';
  protected childPath = '';
  protected thirdTabTitle = '';
  protected parentPath: string = null;
  protected path: string = null;
  protected showOtherLinks = true;
  protected showThirdTab = false;

  protected $title = new Subject<string>();
  protected $secondTabTitle = new Subject<string>();
  protected _subSelectedTabIndex = 0;

  subTabCount: number;

  @Input() set subSelectedTabIndex(value: number) {
    this.setSubSelectedTabIndex(value);
  }
  get subSelectedTabIndex(): number {
    return this._subSelectedTabIndex;
  }

  @Output() readonly subSelectedTabIndexChange = new EventEmitter<number>();

  @ViewChild('subTabGroup', { static: true }) subTabGroup: MatTabGroup;

  protected set title(value: string) {
    this.$title.next(value);
  }

  protected set secondTabTitle(value: string) {
    this.$secondTabTitle.next(value);
  }

  constructor(
    route: ActivatedRoute, // Modal editor give 'null'
    router: Router,
    navController: NavController,
    alertCtrl: AlertController,
    translate: TranslateService,
    protected menuService: MenuService
  ) {
    super(route, router, navController, alertCtrl, translate, {
      tabCount: 4,
    });
  }

  ngOnInit() {
    console.debug(`${this.logPrefix} Init page...`);

    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.initSubMenu();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    console.debug(`${this.logPrefix}Destroying...`);
  }

  load(id?: number, options?: any): Promise<void> {
    return Promise.resolve(undefined);
  }

  get isNewData(): boolean {
    return false;
  }

  reload(): Promise<void> {
    return Promise.resolve(undefined);
  }

  save(event: Event | undefined, options: any): Promise<boolean> {
    return Promise.resolve(false);
  }

  changeTitle(event?: UIEvent) {
    this.title = '';
    setTimeout(() => {
      const time = DateUtils.moment().format('HH:mm:ss');
      this.title = `${this.getDefaultTitle()} <b>${time}</b>`;
    }, 2000);
  }

  changeSecondTabTitle(event?: UIEvent) {
    const time = DateUtils.moment().format('HH:mm:ss');
    this.secondTabTitle = `<small>${time}<br/></small>Second`;
  }

  addSubMenuItem(event?: UIEvent) {
    this.menuService.addSubMenuItem({
      title: 'Fake',
      parentPath: this.path,
      path: this.path + '/fake',
      //pinned: true
    });
  }

  addOutsideRouteSubMenuItem(event?: UIEvent, pinned?: boolean) {
    this.menuService.addSubMenuItem({
      title: 'Fake',
      parentPath: '/admin/users',
      path: '/admin/users/fake',
      pinned: toBoolean(pinned, false),
    });
  }

  protected enableMenu(value: boolean) {
    this.menuService.enable(value);
  }
  protected toggleSplitPaneWhen() {
    this.menuService.toggleSplitPaneWhen();
  }
  protected async initSubMenu() {
    this.title = await this.computeTitle();
    this.secondTabTitle = 'Others';
    this.thirdTabTitle = 'Third';
    this.parentPath = '/testing';
    this.path = this.parentPath + '/shared/menu';
    this.childPath = this.path + '/others';
    console.debug(`${this.logPrefix} Setup with`, { path: this.path, parentPath: this.parentPath });
    this.markForCheck();
  }

  protected getFirstInvalidTabIndex(): number {
    return 0;
  }

  protected getDefaultTitle() {
    return 'Menu';
  }

  protected async computeTitle() {
    return this.getDefaultTitle();
  }

  setSubSelectedTabIndex(value: number) {
    // Fix value
    if (value < 0) {
      value = 0;
    } else if (value > this.tabCount - 1) {
      console.warn(`[menu-other] Invalid tab index ${value}. Max value is ${this.subTabCount - 1}`);
      value = this._subSelectedTabIndex;
    }

    this._subSelectedTabIndex = value;
    this.subSelectedTabIndexChange.next(value);

    if (this.subTabGroup) {
      this.subTabGroup.selectedIndex = value;
      this.markForCheck();
      // Wait tab change in UI, before realign ink tab
      setTimeout(() => this.subTabGroup.realignInkBar(), 200);
    }
  }
}
