import { AppTable } from './table.class';
import { TableElement } from '@e-is/ngx-material-table';
import { AppFormUtils } from '../form/form.utils';
import { AsyncTableElement } from '@e-is/ngx-material-table/src/app/ngx-material-table/async/async-table-element';
import { EntityUtils, IEntity } from '../services/model/entity.model';
import { isNotNil } from '../../shared/functions';
import { SortDirection } from '@angular/material/sort';

export class AppTableUtils {
  static waitIdle(table: AppTable<any, any, any>): Promise<any> {
    if (!table || !table.dataSource) {
      throw new Error('Invalid table. Missing table or table.dataSource');
    }
    return table.dataSource.waitIdle();
  }

  static logRowErrors(row: TableElement<any> | AsyncTableElement<any>, logPrefix?: string): void {
    AppFormUtils.logFormErrors(row.validator, logPrefix);
  }

  static getEntityId<T extends IEntity<T, ID> = IEntity<any, any>, ID = any>(row: TableElement<T> | AsyncTableElement<T>): ID {
    // When using validator: use the control (avoid to load the full form's value)
    if (row.validator) return row.validator.get('id').value as ID;
    return EntityUtils.getId(row.currentData);
  }

  static getEntityIds<T extends IEntity<T, ID> = IEntity<any, any>, ID = any>(rows: TableElement<T>[] | AsyncTableElement<T>[]): ID[] {
    return (rows || [])
      .map((row) => row.currentData)
      .filter(isNotNil)
      .map(EntityUtils.getId)
      .filter(isNotNil);
  }

  static getEntities<T extends IEntity<T, ID> = IEntity<any, any>, ID = any>(rows: TableElement<T>[] | AsyncTableElement<T>[]): T[] {
    return (rows || []).map((row) => row.currentData).filter(isNotNil);
  }

  static inverseDirection(direction: SortDirection): SortDirection {
    return direction === 'asc' ? 'desc' : 'asc';
  }
}
