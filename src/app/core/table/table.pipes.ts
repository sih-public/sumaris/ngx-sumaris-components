import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { AbstractSelectionModelPipe } from '../../shared/pipes/selection.pipes';
import { AppTable } from './table.class';
import { Subscription } from 'rxjs';

export type AppTableRowCountProperty = 'visibleRowCount' | 'totalRowCount';

@Pipe({
  name: 'abstract-table-selection',
})
export abstract class AbstractTableSelectionPipe<R, O, T extends AppTable<any> = AppTable<any>> extends AbstractSelectionModelPipe<any, R, O> {
  private _onRowsChanges: Subscription;

  protected constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }

  transform(selectionOrTable: SelectionModel<any> | T, tableOrOpts?: T | O, opts?: O): R {
    // @ts-ignore
    const _table: T = tableOrOpts instanceof AppTable<any> ? tableOrOpts : selectionOrTable instanceof AppTable<any> ? selectionOrTable : undefined;
    // @ts-ignore
    const _selection: SelectionModel<any> = _table?.selection || selectionOrTable;
    // @ts-ignore
    const _opts: O = opts || !(tableOrOpts instanceof AppTable<any> && (tableOrOpts as O));
    return super.transform(_selection, _table, _opts);
  }

  protected _subscribe(selection: SelectionModel<any>, table?: T, opts?: O): Subscription {
    this._onRowsChanges = (table as T)?.dataSource?.rowsSubject.subscribe((_) => {
      const result = this._transform(selection, table, opts);
      if (result !== this._result) {
        this._result = result;
        this._ref.markForCheck();
      }
    });

    return super._subscribe(selection, table);
  }

  protected _dispose() {
    super._dispose();
    this._onRowsChanges?.unsubscribe();
    this._onRowsChanges = null;
  }
}

@Pipe({
  name: 'isAllSelected',
  pure: false,
})
export class IsAllSelectedPipe extends AbstractTableSelectionPipe<boolean, AppTableRowCountProperty> implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }

  protected _transform(
    selection: SelectionModel<any>,
    table: AppTable<any>,
    countPropertyName: AppTableRowCountProperty = 'visibleRowCount'
  ): boolean {
    return selection.selected.length === table?.[countPropertyName];
  }
}

@Pipe({
  name: 'isNotAllSelected',
  pure: false,
})
export class IsNotAllSelectedPipe extends IsAllSelectedPipe implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }

  protected _transform(
    selection: SelectionModel<any>,
    table: AppTable<any>,
    countPropertyName: AppTableRowCountProperty = 'visibleRowCount'
  ): boolean {
    return selection.hasValue() && selection.selected.length !== table?.[countPropertyName];
  }
}
