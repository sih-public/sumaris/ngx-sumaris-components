import { Directive, Injector, Input, OnInit } from '@angular/core';
import { ValidatorService } from '@e-is/ngx-material-table';
import { EntitiesTableDataSourceConfig, EntitiesTableDataSource } from './entities-table-datasource.class';
import { AppTable } from './table.class';
import { IEntity } from '../services/model/entity.model';
import { InMemoryEntitiesService } from '../../shared/services/memory-entity-service.class';

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppInMemoryTable<T extends IEntity<T, ID>, F = any, ID = number> extends AppTable<T, F, ID> implements OnInit {
  @Input() canEdit = false;
  @Input() canDelete = false;

  set value(data: T[]) {
    this.setValue(data);
  }

  get value(): T[] {
    return this.memoryDataService.value;
  }

  protected constructor(
    injector: Injector,
    protected columns: string[],
    protected dataType: new () => T,
    protected memoryDataService: InMemoryEntitiesService<T, F, ID>,
    protected validatorService: ValidatorService,
    options?: EntitiesTableDataSourceConfig<T, ID>,
    filter?: F
  ) {
    super(injector, columns, new EntitiesTableDataSource<T, F, ID>(dataType, memoryDataService, validatorService, options), filter);

    this.autoLoad = false; // waiting value to be set
  }

  ngOnInit() {
    super.ngOnInit();
  }

  setValue(value: T[], opts?: { emitEvent?: boolean }) {
    // Reset previous error
    if (this.error) {
      this.setError(null);
      this.markForCheck();
    }
    this.memoryDataService.value = value;
    if (!opts || opts.emitEvent !== false) {
      this.emitRefresh();
    }
  }

  protected equals(t1: T, t2: T): boolean {
    return this.memoryDataService.equals(t1, t2);
  }
}
