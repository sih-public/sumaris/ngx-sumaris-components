// noinspection DuplicatedCode

import { AsyncTableDataSource, AsyncTableElement, TableDataSourceConfig, ValidatorService } from '@e-is/ngx-material-table';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Entity, IEntity } from '../services/model/entity.model';
import { ErrorCodes } from '../services/errors';
import { catchError, debounceTime, map, takeUntil } from 'rxjs/operators';
import { Directive, OnDestroy } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { CollectionViewer } from '@angular/cdk/collections';
import { EntitiesServiceWatchOptions, FetchMoreFn, IEntitiesService, LoadResult } from '../../shared/services/entity-service.class';
import { firstFalsePromise } from '../../shared/observables';
import { isEmptyArray, isNotEmptyArray, isNotNil, removeEnd, sleep } from '../../shared/functions';
import { AppTableUtils } from './table.utils';
import { environment } from '../../../environments/environment';
import { IEntitiesTableDataSource } from './table.model';

export interface EntitiesAsyncTableDataSourceConfig<
  T extends Entity<T, ID>,
  ID = number,
  WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
  SO = any,
> extends TableDataSourceConfig {
  onRowCreated?: (row: AsyncTableElement<T>) => Promise<void> | void;
  saveOnlyDirtyRows?: boolean;
  readOnly?: boolean;

  /**
   * @deprecated
   */
  dataServiceOptions?: WO & SO; // Options passed to the dataService.watchAll() and dataService.saveAll()

  watchAllOptions?: WO; // Options passed to the dataService.saveAll()
  saveAllOptions?: SO; // Options passed to the dataService.saveAll()

  [key: string]: any;
}

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export class EntitiesAsyncTableDataSource<
    T extends IEntity<T, ID>,
    F = any,
    ID = number,
    WO extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
    SO = any,
    V extends ValidatorService = ValidatorService,
    C extends EntitiesAsyncTableDataSourceConfig<T, ID, WO, SO> = EntitiesAsyncTableDataSourceConfig<T, ID, WO, SO>,
    R extends AsyncTableElement<T> = AsyncTableElement<T>,
  >
  extends AsyncTableDataSource<T, V, C, R>
  implements IEntitiesTableDataSource<R>, OnDestroy
{
  private readonly _entityName: string;

  protected _debug = false;
  protected _creating = false;
  protected _saving = false;
  protected _fetchMoreFn: FetchMoreFn<LoadResult<T>> = null;
  protected _stopWatchSubject = new Subject<void>();

  loadingSubject = new BehaviorSubject<boolean>(undefined);

  get watchAllOptions(): WO {
    return this.config.watchAllOptions;
  }

  set watchAllOptions(value: WO) {
    this.config.watchAllOptions = value;
  }

  get saveAllOptions(): SO {
    return this.config.saveAllOptions;
  }

  set saveAllOptions(value: SO) {
    this.config.saveAllOptions = value;
  }

  get loaded(): boolean {
    return this.loadingSubject.value === false; // Should be false when undefined (initial state)
  }

  get loading(): boolean {
    return this.loadingSubject.value !== false; // Should be true when undefined (initial state)
  }

  /**
   * Creates a new TableDataSource instance, that can be used as datasource of `@angular/cdk` data-table.
   *
   * @param dataService A service to load and save data
   * @param dataType Type of data contained by the Table. If not specified, then `data` with at least one element must be specified.
   * @param environment
   * @param validatorService Service that create instances of the FormGroup used to validate row fields.
   * @param config Additional configuration for table.
   */
  constructor(
    dataType: new () => T,
    public readonly dataService: IEntitiesService<T, F, WO>,
    validatorService?: V,
    options?: C
  ) {
    super([], dataType, validatorService, {
      keepOriginalDataAfterConfirm: false,
      readOnly: false,
      saveOnlyDirtyRows: false,
      ...options,
    });

    this._entityName = removeEnd(new dataType().__typename || 'UnknownVO', 'VO');
    this._debug = options?.suppressErrors === false && !environment.production;
  }

  ngOnDestroy() {
    this.disconnect();
  }

  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<F>): Observable<LoadResult<T>> {
    this._stopWatchSubject.next();
    this._fetchMoreFn = null;
    this.markAsLoading();

    return this.dataService.watchAll(offset, size, sortBy, sortDirection, filter, this.watchAllOptions as WO).pipe(
      catchError((err) => this.handleError(err, 'ERROR.LOAD_DATA_ERROR')),
      map((res: LoadResult<T>) => {
        if (this._saving) {
          console.info(`[entities-table-datasource] Received ${this._entityName} data (from service), but still saving: skip`);
        } else {
          if (!this.config.suppressErrors && this.hasSomeDirtyRow()) {
            console.warn(`[entities-table-datasource] Received ${this._entityName} data, while some row still dirty. Some data can be lost`);
          }
          this.updateDatasource((res.data || []) as T[]);
          this._fetchMoreFn = res.fetchMore;
        }
        return res;
      }),
      // Stop this pipe next time we call watchAll()
      takeUntil(this._stopWatchSubject)
      // ⚠ Notice: Don't put any operator after takeUntil to avoid potential subscription leaks
    );
  }

  protected updateDatasourceFromRows(rows: R[]) {
    // Avoid to update dataSourceSubject, when not need
    if (this.datasourceSubject.observed) {
      if (!this.config.suppressErrors)
        console.warn("[entities-table-datasource] Update datasource subject. Please prefer using 'rowsSubject' instead of 'datasourceSubject'");
      super.updateDatasourceFromRows(rows);
    } else {
      console.debug('[entities-table-datasource] Skipping datasourceSubject update (not used yet).');
    }
  }

  async save(): Promise<boolean> {
    if (this.config.readOnly) {
      console.error('[entities-table-datasource] Enable to save, because config.readOnly=true');
      return false;
    }
    // Saving twice (should never occur)
    if (this._saving) {
      console.warn(`[entities-table-datasource] Trying to save ${this._entityName} rows twice. Skip`);
      return false;
    }

    this._saving = true;
    this.markAsLoading();

    const onlyDirtyRows = this.config.saveOnlyDirtyRows;

    try {
      if (this._debug) console.debug(`[entities-table-datasource] Saving ${this._entityName} rows... {onlyDirtyRows: ${onlyDirtyRows}}`);

      // Get all rows
      const rows = this.getRows();

      // Finish editing all rows
      const invalidRows: AsyncTableElement<T>[] = (
        await Promise.all(this.getEditingRows().map((row) => row.confirmEditCreate().then((confirmed) => (confirmed === false ? row : null))))
      ).filter(isNotNil);

      // Cannot finish some rows: error
      if (invalidRows.length) {
        // log errors
        if (this._debug)
          invalidRows.forEach((row) => AppTableUtils.logRowErrors(row, `[entities-table-datasource] ${this._entityName} row #${row.id}`));
        // Stop with an error
        throw { code: ErrorCodes.TABLE_INVALID_ROW_ERROR, message: 'ERROR.TABLE_INVALID_ROW_ERROR' };
      }

      let data: T[];
      let dataToSave: T[];

      if (this.validatorService) {
        dataToSave = [];
        data = rows.map((row) => {
          const currentData = new this.dataConstructor() as T;
          currentData.fromObject(row.currentData);
          // Filter to keep only dirty row
          if (onlyDirtyRows && row.validator.dirty) dataToSave.push(currentData);
          return currentData;
        });
        if (!onlyDirtyRows) dataToSave = data;
      }
      // Or use the current data without conversion (when no validator service used)
      else {
        data = rows.map((row) => row.currentData);
        // save all data, as we don't have any dirty marker
        dataToSave = data;
      }

      // If no data to save: exit
      if (onlyDirtyRows && !dataToSave.length) {
        if (this._debug) console.debug(`[entities-table-datasource] No ${this._entityName} data to save. Skip`);
        return false;
      }

      if (this._debug) console.debug(`[entities-table-datasource] Asking service to save this ${this._entityName} data:`, dataToSave);
      await this.dataService.saveAll(dataToSave, this.saveAllOptions);

      if (this._debug) console.debug(`[entities-table-datasource] Saving ${this._entityName} data [OK]`);

      // LP 23/03/2021: update datasource is necessary but can be changed to a refetch() on QueryRef (must be created and registered in GraphqlService.watchQuery)
      this.updateDatasource(data, { emitEvent: false });

      return true;
    } catch (error) {
      if (this._debug) console.error(('[entities-table-datasource] Error while saving: ' + error && error.message) || error);
      throw error;
    } finally {
      this._saving = false;
      this.markAsLoaded();
    }
  }

  updateDatasource(data: T[], opts?: { emitEvent: boolean }) {
    if (this._debug) console.debug(`[entities-table-datasource] Updating datasource with data:`, data);

    super.updateDatasource(data, opts);

    if (!opts || opts.emitEvent !== false) {
      this.markAsLoaded();
    }
  }

  connect(collectionViewer: CollectionViewer): Observable<R[] | ReadonlyArray<R>> {
    // DEBUG
    //console.debug("[entities-datasource] connect");
    return super.connect(collectionViewer);
  }

  disconnect(collectionViewer?: CollectionViewer) {
    if (this._debug) console.debug('[entities-table-datasource] Disconnecting...');
    super.disconnect(collectionViewer);
    if (!this._stopWatchSubject.closed) {
      if (this._debug) console.debug('[entities-table-datasource] Closing...');
      this._stopWatchSubject.next();
      this._stopWatchSubject.complete();
      this._stopWatchSubject.unsubscribe();
      this.loadingSubject.complete();
      this.loadingSubject.unsubscribe();
    }
  }

  waitIdle(debounceTimeMs?: number): Promise<any> {
    return firstFalsePromise(
      this.loadingSubject.asObservable().pipe(
        debounceTime(debounceTimeMs || 100) // if not started yet, wait
      )
    );
  }

  async confirmCreate(row: R): Promise<boolean> {
    const confirmed = await super.confirmCreate(row);
    if (!confirmed) return false;
    if (row.editing && row.validator) {
      console.warn('[entities-table-datasource] Row still has {editing: true} after confirmCreate()! Force editing to false');
      row.validator.disable({ onlySelf: true, emitEvent: false });
    }
    return confirmed;
  }

  async confirmEdit(row: R): Promise<boolean> {
    const confirmed = await super.confirmEdit(row);
    if (!confirmed) return false;
    if (row.validator?.enabled) {
      console.warn('[entities-table-datasource] Row still has {editing: true} after confirmEdit()! Force editing to false');
      row.validator.disable({ onlySelf: true, emitEvent: false });
    }
    return true;
  }

  async startEdit(row: R): Promise<boolean> {
    const editing = await super.startEdit(row);
    if (!editing) return false;
    if (!row.editing && row.validator) {
      console.warn('[entities-table-datasource] Row still has {editing: false} after startEdit()! Force editing');
      row.validator.enable({ onlySelf: true, emitEvent: false });
    }
    return true;
  }

  handleError(error: any, message: string): Observable<LoadResult<T>> {
    const errorMsg = (error && error.message) || error;
    console.error(`[entities-table-datasource] Service ${this._entityName} sent error: ${errorMsg}`, error);
    this.markAsLoaded();
    throw new Error(message || errorMsg);
  }

  handleServiceError(error: any) {
    const errorMsg = (error && error.message) || error;
    console.error(`[entities-table-datasource] Service ${this._entityName} sent error: ${errorMsg}`, error);
    this.markAsLoaded();
    throw error;
  }

  async delete(id: number): Promise<boolean> {
    // If new row: not need to propagate to the dataService
    if (id === -1) {
      return super.delete(id);
    }

    const row = this.getRow(id);
    if (!row) {
      console.error(`[entities-table-datasource] Row to delete with id=${id} not found`);
      return;
    }

    this.markAsLoading();

    try {
      await this.dataService.deleteAll([row.currentData], this.saveAllOptions);

      // Wait cache update, then table update
      await sleep(300);

      // make sure row has been deleted (because GraphQl cache remove can fail)
      const present = this.getRow(id) === row;
      if (present) await super.delete(id);
      this.markAsLoaded();
    } catch (err) {
      this.handleServiceError(err);
    }
  }

  async deleteAll(rows: R[]): Promise<any> {
    this.markAsLoading();

    const data = this.getDataFromRows(rows);

    try {
      // Call service deletion
      await this.dataService.deleteAll(data, this.saveAllOptions);

      // Wait propagation (e.g. update cache, then received update from dataService.watchAll())
      await sleep(500);

      // Workaround, to be sure all rows have been deleted
      // Sometime, the service miss deletion, or GraphQl cache remove failed.
      // In this case, apply missing deletion using the parent delete() function
      const rowNotDeleted = this.getRows().filter((row) => rows.includes(row));
      if (isNotEmptyArray(rowNotDeleted)) {
        console.warn(
          `[entities-table-datasource] Force deletion of ${rowNotDeleted.length} rows! Please check that data service update the cache, after deletion`
        );
        // Start at the end
        for (const r of rowNotDeleted.sort((a, b) => (a.id > b.id ? -1 : 1))) {
          await super.delete(r.id);
        }
      }
    } catch (err) {
      // Handle service error
      this.handleServiceError(err);
    } finally {
      this.markAsLoaded();
    }
  }

  getRow(id: number): R {
    return super.getRow(id);
  }

  getRows(): R[] {
    return this.rowsSubject.value || [];
  }

  getEditingRows(): R[] {
    return this.getRows().filter((row) => row.editing);
  }

  getSingleEditingRow(): R {
    const rows = this.getEditingRows();
    return rows.length === 1 ? rows[0] : undefined;
  }

  hasSomeEditingRow() {
    return this.getRows().some((row) => row.editing);
  }

  hasSomeDirtyRow(): boolean {
    return this.getRows().some((row) => row.dirty);
  }

  async createNew(insertAt?: number, opts = { editing: true }): Promise<R | undefined> {
    // Avoid multiple call (only one editing row is allowed)
    if (this._creating && opts.editing) return;

    this._creating = true;

    try {
      const row = await super.createNew(insertAt, opts);

      if (!row) return undefined; // Stop here

      // Call observers
      if (this.config?.onRowCreated) {
        try {
          await this.config.onRowCreated(row);
        } catch (err) {
          // Log, then continue
          console.error((err && err.message) || err, err);
        }
      }

      return row;
    } finally {
      this._creating = false;
    }
  }

  getData(): T[] {
    const rows = this.getRows();
    return this.getDataFromRows(rows);
  }

  async fetchMore(opts?: { emitEvent: boolean }): Promise<boolean> {
    if (!this._fetchMoreFn) return false; // Avoid multiple call
    if (this.hasSomeDirtyRow()) {
      console.warn(`Cannot fetch more ${this._entityName} because some row) still dirty`);
      return;
    }
    console.debug(`Will fetching more row(s) still...`);

    // Forget the fetchMore function, to avoid multiple call
    const fetchMoreFn = this._fetchMoreFn;
    this._fetchMoreFn = null;

    // Fetch next page
    const res: LoadResult<T> = await fetchMoreFn();

    // Skip if empty (no more data)
    if (isEmptyArray(res?.data)) return false;

    // Update the data source
    super.updateDatasource((this.currentData || []).concat(...res.data), opts);

    // Remember fetchMore
    this._fetchMoreFn = res.fetchMore;
    return true;
  }

  /* -- protected method -- */

  markAsLoading() {
    if (this.loadingSubject.value !== true) {
      this.loadingSubject.next(true);
    }
  }

  markAsLoaded() {
    if (this.loadingSubject.value !== false) {
      this.loadingSubject.next(false);
    }
  }
}
