import {
  booleanAttribute,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { AsyncTableElement, TableElement } from '@e-is/ngx-material-table';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-actions-column',
  styleUrls: ['./actions-column.component.scss'],
  templateUrl: './actions-column.component.html',
})
export class ActionsColumnComponent<T extends TableElement<any> | AsyncTableElement<any> = TableElement<any>> implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;
  @ViewChild(MatMenuTrigger) matMenuTrigger: MatMenuTrigger;

  @Input() matColumnDef = 'actions';
  @Input() style: 'table' | 'mat-table' = 'table';
  @Input({ transform: booleanAttribute }) stickyEnd = false;
  @Input({ transform: booleanAttribute }) canCancel: boolean;
  @Input({ transform: booleanAttribute }) canConfirm: boolean;
  @Input({ transform: booleanAttribute }) canDelete: boolean;
  @Input({ transform: booleanAttribute }) canBackward: boolean;
  @Input({ transform: booleanAttribute }) canForward: boolean;
  @Input({ transform: booleanAttribute }) canConfirmAndAdd: boolean;
  @Input() dirtyIcon: boolean | string;
  @Input() optionsTitle = 'COMMON.BTN_OPTIONS';
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;
  @Input() cellTemplate: TemplateRef<{ $implicit: T }>;

  @Output() optionsClick = new EventEmitter<Event>();
  @Output() cancelOrDeleteClick = new EventEmitter<{ event: Event; row: T }>();
  @Output() confirmEditCreateClick = new EventEmitter<{ event: Event; row: T }>();
  @Output() confirmAndAddClick = new EventEmitter<{ event: Event; row: T }>();
  @Output() backward = new EventEmitter<{ event: Event; row: T }>();
  @Output() forward = new EventEmitter<{ event: Event; row: T }>();

  constructor(
    private table: MatTable<any>,
    private cd: ChangeDetectorRef
  ) {
    // TODO auto configure using AppTable ? (need a forwardRef() in the table component
    //@Optional() private appTable: AppTable<any>
    if (!table) throw new Error(`[actions-column] this column component must be inside a MatTable component`);
  }

  ngOnInit(): void {
    this.cd.detectChanges();
    this.table.addColumnDef(this.columnDef);
    this.canCancel = this.canCancel ?? this.cancelOrDeleteClick.observed;
    this.canConfirm = this.canConfirm ?? this.confirmEditCreateClick.observed;
    this.canDelete = this.canDelete ?? this.cancelOrDeleteClick.observed;
    this.canConfirmAndAdd = this.canConfirmAndAdd ?? this.confirmAndAddClick.observed;
    this.canBackward = this.canBackward ?? this.backward.observed;
    this.canForward = this.canForward ?? this.forward.observed;
    this.dirtyIcon = this.dirtyIcon === true ? 'star' : this.dirtyIcon === false ? null : this.dirtyIcon ?? 'star';
  }

  ngOnDestroy() {
    this.table.removeColumnDef(this.columnDef);
    this.optionsClick.complete();
    this.optionsClick.unsubscribe();
    this.cancelOrDeleteClick.complete();
    this.cancelOrDeleteClick.unsubscribe();
    this.confirmAndAddClick.complete();
    this.confirmAndAddClick.unsubscribe();
    this.backward.complete();
    this.backward.unsubscribe();
    this.forward.complete();
    this.forward.unsubscribe();
  }
}
