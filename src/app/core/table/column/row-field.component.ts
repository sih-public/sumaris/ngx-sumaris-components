import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { PredefinedColors } from '@ionic/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { AppFloatLabelType, FormFieldDefinition } from '../../../shared/form/field.model';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { AsyncTableElement, TableElement } from '@e-is/ngx-material-table';

@Component({
  selector: 'app-row-field',
  styleUrls: ['./row-field.component.scss'],
  templateUrl: './row-field.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppRowField implements OnInit, OnDestroy {
  @ViewChild(MatColumnDef) columnDef: MatColumnDef;

  @Input() name!: string;
  @Input() definition: FormFieldDefinition;
  @Input() definitionFn: (row: TableElement<any> | AsyncTableElement<any>) => FormFieldDefinition;
  @Input() headerI18n: string;
  @Input() sortable = false;
  @Input() resizable = false;
  @Input() required: boolean;
  @Input() readonly = false;
  @Input() sticky = false;
  @Input() draggable = false;
  @Input() disabled: boolean;
  @Input() placeholder: string;
  @Input() compact = false;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() appearance: MatFormFieldAppearance;
  @Input() tabindex: number;
  @Input() autofocus: boolean;

  @Input() clearable: boolean;
  @Input() chipColor: ThemePalette | PredefinedColors = null;
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;
  @Input() debug = false;

  protected logPrefix = '[row-field]';

  constructor(
    // protected translate: TranslateService,
    protected cd: ChangeDetectorRef,
    // protected injector: Injector,
    public matTable: MatTable<any>
  ) {
    if (!matTable) {
      throw new Error(`${this.logPrefix} this column component must be inside a MatTable`);
    }
  }

  ngOnInit() {
    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
  }
}
