import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { AsyncTableElement, TableElement } from '@e-is/ngx-material-table';
import { MatColumnDef, MatTable } from '@angular/material/table';
import { MatMenuTrigger } from '@angular/material/menu';
import { AppTable } from '../table.class';
import { AppAsyncTable } from '../async-table.class';
import { Subject, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-nav-actions-column',
  styleUrls: ['./actions-column.component.scss'],
  templateUrl: './nav-actions-column.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavActionsColumnComponent<T extends TableElement<any> | AsyncTableElement<any> = TableElement<any>> implements OnInit, OnDestroy {
  _subscription = new Subscription();
  _forwardEventSubject = new Subject<{ event: Event; row: T }>();
  _backwardEventSubject = new Subject<{ event: Event; row: T }>();

  @ViewChild(MatColumnDef) columnDef: MatColumnDef;
  @ViewChild(MatMenuTrigger) matMenuTrigger: MatMenuTrigger;

  @Input() appTable: AppTable<any> | AppAsyncTable<any>;
  @Input() matColumnDef = 'actions';
  @Input() style: 'table' | 'mat-table' = 'table';
  @Input({ transform: booleanAttribute }) stickyEnd = false;
  @Input({ transform: booleanAttribute }) showPending: boolean;
  @Input() dirtyIcon: boolean | string;
  @Input() optionsTitle = 'COMMON.BTN_OPTIONS';
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;
  @Input() cellTemplate: TemplateRef<{ $implicit: T }>;
  @Input() throttleTime = 250;

  @Output() optionsClick = new EventEmitter<Event>();

  constructor(
    private matTable: MatTable<any>,
    private cd: ChangeDetectorRef,
    @Optional() appTable: AppTable<any>,
    @Optional() appAsyncTable: AppAsyncTable<any>
  ) {
    if (!matTable) throw new Error(`[actions-column] this column component must be inside a MatTable component`);
    this.appTable = appTable ?? appAsyncTable;
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    // Default values
    this.showPending = this.showPending ?? true;
    this.dirtyIcon = this.dirtyIcon === true ? 'star' : this.dirtyIcon === false ? null : this.dirtyIcon ?? 'star';

    this.cd.detectChanges();
    this.matTable.addColumnDef(this.columnDef);

    if (!this.appTable)
      throw new Error(`[nav-actions-column] No AppTable or AppAsyncTable found. Please define a provider or set input '[appTable]'`);

    this._subscription.add(
      this._forwardEventSubject
        .pipe(
          throttleTime(this.throttleTime) // Avoid too many event, when leaving Tab key pressed
        )
        .subscribe(({ event, row }) => this.appTable.confirmAndForward(event, row as any))
    );

    this._subscription.add(
      this._backwardEventSubject
        .pipe(
          throttleTime(this.throttleTime) // Avoid too many event, when leaving Back-Tab key pressed
        )
        .subscribe(({ event, row }) => this.appTable.confirmAndBackward(event, row as any))
    );
  }

  ngOnDestroy() {
    this.matTable.removeColumnDef(this.columnDef);
    this.optionsClick.complete();
    this.optionsClick.unsubscribe();
    this._subscription.unsubscribe();
  }

  protected cancelOrDelete(event: Event, row: T) {
    this.appTable.cancelOrDelete(event, row as any);
  }

  protected confirmAndForward(event: Event, row: T) {
    this._forwardEventSubject.next({ event, row });
  }

  protected confirmAndBackward(event: Event, row: T) {
    this._backwardEventSubject.next({ event, row });
  }
}
