import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TableSelectColumnsComponent } from './table-select-columns.component';
import { TranslateModule } from '@ngx-translate/core';
import { IsAllSelectedPipe, IsNotAllSelectedPipe } from './table.pipes';
import { AppRowField } from './column/row-field.component';
import { ResizableModule } from '../../shared/directives/resizable/resizable.module';
import { ActionsColumnComponent } from './column/actions-column.component';
import { NavActionsColumnComponent } from './column/nav-actions-column.component';

const components = [
  // Components
  TableSelectColumnsComponent,
  ActionsColumnComponent,
  NavActionsColumnComponent,
  AppRowField,
  // Pipes
  IsAllSelectedPipe,
  IsNotAllSelectedPipe,
];

@NgModule({
  imports: [SharedModule, TranslateModule.forChild(), ResizableModule],

  declarations: components,
  exports: [TranslateModule, ...components],
})
export class AppTableModule {}
