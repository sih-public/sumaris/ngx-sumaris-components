import { AfterViewInit, ChangeDetectorRef, Directive, EventEmitter, Injector, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject, EMPTY, merge, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, first, mergeMap, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { EntityUtils, IEntity } from '../services/model/entity.model';
import { AlertController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnItem, TableSelectColumnsComponent } from './table-select-columns.component';
import { Location } from '@angular/common';
import { ErrorCodes } from '../services/errors';
import { AppFormUtils, CanSave, IAppForm, OnReady } from '../form/form.utils';
import { LocalSettingsService } from '../services/local-settings.service';
import { TranslateService } from '@ngx-translate/core';
import {
  MatAutocompleteConfigHolder,
  MatAutocompleteFieldAddOptions,
  MatAutocompleteFieldConfig,
} from '../../shared/material/autocomplete/material.autocomplete.config';
import { CompletableEvent, createPromiseEventEmitter, emitPromiseEvent } from '../../shared/events';
import { changeCaseToUnderscore, isEmptyArray, isNil, isNotNil, toBoolean } from '../../shared/functions';
import { waitFor, waitForFalse, WaitForOptions, waitForTrue } from '../../shared/observables';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { Alerts } from '../../shared/alerts';
import { CdkColumnDef } from '@angular/cdk/table';
import { LoadResult } from '../../shared/services/entity-service.class';
import { FormErrorTranslateOptions, FormErrorTranslator, IFormPathTranslator } from '../../shared/validator/form-error-adapter.class';
import {
  CellValueChangeListener,
  DEFAULT_PAGE_SIZE,
  DEFAULT_PAGE_SIZE_OPTIONS,
  DEFAULT_REQUIRED_COLUMNS,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  SaveActionType,
  SETTINGS_DISPLAY_COLUMNS,
  SETTINGS_PAGE_SIZE,
  SETTINGS_SORTED_COLUMN,
} from './table.model';
import { AsyncTableElement } from '@e-is/ngx-material-table';
import { EntitiesAsyncTableDataSource } from './entities-async-table-datasource.class';
import { AppTableUtils } from './table.utils';

// @dynamic
// noinspection DuplicatedCode
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AppAsyncTable<T extends IEntity<T, ID>, F = any, ID = number>
  implements OnInit, OnDestroy, AfterViewInit, IAppForm, CanSave, IFormPathTranslator
{
  private _initialized = false;
  private _openingRow = false;
  private _subscription = new Subscription();
  private _dataSourceLoadingSubscription: Subscription;

  private _cellValueChangesDefs: {
    [key: string]: CellValueChangeListener;
  } = {};

  protected _enabled = true;
  protected _autocompleteConfigHolder: MatAutocompleteConfigHolder;
  protected allowRowDetail = true;
  protected readonly cd: ChangeDetectorRef;
  protected route: ActivatedRoute;
  protected router: Router;
  protected navController: NavController;
  protected location: Location;
  protected settings: LocalSettingsService;
  protected translate: TranslateService;
  protected modalCtrl: ModalController;
  protected alertCtrl: AlertController;
  protected toastController: ToastController;
  protected previouslyEditedRowId: number;
  protected formErrorAdapter: FormErrorTranslator;
  protected readonly destroySubject = new Subject<void>();

  excludesColumns: string[] = [];
  displayedColumns: string[];
  totalRowCount: number | null = null;
  visibleRowCount: number;
  errorTranslateOptions: FormErrorTranslateOptions;

  readonly readySubject = new BehaviorSubject<boolean>(false);
  readonly loadingSubject = new BehaviorSubject<boolean>(true);
  readonly savingSubject = new BehaviorSubject<boolean>(false);
  readonly touchedSubject = new BehaviorSubject<boolean>(false);
  readonly dirtySubject = new BehaviorSubject<boolean>(false);
  readonly errorSubject = new BehaviorSubject<string>(undefined);

  get error(): string {
    return this.errorSubject.value;
  }

  set error(error: string) {
    this.setError(error);
  }

  selection = new SelectionModel<AsyncTableElement<T>>(true, []);
  permanentSelection: SelectionModel<T> = null;
  permanentSelectionChangedPrevented = false;
  autocompleteFields: { [key: string]: MatAutocompleteFieldConfig };
  readonly mobile: boolean;

  // Table options
  @Input() settingsId: string;
  @Input() debug: boolean;
  @Input() i18nColumnPrefix = 'COMMON.';
  @Input() i18nColumnSuffix: string;
  @Input() autoLoad = true;
  @Input() readOnly: boolean;
  @Input() inlineEdition: boolean;
  @Input() focusFirstColumn = false;
  @Input() confirmBeforeDelete = false;
  @Input() confirmBeforeCancel = false;
  @Input() undoableDeletion = false;
  @Input() saveBeforeDelete: boolean;
  @Input() keepEditedRowOnSave: boolean;
  @Input() saveBeforeSort: boolean;
  @Input() saveBeforeFilter: boolean;
  @Input() propagateRowError = false;
  @Input() permanentSelectionAllowed = false; // Allow keep selected rows over page change or sort change
  @Input() openRowThrottleTime = 800; // Prevent double click - see issue sumaris-app#864

  @Input() defaultSortBy: string;
  @Input() defaultSortDirection: SortDirection;
  @Input() defaultPageSize = DEFAULT_PAGE_SIZE;
  @Input() defaultPageSizeOptions = DEFAULT_PAGE_SIZE_OPTIONS;

  // Focus manager
  @Input() focusColumn: string;

  get firstUserColumn(): string {
    return this.displayedColumns[RESERVED_START_COLUMNS.length];
  }

  get lastUserColumn(): string {
    return this.displayedColumns[this.displayedColumns.length - RESERVED_END_COLUMNS.length - 1];
  }

  @Input() set dataSource(value: EntitiesAsyncTableDataSource<T, F, ID>) {
    this.setDatasource(value);
  }

  get dataSource(): EntitiesAsyncTableDataSource<T, F, ID> {
    return this._dataSource;
  }

  @Input() set filter(value: F) {
    this.setFilter(value);
  }

  get filter(): F {
    return this._filter;
  }

  get empty(): boolean {
    return this.loading || this.totalRowCount === 0;
  }

  get selectedEntities(): T[] {
    return this.permanentSelection?.selected || AppTableUtils.getEntities(this.selection.selected);
  }

  @Output() onRefresh = new EventEmitter<any>();
  @Output() onOpenRow = new EventEmitter<AsyncTableElement<T>>();
  @Output() onNewRow = new EventEmitter<any>();
  @Output() onStartEditingRow = new EventEmitter<AsyncTableElement<T>>();
  @Output() onConfirmEditCreateRow = new EventEmitter<AsyncTableElement<T>>();
  @Output() onCancelOrDeleteRow = new EventEmitter<AsyncTableElement<T>>();
  @Output() onBeforeDeleteRows = createPromiseEventEmitter<boolean, { rows: AsyncTableElement<T>[] }>();
  @Output() onBeforeCancelRows = createPromiseEventEmitter<boolean, { rows: AsyncTableElement<T>[] }>();
  @Output() onBeforeSave = createPromiseEventEmitter<{ confirmed: boolean; save: boolean }, { action: SaveActionType; valid: boolean }>();
  @Output() onAfterDeletedRows = new EventEmitter<AsyncTableElement<T>[]>();
  @Output() onSort = new EventEmitter<any>();
  @Output() onDirty = new EventEmitter<boolean>();
  @Output() onError = new EventEmitter<string>();

  get dirty(): boolean {
    return this.dirtySubject.value;
  }

  get valid(): boolean {
    return this.dataSource.getEditingRows().every((row) => (row.editing ? row.valid : true));
  }

  get invalid(): boolean {
    return this.dataSource.getEditingRows().some((row) => (row.editing ? row.invalid : false));
  }

  get pending(): boolean {
    return this.dataSource.getEditingRows().some((row) => (row.editing ? row.pending : false));
  }

  get touched(): boolean {
    return this.touchedSubject.value;
  }

  get untouched(): boolean {
    return !this.touchedSubject.value;
  }

  disable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.sort) this.sort.disabled = true;
    this._enabled = false;
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  enable(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (this.sort) this.sort.disabled = false;
    this._enabled = true;
    if (!opts || opts.emitEvent !== false) this.markForCheck();
  }

  get enabled(): boolean {
    return this._enabled;
  }

  // FIXME: need to hidden buttons (in HTML), etc. when disabled
  @Input() set disabled(value: boolean) {
    if (value !== !this._enabled) {
      if (value) this.disable({ emitEvent: false });
      else this.enable({ emitEvent: false });
    }
  }

  get disabled(): boolean {
    return !this._enabled;
  }

  markAsDirty(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (!this.dirtySubject.closed && this.dirtySubject.value !== true) {
      this.dirtySubject.next(true);
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  markAsPristine(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    if (!this.dirtySubject.closed && this.dirtySubject.value !== false) {
      this.dirtySubject.next(false);
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  async markAsUntouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    let needEmitEvent = false;
    if (this.touchedSubject.value) {
      this.touchedSubject.next(false);
      needEmitEvent = true;
    }
    if (this.dirty) {
      for (const row of this.dataSource.getEditingRows()) {
        // Cancel the current editing row only if editing and if it was not previously saved
        if (row.id === -1) {
          await this.dataSource.delete(-1);
        } else if (row.editing) {
          await this.dataSource.cancel(row);
        }

        // Check if row as pristine
        await this.checkIfRowPristine(row, { onlySelf: true /* avoid propagation to table - done just after */ });
      }

      // Check if the table is pristine
      await this.checkIfPristine({ emitEvent: false /*will be done just after*/ });

      needEmitEvent = true;
    }
    this.previouslyEditedRowId = undefined;
    if (needEmitEvent && (!opts || opts.emitEvent !== false)) this.markForCheck();
  }

  /**
   * @deprecated prefer to use markAllAsTouched()
   * @param opts
   */
  markAsTouched(opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
    console.warn('TODO: Replace this call by markAllAsTouched() - because of changes in ngx-components >= 0.16.0');
    if (this.dataSource.hasSomeEditingRow()) {
      this.dataSource.getEditingRows().forEach((row) => row.validator?.markAllAsTouched());
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  markAllAsTouched(opts?: { emitEvent?: boolean }) {
    if (!this.touchedSubject.closed && this.touchedSubject.value !== true) {
      this.touchedSubject.next(true);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
    this.dataSource.getEditingRows().forEach((row) => row.validator?.markAllAsTouched());
  }

  markAsSaving(opts?: { emitEvent?: boolean }) {
    if (!this.savingSubject.closed && this.savingSubject.value !== true) {
      this.focusColumn = undefined; // unselect focus column
      this.savingSubject.next(true);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  markAsSaved(opts?: { emitEvent?: boolean }) {
    if (!this.savingSubject.closed && this.savingSubject.value !== false) {
      this.savingSubject.next(false);
      if (!opts || opts.emitEvent !== false) this.markForCheck();
    }
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    this.setLoading(true, opts);
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    this.setLoading(false, opts);
  }

  markAsReady(opts?: { emitEvent?: boolean }) {
    if (!this.readySubject.closed && this.readySubject.value !== true) {
      this.readySubject.next(true);

      // If subclasses implements OnReady
      if (typeof this['ngOnReady'] === 'function') {
        (this as any as OnReady).ngOnReady();
      }
    }
  }

  get loading(): boolean {
    return this.loadingSubject.value;
  }

  get loaded(): boolean {
    return !this.loadingSubject.value;
  }

  enableSort() {
    if (this.sort) this.sort.disabled = false;
  }

  disableSort() {
    if (this.sort) this.sort.disabled = true;
  }

  set pageSize(value: number) {
    this.defaultPageSize = value;
    if (this.paginator) {
      this.paginator.pageSize = value;
    }
  }

  get pageSize(): number {
    return (this.paginator && this.paginator.pageSize) || this.defaultPageSize || DEFAULT_PAGE_SIZE;
  }

  get pageOffset(): number {
    return (this.paginator && this.paginator.pageIndex * this.paginator.pageSize) || 0;
  }

  get sortActive(): string {
    return this.sort && this.sort.active;
  }

  get sortDirection(): SortDirection {
    return (this.sort && this.sort.direction && (this.sort.direction === 'desc' ? 'desc' : 'asc')) || undefined;
  }

  set sortDirection(value: SortDirection) {
    if (this.sort) {
      this.sort.direction = value;
    } else {
      this.defaultSortDirection = value;
    }
  }

  private _paginator: MatPaginator | null = null;

  @Input() set paginator(value: MatPaginator) {
    this._paginator = value;
  }

  get paginator(): MatPaginator {
    return this._paginator || this.childPaginator;
  }

  get destroyed(): boolean {
    return this.destroySubject?.closed !== false;
  }

  @ViewChild(MatTable, { static: false }) table: MatTable<T>;
  @ViewChild(MatPaginator, { static: false }) childPaginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  protected constructor(
    injector: Injector,
    protected columns: string[],
    protected _dataSource?: EntitiesAsyncTableDataSource<T, F, ID>,
    private _filter?: F
  ) {
    this.cd = injector.get(ChangeDetectorRef);
    this.route = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
    this.navController = injector.get(NavController);
    this.location = injector.get(Location);
    this.settings = injector.get(LocalSettingsService);
    this.translate = injector.get(TranslateService);
    this.modalCtrl = injector.get(ModalController);
    this.alertCtrl = injector.get(AlertController);
    this.toastController = injector.get(ToastController);
    this.formErrorAdapter = injector.get(FormErrorTranslator);
    this.mobile = this.settings.mobile;

    // Autocomplete fields
    this._autocompleteConfigHolder = new MatAutocompleteConfigHolder({
      getUserAttributes: (a, b) => this.settings.getFieldDisplayAttributes(a, b),
    });
    this.autocompleteFields = this._autocompleteConfigHolder.fields;
  }

  ngOnInit() {
    if (this._initialized) return; // Init only once
    this._initialized = true;

    // Set defaults
    this.readOnly = toBoolean(this.readOnly, this.dataSource?.config.readOnly || false); // read/write by default
    this.inlineEdition = !this.readOnly && toBoolean(this.inlineEdition, false); // force to false when readonly
    this.saveBeforeDelete = toBoolean(this.saveBeforeDelete, !this.readOnly); // force to false when readonly
    this.saveBeforeSort = toBoolean(this.saveBeforeSort, !this.readOnly); // force to false when readonly
    this.saveBeforeFilter = toBoolean(this.saveBeforeFilter, !this.readOnly); // force to false when readonly
    this.keepEditedRowOnSave = toBoolean(this.keepEditedRowOnSave, this.inlineEdition);
    this.errorTranslateOptions = this.errorTranslateOptions || { separator: ', ', pathTranslator: this }; // Can be override in subclasses constructors

    // Check ask user confirmation is possible
    if (this.confirmBeforeDelete && !this.alertCtrl) throw Error("Missing 'alertCtrl' or 'injector' in component's constructor.");

    // Defined unique id for settings for the page
    this.settingsId = this.settingsId || this.generateTableId();

    this.displayedColumns = this.getDisplayColumns();

    // Load the sorted columns, from settings
    {
      const sortedColumn = this.getSortedColumn();
      this.defaultSortBy = sortedColumn.id;
      this.defaultSortDirection = sortedColumn.start;
    }

    this.defaultPageSize = this.getPageSize();

    // Propagate error to event emitter
    this.registerSubscription(this.errorSubject.subscribe((value) => this.onError.emit(value)));

    // Propagate dirty to event emitter
    this.registerSubscription(this.dirtySubject.subscribe((value) => this.onDirty.emit(value)));

    // Propagate row dirty state to table
    this.registerSubscription(
      this.onStartEditingRow
        .pipe(
          filter((row) => row?.validator && true),
          mergeMap((row) =>
            row.validator.valueChanges.pipe(
              filter((row) => row.dirty),
              first(),
              // DEBUG
              //tap(() => console.debug("Propagate row's dirty to table..."))

              // Stop if next another row, or destroying
              takeUntil(this.onStartEditingRow),
              takeUntil(this.destroySubject)
            )
          )
        )
        .subscribe(() => this.markAsDirty())
    );

    // Call datasource refresh, on each refresh events
    this.registerSubscription(
      this.onRefresh
        .pipe(
          startWith<any, any>((this.autoLoad ? {} : 'skip') as any),
          switchMap((event: any) => {
            this.dirtySubject.next(false);
            this.selection.clear();
            if (event === 'skip') {
              return of(undefined);
            }
            if (!this._dataSource) {
              if (this.debug) console.debug('[table] Skipping data load: no dataSource defined');
              return of(undefined);
            }
            if (this.debug) console.debug('[table] Calling dataSource.watchAll()...');
            return this._dataSource.watchAll(this.pageOffset, this.pageSize, this.sortActive, this.sortDirection, this._filter);
          }),
          catchError((err) => {
            if (this.debug) console.error(err);
            this.setError((err && err.message) || err);
            return of(undefined); // Continue
          })
        )
        .subscribe((res) => this.updateView(res as LoadResult<T> | undefined))
    );

    // Listen dataSource loading events
    if (this._dataSource) this.listenDatasourceLoading(this._dataSource);

    // Permanent selection behavior
    if (this.permanentSelectionAllowed) this.initPermanentSelection();
  }

  ngAfterViewInit() {
    // Detect when parent ngOnInit() not call
    if (this.debug && !this.displayedColumns)
      console.warn(`[table] Missing 'displayedColumns'. Did you call parent ngOnInit() in component ${this.constructor.name} ?`);

    // Start listening sort and paginator events
    // noinspection JSIgnoredPromiseFromCall
    this.listenSortAndPaginationEvents();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();

    // Unsubscribe column value changes
    Object.keys(this._cellValueChangesDefs).forEach((col) => this.stopCellValueChanges(col, true));
    this._cellValueChangesDefs = {};

    this.readySubject.unsubscribe();
    this.loadingSubject.unsubscribe();
    this.savingSubject.unsubscribe();
    this.errorSubject.unsubscribe();
    this.dirtySubject.unsubscribe();

    this.onRefresh.unsubscribe();
    this.onOpenRow.unsubscribe();
    this.onNewRow.unsubscribe();
    this.onStartEditingRow.unsubscribe();
    this.onConfirmEditCreateRow.unsubscribe();
    this.onCancelOrDeleteRow.unsubscribe();
    this.onBeforeDeleteRows.unsubscribe();
    this.onBeforeCancelRows.unsubscribe();
    this.onBeforeSave.unsubscribe();
    this.onAfterDeletedRows.unsubscribe();
    this.onSort.unsubscribe();
    this.onDirty.unsubscribe();
    this.onError.unsubscribe();

    this.destroySubject.next();
    this.destroySubject.unsubscribe();
  }

  async updateView(
    res: LoadResult<T> | undefined,
    opts?: {
      emitEvent?: boolean;
    }
  ): Promise<void> {
    if (!res) return; // Skip (e.g error)
    if (res && res.data) {
      this.visibleRowCount = res.data.length;
      this.totalRowCount = isNotNil(res.total)
        ? res.total
        : ((this.paginator && this.paginator.pageIndex * (this.paginator.pageSize || DEFAULT_PAGE_SIZE)) || 0) + this.visibleRowCount;
      if (this.debug) console.debug(`[table] ${res.data.length} rows loaded`);
    } else {
      //if (this.debug) console.debug('[table] NO rows loaded');
      this.totalRowCount = 0;
      this.visibleRowCount = 0;
    }

    if (!opts || opts.emitEvent !== false) {
      await this.markAsUntouched({ emitEvent: false });
      this.markAsPristine({ emitEvent: false });
      this.markAsLoaded({ emitEvent: false });
    }

    this.markForCheck();
  }

  setDatasource(datasource: EntitiesAsyncTableDataSource<T, F, ID>) {
    if (this._dataSource) throw new Error('[table] dataSource already set !');
    if (datasource && this._dataSource !== datasource) {
      this._dataSource = datasource;
      if (this._initialized) this.listenDatasourceLoading(datasource);
    }
  }

  resetDataSource() {
    if (this._dataSourceLoadingSubscription) {
      this._dataSourceLoadingSubscription.unsubscribe();
      this._subscription.remove(this._dataSourceLoadingSubscription);
    }
    //this._dataSource?.close();
    this._dataSource = null;
  }

  addColumnDef(column: CdkColumnDef) {
    this.table.addColumnDef(column);
  }

  removeColumnDef(column: CdkColumnDef) {
    this.table.removeColumnDef(column);
  }

  async setFilter(filter: F, opts?: { emitEvent?: boolean; permanentSelectionChangedPrevented?: boolean }) {
    opts = opts || { emitEvent: true };

    // Prevent permanent selection change events
    this.permanentSelectionChangedPrevented = toBoolean(opts.permanentSelectionChangedPrevented, this.permanentSelectionChangedPrevented);

    if (this.saveBeforeFilter) {
      // if a dirty table is to be saved before filter
      if (this.dirty) {
        // Save
        if (await this.saveBeforeAction('filter')) {
          // Apply filter only if user didn't cancel the save or the save is ok
          this.applyFilter(filter, opts);
        }
      } else {
        // apply filter on non-dirty table
        this.applyFilter(filter, opts);
      }
    } else {
      // apply filter directly
      this.applyFilter(filter, opts);
    }
  }

  async confirmAndAdd(event?: Event, row?: AsyncTableElement<T>): Promise<boolean> {
    if (!(await this.confirmEditCreate(event, row))) {
      return false;
    }
    // Add row
    return this.addRow(event);
  }

  async confirmAndBackward(event?: Event, row?: AsyncTableElement<T>): Promise<boolean> {
    // Deleting edited row, if empty and not dirty
    if (this.dataSource.hasSomeEditingRow()) {
      for (const editingRow of this.dataSource.getEditingRows().filter((row) => row.id === -1 && row.invalid && !row.dirty)) {
        await this.deleteNewRow(event, editingRow);
      }

      // Wait deletion is done, then edit previous row (by id, because of reloading)
      await this.waitIdle();
      await this.editRowById(event, row.id, { focusColumn: this.lastUserColumn });
      return true;
    }

    // Edit previous row
    await this.editRow(event, row, { focusColumn: this.lastUserColumn });
    return true;
  }

  async confirmAndForward(event?: Event, row?: AsyncTableElement<T>): Promise<boolean> {
    if (!this.inlineEdition) return false;

    const confirmed = await this.confirmEditCreate(event, row);
    if (!confirmed) return false; // Not confirmed

    // Edit next row
    await this.editRowById(event, row.id + 1, { focusColumn: this.firstUserColumn });
    return true;
  }

  /**
   * Confirm the creation of the given row, or if not specified the currently edited row
   *
   * @param event
   * @param row
   */
  async confirmEditCreate(event?: Event, row?: AsyncTableElement<T>): Promise<boolean> {
    row = row || this.dataSource.getSingleEditingRow();
    if (!row || !row.editing) return true; // no row to confirm

    // Stop event
    event?.stopPropagation();

    // Confirmation edition or creation
    const confirmed = await row.confirmEditCreate();

    if (confirmed) {
      // Mark table as dirty (if row is dirty)
      if (row.dirty) {
        this.markAsDirty({ emitEvent: false /* because of resetError() */ });
      }

      // Clear error
      this.resetError();

      // Emit the confirm event
      this.onConfirmEditCreateRow.next(row);

      return true; // Continue
    }

    if (row.validator) {
      // NOT confirmed = row has error
      if (this.debug) {
        console.warn('[table] Cannot confirm row, because invalid');
        AppFormUtils.logFormErrors(row.validator, '[table] ');
      }

      // fix: mark all controls as touched to show errors
      row.validator.markAllAsTouched();

      // Compute row error, and propagate to table's error
      if (this.propagateRowError) {
        const error = this.getRowError(row);
        this.setError(error);
      }
    }

    // Not confirmed
    return false;
  }

  async cancelOrDelete(event: Event, row: AsyncTableElement<T>, opts?: { interactive?: boolean; keepEditing?: boolean }) {
    // Delete new row
    if (row.id === -1) {
      await this.deleteNewRow(event, row);
    }
    // Delete existing (but not editing) row
    else if (!row.editing) {
      await this.deleteExistingRow(event, row, opts);
    }
    // Cancel existing (and editing) row
    else {
      await this.cancelExistingRow(event, row, opts);
    }
  }

  async addRow(event?: Event, insertAt?: number, opts?: { focusColumn?: string; editing?: boolean }): Promise<boolean> {
    if (this.debug) console.debug('[table] Asking for new row...');
    if (!this._enabled) return false;

    // Use modal if inline edition is disabled
    if (!this.inlineEdition) {
      await this.openNewRowDetail(event);
      return false;
    }

    // Try to finish edited row first
    if (!(await this.confirmEditCreate())) {
      return false;
    }

    // Add new row
    const row = await this.addRowToTable(insertAt, opts);
    return !!row;
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    opts = {
      keepEditing: this.keepEditedRowOnSave,
      ...opts,
    };
    if (this.readOnly) {
      throw { code: ErrorCodes.TABLE_READ_ONLY, message: 'ERROR.TABLE_READ_ONLY' };
    }

    this.resetError();

    // Keep edited row id (should be done BEFORE confirmEditCreate() )
    const editedRow = this.dataSource.getSingleEditingRow();
    this.previouslyEditedRowId = opts.keepEditing ? (editedRow?.editing ? editedRow.id : undefined) || this.singleSelectedRow?.id : undefined;
    const previouslyEditedRowId = this.previouslyEditedRowId;
    const previouslyEditedData = opts.keepEditing
      ? (isNotNil(previouslyEditedRowId) && editedRow?.currentData) || this.singleSelectedRow?.currentData
      : undefined;

    if (!(await this.confirmEditCreate())) {
      throw { code: ErrorCodes.TABLE_INVALID_ROW_ERROR, message: 'ERROR.TABLE_INVALID_ROW_ERROR' };
    }

    // Mark as saving
    this.markAsSaving();

    try {
      // Calling service save()
      if (this.debug) console.debug('[table] Calling dataSource.save()...');
      const isOK = await this._dataSource.save();

      if (isOK) this.markAsPristine();

      return isOK;
    } catch (err) {
      this.setError((err && err.message) || err);
      throw err;
    } finally {
      this.markAsSaved();

      // Restore previous row
      if (isNotNil(previouslyEditedRowId)) {
        await this.selectRowByIdOrData(previouslyEditedRowId, previouslyEditedData);
      }
    }
  }

  async cancel(event?: Event, opts?: { interactive?: boolean }) {
    // Check confirmation
    if ((!opts || opts.interactive !== false) && this.dirty && (this.confirmBeforeCancel || this.onBeforeCancelRows.observed)) {
      event?.stopPropagation();
      if (!(await this.canCancelRows())) {
        return;
      }
    }

    this.emitRefresh();
  }

  async duplicateRow(
    event?: Event,
    row?: AsyncTableElement<T>,
    opts?: {
      skipProperties?: string[];
    }
  ) {
    event?.stopPropagation();

    row = row || this.singleSelectedRow;
    if (!row || !(await this.confirmEditCreate(event, row))) {
      return false;
    }

    const newRow = await this.addRowToTable(row.id + 1);
    if (!newRow) throw new Error('Cannot add new row to table');
    const json = { ...row.currentData, id: null };

    // Reset some properties (e.g. rankOrder, etc)
    if (opts && opts.skipProperties) {
      const newData = newRow.currentData;
      opts.skipProperties.forEach((key) => (json[key] = newData[key]));
    }

    if (newRow.validator) {
      newRow.validator.patchValue(json);
      newRow.validator.markAsDirty();
    } else {
      if (newRow.currentData?.fromObject) {
        newRow.currentData.fromObject(json);
      } else {
        newRow.currentData = json;
      }
      this.markAsDirty();
    }

    // select
    await this.clickRow(undefined, newRow);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    // DEBUG
    //console.debug('isAllSelected. lengths', this.selection.selected.length, this.totalRowCount);

    return this.selection.selected.length === this.totalRowCount || this.selection.selected.length === this.visibleRowCount;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  async masterToggle() {
    if (this.loading) return;
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      const rows = this._dataSource.getRows();
      this.selection.setSelection(...rows);
    }
  }

  async deleteSelection(event: Event, opts?: { interactive?: boolean }): Promise<number> {
    const deleteCount = await this.deleteRows(event, this.selection.selected, opts);
    if (deleteCount > 0) this.selection.clear();
    return deleteCount;
  }

  /**
   *
   * @param event
   * @param row
   * @param opts Use interactive=false to avoid user interaction (e.g. user confirmation)
   *  And to force deletion even if table is busy
   */
  async deleteRow(event: Event | null, row: AsyncTableElement<T>, opts?: { interactive?: boolean }): Promise<boolean> {
    const deleteCount = await this.deleteRows(event, [row], opts);
    return deleteCount === 1;
  }

  /**
   *
   * @param event
   * @param rows
   * @param opts Use interactive=false to avoid user interaction (e.g. user confirmation)
   *    And to force deletion even if table is busy
   */
  async deleteRows(event: Event | null, rows: AsyncTableElement<T>[], opts?: { interactive?: boolean }): Promise<number> {
    if (this.readOnly) {
      throw { code: ErrorCodes.TABLE_READ_ONLY, message: 'ERROR.TABLE_READ_ONLY' };
    }
    if (event?.defaultPrevented) return 0; // SKip
    event?.preventDefault();
    this.resetError();

    if (!this._enabled || isEmptyArray(rows)) return 0; // Skip is disabled, or no rows to delete

    if (this.loading && (!opts || opts.interactive !== false)) {
      console.warn('[app-table] Skip deleteRows() because table is busy (loading). Use opts.interactive = false to force deletion');
      return 0; // Skip if loading
    }

    // Make sure to keep newly created row
    const editedRow = this.dataSource.getSingleEditingRow();
    if (editedRow?.id === -1 && editedRow.editing && !rows.includes(editedRow)) {
      const confirmed = await this.confirmEditCreate();
      if (!confirmed) return 0; // Cannot delete (e.g. edited row is invalid)
    }

    // Check if can delete
    const canDelete = await this.canDeleteRows(rows, opts);
    if (!canDelete) return 0; // Cannot delete

    // Reverse row order (on a copy)
    // This is a workaround, need because row.delete() has async execution
    // and index cache is updated with a delay
    let tempRows = rows.slice().sort((a, b) => (a.id > b.id ? -1 : 1));

    const deletedRows: AsyncTableElement<T>[] = [];

    // If data need to be saved first
    if (this.saveBeforeDelete) {
      // Exclude invalid rows (because of save() will fail, when exists some invalid rows)
      tempRows = tempRows.filter((row) => {
        // Delete the row :
        // - if newly created row (id = -1),
        // - or if invalid and not editing (= not cancellable)
        if (row.id === -1 || (!row.editing && row.invalid) /*do not use !valid because if row is disabled, it will be always !valid */) {
          if (this.debug) console.debug(`[table] Delete row #${row.id}`);
          this.selection.deselect(row);
          row.delete();
          this.visibleRowCount--;
          this.totalRowCount--;
          deletedRows.push(row);
          return false; // Exclude from the list to delete using the service
        }
        // Cancel the row (and mark as pristine), when not valid but in edition
        else if (row.editing && !row.valid) {
          this.selection.deselect(row);
          row.cancel();
          // Mark row as pristine (if possible)
          this.checkIfRowPristine(row, { emitEvent: false, onlySelf: true /*avoid propagation to table*/ });
          return true; // Keep the row. the row will be deleted after the save
        }

        return true;
      });

      // Apply save, only if there is still some rows to delete
      // WARN: If no more rows (e.g. because all has been cancelled) then continue anyway to clear editedRow and selection (issue IMAGINE-669)
      if (deletedRows.length || tempRows.length) {
        // Save data (e.g. when using memory service)
        const saved = await this.saveBeforeAction('delete');
        if (!saved) {
          // Stop if save cancelled or save failed
          return;
        }
      }
    }

    try {
      // Apply deletion on datasource
      // If no more rows to delete, continue anyway to clear editedRow and selection (issue IMAGINE-669)
      if (tempRows.length) {
        if (this.debug) console.debug(`[table] Delete ${tempRows.length} rows...`);
        await this._dataSource.deleteAll(tempRows);
      }

      // DO not update manually, because watchALl().subscribe() will update this count
      //this.totalRowCount -= deleteCount;
      //this.visibleRowCount -= deleteCount;

      // Deselect deleted rows
      this.selection.deselect(...rows);

      this.markAsDirty({ emitEvent: false /*markForCheck() is called just after*/ });
      this.markForCheck();

      this.onAfterDeletedRows.next(rows);

      return rows.length;
    } catch (err) {
      this.setError((err && err.message) || err);
      throw err;
    }
  }

  async selectRowById(id: number): Promise<boolean> {
    if (id === undefined) return false;
    await this.waitIdle();
    const row = this.dataSource.getRow(id);
    if (!row) return false;
    return this.clickRow(null, row);
  }

  /**
   * Try to select row by data. Will use equals() to find same row's data
   *
   * @param data
   */
  async selectRowByData(data: T): Promise<boolean> {
    if (data === undefined) return Promise.resolve(false);
    await this.waitIdle();
    const row = this.dataSource.getRows().find((row) => this.equals(row.currentData, data));
    if (!row) return false;
    return this.clickRow(null, row);
  }

  toggleSelectRow(event: Event, row: AsyncTableElement<T>) {
    // /!\ DO NOT prevent default, otherwise the checkbox will not be updated
    //event?.preventDefault();
    event.stopPropagation();
    this.selection.toggle(row);
  }

  async clickRow(event: Event | undefined, row: AsyncTableElement<T>): Promise<boolean> {
    if (this.loading) {
      // Wait while loading, and loop
      if (this.debug) console.debug('[table] Waiting before apply clickRow() (datasource is busy)...');
      await this.waitIdle({ timeout: 2000 });
    }

    // DEBUG
    //console.debug("[table] Detect click on row");

    // Cancelled by event
    if (event?.defaultPrevented) {
      if (this.debug) console.debug('[table] clickRow event is defaultPrevented');
      return false;
    }

    // Already in edition
    if (row.id === -1 || row.editing) {
      if (this.debug) console.debug('[table] Row already editing. Skip clickRow event');
      return true;
    }

    // Open the detail page (if not inline editing)
    if (!this.inlineEdition) {
      if (this._openingRow) return; // Prevent double click

      if (event) {
        event.stopPropagation();
        event.preventDefault();
      }

      this._openingRow = true; // Prevent double click
      this.markAsLoading();
      this.selection.clear();

      this.openRow(row.currentData.id, row)
        .catch((err) => console.error('Failed to open row', err)) // Continue
        .then(() => {
          this.markAsLoaded();

          // Prevent double click (use an open throttle time)
          if (this.openRowThrottleTime > 0) {
            setTimeout(() => {
              this._openingRow = false;
            }, this.openRowThrottleTime);
          } else {
            this._openingRow = false;
          }
        });

      return true;
    }

    // Start editing row
    return this.editRow(event, row, { focusColumn: undefined /*force to use the click target*/ });
  }

  async moveRow(id: number, direction: number) {
    await this.dataSource.move(id, direction);
  }

  ready(opts?: WaitForOptions): Promise<void> {
    return waitForTrue(this.readySubject, { stop: this.destroySubject, ...opts });
  }

  waitIdle(opts?: WaitForOptions): Promise<void> {
    if (!this.loadingSubject.value) return Promise.resolve();
    return waitForFalse(this.loadingSubject, { stop: this.destroySubject, ...opts });
  }

  async openSelectColumnsModal(event?: Event): Promise<any> {
    event?.preventDefault();

    // Copy current columns (deep copy)
    const columns = this.getCurrentColumns();

    const hasTopModal = !!(await this.modalCtrl.getTop());
    const modal = await this.modalCtrl.create({
      component: TableSelectColumnsComponent,
      componentProps: { columns },
      cssClass: hasTopModal && 'stack-modal',
    });

    // Open the modal
    await modal.present();

    // On dismiss
    const { data } = await modal.onDidDismiss();
    if (!data) return; // CANCELLED

    // Apply columns
    const userColumns = (data || []).filter((c) => c.canHide === false || c.visible).map((c) => c.name) || [];
    this.displayedColumns = RESERVED_START_COLUMNS.concat(userColumns).concat(RESERVED_END_COLUMNS);
    this.markForCheck();

    // Update user settings
    await this.savePageSettings(userColumns, SETTINGS_DISPLAY_COLUMNS);
  }

  trackByFn(index: number, row: AsyncTableElement<T>) {
    return row.id;
  }

  doRefresh(event?: CompletableEvent) {
    this.emitRefresh(event);

    // When target wait for a complete (e.g. IonRefresher)
    if (event?.target && event.target.complete) {
      setTimeout(async () => {
        await this.waitIdle();
        event.target.complete();
      });
    }
  }

  getCurrentColumns(): ColumnItem[] {
    const hiddenColumns = this.columns.slice(RESERVED_START_COLUMNS.length).filter((name) => this.displayedColumns.indexOf(name) === -1);
    return this.displayedColumns
      .concat(hiddenColumns)
      .filter((name) => !RESERVED_START_COLUMNS.includes(name) && !RESERVED_END_COLUMNS.includes(name) && !this.excludesColumns.includes(name))
      .map((name) => ({
        name,
        label: this.getI18nColumnName(name),
        visible: this.displayedColumns.indexOf(name) !== -1,
        canHide: this.getRequiredColumns().indexOf(name) === -1,
      }));
  }

  async escapeEditingRow(event?: Event, row?: AsyncTableElement<T>) {
    row = row || this.dataSource.getSingleEditingRow();
    if (!row || !row.editing) return;

    // DEBUG
    //console.debug('[app-table] Cancel the row (keydown.escape)');

    if (event) {
      // Avoid to cancel the editor
      event.preventDefault();
      event.stopPropagation();
    }

    // If new row (no id)
    if (row.id === -1) {
      if (row.validator) {
        // If pending: Wait end of validation, then loop
        if (row.pending) {
          await AppFormUtils.waitWhilePending(row.validator);
        }

        // Row is invalid: delete the row
        if (row.invalid) {
          await this.deleteNewRow(event, row);
          return;
        }
      }
    }
    // If the row exists (has an id)
    else {
      // need to call cancel function (if confirmation will be called before)
      if (this.confirmBeforeCancel) {
        await this.cancelExistingRow(event, row, { keepEditing: false });
        return;
      }
    }

    // By default, try to confirm the row
    await this.confirmEditCreate(event, row);
  }

  translateFormPath(path: string): string {
    // Can be overridden by subclasses, to resolve all field name

    // Use columns key, has default name
    const i18nColumnKey = this.getI18nColumnName(path);
    return this.translate?.instant(i18nColumnKey) || i18nColumnKey;
  }

  /* -- protected method -- */

  protected async editRow(event: Event | undefined, row: AsyncTableElement<T>, opts?: { focusColumn?: string }): Promise<boolean> {
    if (!this._enabled || !this.inlineEdition) return false;
    if (this.dataSource.getEditingRows().includes(row)) return true; // Already the edited row
    if (event?.defaultPrevented) return false;

    if (!(await this.confirmEditCreate())) {
      return false;
    }

    if (!row.editing && !this.loading) {
      this.focusColumn = (opts && opts.focusColumn) || this.focusColumn;
      await this._dataSource.startEdit(row);
    }
    this.onStartEditingRow.emit(row);
    return true;
  }

  /**
   * Try to select row, by row.id, or by data. WIll use EntityUtils.equals()
   *
   * @param id
   * @param data
   * @protected
   */
  protected async selectRowByIdOrData(id: number, data?: T): Promise<boolean> {
    let done = false;
    try {
      // Select by row id (if NOT a new row)
      if (isNotNil(id) || id !== -1) {
        done = await this.selectRowById(id);
        if (done) return true;
        console.warn('[app-table] Save: Cannot reselect row by row.id: ', id);
      }

      // Try by data
      if (data) {
        done = await this.selectRowByData(data);
        if (done) return true;
        console.warn('[app-table] Save: Cannot reselect row by data: ', data);
      }

      return false;
    } catch (err) {
      // Log, but continue
      console.error((err && err.message) || err);
      return false;
    }
  }

  /**
   * return the selected row if unique in selection
   */
  protected get singleSelectedRow(): AsyncTableElement<T> {
    return this.selection.selected?.length === 1 ? this.selection.selected[0] : undefined;
  }

  protected async canDeleteRows(rows: AsyncTableElement<T>[], opts?: { interactive?: boolean }): Promise<boolean> {
    // Check using emitter
    if (this.onBeforeDeleteRows.observed) {
      try {
        const canDelete = await emitPromiseEvent(this.onBeforeDeleteRows, 'canDelete', {
          detail: { rows },
        });
        if (!canDelete) return false;
      } catch (err) {
        if (err === 'CANCELLED') return false; // User cancel
        console.error('Error while checking if can delete rows', err);
        throw err;
      }
    }

    // Ask user confirmation
    if (this.confirmBeforeDelete && (!opts || opts.interactive !== false)) {
      return this.askDeleteConfirmation(null, rows);
    }
    return true;
  }

  protected async canCancelRows(rows?: AsyncTableElement<T>[], opts?: { interactive?: boolean }): Promise<boolean> {
    // Get dirty rows
    rows = rows || this.dataSource.getRows().filter((row) => row.validator?.dirty);

    if (isEmptyArray(rows)) return true; // No dirty: OK

    // Check using emitter
    if (this.onBeforeCancelRows.observed) {
      try {
        const isCancel = await emitPromiseEvent(this.onBeforeCancelRows, 'canCancel', {
          detail: { rows },
        });
        if (!isCancel) return false;
      } catch (err) {
        if (err === 'CANCELLED') return false; // User cancel
        console.error('Error while checking if can cancel rows', err);
        throw err;
      }
    }

    // Ask user confirmation
    if (this.confirmBeforeCancel && (!opts || opts.interactive !== false)) {
      return this.askCancelConfirmation(null, rows);
    }
    return true;
  }

  protected async saveBeforeAction(saveAction: SaveActionType): Promise<boolean> {
    if (!this.dirty) {
      // Continue without save
      return true;
    }

    let save: boolean;
    switch (saveAction) {
      case 'delete':
        save = this.saveBeforeDelete;
        break;
      case 'filter':
        save = this.saveBeforeFilter;
        break;
      case 'sort':
        save = this.saveBeforeSort;
        break;
      default:
        save = true;
    }

    // Default behavior
    let confirmed = true;

    if (save) {
      if (this.onBeforeSave.observed) {
        // Ask confirmation
        try {
          const res = await emitPromiseEvent(this.onBeforeSave, 'beforeSave', {
            detail: { action: saveAction, valid: this.valid },
          });
          confirmed = res.confirmed;
          save = res.save;
        } catch (err) {
          if (err === 'CANCELLED') return false; // User cancel
          console.error('Error while checking if can delete rows', err);
          throw err;
        }
      }
    }

    if (confirmed) {
      if (save) {
        // User confirmed save
        const saved = await this.save();
        this.markAsDirty(); // Restore dirty flag
        return saved;
      }
      return true; // No save but continue action
    }

    return false; // User cancel the action
  }

  /**
   * Open a row detail view. By default, will to open row detail page.
   * Can be overridden by subclasses, BUT prefer to subscribe on onOpenRow
   *
   * @param id
   * @param row
   * @protected
   */
  protected async openRow(id: ID, row: AsyncTableElement<T>): Promise<boolean> {
    if (this.allowRowDetail) {
      if (this.debug && this.dirty) {
        console.warn('[table] Opening row details, but table has unsaved changes!');
      }

      if (this.onOpenRow.observed) {
        this.onOpenRow.emit(row);
        return true;
      }

      // No ID defined: unable to open details
      if (isNil(id)) {
        console.warn('[table] Opening row details, but data has no id!');
        return false;
      }

      return this.navController.navigateForward(`${this.router.url}/${id}`, {
        queryParams: {},
      });
    }
    return false;
  }

  protected async openNewRowDetail(event?: any): Promise<boolean> {
    if (!this.allowRowDetail) return false;

    if (this.onNewRow.observed) {
      this.onNewRow.emit(event);
      return true;
    }

    return this.navController.navigateForward(`${this.router.url}/new`);
  }

  // can be overridden to add more required columns
  protected getRequiredColumns() {
    return DEFAULT_REQUIRED_COLUMNS;
  }

  protected getUserColumns(): string[] {
    return this.getPageSettings(SETTINGS_DISPLAY_COLUMNS);
  }

  protected getSortedColumn(): MatSortable {
    const data = this.getPageSettings(SETTINGS_SORTED_COLUMN);
    const parts = data && data.split(':');
    if (parts && parts.length === 2 && this.columns.includes(parts[0])) {
      return { id: parts[0], start: parts[1] === 'desc' ? 'desc' : 'asc', disableClear: false };
    }
    if (this.defaultSortBy) {
      return { id: this.defaultSortBy, start: this.defaultSortDirection || 'asc', disableClear: false };
    }
    return { id: 'id', start: 'asc', disableClear: false };
  }

  protected getPageSize(): number {
    const pageSize = this.getPageSettings<number>(SETTINGS_PAGE_SIZE);
    return pageSize || this.defaultPageSize;
  }

  protected getDisplayColumns(): string[] {
    let userColumns = this.getUserColumns();

    // No user override
    if (!userColumns) {
      // Return default, without columns to hide
      return this.columns.filter((column) => !this.excludesColumns.includes(column));
    }

    // Get fixed start columns
    const fixedStartColumns = this.columns.filter((c) => RESERVED_START_COLUMNS.includes(c));

    // Remove end columns
    const fixedEndColumns = this.columns.filter((c) => RESERVED_END_COLUMNS.includes(c));

    // Remove fixed columns from user columns
    userColumns = userColumns.filter((c) => !fixedStartColumns.includes(c) && !fixedEndColumns.includes(c) && this.columns.includes(c));

    // Add required columns if missing
    userColumns.push(
      ...this.getRequiredColumns().filter((c) => !fixedStartColumns.includes(c) && !fixedEndColumns.includes(c) && !userColumns.includes(c))
    );

    return (
      fixedStartColumns
        .concat(userColumns)
        .concat(fixedEndColumns)
        // Remove columns to hide
        .filter((column) => !this.excludesColumns.includes(column))
    );
  }

  /**
   * Recompute display columns
   *
   * @protected
   */
  protected updateColumns() {
    this.displayedColumns = this.getDisplayColumns();
    if (!this.loading) this.markForCheck();
  }

  protected registerSubscription(sub: Subscription) {
    this._subscription.add(sub);
  }

  protected unregisterSubscription(sub: Subscription) {
    this._subscription.remove(sub);
  }

  protected registerAutocompleteField<E = any, EF = any>(
    fieldName: string,
    options?: MatAutocompleteFieldAddOptions<E, EF>
  ): MatAutocompleteFieldConfig<E, EF> {
    return this._autocompleteConfigHolder.add(fieldName, options);
  }

  protected getI18nColumnName(columnName: string) {
    return (this.i18nColumnPrefix || '') + changeCaseToUnderscore(columnName).toUpperCase();
  }

  protected generateTableId() {
    // noinspection JSNonASCIINames
    const meta = this.constructor['ɵcmp'];
    // Get a component unique name - See https://stackoverflow.com/questions/60114682/how-to-access-components-unique-encapsulation-id-in-angular-9
    const componentId = (meta && (meta.selectors?.[0] || meta.id)) || this.constructor.name;
    const id =
      this.location
        .path(true)
        .replace(/[?].*$/g, '')
        .replace(/\/\d+/g, '_id') +
      '_' +
      componentId;
    //if (this.debug) console.debug('[table] id = ' + id);
    return id;
  }

  protected async addRowToTable(
    insertAt?: number,
    opts?: { focusColumn?: string; editing?: boolean; emitEvent?: boolean }
  ): Promise<AsyncTableElement<T> | undefined> {
    // Try to finish edited row first
    if (!(await this.confirmEditCreate())) {
      console.warn('[table] Cannot add new row, because the previous edited row cannot be confirmed');
      return undefined;
    }

    const editing = this.inlineEdition && (!opts || opts.editing !== false); // true by default, if inlineEdition

    const row = await this._dataSource.createNew(insertAt, { editing });
    if (!row) return undefined;

    if (row.editing) {
      // Update focused column
      this.focusFirstColumn = true;
      this.focusColumn = opts?.focusColumn || this.firstUserColumn;

      // Emit start editing event
      this.onStartEditingRow.emit(row);
    }

    this.totalRowCount++;
    this.visibleRowCount++;

    this.markAsDirty({ emitEvent: false /*markForCheck() is called just after*/ });

    // Emit event
    if (!opts || opts.emitEvent !== false) this.markForCheck();

    return row;
  }

  protected registerCellValueChanges(name: string, formPath?: string, emitInitialValue?: boolean): Observable<any> {
    formPath = formPath || name;
    emitInitialValue = emitInitialValue || false;
    let def = this._cellValueChangesDefs[name];
    if (def && (def.formPath !== formPath || def.emitInitialValue !== emitInitialValue)) {
      throw Error("Already register a cell value change for this name, with different 'formPath' or 'emitInitialValue'. Please use same arguments.");
    }

    // Not exists: register new definition
    if (!def) {
      if (this.debug) console.debug(`[table] New listener {${name}} for value changes on path ${formPath}`);
      def = {
        subject: new Subject<any>(),
        subscription: null,
        formPath,
        emitInitialValue,
      };
      this._cellValueChangesDefs[name] = def;

      // Start the listener, when editing starts
      this.registerSubscription(this.onStartEditingRow.subscribe((row) => this.startCellValueChanges(name, row)));
    }

    return def.subject;
  }

  protected setShowColumn(columnName: string, show: boolean, opts?: { emitEvent?: boolean }) {
    if (!this.excludesColumns.includes(columnName) !== show) {
      if (!show) {
        this.excludesColumns.push(columnName);
      } else {
        const index = this.excludesColumns.findIndex((value) => value === columnName);
        if (index >= 0) this.excludesColumns.splice(index, 1);
      }

      // Recompute display columns
      if (this.displayedColumns && (!opts || opts.emitEvent !== false)) {
        this.updateColumns();
      }
    }
  }

  protected getShowColumn(columnName: string): boolean {
    return !this.excludesColumns.includes(columnName);
  }

  protected startsWithUpperCase(input: string, search: string): boolean {
    return input && input.toUpperCase().startsWith(search);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected detectChanges() {
    this.cd.detectChanges();
  }

  protected async askDeleteConfirmation(event?: Event, rows?: AsyncTableElement<T>[]): Promise<boolean> {
    if (this.undoableDeletion) {
      // Special message, for undoable deletion
      return Alerts.askConfirmation(rows?.length === 1 ? 'CONFIRM.DELETE_ROW' : 'CONFIRM.DELETE_ROWS', this.alertCtrl, this.translate, event);
    }
    // Immediate deletion action
    return Alerts.askActionConfirmation(this.alertCtrl, this.translate, true, event);
  }

  protected async askCancelConfirmation(event?: Event, rows?: AsyncTableElement<T>[]): Promise<boolean> {
    return Alerts.askConfirmation(rows?.length === 1 ? 'CONFIRM.CANCEL_ROW' : 'CONFIRM.CANCEL_ROWS', this.alertCtrl, this.translate, event);
  }

  protected async askRestoreConfirmation(event?: Event): Promise<boolean> {
    return Alerts.askActionConfirmation(this.alertCtrl, this.translate, false, event);
  }

  protected async showToast(opts: ShowToastOptions) {
    if (!this.toastController) throw new Error("Missing toastController in component's constructor");
    return Toasts.show(this.toastController, this.translate, opts);
  }

  protected resetError(opts?: { emitEvent?: boolean }) {
    this.setError(undefined, opts);
  }

  protected getRowError(row?: AsyncTableElement<T>, opts?: FormErrorTranslateOptions): string {
    row = row || this.dataSource.getSingleEditingRow();
    if (!row || !this.formErrorAdapter) return undefined;

    return this.formErrorAdapter.translateFormErrors(row.validator, {
      ...this.errorTranslateOptions,
      ...opts,
    });
  }

  protected setError(value: string, opts?: { emitEvent?: boolean }) {
    if (this.errorSubject.value !== value) {
      this.errorSubject.next(value);
      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  /**
   * Compare data equality (default by id, using EntityUtils.equals())
   * Can be overridden to add additional properties to compare
   *
   * @param d1
   * @param d2
   * @protected
   */
  protected equals(d1: T, d2: T): boolean {
    return EntityUtils.equals(d1, d2, 'id');
  }

  protected markRowAsDirty(row?: AsyncTableElement<T>, opts?: { onlySelf?: boolean; emitEVent?: boolean }) {
    row = row || this.dataSource.getSingleEditingRow();
    if (row) row.validator?.markAsDirty(opts);
    this.markAsDirty(opts);
  }

  protected getPageSettings<T = any>(propertyName?: string): T {
    return this.settings.getPageSettings(this.settingsId, propertyName);
  }

  protected async savePageSettings(value: any, propertyName?: string) {
    await this.settings.savePageSetting(this.settingsId, value, propertyName);
  }

  protected emitRefresh(value?: any) {
    // Emit onRefresh if not closed (can happen if component is destroyed to early)
    if (!!this.onRefresh && !this.onRefresh.closed) {
      this.onRefresh.emit(value);
    }
  }

  /**
   * Initialize the permanent selection model
   *
   * @protected
   */
  protected initPermanentSelection() {
    if (this.permanentSelection) throw new Error('initPermanentSelection() already called!');

    this.permanentSelection = new SelectionModel<T>(true, []);

    // Listen sort change
    this.registerSubscription(
      this.onSort.subscribe(() => (this.permanentSelectionChangedPrevented = true)) // prevent selection change on sort
    );

    // Listen datasource update
    this.registerSubscription(
      this.dataSource.rowsSubject
        // restore the 'adding' selection (for example after a page or sort change)
        .subscribe((rows) => {
          this.permanentSelectionChangedPrevented = true;
          try {
            const permanentSelectionIds = (this.permanentSelection.selected || []).map(EntityUtils.getId);
            this.selection.setSelection(...rows.filter((row) => permanentSelectionIds.includes(AppTableUtils.getEntityId(row))));
            this.markForCheck();
          } finally {
            this.permanentSelectionChangedPrevented = false;
          }
        })
    );

    // Listen selection change
    this.registerSubscription(
      this.selection.changed.pipe(filter(() => !this.permanentSelectionChangedPrevented)).subscribe((selectionChange) => {
        // add to selection
        if (selectionChange.added.length) {
          this.permanentSelection.setSelection(
            ...(this.permanentSelection.selected || [])
              // Add new elements
              .concat(...AppTableUtils.getEntities(selectionChange.added).filter(EntityUtils.hasId))
              // Avoid duplicated entities (Mantis #55351)
              .filter(EntityUtils.arrayDistinctFilterFn)
          );
        }
        // remove from selection
        if (selectionChange.removed.length) {
          const removedIds = AppTableUtils.getEntityIds(selectionChange.removed);
          this.permanentSelection.setSelection(
            ...(this.permanentSelection.selected || []).filter((entity) => EntityUtils.hasId(entity) && !removedIds.includes(entity.id))
          );
        }
        this.markForCheck();
      })
    );
  }

  /* -- private method -- */

  private async listenSortAndPaginationEvents() {
    if (!this.table) {
      // DEBUG only -- alert user that table not found in template
      if (this.debug) {
        setTimeout(
          () =>
            !this.table &&
            console.warn(`[table] Missing <mat-table> in the HTML template (after waiting 500ms)! Component: ${this.constructor.name}`),
          500
        );
      }

      // Make sure to wait the table
      await waitFor(() => !!this.table, { stop: this.destroySubject, stopError: false /*avoid error when destorying the table*/ });
    }

    this.registerSubscription(
      merge(
        // Listen sort events
        (this.sort &&
          this.sort.sortChange.pipe(
            filter(() => !this.sort.disabled),
            mergeMap(async () => this.saveBeforeAction('sort')),
            filter((res) => res === true),
            // Save sort in settings
            tap(() => {
              const value = [this.sort.active, this.sort.direction || 'asc'].join(':');
              this.savePageSettings(value, SETTINGS_SORTED_COLUMN);
            })
          )) ||
          EMPTY,

        // Listen paginator events
        (this.paginator &&
          this.paginator.page.pipe(
            mergeMap((_) => this.saveBeforeAction('sort')),
            filter((saved) => saved === true),
            // Save page size in settings
            tap(() => this.savePageSettings(this.paginator.pageSize, SETTINGS_PAGE_SIZE))
          )) ||
          EMPTY
      )
        // Refresh on any sort or paginator events
        .subscribe((value) => {
          this.onSort.emit(value);
          this.emitRefresh(value);
        })
    );

    // If the user changes the sort order, reset back to the first page.
    if (this.sort && this.paginator) {
      this.registerSubscription(this.sort.sortChange.pipe(filter(() => !this.sort.disabled)).subscribe(() => (this.paginator.pageIndex = 0)));
    }
  }

  private async editRowById(event: Event | undefined, id: number, opts?: { focusColumn?: string }) {
    if (id < 0) return;
    if (id >= this.visibleRowCount) {
      await this.addRow(event, undefined, { ...opts, editing: true });
    } else {
      const row = this.dataSource.getRow(id);
      await this.editRow(event, row, opts);
    }
  }

  private setLoading(value: boolean, opts?: { emitEvent?: boolean }) {
    if (!this.loadingSubject.closed && this.loadingSubject.value !== value) {
      this.loadingSubject.next(value);

      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  private async deleteNewRow(event: Event | undefined, row: AsyncTableElement<T>) {
    // Make sure row will be cancelled, and NOT deleted
    if (row.id !== -1) throw new Error('Row must have id = -1');
    event?.stopPropagation();

    this.selection.clear();
    await this._dataSource.cancelOrDelete(row);
    this.resetError();

    this.onCancelOrDeleteRow.next(row);
    this.totalRowCount--;
    this.visibleRowCount--;
  }

  private async deleteExistingRow(event: Event | null, row: AsyncTableElement<T>, opts?: { interactive?: boolean }) {
    // Make sure row will be cancelled, and NOT deleted
    if (row.id === -1) throw new Error('Row must have an id');

    if (event?.defaultPrevented) return 0; // SKip
    event?.preventDefault();

    await this.deleteRow(null, row, opts);
  }

  private async cancelExistingRow(event: Event | undefined, row: AsyncTableElement<T>, opts?: { interactive?: boolean; keepEditing?: boolean }) {
    // Make sure row will be cancelled, and NOT deleted
    if (row.id === -1 || !row.editing) throw new Error('Row cannot be canceling, but only deleting');

    const confirmed = !opts || opts.interactive !== false;

    // Ask user confirmation, if cancel
    if (!confirmed && row.dirty && (this.confirmBeforeCancel || this.onBeforeCancelRows.observed)) {
      event?.stopPropagation();
      if (!(await this.canCancelRows([row], opts))) {
        return;
      }
    }

    const keepEditing = row.editing && (!opts || opts.keepEditing !== false);
    await this._dataSource.cancelOrDelete(row);
    this.onCancelOrDeleteRow.next(row);

    // Mark row as pristine
    await this.checkIfRowPristine(row);

    // Restore editing state
    if (keepEditing) {
      await this.editRow(undefined, row);
    }
  }

  private async checkIfRowPristine(row: AsyncTableElement<T>, opts?: { emitEvent?: boolean; onlySelf?: boolean }) {
    // Mark row as pristine
    const markRowAsPristine = this.dataSource?.config.restoreOriginalDataOnCancel === true;
    if (markRowAsPristine) {
      row.validator?.markAsPristine();

      // Check if table is now pristine
      if (!opts || opts.onlySelf !== true) {
        await this.checkIfPristine(opts);
      }
    }
  }

  private async checkIfPristine(opts?: { emitEvent?: boolean }) {
    if (!this.dirty) return; // Already pristine

    const rows = this._dataSource.getRows();
    const pristine = (rows || []).findIndex((row) => row.dirty) === -1;
    if (pristine) this.markAsPristine(opts);
  }

  private applyFilter(filter: F, opts: { emitEvent?: boolean }) {
    if (this.debug) console.debug('[table] Applying filter', filter);
    this._filter = filter;
    if (opts && opts.emitEvent) {
      if (this.paginator && this.paginator.pageIndex > 0) {
        this.paginator.pageIndex = 0;
      }
      this.emitRefresh();
    }
  }

  private listenDatasourceLoading(dataSource: EntitiesAsyncTableDataSource<T, F, ID>) {
    if (!dataSource) throw new Error('[table] dataSource not set !');

    // Cleaning previous subscription on datasource
    if (isNotNil(this._dataSourceLoadingSubscription)) {
      if (this.debug) console.debug('[table] Many call to listenDatasource(): Cleaning previous subscriptions...');
      this._dataSourceLoadingSubscription.unsubscribe();
      this.unregisterSubscription(this._dataSourceLoadingSubscription);
    }

    // Propage loading to table
    this._dataSourceLoadingSubscription = this._dataSource.loadingSubject
      .pipe(
        distinctUntilChanged(),

        // If changed to True: propagate as soon as possible
        tap((loading) => loading && this.setLoading(true)),

        // If changed to False: wait 250ms before propagate (to make sure the spinner has been displayed)
        debounceTime(250),
        tap((loading) => !loading && this.setLoading(false))
      )
      .subscribe();
    this.registerSubscription(this._dataSourceLoadingSubscription);
  }

  private startCellValueChanges(name: string, row: AsyncTableElement<T>) {
    const def = this._cellValueChangesDefs[name];
    if (!def) {
      console.warn('[table] Listener with name {' + name + '} not registered! Please call registerCellValueChanges() before;');
      return;
    }
    // Stop previous subscription
    if (def.subscription) {
      def.subscription.unsubscribe();
      def.subscription = null;
    } else {
      if (this.debug) console.debug(`[table] Start values changes on row path {${def.formPath}}`);
    }

    // Listen value changes, and redirect to event emitter
    const control = row.validator && AppFormUtils.getControlFromPath(row.validator, def.formPath);
    if (!control) {
      console.warn(`[table] Could not listen cell changes: no validator or invalid form path {${def.formPath}}`);
    } else {
      def.subscription = control.valueChanges
        .pipe(
          // don't emit if control is disabled
          filter(() => control.enabled)
        )
        .subscribe((value) => def.subject.next(value));

      // Emit the actual value
      if (def.emitInitialValue !== false) {
        def.subject.next(control.value);
      }
    }
  }

  private stopCellValueChanges(name: string, destroy?: boolean) {
    const def = this._cellValueChangesDefs[name];
    if (!def) return;
    if (def.subscription) {
      if (this.debug) console.debug('[table] Stop value changes on row path {' + def.formPath + '}');
      def.subscription.unsubscribe();
      def.subscription = null;
    }
    if (destroy && def.subject) {
      def.subject.complete();
      def.subject.unsubscribe();
    }
  }
}
