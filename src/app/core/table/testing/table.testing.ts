import { AfterViewInit, ChangeDetectionStrategy, Component, forwardRef, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppTable } from '../table.class';
import { Referential, StatusList } from '../../services/model/referential.model';
import { IonInfiniteScroll } from '@ionic/angular';
import { EntitiesTableDataSource } from '../entities-table-datasource.class';
import { InMemoryEntitiesService } from '../../../shared/services/memory-entity-service.class';
import { AppValidatorService } from '../../services/validator/base.validator.class';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { isNil, isNotNil } from '../../../shared/functions';
import { DateUtils, fromUnixMsTimestamp, toDateISOString } from '../../../shared/dates';
import { ReferentialFilter } from '../../services/testing/referential-filter.model';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { MatExpansionPanel } from '@angular/material/expansion';
import { StatusIds } from '../../services/model/model.enum';
import { Subscription, timer } from 'rxjs';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '../table.model';
import { TableValidatorService } from './table-validator.service';
import { Table2TestPage } from './table2.testing';

@Component({
  selector: 'app-table-testing',
  styleUrls: ['table.testing.scss'],
  templateUrl: 'table.testing.html',
  providers: [
    {
      provide: AppValidatorService,
      useClass: TableValidatorService,
    },
    {
      provide: AppTable,
      useExisting: forwardRef(() => TableTestPage),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableTestPage extends AppTable<Referential, ReferentialFilter> implements OnInit, OnDestroy, AfterViewInit {
  static readonly maxRowCount = 100;

  timer: Subscription;
  canEdit = true;
  data: Referential[];
  filterForm: UntypedFormGroup;
  filterCriteriaCount = 0;
  groupColumns = ['top-start', 'group-1', 'group-2', 'top-end'];
  rowHeight = 48;
  permanentSelectionAllowed = true;

  readonly statusList = StatusList;

  @Input() filterPanelFloating = true;
  @Input() enableInfiniteScroll: boolean;
  @Input() sticky = true;
  @Input() stickyEnd = true;

  get hasMoreData(): boolean {
    return this.data.length < 100;
  }

  @ViewChild(MatExpansionPanel, { static: true }) filterExpansionPanel: MatExpansionPanel;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  get dataService(): InMemoryEntitiesService<Referential> {
    return this._dataSource.dataService as InMemoryEntitiesService<Referential>;
  }

  constructor(
    injector: Injector,
    protected validatorService: AppValidatorService,
    protected formBuilder: UntypedFormBuilder
  ) {
    super(
      injector,
      [
        ...RESERVED_START_COLUMNS,
        'label',
        'name',
        'levelId',
        'statusId',
        'boolean',
        'date',
        'latitude',
        'updateDate',
        'comments',
        ...RESERVED_END_COLUMNS,
      ],
      new EntitiesTableDataSource(
        Referential,
        new InMemoryEntitiesService(Referential, ReferentialFilter, {
          sortByReplacement: { id: 'id' },
        }),
        validatorService,
        {
          suppressErrors: false,
        }
      ),
      null
    );

    // Works with true and false
    this.enableInfiniteScroll = this.mobile;

    this.autoLoad = false;
    this.inlineEdition = true;
    this.i18nColumnPrefix = 'TABLE.TESTING.';

    this.confirmBeforeDelete = true;
    this.confirmBeforeCancel = false;
    this.undoableDeletion = false;
    this.saveBeforeDelete = false;

    this.saveBeforeSort = true;
    this.saveBeforeFilter = true;
    this.propagateRowError = true;

    this.filterForm = formBuilder.group({
      searchText: [null],
    });

    // this.columnDefinitions['values'] = <FormFieldDefinition>{
    //   key: 'values',
    //   type: 'enums',
    //   values: [
    //     {key: 'A', value: 'Value A'},
    //     {key: 'B', value: 'Value B'}
    //   ]
    // };

    // Init new entity
    this.onStartEditingRow.subscribe((row) => {
      row.validator.patchValue(
        {
          entityName: 'TestVO',
          properties: {
            boolean: false,
            date: toDateISOString(DateUtils.moment()),
            latitude: 11.1111,
            ...row.currentData.properties,
          },
        },
        { emitEvent: false }
      );
    });
  }

  ngOnInit() {
    super.ngOnInit();

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter(() => this.filterForm.valid),
          tap((value) => {
            const filter = ReferentialFilter.fromObject(value);
            this.filterCriteriaCount = filter.countNotEmptyCriteria();
            this.markForCheck();
            // Applying the filter
            this.setFilter(filter, { emitEvent: false });
          }),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap((json) => this.settings.savePageSetting(this.settingsId, json, 'filter'))
        )
        .subscribe()
    );

    this.registerAutocompleteField('level', {
      items: [
        { id: 1, label: 'A', name: 'item A' },
        { id: 1, label: 'B', name: 'item B' },
      ],
      mobile: this.mobile,
    });

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      })
    );

    this.markAsReady();
  }

  async ngAfterViewInit() {
    super.ngAfterViewInit();

    // Restore filter from settings
    await this.restoreFilter();

    // Load data
    this.load();
  }

  ngOnDestroy() {
    console.debug('[test-table] Destroying table...');
    super.ngOnDestroy();
    this.stopTimer();
  }

  protected async restoreFilter() {
    await this.settings.ready();

    const json = this.settings.getPageSettings(this.settingsId, 'filter');
    console.debug('[table-test] Restoring filter from settings...', json);

    if (json) {
      const filter = ReferentialFilter.fromObject(json);
      this.filterForm.patchValue(json, { emitEvent: false });
      this.filterCriteriaCount = filter.countNotEmptyCriteria();
      this.setFilter(filter, { emitEvent: true });
      this.markForCheck();
    }
  }

  toggleTimer() {
    if (isNotNil(this.timer)) this.stopTimer();
    else this.startTimer();
  }

  async load() {
    console.debug('[test-table] Updating data...');
    this.markAsLoading();

    if (isNil(this.data)) {
      this.data = this.generateData(0, Table2TestPage.maxRowCount);
    }
    this.dataService.value = this.data;

    this.emitRefresh();
  }

  async fetchMore(event: any) {
    console.debug('[table-test] Loading more...', event);
    const fetched = await this._dataSource.fetchMore();

    if (fetched && event?.target && event.target.complete) {
      // Wait end of load
      await this.waitIdle();
      // Mark target event as completed (e.g. IonRefresher)
      event.target.complete();
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const saved = await super.save(opts);
    if (saved) {
      // Update source data
      this.data = this.dataService.value;
    }
    return saved;
  }

  clearControlValue(event: Event, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  toggleFilterPanelFloating() {
    this.filterPanelFloating = !this.filterPanelFloating;
    this.markForCheck();
  }

  applyFilterAndClosePanel(event?: Event) {
    const filter = this.filterForm.value;
    this.setFilter(filter, { emitEvent: false });
    this.emitRefresh(event);
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  closeFilterPanel() {
    if (this.filterExpansionPanel) this.filterExpansionPanel.close();
    if (!this.filterPanelFloating) {
      this.filterPanelFloating = true;
      this.markForCheck();
    }
  }

  resetFilter(_?: Event) {
    this.filterForm.reset({}, { emitEvent: true });
    this.setFilter(ReferentialFilter.fromObject({}), { emitEvent: true });
    this.filterExpansionPanel.close();
  }

  startTimer() {
    this.stopTimer();
    this.timer = timer(500).subscribe(() => {
      this.data = null; // Reset
      this.load();
    });
  }

  stopTimer() {
    this.timer?.unsubscribe();
    this.timer = null;
  }

  protected generateData(offset?: number, size?: number): Referential[] {
    offset = offset || 0;
    size = size || 100;

    const result = new Array<Referential>();
    for (let i = 0; i < size; i++) {
      const id = i + 1 + offset;
      const item = Referential.fromObject({
        id,
        label: 'CODE-' + id,
        name: 'Name #' + id,
        levelId: { id: 1, label: 'A', name: 'Item A' },
        statusId: StatusIds.ENABLE,
        comments: i % 3 === 0 ? 'My comment #' + id : undefined,
        properties: {
          values: i % 2 === 0 ? 'A' : 'A,B',
          boolean: false,
          date: null, // toDateISOString(DateUtils.moment()),
          latitude: null, // 11.1111
        },
        creationDate: fromUnixMsTimestamp(Date.now() - i * 60 * 1000),
        updateDate: fromUnixMsTimestamp(Date.now() - i * 60 * 1000),
        entityName: 'TestVO',
      });
      result.push(item);
    }

    return result;
  }
}
