import { AfterViewInit, ChangeDetectionStrategy, Component, forwardRef, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppTable } from '../table.class';
import { Referential } from '../../services/model/referential.model';
import { IonInfiniteScroll } from '@ionic/angular';
import { EntitiesTableDataSource } from '../entities-table-datasource.class';
import { InMemoryEntitiesService } from '../../../shared/services/memory-entity-service.class';
import { AppValidatorService } from '../../services/validator/base.validator.class';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { isNil } from '../../../shared/functions';
import { ReferentialFilter } from '../../services/testing/referential-filter.model';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { MatExpansionPanel } from '@angular/material/expansion';
import { DEFAULT_PAGE_SIZE, RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '../table.model';
import { Table2ValidatorService } from './table2-validator.service';
import { StatusIds } from '../../services/model/model.enum';

@Component({
  selector: 'app-table2-testing',
  styleUrls: ['table2.testing.scss'],
  templateUrl: 'table2.testing.html',
  providers: [
    {
      provide: AppValidatorService,
      useClass: Table2ValidatorService,
    },
    {
      provide: AppTable,
      useExisting: forwardRef(() => Table2TestPage),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Table2TestPage extends AppTable<Referential, ReferentialFilter> implements OnInit, OnDestroy, AfterViewInit {
  static readonly maxRowCount = 100;

  canEdit = true;
  data: Referential[];
  filterForm: UntypedFormGroup;
  filterCriteriaCount = 0;
  rowHeight = 33;
  enableInfiniteScroll: boolean;
  checkBoxSelection = true;
  resizable = true;

  @Input() filterPanelFloating = true;
  @Input() sticky = true;
  @Input() stickyEnd = true;

  get hasMoreData(): boolean {
    return this.data.length < 100;
  }

  @ViewChild(MatExpansionPanel, { static: true }) filterExpansionPanel: MatExpansionPanel;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  get dataService(): InMemoryEntitiesService<Referential> {
    return this._dataSource.dataService as InMemoryEntitiesService<Referential>;
  }

  constructor(
    injector: Injector,
    protected validatorService: AppValidatorService,
    protected formBuilder: UntypedFormBuilder
  ) {
    super(
      injector,
      [
        ...RESERVED_START_COLUMNS,
        'name',
        'levelId',
        'boolean',
        'boolean2',
        //'boolean3',
        ...RESERVED_END_COLUMNS,
      ],
      new EntitiesTableDataSource(
        Referential,
        new InMemoryEntitiesService(Referential, ReferentialFilter, {
          sortByReplacement: { id: 'id' },
        }),
        validatorService,
        {
          suppressErrors: false,
        }
      ),
      null
    );

    // Works with true and false
    this.enableInfiniteScroll = this.mobile;

    this.autoLoad = false;
    this.inlineEdition = true;
    this.i18nColumnPrefix = 'TABLE.TESTING.';

    this.confirmBeforeDelete = true;
    this.confirmBeforeCancel = false;
    this.undoableDeletion = false;
    this.saveBeforeDelete = false;

    this.saveBeforeSort = true;
    this.saveBeforeFilter = true;
    this.propagateRowError = true;

    this.filterForm = formBuilder.group({
      searchText: [null],
    });

    this.onStartEditingRow.subscribe((row) => {
      row.validator.patchValue({ entityName: 'TestVO' }, { emitEvent: false });
    });
  }

  ngOnInit() {
    super.ngOnInit();

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter(() => this.filterForm.valid),
          tap((value) => {
            const filter = ReferentialFilter.fromObject(value);
            this.filterCriteriaCount = filter.countNotEmptyCriteria();
            this.markForCheck();
            // Applying the filter
            this.setFilter(filter, { emitEvent: false });
          }),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap((json) => this.settings.savePageSetting(this.settingsId, json, 'filter'))
        )
        .subscribe()
    );

    this.registerAutocompleteField('level', {
      items: [
        { id: 1, label: 'A', name: 'item A' },
        { id: 1, label: 'B', name: 'item B' },
      ],
      mobile: this.mobile,
    });

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      })
    );

    this.markAsReady();
  }

  async ngAfterViewInit() {
    super.ngAfterViewInit();

    // Restore filter from settings
    await this.restoreFilter();

    // Load data
    this.load();
  }

  ngOnDestroy() {
    console.debug('[test-table] Destroying table...');
    super.ngOnDestroy();
  }

  protected async restoreFilter() {
    await this.settings.ready();

    const json = this.settings.getPageSettings(this.settingsId, 'filter');
    console.debug('[table-test] Restoring filter from settings...', json);

    if (json) {
      const filter = ReferentialFilter.fromObject(json);
      this.filterForm.patchValue(json, { emitEvent: false });
      this.filterCriteriaCount = filter.countNotEmptyCriteria();
      this.setFilter(filter, { emitEvent: true });
      this.markForCheck();
    }
  }

  async load() {
    console.debug('[test-table] Updating data...');
    this.markAsLoading();

    if (isNil(this.data)) {
      this.data = this.generateData(0, Table2TestPage.maxRowCount);
    }
    this.dataService.value = this.data;

    this.emitRefresh();
  }

  async fetchMore(event) {
    console.debug('[table-test] Loading more...', event);
    const fetched = await this._dataSource.fetchMore();

    if (fetched && event?.target && event.target.complete) {
      // Wait end of load
      await this.waitIdle();
      // Mark target event as completed (e.g. IonRefresher)
      event.target.complete();
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const saved = await super.save(opts);
    if (saved) {
      // Update source data
      this.data = this.dataService.value;
    }
    return saved;
  }

  clearControlValue(event: Event, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  toggleFilterPanelFloating() {
    this.filterPanelFloating = !this.filterPanelFloating;
    this.markForCheck();
  }

  applyFilterAndClosePanel(event?: Event) {
    this.defaultPageSize = DEFAULT_PAGE_SIZE;
    this.emitRefresh(event);
    this.filterExpansionPanel.close();
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  closeFilterPanel() {
    if (this.filterExpansionPanel) this.filterExpansionPanel.close();
    if (!this.filterPanelFloating) {
      this.filterPanelFloating = true;
      this.markForCheck();
    }
  }

  resetFilter(_?: Event) {
    this.filterForm.reset({}, { emitEvent: true });
    this.setFilter(ReferentialFilter.fromObject({}), { emitEvent: true });
    this.filterExpansionPanel.close();
  }

  protected generateData(offset?: number, size?: number): Referential[] {
    offset = offset || 0;
    size = size || 100;

    const result = new Array<Referential>();
    for (let i = 0; i < size; i++) {
      const id = i + 1 + offset;
      const item = Referential.fromObject({
        id,
        label: 'CODE-' + id,
        name: 'Name #' + id,
        levelId: { id: 1, label: 'A', name: 'Item A' },
        statusId: StatusIds.ENABLE,
        comments: 'My comment #' + id,
        boolean: id % 2 === 0,
        boolean2: true,
        // boolean3: false,
        entityName: 'TestVO',
      });
      result.push(item);
    }

    return result;
  }
}
