import { Injectable } from '@angular/core';
import { ReferentialValidatorService } from '../../services/testing/referential.validator';
import { UntypedFormBuilder } from '@angular/forms';

@Injectable()
export class Table2ValidatorService extends ReferentialValidatorService {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroupConfig(data?: any, opts?: { withDescription?: boolean; withComments?: boolean }): { [p: string]: any } {
    return {
      ...super.getFormGroupConfig(data, opts),
      boolean: [data?.boolean || null],
      boolean2: [data?.boolean2 || null],
      boolean3: [data?.boolean3 || null],
    };
  }
}
