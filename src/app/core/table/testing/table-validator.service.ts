import { Injectable } from '@angular/core';
import { ReferentialValidatorService } from '../../services/testing/referential.validator';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Referential } from '../../services/model/referential.model';
import { SharedValidators } from '../../../shared/validator/validators';

@Injectable()
export class TableValidatorService extends ReferentialValidatorService {
  constructor(protected formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroup(data?: Referential, opts?: { withDescription?: boolean; withComments?: boolean }): UntypedFormGroup {
    const form = super.getFormGroup(data, opts);

    form.addControl('properties', this.getPropertiesForm(data?.properties));

    return form;
  }

  getPropertiesForm(data: { [key: string]: any }, opts?: any): UntypedFormGroup {
    return this.formBuilder.group({
      values: [data?.['values'], Validators.required],
      boolean: [data?.['boolean'], Validators.required],
      date: [data?.['date'], Validators.required, SharedValidators.validDate],
      latitude: [data?.['latitude'], Validators.required, SharedValidators.latitude],
    });
  }
}
