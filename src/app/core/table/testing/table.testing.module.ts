import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CoreModule } from '../../core.module';
import { TableTestPage } from './table.testing';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { Table2TestPage } from './table2.testing';
import { ResizableModule } from '../../../shared/directives/resizable/resizable.module';

@NgModule({
  imports: [CommonModule, SharedModule, CoreModule, TranslateModule.forChild(), FormsModule, ResizableModule],
  declarations: [TableTestPage, Table2TestPage],
  exports: [TableTestPage, Table2TestPage, TranslateModule],
})
export class TableTestingModule {}
