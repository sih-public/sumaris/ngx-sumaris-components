import { AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppTable } from '../table.class';
import { Referential } from '../../services/model/referential.model';
import { IonInfiniteScroll } from '@ionic/angular';
import { EntitiesTableDataSource } from '../entities-table-datasource.class';
import { InMemoryEntitiesService } from '../../../shared/services/memory-entity-service.class';
import { AppValidatorService } from '../../services/validator/base.validator.class';
import { AbstractControl, FormBuilder, UntypedFormGroup } from '@angular/forms';
import { isNil, isNotNil } from '../../../shared/functions';
import { fromUnixMsTimestamp } from '../../../shared/dates';
import { debounceTime, filter, tap } from 'rxjs/operators';
import { MatExpansionPanel } from '@angular/material/expansion';
import { ReferentialFilter } from '../../services/testing/referential-filter.model';
import { ReferentialValidatorService } from '../../services/testing/referential.validator';
import { TableTestPage } from './table.testing';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS } from '../table.model';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-multi-table-testing',
  templateUrl: 'multi-table.testing.html',
  providers: [
    {
      provide: InMemoryEntitiesService,
      useFactory: () =>
        new InMemoryEntitiesService(Referential, ReferentialFilter, {
          sortByReplacement: { id: 'id' },
        }),
    },
    {
      provide: AppValidatorService,
      useClass: ReferentialValidatorService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiTableTestingPage extends AppTable<Referential, ReferentialFilter> implements OnInit, OnDestroy, AfterViewInit {
  static maxRowCount = 100;

  timer: Subscription;

  canEdit = true;
  data: Referential[];
  filterForm: UntypedFormGroup;
  filterCriteriaCount = 0;
  filterPanelFloating = true;

  @Input() enableInfiniteScroll: boolean;
  @Input() sticky = true;
  @Input() stickyEnd = true;

  constructor(
    injector: Injector,
    protected dataService: InMemoryEntitiesService<Referential, ReferentialFilter>,
    protected validatorService: AppValidatorService,
    protected formBuilder: FormBuilder
  ) {
    super(
      injector,
      [...RESERVED_START_COLUMNS, 'label', 'name', 'updateDate', 'comments', ...RESERVED_END_COLUMNS],
      new EntitiesTableDataSource(Referential, dataService, validatorService, {
        suppressErrors: false,
      }),
      null
    );

    // Works with true and false
    this.enableInfiniteScroll = this.mobile;

    this.autoLoad = false;
    this.inlineEdition = true;
    this.i18nColumnPrefix = 'TABLE.TESTING.';

    this.confirmBeforeDelete = false;
    this.saveBeforeDelete = false;

    this.saveBeforeSort = true;
    this.saveBeforeFilter = true;

    this.filterForm = formBuilder.group({
      searchText: [null],
    });
  }

  get hasMoreData(): boolean {
    return this.data.length < 100;
  }

  @ViewChild(MatExpansionPanel, { static: true }) filterExpansionPanel: MatExpansionPanel;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  ngOnInit() {
    super.ngOnInit();

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter(() => this.filterForm.valid),
          tap((value) => {
            const filter = ReferentialFilter.fromObject(value);
            this.filterCriteriaCount = filter.countNotEmptyCriteria();
            this.markForCheck();
            // Applying the filter
            this.setFilter(filter, { emitEvent: false });
          }),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap((json) => this.settings.savePageSetting(this.settingsId, json, 'filter'))
        )
        .subscribe()
    );

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      })
    );

    this.markAsReady();
  }

  async ngAfterViewInit() {
    super.ngAfterViewInit();

    // Restore filter from settings
    await this.restoreFilter();

    // Load data
    this.load();
  }

  ngOnDestroy() {
    console.debug('[test-table] Destroying table...');
    super.ngOnDestroy();
    this.stopTimer();
    this.dataService.stop();
  }

  protected async restoreFilter() {
    await this.settings.ready();

    const json = this.settings.getPageSettings(this.settingsId, 'filter');
    console.debug('[table-test] Restoring filter from settings...', json);

    if (json) {
      const filter = ReferentialFilter.fromObject(json);
      this.filterForm.patchValue(json, { emitEvent: false });
      this.filterCriteriaCount = filter.countNotEmptyCriteria();
      this.setFilter(filter, { emitEvent: true });
      this.markForCheck();
    }
  }

  toggleTimer() {
    if (isNotNil(this.timer)) this.stopTimer();
    else this.startTimer();
  }

  async load() {
    console.debug('[test-table] Updating data...');
    this.markAsLoading();

    if (isNil(this.data)) {
      this.data = this.generateData(0, this.enableInfiniteScroll ? this.defaultPageSize : TableTestPage.maxRowCount);
    }
    this.dataService.value = this.data;
    this.emitRefresh();

    if (this.infiniteScroll) {
      this.infiniteScroll.disabled = !this.infiniteScroll || this.data.length >= TableTestPage.maxRowCount;
    }
  }

  async fetchMore(event) {
    console.debug('[table-test] Loading more...', event);
    const fetched = await this._dataSource.fetchMore();

    if (fetched && event?.target && event.target.complete) {
      // Wait end of load
      await this.waitIdle();
      // Mark target event as completed (e.g. IonRefresher)
      event.target.complete();
    }
  }

  async save(opts?: { keepEditing?: boolean }): Promise<boolean> {
    const saved = await super.save(opts);
    if (saved) {
      // Update source data
      this.data = this.dataService.value;
    }
    return saved;
  }

  clearControlValue(event: Event, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  toggleFilterPanelFloating() {
    this.filterPanelFloating = !this.filterPanelFloating;
    this.markForCheck();
  }

  applyFilterAndClosePanel(event?: Event) {
    const filter = this.filterForm.value;
    this.setFilter(filter, { emitEvent: false });
    this.emitRefresh(event);
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  closeFilterPanel() {
    if (this.filterExpansionPanel) this.filterExpansionPanel.close();
    if (!this.filterPanelFloating) {
      this.filterPanelFloating = true;
      this.markForCheck();
    }
  }

  resetFilter(_?: Event) {
    this.filterForm.reset({}, { emitEvent: true });
    this.setFilter(ReferentialFilter.fromObject({}), { emitEvent: true });
    this.filterExpansionPanel.close();
  }

  startTimer() {
    this.stopTimer();
    this.timer = timer(500).subscribe(() => {
      this.data = null; // Reset
      this.load();
    });
  }

  stopTimer() {
    this.timer?.unsubscribe();
    this.timer = null;
  }

  protected generateData(offset?: number, size?: number): Referential[] {
    offset = offset || 0;
    size = size || 100;

    const result = new Array<Referential>();
    for (let i = 0; i < size; i++) {
      const id = i + 1 + offset;
      const item = Referential.fromObject({
        id,
        label: 'CODE-' + id,
        name: 'Name #' + id,
        comments: 'My comment #' + id,
        creationDate: fromUnixMsTimestamp(Date.now() - i * 60 * 1000),
        updateDate: fromUnixMsTimestamp(Date.now() - i * 60 * 1000),
      });
      result.push(item);
    }

    return result;
  }
}
