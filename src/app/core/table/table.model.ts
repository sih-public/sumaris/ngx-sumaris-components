import { Subject, Subscription } from 'rxjs';

export const SETTINGS_DISPLAY_COLUMNS = 'displayColumns';
export const SETTINGS_SORTED_COLUMN = 'sortedColumn';
export const SETTINGS_FILTER = 'filter';
export const SETTINGS_PAGE_SIZE = 'pageSize';
export const DEFAULT_PAGE_SIZE = 20;
export const DEFAULT_PAGE_SIZE_OPTIONS = [20, 50, 100, 200, 500];
export const RESERVED_START_COLUMNS = ['select', 'id'];
export const RESERVED_END_COLUMNS = ['actions'];
export const DEFAULT_REQUIRED_COLUMNS = ['id'];

export class CellValueChangeListener {
  subject: Subject<any>;
  subscription: Subscription; // The row start editing subscription
  formPath: string;
  emitInitialValue: boolean;
}

export interface IModalDetailOptions<T = any> {
  // Data
  isNew: boolean;
  data: T;
  disabled: boolean;

  // Callback functions
  onDelete?: (event: Event, data: T) => Promise<boolean>;
}

export type SaveActionType = 'delete' | 'sort' | 'filter';

export interface IEntitiesTableDataSource<R> {
  getRows(): R[];
}
