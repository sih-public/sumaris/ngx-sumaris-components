import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { slideUpDownAnimation } from '../../shared/material/material.animations';

@Component({
  selector: 'app-update-offline-mode-card',
  templateUrl: 'update-offline-mode-card.component.html',
  styleUrls: ['./update-offline-mode-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation],
})
export class AppUpdateOfflineModeCard implements OnDestroy {
  @Input() progressionMessage: string;
  @Input() progressionValue: number;
  @Output() onUpdateClick = new EventEmitter<Event>();

  constructor() {}

  ngOnDestroy() {
    this.onUpdateClick.complete();
  }

  /* -- private method  -- */
}
