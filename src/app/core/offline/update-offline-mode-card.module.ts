import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AppUpdateOfflineModeCard } from './update-offline-mode-card.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [SharedModule, TranslateModule.forChild()],

  declarations: [
    // Network
    AppUpdateOfflineModeCard,
  ],
  exports: [
    TranslateModule,

    // Components
    AppUpdateOfflineModeCard,
  ],
})
export class AppUpdateOfflineModeCardModule {}
