// TODO: rename to CONFIG_OPTIONS_MAP
// then declare a type like this :
// > export declare type ConfigOptions = key of CONFIG_OPTIONS_MAP
import { FormFieldDefinition } from '../../../shared/form/field.model';
import { APP_LOCALES } from '../model/settings.model';
import { AuthTokenType } from '../network.types';

export const CORE_CONFIG_OPTIONS = Object.freeze({
  LOGO: <FormFieldDefinition>{
    key: 'sumaris.logo',
    label: 'CONFIGURATION.OPTIONS.LOGO',
    type: 'string',
  },
  FAVICON: <FormFieldDefinition>{
    key: 'sumaris.favicon',
    label: 'CONFIGURATION.OPTIONS.FAVICON',
    type: 'string',
  },
  DEFAULT_LOCALE: <FormFieldDefinition>{
    key: 'sumaris.defaultLocale',
    label: 'CONFIGURATION.OPTIONS.DEFAULT_LOCALE',
    type: 'enum',
    values: APP_LOCALES,
  },
  DEFAULT_LAT_LONG_FORMAT: <FormFieldDefinition>{
    key: 'sumaris.defaultLatLongFormat',
    label: 'CONFIGURATION.OPTIONS.DEFAULT_LATLONG_FORMAT',
    type: 'enum',
    values: [
      {
        key: 'DDMMSS',
        value: 'COMMON.LAT_LONG.ENUM.DDMMSS',
      },
      {
        key: 'DDMM',
        value: 'COMMON.LAT_LONG.ENUM.DDMM',
      },
      {
        key: 'DD',
        value: 'COMMON.LAT_LONG.ENUM.DD',
      },
    ],
  },
  /**
   * @deprecated Use `ACCOUNT_LAT_LONG_FORMAT_ENABLE` instead
   */
  //@deprecated
  DEFAULT_LAT_LONG_FORMAT_ENABLED: <FormFieldDefinition>{
    key: 'sumaris.defaultLatLongFormat.enabled',
    label: 'CONFIGURATION.OPTIONS.ENABLE_DEFAULT_LATLONG_FORMAT',
    type: 'boolean',
    defaultValue: true, // Important, for compatibility reason after deprecation
  },
  REGISTRATION_ENABLE: <FormFieldDefinition>{
    key: 'sumaris.registration.enable',
    label: 'CONFIGURATION.OPTIONS.HOME.ENABLE_REGISTRATION',
    type: 'boolean',
    defaultValue: true,
  },
  AUTH_TOKEN_TYPE: <FormFieldDefinition>{
    key: 'sumaris.auth.token.type',
    label: 'CONFIGURATION.OPTIONS.AUTH_TOKEN_TYPE_PLACEHOLDER',
    type: 'enum',
    values: [
      {
        key: <AuthTokenType>'token',
        value: 'CONFIGURATION.OPTIONS.AUTH_TOKEN_TYPE.TOKEN',
      },
      {
        key: <AuthTokenType>'basic',
        value: 'CONFIGURATION.OPTIONS.AUTH_TOKEN_TYPE.BASIC',
      },
      {
        key: <AuthTokenType>'basic-and-token',
        value: 'CONFIGURATION.OPTIONS.AUTH_TOKEN_TYPE.BASIC_AND_TOKEN',
      },
    ],
    defaultValue: <AuthTokenType>'token',
  },
  AUTH_RESET_PASSWORD_ENABLE: <FormFieldDefinition>{
    key: 'sumaris.auth.reset.enable',
    label: 'CONFIGURATION.OPTIONS.HOME.CHANGE_PASSWORD',
    type: 'boolean',
    defaultValue: true,
  },
  AUTH_API_TOKEN_ENABLED: <FormFieldDefinition>{
    key: 'sumaris.auth.api.token.enabled',
    label: 'CONFIGURATION.OPTIONS.ENABLE_AUTH_API_TOKENS',
    type: 'boolean',
    defaultValue: false,
    isTransient: true,
  },
  AUTH_API_TOKEN_SCOPE_ENABLED: <FormFieldDefinition>{
    key: 'sumaris.auth.api.token.scope.enabled',
    label: 'CONFIGURATION.OPTIONS.ENABLE_AUTH_API_TOKENS_SCOPE',
    type: 'boolean',
    defaultValue: false,
    isTransient: true,
  },
  DEFAULT_AUTH_API_TOKEN_SCOPE: <FormFieldDefinition>{
    key: 'sumaris.auth.api.token.scope.default',
    label: 'CONFIGURATION.OPTIONS.DEFAULT_AUTH_API_TOKEN_SCOPE',
    type: 'string',
    defaultValue: 'read_api',
    isTransient: true,
  },
  GRAVATAR_ENABLE: <FormFieldDefinition>{
    key: 'sumaris.gravatar.enable',
    label: 'CONFIGURATION.OPTIONS.ENABLE_GRAVATAR',
    type: 'boolean',
    defaultValue: false,
  },
  GRAVATAR_URL: <FormFieldDefinition>{
    key: 'sumaris.gravatar.url',
    label: 'CONFIGURATION.OPTIONS.GRAVATAR_URL',
    type: 'string',
    defaultValue: 'https://www.gravatar.com/avatar/{md5}',
  },
  TESTING: <FormFieldDefinition>{
    key: 'sumaris.testing.enable',
    label: 'CONFIGURATION.OPTIONS.TESTING',
    type: 'boolean',
  },
  APP_MIN_VERSION: <FormFieldDefinition>{
    key: 'sumaris.app.version.min',
    label: 'CONFIGURATION.OPTIONS.APP_MIN_VERSION',
    type: 'string',
  },
  HELP_URL: <FormFieldDefinition>{
    key: 'sumaris.help.url',
    label: 'CONFIGURATION.OPTIONS.HELP_URL',
    type: 'string',
  },
  FORUM_URL: <FormFieldDefinition>{
    key: 'sumaris.forum.url',
    label: 'CONFIGURATION.OPTIONS.FORUM_URL',
    type: 'string',
  },
  REPORT_ISSUE_URL: <FormFieldDefinition>{
    key: 'sumaris.reportIssue.url',
    label: 'CONFIGURATION.OPTIONS.REPORT_ISSUE_URL',
    type: 'string',
  },
  PRIVACY_POLICY_URL: <FormFieldDefinition>{
    key: 'sumaris.policy.url',
    label: 'CONFIGURATION.OPTIONS.PRIVACY_POLICY_URL',
    type: 'string',
  },
  TERMS_OF_USE_URL: <FormFieldDefinition>{
    key: 'sumaris.termsOfUse.url',
    label: 'CONFIGURATION.OPTIONS.TERMS_OF_USE_URL',
    type: 'string',
  },
  LOGO_LARGE: <FormFieldDefinition>{
    key: 'sumaris.logo.large',
    label: 'CONFIGURATION.OPTIONS.HOME.LOGO_LARGE',
    type: 'string',
  },
  MENU_ITEMS: <FormFieldDefinition>{
    key: 'sumaris.menu.items',
    label: 'CONFIGURATION.OPTIONS.MENU.ITEMS',
    type: 'string',
    // -- FOR DEV
    /*,defaultValue: JSON.stringify([
        {title: "Item #1", path: "/settings", icon: "people", after: "MENU.HOME"},
        {title: "Item #2", path: "/settings", icon: "people", before: "MENU.LOGOUT"}
    ])*/
  },
  HOME_PARTNERS_DEPARTMENTS: <FormFieldDefinition>{
    key: 'sumaris.partner.departments',
    label: 'CONFIGURATION.OPTIONS.HOME.PARTNER_DEPARTMENTS',
    type: 'string',
  },
  HOME_BACKGROUND_IMAGE: <FormFieldDefinition>{
    key: 'sumaris.background.images',
    label: 'CONFIGURATION.OPTIONS.HOME.BACKGROUND_IMAGES',
    type: 'string',
  },
  COLOR_PRIMARY: <FormFieldDefinition>{
    key: 'sumaris.color.primary',
    label: 'CONFIGURATION.OPTIONS.COLORS.PRIMARY',
    type: 'color',
  },
  COLOR_SECONDARY: <FormFieldDefinition>{
    key: 'sumaris.color.secondary',
    label: 'CONFIGURATION.OPTIONS.COLORS.SECONDARY',
    type: 'color',
  },
  COLOR_TERTIARY: <FormFieldDefinition>{
    key: 'sumaris.color.tertiary',
    label: 'CONFIGURATION.OPTIONS.COLORS.TERTIARY',
    type: 'color',
  },
  COLOR_SUCCESS: <FormFieldDefinition>{
    key: 'sumaris.color.success',
    label: 'CONFIGURATION.OPTIONS.COLORS.SUCCESS',
    type: 'color',
  },
  COLOR_WARNING: <FormFieldDefinition>{
    key: 'sumaris.color.warning',
    label: 'CONFIGURATION.OPTIONS.COLORS.WARNING',
    type: 'color',
  },
  COLOR_ACCENT: <FormFieldDefinition>{
    key: 'sumaris.color.accent',
    label: 'CONFIGURATION.OPTIONS.COLORS.ACCENT',
    type: 'color',
  },
  COLOR_DANGER: <FormFieldDefinition>{
    key: 'sumaris.color.danger',
    label: 'CONFIGURATION.OPTIONS.COLORS.DANGER',
    type: 'color',
  },
  FORM_FIELD_BACKGROUND_COLOR: <FormFieldDefinition>{
    key: 'sumaris.theme.fieldBackgroundColor',
    label: 'CONFIGURATION.OPTIONS.FORM_FIELD_BACKGROUND_COLOR',
    type: 'color',
  },
  FORM_FIELD_FOCUS_BACKGROUND_COLOR: <FormFieldDefinition>{
    key: 'sumaris.theme.fieldFocusBackgroundColor',
    label: 'CONFIGURATION.OPTIONS.FORM_FIELD_FOCUS_BACKGROUND_COLOR',
    type: 'color',
  },
  FORM_FIELD_DISABLED_BACKGROUND_COLOR: <FormFieldDefinition>{
    key: 'sumaris.theme.fieldDisabledBackgroundColor',
    label: 'CONFIGURATION.OPTIONS.FORM_FIELD_DISABLED_BACKGROUND_COLOR',
    type: 'color',
  },
  ANDROID_INSTALL_URL: <FormFieldDefinition>{
    key: 'sumaris.android.install.url',
    label: 'CONFIGURATION.OPTIONS.ANDROID_INSTALL_URL',
    type: 'string',
  },
  IOS_INSTALL_URL: <FormFieldDefinition>{
    key: 'sumaris.ios.install.url',
    label: 'CONFIGURATION.OPTIONS.IOS_INSTALL_URL',
    type: 'string',
  },
  DESKTOP_INSTALL_URL: <FormFieldDefinition>{
    key: 'sumaris.desktop.install.url',
    label: 'CONFIGURATION.OPTIONS.DESKTOP_INSTALL_URL',
    type: 'string',
  },
  DB_TIMEZONE: <FormFieldDefinition>{
    key: 'sumaris.persistence.db.timezone',
    label: 'CONFIGURATION.OPTIONS.DB_TIMEZONE',
    type: 'string',
    isTransient: true, // Only on server, cannot be set
  },
  ACCOUNT_LAT_LONG_FORMAT_ENABLE: <FormFieldDefinition>{
    key: 'sumaris.account.latLongFormat.enable',
    label: 'CONFIGURATION.OPTIONS.ACCOUNT.ENABLE_LATLONG_FORMAT',
    type: 'boolean',
    defaultValue: true,
  },
  ACCOUNT_READONLY: <FormFieldDefinition>{
    key: 'sumaris.account.readOnly',
    label: 'CONFIGURATION.OPTIONS.ACCOUNT.READONLY',
    type: 'boolean',
    defaultValue: false,
    isTransient: true,
  },
});
