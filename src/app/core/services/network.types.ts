import { ConnectionType } from '@capacitor/network';

export type AuthTokenType = 'token' | 'basic' | 'basic-and-token';

export declare type NetworkEventType = 'start' | 'peerChanged' | 'statusChanged' | 'resetCache' | 'beforeTryOnlineFinish';

export { ConnectionType };
