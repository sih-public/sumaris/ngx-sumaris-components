import { IEntity } from './entity.model';
import { isEmptyArray, isNotNil } from '../../../shared/functions';
import { EntityFilter } from './filter.model';
import { FilterFn } from '../../../shared/types';

export declare interface ITreeItemEntity<T extends IEntity<T, ID>, ID = number> {
  parentId: ID;
  parent: T;
  children: T[];
}

export class TreeItemEntityUtils {
  static forward<T extends ITreeItemEntity<any>, ID = number>(node: T, filterFn?: FilterFn<T>, loopCount?: number): T {
    // Stop infinite loop
    if (loopCount === 100) {
      console.error('Infinite loop detected! Make sure there is valid node in this tree!');
      return undefined;
    }

    let indexInParent = node.parent ? (node.parent.children || []).indexOf(node) : -1;
    // Root node
    if (indexInParent === -1) {
      return this.first(node, filterFn);
    }

    // Dive into children
    const firstChild = (node.children || []).map((c) => this.first(c, filterFn)).find(isNotNil);
    if (firstChild) return firstChild;

    // Lookup in brothers
    let current = node;
    while (current.parent && indexInParent !== -1) {
      // Has brother ?
      if (indexInParent + 1 < current.parent.children.length) {
        const brother = (current.parent.children || [])
          .slice(indexInParent + 1)
          .map((c) => (!filterFn || filterFn(c) ? c : this.first(c, filterFn)))
          .find(isNotNil);
        // OK, found a brother
        if (brother) return brother;
      }
      // Retry, with parent's brother
      current = current.parent;
      indexInParent = current.parent ? (current.parent.children || []).indexOf(current) : -1;
    }

    // Return parent
    if (current !== node && (!filterFn || filterFn(current))) return current;

    return this.first(current, filterFn);
  }

  static backward<T extends ITreeItemEntity<any>, ID = number>(node: T, filterFn?: FilterFn<T>, loopCount?: number): T {
    // Stop infinite loop
    if (loopCount === 100) {
      console.error('Infinite loop detected! Make sure there is valid node in this tree!');
      return undefined;
    }

    const indexInParent = node.parent ? (node.parent.children || []).indexOf(node) : -1;
    // Root node
    if (indexInParent === -1) {
      return this.lastLeaf(node, filterFn);
    }

    // No previous brother (first in parent's list)
    else if (indexInParent === 0) {
      // Return the parent, if match
      return !filterFn || filterFn(node.parent)
        ? node.parent
        : // Or recursively call backward() on the parent
          this.backward(node.parent, filterFn, (loopCount || 0) + 1);
    }

    // Lookup in brother
    else {
      const previousBrother = (node.parent.children || [])
        .slice(0, indexInParent)
        .reverse()
        .find((c) => !filterFn || filterFn(c));

      // OK, there is a brother before the current index
      if (previousBrother) return this.lastLeaf(previousBrother, filterFn);
    }

    // Not found in brother: recursively call backward() on the parent
    return this.backward(node.parent, filterFn, (loopCount || 0) + 1);
  }

  static first<T extends ITreeItemEntity<any>>(node: T, filterFn?: FilterFn<T>): T {
    // Node has children: dive into children
    const child = (node.children || []).find((c) => this.first(c, filterFn));
    if (child) return child;

    // Node match: use it
    return !filterFn || filterFn(node) ? node : undefined;
  }

  static lastLeaf<T extends ITreeItemEntity<any>>(node: T, filterFn?: FilterFn<T>): T {
    // Node has children: dive into children
    const childLeaf = (node.children || [])
      // Reverse order (after clone)
      .slice()
      .reverse()
      .map((c) => this.lastLeaf(c, filterFn))
      .find(isNotNil);
    if (childLeaf) return childLeaf; // OK, found in children

    // Node is a leaf, or not match found in children: check current
    return !filterFn || filterFn(node) ? node : undefined;
  }

  static findByFilter<T extends ITreeItemEntity<T> & IEntity<T>>(node: T, filter: EntityFilter<any, T>): T[] {
    const filterFn = filter?.asFilterFn();
    if (!filterFn) throw new Error('Missing or empty filter argument');

    return this.filterRecursively(node, filterFn);
  }

  /**
   * Delete matches batches
   *
   * @param node
   * @param filter
   */
  static deleteByFilter<T extends ITreeItemEntity<T> & IEntity<T>>(node: T, filter: EntityFilter<any, T>): T[] {
    const filterFn = filter?.asFilterFn();
    if (!filterFn) throw new Error('Missing or empty filter argument');

    return this.deleteRecursively(node, filterFn);
  }

  static filterRecursively<T extends ITreeItemEntity<any>>(node: T, filterFn: (n: T) => boolean): T[] {
    return (node?.children || []).reduce(
      (res, child) => res.concat(this.filterRecursively(child, filterFn)),
      // Init result
      filterFn(node) ? [node] : []
    );
  }

  static deleteRecursively<T extends ITreeItemEntity<any>>(node: T, filterFn: (n: T) => boolean): T[] {
    if (isEmptyArray(node.children)) return []; // Skip

    // Delete children
    const deletedChildren = node.children.filter(filterFn);
    node.children = node.children.filter((c) => !deletedChildren.includes(c));

    // TODO: add an options to clean the parent
    //deletedChildren.forEach(child => child.parent = null);

    // Recursive call, in still existing children
    return node.children.reduce((res, c) => res.concat(...this.deleteRecursively(c, filterFn)), deletedChildren);
  }

  /**
   * Visit each node, from root to leaf
   *
   * @param node
   * @param action
   */
  static visit<T extends ITreeItemEntity<any>>(node: T, action: (item: T) => void) {
    if (node == null) {
      return;
    }

    // Appliquer l'action sur le nœud actuel
    action(node);

    // Parcourir les enfants
    if (node.children) {
      node.children.forEach((child) => this.visit(child, action));
    }
  }

  /**
   * Visit each node, from leaf to root
   *
   * @param node
   * @param action
   */
  static visitInverse<T extends ITreeItemEntity<any>>(node: T, action: (T) => void) {
    if (node == null) {
      return;
    }

    // Parcourir les enfants
    if (node.children) {
      node.children.forEach((child) => this.visitInverse(child, action));
    }

    // Appliquer l'action sur le nœud actuel
    action(node);
  }

  /**
   * Transform a batch entity tree into a array list. This method keep links parent/children.
   *
   * Method used to find batch without id (e.g. before local storage)
   *
   * @param source
   */
  static treeToArray<T extends ITreeItemEntity<any>>(source: T): T[] {
    if (!source) return null;
    return (source.children || []).reduce(
      (res, batch) => {
        batch.parent = source;
        return res.concat(this.treeToArray(batch)); // recursive call
      },
      [source]
    );
  }

  static listOfTreeToArray<T extends ITreeItemEntity<any>>(sources: T[]): T[] {
    if (!sources || !sources.length) return null;
    return sources.reduce((res, source) => res.concat(this.treeToArray<T>(source)), []);
  }

  /**
   * Fill parent attribute, of all children found in the tree
   *
   * @param source
   */
  static treeFillParent<T extends ITreeItemEntity<any>>(source: T) {
    if (!source) return null;
    (source.children || []).forEach((child) => {
      child.parent = source;
      this.treeFillParent(child); // Loop
    });
  }
}
