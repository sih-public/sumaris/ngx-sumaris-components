import { environment } from '../../../../environments/environment';
import { Entity } from './entity.model';
import { removeEnd } from '../../../shared/functions';

declare type Constructor = new (...args: any[]) => any;

export class EntityClasses {
  static CLASSES_BY_NAME = new Map<string, Constructor>();

  static register(typename: string, entityClass: Constructor) {
    // Convert to extract entityName
    const entityName = removeEnd(typename, 'VO');
    // DEBUG
    //console.debug('[entity-classes] Detected entity class ' + entityName);

    // Add to map
    EntityClasses.CLASSES_BY_NAME.set(entityName, entityClass);
  }

  static get<T extends Entity<T>>(entityName: string): Constructor {
    const entityClass = EntityClasses.CLASSES_BY_NAME.get(entityName);
    if (!entityClass) {
      throw new Error("Cannot found entity class '" + entityName + "'. Make sure it exists and has the @EntityClass decorator.");
    }
    return entityClass;
  }

  static fromObject<T extends Entity<T>>(
    value: any,
    opts?: {
      [key: string]: any;
      typename?: string;
      entityName?: string;
    }
  ): T {
    if (!value) return value;
    const entityName = (opts && opts.entityName) || removeEnd(value.__typename || opts?.typename, 'VO');
    if (!entityName) throw new Error("Missing one of 'value.__typename', 'opts.entityName', or 'opts.typename'.");
    const entityClass = EntityClasses.get(entityName) as any;
    if (typeof entityClass.fromObject !== 'function') {
      throw new Error(
        "Cannot found the static function '" +
          entityName +
          ".fromObject()'. Make sure this function has been declared and the entity class has the @EntityClass decorator"
      );
    }
    return entityClass.fromObject(value, opts) as T;
  }
}

export function EntityClass(opts: { typename: string; fromObjectReuseStrategy?: 'default' | 'clone' }) {
  return function <T extends Constructor>(constructor: T) {
    // Make sure the class extends Entity
    if (!environment.production) {
      const obj = new constructor();
      if (!(obj instanceof Entity)) {
        throw new Error(`Class ${constructor.name} must extends <<Entity>> to be able to use @FromObject!`);
      }
    }

    const clazz = constructor as any;
    const typename = opts.typename || `${constructor.name}VO`;

    // Define static field TYPENAME
    clazz.TYPENAME = typename;

    // Define static fromObject()
    if (opts.fromObjectReuseStrategy === 'clone') {
      clazz.fromObject = function (source: any, opts?: any): T {
        if (!source) return undefined;
        if (source instanceof constructor) return (source as any).clone(opts) as T;
        const target: any = new constructor();
        target.fromObject(source, opts);
        return target as T;
      };
      clazz.asObject = function (source: any, opts?: any): any {
        if (!source) return undefined;
        if (source instanceof constructor) return source.asObject(opts);
        return Object.assign({}, source);
      };
    } else {
      clazz.fromObject = function (source: any, opts?: any): T {
        if (!source) return undefined;
        if (source instanceof constructor) {
          // DEBUG
          //console.debug('@EntityClass() fromObject() => will recycle existing object: ', source);
          return source as T;
        }
        const target: any = new constructor();
        target.fromObject(source, opts);
        return target as T;
      };

      clazz.asObject = function (source: any, opts?: any): any {
        if (!source) return undefined;
        if (source instanceof constructor) {
          return source.asObject(opts);
        }
        // DEBUG
        //console.debug('@EntityClass() asObject() => will recycle existing object: ', source);
        return source;
      };
    }

    // Register the entity class
    EntityClasses.register(typename, constructor);

    return constructor;
  };
}
