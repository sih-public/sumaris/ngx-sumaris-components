/* -- Network -- */

import { isNilOrBlank, isNotNil, isNotNilOrBlank, noTrailingSlash } from '../../../shared/functions';
import { Cloneable, Entity, EntityAsObjectOptions } from './entity.model';
import { EntityClass } from './entity.decorators';
import { NodeFeature } from './node-feature.model';

// @dynamic
@EntityClass({ typename: 'PeerVO' })
export class Peer extends Entity<Peer> implements Cloneable<Peer> {
  static fromObject: (source: any, opts?: any) => Peer;

  static parseUrl(peerUrl: string) {
    const url = new URL(peerUrl);
    return this.fromObject({
      dns: url.hostname,
      port: isNilOrBlank(url.port) ? undefined : url.port,
      useSsl: url.protocol && (url.protocol.startsWith('https') || url.protocol.startsWith('wss')),
      path: noTrailingSlash(url.pathname),
    });
  }
  static path(peer: Peer, ...paths: string[]): string {
    if (!peer) throw new Error("Missing required argument 'peer'!");
    // Remove starting slashes
    paths = (paths || [])
      .map((path) => {
        if (path.startsWith('./')) return path.substring(2);
        if (path.startsWith('/')) return path.substring(1);
        return path;
      })
      .filter(isNotNilOrBlank);
    return [noTrailingSlash(this.fromObject(peer).url)].concat(...paths).join('/');
  }

  static equals(p1: Peer, p2: Peer) {
    return p1 && p2 && Peer.fromObject(p1).equals(p2);
  }
  static countAllFeatures(peers: Peer[]): number {
    return (peers || []).reduce((res, peer) => {
      return res + (peer?.features?.length || 0);
    }, 0);
  }

  dns: string;
  ipv4: string;
  ipv6: string;
  port: number;
  useSsl: boolean;
  pubkey: string;
  path?: string;

  favicon: string;
  status: 'UP' | 'DOWN';
  softwareName: string;
  softwareVersion: string;
  label: string;
  name: string;
  registrationUri?: string;
  registrationManager?: string;
  features?: NodeFeature[];

  constructor() {
    super(Peer.TYPENAME);
  }

  asObject(options?: EntityAsObjectOptions): any {
    const target = super.asObject(options);
    target.features = this.features?.map((feature) => feature.asObject(options));
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.dns = source.dns || source.host;
    this.ipv4 = source.ipv4;
    this.ipv6 = source.ipv6;
    this.port = isNotNil(source.port) ? +source.port : undefined;
    this.useSsl = source.useSsl || this.port === 443;
    this.pubkey = source.pubkey;
    this.path = source.path || '';

    this.favicon = source.favicon;
    this.status = source.status;
    this.softwareName = source.softwareName;
    this.softwareVersion = source.softwareVersion;
    this.label = source.label || '';
    this.name = source.name || '';
    this.registrationUri = source.registrationUri || '';
    this.registrationManager = source.registrationManager;

    this.features = source.features?.map(NodeFeature.fromObject);
  }

  equals(other: Peer): boolean {
    return (
      // Same entity (by id)
      (isNotNil(this.id) && isNotNil(other.id) && this.id === other.id) ||
      // Same pubkey (if defined)
      ((!this.pubkey || !other.pubkey || this.pubkey === other.pubkey) &&
        // Same url
        this.url === other.url)
    );
  }

  /**
   * Return the peer URL (without trailing slash)
   */
  get url(): string {
    return (this.useSsl ? 'https://' : 'http://') + this.hostAndPort + (this.path || '');
  }

  get hostAndPort(): string {
    return (this.dns || this.ipv4 || this.ipv6) + (this.port && this.port !== 80 && this.port !== 443 ? ':' + this.port : '');
  }

  get reachable(): boolean {
    return this.status && this.status === 'UP';
  }
}
