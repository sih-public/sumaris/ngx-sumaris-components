import { Moment } from 'moment';
import { getPropertyByPath, isEmptyArray, isNil, isNilOrBlank, isNotNil, joinPropertiesPath } from '../../../shared/functions';
import { FilterFn, KeyValueType, ObjectMap, ObjectMapEntry } from '../../../shared/types';
import { StoreObject } from '@apollo/client/core';
import { fromDateISOString, toDateISOString } from '../../../shared/dates';
import { TreeItemEntityUtils } from './tree-item-entity.model';
import { SortDirection } from '@angular/material/sort';
import { AppPropertiesUtils } from '../../form/properties/properties.utils';

export declare interface Cloneable<T> {
  clone(): T;
}

export function entityToString(obj: Entity<any> | any, properties?: string[]): string | undefined {
  return (obj && obj.id && joinPropertiesPath(obj, properties || ['name'])) || undefined;
}

export interface EntityAsObjectOptions {
  minify?: boolean;
  keepTypename?: boolean; // true by default
  keepLocalId?: boolean; // true by default
}

export interface IEntity<T, ID = number, AO extends EntityAsObjectOptions = EntityAsObjectOptions, FO = any> extends Cloneable<T> {
  id: ID;
  updateDate: Moment;
  __typename: string;
  equals(other: T): boolean;
  clone(opts?: AO & FO): T;
  copy(target: T, opts?: AO & FO);
  asObject(opts?: AO): any;
  fromObject(source: any, opts?: FO);
}

//@dynamic
export abstract class Entity<T extends Entity<T, ID, AO, FO>, ID = number, AO extends EntityAsObjectOptions = EntityAsObjectOptions, FO = any>
  implements IEntity<T, ID, AO, FO>
{
  // The GraphQL typename (injected by @EntityClass())
  static TYPENAME: string;

  id: ID = null;
  updateDate: Moment = null;
  __typename: string;

  constructor(__typename?: string) {
    this.__typename = __typename || null;
  }

  clone(opts?: AO & FO): T {
    const target = new (this.constructor as any)() as T;
    this.copy(target, opts);
    return target;
  }

  copy(target: T, opts?: AO & FO) {
    target.fromObject(this.asObject(opts), opts);
  }

  asObject(opts?: AO): StoreObject {
    const target: any = Object.assign({}, this); //= {...this};
    if (!opts || opts.keepTypename !== true) delete target.__typename;
    target.updateDate = toDateISOString(this.updateDate);
    if (target.id == null || (opts && opts.keepLocalId === false && target.id < 0)) {
      delete target.id;
      delete target.updateDate; // Make to sens to keep updateDate of a local entity to save
    }
    return target;
  }

  fromObject(source: any, opts?: FO) {
    this.id = source.id || source.id === 0 ? source.id : undefined;
    this.updateDate = fromDateISOString(source.updateDate);
    this.__typename = source.__typename || this.__typename; // Keep original type (can be set in constructor)
  }

  equals(other: T): boolean {
    return other && this.id === other.id;
  }
}

/**
 * @deprecated use standard instanceof instead
 * @param obj
 * @param constructor
 */
export function isInstanceOf<T>(obj: any, constructor: new (...args: any[]) => T): obj is T {
  if (!obj) return false;

  return obj instanceof constructor;
}

// @dynamic
export abstract class EntityUtils {
  // Check that the object is an entity
  static isEntity<T extends IEntity<any>>(obj: T | any): obj is T {
    return isNotNil(obj?.__typename) && typeof obj.fromObject === 'function' && typeof obj.asObject === 'function';
  }

  // Check that the object has a NOT nil attribute (ID by default)
  static isNotEmpty<T extends IEntity<any> | any>(obj: T, checkedAttribute: keyof T = <keyof T>'id'): boolean {
    return !!obj && obj[checkedAttribute] !== null && obj[checkedAttribute] !== undefined;
  }

  // Check that the object has a nil attribute (ID by default)
  static isEmpty<T extends IEntity<any> | any>(obj: T, checkedAttribute: keyof T = <keyof T>'id'): boolean {
    return !obj || obj[checkedAttribute] === null || obj[checkedAttribute] === undefined;
  }

  static equals<T extends IEntity<any> | any>(o1: T, o2: T, checkAttribute?: keyof T): boolean {
    checkAttribute = checkAttribute || ('id' as keyof T);
    return o1 === o2 || (isNil(o1) && isNil(o2)) || (o1 && o2 && o1[checkAttribute] === o2[checkAttribute]);
  }

  static getPropertyByPath = getPropertyByPath;

  static getArrayAsMap<T = any>(source?: ObjectMapEntry<T>[]): ObjectMap<T> {
    return (source || []).reduce((res, item) => {
      res[item.key] = item.value;
      return res;
    }, {});
  }

  static getMapAsArray<T = any>(source?: ObjectMap<T>): ObjectMapEntry<T>[] {
    if (Array.isArray(source)) return source;
    return Object.getOwnPropertyNames(source || {}).map((key) => ({
      key,
      value: source[key],
    }));
  }

  static getPropertyArrayAsObject = AppPropertiesUtils.arrayAsObject;

  static copyIdAndUpdateDate<T extends Entity<any, any, any>>(source: T, target: T) {
    if (!source) return;

    // Update (id and updateDate)
    target.id = isNotNil(source.id) ? source.id : target.id;
    target.updateDate = fromDateISOString(source.updateDate) || target.updateDate;

    // Update creation Date, if exists
    if (source['creationDate']) {
      target['creationDate'] = fromDateISOString(source['creationDate']);
    }
  }

  static async fillLocalIds<T extends IEntity<T>>(items: T[], sequenceFactory: (firstEntity: T, incrementSize: number) => Promise<number>) {
    const newItems = (items || []).filter((item) => isNil(item.id) || item.id === 0);
    if (isEmptyArray(newItems)) return;
    // Get the sequence
    let currentId = (await sequenceFactory(newItems[0], newItems.length)) + 1;
    // Take the min (sequence, id), in case the sequence is corrupted
    currentId = items.filter((item) => isNotNil(item.id) && item.id < 0).reduce((res, item) => Math.min(res, item.id), currentId);
    newItems.forEach((item) => (item.id = --currentId));
  }

  static cleanIdAndUpdateDate<T extends IEntity<T>>(source: T) {
    if (!source) return; // Skip
    source.id = null;
    source.updateDate = null;
  }

  static cleanIdsAndUpdateDates<T extends IEntity<T>>(items: T[]) {
    (items || []).forEach(EntityUtils.cleanIdAndUpdateDate);
  }

  static sort<T extends IEntity<T> | any>(data: T[], sortBy?: keyof T | string, sortDirection?: SortDirection): T[] {
    if (!data || data.length <= 1 || !sortBy) return data;
    return data.sort(this.sortComparator(sortBy, sortDirection));
  }

  static sortComparator<T extends IEntity<T> | any>(sortBy?: keyof T | string, sortDirection?: SortDirection): (a: T, b: T) => number {
    const after = !sortDirection || sortDirection === 'asc' ? 1 : -1;
    const property = (sortBy || 'id') as string;
    return (a, b) => {
      const valueA = EntityUtils.getPropertyByPath(a, property);
      const valueB = EntityUtils.getPropertyByPath(b, property);
      return EntityUtils.compare(valueA, valueB, after);
    };
  }

  static compare<T extends IEntity<any>>(value1: T, value2: T, direction: 1 | -1, checkAttribute?: keyof T): number {
    checkAttribute = checkAttribute || 'id';
    if (EntityUtils.isNotEmpty(value1, checkAttribute) && EntityUtils.isNotEmpty(value2, checkAttribute)) {
      return EntityUtils.equals(value1, value2, checkAttribute) ? 0 : value1[checkAttribute] > value2[checkAttribute] ? direction : -1 * direction;
    }
    return value1 === value2 ? 0 : value1 > value2 ? direction : -1 * direction;
  }

  static compareValue<T>(value1: T, value2: T, direction: 1 | -1): number {
    return value1 === value2 ? 0 : value1 > value2 ? direction : -1 * direction;
  }

  static filter<T extends IEntity<T> | any>(data: T[], searchAttribute: string, searchText: string): T[] {
    const filterFn = this.searchTextFilter(searchAttribute, searchText);
    return data.filter(filterFn);
  }

  static searchTextFilter<T extends IEntity<T> | any>(searchAttribute: string | string[], searchText: string): FilterFn<T> {
    if (isNilOrBlank(searchAttribute) || isNilOrBlank(searchText)) return undefined; // filter not need

    const searchRegexp = searchText
      // Escape special characters
      // eslint-disable-next-line no-useless-escape
      .replace(/[-\/\\^$+.()|[\]{}]/g, '\\$&')
      // Interpret * as any text
      .replace(/[*]+/g, '.*')
      // Interpret ? as one character
      .replace(/[?]/g, '.');
    if (searchRegexp === '.*') return undefined; // filter not need

    const flags = 'i'; // don't use global regex search
    const asPrefixPattern = '^' + searchRegexp;
    const anyMatchPattern = '^.*' + searchRegexp;

    // Only one search attributes
    if (typeof searchAttribute === 'string') {
      const isSimplePath = !searchAttribute || searchAttribute.indexOf('.') === -1;
      const searchAsPrefix = searchAttribute.toLowerCase().endsWith('label') || searchAttribute.toLowerCase().endsWith('code');
      const regexp = new RegExp((searchAsPrefix && asPrefixPattern) || anyMatchPattern, flags);
      if (isSimplePath) {
        return (a) => regexp.test(a[searchAttribute]);
      } else {
        return (a) => regexp.test(EntityUtils.getPropertyByPath(a, searchAttribute));
      }
    }

    // many search attributes
    else {
      const regexps = searchAttribute.map((path) => {
        const searchAsPrefix = path.toLowerCase().endsWith('label') || path.toLowerCase().endsWith('code');
        return new RegExp((searchAsPrefix && asPrefixPattern) || anyMatchPattern, flags);
      });
      return (a) => searchAttribute.findIndex((path, index) => regexps[index].test(EntityUtils.getPropertyByPath(a, path))) !== -1;
    }
  }

  /**
   * Transform a batch entity tree into a array list. This method keep links parent/children.
   *
   * Method used to find batch without id (e.g. before local storage)
   *
   * @param source
   */
  static treeToArray = TreeItemEntityUtils.treeToArray;

  static listOfTreeToArray = TreeItemEntityUtils.listOfTreeToArray;

  /**
   * Fill parent attribute, of all children found in the tree
   *
   * @param source
   */
  static treeFillParent = TreeItemEntityUtils.treeFillParent;

  static isLocal(entity: IEntity<any, any> | Entity<any, any>): boolean {
    return entity && isNotNil(entity.id) && +entity.id < 0;
  }

  static isRemote(entity: IEntity<any, any> | Entity<any, any>): boolean {
    return entity && !EntityUtils.isLocal(entity);
  }

  static isLocalId(id: number): boolean {
    return isNotNil(id) && +id < 0;
  }

  static isRemoteId(id: number): boolean {
    return isNotNil(id) && +id >= 0;
  }

  static getId<T extends IEntity<T, ID> = IEntity<any, any>, ID = any>(entity: T): ID {
    return entity.id;
  }

  // Check that the object has an ID
  static hasId<T extends IEntity<any, any> = IEntity<any, any>>(entity: T): boolean {
    return isNotNil(entity?.id);
  }

  // Check that the object has an nil ID
  static hasNilId<T extends IEntity<any, any> = IEntity<any, any>>(entity: T): boolean {
    return isNil(entity?.id);
  }

  /**
   * Splits an array of entities into a key-value map using an entity ID as the key.
   *
   * @param {Array<T>} entities - The array of entities to be split by their ID.
   * @return {KeyValueType<T>} A key-value map with entity IDs as keys and entities as values.
   */
  static splitById<T extends IEntity<T, ID>, ID = any>(entities: T[]): KeyValueType<T> {
    return (entities || []).reduce(
      (res, item) => {
        const key = item?.id;
        if (typeof key === 'number' || typeof key === 'string') {
          res[key] = item;
        }
        return res;
      },
      <KeyValueType<T>>{}
    );
  }

  /**
   * Collects the IDs from an array of entities and returns them as an array.
   *
   * @param {T[]} entities - An array of entities from which to collect IDs.
   * @return {ID[]} An array of IDs collected from the entities.
   */
  static collectIds<T extends IEntity<T, ID>, ID = any>(entities: T[]): ID[] {
    return (entities || []).map((e) => e.id);
  }

  /**
   * @deprecated Use collectIds() instead - will be remove in next major release
   */
  static collectById<T extends IEntity<T, ID>, ID = any>(entities: T[]): ID[] {
    return (entities || []).map((e) => e.id);
  }

  static arrayDistinctFilterFn<E extends IEntity<any, any>>(item: E, index: number, values: E[]): boolean {
    return values.findIndex((e) => e.id === item.id) === index;
  }

  static findById<E extends IEntity<any, ID> = IEntity<any, any>, ID = number>(items: E[], id: ID): E | undefined {
    return (items || []).find((g) => g.id === id);
  }
}
