import { Moment } from 'moment/moment';
import { EntityClass } from './entity.decorators';
import { Cloneable, Entity, EntityAsObjectOptions } from './entity.model';

@EntityClass({ typename: 'NodeFeatureVO' })
export class NodeFeature extends Entity<NodeFeature> implements Cloneable<NodeFeature> {
  static fromObject: (source: any, opts?: any) => NodeFeature;

  label: string;
  name: string;
  description: string;
  logo: string;
  creationDate: Moment;
  statusId: number;
  validityStatusId: number;

  asObject(options?: EntityAsObjectOptions): any {
    return super.asObject(options);
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.label = source.label;
    this.name = source.name;
    this.description = source.description;
    this.logo = source.logo;
    this.creationDate = source.creationDate;
    this.statusId = source.statusId;
    this.validityStatusId = source.validityStatusId;
  }
}
