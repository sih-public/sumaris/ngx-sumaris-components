import { Moment } from 'moment';
import { isNil, isNotNil, joinPropertiesPath, splitById } from '../../../shared/functions';
import { Entity, EntityAsObjectOptions, EntityUtils, IEntity } from './entity.model';
import { StatusIds } from './model.enum';
import { fromDateISOString, toDateISOString } from '../../../shared/dates';
import { EntityClass } from './entity.decorators';
import { IconRef } from '../../../shared/types';

export function referentialToString(obj: Referential | any, properties?: string[], separator?: string): string | undefined {
  return (obj && isNotNil(obj.id) && joinPropertiesPath(obj, properties || ['label', 'name'], separator)) || undefined;
}
export function referentialsToString(values: Referential[], properties?: string[], valueSeparator?: string, propertySeparator?: string): string {
  return (values || []).map((v) => referentialToString(v, properties, propertySeparator)).join(valueSeparator || ', ');
}

export declare interface IStatus {
  id: number;
  icon?: string;
  label: string;
}

export const StatusList: Readonly<IStatus[]> = Object.freeze([
  {
    id: StatusIds.ENABLE,
    icon: 'checkmark',
    label: 'REFERENTIAL.STATUS_ENUM.ENABLE',
  },
  {
    id: StatusIds.DISABLE,
    icon: 'close',
    label: 'REFERENTIAL.STATUS_ENUM.DISABLE',
  },
  {
    id: StatusIds.TEMPORARY,
    icon: 'help',
    label: 'REFERENTIAL.STATUS_ENUM.TEMPORARY',
  },
]);

export const StatusById = Object.freeze(splitById(StatusList));

export declare interface IReferentialRef<T extends IEntity<T, ID> = IEntity<any, any>, ID = number> extends IEntity<T, ID> {
  label: string;
  name: string;
  description?: string;
  levelId?: number;
  statusId: number;
  entityName: string;
  icon?: IconRef;
}

//@dynamic
export abstract class BaseReferential<
    T extends BaseReferential<T, ID, AO>,
    ID = number,
    AO extends ReferentialAsObjectOptions = ReferentialAsObjectOptions,
    FO = any,
  >
  extends Entity<T, ID, AO, FO>
  implements IReferentialRef<T, ID>
{
  label: string;
  name: string;
  description: string;
  comments: string;
  creationDate: Date | Moment;
  statusId: number;
  validityStatusId: number;
  levelId: number;
  parentId: number;
  entityName: string;
  icon?: IconRef;

  protected constructor(__typename?: string) {
    super(__typename);
  }

  asObject(opts?: AO): any {
    if (opts?.minify) {
      return {
        id: this.id,
        entityName: (opts.keepEntityName && this.entityName) || undefined, // Don't keep by default
        __typename: (opts.keepTypename && this.__typename) || undefined,
      };
    }
    const target: any = super.asObject(opts);
    target.creationDate = toDateISOString(this.creationDate);
    if (opts?.keepEntityName === false) delete target.entityName;
    if (opts?.keepIcon === false) delete target.icon;
    if (opts?.keepProperties === false) delete target.properties;
    if (isNil(target.validityStatusId)) delete target.validityStatusId;
    return target;
  }

  fromObject(source: any, opts?: FO) {
    super.fromObject(source, opts);
    this.label = source.label;
    this.name = source.name;
    this.description = source.description;
    this.comments = source.comments;
    this.statusId = source.statusId;
    this.validityStatusId = source.validityStatusId;
    this.levelId = source.levelId && source.levelId !== 0 ? source.levelId : undefined; // Do not set as null (need for account.department, when register)
    this.parentId = source.parentId;
    this.creationDate = fromDateISOString(source.creationDate);
    this.entityName = source.entityName;
    this.icon = source.icon;
  }

  equals(other: T): boolean {
    return super.equals(other) && this.entityName === other.entityName;
  }
}

// @dynamic
@EntityClass({ typename: 'ReferentialVO' })
export class Referential<T extends Referential<T, any> = Referential<any>, ID = number> extends BaseReferential<T, ID> {
  static fromObject: (source: any, opts?: any) => Referential;

  properties?: { [key: string]: any };

  constructor() {
    super();
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.properties = (source.properties && { ...source.properties }) || undefined;
  }

  asObject(opts?: ReferentialAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      delete target.properties;
    }
    return target;
  }
}

export class ReferentialUtils {
  // FIXME: remove this
  static fromObject(source: any, opts?: any): Referential {
    if (!source || source instanceof Referential) return source;
    const res = new Referential();
    res.fromObject(source, opts);
    return res as Referential;
  }

  static isNotEmpty<T extends IReferentialRef<T>>(obj: T | any): boolean {
    // A referential entity should always have a 'id' filled (can be negative is local and temporary)
    return EntityUtils.isNotEmpty(obj, 'id');
  }

  static isEmpty<T extends IReferentialRef<T>>(obj: T | any): boolean {
    return EntityUtils.isEmpty(obj, 'id');
  }

  static equals<T extends IReferentialRef<T>>(o1: T | any, o2: T | any): boolean {
    return EntityUtils.equals(o1, o2, 'id');
  }

  static isNotDisabled<T extends IReferentialRef<T>>(obj: T | any): boolean {
    return obj && obj.statusId !== StatusIds.DISABLE;
  }
}

export interface ReferentialAsObjectOptions extends EntityAsObjectOptions {
  keepEntityName?: boolean;
  keepIcon?: boolean;
  keepProperties?: boolean;
}

/**
 * Used to store an entity locally
 */
export const MINIFY_ENTITY_FOR_LOCAL_STORAGE: ReferentialAsObjectOptions = Object.freeze({
  minify: true,
  keepTypename: true,
  keepEntityName: true,
  keepLocalId: true,
  keepIcon: true,
  keepProperties: true,
});

/**
 * Used to send an entity to a pod
 */
export const MINIFY_ENTITY_FOR_POD: ReferentialAsObjectOptions = Object.freeze({
  minify: true,
  keepTypename: false,
  keepEntityName: false,
  keepLocalId: false,
  keepIcon: false,
  keepProperties: false,
});

// @dynamic
@EntityClass({ typename: 'ReferentialVO' })
export class ReferentialRef<
    T extends ReferentialRef<any, ID> = ReferentialRef<any, any>,
    ID = number,
    O extends ReferentialAsObjectOptions = ReferentialAsObjectOptions,
    FO = any,
  >
  extends Entity<T, ID, O, FO>
  implements IReferentialRef<T, ID>
{
  static fromObject: (source: any, opts?: any) => ReferentialRef;
  static clone(source: any): ReferentialRef {
    return ReferentialRef.fromObject(source)?.clone();
  }

  label: string;
  name: string;
  description: string;
  statusId: number;
  levelId: number;
  entityName: string;
  icon?: IconRef;
  properties?: { [key: string]: any };

  constructor() {
    super(ReferentialRef.TYPENAME);
  }

  asObject(opts?: O): any {
    if (opts?.minify) {
      return {
        id: this.id,
        entityName: (opts.keepEntityName && this.entityName) || undefined, // Don't keep by default
        __typename: (opts.keepTypename && this.__typename) || undefined,
      };
    }
    const target: any = super.asObject(opts);
    if (opts?.keepEntityName === false) delete target.entityName;
    if (opts?.keepIcon === false) delete target.icon;
    if (opts?.keepProperties === false) delete target.properties;

    return target;
  }

  fromObject(source: any, opts?: FO) {
    super.fromObject(source);
    this.entityName = source.entityName || this.entityName;
    this.label = source.label;
    this.name = source.name;
    this.description = source.description;
    this.statusId = source.statusId;
    this.levelId = source.levelId;
    this.icon = source.icon;
    this.properties = (source.properties && { ...source.properties }) || undefined;
  }
}
