import { LocalSettings } from './settings.model';
import { Person, PersonUtils, UserProfileLabel } from './person.model';
import { Entity, EntityAsObjectOptions } from './entity.model';
import { EntityClass } from './entity.decorators';
import { StatusIds } from './model.enum';
import { LatLongPattern } from '../../../shared/material/latlong/latlong.utils';
import { equals, isNil } from '../../../shared/functions';
import { UserToken } from './token.model';

/**
 * A user account
 */
// @dynamic
@EntityClass({ typename: 'AccountVO' })
export class Account extends Person<Account> {
  static fromObject: (source: any, opts?: any) => Account;

  settings: UserSettings = null;
  tokens: UserToken[] = [];

  constructor() {
    super(Account.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target: any = super.asObject({ ...opts, minify: false /*disable Person minify (too restrictive) */ });
    target.settings = this.settings?.asObject(opts);
    target.tokens = this.tokens?.map((value) => value.asObject(opts));
    if (opts?.minify) {
      delete target.mainProfile; // Not known on server
    }
    return target;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.settings = source.settings && UserSettings.fromObject(source.settings);
    this.tokens = source.tokens?.map((value) => UserToken.fromObject(value)) || [];
  }

  /**
   * Convert into a Person. This will fill __typename with a right value, for data cache
   */
  asPerson(): Person {
    const person = Person.fromObject(
      this.asObject({
        keepTypename: true, // This is need for the department object
      })
    );
    person.__typename = Person.TYPENAME; // Do not keep AccountVO as typename
    return person;
  }

  get displayName(): string {
    return accountToString(this);
  }
}

export abstract class AccountUtils {
  static accountToString = accountToString;

  static hasMinProfile(account: Account | null, userProfile: UserProfileLabel): boolean {
    // should be login, and status ENABLE or TEMPORARY
    if (!account || !account.pubkey || (account.statusId !== StatusIds.ENABLE && account.statusId !== StatusIds.TEMPORARY)) {
      return false;
    }
    return PersonUtils.hasUpperOrEqualsProfile(account.profiles, userProfile);
  }

  static hasExactProfile(account: Account | null, label: UserProfileLabel): boolean {
    // should be login, and status ENABLE or TEMPORARY
    if (!account || !account.pubkey || (account.statusId !== StatusIds.ENABLE && account.statusId !== StatusIds.TEMPORARY)) return false;
    return account.profiles.some((profile) => profile === label);
  }

  static hasProfileAndIsEnable(account: Account | null, userProfile: UserProfileLabel): boolean {
    // should be login, and status ENABLE
    if (!account || !account.pubkey || account.statusId !== StatusIds.ENABLE) return false;
    return PersonUtils.hasUpperOrEqualsProfile(account.profiles, userProfile);
  }
}

// @dynamic
@EntityClass({ typename: 'UserSettingsVO' })
export class UserSettings extends Entity<UserSettings> {
  static SETTINGS_PROPERTIES = [<keyof UserSettings>'locale', <keyof UserSettings>'latLongFormat'] as string[];
  static fromObject: (source: any, opts?: any) => UserSettings;

  locale: string;
  latLongFormat: LatLongPattern;
  content: any;
  nonce: string;

  constructor() {
    super(UserSettings.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    if (opts?.minify) {
      // Stringify content
      target.content = this.content ? JSON.stringify(this.content) : undefined;
    }
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source);
    this.locale = source.locale;
    this.latLongFormat = source.latLongFormat as LatLongPattern;
    if (isNil(source.content) || typeof source.content === 'object') {
      this.content = source.content || {};
    } else {
      this.content = (source.content && JSON.parse(source.content)) || {};
    }
    this.nonce = source.nonce;
  }

  merge<S = LocalSettings>(source: S, includesProperties: (keyof S)[]): boolean {
    if (!source) return; // Skip

    // Reset content
    this.content = {};
    let hasChanges = false;

    // For each property of source object
    Object.keys(source).forEach((key) => {
      // If property exists in UserSettings class: store it directly
      if (UserSettings.SETTINGS_PROPERTIES.includes(key)) {
        if (!equals(this[key], source[key])) {
          this[key] = source[key];
          hasChanges = true;
        }
      }
      // Else, store property into the 'content' map
      else if (includesProperties && includesProperties.includes(key as keyof S)) {
        if (!equals(this.content[key], source[key])) {
          if (typeof source[key] === 'object' && source[key] !== null) {
            this.content[key] = { ...source[key] }; // Copy object
          } else {
            this.content[key] = source[key];
          }
          hasChanges = true;
        }
      }
    });
    return hasChanges;
  }

  asLocalSettings(): Partial<LocalSettings> {
    const target = {};
    UserSettings.SETTINGS_PROPERTIES.forEach((key) => {
      target[key] = this[key];
    });
    Object.keys(this.content).forEach((key) => {
      target[key] = this.content[key];
    });
    // Clear properties if empty, to avoid to reset the existing properties - See issue sumaris-app#897
    if (Object.keys(target['properties'] || {}).length === 0) {
      delete target['properties'];
    }
    return target;
  }
}

export function accountToString(data: Account): string {
  return (data && ((data.firstName && data.firstName + ' ') || '') + (data.lastName || '')) || '';
}
