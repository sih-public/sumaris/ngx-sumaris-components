import { EntityClass } from './entity.decorators';
import { Entity, EntityAsObjectOptions } from './entity.model';
import { Moment } from 'moment/moment';
import { DateUtils, fromDateISOString, toDateISOString } from '../../../shared/dates';
import { isNotNil, toNumber } from '../../../shared/functions';

export class TokenScope {
  id: string;
  flag: number;
  name: string;
}

// @dynamic
@EntityClass({ typename: 'UserTokenVO' })
export class UserToken extends Entity<UserToken> {
  static fromObject: (source: any, opts?: any) => UserToken;

  static equals(t1: UserToken, t2: UserToken): boolean {
    return (
      (isNotNil(t1.id) && t1.id === t2?.id) ||
      (t1 && t1.pubkey && t1.pubkey === t2?.pubkey && t1.name === t2?.name && DateUtils.equals(t1.expirationDate, t2?.expirationDate))
    );
  }

  pubkey: string = null;
  token: string = null;
  name: string = null;
  flags: number = null;
  scopes: TokenScope[] = null;
  expirationDate: Moment = null;
  lastUsedDate: Moment = null;
  creationDate: Moment = null;

  constructor() {
    super(UserToken.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.expirationDate = toDateISOString(this.expirationDate);
    target.lastUsedDate = toDateISOString(this.lastUsedDate);
    target.creationDate = toDateISOString(this.creationDate);
    if (opts?.minify) {
      delete target.scopes;
    }
    return target;
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source);
    this.pubkey = source.pubkey;
    this.token = source.token;
    this.name = source.name;
    this.flags = toNumber(source.flags);
    this.scopes = source.scopes;
    this.expirationDate = fromDateISOString(source.expirationDate);
    this.lastUsedDate = fromDateISOString(source.lastUsedDate);
    this.creationDate = fromDateISOString(source.creationDate);
  }
}
