import { Property, PropertyMap } from '../../../shared/types';
import { InjectionToken } from '@angular/core';
import { HistoryPageReference } from './history.model';

export type UsageMode = 'DESK' | 'FIELD';

export function isOnFieldMode(value: UsageMode): value is 'FIELD' {
  return value === 'FIELD';
}

export declare interface LocaleConfig extends Property {
  country?: string;
}

export const APP_LOCALES = new InjectionToken<LocaleConfig[]>('locales');

export declare interface OfflineFeature<F = any> {
  name: string;
  lastSyncDate?: string;
  filter?: F;
}

export declare interface LocalSettings {
  pages?: any;
  peerUrl?: string;
  latLongFormat: 'DDMMSS' | 'DDMM' | 'DD';
  accountInheritance?: boolean;
  locale: string;
  usageMode?: UsageMode;
  mobile?: boolean;
  properties?: PropertyMap | Property[];
  pageHistory?: HistoryPageReference[];
  offlineFeatures?: (string | OfflineFeature)[];
  pageHistoryMaxSize: number;
  darkMode?: boolean;
  autoDarkMode?: boolean;
}
