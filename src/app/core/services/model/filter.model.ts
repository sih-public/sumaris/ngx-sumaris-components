import { isEmptyArray } from '../../../shared/functions';
import { MINIFY_ENTITY_FOR_POD, ReferentialUtils } from './referential.model';
import { Entity, EntityAsObjectOptions, IEntity, isInstanceOf } from './entity.model';

import { FilterFn } from '../../../shared/types';

export interface IEntityFilter<
  F extends IEntityFilter<F, T, ID, AO, FO>,
  T extends IEntity<T, any>,
  ID = number,
  AO extends EntityAsObjectOptions = EntityAsObjectOptions,
  FO = any,
> extends IEntity<F, ID, AO, FO> {
  asPodObject(): any;

  asFilterFn(): FilterFn<T>;

  isEmpty(): boolean;

  countNotEmptyCriteria(): number;
}

export abstract class EntityFilter<
    F extends EntityFilter<F, T, ID, AO, FO>,
    T extends IEntity<T, any>,
    ID = number,
    AO extends EntityAsObjectOptions = EntityAsObjectOptions,
    FO = any,
  >
  extends Entity<F, ID, AO, FO>
  implements IEntityFilter<F, T, ID, AO, FO>
{
  /**
   * Compose some filter functions: all should return true
   *
   * @param filterFns
   */
  static composeFilters<T extends IEntity<T, any>>(filterFns: FilterFn<T>[]): FilterFn<T> {
    if (isEmptyArray(filterFns)) return undefined;
    return (entity) => (filterFns || []).every((fn) => fn(entity));
  }

  static compose<F extends EntityFilter<F, T>, T extends IEntity<T>>(filters: F[]): FilterFn<T> {
    const filterFns = (filters || []).reduce((res, f) => {
      const fns = f.buildFilter();
      if (!fns) return res;
      return res.concat(fns);
    }, []);
    return this.composeFilters(filterFns);
  }

  /**
   * Compose some filter functions: all should return true
   *
   * @param filterFns
   */
  static composeFns<T extends IEntity<T, any>>(filterFns: FilterFn<T>[]): FilterFn<T> {
    if (isEmptyArray(filterFns)) return undefined;
    return (entity) => (filterFns || []).every((fn) => fn(entity));
  }

  constructor(__typename?: string) {
    super(__typename);
  }

  /**
   * Clean a filter, before sending to the pod (e.g convert dates, remove internal properties, etc.)
   */
  asPodObject(): any {
    return this.asObject(MINIFY_ENTITY_FOR_POD as AO);
  }

  isEmpty(): boolean {
    return this.countNotEmptyCriteria() === 0;
  }

  countNotEmptyCriteria(): number {
    const json = this.asPodObject();
    return Object.keys(json).filter((key) => this.isCriteriaNotEmpty(key, json[key])).length;
  }

  asFilterFn(): FilterFn<T> {
    const filterFns = this.buildFilter();
    return EntityFilter.composeFns(filterFns);
  }

  protected buildFilter(): FilterFn<T>[] {
    // Can be completed by subclasses
    return [];
  }

  protected isCriteriaNotEmpty(key: string, value: any): boolean {
    // Can be overridden by subclasses
    return (
      value !== undefined &&
      value !== null &&
      // not empty string
      ((typeof value === 'string' && value.trim() !== '') ||
        // valid number
        (typeof value === 'number' && !isNaN(value)) ||
        // valid boolean
        typeof value === 'boolean' ||
        // not empty array
        (Array.isArray(value) && value.length > 0) ||
        // entity with an id
        ReferentialUtils.isNotEmpty(value))
    );
  }
}

export abstract class EntityFilterUtils {
  static isEntityFilter<F extends EntityFilter<F, any>>(obj: Partial<F>): obj is F {
    return (obj && obj.asPodObject && obj.asFilterFn && true) || false;
  }

  static fromObject<F>(source: any, filterType: new () => F): F {
    if (!source) return source;
    if (isInstanceOf(source, filterType)) return source as F;
    const target = new filterType();
    if (EntityFilterUtils.isEntityFilter(target)) {
      target.fromObject(source);
    } else {
      Object.assign(target, source);
    }
    return target;
  }
}
