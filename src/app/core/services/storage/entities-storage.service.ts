import { defer, merge, Observable, timer } from 'rxjs';
import { EventEmitter, Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { Platform } from '@ionic/angular';
import { first, switchMap, throttleTime } from 'rxjs/operators';
import { Entity, IEntity } from '../model/entity.model';
import { isEmptyArray, isNilOrBlank } from '../../../shared/functions';
import { LoadResult } from '../../../shared/services/entity-service.class';
import { ENTITIES_STORAGE_KEY_PREFIX, EntityStorageLoadOptions, EntityStore, EntityStoreTypePolicy } from './entity-store.class';
import { APP_PROGRESS_BAR_SERVICE, IProgressBarService } from '../../../shared/services/progress-bar.service';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';
import { StartableService } from '../../../shared/services/startable-service.class';
import { chainPromises } from '../../../shared/observables';
import { APP_STORAGE, IStorage } from '../../../shared/storage/storage.utils';

export interface EntitiesStorageTypePolicies {
  [__typename: string]: EntityStoreTypePolicy;
}

export const APP_LOCAL_STORAGE_TYPE_POLICIES = new InjectionToken<EntitiesStorageTypePolicies>('localStorageTypePolicies');

declare interface EntityStorageMap {
  [key: string]: EntityStore<any, any>;
}

@Injectable({ providedIn: 'root', deps: [APP_STORAGE, ENVIRONMENT] })
export class EntitiesStorage extends StartableService<EntityStorageMap> {
  static readonly TRASH_PREFIX = 'Trash#';
  static readonly REMOTE_PREFIX = 'Remote#';

  private readonly _typePolicies: EntitiesStorageTypePolicies;
  private readonly _saveTimerPeriod: number;

  private _$save = new EventEmitter(true);
  private _dirty = false;
  private _saving = false;

  get dirty(): boolean {
    return this._dirty || Object.values(this._data).some((store) => store.dirty);
  }

  public constructor(
    private platform: Platform,
    @Inject(APP_STORAGE) private storage: IStorage,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_PROGRESS_BAR_SERVICE) private progressBarService?: IProgressBarService,
    @Optional() @Inject(APP_LOCAL_STORAGE_TYPE_POLICIES) typePolicies?: EntitiesStorageTypePolicies
  ) {
    super(storage);
    this._typePolicies = typePolicies || {};
    this._saveTimerPeriod = environment.storageSavePeriodMs || 10000 /* = 10s */;
    this._data = {};

    // For DEV only
    this._debug = !environment.production;
    if (this._debug) console.debug('[entities-storage] Creating service');
  }

  async ngOnStart(): Promise<EntityStorageMap> {
    const now = Date.now();
    console.info(`[entities-storage] Starting entity storage...`);

    // Restore stores
    await this.restoreLocally();

    // Start a save timer
    this.registerSubscription(
      merge(this._$save, timer(this._saveTimerPeriod, this._saveTimerPeriod))
        .pipe(
          // Avoid to many call (e.g. when $save AND timer are triggered
          throttleTime(this._saveTimerPeriod)
        )
        .subscribe(() => this.storeLocally({ skipIfPristine: true }))
    );

    // Save, when platform is in pause
    this.registerSubscription(
      this.platform.pause.subscribe(async () => {
        const now = Date.now();
        console.info(`[entities-storage] Platform will pause: saving...`);
        await this.storeLocally({ skipIfPristine: true });
        console.info(`[entities-storage] Platform will pause: saving [OK] in ${Date.now() - now}ms`);
      })
    );

    console.info(`[entities-storage] Starting [OK] in ${Date.now() - now}ms`);

    return this._data;
  }

  protected async ngOnStop() {
    await this.storeLocally({ skipIfPristine: true });
  }

  watchAll<T extends Entity<T>>(
    entityName: string,
    variables: {
      offset?: number;
      size?: number;
      sortBy?: keyof T | string;
      sortDirection?: string;
      trash?: boolean;
      filter?: (data: T) => boolean;
    },
    opts?: EntityStorageLoadOptions
  ): Observable<LoadResult<T>> {
    // Make sure store is ready
    if (!this.started) {
      return defer(() => this.ready()).pipe(switchMap(() => this.watchAll<T>(entityName, variables, opts))); // Loop
    }

    this.progressBarService?.increase();
    const storeName = variables && variables.trash ? EntitiesStorage.TRASH_PREFIX + entityName : entityName;
    const result = this.getEntityStore<T>(storeName, { create: true }).watchAll(variables, opts);

    result.pipe(first()).subscribe(() => this.progressBarService?.decrease());

    return result;
  }

  async loadAll<T extends Entity<T>>(
    entityName: string,
    variables?: {
      offset?: number;
      size?: number;
      sortBy?: keyof T | string;
      sortDirection?: string;
      filter?: (data: T) => boolean;
    },
    opts?: EntityStorageLoadOptions
  ): Promise<LoadResult<T>> {
    // Make sure store is ready
    if (!this.started) await this.ready();

    try {
      this.progressBarService?.increase();
      if (this._debug) console.debug(`[entities-storage] Loading ${entityName}...`);

      const entityStore = this.getEntityStore<T>(entityName, { create: false });
      if (!entityStore) return { data: [], total: 0 }; // No store for this entity name

      return entityStore.loadAll(variables, opts);
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async load<T extends Entity<T>>(id: number, entityName: string, opts?: EntityStorageLoadOptions): Promise<T> {
    await this.ready();

    try {
      this.progressBarService?.increase();

      const entityStore = this.getEntityStore<T>(entityName, { create: false });
      if (!entityStore) return undefined;
      return await entityStore.load(id, opts);
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async nextValue(entityOrName: string | any): Promise<number> {
    await this.ready();
    this._dirty = true;
    return this.getEntityStore(this.detectEntityName(entityOrName)).nextValue();
  }

  async nextValues(entityOrName: string | any, entityCount: number): Promise<number> {
    await this.ready();
    this._dirty = true;
    const entityName = this.detectEntityName(entityOrName);
    const store = this.getEntityStore(entityName);
    const firstValue = store.nextValue();
    for (let i = 0; i < entityCount - 1; i++) {
      store.nextValue();
    }
    if (this._debug) console.debug(`[entities-storage] Reserving range [${firstValue},${store.currentValue()}] for ${entityName}'s sequence`);
    return firstValue;
  }

  async currentValue(entityOrName: string | any): Promise<number> {
    await this.ready();
    return this.getEntityStore(this.detectEntityName(entityOrName)).currentValue();
  }

  async save<T extends Entity<T>>(
    entity: T,
    opts?: {
      entityName?: string;
      emitEvent?: boolean;
    }
  ): Promise<T> {
    if (!entity) return; // skip

    if (!this.started) await this.ready();

    try {
      this.progressBarService?.increase();

      this._dirty = true;
      const storeName = (opts && opts.entityName) || this.detectEntityName(entity);
      this.getEntityStore<T>(storeName).save(entity, opts);

      // Ask to save
      this._$save.emit();

      return entity;
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async saveAll<T extends Entity<T>>(
    entities: T[],
    opts?: {
      entityName?: string;
      reset?: boolean;
      emitEvent?: boolean;
    }
  ): Promise<T[]> {
    if (isEmptyArray(entities) && (!opts || opts.reset !== true)) return entities; // Skip (nothing to save)

    if (!this.started) await this.ready();

    try {
      this.progressBarService?.increase();

      this._dirty = true;
      const entityName = (opts && opts.entityName) || this.detectEntityName(entities[0]);
      return this.getEntityStore<T>(entityName).saveAll(entities, opts);
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async delete<T extends Entity<T, ID>, ID = number>(
    entity: T,
    opts?: {
      entityName?: string;
    }
  ): Promise<T> {
    if (!entity) return undefined; // skip

    if (!this.started) await this.ready();

    return this.deleteById(entity.id, {
      ...opts,
      entityName: (opts && opts.entityName) || this.detectEntityName(entity),
    });
  }

  async deleteById<T extends IEntity<T, ID>, ID = number>(
    id: ID,
    opts: {
      entityName: string;
      emitEvent?: boolean;
    }
  ): Promise<T> {
    if (!this.started) await this.ready();

    if (!opts || isNilOrBlank(opts.entityName)) throw new Error("Missing argument 'opts' or 'entityName'");
    //if (id >= 0) throw new Error('Invalid id a local entity (not a negative number): ' + id);

    try {
      this.progressBarService?.increase();
      const entityStore = this.getEntityStore<T, ID>(opts.entityName, { create: false });
      if (!entityStore) return undefined;

      const deletedEntity = entityStore.delete(id, opts);

      // If something deleted
      if (deletedEntity) {
        // Mark as dirty
        this._dirty = true;

        // Ask to save
        this._$save.emit();
      }

      return deletedEntity;
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async deleteMany<T extends Entity<T>>(
    ids: number[],
    opts: {
      entityName: string;
      emitEvent?: boolean;
    }
  ): Promise<T[]> {
    if (!this.started) await this.ready();

    if (!opts || isNilOrBlank(opts.entityName)) throw new Error("Missing argument 'opts' or 'opts.entityName'");

    try {
      this.progressBarService?.increase();
      const entityStore = this.getEntityStore<T>(opts.entityName, { create: false });
      if (!entityStore) return undefined;

      // Do deletion
      const deletedEntities = entityStore.deleteMany(ids, opts);

      // If something deleted
      if (deletedEntities.length > 0) {
        // Mark as dirty
        this._dirty = true;

        // Mark as saved need
        this._$save.emit();
      }

      return deletedEntities;
    } finally {
      this.progressBarService?.decrease();
    }
  }

  async deleteFromTrash<T extends Entity<T>>(
    entity: T,
    opts?: {
      entityName?: string;
    }
  ): Promise<T> {
    if (!entity) return undefined; // skip

    if (!this.started) await this.ready();

    return this.deleteFromTrashById(entity.id, {
      ...opts,
      entityName: (opts && opts.entityName) || this.detectEntityName(entity),
    });
  }

  async deleteFromTrashById<T extends Entity<T>>(
    id: number,
    opts: {
      entityName: string;
      emitEvent?: boolean;
    }
  ): Promise<T> {
    await this.ready();

    if (!opts || isNilOrBlank(opts.entityName)) throw new Error("Missing argument 'opts' or 'entityName'");

    return this.deleteById(id, {
      ...opts,
      entityName: EntitiesStorage.TRASH_PREFIX + opts.entityName,
    });
  }

  async moveToTrash<T extends Entity<T>>(
    entity: T,
    opts?: {
      entityName?: string;
      emitEvent?: boolean;
    }
  ): Promise<T> {
    if (!entity) return undefined; // skip

    if (!this.started) await this.ready();

    const entityName = (opts && opts.entityName) || this.detectEntityName(entity);

    // Delete entity by id, if exists
    const entityStore = this.getEntityStore<T>(entityName);
    if (entityStore) entityStore.delete(entity.id, opts);

    // Clean id (a new id will be set by the trash store)
    entity.id = undefined;

    // Add to trash
    const trashName = EntitiesStorage.TRASH_PREFIX + entityName;
    this.getEntityStore<T>(trashName).save(entity, opts);

    this._$save.emit();

    return entity;
  }

  async moveManyToTrash<T extends Entity<T>>(
    ids: number[],
    opts: {
      entityName: string;
      emitEvent?: boolean;
    }
  ): Promise<T[]> {
    if (!this.started) await this.ready();

    if (!opts || isNilOrBlank(opts.entityName)) throw new Error("Missing argument 'opts.entityName'");

    const entityStore = this.getEntityStore<T>(opts.entityName, { create: false });
    if (!entityStore) return undefined;

    // Do deletion
    const deletedEntities = entityStore.deleteMany(ids, opts);

    // If something deleted
    if (deletedEntities.length > 0) {
      // Clean ids (new ids will be set by the trash store)
      deletedEntities.forEach((e) => (e.id = undefined));

      const trashName = EntitiesStorage.TRASH_PREFIX + opts.entityName;
      this.getEntityStore<T>(trashName, { create: true }).saveAll(deletedEntities, opts);

      // Mark as dirty
      this._dirty = true;

      // Mark as save need
      this._$save.emit();
    }

    return deletedEntities;
  }

  async saveToTrash<T extends Entity<T>>(
    entity: T,
    opts?: {
      entityName?: string;
      emitEvent?: boolean;
    }
  ): Promise<T> {
    if (!entity) return undefined; // skip

    if (!this.started) await this.ready();

    const entityName = (opts && opts.entityName) || this.detectEntityName(entity);
    const trashName = EntitiesStorage.TRASH_PREFIX + entityName;
    this.getEntityStore<T>(trashName).save(entity, opts);

    this._$save.emit();

    return entity;
  }

  async clearTrash(entityName: string) {
    if (!this.started) await this.ready();

    const trashName = EntitiesStorage.TRASH_PREFIX + entityName;
    const entityStore = this.getEntityStore(trashName, { create: false });
    if (!entityStore) return; // Skip

    entityStore.reset();
    this._dirty = true;

    this._$save.emit();
  }

  persist(opts = { skipIfPristine: true }): Promise<void> {
    return this.storeLocally(opts);
  }

  /* -- protected methods -- */

  protected getEntityStore<T extends IEntity<T, ID>, ID = number>(
    name: string,
    opts?: {
      create?: boolean;
    }
  ): EntityStore<T, ID> {
    let store = this._data[name];
    if (!store && (!opts || opts.create !== false)) {
      if (this._debug) console.debug(`[entities-storage] Creating store ${name}`);
      const typePolicy = this._typePolicies[name];
      store = new EntityStore<T, ID>(name, this.storage, typePolicy);
      this._data[name] = store;
    }
    return store;
  }

  protected detectEntityName(entityOrName: string | Entity<any, any>): string {
    if (!entityOrName) throw Error('Unable to detect entityName of object: ' + entityOrName);
    if (typeof entityOrName === 'string') return entityOrName;
    if (entityOrName.__typename) {
      return entityOrName.__typename;
    }
    return entityOrName.constructor.name + 'VO';
  }

  protected async restoreLocally() {
    const entityNames = await this.storage.get(ENTITIES_STORAGE_KEY_PREFIX);
    if (!entityNames) return;

    const now = this._debug && Date.now();
    if (this._debug) console.info('[entities-storage] Restoring entities...');
    const entitiesCount = (
      await Promise.all<number>(entityNames.map((name) => this.getEntityStore<any>(name)).map((store: EntityStore<any>) => store.restore()))
    ).reduce((res, count) => res + count, 0);

    if (this._debug) console.debug(`[entities-storage] Restoring entities [OK] ${entitiesCount} entities found in ${Date.now() - now}ms`);
  }

  protected async storeLocally(opts = { skipIfPristine: true }): Promise<void> {
    if (opts.skipIfPristine && !this.dirty) return; // skip

    // Saving progress: report this save later
    if (this._saving) {
      if (this._debug) console.debug('[entities-storage] Previous persist not finished. Waiting...');
      this._$save.emit();
      return;
    }

    this._saving = true;
    this._dirty = false;
    this.progressBarService?.increase();
    const entityNames = (this._data && Object.keys(this._data)) || [];

    const now = Date.now();
    if (this._debug) console.debug('[entities-storage] Persisting...');

    let currentEntityName;
    return chainPromises(
      entityNames.map((entityName) => () => {
        currentEntityName = entityName;
        const entityStore = this.getEntityStore(entityName, { create: false });

        if (!entityStore) {
          console.warn(`[entities-storage] Persisting ${entityName}: store not found!`);
          return;
        }

        // SKip store if pristine
        if (opts.skipIfPristine && !entityStore.dirty) return Promise.resolve();

        // Persist store
        return entityStore.persist(opts).then((count) => {
          // If no entity found, remove from the entity names array
          if (!count) {
            const entityNameIndex = entityNames.findIndex((e) => e === entityName);
            if (entityNameIndex !== -1) entityNames.splice(entityNameIndex, 1);
          }
        });
      })
    )
      .then(() => {
        currentEntityName = undefined;
        return isEmptyArray(entityNames)
          ? this.storage.remove(ENTITIES_STORAGE_KEY_PREFIX)
          : this.storage.set(ENTITIES_STORAGE_KEY_PREFIX, entityNames);
      })
      .then(() => {
        if (this._debug) console.debug(`[entities-storage] Persisting [OK] ${entityNames.length} stores saved in ${Date.now() - now}ms...`);
        this._saving = false;
        this.progressBarService?.decrease();
      })
      .catch((err) => {
        this._saving = false;
        this.progressBarService?.decrease();
        if (currentEntityName) {
          console.error(`[entities-storage] Error while persisting ${currentEntityName}`, err);
        } else {
          console.error(`[entities-storage] Error while persisting: ${(err && err.message) || err}`, err);
        }
        throw err;
      });
  }
}
