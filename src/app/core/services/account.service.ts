import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { CryptoService, Keypair } from './crypto.service';
import { Department } from './model/department.model';
import { Account, AccountUtils, UserSettings } from './model/account.model';
import { Person, PersonUtils, UserProfileLabel } from './model/person.model';
import { LocalSettings, UsageMode } from './model/settings.model';
import { BehaviorSubject, from, Observable, Subject, Subscription } from 'rxjs';
import { FetchPolicy, gql } from '@apollo/client/core';
import { Storage } from '@ionic/storage-angular';

import { isEmptyArray, isNil, sleep, toNumber } from '../../shared/functions';
import { BaseGraphqlService } from './base-graphql-service.class';
import { ErrorCodes, ServerErrorCodes } from './errors';
import { GraphqlService } from '../graphql/graphql.service';
import { LocalSettingsService } from './local-settings.service';
import { FormFieldDefinition, FormFieldDefinitionMap } from '../../shared/form/field.model';
import { NetworkService } from './network.service';
import { AuthTokenType } from './network.types';
import { FileService } from '../../shared/file/file.service';
import { MINIFY_ENTITY_FOR_POD, Referential, ReferentialUtils } from './model/referential.model';
import { StatusIds } from './model/model.enum';
import { Base58 } from './base58';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { fromDateISOString } from '../../shared/dates';
import { firstNotNilPromise } from '../../shared/observables';
import { debounceTime, filter, map, switchMap } from 'rxjs/operators';
import { BaseEntityGraphqlMutations, BaseEntityGraphqlQueries, BaseEntityGraphqlSubscriptions } from './base-entity-service.class';
import { Moment } from 'moment';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { OverlayEventDetail } from '@ionic/core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { TokenScope, UserToken } from './model/token.model';
import { AppPropertiesUtils } from '../form/properties/properties.utils';

export declare interface AccountDetails {
  loaded: boolean;
  keypair: Keypair;
  authToken: string;
  authBasic?: string;
  pubkey: string;
  person: Person;
  department: Department;
  mainProfile: string;
}

export interface AuthData {
  username: string;
  password: string;
  offline?: boolean;
}

export interface RegisterData extends AuthData {
  account: Account;
}

export interface ChangePasswordData {
  password: string;
  confirmPassword: string;
  email: string;
}

const TOKEN_STORAGE_KEY = 'token';
const PUBKEY_STORAGE_KEY = 'pubkey';
const SECKEY_STORAGE_KEY = 'seckey';
const ACCOUNT_STORAGE_KEY = 'account';

const DEFAULT_AVATAR_IMAGE = 'assets/img/person.png';

export interface UserSettingsOptions {
  options: FormFieldDefinitionMap;
}

export const APP_USER_SETTINGS_OPTIONS = new InjectionToken<UserSettingsOptions>('UserSettingsOptions');
export const APP_USER_TOKEN_SCOPES = new InjectionToken<TokenScope[]>('UserTokenScopes');

/* ------------------------------------
 * GraphQL queries
 * ------------------------------------*/
const Fragments = {
  account: gql`
    fragment AccountFragment on AccountVO {
      id
      firstName
      lastName
      email
      pubkey
      avatar
      statusId
      updateDate
      creationDate
      profiles
      settings {
        ...UserSettingsFragment
      }
      department {
        id
        label
        name
        __typename
      }
      __typename
    }
  `,

  settings: gql`
    fragment UserSettingsFragment on UserSettingsVO {
      id
      locale
      latLongFormat
      content
      nonce
      updateDate
      __typename
    }
  `,

  token: gql`
    fragment UserTokenFragment on UserTokenVO {
      id
      pubkey
      name
      flags
      expirationDate
      lastUsedDate
      creationDate
      updateDate
      __typename
    }
  `,
};

// Account queries
const Queries: Partial<BaseEntityGraphqlQueries> & { loadWithTokens: any } = {
  load: gql`
    query Account {
      data: account {
        ...AccountFragment
      }
    }
    ${Fragments.account}
    ${Fragments.settings}
  `,

  loadWithTokens: gql`
    query AccountWithToken {
      data: account {
        ...AccountFragment
        tokens {
          ...UserTokenFragment
        }
      }
    }
    ${Fragments.account}
    ${Fragments.settings}
    ${Fragments.token}
  `,
};

// Check email query
const IsEmailExistsQuery: any = gql`
  query IsEmailExists($email: String, $hash: String) {
    isEmailExists(email: $email, hash: $hash)
  }
`;

export declare interface IsEmailExistsVariables {
  email: string;
  hash: string;
}

// Account mutations
const Mutations: Partial<BaseEntityGraphqlMutations> & { saveWithTokens: any; saveSettings: any; create: any } = {
  save: gql`
    mutation SaveAccount($data: AccountVOInput) {
      data: saveAccount(account: $data) {
        ...AccountFragment
      }
    }
    ${Fragments.account}
    ${Fragments.settings}
  `,

  saveWithTokens: gql`
    mutation SaveAccountWithTokens($data: AccountVOInput) {
      data: saveAccount(account: $data) {
        ...AccountFragment
        tokens {
          ...UserTokenFragment
        }
      }
    }
    ${Fragments.account}
    ${Fragments.settings}
    ${Fragments.token}
  `,

  create: gql`
    mutation CreateAccount($data: AccountVOInput) {
      data: createAccount(account: $data) {
        ...AccountFragment
      }
    }
    ${Fragments.account}
    ${Fragments.settings}
  `,

  saveSettings: gql`
    mutation SaveSettings($data: UserSettingsVOInput) {
      data: saveSettings(settings: $data) {
        ...UserSettingsFragment
      }
    }
    ${Fragments.settings}
  `,
};

// Sent confirmation email
const SendConfirmEmailQuery: any = gql`
  query sendAccountConfirmationEmail($email: String, $locale: String) {
    data: sendAccountConfirmationEmail(email: $email, locale: $locale)
  }
`;

// Confirm account email
const ConfirmEmailQuery: any = gql`
  query confirmAccountEmail($email: String, $code: String) {
    data: confirmAccountEmail(email: $email, code: $code)
  }
`;

// Send email to reset password
const SendResetPasswordEmailQuery: any = gql`
  query SendResetPasswordEmail($username: String!, $locale: String!) {
    sendResetPasswordEmail(username: $username, locale: $locale)
  }
`;

const ResetAccountPubkeyQuery: any = gql`
  query ResetPubkey($username: String!, $token: String!) {
    data: resetAccountPubkey(username: $username, token: $token)
  }
`;

const UpdatePubKeyQuery: any = gql`
  query UpdateAccountPubkey($pubkey: String!) {
    data: updateAccountPubkey(pubkey: $pubkey)
  }
`;

// Authentication  query
const AuthQuery: any = gql`
  query Auth($token: String) {
    authenticate(token: $token)
  }
`;

// New auth challenge query
const AuthChallengeQuery: any = gql`
  query AuthChallenge {
    authChallenge {
      challenge
      pubkey
      signature
    }
  }
`;

interface IAuthChallenge {
  pubkey: string;
  challenge: string;
  signature: string;
}

const AccountSubscriptions: BaseEntityGraphqlSubscriptions = {
  listenChanges: gql`
    subscription updateAccount($interval: Int) {
      data: updateAccount(interval: $interval) {
        id
        updateDate
      }
    }
  `,
};

export interface AccountWatchOptions {
  intervalInSeconds?: number;
}

@Injectable({ providedIn: 'root', deps: [ENVIRONMENT] })
export class AccountService extends BaseGraphqlService<Account, any, number, Account> {
  onLogin = new Subject<Account>();
  onWillLogout = new Subject<Account>();
  onLogout = new Subject<void>();
  onChange = new Subject<Account>();
  onAuthTokenChange = new BehaviorSubject<string | undefined>(undefined);
  onAuthBasicChange = new BehaviorSubject<string | undefined>(undefined);

  private _listenChangesSubscription: Subscription = null;
  private readonly _enableListenChanges: boolean;
  private readonly _listenIntervalInSeconds: number;
  private _cache: AccountDetails = {
    loaded: false,
    keypair: null,
    authToken: null,
    authBasic: null,
    pubkey: null,
    mainProfile: null,
    person: null,
    department: null,
  };
  private readonly _optionDefs: FormFieldDefinition[];
  private _$additionalFields = new BehaviorSubject<FormFieldDefinition[]>([]);
  private _tokenType$ = new BehaviorSubject<AuthTokenType>(undefined);
  private _apiTokenEnabled = true;

  get account(): Account {
    return this._cache.loaded ? this._data : undefined;
  }

  get person(): Person {
    if (this._cache.loaded && !this._cache.person) {
      this._cache.person = this._cache.loaded ? this._data.asPerson() : undefined;
    }
    return this._cache.person;
  }

  get department(): Department {
    if (this._cache.loaded && !this._cache.department) {
      this._cache.department = this._cache.loaded ? this._data.asPerson().department : undefined;
    }
    return this._cache.department;
  }

  get tokenType(): AuthTokenType {
    return this._tokenType$.value;
  }

  set tokenType(value: AuthTokenType) {
    if (this._tokenType$.value !== value) {
      console.info('[account] Using authentication token type: ' + value);
      this._tokenType$.next(value);
      // Reset values
      this._cache.authToken = undefined;
      this.onAuthTokenChange.next(undefined);
      this._cache.authBasic = undefined;
      this.onAuthBasicChange.next(undefined);
    }
  }

  get apiTokenEnabled(): boolean {
    return this._apiTokenEnabled;
  }

  set apiTokenEnabled(value: boolean) {
    this._apiTokenEnabled = value;
  }

  constructor(
    protected network: NetworkService,
    protected graphql: GraphqlService,
    protected settings: LocalSettingsService,
    protected storage: Storage,
    protected file: FileService,
    @Optional() private translate: TranslateService,
    @Optional() private toastController: ToastController,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_USER_SETTINGS_OPTIONS) options: UserSettingsOptions
  ) {
    super(graphql, environment);
    this._enableListenChanges = !environment.account || environment.account.enableListenChanges !== false; // True by default
    this._listenIntervalInSeconds = toNumber(environment.account && environment.account.listenIntervalInSeconds, 0); // no timer by default

    this._debug = !environment.production;
    if (this._debug) console.debug('[account-service] Creating service');
    this._optionDefs = Object.values(options?.options || {});

    this.resetData();

    // Send auth token to the graphql layer, when changed
    this.onAuthTokenChange.subscribe((token) => this.graphql.setAuthToken(token));
    this.onAuthBasicChange.subscribe((basic) => this.graphql.setAuthBasic(basic));

    // Force network to wait account service, after getting connection to the peer
    this.network.on('beforeTryOnlineFinish', async (online) => {
      // If online, wait a full restart, because it can force offline mode
      if (online && (!this.started || this.isLogin())) {
        console.debug('[account] Force networkService.tryOnline() to wait, that graphql and account service is restarted...');
        await sleep(500); // wait graphql service to be restarted
        if (!this.started) await this.ready();
      }
    });
  }

  async ngOnStart(): Promise<Account> {
    await Promise.all([
      this.settings.ready(),
      // Wait token type to be set
      firstNotNilPromise(this._tokenType$, { stop: this.stopSubject }),
    ]);

    // Listen graphql start (or restart)
    this.registerSubscription(
      this.graphql.stopSubject.subscribe(() => {
        if (this.started && this.isLogin()) {
          console.debug('[account] Restarting, to retry to authenticate...');
          this.restart();
        }
      })
    );

    await this.restoreLocally();
    await this.listenSettings();

    return this._data;
  }

  protected async ngOnStop(): Promise<void> {
    const hadAuthToken = this._cache.authToken && true;
    const hadAuthBasic = this._cache.authBasic && true;
    const hadAuth = hadAuthToken || hadAuthBasic;
    this.resetData();
    if (hadAuth) {
      this.onLogout.next();
      this.onChange.next(undefined);
      if (hadAuthToken) this.onAuthTokenChange.next(undefined);
      if (hadAuthBasic) this.onAuthBasicChange.next(undefined);
    }
  }

  isLogin(): boolean {
    return !!(this._cache.pubkey && this._cache.loaded);
  }

  isAuth(): boolean {
    return !!(this._cache.pubkey && this._cache.keypair && this._cache.keypair.secretKey);
  }

  hasMinProfile(userProfile: UserProfileLabel): boolean {
    // should be login, and status ENABLE or TEMPORARY
    if (!this._data || !this._data.pubkey || (this._data.statusId !== StatusIds.ENABLE && this._data.statusId !== StatusIds.TEMPORARY)) {
      return false;
    }
    return PersonUtils.hasUpperOrEqualsProfile(this._data.profiles, userProfile);
  }

  hasExactProfile(label: UserProfileLabel): boolean {
    return AccountUtils.hasExactProfile(this._data, label);
  }

  hasProfileAndIsEnable(userProfile: UserProfileLabel): boolean {
    return AccountUtils.hasProfileAndIsEnable(this._data, userProfile);
  }

  isAdmin(): boolean {
    return this.hasProfileAndIsEnable('ADMIN');
  }

  isSupervisor(): boolean {
    return this.hasProfileAndIsEnable('SUPERVISOR');
  }

  isUser(): boolean {
    return this.hasProfileAndIsEnable('USER');
  }

  /**
   * @deprecated
   * @param mode
   */
  isUsageMode(mode: UsageMode): boolean {
    return this.settings.isUsageMode(mode);
  }

  isOnlyGuest(): boolean {
    // Should be login, and status ENABLE or TEMPORARY
    if (!this._data || !this._data.pubkey || (this._data.statusId !== StatusIds.ENABLE && this._data.statusId !== StatusIds.TEMPORARY)) return false;
    // Profile less then user
    return !PersonUtils.hasUpperOrEqualsProfile(this._data.profiles, 'USER');
  }

  /**
   *
   * @param recorderDepartment
   * @deprecated use ProgramRefService.canUserWriteEntity() instead
   */
  canUserWriteDataForDepartment(recorderDepartment: Referential | any): boolean {
    if (ReferentialUtils.isEmpty(recorderDepartment)) {
      return this.isAdmin();
    }

    // Should be login, and status ENABLE
    if (!this._data || !this._data.pubkey || this._data.statusId !== StatusIds.ENABLE) return false;

    if (!this._data.department || !this._data.department.id) {
      console.warn('User account has no department ! Unable to check write right against recorderDepartment');
      return false;
    }

    // Same recorder department: OK, user can write
    if (this._data.department.id === recorderDepartment.id) return true;

    // Cannot write
    return false;
  }

  async register(data: RegisterData): Promise<Account> {
    if (this.isLogin()) {
      throw new Error('User already login. Please logout before register.');
    }
    if (!data.username || !data.password) throw new Error('Missing required username or password');

    if (this._debug) console.debug('[account] Register new user account...', data.account);
    this._cache.loaded = false;
    const now = Date.now();

    try {
      const keypair = await CryptoService.scryptKeypair(data.username, data.password);
      data.account.pubkey = Base58.encode(keypair.publicKey);

      // Default values
      data.account.settings = data.account.settings || new UserSettings();
      data.account.settings.locale = this.settings.locale;
      data.account.settings.latLongFormat = this.settings.latLongFormat;
      data.account.settings.content = data.account.settings.content || {};
      data.account.department.id = data.account.department.id || this.environment.defaultDepartmentId;

      this._cache.keypair = keypair;
      const account = await this.saveRemotely(data.account, keypair);

      // Default values
      account.avatar = account.avatar || this.environment.baseUrl + DEFAULT_AVATAR_IMAGE;
      this._cache.mainProfile = PersonUtils.getMainProfile(account.profiles);

      this._data = account;
      this._cache.pubkey = account.pubkey;

      // Try to auth on pod
      await this.authenticate(data);

      this._cache.loaded = true;

      await this.saveLocally();

      console.debug(`[account] Account successfully registered in ${Date.now() - now}ms`);

      // Emit events
      this.onLogin.next(this._data);
      this.onChange.next(this._data);

      // Listen remote changes
      if (this._enableListenChanges) this.startListenRemoteChanges();

      return this._data;
    } catch (error) {
      console.error((error && error.message) || error);
      this.resetData();
      throw error;
    }
  }

  async authenticate(data?: AuthData) {
    // Wait the auth token, then continue
    const tokenType = await firstNotNilPromise(this._tokenType$);

    // Basic auth
    if (tokenType === 'basic' || tokenType === 'basic-and-token') {
      // Generate the authBasic, if used
      if (!this._cache.authBasic) {
        // Skip if token already provided
        if (!(this._cache.authToken && tokenType === 'basic-and-token')) {
          if (!data || !data.username || !data.password) throw new Error('Missing username and password');
          this._cache.authBasic = CryptoService.encodeBase64(`${data.username}:${data.password}`);
        }
      }
      this.onAuthBasicChange.next(this._cache.authBasic);
    }

    // Generate the authToken, if used
    if (tokenType === 'token' || tokenType === 'basic-and-token') {
      try {
        this._cache.authToken = await this.authenticateAndGetToken(this._cache.authToken);
      } catch (error) {
        // Never authenticate, or not ready for offline mode => exit
        console.error(error);
        this.resetData();
        throw error;
      }
    }

    // Forget authBasic, to switch to authToken
    if (tokenType === 'basic-and-token') {
      this._cache.authBasic = undefined;
      this.onAuthBasicChange.next(undefined);
    }
  }

  async login(data: AuthData): Promise<Account> {
    if (!data || !data.username || !data.password) throw new Error('Missing required username or password');

    console.debug('[account] Login...');

    let keypair;
    try {
      keypair = await CryptoService.scryptKeypair(data.username, data.password);
    } catch (error) {
      console.error(error);
      this.resetData();
      throw { code: ErrorCodes.UNKNOWN_ERROR, message: 'ERROR.SCRYPT_ERROR' };
    }

    // Store pubkey+keypair
    this._cache.pubkey = Base58.encode(keypair.publicKey);
    this._cache.keypair = keypair;

    // Try to load previous token
    let previousToken: string = await this.storage.get(TOKEN_STORAGE_KEY);
    previousToken = (previousToken && previousToken.startsWith(this._cache.pubkey) && previousToken) || null;

    // Offline mode
    const offline = this.settings.hasOfflineFeature() && (this.network.offline || data.offline === true);
    if (offline) {
      this._cache.authToken = previousToken;

      // Make sure network if set as offline
      this.network.setForceOffline(true, { showToast: false });
      console.info(`[account] Login [OK] {pubkey: ${this._cache.pubkey.substr(0, 8)}}, {offline: true}`);
    }

    // Online mode: try to auth on pod
    else {
      try {
        await this.authenticate(data);
      } catch (error) {
        // Never authenticate, or not ready for offline mode => exit
        console.error(error);
        this.resetData();
        throw error;
      }
    }

    // Load account data
    try {
      await this.loadData({ offline, fetchPolicy: 'network-only' });
    } catch (err) {
      // If account not found, check if email is valid
      if (err && +err.code === ErrorCodes.LOAD_ACCOUNT_ERROR) {
        // Check email exists
        if (data.username.indexOf('@') !== -1) {
          let isEmailExists;
          try {
            isEmailExists = await this.isEmailExists(data.username);
          } catch (otherError) {
            throw err; // resend the first error
          }

          // Email not exists (no account)
          if (!isEmailExists) {
            throw { code: ErrorCodes.UNKNOWN_ACCOUNT_EMAIL, message: 'ERROR.UNKNOWN_ACCOUNT_EMAIL' };
          }
        }

        // Email exists, so error = 'bad password'
        throw { code: ErrorCodes.BAD_PASSWORD, message: 'ERROR.BAD_PASSWORD' };
      }

      throw err; // resend the first error
    }

    try {
      // Store to local storage
      await this.saveLocally();
    } catch (error) {
      console.error(error);
      this.resetData();
      throw error;
    }

    // Emit event to observers
    this.onLogin.next(this._data);
    this.onChange.next(this._data);

    if (this._enableListenChanges) this.startListenRemoteChanges();

    return this._data;
  }

  async reload(opts?: { showToast?: boolean }): Promise<Account> {
    if (!this._cache.pubkey) throw new Error('User not logged');
    if (this.network.offline) throw new Error('Cannot check account in offline mode');

    const now = Date.now();
    console.debug(`[account] Reloading account...`);
    const wasLogin = this.isLogin();

    try {
      await this.loadData();
      await this.saveLocally();

      console.debug(`[account] Reloading account [OK] in ${Date.now() - now}ms`);

      // Emit login event to subscribers
      this.onLogin.next(this._data);
      this.onChange.next(this._data);

      // Display toast (without await, because not need to wait toast close event)
      if (!opts || opts.showToast !== false) {
        this.showToast({ message: 'ACCOUNT.INFO.RELOADED', type: 'info' });
      }

      return this._data;
    } catch (error) {
      // Cannot reload but was login: force to logout
      if (wasLogin && !this.isLogin()) {
        console.error(`[account] Reloading account failed. Will force logout...`, error);
        await this.logout();
      } else {
        throw error;
      }
    }
  }

  /**
   * Create or update an user account, to the remote storage
   *
   * @param account
   */
  async save(account: Account): Promise<Account> {
    if (!this._cache.pubkey) return Promise.reject('User not logged');
    if (this._cache.pubkey !== account.pubkey) return Promise.reject('Not user account');

    account = await this.saveRemotely(account, this._cache.keypair);

    // Set defaults
    account.avatar = account.avatar || this.environment.baseUrl + DEFAULT_AVATAR_IMAGE;
    this._cache.mainProfile = PersonUtils.getMainProfile(account.profiles);

    this._data = account;

    // Update cache
    this._cache.person = account.asPerson();
    this._cache.department = this._cache.person.department;

    this._cache.loaded = true;

    // Save locally (in storage)
    await this.saveLocally();

    // Send event
    this.onLogin.next(this._data);
    this.onChange.next(this._data);

    return this._data;
  }

  async logout(): Promise<void> {
    const hadAuthToken = this._cache.authToken && true;
    const hadAuthBasic = this._cache.authBasic && true;
    const pubkey = this._cache && this._cache.pubkey;

    // Notify observers
    this.onWillLogout.next(this.account);

    this.resetData();

    if (!this.settings.hasOfflineFeature()) {
      // Remove all data from the local storage
      await Promise.all([
        this.storage.remove(PUBKEY_STORAGE_KEY),
        this.storage.remove(TOKEN_STORAGE_KEY),
        this.storage.remove(ACCOUNT_STORAGE_KEY),
        (pubkey && this.storage.remove(ACCOUNT_STORAGE_KEY + '#' + pubkey)) || Promise.resolve(),
        this.storage.remove(SECKEY_STORAGE_KEY),
      ]);
    }

    // Offline features enable: need to keep some data
    else {
      // Always remove only secret key
      // But keep:
      // - account by pubkey
      // - auth token
      await Promise.all([this.storage.remove(PUBKEY_STORAGE_KEY), this.storage.remove(ACCOUNT_STORAGE_KEY), this.storage.remove(SECKEY_STORAGE_KEY)]);
    }

    // Clean page history, in local settings
    await this.settings.clearPageHistory();

    // Notify observers
    this.onLogout.next();
    if (hadAuthToken) this.onAuthTokenChange.next(undefined);
    if (hadAuthBasic) this.onAuthBasicChange.next(undefined);
    this.onChange.next(undefined);
  }

  /**
   * Load a account
   *
   * @param opts
   */
  async load(opts?: { offline?: boolean; fetchPolicy?: FetchPolicy; query?: any }): Promise<Account | undefined> {
    const now = this._debug && Date.now();
    if (this._debug) console.debug(`[account] Loading account...`);
    let json: any;

    // Load locally
    const offline =
      (this.network.offline && (!opts || (opts.fetchPolicy !== 'network-only' && opts.fetchPolicy !== 'no-cache'))) ||
      (opts && opts.offline === true);
    if (offline) {
      json = await this.storage.get(ACCOUNT_STORAGE_KEY);
      json = (json && typeof json === 'string' && JSON.parse(json)) || json;
      json = (json && this._cache.pubkey && json.pubkey === this._cache.pubkey && json) || null;
      if (!json && this._cache.pubkey) {
        json = await this.storage.get(ACCOUNT_STORAGE_KEY + '#' + this._cache.pubkey);
        json = (json && typeof json === 'string' && JSON.parse(json)) || json;
      }
    }

    // Load remotely
    else {
      const query = opts?.query || (this.apiTokenEnabled ? Queries.loadWithTokens : Queries.load);
      const { data } = await this.graphql.query<{ data: any }>({
        query,
        error: { code: ErrorCodes.LOAD_ACCOUNT_ERROR, message: 'ERROR.LOAD_ACCOUNT_ERROR' },
        fetchPolicy: (opts && opts.fetchPolicy) || 'no-cache' || undefined,
      });
      json = data;
    }

    if (json) {
      const account = Account.fromObject(json);
      if (this._debug) console.debug(`[account] Account loaded in ${Date.now() - now}ms`, account);
      return account;
    } else {
      console.warn(`[account] Account not found !`);
      return undefined;
    }
  }

  async generateToken(flags: number): Promise<string | undefined> {
    const now = this._debug && Date.now();
    if (this._debug) console.debug(`[account] Generate token...`);

    const authChallenge = await this.getAuthChallenge();
    // Add Flags to challenge
    const challenge = `${authChallenge.challenge}:${flags}`;
    const signature = await CryptoService.sign(challenge, this._cache.keypair);
    const newToken = `${this._cache.pubkey}:${challenge}|${signature}`;

    if (newToken) {
      if (this._debug) console.debug(`[account] Token generated in ${Date.now() - now}ms`);
    } else {
      console.warn(`[account] Token generation failed !`);
    }

    return newToken;
  }

  /**
   * Check if email is available for new account registration.
   * Throw an error if not available
   *
   * @param email
   */
  async checkEmailAvailable(email: string): Promise<void> {
    const isEmailExists = await this.isEmailExists(email);
    if (isEmailExists) {
      throw { code: ErrorCodes.EMAIL_ALREADY_REGISTERED, message: 'ERROR.EMAIL_ALREADY_REGISTERED' };
    }
  }

  /**
   * Check if email is exists in server.
   *
   * @param email
   */
  async isEmailExists(email: string): Promise<boolean> {
    if (this._debug) console.debug('[account] Checking if {' + email + '} exists...');

    const data = await this.graphql.query<{ isEmailExists: boolean }, IsEmailExistsVariables>({
      query: IsEmailExistsQuery,
      variables: {
        email,
        hash: undefined,
      },
    });

    if (this._debug) console.debug('[account] Email exist: ' + (data && data.isEmailExists));

    return data && data.isEmailExists;
  }

  async sendConfirmationEmail(email: string, locale?: string): Promise<boolean> {
    locale = locale || this.settings.locale;
    console.debug('[account] Sending confirmation email to {' + email + '} with locale {' + locale + '}...');

    const { data } = await this.graphql.query<{ data: boolean }>({
      query: SendConfirmEmailQuery,
      variables: {
        email,
        locale,
      },
      error: {
        code: ErrorCodes.SENT_CONFIRMATION_EMAIL_FAILED,
        message: 'ERROR.SENT_ACCOUNT_CONFIRMATION_EMAIL_FAILED',
      },
    });

    return data;
  }

  async confirmEmail(email: string, code: string): Promise<boolean> {
    console.debug('[account] Sending confirm request for email {' + email + '} with code {' + code + '}...');

    const { data } = await this.graphql.query<{ data: boolean }>({
      query: ConfirmEmailQuery,
      variables: {
        email,
        code,
      },
      error: {
        code: ErrorCodes.CONFIRM_EMAIL_FAILED,
        message: 'ERROR.CONFIRM_ACCOUNT_EMAIL_FAILED',
      },
    });
    return data;
  }

  async sendResetPasswordEmail(username: string, locale?: string): Promise<boolean> {
    locale = locale || this.settings.locale;

    console.debug(`[account] Sending change password email to {${username}} with locale {${locale}}...`);

    const { data: isOk } = await this.graphql.query<{ data: boolean }>({
      query: SendResetPasswordEmailQuery,
      variables: {
        username,
        locale,
      },
      error: {
        code: ErrorCodes.AUTH_SEND_RESET_PASSWORD_EMAIL_ERROR,
        message: 'ACCOUNT.ERROR.SEND_RESET_PASSWORD_EMAIL_ERROR',
      },
      fetchPolicy: 'no-cache',
    });
    return isOk;
  }

  async updatePubkey(data: AuthData): Promise<boolean> {
    if (!data.username || !data.password) throw new Error('Missing required username or password');
    if (!this.isLogin()) throw new Error('Should be login');

    const keypair = await CryptoService.scryptKeypair(data.username, data.password);
    const pubkey = Base58.encode(keypair.publicKey);

    console.debug(`[account] Updating account pubkey to ${pubkey} ...`);

    const { data: res } = await this.graphql.query<{ data: boolean }>({
      query: UpdatePubKeyQuery,
      variables: { pubkey },
      error: {
        code: ErrorCodes.AUTH_UPDATE_PUBKEY_ERROR,
        message: 'ACCOUNT.ERROR.UPDATE_PUBKEY_ERROR',
      },
      fetchPolicy: 'no-cache',
    });

    console.debug(`[account] Updating account pubkey response`, res);

    if (res) {
      await this.logout();
      await this.login(data);
    }

    return res;
  }

  async resetPubkey(data: AuthData, token: string): Promise<boolean> {
    if (this.isLogin()) {
      await this.logout();
    }

    // Parse challenge
    const challenge = token.split('|', 2)[0].split(':', 2)[1];
    const keypair = await CryptoService.scryptKeypair(data.username, data.password);
    const pubkey = Base58.encode(keypair.publicKey);
    const signature = await CryptoService.sign(challenge, keypair);
    const newToken = `${pubkey}:${challenge}|${signature}`;

    console.debug(`[account] Resetting account pubkey, using token: `, newToken);

    const { data: res } = await this.graphql.query<{ data: boolean }>({
      query: ResetAccountPubkeyQuery,
      variables: { token: newToken, username: data.username },
      error: {
        code: ErrorCodes.AUTH_RESET_PUBKEY_ERROR,
        message: 'ACCOUNT.ERROR.RESET_PUBKEY_ERROR',
      },
      fetchPolicy: 'no-cache',
    });

    // Login
    if (res) {
      await this.login(data);
    }

    return res;
  }

  listenChanges(opts?: AccountWatchOptions): Subscription {
    if (this._enableListenChanges) return new Subscription(); // Already started: skip

    return this.startListenRemoteChanges(opts);
  }

  get additionalFields(): FormFieldDefinition[] {
    return this._$additionalFields.getValue();
  }

  get $additionalFields(): Observable<FormFieldDefinition[]> {
    return this._$additionalFields.asObservable();
  }

  getAdditionalField(key: string): FormFieldDefinition | undefined {
    return this._$additionalFields.getValue().find((f) => f.key === key);
  }

  registerAdditionalField(field: FormFieldDefinition) {
    const values = this._$additionalFields.getValue();
    if (values.some((f) => f.key === field.key)) {
      throw new Error(`Additional account field {key: ${field.key}} already define.`);
    }
    if (this._debug) console.debug(`[account] Found additional account's field {key: ${field.key}}`);
    this._$additionalFields.next(values.concat(field));
  }

  get optionDefs(): FormFieldDefinition[] {
    return this._optionDefs;
  }

  registerOption(def: FormFieldDefinition) {
    if (this._optionDefs.findIndex((f) => f.key === def.key) !== -1) {
      throw new Error(`Additional option {${def.key}} already define.`);
    }
    if (this._debug) console.debug(`[account] Adding additional option {${def.key}}`, def);
    this._optionDefs.push(def);
  }

  registerOptions(defs: FormFieldDefinition[]) {
    (defs || []).forEach((def) => this.registerOption(def));
  }

  /* -- protected method -- */

  protected resetData() {
    this.stopListenRemoteChanges();
    this._cache.loaded = false;
    this._cache.keypair = null;
    this._cache.authToken = null;
    this._cache.authBasic = null;
    this._cache.pubkey = null;
    this._cache.mainProfile = null;
    this._cache.person = null;
    this._cache.department = null;
    this._data = new Account();
  }

  protected async loadData(opts?: { offline?: boolean; fetchPolicy?: FetchPolicy }): Promise<Account> {
    if (!this._cache.pubkey) throw new Error('User not logged');

    this._cache.loaded = false;

    try {
      const account = (await this.load(opts)) || new Account();

      // Set defaults
      account.avatar = account.avatar || this.environment.baseUrl + DEFAULT_AVATAR_IMAGE;
      account.settings = account.settings || new UserSettings();
      account.settings.locale = account.settings.locale || this.settings.locale;
      account.settings.latLongFormat = account.settings.latLongFormat || this.settings.latLongFormat || 'DDMM';
      account.settings.content = account.settings.content || {};

      // Read main profile
      this._cache.mainProfile = PersonUtils.getMainProfile(account.profiles);

      // Update, instead of replace it
      if (this._data) {
        this._data.fromObject(account);
      } else {
        this._data = account;
      }
      this._cache.loaded = true;

      // Apply settings, found in remote account
      if (account.settings && this.settings.settings.accountInheritance) {
        const accountSettings = account.settings.asLocalSettings();
        await this.settings.apply(accountSettings);
      }

      return this._data;
    } catch (error) {
      this.resetData();
      if (error.code && error.message) throw error;

      console.error(error);
      throw {
        code: ErrorCodes.LOAD_ACCOUNT_ERROR,
        message: 'ERROR.LOAD_ACCOUNT_ERROR',
      };
    }
  }

  protected async listenSettings(): Promise<void> {
    // When settings changed: save it remotely
    this.registerSubscription(
      this.settings.onChange
        .pipe(
          debounceTime(1000),
          filter(() => this.isLogin() && this.network.online)
        )
        .subscribe((settings) => this.saveLocalSettingsRemotely(settings))
    );
  }

  protected async restoreLocally(): Promise<Account | undefined> {
    // Restore from storage
    const values = await Promise.all([
      this.storage.get(PUBKEY_STORAGE_KEY),
      this.storage.get(TOKEN_STORAGE_KEY),
      this.storage.get(SECKEY_STORAGE_KEY),
    ]);
    let pubkey = values[0];
    let token = values[1];
    const seckey = values[2];

    // DEV only - auth by token
    if (!this.environment.production && this.environment.defaultAuthValues?.token) {
      token = token || this.environment.defaultAuthValues.token;
      pubkey = pubkey || token?.split(':', 2)[0];
    }

    // Quit if no pubkey (not logged)
    if (!pubkey) return;

    // Quit if could not auth on pod
    const canRemoteAuth = token || seckey || false;
    if (!canRemoteAuth) return;

    if (this._debug) console.debug(`[account] Account restoration...`);

    this._cache.authToken = token;
    this._cache.pubkey = pubkey;
    this._cache.keypair =
      (seckey && {
        publicKey: Base58.decode(pubkey),
        secretKey: Base58.decode(seckey),
      }) ||
      null;

    // Online mode: try to connect to pod
    if (this.network.online) {
      try {
        await this.authenticate();
        if (!this._cache.authToken && !this._cache.authBasic) throw new Error('Authentication failed');
      } catch (error) {
        // Offline feature are enable: continue in offline mode
        if (this.settings.hasOfflineFeature()) {
          console.warn('[account] Unable to authenticate on pod: forcing offline mode');
          this.network.setForceOffline(true, { showToast: false });
          // Continue
        }
        // No offline features enable (=offline mode not allowed)
        else {
          console.error(error);
          this.logout();
          return;
        }
      }
    }

    // Get the account, from pubkey
    let jsonAccount = await this.storage.get(`${ACCOUNT_STORAGE_KEY}#${pubkey}`);
    if (!jsonAccount) {
      // Try using the old storage key
      const accountStr = await this.storage.get(ACCOUNT_STORAGE_KEY);
      jsonAccount = accountStr && ((typeof accountStr === 'string' && JSON.parse(jsonAccount)) || accountStr);
    }

    // Invalid account: do not use it
    if (!jsonAccount || jsonAccount.pubkey !== pubkey) {
      // DEV only: create a fake account with given username
      if (!this.environment.production && this.environment.defaultAuthValues.username) {
        const username = this.environment.defaultAuthValues.username;
        jsonAccount = { pubkey, username, firstName: username, lastName: username };
      } else {
        return;
      }
    }

    // Transform to entity
    const account = Account.fromObject(jsonAccount);

    // Update data
    this._data = account;
    this._cache.mainProfile = PersonUtils.getMainProfile(account.profiles);
    this._cache.loaded = true;

    // Emit event
    this.onLogin.next(this._data);
    this.onChange.next(this._data);

    if (this._enableListenChanges) this.startListenRemoteChanges();

    if (this._debug) console.debug(`[account] Account restoration [OK] {pubkey: ${pubkey.substr(0, 8)}}, {profile: ${this._cache.mainProfile}}`);

    return account;
  }

  /**
   * Save account into the local storage
   */
  protected async saveLocally(): Promise<void> {
    if (!this._cache.pubkey) throw new Error('User not logged');

    if (this._debug) console.debug(`[account] Saving account {${this._cache.pubkey.substring(0, 6)}} in local storage...`);

    // Convert account to json
    const json = this._data.asObject({ keepTypename: true });
    const seckey = (this._cache.keypair && this._cache.keypair.secretKey && Base58.encode(this._cache.keypair.secretKey)) || null;

    // Convert avatar URL to dataUrl (e.g. 'data:image/png:<base64 content>')
    const hasAvatarUrl =
      json.avatar && !json.avatar.endsWith(DEFAULT_AVATAR_IMAGE) && (json.avatar.startsWith('http://') || json.avatar.startsWith('https://'));
    if (hasAvatarUrl && this.network.online) {
      await this.file
        .getImage(json.avatar, {
          thumbnail: true,
          outputType: 'dataUrl',
        })
        .then((dataUrl) => {
          if (dataUrl && this._debug) console.debug('[account] Image fetched: ', dataUrl.substring(0, 50));

          // TODO: make sure to display Base64 image in the menu top header
          //jsonAccount.avatar = dataUrl;
        })
        .catch((err) => {
          console.error(`[account] Error while fetching image: ${json.avatar}: ${err}`);
        });
    }

    try {
      await Promise.all([
        this.storage.set(PUBKEY_STORAGE_KEY, this._cache.pubkey),
        this.storage.set(TOKEN_STORAGE_KEY, this._cache.authToken),
        this.storage.set(`${ACCOUNT_STORAGE_KEY}#${this._cache.pubkey}`, json),
        // Secret key (optional)
        (seckey && this.storage.set(SECKEY_STORAGE_KEY, seckey)) || this.storage.remove(SECKEY_STORAGE_KEY),
        // Remove old storage key
        this.storage.remove(ACCOUNT_STORAGE_KEY),
      ]);

      if (this._debug) console.debug('[account] Account saved in local storage');
    } catch (err) {
      console.error('[account] Error while saving account locally: ' + ((err && err.message) || err), err);
    }
  }

  async saveLocalSettingsRemotely(source: LocalSettings) {
    if (!source || !this.isLogin() || source.accountInheritance === false) return; // Skip
    const account = this.account;

    // Merge local settings into account's settings
    const settings = UserSettings.fromObject(account.settings) || new UserSettings();
    const changed = settings.merge(source, ['locale', 'latLongFormat', 'properties']);

    if (!changed) return; // Skip if unchanged

    // Save remotely
    this.account.settings = settings;
    await this.saveSettingsRemotely(settings);
  }

  /**
   * Create or update an user account
   *
   * @param account
   * @param keyPair
   */
  protected async saveRemotely(account: Account, keyPair?: Keypair): Promise<Account> {
    account.pubkey = account.pubkey || (keyPair && Base58.encode(keyPair.publicKey));
    if (!account.pubkey) throw new Error('Missing account pubkey');

    const now = this._debug && Date.now();
    if (this._debug) console.debug(`[account] Saving account remotely...`);

    const isNew = isNil(account.id);

    // If this is an update: get existing account's updateDate, to avoid 'version error' when saving
    if (!isNew) {
      const existingAccount = await this.load({ fetchPolicy: 'network-only' });
      if (!existingAccount || !existingAccount.updateDate) {
        throw { code: ErrorCodes.ACCOUNT_NOT_EXISTS, message: 'ERROR.ACCOUNT_NOT_EXISTS' };
      }
      this.copyIdAndUpdateDate(existingAccount, account);
    }

    // Merging settings - FIXME - Ludo - A revoir avec la MR sur le user settings
    //account.settings.content['pages'] = this.settings.settings.pages;
    //account.settings.content['pageHistory'] = this.settings.settings.pageHistory;

    // Convert to json
    const json = account.asObject(MINIFY_ENTITY_FOR_POD);

    // User not allow to change his profiles
    delete json.profiles;

    const mutation = isNew ? Mutations.create : this._apiTokenEnabled ? Mutations.saveWithTokens : Mutations.save;

    // Execute mutation
    const { data } = await this.graphql.mutate<{ data: Account }>({
      mutation,
      variables: { data: json },
      error: {
        code: ErrorCodes.SAVE_ACCOUNT_ERROR,
        message: 'ERROR.SAVE_ACCOUNT_ERROR',
      },
    });

    // Copy update properties
    this.copyIdAndUpdateDate(data, account);

    if (this._debug) console.debug(`[account] Account remotely saved in ${Date.now() - now}ms`);

    return account;
  }

  /**
   * Create or update user settings
   *
   * @param settings
   */
  protected async saveSettingsRemotely(settings: UserSettings) {
    const now = this._debug && Date.now();
    if (this._debug) console.debug(`[account] Saving settings remotely...`);

    const json = settings.asObject(MINIFY_ENTITY_FOR_POD);

    // Execute mutation
    const { data } = await this.graphql.mutate<{ data: UserSettings }>({
      mutation: Mutations.saveSettings,
      variables: { data: json },
      error: {
        code: ErrorCodes.SAVE_SETTINGS_ERROR,
        message: 'ERROR.SAVE_SETTINGS_ERROR',
      },
    });

    // Copy update properties
    this.copyIdAndUpdateDateSettings(data, settings);

    if (this._debug) console.debug(`[account] Settings remotely saved in ${Date.now() - now}ms`);

    // Save into account
    if (this.account) this.account.settings = settings;
  }

  protected async authenticateAndGetToken(token?: string, counter?: number): Promise<string> {
    if (!this._cache.pubkey) throw new Error('User not logged');

    if (!counter) console.info('[account] Authentication on pod...');

    if (counter > 4) {
      if (this._debug) console.debug(`[account] Failed to authentication on pod (after ${counter} attempts)`);
      throw { code: ErrorCodes.AUTH_SERVER_ERROR, message: 'ERROR.AUTH_SERVER_ERROR' };
    }

    // Check if valid
    if (token) {
      const data = await this.graphql.query<{ authenticate: boolean }, { token: string }>({
        query: AuthQuery,
        variables: {
          token,
        },
        error: {
          code: ErrorCodes.UNAUTHORIZED,
          message: 'ERROR.UNAUTHORIZED',
        },
        fetchPolicy: 'no-cache',
      });

      // Token is accepted by the server
      if (data && data.authenticate) {
        // Store the token
        this.onAuthTokenChange.next(token);

        console.info(`[account] Authentication on pod [OK] {pubkey: '${this._cache.pubkey.substr(0, 8)}'}`);
        return token; // return the token
      }

      // Continue (will retry with another challenge)
    }

    // Generate a new token
    const authChallenge = await this.getAuthChallenge();
    // TODO: check server pubkey as a valid certificate

    // Do the challenge
    const signature = await CryptoService.sign(authChallenge.challenge, this._cache.keypair);
    const newToken = `${this._cache.pubkey}:${authChallenge.challenge}|${signature}`;

    // iterate with the new token
    return await this.authenticateAndGetToken(newToken, (counter || 1) + 1 /* increment */);
  }

  protected async getAuthChallenge(): Promise<IAuthChallenge> {
    const challengeError = {
      code: ErrorCodes.AUTH_CHALLENGE_ERROR,
      message: 'ERROR.AUTH_CHALLENGE_ERROR',
    };
    const { authChallenge } = await this.graphql.query<{
      authChallenge: IAuthChallenge;
    }>({
      query: AuthChallengeQuery,
      variables: {},
      error: challengeError,
      fetchPolicy: 'network-only',
    });

    // Check challenge
    if (!authChallenge) throw challengeError; // Should never occur

    // Check server signature
    const signatureOK = await CryptoService.verify(authChallenge.challenge, authChallenge.signature, authChallenge.pubkey);
    if (!signatureOK) {
      console.warn('FIXME: Bad peer signature on auth challenge !', authChallenge);
    }
    return authChallenge;
  }

  protected startListenRemoteChanges(opts?: AccountWatchOptions): Subscription {
    if (this._listenChangesSubscription) this.stopListenRemoteChanges(); // Stop previous subscription
    if (this.network.offline) return new Subscription(); // Offline: skip

    this._listenChangesSubscription = this.watchChanges(opts).subscribe({
      next: (data) => {
        console.debug(`[account] Remote update detected at ${data.updateDate}`);
        this.reload();
      },
      error: (err) => {
        if (err && +err.code === ServerErrorCodes.NOT_FOUND) {
          console.info('[account] [WS] Account not exists anymore: force user to logout...', err);
          this.logout();
        } else if (err && +err.code === ServerErrorCodes.UNAUTHORIZED) {
          console.info('[account] [WS] Account not authorized: force user to logout...', err);
          this.logout();
        } else {
          console.warn('[account] [WS] Received error:', err);
        }
      },
    });
    this._listenChangesSubscription.add(() => {
      console.debug(`[account] Stop watching remote changes`);
      this._listenChangesSubscription = null;
    });
    return this._listenChangesSubscription;
  }

  protected stopListenRemoteChanges() {
    this._listenChangesSubscription?.unsubscribe();
  }

  protected watchChanges(opts?: AccountWatchOptions): Observable<Account> {
    if (!this.started) {
      // Wait service ready, then loop
      return from(this.ready()).pipe(switchMap(() => this.watchChanges(opts)));
    }

    if (!this._cache.pubkey) throw new Error('Not logged in');
    if (this.network.offline) throw new Error('Cannot watch account changes: network is offline.');

    const variables = {
      interval: toNumber(opts && opts.intervalInSeconds, this._listenIntervalInSeconds),
    };

    console.debug(`[account] Watching remote changes, every ${variables.interval}s`);

    return this.graphql
      .subscribe<{ data: any }>({
        query: AccountSubscriptions.listenChanges,
        variables,
        error: {
          code: ErrorCodes.SUBSCRIBE_ACCOUNT_ERROR,
          message: 'ACCOUNT.ERROR.SUBSCRIBE_ERROR',
        },
      })
      .pipe(
        map(({ data }) => data),
        // Keep only if newer
        filter((data) => {
          const currentUpdateDate: Moment = fromDateISOString(this._data?.updateDate);
          const remoteUpdateDate: Moment = fromDateISOString(data?.updateDate);
          return remoteUpdateDate && (!currentUpdateDate || !remoteUpdateDate.isSame(currentUpdateDate, 'millisecond'));
        })
      );
  }

  protected copyIdAndUpdateDate(source: Account | undefined, target: Account) {
    if (!source) return;

    target.id = source.id || target.id;
    target.updateDate = source.updateDate || target.updateDate;

    // Update settings
    this.copyIdAndUpdateDateSettings(source.settings, target.settings);

    // Update tokens
    if (this._apiTokenEnabled) {
      this.copyIdAndUpdateDateToken(source.tokens, target.tokens);
    }
  }

  protected copyIdAndUpdateDateSettings(source: UserSettings | undefined, target: UserSettings) {
    if (!source) return;

    target.id = source.id || target.id;
    target.updateDate = source.updateDate || target.updateDate;
  }

  protected copyIdAndUpdateDateToken(sources: UserToken[] | undefined, targets: UserToken[]) {
    if (isEmptyArray(sources)) return;

    sources.forEach((source) => {
      const target = targets.find((value) => UserToken.equals(value, source));
      if (target) {
        target.id = source.id || target.id;
        target.creationDate = source.creationDate || target.creationDate;
        target.updateDate = source.updateDate || target.updateDate;
        target.token = undefined;
      }
    });
  }

  protected showToast<T = any>(opts: ShowToastOptions): Promise<OverlayEventDetail<T>> {
    return Toasts.show(this.toastController, this.translate, opts);
  }
}
