import { NgModule } from '@angular/core';
import { IsOnDeskPipe, IsOnFieldPipe } from './usage-mode.pipes';
import { PersonToStringPipe } from './person-to-string.pipe';
import { AccountToStringPipe, IsLoginAccountPipe } from './account.pipes';
import { DepartmentToStringPipe } from './department-to-string.pipe';
import { ReferentialToStringPipe } from './referential-to-string.pipe';

@NgModule({
  declarations: [
    // Pipes
    IsOnFieldPipe,
    IsOnDeskPipe,
    PersonToStringPipe,
    IsLoginAccountPipe,
    AccountToStringPipe,
    DepartmentToStringPipe,
    ReferentialToStringPipe,
  ],
  exports: [
    // Pipes
    IsOnFieldPipe,
    IsOnDeskPipe,
    PersonToStringPipe,
    IsLoginAccountPipe,
    AccountToStringPipe,
    DepartmentToStringPipe,
    ReferentialToStringPipe,
  ],
})
export class CorePipesModule {}
