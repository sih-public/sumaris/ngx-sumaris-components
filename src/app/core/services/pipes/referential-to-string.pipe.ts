import { Pipe, PipeTransform } from '@angular/core';
import { IReferentialRef, Referential, ReferentialRef, referentialsToString, referentialToString } from '../model/referential.model';

@Pipe({
  name: 'referentialToString',
})
export class ReferentialToStringPipe implements PipeTransform {
  constructor() {}

  transform(
    value: Referential | ReferentialRef | IReferentialRef | any,
    opts?: string[] | { properties?: string[]; separator?: string; propertySeparator?: string }
  ): string {
    const properties = Array.isArray(opts) ? opts : opts?.properties;
    if (Array.isArray(value)) return referentialsToString(value, properties, opts?.['separator'], opts?.['propertySeparator']);
    return referentialToString(value, properties, opts?.['propertySeparator']);
  }
}
