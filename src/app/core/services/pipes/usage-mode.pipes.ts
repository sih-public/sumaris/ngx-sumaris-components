import { Injectable, Pipe, PipeTransform } from '@angular/core';

import { UsageMode } from '../model/settings.model';

@Pipe({
  name: 'isOnField',
})
@Injectable({ providedIn: 'root' })
export class IsOnFieldPipe implements PipeTransform {
  transform(value: UsageMode): boolean {
    return value === 'FIELD';
  }
}

@Pipe({
  name: 'isOnDesk',
})
@Injectable({ providedIn: 'root' })
export class IsOnDeskPipe implements PipeTransform {
  transform(value: UsageMode): boolean {
    return value !== 'FIELD';
  }
}
