import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Person, PersonUtils } from '../model/person.model';

@Pipe({
  name: 'personToString',
})
@Injectable({ providedIn: 'root' })
export class PersonToStringPipe implements PipeTransform {
  transform(value: Person | Person[], separator?: string, opts?: { withDepartment?: boolean }): string {
    if (Array.isArray(value)) return PersonUtils.personsToString(value, separator, opts);
    return PersonUtils.personToString(value as Person, opts);
  }
}
