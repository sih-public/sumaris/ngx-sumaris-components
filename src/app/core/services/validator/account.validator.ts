import { Injectable } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { Account } from '../model/account.model';
import { AccountService } from '../account.service';
import { UserSettingsValidatorService } from './user-settings.validator';
import { PersonValidatorOptions, PersonValidatorService } from '../../../admin/services/validator/person.validator';
import { TranslateService } from '@ngx-translate/core';

export interface AccountValidatorOptions extends PersonValidatorOptions {
  withSettings?: boolean;
  withTokens?: boolean;
}

@Injectable({ providedIn: 'root' })
export class AccountValidatorService<
  T extends Account = Account,
  O extends AccountValidatorOptions = AccountValidatorOptions,
> extends PersonValidatorService<T, O> {
  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected translate: TranslateService,
    protected accountService: AccountService,
    protected userSettingsValidatorService: UserSettingsValidatorService
  ) {
    super(formBuilder, translate, accountService);
  }

  getFormGroupConfig(data?: T, opts?: O): { [key: string]: any[] } {
    opts = this.fillDefaultOptions(opts);

    const formConfig = super.getFormGroupConfig(data, opts);

    if (opts.withSettings) {
      formConfig.settings = this.userSettingsValidatorService.getFormGroup(data?.settings);
    }
    if (opts.withTokens) {
      formConfig.tokens = [data?.tokens || null];
    }

    return formConfig;
  }

  protected fillDefaultOptions(opts?: O): O {
    return {
      withSettings: true,
      withTokens: true,
      ...super.fillDefaultOptions(opts),
    };
  }
}
