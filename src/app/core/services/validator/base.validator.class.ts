import { ValidatorService } from '@e-is/ngx-material-table';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { IValidatorService } from '../../../shared/services/validator-service.class';
import { Directive } from '@angular/core';
import { AppFormUtils } from '../../form/form.utils';
import { changeCaseToUnderscore } from '../../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import { FormErrors } from '../../../shared/forms';

@Directive()
export abstract class AppValidatorService<T = any> extends ValidatorService implements IValidatorService<T> {
  protected constructor(
    protected formBuilder: UntypedFormBuilder,
    protected translate?: TranslateService
  ) {
    super();
  }

  getRowValidator(): UntypedFormGroup {
    return this.getFormGroup();
  }

  getFormGroup(data?: T): UntypedFormGroup {
    return this.formBuilder.group(this.getFormGroupConfig(data));
  }

  getFormGroupConfig(data?: T): { [key: string]: any[] } {
    return {};
  }

  getI18nFormErrors(control: AbstractControl): string[] {
    const errors = AppFormUtils.getFormErrors(control);
    return this.getI18nErrors(errors);
  }

  getI18nErrors(errors: FormErrors): string[] {
    return Object.keys(errors || {}).map((errorKey) => this.getI18nError(errorKey, errors[errorKey]));
  }

  protected getI18nError(errorKey: string, errorContent?: any): string {
    const i18nKey = 'ERROR.FIELD_' + changeCaseToUnderscore(errorKey).toUpperCase();
    const i18nMessage = this.translate.instant(i18nKey, errorContent);
    if (i18nKey !== i18nMessage) return i18nMessage;
    if (typeof errorContent === 'string') return errorContent;

    // Not translated: show error
    console.error(`[validator] Cannot translate error key '${errorKey}'. Please override getI18nError() in your validator`);

    return changeCaseToUnderscore(errorKey);
  }
}
