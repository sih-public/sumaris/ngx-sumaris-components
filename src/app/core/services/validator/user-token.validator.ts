import { Injectable } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AppValidatorService } from './base.validator.class';

import { UserToken } from '../model/token.model';
import { isEmptyArray, isNotNilOrBlank, toNumber } from '../../../shared/functions';

@Injectable({ providedIn: 'root' })
export class UserTokenValidatorService extends AppValidatorService<UserToken> {
  constructor(formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroup(data?: UserToken): UntypedFormGroup {
    return this.formBuilder.group({
      id: [toNumber(data?.id, null)],
      pubkey: [data?.pubkey || null, Validators.required],
      token: [data?.token || null],
      name: [data?.name || null, Validators.compose([Validators.required, Validators.maxLength(100)])],
      flags: [data?.flags || null],
      scopes: [data?.scopes || null, UserTokenValidatorService.notEmptyArray],
      expirationDate: [data?.expirationDate || null],
      lastUsedDate: [data?.lastUsedDate || null],
      creationDate: [data?.creationDate || null],
      updateDate: [data?.updateDate || null],
    });
  }

  updateFormGroup(formGroup: UntypedFormGroup, existingNames: string[]) {
    formGroup.controls.name.validator = Validators.compose([
      Validators.required,
      Validators.maxLength(100),
      UserTokenValidatorService.notAlreadyExists(existingNames),
    ]);
  }

  static notAlreadyExists(existingNames: string[]): ValidatorFn {
    return (control) => {
      const value = control.value;
      if (isNotNilOrBlank(value) && existingNames?.includes(value)) {
        return { notAlreadyExists: true };
      }
      return null;
    };
  }

  static notEmptyArray(control: UntypedFormControl): ValidationErrors | null {
    const value = control.value;
    if (isEmptyArray(value)) {
      return { notEmptyArray: true };
    }
    return null;
  }
}
