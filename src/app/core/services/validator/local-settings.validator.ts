import { Injectable } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { LocalSettings } from '../model/settings.model';
import { SharedValidators } from '../../../shared/validator/validators';
import { NetworkService } from '../network.service';
import { AppValidatorService } from './base.validator.class';
import { toBoolean } from '../../../shared/functions';
import { PropertyValidator } from '../../form/properties/property.validator';
import { FormFieldDefinition } from '../../../shared/form/field.model';

@Injectable({ providedIn: 'root' })
export class LocalSettingsValidatorService extends AppValidatorService<LocalSettings> {
  constructor(
    formBuilder: UntypedFormBuilder,
    private propertyValidator: PropertyValidator,
    private networkService: NetworkService
  ) {
    super(formBuilder);
  }

  getFormGroup(data?: LocalSettings, opts?: { definitions?: FormFieldDefinition[] }): UntypedFormGroup {
    return this.formBuilder.group(
      {
        accountInheritance: [toBoolean(data?.accountInheritance, true), Validators.required],
        locale: [data?.locale || null, Validators.required],
        darkMode: [toBoolean(data?.darkMode, null)],
        autoDarkMode: [toBoolean(data?.autoDarkMode, true)],
        latLongFormat: [data?.latLongFormat || null, Validators.required],
        usageMode: [data?.usageMode || 'DESK', Validators.required],
        peerUrl: [data?.peerUrl || null, Validators.required],
        properties: this.propertyValidator.getFormArray(data?.properties, opts),
      },
      {
        asyncValidators: (group: UntypedFormGroup) => this.peerAlive(group.get('peerUrl')),
      }
    );
  }

  /* -- protected methods -- */

  protected async peerAlive(peerUrlControl: AbstractControl): Promise<ValidationErrors | null> {
    if (this.networkService.online) {
      const alive = await this.networkService.checkPeerAlive(peerUrlControl.value);
      // KO: add a validation error
      if (!alive) {
        const errors: ValidationErrors = peerUrlControl.errors || {};
        errors['peerAlive'] = true;
        peerUrlControl.setErrors(errors);
        peerUrlControl.markAsTouched({ onlySelf: true });
        // Return the error (should be apply to the parent form)
        return { peerAlive: true };
      }
    }

    // OK: remove the existing error on control
    SharedValidators.clearError(peerUrlControl, 'peerAlive');
    return null;
  }
}
