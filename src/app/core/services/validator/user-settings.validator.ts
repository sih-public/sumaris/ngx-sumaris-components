import { Injectable } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AppValidatorService } from './base.validator.class';
import { UserSettings } from '../model/account.model';

@Injectable({ providedIn: 'root' })
export class UserSettingsValidatorService extends AppValidatorService<UserSettings> {
  constructor(formBuilder: UntypedFormBuilder) {
    super(formBuilder);
  }

  getFormGroup(data?: UserSettings): UntypedFormGroup {
    return this.formBuilder.group({
      id: [data?.id || null],
      updateDate: [data?.updateDate || null],
      locale: [data?.locale || null, Validators.required],
      latLongFormat: [data?.latLongFormat || null, Validators.required],
      content: [data?.content || null],
      nonce: [data?.nonce || null],
    });
  }
}
