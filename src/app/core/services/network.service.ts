import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage-angular';
import { Peer } from './model/peer.model';
import { LocalSettings } from './model/settings.model';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { ISelectPeerModalOptions, SelectPeerModal } from '../peer/select-peer.modal';
import { BehaviorSubject, firstValueFrom, Subscription, timer } from 'rxjs';
import { ConnectionType } from '@capacitor/network';
import { LocalSettingsService, SETTINGS_STORAGE_KEY } from './local-settings.service';
import { SplashScreen } from '@capacitor/splash-screen';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { equals, isNotEmptyArray, isNotNilOrBlank, sleep, toBoolean } from '../../shared/functions';
import { DOCUMENT } from '@angular/common';
import { CacheService } from 'ionic-cache';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { distinctUntilChanged, filter, first, map, mergeMap, takeUntil, tap } from 'rxjs/operators';
import { OverlayEventDetail } from '@ionic/core';
import { AppManifest, NetworkUtils, NodeInfo } from './network.utils';
import { HttpUtils } from '../../shared/http/http.utils';
import { VersionUtils } from '../../shared/version/versions';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { StartableObservableService } from '../../shared/services/startable-observable-service.class';
import { NetworkEventType } from './network.types';
import { APP_LOGGING_SERVICE } from '../../shared/logging/logging-service.class';
import { ILogger, ILoggingService } from '../../shared/logging/logger.model';
import { Capacitor } from '@capacitor/core';

const NetworkRefreshTimerPeriod = {
  MOBILE: 1000 * 60 * 10 /* every 10 min */,
  DESKTOP: 1000 * 60 * 5 /* every 5 min */,
};

export const PEER_URL_REGEXP = /^(http|https):\/\/[^ "?#@]+$/;

export const NETWORK_DEFAULT_CONNECTION_TIMEOUT = 10000; // 10s - /!\ should be high (e.g. for poor connection)

/* -- DEV only (to debug refresh timer)
NetworkRefreshTimerPeriod.MOBILE =  1000;
NetworkRefreshTimerPeriod.DESKTOP =  1000; */

@Injectable({ providedIn: 'root' })
export class NetworkService extends StartableObservableService<Peer, Peer> {
  onPeerChanges = this.startSubject.pipe(
    map((peer) => peer?.url as string),
    filter(isNotNilOrBlank),
    distinctUntilChanged<string>()
  );
  onNetworkStatusChanges = new BehaviorSubject<ConnectionType>(null);
  onResetNetworkCache = new EventEmitter(true);

  private readonly _mobile: boolean;
  private readonly _logger: ILogger;
  private _timerSubscription: Subscription;
  private readonly _timerRefreshPeriod: number;
  private readonly _timerRefreshCondition: () => boolean;
  private _deviceConnectionType: ConnectionType;
  private _forceOffline: boolean;
  private readonly _connectionTimeout: number;

  private _listeners: {
    [key: string]: ((data?: any) => Promise<void>)[];
  } = {};

  get online(): boolean {
    return this.connectionType !== 'none';
  }

  get offline(): boolean {
    return this.connectionType === 'none';
  }

  get forceOffline(): boolean {
    return this._forceOffline;
  }

  get connectionType(): ConnectionType {
    // If force offline: return 'none'
    return this._forceOffline
      ? 'none'
      : // Else, return device connection type (or unknown)
        (this.started && this._deviceConnectionType) || 'unknown';
  }

  get peer(): Peer {
    return this.dataSubject.value?.clone();
  }

  set peer(peer: Peer) {
    if (!Peer.equals(this.peer, peer)) {
      this.restart(peer);
    }
  }

  get connectionTimeout(): number {
    return this._connectionTimeout;
  }

  constructor(
    @Inject(DOCUMENT) private _document: any,
    platform: Platform,
    private modalCtrl: ModalController,
    private storage: Storage,
    private settings: LocalSettingsService,
    private cache: CacheService,
    private http: HttpClient,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_LOGGING_SERVICE) private loggingService: ILoggingService,
    @Optional() private translate: TranslateService,
    @Optional() private toastController: ToastController
  ) {
    super(platform);
    this._mobile = this.settings.mobile;
    this._connectionTimeout = environment.connectionTimeout || NETWORK_DEFAULT_CONNECTION_TIMEOUT;
    this._logger = this.loggingService?.getLogger('network');

    if (this._mobile) {
      this._timerRefreshPeriod = NetworkRefreshTimerPeriod.MOBILE;
      this._timerRefreshCondition = () => this.online; // Check only when online, and stop when offline
    } else {
      this._timerRefreshPeriod = NetworkRefreshTimerPeriod.DESKTOP;
      this._timerRefreshCondition = () => true; // Always check
    }

    this.startSubject.subscribe((peer) => this.ngOnAfterStart(peer));

    // For DEV only
    this._debug = !environment.production;
  }

  /**
   * Register to network event
   *
   * @param eventType
   * @param callback
   */
  on<T = any>(eventType: NetworkEventType, callback: (data?: T) => Promise<any>): Subscription {
    switch (eventType) {
      case 'start':
        return this.startSubject.subscribe(() => callback());

      case 'peerChanged':
        return this.onPeerChanges.subscribe(() => callback());

      case 'statusChanged':
        return this.onNetworkStatusChanges.subscribe((type) => callback(type as unknown as T));

      case 'resetCache':
        return this.onResetNetworkCache.subscribe(() => callback());

      default:
        return this.addListener(eventType, callback);
    }
  }

  async tryOnline(opts?: {
    showOfflineToast?: boolean;
    showOnlineToast?: boolean;
    showLoadingToast?: boolean;
    afterRetryDelay?: number;
    afterRetryPromise?: () => Promise<any>;
  }): Promise<boolean> {
    // If offline mode not forced, or if device says there is no connection: skip
    if (!this._forceOffline || (await NetworkUtils.getConnectionType()) === 'none') return false;

    // SHow loading toast
    const now = Date.now();
    const showLoadingToast = !opts || opts.showLoadingToast !== false;
    let loadingToast: HTMLIonToastElement;
    if (showLoadingToast) {
      await this.showToast({ message: 'NETWORK.INFO.RETRY_TO_CONNECT', duration: 10000, onWillPresent: (t) => (loadingToast = t) });
    }

    try {
      console.info('[network] Checking connection to pod...');
      const settings: LocalSettings = await this.settings.ready();

      if (!settings.peerUrl) return false; // No peer define. Skip

      const peer = Peer.parseUrl(settings.peerUrl);
      const peerInfo = await this.checkPeerAlive(peer);
      const peerAliveAndCompatible = peerInfo && (await this.checkPeerCompatible(peerInfo));
      if (peerAliveAndCompatible) {
        // Disable the offline mode
        this.setForceOffline(false);

        // Restart
        await this.restart(peer);

        // Wait a promise, before recheck
        await this.emit('beforeTryOnlineFinish', this.online);
      }
    } catch (err) {
      console.error((err && err.message) || err);
      // Continue
    }

    // Close loading toast (with a minimal display duration of 2s)
    if (showLoadingToast) {
      await sleep(2000 - (Date.now() - now));
      if (loadingToast) await loadingToast.dismiss();
    }

    // Recheck network status
    const online = this.online;

    // Display a toast to user
    if (online) {
      if (!opts || opts.showOnlineToast !== false) {
        // Display toast (without await, because not need to wait toast close event)
        this.showToast({ message: 'NETWORK.INFO.ONLINE', type: 'info' });
      }
    } else if (opts && opts.showOfflineToast === true) {
      // Display toast (without await, because not need to wait toast close event)
      return this.showOfflineToast({ showRetryButton: false });
    }

    return this.started && online;
  }

  async showOfflineToast(opts?: {
    message?: string;
    showCloseButton?: boolean;
    showRetryButton?: boolean;
    showRetrySuccessToast?: boolean;
    showRetryLoadingToast?: boolean;
    onRetrySuccess?: () => void;
  }): Promise<boolean> {
    if (this.online) return; // Skip if online

    // Toast with a retry button
    if (!opts || opts.showRetryButton !== false) {
      const toastResult = await this.showToast({
        message: (opts && opts.message) || 'ERROR.NETWORK_REQUIRED',
        type: 'warning',
        showCloseButton: true,
        duration: 100000,
        buttons: [
          // reconnect button
          {
            role: 'refresh',
            text: this.translate.instant('NETWORK.BTN_CHECK_ALIVE'),
          },
        ],
      });

      // User don't click reconnect: return
      if (!toastResult || toastResult.role !== 'refresh') return false;

      // if network state changed to online: exit here
      if (this.online) return true;

      // Try to reconnect
      const online = await this.tryOnline({
        showOfflineToast: false,
        showOnlineToast: toBoolean(opts && opts.showRetrySuccessToast, true),
        showLoadingToast: toBoolean(opts && opts.showRetryLoadingToast, true),
      });
      if (online) {
        // Call success callback (async)
        if (opts && opts.onRetrySuccess) {
          setTimeout(opts.onRetrySuccess);
        }
        return true;
      }

      opts = {
        ...opts,
        showRetryButton: false,
        showCloseButton: true,
      };
    }

    // Simple toast, without 'await', because not need to wait toast's dismiss
    this.showToast({
      message: 'ERROR.NETWORK_REQUIRED',
      type: 'error',
      ...opts,
    });

    return false;
  }

  /**
   * Check if the peer is alive
   *
   * @param peer
   * @param opts
   */
  async checkPeerAlive(peer?: string | Peer): Promise<NodeInfo> {
    peer = peer || this.peer;
    if (!peer) {
      const settings: LocalSettings = await this.settings.ready();
      if (!settings.peerUrl) return undefined; // No peer define. Skip
      peer = Peer.parseUrl(settings.peerUrl);
    }

    return this.getNodeInfo(peer, { nocache: true, timeout: this._connectionTimeout });
  }

  async checkPeerCompatible(peerInfo: NodeInfo, opts?: { showToast?: boolean }): Promise<boolean> {
    if (!peerInfo) return false; // Peer cannot be reached
    if (!this.environment.peerMinVersion) return true; // Skip compatibility check

    // Check the min pod version, defined by the app
    const isCompatible = peerInfo.softwareVersion && VersionUtils.isCompatible(this.environment.peerMinVersion, peerInfo.softwareVersion);

    // Display toast, if not compatible
    if (!isCompatible && (!opts || opts.showToast !== false)) {
      await this.showToast({
        type: 'error',
        message: 'NETWORK.ERROR.NOT_COMPATIBLE_PEER',
        messageParams: {
          version: this.environment.peerMinVersion,
        },
        showCloseButton: true,
      });
    }
    return isCompatible;
  }

  async getNodeInfo(peer?: string | Peer, opts?: { nocache?: boolean; timeout?: number }): Promise<NodeInfo> {
    const path = this.computePeerPath(peer, '/api/node/info');
    this._logger?.debug('getNodeInfo', `Getting '${path}' ...`);

    try {
      const data: NodeInfo = await this.get(path, opts);
      this._logger?.debug('getNodeInfo', `Response of '${path}':\n${JSON.stringify(data)}`);
      return data;
    } catch (err) {
      console.debug(`[network] Error while getting '${path}': ${(err && err.message) || err}`, err);
      this._logger?.error('getNodeInfo', `Error while getting '${path}': ${err?.message || ''}`);
      return undefined;
    }
  }

  async getAppManifest(peer?: string | Peer, opts?: { nocache?: boolean }): Promise<AppManifest | undefined> {
    try {
      let path = this.computePeerPath(peer, 'manifest.json');
      if (opts?.nocache) {
        path += '?t=' + Date.now();
      }
      return await this.get(path, opts);
    } catch (err) {
      if (this.environment.production)
        console.error("[network] Cannot load file 'manifest.json'. Please make sure webserver config allow CORS access", err);
      else console.error("[network] Cannot load file 'manifest.json'.", err);
      // Continue
    }
  }

  async getAppVersion(peer?: string | Peer, opts = { nocache: true }): Promise<string | undefined> {
    try {
      let path = this.computePeerPath(peer, 'version.appup');
      if (opts.nocache) {
        path += '?t=' + Date.now();
      }
      const appVersion: string = await this.getText(path, opts);
      if (isNotNilOrBlank(appVersion)) return appVersion;
    } catch (err) {
      if (this.environment.production)
        console.error("[network] Cannot load file 'version.appup'. Please make sure webserver config allow CORS access");
      else console.error("[network] Cannot load file 'version.appup'.", err);
      // Continue
    }

    // Try loading manifest.json
    const manifest = await this.getAppManifest(peer, opts);
    return manifest?.version;
  }

  computePeerPath(peer: string | Peer | undefined, path: string): string | undefined {
    peer = peer || this.peer;
    if (!peer) return undefined;

    let peerUrl = peer instanceof Peer ? peer.url : (peer as string);

    // Remove trailing slash
    if (peerUrl.endsWith('/')) {
      peerUrl = peerUrl.substring(0, peerUrl.length - 1);
    }

    // Add first path
    if (!path.startsWith('/')) {
      path = '/' + path;
    }

    // Concat peer URL and path
    return peerUrl + path;
  }

  /**
   * Allow to force offline mode
   */
  setForceOffline(
    value?: boolean,
    opts?: {
      showToast?: boolean; // Display a toast, when offline ?
    }
  ) {
    value = toBoolean(value, true);
    if (this._forceOffline !== value) {
      const previousConnectionType = this.connectionType;
      this._forceOffline = value;
      const currentConnectionType = this.connectionType;

      if (previousConnectionType !== currentConnectionType) {
        console.info(`[network] Connection changed to {${currentConnectionType}}`);
        this.onNetworkStatusChanges.next(currentConnectionType);

        // Offline mode: alert the user
        if (currentConnectionType === 'none' && (!opts || opts.showToast !== false)) {
          this.showToast({ message: 'NETWORK.INFO.OFFLINE_HELP', type: 'warning' });
        }
      }
    }
  }

  async showSelectPeerModal(opts?: Partial<ISelectPeerModalOptions>): Promise<Peer | undefined> {
    opts = opts || {};

    const peers = await this.getDefaultPeers();
    const modal = await this.modalCtrl.create({
      component: SelectPeerModal,
      componentProps: <ISelectPeerModalOptions>{
        defaultPeers: peers,
        ...opts,
      },
      keyboardClose: true,
      showBackdrop: true,
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();

    return (data && (data as Peer)) || undefined;
  }

  async clearCache(opts?: { emitEvent?: boolean }): Promise<void> {
    const now = this._debug && Date.now();

    console.info('[network] Clearing all caches...');
    return this.cache
      .clearAll()
      .then(() => {
        // Emit event
        if ((!opts || opts.emitEvent !== false) && this.onResetNetworkCache.observed) {
          this.onResetNetworkCache.emit();

          // Wait observers clean their caches, if needed
          return sleep(500);
        }
      })
      .then(() => {
        if (this._debug) console.debug(`[network] All cache cleared, in ${Date.now() - now}ms`);
      });
  }

  /* -- protected functions -- */

  protected async ngOnStart(peer?: Peer): Promise<Peer> {
    console.info('[network] Starting network...');

    // Check if we can use Network plugin
    console.debug(`[network] Capacitor Network plugin is ${Capacitor.isPluginAvailable('Network') ? 'available' : 'not available'}`);

    // Restoring local settings
    peer = peer || (await this.restoreLocally());

    if (!peer) {
      // Make sure to hide the splashscreen, before open the modal
      await SplashScreen.hide();

      // No peer in settings: ask user to choose
      while (!peer) {
        console.debug('[network] No peer defined. Asking user to choose a peer.');
        peer = await this.showSelectPeerModal({ allowSelectDownPeer: false, title: 'NETWORK.PEER.SELECT_MODAL.STARTUP_TITLE', canCancel: false });
      }
    } else if (this.online) {
      // Check if alive. If not, force offline mode
      const alive = await this.checkPeerAlive(peer);
      if (!alive) {
        // Peer not alive, but should be at the same URL, retrying each 1s
        if (this.environment.sameUrlPeer) {
          const retryMs = this._connectionTimeout > 0 ? this._connectionTimeout : 1000;
          await firstValueFrom(
            timer(retryMs, retryMs).pipe(
              takeUntil(this.stopSubject),
              tap(() => console.warn(`[network-service] Waiting peer be be alive, at {${peer.url}} ...`)),
              mergeMap(() => this.checkPeerAlive(peer)),
              filter((alive) => !!alive),
              first()
            )
          );
        } else {
          // Continue, in offline mode
          this.setForceOffline(true);
        }
      }
    }

    console.info(`[network] Starting service [OK] {peer: '${peer.url}', online: ${this.online}}`);

    return peer;
  }

  protected async ngOnAfterStart(peer: Peer) {
    // Wait settings starts, then save peer in settings
    await this.settings.apply({ peerUrl: peer.url });

    // Listen for device network changes
    if (NetworkUtils.isPluginAvailable()) {
      const connectionType = await NetworkUtils.getConnectionType();
      this.onDeviceConnectionChanged(connectionType);

      this.registerSubscription(
        NetworkUtils.statusChanges().subscribe((status) => this.onDeviceConnectionChanged(status?.connectionType || 'unknown'))
      );
    } else {
      this.onDeviceConnectionChanged('unknown');
    }

    // Start the refresh timer
    this.startRefreshTimer();
  }

  protected ngOnStop() {
    // Stop timer, if we cannot refresh anymore
    if (this._timerRefreshCondition() === false) {
      this.stopRefreshTimer();
    }
  }

  /**
   * Try to restore peer from the local storage
   */
  protected async restoreLocally(): Promise<Peer | undefined> {
    // Force use predefined peer if provided
    if (this.environment.peer) {
      return Peer.fromObject(this.environment.peer);
    }

    // Restore from storage
    let settings = await this.storage.get(SETTINGS_STORAGE_KEY);
    settings = typeof settings === 'string' ? JSON.parse(settings) : settings;
    if (settings && settings.peerUrl) {
      console.debug(`[network] Use peer {${settings.peerUrl}} (found in the local storage)`);
      return Peer.parseUrl(settings.peerUrl);
    }

    // Else, use default peer in env, if exists
    if (this.environment.defaultPeer) {
      return Peer.fromObject(this.environment.defaultPeer);
    }

    // Else, if App is hosted, try the website as a peer
    const location = this._document?.location;
    if (location?.protocol?.startsWith('http')) {
      const hostname = this._document.location.host;
      const detectedPeer = Peer.parseUrl(`${this._document.location.protocol}${hostname}${this.environment.baseUrl}`);

      if (this.environment.sameUrlPeer || (await this.checkPeerAlive(detectedPeer))) {
        return detectedPeer;
      }
    }

    return undefined;
  }

  /**
   * Stop to network state
   *
   * @protected
   */
  protected stopRefreshTimer() {
    if (this._timerSubscription) {
      this._timerSubscription.unsubscribe();
      this._timerSubscription = undefined;
    }
  }

  /**
   * Refresh the network state
   *
   * @protected
   */
  protected startRefreshTimer() {
    if (this._timerSubscription) return; // Already running: skip

    console.info(`[network] Starting refresh timer, every ${this._timerRefreshPeriod}ms...`);

    let lastInfo: NodeInfo;
    this._timerSubscription = timer(this._timerRefreshPeriod, this._timerRefreshPeriod)
      .pipe(
        // Skip some timer event (see constructor)
        filter(this._timerRefreshCondition),

        // Checkin if peer alive
        tap(() => console.debug('[network] Checking connection to pod...')),
        mergeMap(() => this.checkPeerAlive(this.peer)),

        // Filter to keep only changes
        filter((info) => !equals(info, lastInfo)),
        tap((info) => (lastInfo = info)),

        // Check compatibility
        mergeMap((info) => this.checkPeerCompatible(info, { showToast: true }))
      )
      .subscribe((alive) => {
        if (alive && this.offline) {
          this.setForceOffline(false);

          // Restart the service (to force re auth)
          this.restart();
        } else if (!alive && this.online) {
          this.setForceOffline(true);

          // Stop the service
          this.stop();
        }
      });

    this._timerSubscription.add(() => console.debug('[network] Refresh timer stopped'));
  }

  protected async getText(
    path: string,
    opts?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      responseType?: 'text';
      nocache?: boolean;
    }
  ): Promise<string> {
    return HttpUtils.getText(this.http, this.getUri(path), opts);
  }

  protected get<T>(
    path: string,
    opts?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      responseType?: 'json';
      nocache?: boolean;
      timeout?: number;
    }
  ): Promise<T> {
    return HttpUtils.getJson<T>(this.http, this.getUri(path), opts);
  }

  /* -- Protected methods -- */

  protected getUri(path: string): string {
    let uri = path;

    // If path is not an URI: prepend with peer URL
    if (!uri.startsWith('http://') && !uri.startsWith('https://')) {
      let peerUrl = this.peer && this.peer.url;

      // Remove trailing slash
      if (peerUrl.endsWith('/')) {
        peerUrl = peerUrl.substring(0, peerUrl.length - 1);
      }

      // Add first path
      if (!path.startsWith('/')) {
        path = '/' + path;
      }

      // Create the URI: concat peer URL and path
      uri = peerUrl + path;
    }
    return uri;
  }
  protected onDeviceConnectionChanged(connectionType?: ConnectionType) {
    if (connectionType !== this._deviceConnectionType) {
      this._deviceConnectionType = connectionType;

      // If NOT already forced as offline, emit event
      if (!this._forceOffline) {
        console.info(`[network] Connection changed to {${this._deviceConnectionType}}`);
        this.onNetworkStatusChanges.next(this._deviceConnectionType);

        // Change to offline
        if (this._deviceConnectionType === 'none') {
          if (this._mobile) {
            // Force offline mode
            this._forceOffline = true;

            // Alert the user
            this.showToast({ message: 'NETWORK.INFO.OFFLINE_HELP', type: 'warning' });
          } else {
            // Alert the user
            this.showToast({ message: 'NETWORK.INFO.OFFLINE', type: 'warning' });
          }

          // Stop the network service
          this.stop();
        }
      }
    }
  }

  /**
   * Get default peers, from environment
   */
  protected async getDefaultPeers(): Promise<Peer[]> {
    const peers = (this.environment.defaultPeers || []).map(Peer.fromObject);
    return Promise.resolve(peers);
  }

  protected showToast<T = any>(opts: ShowToastOptions): Promise<OverlayEventDetail<T>> {
    return Toasts.show(this.toastController, this.translate, opts);
  }

  protected addListener<T = any>(name: NetworkEventType, callback: (data?: T) => Promise<void>): Subscription {
    this._listeners[name] = this._listeners[name] || [];
    this._listeners[name].push(callback);

    // When unsubscribe, remove from the listener
    return new Subscription(() => {
      const index = this._listeners[name].indexOf(callback);
      if (index !== -1) {
        this._listeners[name].splice(index, 1);
      }
    });
  }

  protected async emit<T = any>(name: NetworkEventType, data?: T) {
    const hooks = this._listeners[name];
    if (isNotEmptyArray(hooks)) {
      console.info(`[network-service] Trigger ${name} hook: Executing ${hooks.length} callbacks...`);

      return Promise.all(
        hooks.map((callback) => {
          const promise = callback(data);
          if (!promise) return;
          return promise.catch((err) => console.error('Error while executing hook ' + name, err));
        })
      );
    }
  }
}
