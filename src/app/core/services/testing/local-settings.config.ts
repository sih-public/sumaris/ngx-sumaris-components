// Add dynamic option
import { FormFieldDefinition } from '../../../shared/form/field.model';
import { PersonFilter } from '../../../admin/services/filter/person.filter';
import { StatusIds } from '../model/model.enum';

// This test options should be visible in the settings page
export const TEST_LOCAL_SETTINGS_OPTIONS = {
  DUMMY_OPTION_ENTITY: <FormFieldDefinition>{
    key: 'sumaris.option.dummyEntity',
    label: 'ACCOUNT.SETTINGS.DUMMY_ENTITY',
    type: 'entity',
    minProfile: 'USER', // Should be login
    autocomplete: {
      showAllOnFocus: true,
      attributes: ['firstName', 'lastName', 'id'],
      filter: <PersonFilter>{
        statusIds: [StatusIds.ENABLE, StatusIds.TEMPORARY],
      },
    },
  },

  DUMMY_OPTION_ENTITIES: <FormFieldDefinition>{
    key: 'sumaris.option.dummyEntities',
    label: 'ACCOUNT.SETTINGS.DUMMY_ENTITIES',
    type: 'entities',
    minProfile: 'USER', // Should be login
    autocomplete: {
      showAllOnFocus: true,
      attributes: ['firstName', 'lastName', 'id'],
      filter: <PersonFilter>{
        statusIds: [StatusIds.ENABLE, StatusIds.TEMPORARY],
      },
    },
  },
};
