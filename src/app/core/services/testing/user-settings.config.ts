import { FormFieldDefinition } from '../../../shared/form/field.model';

// This test options should be visible in the account page
export const TEST_USER_SETTINGS_OPTIONS = Object.freeze({
  DUMMY_OPTION_1: <FormFieldDefinition>{
    key: 'sumaris.option.dummy1',
    label: 'ACCOUNT.SETTINGS.DUMMY_1',
    type: 'boolean',
    defaultValue: false,
  },
  DUMMY_OPTION_2: <FormFieldDefinition>{
    key: 'sumaris.option.dummy2',
    label: 'ACCOUNT.SETTINGS.DUMMY_2',
    type: 'boolean',
    defaultValue: false,
  },
  DUMMY_OPTION_3: <FormFieldDefinition>{
    key: 'sumaris.option.dummy3',
    label: 'ACCOUNT.SETTINGS.DUMMY_3',
    type: 'boolean',
    defaultValue: false,
  },
  DUMMY_OPTION_4: <FormFieldDefinition>{
    key: 'sumaris.option.dummy4',
    label: 'ACCOUNT.SETTINGS.DUMMY_4',
    type: 'boolean',
    defaultValue: false,
  },
  DUMMY_OPTION_5: <FormFieldDefinition>{
    key: 'sumaris.option.dummy5',
    label: 'ACCOUNT.SETTINGS.DUMMY_5',
    type: 'boolean',
    defaultValue: false,
  },

  DUMMY_OPTION_6: <FormFieldDefinition>{
    key: 'sumaris.option.dummy6',
    label: 'ACCOUNT.SETTINGS.DUMMY_6',
    type: 'enum',
    values: [
      {
        key: 'a',
        value: 'Value A',
      },
      {
        key: 'b',
        value: 'Value B',
      },
      {
        key: 'c',
        value: 'Value C',
      },
    ],
    defaultValue: null,
  },

  DUMMY_OPTION_7: <FormFieldDefinition>{
    key: 'sumaris.option.dummy7',
    label: 'ACCOUNT.SETTINGS.DUMMY_7',
    type: 'enums',
    values: [
      {
        key: 'a',
        value: 'Value A',
      },
      {
        key: 'b',
        value: 'Value B',
      },
      {
        key: 'c',
        value: 'Value C',
      },
    ],
    defaultValue: null,
  },
});
