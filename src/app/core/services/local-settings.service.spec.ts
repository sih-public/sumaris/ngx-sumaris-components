import { TestBed } from '@angular/core/testing';
import { LocalSettingsService } from './local-settings.service';
import { TranslateModule } from '@ngx-translate/core';
import { IonicStorageModule } from '@ionic/storage-angular';
import { ENVIRONMENT } from '../../../environments/environment.class';
import { environment } from '../../../environments/environment.test';
import { StorageService } from '../../shared/storage/storage.service';
import { IonicModule } from '@ionic/angular';

describe('LocalSettingsService', () => {
  let service: LocalSettingsService;
  let storage: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), IonicModule, IonicStorageModule.forRoot()],
      providers: [
        StorageService,
        LocalSettingsService,
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
      ],
      teardown: { destroyAfterEach: false },
    });
    service = TestBed.inject(LocalSettingsService);
    storage = TestBed.inject(StorageService);
  });

  it('should be created', async () => {
    expect(service).toBeTruthy();
    await storage.ready();
    const settings = await service.ready();
    expect(settings).toBeDefined();
    expect(settings.locale).toBeUndefined();
    expect(settings.latLongFormat).toBe('DDMM');
  });
});
