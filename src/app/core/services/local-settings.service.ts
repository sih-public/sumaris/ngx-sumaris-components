import { EventEmitter, Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { isOnFieldMode, LocalSettings, OfflineFeature, UsageMode } from './model/settings.model';
import { Peer } from './model/peer.model';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage-angular';

import { equals, getPropertyByPath, isEmptyArray, isNil, isNotEmptyArray, isNotNil, isNotNilOrBlank, undefinedIfNull } from '../../shared/functions';
import { merge, Subject } from 'rxjs';
import { Platform } from '@ionic/angular';
import { FormFieldDefinition, FormFieldDefinitionMap } from '../../shared/form/field.model';
import { Moment } from 'moment';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { LatLongPattern } from '../../shared/material/latlong/latlong.utils';
import { ENVIRONMENT, Environment } from '../../../environments/environment.class';
import { environment } from '../../../environments/environment';
import { DateUtils, fromDateISOString } from '../../shared/dates';
import { HistoryPageReference } from './model/history.model';
import { StartableService } from '../../shared/services/startable-service.class';
import { isCapacitor, isMobile } from '../../shared/platforms';

export const SETTINGS_STORAGE_KEY = 'settings';
export const SETTINGS_TRANSIENT_PROPERTIES = ['mobile', 'touchUi' /*deprecated*/];

// fixme: this constant points to static environment
const DEFAULT_SETTINGS: LocalSettings = {
  accountInheritance: true,
  locale: environment.defaultLocale,
  latLongFormat: environment.defaultLatLongFormat || 'DDMM',
  pageHistoryMaxSize: 3,
};

export interface LocalSettingsOptions {
  serializeAsString?: boolean; // false by default
  options: FormFieldDefinitionMap;
}
export declare interface AddToPageHistoryOptions {
  removePathQueryParams?: boolean;
  removeTitleSmallTag?: boolean;
  emitEvent?: boolean;
}

export const APP_LOCAL_SETTINGS = new InjectionToken<Partial<LocalSettings>>('DefaultLocalSettings');
export const APP_LOCAL_SETTINGS_OPTIONS = new InjectionToken<LocalSettingsOptions>('LocalSettingsOptions');

@Injectable({
  providedIn: 'root',
  //deps: [APP_LOCAL_SETTINGS, APP_LOCAL_SETTINGS_OPTIONS]
})
export class LocalSettingsService extends StartableService<LocalSettings> {
  onChange = new Subject<LocalSettings>();
  darkMode$ = merge(this.onChange, this.startSubject).pipe(
    distinctUntilChanged(),
    map((data) => data?.darkMode || false)
  );

  private readonly _optionDefs: FormFieldDefinition[];
  private readonly _serializeAsString: boolean;
  private _$persist: EventEmitter<any>;

  get settings(): LocalSettings {
    return this._data || this.defaultSettings;
  }

  get locale(): string {
    return (this._data && this._data.locale) || this.translate.currentLang || this.translate.defaultLang;
  }

  get latLongFormat(): LatLongPattern {
    return (this._data && this._data.latLongFormat) || 'DDMM';
  }

  get usageMode(): UsageMode {
    return isNotNil(this._data.usageMode) ? this._data.usageMode : isCapacitor(window) || isMobile(window) ? 'FIELD' : 'DESK';
  }

  get mobile(): boolean {
    return isNotNil(this._data.mobile) ? this._data.mobile : isMobile(window);
  }

  set mobile(value: boolean) {
    this._data.mobile = value;
  }

  get pageHistory(): HistoryPageReference[] {
    return (this._data && this._data.pageHistory) || [];
  }

  get allowDarkMode(): boolean {
    return this.environment?.allowDarkMode === true;
  }

  get isDarkMode(): boolean {
    return this.allowDarkMode && this._data?.darkMode === true;
  }

  constructor(
    platform: Platform,
    private translate: TranslateService,
    private storage: Storage,
    @Inject(ENVIRONMENT) protected readonly environment: Environment,
    @Optional() @Inject(APP_LOCAL_SETTINGS) private readonly defaultSettings: LocalSettings,
    @Optional() @Inject(APP_LOCAL_SETTINGS_OPTIONS) options: LocalSettingsOptions
  ) {
    super(platform);
    this.defaultSettings = { ...DEFAULT_SETTINGS, ...this.defaultSettings };

    this._optionDefs = Object.values(options?.options || {});
    this._serializeAsString = options?.serializeAsString || false;

    this.resetData();

    this._debug = true; // !environment.production;
    if (this._debug) console.debug('[settings] Creating service');
  }

  ngOnStart(): Promise<LocalSettings> {
    console.info('[settings] Starting service...');

    // Default value
    this._data.mobile = this.mobile;
    this._data.usageMode = this.usageMode;

    // Restoring local settings
    return this.restoreLocally();
  }

  isUsageMode(mode: UsageMode): boolean {
    return this.usageMode === mode;
  }

  isOnFieldMode(value?: UsageMode): boolean {
    return isOnFieldMode(value || this.usageMode);
  }

  async restoreLocally(): Promise<LocalSettings | undefined> {
    let data = this._data || <LocalSettings>{};

    // Restore from storage
    const settings = await this.storage.get(SETTINGS_STORAGE_KEY);

    // Restore local settings (or keep old settings)
    if (isNotNil(settings)) {
      console.info('[settings] Restoring previous settings...');

      const restoredData = typeof settings === 'string' ? JSON.parse(settings) : settings;

      // Avoid to override transient properties
      SETTINGS_TRANSIENT_PROPERTIES.forEach((transientKey) => {
        delete restoredData[transientKey];
      });

      // Merge into existing data
      data = Object.assign(data, restoredData);

      // Fix known attribute
      if (data.pageHistory && !Array.isArray(data.pageHistory)) data.pageHistory = [];
    }

    // Initialize darkMode, if not set
    if (this.environment?.allowDarkMode !== true) {
      delete data.darkMode;
      delete data.autoDarkMode;
    } else if (isNil(data.darkMode) || data.autoDarkMode !== false) {
      console.info('[settings] Auto-detect system dark mode...');
      data.darkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
      data.autoDarkMode = true;
    }

    // Emit event
    this.onChange.next(data);

    return data;
  }

  setProperty<T = string>(keyOrDef: string | FormFieldDefinition, value: T, opts?: { emitEvent?: boolean; immediate?: boolean }) {
    if (!this._data || !keyOrDef) return;
    if (typeof keyOrDef === 'object') {
      this.setProperty(keyOrDef.key, value);
      return;
    }
    if (this._debug) console.debug(`[settings] Setting property ${keyOrDef} to:`, value);

    this._data.properties = this._data.properties || {};
    this._data.properties[keyOrDef] = isNil(value) ? undefined : value.toString();

    // Update local settings
    if (!opts || opts.emitEvent !== false) {
      this.persistLocally(opts?.immediate);
    }
  }

  getProperty(keyOrDef: string | FormFieldDefinition, defaultValue?: any): any {
    if (typeof keyOrDef === 'object') {
      return this.getProperty(keyOrDef.key, isNotNil(defaultValue) ? defaultValue : keyOrDef.defaultValue);
    }
    const value = this._data && this._data.properties && this._data.properties[keyOrDef];
    return isNotNil(value) ? value : undefinedIfNull(defaultValue);
  }

  getPropertyAsBoolean(definition: FormFieldDefinition, defaultValue?: boolean): boolean {
    const value = this.getProperty(definition, defaultValue);
    return isNotNil(value) ? value && value !== 'false' : undefined;
  }

  getPropertyAsInt(definition: FormFieldDefinition, defaultValue?: number): number {
    const value = this.getProperty(definition, defaultValue);
    return isNotNil(value) ? parseInt(value) : undefined;
  }

  getPropertyAsNumbers(definition: FormFieldDefinition, defaultValue?: number[]): number[] {
    const value = this.getProperty(definition, defaultValue);
    if (typeof value === 'string') return value.split(',').map(parseFloat) || undefined;
    return isNotNil(value) ? [parseFloat(value)] : undefined;
  }

  getPropertyAsStrings(definition: FormFieldDefinition, defaultValue?: string[]): string[] {
    const value = this.getProperty(definition, defaultValue);
    return (value && value.split(',')) || undefined;
  }

  getPropertyAsObjects(definition: FormFieldDefinition, defaultValue?: any[]): any[] {
    let value = this.getProperty(definition, defaultValue);
    // Parse array
    if (value && typeof value === 'string') {
      value = JSON.parse(value);
    }
    if (value && !Array.isArray(value)) {
      return [value];
    }
    return value;
  }

  async apply(settings: Partial<LocalSettings>, opts?: { emitEvent?: boolean; persistImmediate?: boolean }) {
    if (!this.started) await this.ready();

    const data = {
      ...this._data,
      ...settings,
    };

    if (equals(data, this._data)) return; // Skip if no changes

    this._data = data;

    // Save locally
    if (opts && opts.persistImmediate) {
      await this.persistLocally(true);
    } else {
      this.persistLocally(); // No AWAIT
    }

    // Emit event
    if (!opts || opts.emitEvent !== false) {
      this.onChange.next(this._data);
    }
  }

  async applyProperty(key: keyof LocalSettings, value: any, opts?: { emitEvent?: boolean; persistImmediate?: boolean }) {
    const changes = {};
    changes[key] = value;
    await this.apply(changes, opts);
  }

  getPageSettings<T = any>(pageId: string, propertyName?: string): T {
    if (!this._data || !this._data.pages) return undefined;
    const key = pageId.replace(/[/]/g, '__');
    if (isNotNilOrBlank(propertyName)) {
      return getPropertyByPath(this._data.pages, key + '.' + propertyName);
    }
    return this._data.pages[key];
  }

  async savePageSetting(pageId: string, value: any, propertyName?: string) {
    this._data = this._data || this.defaultSettings;
    this._data.pages = this._data.pages || {};

    const key = pageId.replace(/[/]/g, '__');
    if (propertyName) {
      this._data.pages[key] = this._data.pages[key] || {};
      this._data.pages[key][propertyName] = value;
    } else {
      this._data.pages[key] = value;
    }

    // Update local settings
    this.persistLocally();
  }

  getOfflineFeature<F = any>(featureName: string): OfflineFeature<F> {
    if (!this._data || !this._data.offlineFeatures || isEmptyArray(this._data.offlineFeatures)) return undefined;
    if (!featureName) throw Error("Missing 'featureName' argument");

    featureName = featureName.toLowerCase();
    const featurePrefix = featureName + '#';
    const feature = this._data.offlineFeatures.find((f) => {
      if (typeof f === 'string') return f.toLowerCase().startsWith(featurePrefix);
      if (f && typeof f === 'object' && f.name) return f.name === featureName;
      return false;
    });
    if (!feature) return; // Not found
    if (typeof feature === 'string') {
      const lastSyncDate = feature.substring(featurePrefix.length);
      console.debug(`[${featureName}] Last synchronization date: ${lastSyncDate}`);
      return {
        name: featureName,
        lastSyncDate,
      };
    }
    return feature;
  }

  getOfflineFeatureLastSyncDate(featureName: string): Moment {
    const feature = this.getOfflineFeature(featureName);
    return feature && fromDateISOString(feature.lastSyncDate);
  }

  hasOfflineFeature(featureName?: string): boolean {
    if (featureName) return isNotNil(this.getOfflineFeature(featureName));
    return this._data && isNotEmptyArray(this._data.offlineFeatures);
  }

  saveOfflineFeature(feature: OfflineFeature) {
    this._data = this._data || this.defaultSettings;
    this._data.offlineFeatures = this._data.offlineFeatures || [];

    feature.name = feature.name.toLowerCase();

    const featurePrefix = feature.name + '#';
    const existingIndex = this._data.offlineFeatures.findIndex((f) => {
      if (typeof f === 'string') return f.toLowerCase().startsWith(featurePrefix);
      if (f && typeof f === 'object' && f.name) return f.name === feature.name;
      return false;
    });
    if (existingIndex !== -1) {
      this._data.offlineFeatures[existingIndex] = feature;
    } else {
      this._data.offlineFeatures.push(feature);
    }

    // Update local settings
    this.persistLocally();
  }

  markOfflineFeatureAsSync(featureName: string) {
    let feature = this.getOfflineFeature(featureName);

    if (!feature) {
      feature = <OfflineFeature>{
        name: featureName.toLowerCase(),
        lastSyncDate: DateUtils.moment().toISOString(),
      };
    } else {
      feature.lastSyncDate = DateUtils.moment().toISOString();
    }

    this.saveOfflineFeature(feature);
  }

  removeOfflineFeatures() {
    if (this._data && this._data.offlineFeatures) {
      this._data.offlineFeatures = [];

      // Update local settings
      this.persistLocally();
    }
  }

  getFieldDisplayAttributes(fieldName: string, defaultAttributes?: string[]): string[] {
    const value = this._data && this._data.properties && this._data.properties[`sumaris.field.${fieldName}.attributes`];
    // Nothing found in settings: return defaults
    if (!value) return defaultAttributes || ['label', 'name'];

    return value.split(',');
  }

  getPageFieldDefaultValue(pageId: string, fieldName: string): any {
    return this.getPageSettings(pageId, `field.${fieldName}.defaultValue`);
  }

  get optionDefs(): FormFieldDefinition[] {
    return this._optionDefs;
  }

  registerOption(def: FormFieldDefinition, opts?: { replaceIfExists?: boolean }) {
    const index = this._optionDefs.findIndex((f) => f.key === def.key);
    if (index !== -1) {
      if (opts?.replaceIfExists !== true) {
        throw new Error(`Additional additional property {${def.key}} already define.`);
      }
      // Replace existing
      if (this._debug) console.debug(`[settings] Updating additional property {${def.key}}`, def);
      this._optionDefs[index] = def;
    } else {
      if (this._debug) console.debug(`[settings] Adding additional property {${def.key}}`, def);
      this._optionDefs.push(def);
    }
  }

  registerOptions(defs: FormFieldDefinition[]) {
    (defs || []).forEach((def) => this.registerOption(def));
  }

  async addToPageHistory(
    page: HistoryPageReference,
    opts?: AddToPageHistoryOptions,
    pageHistory?: HistoryPageReference[] // used for recursive call to children
  ) {
    // If not inside recursive call: fill page history defaults
    if (!pageHistory) this.fillPageHistoryDefaults(page, opts);

    pageHistory = Array.isArray(pageHistory) ? pageHistory : this._data.pageHistory;

    const index = pageHistory.findIndex(
      (p) =>
        // same path
        p.path === page.path ||
        // or sub-path
        page.path.startsWith(p.path + '/')
    );

    // New page: insert it
    if (index === -1) {
      //if (this._debug)
      console.debug('[settings] Adding page to history: ', page);

      // Prepend to list
      pageHistory.splice(0, 0, page);
    } else {
      const existingPage = pageHistory[index];

      // Same path: replace existing page
      if (pageHistory[index].path === page.path) {
        //if (this._debug)
        console.debug('[settings] Updating existing page in history: ', page);
        pageHistory.splice(index, 1);

        // Copy exiting children
        page.children = existingPage.children || [];

        // Prepend to list
        pageHistory.splice(0, 0, page);
      }

      // Not same path (should be a parent page)
      else {
        // Create parent's children array, if not exists
        existingPage.children = existingPage.children || [];

        // Update the parent time
        existingPage.time = page.time;

        // Add page as parent's children (recursive call)
        await this.addToPageHistory(page, opts, existingPage.children);
      }
    }

    // Save locally (only if not a recursive execution)
    if (pageHistory === this._data.pageHistory) {
      // If max has been reached, remove old pages
      if (pageHistory.length > this._data.pageHistoryMaxSize) {
        const removedPages = pageHistory.splice(this._data.pageHistoryMaxSize, pageHistory.length - this._data.pageHistoryMaxSize);
        console.debug('[settings] Pages removed from history: ', removedPages);
      }

      // Apply new value
      await this.applyProperty('pageHistory', pageHistory);
    }
  }

  async removePageHistory(
    path: string,
    opts?: { emitEvent?: boolean; persistImmediate?: boolean },
    pageHistory: HistoryPageReference[] = undefined, // used for recursive call to children)
    depth = 0
  ) {
    const root = depth === 0;
    pageHistory = pageHistory || this._data.pageHistory?.slice() /*clone, to force apply to save */ || [];

    const index = pageHistory.findIndex((p) => p.path === path);
    let found = index !== -1;
    if (found) {
      console.debug('[settings] Remove page history: ', path);
      // Remove value
      pageHistory.splice(index, 1);
    } else {
      // Search path on children (stop when found)
      found =
        pageHistory
          .map((p) => p.children)
          .filter(isNotEmptyArray)
          .findIndex((children) => this.removePageHistory(path, opts, children, (depth || 0) + 1)) !== -1;
    }

    // Save locally (only if root level)
    if (found && root) {
      // Apply changes
      await this.applyProperty('pageHistory', pageHistory);
    }

    return found;
  }

  async clearPageHistory() {
    // Reset all page history
    await this.applyProperty('pageHistory', [], { persistImmediate: true });
  }

  /* -- Protected methods -- */

  protected async ngOnStop(): Promise<void> {
    this.resetData();
  }

  private resetData() {
    this._data = { ...this._data, ...this.defaultSettings };

    this._data.locale = this.translate.currentLang || this.translate.defaultLang;
    this._data.mobile = undefined;
    this._data.usageMode = undefined;
    this._data.pageHistory = [];
    this._data.properties = {};

    const defaultPeer = this.environment.defaultPeer && Peer.fromObject(this.environment.defaultPeer);
    this._data.peerUrl = (defaultPeer && defaultPeer.url) || undefined;

    if (this.started) this.onChange.next(this._data);
  }

  private persistLocally(immediate?: boolean): Promise<any> {
    // Execute immediate
    if (immediate) {
      if (!this._data) {
        console.debug('[settings] Removing local settings from storage');
        return this.storage.remove(SETTINGS_STORAGE_KEY);
      } else {
        console.debug('[settings] Store local settings', this._data);
        if (this._serializeAsString) {
          // Fix serialization error, in Sumaris-App
          return this.storage.set(SETTINGS_STORAGE_KEY, JSON.stringify(this._data));
        }
        return this.storage.set(SETTINGS_STORAGE_KEY, this._data);
      }
    }

    // Execute with delay
    else {
      // Create the event emitter
      if (!this._$persist) {
        this._$persist = new EventEmitter<any>(true);
        this._$persist
          .pipe(
            debounceTime(2000), // add a delay of 2s
            filter(() => this.started)
          )
          .subscribe(() => this.persistLocally(true));
      }

      this._$persist.emit();
    }
  }

  private fillPageHistoryDefaults(
    page: HistoryPageReference,
    opts?: {
      removePathQueryParams?: boolean;
      removeTitleSmallTag?: boolean;
    }
  ): HistoryPageReference {
    if (!page || !page.title || !page.path) throw Error("Missing required argument 'page', 'page.path' or 'page.title'");

    // Set time
    page.time = page.time || DateUtils.moment();

    // Clean title
    if (!opts || opts.removeTitleSmallTag !== false) {
      // Remove <small> HTML tags
      const smallTagIndex = page.title.indexOf('</small>');
      if (smallTagIndex !== -1) {
        page.title = page.title.substring(smallTagIndex + '</small>'.length);
      }
      // Remove class="..." HTML attribute
      page.title = page.title.replace(/\s*class=['"][^'"]+['"]'/g, '');
    }

    // Remove query params
    if (!opts || opts.removePathQueryParams !== false) {
      page.path = page.path.replace(/[?].*$/, '');
    }

    return page;
  }
}
