import { Injectable } from '@angular/core';

import { scryptEncode } from '@polkadot/util-crypto/scrypt/encode';
import { ScryptParams } from '@polkadot/util-crypto/scrypt/types';
import {
  base58Decode,
  base64Decode,
  base64Encode,
  cryptoWaitReady,
  ed25519PairFromSeed,
  ed25519Sign,
  ed25519Verify,
  sha256AsU8a,
  sha512AsU8a,
} from '@polkadot/util-crypto';

import { bytesToHex as toHex } from '@noble/hashes/utils';
import { Keypair } from '@polkadot/util-crypto/types';
import { stringToU8a } from '@polkadot/util';

export const ED25519_SEED_LENGTH = 32; // Length of a ED25519 seed

export const SCRYPT_PARAMS = {
  SIMPLE: <ScryptParams>{
    N: 2048,
    r: 8,
    p: 1,
  },
  DEFAULT: <ScryptParams>{
    N: 4096,
    r: 16,
    p: 1,
  },
  SECURE: <ScryptParams>{
    N: 16384,
    r: 32,
    p: 2,
  },
  HARDEST: <ScryptParams>{
    N: 65536,
    r: 32,
    p: 4,
  },
  EXTREME: <ScryptParams>{
    N: 262144,
    r: 64,
    p: 8,
  },
};

export { Keypair };

@Injectable({ providedIn: 'root' })
export class CryptoService {
  private static _ready = false;

  private static get started() {
    return this._ready;
  }

  private static async ready(): Promise<void> {
    if (this._ready) return; // OK

    await cryptoWaitReady();
    this._ready = true;
  }

  /**
   * Generate a sign keypair, using scrypt, from a salt and a password
   *
   * @param salt
   * @param password
   * @param scryptParams
   */
  static async scryptKeypair(salt: string, password: string, scryptParams?: ScryptParams): Promise<Keypair> {
    if (!this.started) await this.ready();
    const saltU8a = stringToU8a(salt);
    const result = scryptEncode(password, saltU8a, scryptParams || SCRYPT_PARAMS.DEFAULT);
    const seed = result.password.slice(0, ED25519_SEED_LENGTH);
    return ed25519PairFromSeed(seed);
  }

  /**
   * Sign a message, from a key pair
   */
  static async sign(message: string, keypair: Keypair): Promise<string> {
    if (!this.started) await this.ready();

    const m = stringToU8a(message);
    const sig = ed25519Sign(m, keypair);
    return base64Encode(sig);
  }

  /**
   * Sign a message, from a key pair
   */
  static async verify(message: string, signature: string, publicKey: string): Promise<boolean> {
    if (!this.started) await this.ready();

    const m = stringToU8a(message);
    const sig = base64Decode(signature);
    const pub = base58Decode(publicKey);
    return ed25519Verify(m, sig, pub);
  }

  /**
   * Hash (using SHA256) a message, as Hex
   */
  static sha256(message: string): string {
    const bytes = sha256AsU8a(message);
    return toHex(bytes);
  }

  /**
   * Hash (using SHA512) a message, as Hex
   */
  static sha512(message: string): string {
    const bytes = sha512AsU8a(message);
    return toHex(bytes);
  }

  static encodeBase64(message: string): string {
    const m = stringToU8a(message);
    return base64Encode(m);
  }
}
