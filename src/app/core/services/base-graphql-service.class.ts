import { GraphqlService, MutateQueryOptions, WatchQueryOptions } from '../graphql/graphql.service';
import { Page } from '../../shared/services/entity-service.class';
import { Observable } from 'rxjs';
import { FetchResult } from '@apollo/client/link/core';
import { EntityUtils } from './model/entity.model';
import { ApolloCache, InternalRefetchQueriesInclude } from '@apollo/client/core';
import { isNotEmptyArray, isNotNil, toBoolean } from '../../shared/functions';
import { environment } from '../../../environments/environment';
import { Directive, Optional } from '@angular/core';
import { QueryRef } from 'apollo-angular';
import { PureQueryOptions } from '@apollo/client/core/types';
import { DocumentNode } from 'graphql';
import { StartableService } from '../../shared/services/startable-service.class';
import { TypedDocumentNode } from '@graphql-typed-document-node/core';
import { CryptoService } from './crypto.service';
import { EmptyObject } from '../graphql/graphql.utils';
import { filter } from 'rxjs/operators';

export interface QueryVariables<F = any> extends Partial<Page> {
  filter?: F;
  [key: string]: any;
}

export interface MutateQueryWithCacheUpdateOptions<T = any, V = EmptyObject> extends MutateQueryOptions<T, V> {
  cacheInsert?: {
    query: any;
    fetchData: (variables: V, mutationResult: FetchResult<T>) => any;
  }[];

  cacheRemove?: {
    query: any;
    fetchIds: (variables: V) => number | number[];
  }[];
}

export interface MutableWatchQueryOptions<D, T = any, V = EmptyObject> extends WatchQueryOptions<V> {
  queryName: string;
  arrayFieldName: keyof D;
  totalFieldName?: keyof D;
  insertFilterFn?: (data: T) => boolean;
  sortFn?: (a: T, b: T) => number;
}

export interface MutableWatchQueryDescription<TData, TEntityClass = any, TVariables = EmptyObject> extends PureQueryOptions<TVariables, TData> {
  id?: string;
  query: DocumentNode | TypedDocumentNode<TData, TVariables>;
  queryRef: QueryRef<TData, TVariables>;
  variables: TVariables;
  arrayFieldName: keyof TData;
  totalFieldName?: keyof TData;
  insertFilterFn?: (data: TEntityClass) => boolean;
  sortFn?: (a: TEntityClass, b: TEntityClass) => number;
  counter: number;
}

export interface FindMutableWatchQueriesOptions {
  query?: DocumentNode;
  queries?: DocumentNode[];
  queryName?: string;
  queryNames?: string[];
}

export class BaseGraphqlServiceOptions {
  production?: boolean;
}

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export abstract class BaseGraphqlService<T = any, F = any, ID = any, S = void> extends StartableService<S> {
  protected _logPrefix: string;
  protected _mutableWatchQueries: MutableWatchQueryDescription<any>[] = [];

  // Max updated queries, for this entity.
  // - Can be override by subclasses (in constructor)
  // - Use value -1 for no max size
  // - Default value to 3, because it usually enough (e.g. OperationService and LandingService need at least 2)
  protected _mutableWatchQueriesMaxCount = 3;

  protected constructor(
    protected graphql: GraphqlService,
    @Optional() options?: BaseGraphqlServiceOptions
  ) {
    super(graphql);

    // for DEV only
    this._debug = toBoolean(options && !options.production, !environment.production);
    this._logPrefix = `[base-graphql-service] `;
  }

  mutableWatchQuery<D, V = EmptyObject>(opts: MutableWatchQueryOptions<D, T, V>): Observable<D> {
    opts.fetchPolicy = opts.fetchPolicy || this.graphql.defaultFetchPolicy;

    // Standard query (no cache)
    if (opts.fetchPolicy === 'no-cache') {
      return this.graphql.watchQuery(opts);
    }

    // No arrayFieldName
    if (!opts.arrayFieldName) {
      if (this._debug) console.warn(this._logPrefix + "̀Missing 'opts.arrayFieldName' : unable to create a mutable watch query");
      return this.graphql.watchQuery(opts);
    }

    // Create the query id
    const queryId = this.computeMutableWatchQueryId(opts);
    const exactMatchQueries = this._mutableWatchQueries.filter((q) => q.id === queryId);

    let mutableQuery: MutableWatchQueryDescription<D, T, V>;
    let queryRef: QueryRef<D, V>;

    if (exactMatchQueries.length === 1) {
      mutableQuery = exactMatchQueries[0] as MutableWatchQueryDescription<D, T, V>;
      mutableQuery.counter += 1;
      queryRef = mutableQuery.queryRef;
      if (this._debug) console.debug(this._logPrefix + `Find same mutable watch query (same variables) {${queryId}}. Skip register`);

      // Refetch if need data from network
      if (opts.fetchPolicy === 'network-only' || opts.fetchPolicy === 'cache-and-network') {
        console.warn(this._logPrefix + ' Mutable query already exists, and fetchPolicy=' + opts.fetchPolicy + '. Force refetch query...');
        queryRef.refetch(mutableQuery.variables);
      }
      //if (mutableQuery.counter > 3) {
      //  console.warn(this._debugPrefix + 'TODO: clean previous queries with name: ' + queryName);
      //}
    } else {
      queryRef = this.graphql.watchQueryRef<D, V>(opts);
      this.registerNewMutableWatchQuery({
        id: queryId,
        query: opts.query,
        queryRef,
        variables: opts.variables,
        arrayFieldName: opts.arrayFieldName,
        totalFieldName: opts.totalFieldName,
        insertFilterFn: opts.insertFilterFn,
        sortFn: opts.sortFn,
        counter: 1,
      });
    }

    return this.graphql.queryRefValuesChanges(queryRef, opts).pipe(
      // FIXME: find why sometimes data is an object, and not and array
      filter((res) => Array.isArray(res[opts.arrayFieldName]))
    );
  }

  insertIntoMutableCachedQueries(
    cache: ApolloCache<any>,
    opts: FindMutableWatchQueriesOptions & {
      data: T[] | T;
    }
  ) {
    const queries = this.findMutableWatchQueries(opts);
    if (!queries.length) return;

    queries.forEach((query) => {
      if (Array.isArray(opts.data)) {
        // Filter values, if a filter function exists
        const data = query.insertFilterFn ? opts.data.filter((d) => query.insertFilterFn(d)) : opts.data;
        if (isNotEmptyArray(data)) {
          if (this._debug) console.debug(`[base-data-service] Inserting data into watching query: `, query.id);
          this.graphql.addManyToQueryCache(cache, {
            query: query.query,
            variables: query.variables,
            arrayFieldName: query.arrayFieldName as string,
            totalFieldName: query.totalFieldName as string,
            sortFn: query.sortFn,
            data,
          });
        }
      }
      // Filter value, if a filter function exists
      else if (!query.insertFilterFn || query.insertFilterFn(opts.data)) {
        if (this._debug) console.debug(`[base-data-service] Inserting data into watching query: `, query.id);
        this.graphql.insertIntoQueryCache(cache, {
          query: query.query,
          variables: query.variables,
          arrayFieldName: query.arrayFieldName as string,
          totalFieldName: query.totalFieldName as string,
          sortFn: query.sortFn,
          data: opts.data,
        });
      }
    });
  }

  removeFromMutableCachedQueriesByIds(
    cache: ApolloCache<any>,
    opts: FindMutableWatchQueriesOptions & {
      ids: ID | ID[];
    }
  ): number {
    const queries = this.findMutableWatchQueries(opts);
    if (!queries.length) return;

    console.debug(`[base-data-service] Removing data from watching queries: `, queries);
    return (
      queries
        .map((query) => {
          if (Array.isArray(opts.ids)) {
            return this.graphql.removeFromCachedQueryByIds(cache, {
              query: query.query,
              variables: query.variables,
              arrayFieldName: query.arrayFieldName as string,
              totalFieldName: query.totalFieldName as string,
              ids: opts.ids,
            });
          } else {
            return this.graphql.removeFromCachedQueryById(cache, {
              query: query.query,
              variables: query.variables,
              arrayFieldName: query.arrayFieldName as string,
              totalFieldName: query.totalFieldName as string,
              ids: opts.ids,
            })
              ? 1
              : 0;
          }
        })
        // Sum
        .reduce((res, count) => res + count, 0)
    );
  }

  async refetchMutableWatchQueries(opts: FindMutableWatchQueriesOptions & { variables?: any }): Promise<void> {
    // Retrieve queries to refetch
    const queries = this.findMutableWatchQueries(opts);

    // Skip if nothing to refetch
    if (!queries.length) return;

    try {
      await Promise.all(
        queries.map((query) => {
          if (this._debug) console.debug(this._logPrefix + `Refetching mutable watch query {${query.id}}`);
          return query.queryRef.refetch({ ...query.variables, ...opts.variables });
        })
      );
    } catch (err) {
      console.error(this._logPrefix + 'Error while refetching mutable watch queries', err);
    }
  }

  /* -- protected methods -- */

  protected computeMutableWatchQueryId<D, T, V = EmptyObject>(opts: MutableWatchQueryOptions<D, T, V>) {
    const queryName = opts.queryName || CryptoService.sha256(JSON.stringify(opts.query)).substring(0, 8);
    const variablesKey = (opts.variables && CryptoService.sha256(JSON.stringify(opts.variables)).substring(0, 8)) || '';
    return [queryName, opts.arrayFieldName, variablesKey].join('|');
  }

  protected findMutableWatchQueries(opts: FindMutableWatchQueriesOptions): MutableWatchQueryDescription<any>[] {
    // Search by queryName
    if (opts.queryName) {
      return this._mutableWatchQueries.filter((q) => q.id.startsWith(opts.queryName + '|'));
    }
    if (opts.queryNames) {
      return (
        opts.queryNames
          .map((queryName) => this._mutableWatchQueries.filter((q) => q.id.startsWith(queryName + '|')))
          // flatMap
          .reduce((res, array) => res.concat(...array), [])
      );
    }

    // Search by query
    if (opts.query) {
      return this._mutableWatchQueries.filter((q) => q.query === opts.query);
    }
    if (opts.queries) {
      return (
        opts.queries
          .filter(isNotNil)
          .map((query) => this._mutableWatchQueries.filter((q) => q.query === query))
          // flatMap
          .reduce((res, array) => res.concat(...array), [])
      );
    }
    throw Error('Invalid options: only one property must be set');
  }

  protected registerNewMutableWatchQuery(mutableQuery: MutableWatchQueryDescription<any>) {
    if (this._debug) console.debug(this._logPrefix + `Register new mutable watch query {${mutableQuery.id}}`);

    // If exceed the max size of mutable queries: remove some
    if (this._mutableWatchQueriesMaxCount > 0 && this._mutableWatchQueries.length >= this._mutableWatchQueriesMaxCount) {
      const removedWatchQueries = this._mutableWatchQueries.splice(0, 1 + this._mutableWatchQueries.length - this._mutableWatchQueriesMaxCount);

      // Warn, as it shouldn't be happen often, when max is correctly set
      console.warn(
        this._logPrefix + `Removing older mutable watching queries (stack exceed max of ${this._mutableWatchQueriesMaxCount}): `,
        removedWatchQueries.map((q) => q.id)
      );
    }

    // Define the sort function (to be used in insertIntoMutableCachedQuery())
    if (!mutableQuery.sortFn && mutableQuery.variables && mutableQuery.variables.sortBy) {
      mutableQuery.sortFn =
        mutableQuery.variables &&
        mutableQuery.variables.sortBy &&
        EntityUtils.sortComparator(mutableQuery.variables.sortBy, mutableQuery.variables.sortDirection || 'asc');
    }

    // Add the new mutable query to array
    this._mutableWatchQueries.push(mutableQuery);
  }

  protected findRefetchQueries(opts: FindMutableWatchQueriesOptions): InternalRefetchQueriesInclude {
    return (
      this.findMutableWatchQueries(opts)
        // Workaround, to make refetch queries works: transform into a simple {query, variables}
        .map(({ query, variables }) => ({ query, variables }))
    );
  }

  protected ngOnStart(): Promise<S> {
    return Promise.resolve(null as S);
  }
}
