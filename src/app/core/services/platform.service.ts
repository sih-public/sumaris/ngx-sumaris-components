import { Inject, Injectable, Optional } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { NetworkService } from './network.service';
import { AuthTokenType } from './network.types';
import { Platforms } from '@ionic/core';
import { LocalSettingsService } from './local-settings.service';
import { CacheService } from 'ionic-cache';
import { AudioProvider } from '../../shared/audio/audio';
import { Platform as CdkPlatform } from '@angular/cdk/platform';

import { isNilOrBlank, isNotNil, isNotNilOrBlank, toBoolean } from '../../shared/functions';
import { EntitiesStorage } from './storage/entities-storage.service';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { TranslateService } from '@ngx-translate/core';
import { AccountService } from './account.service';
import { BehaviorSubject, from, Observable, of, Subscription, timer } from 'rxjs';
import { catchError, skip, tap } from 'rxjs/operators';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { ConfigService } from './config.service';
import { CORE_CONFIG_OPTIONS } from './config/core.config';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader/ngx';
import { StartableService } from '../../shared/services/startable-service.class';
import { isMobile, isWindows } from '../../shared/platforms';
import { VersionUtils } from '../../shared/version/versions';
import { FilesUtils, MimeTypes } from '../../shared/file/file.utils';
import { UriUtils } from '../../shared/file/uri.utils';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar } from '@capacitor/status-bar';
import { Browser, OpenOptions } from '@capacitor/browser';
import { Keyboard } from '@capacitor/keyboard';
import { CapacitorPlugins } from '../../shared/capacitor/plugins';
import { locale as momentLocale } from 'moment';
import { ILogger, ILoggingService } from '../../shared/logging/logger.model';
import { APP_LOGGING_SERVICE } from '../../shared/logging/logging-service.class';
import { Clipboard as CdkClipboard } from '@angular/cdk/clipboard';
import { Clipboard } from '@capacitor/clipboard';
import { WriteOptions } from '@capacitor/clipboard/dist/esm/definitions';

export const AndroidOsEnvironment = Object.freeze({
  DIRECTORY_DOWNLOADS: 'Download',
  DIRECTORY_DOWNLOADS_OLD: 'Downloads', // Old name, for older Android version
  // TODO: find value for this. See https://developer.android.com/reference/android/os/Environment#getExternalStorageState(java.io.File)
  // DIRECTORY_MUSIC,
  // DIRECTORY_PODCASTS,
  // DIRECTORY_RINGTONES,
  // DIRECTORY_ALARMS,
  // DIRECTORY_NOTIFICATIONS,
  // DIRECTORY_PICTURES,
  // DIRECTORY_MOVIES,
  // DIRECTORY_DCIM,
  // DIRECTORY_DOCUMENTS,
  // DIRECTORY_AUDIOBOOKS,
});

@Injectable({ providedIn: 'root' })
export class PlatformService extends StartableService {
  private _logger: ILogger;
  private _mobile: boolean;
  private _desktop: boolean;
  private _cordova: boolean;
  private _capacitor: boolean;
  private _android: boolean;
  private _ios: boolean;

  busySubject = new BehaviorSubject<boolean>(false);

  constructor(
    private ionicPlatform: Platform,
    private cdkPlatform: CdkPlatform,
    private cdkClipboard: CdkClipboard,
    private toastController: ToastController,
    private translate: TranslateService,
    private dateAdapter: MomentDateAdapter,
    private entitiesStorage: EntitiesStorage,
    private settings: LocalSettingsService,
    private networkService: NetworkService,
    private accountService: AccountService,
    private configService: ConfigService,
    private cache: CacheService,
    private audioProvider: AudioProvider,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_LOGGING_SERVICE) loggingService: ILoggingService,
    @Optional() private downloader: Downloader
  ) {
    super(ionicPlatform);

    this._logger = loggingService?.getLogger('platform');
    this._debug = !environment.production;
    if (this._debug) console.debug('[platform] Creating service');

    if (this._logger) {
      this._logger.debug('ctor', 'Creating service');
      ionicPlatform.pause.subscribe(() => {
        this._logger.info('ctor', 'Pause app');
      });
      ionicPlatform.resume.subscribe(() => {
        this._logger.info('ctor', 'Resume app');
      });
    }
  }
  private _downloadingJobs: Map<string, Observable<string>> = new Map();

  private checkAppVersionTimer: Subscription;

  is(platformName: Platforms): boolean {
    switch (platformName) {
      case 'mobile':
        // Exclude Windows touch screens - see sumaris-app issues #323 and #346
        return isMobile(window);
      case 'mobileweb':
        // Exclude Windows touch screens - see sumaris-app issues #323 and #346
        return this.ionicPlatform.is(platformName) && !isWindows(window);
      default:
        return this.ionicPlatform.is(platformName);
    }
  }

  get mobile(): boolean {
    return isNotNil(this._mobile) ? this._mobile : this.is('mobile');
  }

  /**
   * Say if opened has been opened inside an Android or iOs App.
   * This is used to known if there is cordova features
   */
  isCapacitor(): boolean {
    return this._capacitor;
  }

  /**
   * Say if opened has been opened inside an Android or iOs App.
   * This is used to known if there is cordova features
   */
  isCordova(): boolean {
    return this._cordova;
  }

  isAndroidCordova(): boolean {
    return (this._android && this._cordova) || false;
  }

  isIOSCordova(): boolean {
    return (this._ios && this._cordova) || false;
  }

  isAndroid(): boolean {
    return this._android || false;
  }

  isIOS(): boolean {
    return this._ios || false;
  }

  isMacOSDesktop(): boolean {
    return this._desktop && this.isCordova();
  }

  /**
   * Is a native App (and NOT a mobile web)
   */
  isApp(): boolean {
    return this.isAndroidCordova() || this.isIOSCordova();
  }

  isWeb(): boolean {
    return !this.isApp();
  }

  isMobileWeb(): boolean {
    return this.mobile && this.isWeb();
  }

  get canDownload(): boolean {
    return this.downloader && this.isAndroidCordova();
  }

  get canOpenFile(): boolean {
    return false; // TODO: fixme using @capacitor/filesystem ?
  }

  width(): number {
    return this.ionicPlatform.width();
  }

  height(): number {
    return this.ionicPlatform.height();
  }

  protected async ngOnStart(): Promise<any> {
    const now = Date.now();
    this.accountService.tokenType = undefined;
    console.info('[platform] Starting platform...');
    this._logger?.info('ngOnStart', 'Starting platform...');

    try {
      this._mobile = this.is('mobile');
      this._desktop = this.is('desktop');
      this._cordova = this.is('cordova');
      this._capacitor = this.is('capacitor');
      this._android = this.is('android');
      this._ios = this.is('ios');

      const logMessage = `Detected {mobile: ${this._mobile}, desktop: ${this._desktop}, cordova: ${this._cordova}, capacitor: ${this._capacitor}, android: ${this._android}, ios: ${this._ios}}`;
      console.info('[platform] ' + logMessage);
      this._logger?.info('ngOnStart', logMessage);

      // Configure theme
      await this.configureTheme(this._mobile);

      // Configure Capacitor plugins
      await this.configureCapacitorPlugins();

      // Configure translation
      this.configureTranslate();

      // Configure cache
      this.configureCache();

      // Start root services
      await Promise.all([this.entitiesStorage.ready(), this.settings.ready(), this.networkService.ready(), this.audioProvider.ready()]);

      // Check if web app has update, and reload page if need
      if (this.isWeb() && this.environment.production) {
        await this.checkAppVersion({ canReload: true });
      }

      // Update cache configuration when network changed
      this.registerSubscription(
        this.networkService.onNetworkStatusChanges
          .pipe(
            skip(1) // Already done once, so we skip the first event
          )
          .subscribe((type) => this.configureCache(type !== 'none'))
      );

      console.info(
        `[platform] Starting platform [OK] {mobile: ${this._mobile}, capacitor: ${this._capacitor}, web: ${this.isWeb()}, downloader: ${this.canDownload}} in ${Date.now() - now}ms`
      );

      // Pass auth token
      this.registerSubscription(
        this.configService.config.subscribe((config) => {
          if (!config) return; // Skip

          // Update authentication type
          const tokenType: AuthTokenType = config.getProperty(CORE_CONFIG_OPTIONS.AUTH_TOKEN_TYPE) as AuthTokenType;
          if (isNotNilOrBlank(tokenType) && this.accountService.tokenType !== tokenType) {
            console.info(`[platform] Using auth token type {${tokenType}}`);
            this.accountService.tokenType = tokenType;
          }

          // Enable auth API using tokens
          const authApiEnabled = config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.AUTH_API_TOKEN_ENABLED);
          if (isNotNil(authApiEnabled) && authApiEnabled !== this.accountService.apiTokenEnabled) {
            console.info(`[platform] Using auth API tokens ? ${authApiEnabled}}`);
            this.accountService.apiTokenEnabled = authApiEnabled;
          }
        })
      );

      if (this._mobile || this.isMacOSDesktop()) {
        // Hide the splashscreen (if mobile) - after 1s - and play start sound
        setTimeout(async () => {
          await SplashScreen.hide();

          // Play startup sound
          if (this._mobile) {
            await this.audioProvider.playStartupSound();
          }
        }, 1000);
      }

      // Check if new version, every 1 min
      const checkIntervalMs = (this.environment.checkAppVersionIntervalInSeconds || 0) * 1000;
      if (this.isWeb() && this.environment.production && checkIntervalMs > 0) {
        this.checkAppVersionTimer?.unsubscribe();
        const subscription = timer(checkIntervalMs, checkIntervalMs).subscribe((_) =>
          this.checkAppVersion({ silent: true, canReload: false /*do NOT auto reload, when app is started*/ })
        );
        this.checkAppVersionTimer = subscription;
        this.registerSubscription(subscription);
        subscription.add(() => this.unregisterSubscription(subscription));
      }
    } catch (err) {
      // Manage startup error
      return this.onStartupError(err);
    }
  }

  /**
   * Used to show a backdrop when platform is busy (e.g. from the app.component.ts)
   */
  markAsBusy() {
    if (!this.busySubject.value) {
      this.busySubject.next(true);
    }
  }

  markAsNotBusy() {
    if (this.busySubject.value) {
      this.busySubject.next(false);
    }
  }

  async open(url: string, opts?: Omit<OpenOptions, 'url'>): Promise<void> {
    console.info(`[platform] Opening link: ${url}`);
    if (this._mobile && this._capacitor) {
      await Browser.open({ url, ...opts });
    } else {
      window.open(url, opts?.windowName);
    }
  }

  getDownloadingJob(uri: string): Observable<string> {
    return this._downloadingJobs.get(uri);
  }

  download(request: { uri: string; filename?: string } & Partial<DownloadRequest>): Observable<string | undefined> {
    if (!request || !request.uri) throw new Error("Missing argument 'request' or 'request.uri'");

    if (this.canDownload) {
      // Check if not already downloading file
      let job$ = this._downloadingJobs.get(request.uri);
      if (job$) return job$;

      // Compute subPath (final filename)
      let subPath =
        request.destinationInExternalPublicDir?.subPath ||
        request.destinationInExternalFilesDir?.subPath ||
        request.filename ||
        UriUtils.getFilename(request.uri) ||
        request.title ||
        'download';

      subPath = subPath
        .toLowerCase()
        // Remove spaces
        .replace(/\s+/g, '-');

      // Force APK to finish with '.apk'
      if (request.mimeType === MimeTypes.ANDROID_APK && !subPath.toLowerCase().endsWith('.apk')) {
        subPath += '.apk';
      }

      const downloadRequest = <DownloadRequest>{
        visibleInDownloadsUi: true,
        notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
        ...request,
        filename: undefined, // Force unset
        destinationInExternalPublicDir: !request.destinationInExternalFilesDir
          ? {
              dirType: AndroidOsEnvironment.DIRECTORY_DOWNLOADS,
              ...request.destinationInExternalPublicDir,
              subPath,
            }
          : undefined,
        destinationInExternalFilesDir: request.destinationInExternalFilesDir
          ? {
              dirType: AndroidOsEnvironment.DIRECTORY_DOWNLOADS,
              ...request.destinationInExternalFilesDir,
              subPath,
            }
          : undefined,
      };

      // Start download
      console.debug('[platform] Downloading, using request: ' + JSON.stringify(downloadRequest));
      job$ = from(this.downloader.download(downloadRequest));

      // Remember this request uri
      this._downloadingJobs.set(downloadRequest.uri, job$);
      return job$.pipe(
        tap((location) => {
          console.info('[platform] File successfully downloaded at:' + location);
          // Forget job
          this._downloadingJobs.delete(downloadRequest.uri);
          return location;
        }),
        catchError((err) => {
          console.error('[platform] Unable to download: ' + ((err && err.message) || err));
          // Forget the request
          this._downloadingJobs.delete(downloadRequest.uri);

          // Retry with another location - issue in older Android version
          if (downloadRequest.destinationInExternalPublicDir?.dirType === AndroidOsEnvironment.DIRECTORY_DOWNLOADS) {
            downloadRequest.destinationInExternalPublicDir.dirType = AndroidOsEnvironment.DIRECTORY_DOWNLOADS_OLD;
            return this.download(downloadRequest);
          }
          throw err;
        })
      );
    }

    // Fallback (if not Android and Capacitor): opening the URI
    // TODO: find a way to download under iOS (see https://www.c-sharpcorner.com/article/how-to-download-a-file-using-file-transfer-plugin-in-ionic-3/)
    else {
      FilesUtils.downloadUri(request.uri, request.filename);
      return of(undefined);
    }
  }

  async copyToClipboard(data: string | WriteOptions, opts?: ShowToastOptions) {
    const value = typeof data === 'string' ? data : data.string;
    let copied = true;
    if (this.isAndroidCordova()) {
      const writeOptions = typeof data === 'string' ? { string: value } : data;
      await Clipboard.write(writeOptions);
    } else {
      copied = this.cdkClipboard.copy(value);
    }

    if (copied) {
      // Show toast
      if (opts && isNotNilOrBlank(opts.message)) {
        await Toasts.show(this.toastController, this.translate, {
          type: 'info',
          ...opts,
        });
      }
    } else {
      console.error('[platform] Unable to copy to clipboard: ' + data);
    }
  }

  toggleDarkTheme(enable: boolean) {
    if (enable && !document.documentElement.classList.contains('dark')) {
      console.debug('[platform] Enable dark mode...');
    }
    document.documentElement.classList.toggle('dark', enable);
  }

  toggleHighContrast(enable: boolean) {
    if (enable && !document.documentElement.classList.contains('high-contrast')) {
      console.debug('[platform] Enable high contrast...');
    }
    document.documentElement.classList.toggle('high-contrast', enable);
  }

  showToast(opts: ShowToastOptions): Promise<HTMLIonToastElement> {
    if (!this.toastController) throw new Error("Missing toastController in component's constructor");
    return new Promise((resolve) =>
      Toasts.show(this.toastController, this.translate, {
        ...opts,
        onWillPresent: (t) => resolve(t),
      })
    );
  }

  async closeToast(id?: string) {
    return this.toastController?.dismiss(null, null, id);
  }

  /* -- protected methods -- */

  protected async configureTheme(mobile: boolean, win: any = window) {
    // Listen for changes to settings dark mode
    if (this.environment.allowDarkMode) {
      this.registerSubscription(this.settings.darkMode$.subscribe((enable) => this.toggleDarkTheme(enable)));
    }

    // Workaround, for MS Windows touch screen (detected as mobile, but forced as desktop)
    if (
      !mobile &&
      (win.document.documentElement?.classList.contains('plt-mobile') || win.document.documentElement.classList?.contains('plt-mobileweb'))
    ) {
      console.warn('[platform] Force class list to: <html class="plt-desktop">. Workaround for Windows touch screen.');
      win.document.documentElement.classList.remove('plt-mobile', 'plt-mobileweb');
      win.document.documentElement.classList.add('plt-desktop');
    }
  }

  protected async configureCapacitorPlugins() {
    if (!this._capacitor) return; // Skip

    console.info('[platform] Configuring Capacitor plugins...');

    let pluginName: string;
    try {
      pluginName = CapacitorPlugins.StatusBar;
      if (this.isApp() && this.isAndroid()) {
        await StatusBar.setOverlaysWebView({ overlay: false });
      }

      // Hide accessory bar (iOS only)
      if (this.isApp() && this.isIOS()) {
        pluginName = CapacitorPlugins.Keyboard;
        await Keyboard.setAccessoryBarVisible({ isVisible: false });
      }
    } catch (err) {
      const logMessage = `Error while configuring ${pluginName} plugin: ${err.message || err}\n${err?.originalStack || JSON.stringify(err)}`;
      console.error('[platform] ' + logMessage);
      this._logger?.error('configureCapacitorPlugins', logMessage);
    }
  }

  protected configureTranslate() {
    console.info('[platform] Configuring i18n ...');

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang(this.environment.defaultLocale);
    // apply locale immediately
    this.applyLocale(this.environment.defaultLocale);

    // When locale changes, apply to date adapter
    this.registerSubscription(
      this.translate.onLangChange.subscribe((event) => {
        if (event && event.lang) {
          this.applyLocale(event.lang);
        }
      })
    );

    this.registerSubscription(
      this.settings.onChange.subscribe((data) => {
        if (data) {
          // Apply locale
          if (data.locale) {
            if (data.locale !== this.translate.currentLang) {
              this.translate.use(data.locale);
            }
          } else {
            // Applying default, when settings has no locale property (hotfix - ludovic.pecquot@e-is.pro - 14/11/2021 - since v1.20.42)
            console.warn('[platform] No locale found in settings: applying defaultLocale: ', this.environment.defaultLocale);
            this.translate.use(this.environment.defaultLocale);
          }
        }
      })
    );

    // Use account's settings, (like locale), when account inheritance enabled
    this.registerSubscription(
      this.accountService.onLogin.subscribe((account) => {
        if (account.settings && this.settings.settings.accountInheritance) {
          const accountSettings = account.settings.asLocalSettings();
          this.settings.apply(accountSettings);
        }
      })
    );
  }

  protected applyLocale(locale: string) {
    // force 'en' as 'en_GB'
    if (locale === 'en') {
      locale = 'en_GB';
    }

    // config moment lib
    try {
      momentLocale(locale);
      console.debug('[platform] Use locale {' + locale + '}');
    } catch (err) {
      // If error, fallback to en
      momentLocale('en');
      console.warn('[platform] Unknown locale for moment lib. Using default [en]');
    }

    // Config date adapter
    this.dateAdapter.setLocale(momentLocale());
  }

  protected configureCache(online?: boolean) {
    online = toBoolean(online, this.cache.isOnline());
    const cacheTTL = online ? 3600 /* 1h */ : 3600 * 24 * 30; /* 1 month */
    console.info(`[platform] Configuring cache [OK] {online: ${online}, timeToLive: ${cacheTTL / 3600}h, offlineInvalidate: false}`);
    this.cache.setDefaultTTL(cacheTTL);
    this.cache.setOfflineInvalidate(false); // Do not invalidate cache when offline
  }

  protected async checkAppVersion(opts?: { canReload?: boolean; silent?: boolean }) {
    if (this.networkService.offline || !this.isWeb()) return; // Skip

    const silent = opts?.silent === true;
    const production = this.environment.production;
    const canAutoReload = !opts || opts.canReload !== false;

    if (!silent) console.info('[platform] Checking remote app version...');

    const actualVersion = this.environment.version;
    if (isNilOrBlank(actualVersion)) {
      console.error("[platform] Missing required value for 'environment.version'. Cannot check remote app version.");
      this.checkAppVersionTimer?.unsubscribe(); // Stop timer if exists
      return; // Skip
    }

    const remoteVersion = await this.networkService.getAppVersion();
    if (isNilOrBlank(remoteVersion)) {
      if (production && !silent) console.error('[platform] Cannot load remote app version. Skipping version check');
      this.checkAppVersionTimer?.unsubscribe(); // Stop timer if exists
      return;
    }

    if (VersionUtils.isCompatible(remoteVersion, actualVersion)) {
      if (!silent) console.info(`[platform] Checking remote app version [OK] {remote: '${remoteVersion}', actual: '${actualVersion}'}`);
      return;
    }

    // If newer version exists
    let reloadPath = location.href.replace(/[&?]version=[^&]*/gi, ''); // Remove old version in query params
    reloadPath += reloadPath.indexOf('?') === -1 ? '?' : '&'; // Add query param separator
    reloadPath += 'version=' + remoteVersion; // Add version (to avoid a reload infinite loop)

    // Already try to reload, but version still bad
    if (reloadPath === location.href) {
      if (canAutoReload)
        console.error(
          `[platform] Reloaded page failed, because version still mismatch! Please check version.appup file (expected app version: ${actualVersion})`
        );
      // Stop timer, to avoid infinite loop
      this.checkAppVersionTimer?.unsubscribe();
      return;
    }

    console.warn(`[platform] More recent version detected (remote: ${remoteVersion}, actual: ${actualVersion}`);

    // Auto reload
    if (canAutoReload) {
      console.info('[platform] Will reloading at ' + reloadPath);
      location.href = reloadPath;
      // App stop
    }
    // Ask user to reload
    else {
      // Stop timer, to avoid infinite loop
      this.checkAppVersionTimer?.unsubscribe();

      // Display a reload toast
      await this.showToast({
        message: 'CONFIRM.RELOAD_APP',
        messageParams: { version: remoteVersion, name: this.environment.name },
        type: 'info',
        duration: -1,
        showCloseButton: true,
        buttons: [
          // Reload button
          {
            text: this.translate.instant('COMMON.BTN_UPDATE'),
            side: 'end',
            handler: () => {
              location.href = reloadPath;
              return true;
            },
          },
        ],
      });
    }
  }

  protected async onStartupError(err) {
    console.error('[platform] Failed starting the platform! ' + JSON.stringify(err));
    let message = (err && err.message) || err;
    const detailsMessage = err && ((err.details && err.details.message) || err.details);
    // Set specific error when Firefox is in private mode or history never remembered (see Mantis Ifremer #56441)
    if (err.code === 11 && this.cdkPlatform.FIREFOX) {
      message = 'ERROR.FIREFOX_NO_STORAGE';
    }
    if (this.translate) {
      message = await this.translate.instant(message);
      if (err && err.code) {
        message += ` {code: ${err.code}}`;
      }
    } else {
      message = `Fatal error: Please contact your administrator.\n\n{code: ${(err && err.code) || 'null'}, message: "${message}"}`;
    }
    if (this.toastController) {
      if (detailsMessage) {
        message += `<br/><small>${detailsMessage}</small>`;
      }
      await this.showToast({
        type: 'error',
        duration: -1,
        showCloseButton: true,
        message,
      });
    } else if (window) {
      if (detailsMessage) {
        message += `\n\n{cause: "${detailsMessage}"}`;
      }
      window.alert(message);
    } else {
      console.error(message);
      if (err && err.details) console.error('cause', err.details);
    }
  }
}
