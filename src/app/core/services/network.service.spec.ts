import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { IonicStorageModule } from '@ionic/storage-angular';
import { NetworkService } from './network.service';
import { ModalController } from '@ionic/angular';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { CacheModule } from 'ionic-cache';
import { of } from 'rxjs';
import { ENVIRONMENT } from '../../../environments/environment.class';
import { environment } from '../../../environments/environment.test';

describe('NetworkService', () => {
  // service to test
  let service: NetworkService;

  // some mocks
  const modalSpy = jasmine.createSpyObj('Modal', ['present', 'onDidDismiss']);
  const modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
  modalCtrlSpy.create.and.callFake(function () {
    return modalSpy;
  });
  const networkSpy = jasmine.createSpyObj('Network', ['onConnect', 'onDisconnect']);
  networkSpy.onConnect.and.callFake(() => of(true));
  networkSpy.onDisconnect.and.callFake(() => of(true));

  beforeEach(() => {
    TestBed.configureTestingModule({
    teardown: { destroyAfterEach: false },
    imports: [TranslateModule.forRoot(), IonicStorageModule.forRoot(), CacheModule.forRoot()],
    providers: [
        { provide: ModalController, useValue: modalCtrlSpy },
        { provide: Network, useValue: networkSpy },
        { provide: ENVIRONMENT, useValue: environment },
        NetworkService,
        provideHttpClient(withInterceptorsFromDi()),
    ]
});
    service = TestBed.inject(NetworkService);
  });

  it('should be created and started', async () => {
    expect(service).toBeTruthy();
    await service.ready();
    expect(service.started).toBeTrue();
  });
});
