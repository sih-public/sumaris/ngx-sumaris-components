import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpUtils } from '../../shared/http/http.utils';
import { Peer } from './model/peer.model';
import { NodeFeature } from './model/node-feature.model';
import { ConnectionStatus, ConnectionType, Network } from '@capacitor/network';
import { Observable } from 'rxjs';
import { Capacitor } from '@capacitor/core';

export interface NodeInfo {
  softwareName: string;
  softwareVersion: string;
  nodeLabel?: string;
  nodeName?: string;
  features?: NodeFeature[];
}

export interface AppManifest {
  // mandatory
  manifest_version: number;
  name: string;
  version: string;

  // App start (required by platform service)
  start_url: string;

  // optional
  short_name?: string;
  description?: string;
  version_name?: string;
  author?: string;
  default_locale?: string;

  // optional UI
  icons?: any | any[];
  background_color?: string;
  theme_color?: string;
  display?: string;
}

export class NetworkUtils {
  static async getNodeInfo(
    http: HttpClient,
    peer: string | Peer,
    opts?: {
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      responseType?: 'json';
    }
  ): Promise<NodeInfo> {
    if (!peer) throw Error("Missing argument 'peer'.");

    let peerUrl = peer instanceof Peer ? peer.url : (peer as string);
    // Remove trailing slash
    if (peerUrl.endsWith('/')) {
      peerUrl = peerUrl.substr(0, peerUrl.length - 1);
    }

    return HttpUtils.getJson(http, peerUrl + '/api/node/info', opts);
  }

  static isPluginAvailable(): boolean {
    return Capacitor.isPluginAvailable('Network');
  }

  static statusChanges(): Observable<ConnectionStatus> {
    return new Observable<ConnectionStatus>((subscriber) => {
      const handler = Network.addListener('networkStatusChange', (status: ConnectionStatus) => subscriber.next(status));
      return () => {
        handler.then((listener) => listener.remove());
      };
    });
  }

  static async getConnectionType(): Promise<ConnectionType> {
    if (NetworkUtils.isPluginAvailable()) {
      return (await Network.getStatus())?.connectionType || 'unknown';
    }
    return 'unknown';
  }
}
