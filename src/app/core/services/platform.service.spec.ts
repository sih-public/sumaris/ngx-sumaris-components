import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ModalController } from '@ionic/angular';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { CacheModule } from 'ionic-cache';
import { of } from 'rxjs';
import { PlatformService } from './platform.service';
import { NetworkService } from './network.service';
import { AudioProvider } from '../../shared/audio/audio';
import { PlatformModule } from '@angular/cdk/platform';

describe('PlatformService', () => {
  // service to test
  let service: PlatformService;

  // some mocks
  const platformSpy = jasmine.createSpyObj('Platform', ['url']);
  const modalSpy = jasmine.createSpyObj('Modal', ['present', 'onDidDismiss']);
  const modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
  modalCtrlSpy.create.and.callFake(function () {
    return modalSpy;
  });
  const networkSpy = jasmine.createSpyObj('Network', ['onConnect', 'onDisconnect']);
  networkSpy.onConnect.and.callFake(() => of(true));
  networkSpy.onDisconnect.and.callFake(() => of(true));

  beforeEach(() => {
    TestBed.configureTestingModule({
    teardown: { destroyAfterEach: false },
    imports: [TranslateModule.forRoot(),
        PlatformModule,
        CacheModule.forRoot()],
    providers: [
        { provide: ModalController, useValue: modalCtrlSpy },
        { provide: Network, useValue: networkSpy },
        // {provide: Platform, useValue: platformSpy},
        NetworkService,
        AudioProvider,
        PlatformService,
        provideHttpClient(withInterceptorsFromDi()),
    ]
});
    service = TestBed.inject(PlatformService);
  });

  it('should be created and started', async () => {
    expect(service).toBeTruthy();
    await service.ready();
    expect(service.started).toBeTrue();
  });
});
