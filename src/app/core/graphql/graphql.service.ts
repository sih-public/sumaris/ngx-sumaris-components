import { firstValueFrom, Observable, Subject } from 'rxjs';
import { Apollo, ExtraSubscriptionOptions, QueryRef } from 'apollo-angular';
import {
  ApolloCache,
  ApolloClient,
  ApolloLink,
  ApolloQueryResult,
  FetchPolicy,
  InMemoryCache,
  MutationUpdaterFunction,
  NetworkStatus,
  OperationVariables,
  TypePolicies,
  Unmasked,
  WatchQueryFetchPolicy,
} from '@apollo/client/core';
import { ErrorCodes, ServerErrorCodes, ServiceError } from '../services/errors';
import { catchError, distinctUntilChanged, filter, first, map, mergeMap, takeUntil, throttleTime } from 'rxjs/operators';

import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { NetworkService } from '../services/network.service';
import { ConnectionType } from '../services/network.types';
import {
  AppWebSocket,
  createTrackerLink,
  EmptyObject,
  isMutationOperation,
  isSubscriptionOperation,
  restoreTrackedQueries,
  StorageServiceWrapper,
  TrackableMutationContext,
} from './graphql.utils';
import { RetryLink } from '@apollo/client/link/retry';
import queueLinkImported from 'apollo-link-queue';
import serializingLinkImported from 'apollo-link-serialize';
import loggerLinkImported from 'apollo-link-logger';
import { Platform } from '@ionic/angular';
import { EntityUtils, IEntity } from '../services/model/entity.model';
import { isNil, isNotEmptyArray, isNotNil, toNumber } from '../../shared/functions';
import { Resolvers } from '@apollo/client/core/types';
import { HttpHeaders } from '@angular/common/http';
import { HttpLink, Options } from 'apollo-angular/http';
import { persistCache, PersistentStorage } from 'apollo3-cache-persist';
import { ErrorPolicy, MutationBaseOptions } from '@apollo/client/core/watchQueryOptions';
import { Cache } from '@apollo/client/cache/core/types/Cache';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { PropertyMap } from '../../shared/types';
import { StartableService } from '../../shared/services/startable-service.class';
import { isMobile } from '../../shared/platforms';
import { StorageService } from '../../shared/storage/storage.service';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { ClientOptions, createClient } from 'graphql-ws';
import { unwrapESModule } from '../../shared/modules';
import { createFragmentRegistry } from '@apollo/client/cache/inmemory/fragmentRegistry';
import { DocumentNode } from 'graphql';

// Workaround for issue https://github.com/ng-packagr/ng-packagr/issues/2215
const QueueLink = unwrapESModule(queueLinkImported);
const SerializingLink = unwrapESModule(serializingLinkImported);
const loggerLink = unwrapESModule(loggerLinkImported);

export interface WatchQueryOptions<V> {
  query: any;
  variables?: V;
  error?: ServiceError;
  fetchPolicy?: WatchQueryFetchPolicy;
}

export interface MutateQueryOptions<
  TData,
  TVariables = OperationVariables,
  TContext = TrackableMutationContext,
  TCache extends ApolloCache<any> = ApolloCache<any>,
> extends MutationBaseOptions<TData, TVariables, TContext, TCache> {
  mutation: any;
  variables?: TVariables;
  error?: ServiceError;
  context?: TContext;
  optimisticResponse?: Unmasked<NoInfer<TData>>;
  offlineResponse?: TData | ((context: TContext) => Promise<TData>);
  update?: MutationUpdaterFunction<TData, TVariables, TContext, TCache>;
  forceOffline?: boolean;
}

export const APP_GRAPHQL_TYPE_POLICIES = new InjectionToken<TypePolicies>('graphqlTypePolicies');

export const APP_GRAPHQL_FRAGMENTS = new InjectionToken<DocumentNode[]>('graphqlFragments');

export interface ConnectionParams extends Record<string, string> {
  authToken?: string;
  authBasic?: string;
}

@Injectable({
  providedIn: 'root',
})
export class GraphqlService extends StartableService<ApolloClient<any>> {
  private readonly _networkStatusChanged$: Observable<ConnectionType>;

  private httpParams: Options;
  private wsParams: ClientOptions<ConnectionParams>;
  private connectionParams: ConnectionParams = {};
  private readonly _defaultFetchPolicy: WatchQueryFetchPolicy;
  private onNetworkError = new Subject<any>();
  private customErrors: PropertyMap = {};
  private onResetAuth = new Subject<void>();

  get client(): ApolloClient<any> {
    return this._data;
  }

  get cache(): ApolloCache<any> {
    return this.apollo.client.cache;
  }

  get defaultFetchPolicy(): WatchQueryFetchPolicy {
    return this._defaultFetchPolicy;
  }

  constructor(
    private platform: Platform,
    private apollo: Apollo,
    private httpLink: HttpLink,
    private network: NetworkService,
    private storage: StorageService,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Optional() @Inject(APP_GRAPHQL_TYPE_POLICIES) private typePolicies: TypePolicies,
    @Optional() @Inject(APP_GRAPHQL_FRAGMENTS) private fragments: DocumentNode[]
  ) {
    super(platform); // Wait platform

    this._debug = !environment.production;
    this._defaultFetchPolicy = environment.apolloFetchPolicy;

    // Listen network status
    this._networkStatusChanged$ = network.onNetworkStatusChanges.pipe(filter(isNotNil), distinctUntilChanged());

    // When getting network error: try to ping peer, and toggle to offline
    this.onNetworkError
      .pipe(
        throttleTime(300),
        filter(() => this.network.online),
        mergeMap(() => this.network.checkPeerAlive()),
        filter((alive) => !alive)
      )
      .subscribe(() => this.network.setForceOffline(true, { showToast: true }));
  }

  async setAuthToken(token: string) {
    if (token) {
      console.debug('[graphql] Apply token authentication to headers');
      this.connectionParams.authToken = token;
    }
    // Clean auth token
    else if (this.connectionParams.authToken) {
      console.debug('[graphql] Remove token authentication from headers');
      delete this.connectionParams.authToken;

      await this.clearCache();
    }
  }

  async setAuthBasic(basic: string) {
    if (basic) {
      console.debug('[graphql] Apply basic authentication to headers');
      this.connectionParams.authBasic = basic;
    }
    // Clean auth basic
    else if (this.connectionParams.authBasic) {
      console.debug('[graphql] Remove basic authentication from headers');
      delete this.connectionParams.authBasic;

      await this.clearCache();
    }
  }

  /**
   * Allow to add a field resolver
   *  (see doc: https://www.apollographql.com/docs/react/data/local-state/#handling-client-fields-with-resolvers)
   *
   * @param resolvers
   */
  async addResolver(resolvers: Resolvers | Resolvers[]) {
    if (!this.started) await this.ready();
    this.apollo.client.addResolvers(resolvers);
  }

  async query<T, V = EmptyObject>(opts: { query: any; variables?: V; error?: ServiceError; fetchPolicy?: FetchPolicy }): Promise<T> {
    if (!this.started) await this.ready();
    let res: ApolloQueryResult<T>;
    try {
      res = (await this.client.query<T, V>({
        query: opts.query,
        variables: opts.variables,
        fetchPolicy: opts.fetchPolicy || (this._defaultFetchPolicy as FetchPolicy) || undefined,
      })) as ApolloQueryResult<T>;
    } catch (err) {
      res = this.toApolloError<T>(err, opts.error);
    }
    if (res.errors) {
      throw res.errors[0];
    }
    return res.data;
  }

  watchQueryRef<T, V = EmptyObject>(opts: WatchQueryOptions<V>): QueryRef<T, V> {
    return this.apollo.watchQuery<T, V>({
      query: opts.query,
      variables: opts.variables,
      fetchPolicy: opts.fetchPolicy || (this._defaultFetchPolicy as FetchPolicy) || undefined,
      notifyOnNetworkStatusChange: true,
    });
  }

  queryRefValuesChanges<T, V = EmptyObject>(queryRef: QueryRef<T, V>, opts: WatchQueryOptions<V>): Observable<T> {
    return queryRef.valueChanges.pipe(
      catchError(async (error) => this.toApolloError<T>(error, opts.error)),
      filter((value) => value.networkStatus === NetworkStatus.ready || value.networkStatus === NetworkStatus.error),
      map(({ data, errors }) => {
        if (errors) {
          throw errors[0];
        }
        return data as T;
      })
    );
  }

  watchQuery<T, V = EmptyObject>(opts: WatchQueryOptions<V>): Observable<T> {
    const queryRef: QueryRef<T, V> = this.watchQueryRef(opts);
    return this.queryRefValuesChanges(queryRef, opts);
  }

  async mutate<T, V = EmptyObject>(opts: MutateQueryOptions<T, V>): Promise<T> {
    // If offline, compute an optimistic response for tracked queries
    if ((opts.forceOffline || this.network.offline) && opts.offlineResponse) {
      if (typeof opts.offlineResponse === 'function') {
        opts.context = opts.context || {};
        const optimisticResponseFn = opts.offlineResponse as (context: any) => Promise<T>;
        opts.optimisticResponse = (await optimisticResponseFn(opts.context)) as Unmasked<NoInfer<T>>;
        if (this._debug) console.debug('[graphql] [offline] Using an optimistic response: ', opts.optimisticResponse);
      } else {
        opts.optimisticResponse = opts.offlineResponse as Unmasked<NoInfer<T>>;
      }
      if (opts.forceOffline) {
        const res = { data: opts.optimisticResponse as Unmasked<NoInfer<T>> };
        if (opts.update) {
          // @ts-ignore
          opts.update(this.apollo.client.cache, res, {});
        }
        return res.data as T;
      }
    }

    const res = await firstValueFrom(
      this.apollo
        .mutate<ApolloQueryResult<T>, V>({
          mutation: opts.mutation,
          variables: opts.variables,
          context: opts.context,
          optimisticResponse: opts.optimisticResponse as any,
          update: opts.update as any,
        })
        .pipe(
          catchError(async (error) => this.toApolloError<T>(error, opts.error)),
          first()
          // To debug, if need:
          //tap((res) => (!res) && console.error('[graphql] Unknown error during mutation. Check errors in console (maybe an invalid generated cache id ?)'))
        )
    );
    if (Array.isArray(res.errors)) {
      throw res.errors[0];
    }
    return res.data as T;
  }

  subscribe<T, V = EmptyObject>(
    opts: {
      query: any;
      variables: V;
      fetchPolicy?: FetchPolicy;
      errorPolicy?: ErrorPolicy;
      error?: ServiceError;
    },
    extra?: ExtraSubscriptionOptions
  ): Observable<T> {
    return this.apollo
      .subscribe<T>(
        {
          query: opts.query,
          fetchPolicy: (opts && opts.fetchPolicy) || 'network-only',
          errorPolicy: (opts && opts.errorPolicy) || undefined,
          variables: opts.variables,
        },
        {
          useZone: true,
          ...extra,
        }
      )
      .pipe(
        catchError(async (error) => this.toApolloError<T>(error, opts.error)),
        map(({ data, errors }) => {
          if (errors) {
            throw errors[0];
          }
          return data;
        })
      );
  }

  insertIntoQueryCache<T, V = EmptyObject>(
    cache: ApolloCache<any>,
    opts: Cache.ReadQueryOptions<V, any> & {
      arrayFieldName: string;
      totalFieldName?: string;
      data: T;
      sortFn?: (d1: T, d2: T) => number;
      size?: number;
    }
  ) {
    cache = cache || this.apollo.client.cache;
    opts.arrayFieldName = opts.arrayFieldName || 'data';

    try {
      let data = cache.readQuery<any, V>({ query: opts.query, variables: opts.variables });

      if (!data) return; // Skip: nothing in cache

      if (data[opts.arrayFieldName]) {
        // Copy because immutable
        data = { ...data };

        // Append to result array
        data[opts.arrayFieldName] = [...data[opts.arrayFieldName], { ...opts.data }];

        // Resort if needed
        if (opts.sortFn) {
          data[opts.arrayFieldName].sort(opts.sortFn);
        }

        // Exclude if exceed max size
        const size = toNumber(opts.variables && opts.variables['size'], -1);
        if (size > 0 && data[opts.arrayFieldName].length > size) {
          data[opts.arrayFieldName].splice(size, data[opts.arrayFieldName].length - size);
        }

        // Increment total
        if (isNotNil(opts.totalFieldName)) {
          if (isNotNil(data[opts.totalFieldName])) {
            data[opts.totalFieldName] += 1;
          } else {
            console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.totalFieldName);
          }
        }

        cache.writeQuery({
          query: opts.query,
          variables: opts.variables,
          data,
        });
      } else {
        console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.arrayFieldName);
      }
    } catch (err) {
      // continue
      // read in cache is not guaranteed to return a result. see https://github.com/apollographql/react-apollo/issues/1776#issuecomment-372237940
      if (this._debug) console.error('[graphql] Error while updating cache: ', err);
    }
  }

  addManyToQueryCache<T = any, V = EmptyObject>(
    cache: ApolloCache<any>,
    opts: Cache.ReadQueryOptions<V, any> & {
      arrayFieldName: string;
      totalFieldName?: string;
      data: T[];
      equalsFn?: (d1: T, d2: T) => boolean;
      sortFn?: (d1: T, d2: T) => number;
    }
  ) {
    if (!opts.data || !opts.data.length) return; // nothing to process

    cache = cache || this.apollo.client.cache;
    opts.arrayFieldName = opts.arrayFieldName || 'data';

    try {
      let data = cache.readQuery<any, V>({ query: opts.query, variables: opts.variables });
      if (!data) return 0; // skip

      if (data[opts.arrayFieldName]) {
        // Copy because immutable
        data = { ...data };

        // Keep only not existing res
        const equalsFn = opts.equalsFn || ((d1, d2) => d1['id'] === d2['id'] && d1['entityName'] === d2['entityName']);
        const newItems = opts.data.filter(
          (inputValue) => data[opts.arrayFieldName].findIndex((existingValue) => equalsFn(inputValue, existingValue)) === -1
        );

        if (!newItems.length) return; // No new value

        // Append to array
        data[opts.arrayFieldName] = [...data[opts.arrayFieldName], ...newItems];

        // Resort, if need
        if (opts.sortFn) {
          data[opts.arrayFieldName].sort(opts.sortFn);
        }

        // Exclude if exceed max size
        const size = toNumber(opts.variables && opts.variables['size'], -1);
        if (size > 0 && data[opts.arrayFieldName].length > size) {
          data[opts.arrayFieldName].splice(size, data[opts.arrayFieldName].length - size);
        }

        // Increment the total
        if (isNotNil(opts.totalFieldName)) {
          if (isNotNil(data[opts.totalFieldName])) {
            data[opts.arrayFieldName] += newItems.length;
          } else {
            console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.totalFieldName);
          }
        }

        // Write to cache
        cache.writeQuery({
          query: opts.query,
          variables: opts.variables,
          data,
        });
      } else {
        console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.arrayFieldName);
      }
    } catch (err) {
      // continue
      // read in cache is not guaranteed to return a result. see https://github.com/apollographql/react-apollo/issues/1776#issuecomment-372237940
      if (this._debug) console.warn('[graphql] Error while updating cache: ', err);
    }
  }

  /**
   * Remove from cache, and return if removed or not
   *
   * @param cache
   * @param opts
   */
  removeFromCachedQueryById<V = EmptyObject, ID = number>(
    cache: ApolloCache<any>,
    opts: Cache.ReadQueryOptions<V, any> & {
      arrayFieldName: string;
      totalFieldName?: string;
      ids: ID; // Do NOT use 'id', as already used by the Apollo API
    }
  ): boolean {
    cache = cache || this.apollo.client.cache;
    opts.arrayFieldName = opts.arrayFieldName || 'data';

    try {
      let data = cache.readQuery<any, V>({ query: opts.query, variables: opts.variables });

      if (data && data[opts.arrayFieldName]) {
        // Copy because immutable
        data = { ...data };

        const index = data[opts.arrayFieldName].findIndex((item) => item['id'] === opts.ids);
        if (index === -1) return false; // Skip (nothing removed)

        // Copy, then remove deleted item
        data[opts.arrayFieldName] = data[opts.arrayFieldName].slice();
        const deletedItem = data[opts.arrayFieldName].splice(index, 1)[0];
        cache.evict({ id: cache.identify(deletedItem) });

        // Decrement the total
        if (isNotNil(opts.totalFieldName)) {
          if (isNotNil(data[opts.totalFieldName])) {
            data[opts.totalFieldName] -= 1;
          } else {
            console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.totalFieldName);
          }
        }

        // Write to cache
        cache.writeQuery({
          query: opts.query,
          variables: opts.variables,
          data,
        });
        return true;
      } else {
        console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.arrayFieldName);
        return false;
      }
    } catch (err) {
      // continue
      // read in cache is not guaranteed to return a result. see https://github.com/apollographql/react-apollo/issues/1776#issuecomment-372237940
      if (this._debug) console.warn('[graphql] Error while removing from cache: ', err);
      return false;
    }
  }

  /**
   * Remove ids from cache, and return the number of items removed
   *
   * @param cache
   * @param opts
   */
  removeFromCachedQueryByIds<V = EmptyObject, ID = number>(
    cache: ApolloCache<any>,
    opts: Cache.ReadQueryOptions<V, any> & {
      arrayFieldName: string;
      totalFieldName?: string;
      ids: ID[];
    }
  ): number {
    cache = cache || this.apollo.client.cache;
    opts.arrayFieldName = opts.arrayFieldName || 'data';

    try {
      let data = cache.readQuery(opts);

      if (data && data[opts.arrayFieldName]) {
        // Copy because immutable
        data = { ...data };

        const deletedIndexes = data[opts.arrayFieldName].reduce((res, item, index) => (opts.ids.includes(item['id']) ? res.concat(index) : res), []);

        if (deletedIndexes.length <= 0) return 0; // Skip (nothing removed)

        // Query has NO total
        if (isNil(opts.totalFieldName)) {
          // Evict each object
          deletedIndexes
            .map((index) => data[opts.arrayFieldName][index])
            .map((item) => cache.identify(item))
            .forEach((id) => cache.evict({ id }));
        }
        // Query has a total
        else {
          // Copy the array
          data[opts.arrayFieldName] = data[opts.arrayFieldName].slice();

          // remove from array, then evict
          deletedIndexes
            // Reverse: to keep valid index
            .reverse()
            // Remove from the array
            .map((index) => data[opts.arrayFieldName].splice(index, 1)[0])
            // Evict from cache
            .map((item) => cache.identify(item))
            .forEach((id) => cache.evict({ id }));

          if (isNotNil(data[opts.totalFieldName])) {
            data[opts.totalFieldName] -= deletedIndexes.length; // Remove deletion count
          } else {
            console.warn('[graphql] Unable to update the total in cached query. Unknown result part: ' + opts.totalFieldName);
          }

          cache.writeQuery({
            query: opts.query,
            variables: opts.variables,
            data,
          });

          return deletedIndexes.length;
        }
      } else {
        console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.arrayFieldName);
        return 0;
      }
    } catch (err) {
      // continue
      // read in cache is not guaranteed to return a result. see https://github.com/apollographql/react-apollo/issues/1776#issuecomment-372237940
      if (this._debug) console.warn('[graphql] Error while removing from cache: ', err);
      return 0;
    }
  }

  updateToQueryCache<T extends IEntity<any>, V = EmptyObject>(
    cache: ApolloCache<any>,
    opts: Cache.ReadQueryOptions<V, any> & {
      arrayFieldName: string;
      totalFieldName?: string;
      data: T;
      equalsFn?: (d1: T, d2: T) => boolean;
    }
  ) {
    cache = cache || this.apollo.client.cache;
    opts.arrayFieldName = opts.arrayFieldName || 'data';

    try {
      let data: any = cache.readQuery(opts);

      if (data && data[opts.arrayFieldName]) {
        // Copy because immutable
        data = { ...data };

        const equalsFn = opts.equalsFn || ((d1, d2) => EntityUtils.equals(d1, d2, 'id'));

        // Update if exists, or insert
        const index = data[opts.arrayFieldName].findIndex((v) => equalsFn(opts.data, v));
        if (index !== -1) {
          data[opts.arrayFieldName] = data[opts.arrayFieldName].slice().splice(index, 1, opts.data);
        } else {
          data[opts.arrayFieldName] = [...data[opts.arrayFieldName], opts.data];
        }

        // Increment total (if changed)
        if (isNotNil(opts.totalFieldName) && index === -1) {
          if (isNotNil(data[opts.totalFieldName])) {
            data[opts.totalFieldName] += 1;
          } else {
            console.warn('[graphql] Unable to update cached query. Unknown result part: ' + opts.totalFieldName);
          }
        }

        cache.writeQuery({
          query: opts.query,
          variables: opts.variables,
          data,
        });
        return; // OK: stop here
      }
    } catch (err) {
      // continue
      // read in cache is not guaranteed to return a result. see https://github.com/apollographql/react-apollo/issues/1776#issuecomment-372237940
      if (this._debug) console.warn('[graphql] Error while updating cache: ', err);
    }
  }

  async clearCache(client?: ApolloClient<any>): Promise<void> {
    client = (client || this.client) as ApolloClient<any>;
    if (client) {
      console.info('[graphql] Cleaning cache... ');
      const now = this._debug && Date.now();

      // Clearing the cache
      await client.cache.reset();

      if (this._debug) console.debug(`[graphql] Cleaning cache [OK] in ${Date.now() - now}ms`);
    }
  }

  registerCustomError(error: PropertyMap) {
    this.customErrors = { ...this.customErrors, ...error };
  }

  /* -- protected methods -- */

  protected async ngOnStart(): Promise<ApolloClient<any>> {
    // Wait network and storage
    await Promise.all([this.storage.ready(), this.network.ready()]);

    console.info('[graphql] Starting graphql...');
    const mobile = isMobile(window);
    const enableTrackMutationQueries = !mobile;

    const peer = this.network.peer;
    if (!peer) throw Error('[graphql] Missing peer. Unable to start graphql service');

    const uri = peer.url + '/graphql';
    const wsUrl = uri.replace(/^http(s)?:/, 'ws$1:') + '/websocket';
    console.info('[graphql] Base uri: ' + uri);
    console.info('[graphql] Subscription uri: ' + wsUrl);

    this.httpParams = this.httpParams || {};
    this.httpParams.uri = uri;

    this.wsParams = {
      ...this.wsParams,
      lazy: true,
      connectionParams: this.connectionParams,
      webSocketImpl: AppWebSocket,
      url: wsUrl,
      shouldRetry: (errOrCloseEvent) => {
        // If WS URL changed, then do not retry
        if (wsUrl !== this.wsParams.url) {
          return false;
        }
        console.warn('[graphql-service] [WS] Trying to reconnect...', errOrCloseEvent);
        return true;
      },
      retryAttempts: 10,
    };

    // Create a storage configuration
    const storage: PersistentStorage<string> = new StorageServiceWrapper(this.storage);

    let client: ApolloClient<any>;
    try {
      client = this.apollo.client;
    } catch (err) {
      // Since apollo-angular 7, missing client throw an exception
      // Continue (client not exists)
    }

    if (!client) {
      console.debug('[graphql] Creating Apollo GraphQL client...');

      // Websocket link
      const wsLink = new GraphQLWsLink(createClient(this.wsParams));
      this.onResetAuth.pipe(takeUntil(this.stopSubject)).subscribe(() => {
        console.info('[graphql] Closing websocket client, to force unauth');
        wsLink.client.dispose();
      });

      // Retry when failed link
      const retryLink = new RetryLink();
      const authLink = new ApolloLink((operation, forward) => {
        const authorization = [];
        if (this.connectionParams.authToken) {
          authorization.push(`token ${this.connectionParams.authToken}`);
        }
        if (this.connectionParams.authBasic) {
          authorization.push(`Basic ${this.connectionParams.authBasic}`);
        }
        const headers = new HttpHeaders().append('Authorization', authorization);

        // Use the setContext method to set the HTTP headers.
        operation.setContext({
          ...operation.getContext(),
          ...{ headers },
        });

        // Call the next link in the middleware chain.
        return forward(operation);
      });

      // Http link
      const httpLink = this.httpLink.create(this.httpParams);

      // Cache
      const cache = new InMemoryCache({
        typePolicies: this.typePolicies,
        fragments: isNotEmptyArray(this.fragments) ? createFragmentRegistry(...this.fragments) : undefined,
      });

      // Add cache persistence
      if (this.environment.persistCache) {
        console.debug('[graphql] Starting persistence cache...');
        await persistCache({
          cache,
          storage,
          trigger: this.platform.is('android') ? 'background' : 'write',
          debounce: 1000,
          debug: true,
        });
      }

      let mutationLinks: Array<ApolloLink>;

      // Add queue to store tracked queries, when offline
      if (enableTrackMutationQueries) {
        const serializingLink = new SerializingLink();
        const trackerLink = createTrackerLink({
          storage,
          onNetworkStatusChange: this._networkStatusChanged$,
          debounce: 1000,
          debug: true,
        });
        // Creating a mutation queue
        const queueLink = new QueueLink();
        this.registerSubscription(
          this._networkStatusChanged$.subscribe((connectionType) => {
            // Network is offline: start buffering into queue
            if (connectionType === 'none') {
              console.info('[graphql] offline mode: enable mutations buffer');
              queueLink.close();
            }
            // Network is online
            else {
              console.info('[graphql] online mode: disable mutations buffer');
              queueLink.open();
            }
          })
        );
        mutationLinks = [loggerLink, trackerLink, queueLink, serializingLink, retryLink, authLink, httpLink];
      } else {
        mutationLinks = [retryLink, authLink, httpLink];
      }

      // create Apollo
      client = new ApolloClient({
        cache,
        link: ApolloLink.split(
          // Handle mutations
          isMutationOperation,
          ApolloLink.from(mutationLinks),

          ApolloLink.split(
            // Handle subscriptions
            isSubscriptionOperation,
            wsLink,

            // Handle queries
            ApolloLink.from([retryLink, authLink, httpLink])
          )
        ),
        // For @apollo/client < 3.11
        connectToDevTools: !this.environment.production,
        // For @apollo/client 3.11+
        devtools: {
          enabled: !this.environment.production,
          name: 'GraphqlService Client',
        },
      });

      this.apollo.client = client;
    }

    // Enable tracked queries persistence
    if (enableTrackMutationQueries && this.environment.persistCache) {
      try {
        await restoreTrackedQueries({
          apolloClient: client,
          storage,
          debug: true,
        });
      } catch (err) {
        console.error('[graphql] Failed to restore tracked queries from storage: ' + ((err && err.message) || err), err);
      }
    }

    // Listen for network restart
    this.registerSubscription(this.network.on('start', () => this.restart()));

    // Listen for clear cache request, from network
    this.registerSubscription(this.network.on('resetCache', () => this.clearCache()));

    console.info('[graphql] Starting graphql [OK]');
    return client;
  }

  protected async ngOnStop() {
    console.info('[graphql] Stopping graphql service...');
    await this.resetClient();
  }

  protected async resetClient(client?: ApolloClient<any>) {
    try {
      client = (client || this.apollo.client) as ApolloClient<any>;
    } catch (err) {
      // Since apollo-angular 7, missing client throw an exception
      // Continue (client not exists)
    }
    if (!client) return;

    console.info('[graphql] Resetting Apollo client...');
    client.stop();

    this.onResetAuth.next();

    await Promise.all([client.clearStore(), this.clearCache(client)]);
  }

  private toApolloError<T>(err: any, defaultError?: any): ApolloQueryResult<T> {
    let error =
      // If network error: try to convert to App (read as JSON), or create an UNKNOWN_NETWORK_ERROR
      (err.networkError &&
        ((err.networkError.error && this.toAppError(err.networkError.error)) ||
          this.toAppError(err.networkError) ||
          (err.networkError.error && this.createAppErrorByCode(err.networkError.error.status)) ||
          this.createAppErrorByCode(ErrorCodes.UNKNOWN_NETWORK_ERROR))) ||
      // If graphQL: try to convert the first error found
      (err.graphQLErrors && err.graphQLErrors.length && this.toAppError(err.graphQLErrors[0])) ||
      this.toAppError(err) ||
      this.toAppError(err.originalError) ||
      (err.graphQLErrors && err.graphQLErrors[0]) ||
      err;
    console.error('[graphql] ' + ((error && error.message) || error), error.stack || '');
    if (error?.code === ErrorCodes.UNKNOWN_NETWORK_ERROR && err.networkError?.message) {
      console.error('[graphql] original error: ' + err.networkError.message);
      this.onNetworkError.next(error);
    }

    // Apply default error, and store original error into error's details
    if ((!error || !error.code || error.code === ErrorCodes.INTERNAL_SERVER_ERROR) && defaultError) {
      error = { ...defaultError, details: error, stack: err.stack };
      if (defaultError.message) {
        error.message = defaultError.message;
      }
    }

    return {
      data: null,
      errors: [error],
      loading: false,
      networkStatus: NetworkStatus.error,
    };
  }

  private createAppErrorByCode(errorCode: number): any | undefined {
    const message = this.getI18nErrorMessageByCode(errorCode);
    if (message) {
      return {
        code: errorCode,
        message,
      };
    }
    return undefined;
  }

  private getI18nErrorMessageByCode(errorCode: number): string | undefined {
    // look in registered error codes
    const customErrorMessage = this.customErrors[errorCode];
    if (customErrorMessage) {
      return customErrorMessage;
    }

    // Default, switch on error code
    switch (errorCode) {
      case ServerErrorCodes.UNAUTHORIZED:
        return 'ERROR.UNAUTHORIZED';
      case ServerErrorCodes.FORBIDDEN:
        return 'ERROR.FORBIDDEN';
      case ErrorCodes.UNKNOWN_NETWORK_ERROR:
        return 'ERROR.UNKNOWN_NETWORK_ERROR';
      case ServerErrorCodes.BAD_UPDATE_DATE:
        return 'ERROR.BAD_UPDATE_DATE';
      case ServerErrorCodes.DATA_LOCKED:
        return 'ERROR.DATA_LOCKED';
      case ServerErrorCodes.BAD_APP_VERSION:
        return 'ERROR.BAD_APP_VERSION';
      case ServerErrorCodes.DATA_NOT_UNIQUE:
        return 'ERROR.DATA_NOT_UNIQUE';
    }

    return undefined;
  }

  private toAppError(err: any): any | undefined {
    let error = err;
    let message = (err && err.message) || err;
    // parse message if JSON
    if (typeof message === 'string' && message.trim().indexOf('{"code":') === 0) {
      try {
        error = JSON.parse(
          message
            // Remove special characters before parsing (e.g. SQL errors from an Oracle database)
            .replace(/\n\r?/g, ' ')
            .replace(/\r/g, '')
        );
        message = error.message || message;
      } catch (parseError) {
        console.error('Unable to parse error as JSON: ', parseError);
      }
    }
    if (error && error.code) {
      const appError = {
        code: error.code,
        message: typeof message === 'string' ? message : error.message,
        ...this.createAppErrorByCode(error.code),
      };
      if (appError.code !== error.code || appError.message !== error.message) {
        // Store original error in details
        appError.details = error;
        return appError;
      }
      // Keep error (with details, if any)
      return error;
    }
    return undefined;
  }
}
