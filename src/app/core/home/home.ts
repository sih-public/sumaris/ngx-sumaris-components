import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, InjectionToken, OnDestroy, Optional } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { RegisterModal } from '../register/register.modal';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AccountService } from '../services/account.service';
import { Account, accountToString } from '../services/model/account.model';
import { Configuration } from '../services/model/config.model';
import { Department } from '../services/model/department.model';
import { APP_LOCALES, LocaleConfig, LocalSettings } from '../services/model/settings.model';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from '../services/config.service';
import { PlatformService } from '../services/platform.service';
import { LocalSettingsService } from '../services/local-settings.service';
import { debounceTime, distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { AppAuthModal } from '../auth/auth.modal';
import { NetworkService } from '../services/network.service';
import { IMenuItem, MenuItems } from '../menu/menu.model';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { fadeInAnimation, slideUpDownAnimation } from '../../shared/material/material.animations';
import { isNilOrBlank, isNotEmptyArray, isNotNil, isNotNilOrBlank } from '../../shared/functions';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { HistoryPageReference } from '../services/model/history.model';
import { CORE_CONFIG_OPTIONS } from '../services/config/core.config';
import { RxState } from '@rx-angular/state';
import { ImagesUtils } from '../../shared/file/images.utils';
import { UriUtils } from '../../shared/file/uri.utils';
import { Peer } from '../services/model/peer.model';
import { AppMarkdownModal } from '../../shared/markdown/markdown.modal';

/**
 * @param files
 * @deprecated use `getRandomImageWithCredit()` instead
 **/
export const getRandomImage = ImagesUtils.getRandomImage;
export const getRandomImageWithCredit = ImagesUtils.getRandomImageWithCredit;

export const APP_HOME_BUTTONS = new InjectionToken<IMenuItem[]>('homeButton');
export interface HomePageState {
  loading: boolean;
  showSpinner: boolean;
  showPartnerBanner: boolean;
  showLegalInformation: boolean;
  showInstallLinks: boolean;
  pageHistory: HistoryPageReference[];
  darkMode: boolean;
}

@Component({
  selector: 'app-page-home',
  templateUrl: 'home.html',
  styleUrls: ['./home.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeInAnimation, slideUpDownAnimation],
})
export class HomePage extends RxState<HomePageState> implements OnDestroy {
  private readonly _debug: boolean;
  private _subscription = new Subscription();
  private _config: Configuration;

  protected darkMode$ = this.select('darkMode');
  protected pageHistory$ = this.select('pageHistory');
  protected loading$ = this.select('loading');
  protected readonly mobile: boolean;

  waitingNetwork = false;
  accountName = '';
  isLogin: boolean;
  $partners = new BehaviorSubject<Department[]>(null);
  logo: string;
  description: string;
  appName: string;
  isWeb: boolean;
  canRegister: boolean;
  contentStyle: any;
  contentCredits: string;
  legalInformation: IMenuItem[];
  offline: boolean;
  $filteredButtons = new BehaviorSubject<IMenuItem[]>(undefined);
  readonly showAccountButton: boolean;

  set pageHistory(value: HistoryPageReference[]) {
    this.set('pageHistory', () => value);
  }

  get pageHistory(): HistoryPageReference[] {
    return this.get('pageHistory');
  }

  set loading(value: boolean) {
    this.set('loading', () => value);
  }

  get loading(): boolean {
    return this.get('loading');
  }

  set showSpinner(value: boolean) {
    this.set('showSpinner', () => value);
  }
  get showSpinner(): boolean {
    return this.get('showSpinner');
  }

  set showPartnerBanner(value: boolean) {
    this.set('showPartnerBanner', () => value);
  }
  get showPartnerBanner(): boolean {
    return this.get('showPartnerBanner');
  }

  set showLegalInformation(value: boolean) {
    this.set('showLegalInformation', () => value);
  }
  get showLegalInformation(): boolean {
    return this.get('showLegalInformation');
  }

  set showInstallLinks(value: boolean) {
    this.set('showInstallLinks', () => value);
  }
  get showInstallLinks(): boolean {
    return this.get('showInstallLinks');
  }

  set darkMode(value: boolean) {
    this.set('darkMode', () => value);
  }
  get darkMode(): boolean {
    return this.get('darkMode');
  }

  get currentLocaleCode(): string {
    return this.loading ? '' : (this.translate.currentLang || this.translate.defaultLang || this.environment.defaultLocale)?.substring(0, 2);
  }

  get partnerBannerSize(): number {
    return this.showLegalInformation ? 9 : 12;
  }

  constructor(
    private accountService: AccountService,
    private modalCtrl: ModalController,
    private translate: TranslateService,
    private toastController: ToastController,
    private configService: ConfigService,
    private platform: PlatformService,
    private cd: ChangeDetectorRef,
    public network: NetworkService,
    public settings: LocalSettingsService,
    @Inject(ENVIRONMENT) protected environment: Environment,
    @Inject(APP_LOCALES) public locales: LocaleConfig[],
    @Optional() @Inject(APP_HOME_BUTTONS) public buttons: IMenuItem[]
  ) {
    super();
    this.set(<Partial<HomePageState>>{
      loading: true,
      showPartnerBanner: false,
      showSpinner: !this.platform.started,
      darkMode: this.settings.isDarkMode,
    });

    this.mobile = this.platform.is('mobile');
    this.platform.ready().then(() => this.start());
    this.showAccountButton = environment.account?.showAccountButton ?? true;

    this.connect('darkMode', this.settings.darkMode$);

    // DEV
    this._debug = !environment.production;
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  async login() {
    const modal = await this.modalCtrl.create({
      component: AppAuthModal,
    });
    return modal.present();
  }

  async register() {
    const modal = await this.modalCtrl.create({
      component: RegisterModal,
      backdropDismiss: false,
    });
    return modal.present();
  }

  async logout(event: any) {
    await this.accountService.logout();

    // If was offline, try to reconnect (because can be a forced offline mode)
    if (this.offline) {
      await this.tryOnline();
    }
  }

  changeLanguage(locale: string) {
    this.settings.apply({ locale }).then(() => {
      this.markForCheck();
    });
  }

  getPagePath(index: number, page: HistoryPageReference): string {
    return page?.path;
  }

  tryOnline() {
    this.waitingNetwork = true;
    this.markForCheck();

    this.network
      .tryOnline({
        showLoadingToast: false,
        showOnlineToast: true,
        showOfflineToast: false,
      })
      .then(() => {
        this.waitingNetwork = false;
        this.markForCheck();
      });
  }

  /* -- protected method  -- */

  protected async start() {
    await this.accountService.ready();

    this.isLogin = this.accountService.isLogin();
    if (this.isLogin) {
      this.onLogin(this.accountService.account);
    }

    this.offline = this.network.offline;

    // Listen login/logout events
    this._subscription.add(this.accountService.onLogin.subscribe((account) => this.onLogin(account)));
    this._subscription.add(this.accountService.onLogout.subscribe(() => this.onLogout()));

    // Listen pod config
    this._subscription.add(this.configService.config.subscribe((config) => this.onConfigLoaded(config)));

    // Listen settings changes
    this._subscription.add(
      this.settings.onChange
        .pipe(
          // Add a delay, to avoid to apply changes to often
          debounceTime(250),
          // Start with current settings
          startWith(await this.settings.ready())
        )
        .subscribe((res) => this.onSettingsChanged(res))
    );

    // Listen network changes
    this._subscription.add(
      this.network.onNetworkStatusChanges
        .pipe(
          //debounceTime(450),
          //tap(() => this.waitingNetwork = false),
          map((connectionType) => connectionType === 'none'),
          distinctUntilChanged()
        )
        .subscribe((offline) => {
          this.offline = offline;
          this.markForCheck();
        })
    );
  }

  protected async showToast(opts: ShowToastOptions) {
    await Toasts.show(this.toastController, this.translate, opts);
  }

  protected onConfigLoaded(config: Configuration) {
    console.debug('[home] Applying configuration', config);
    this._config = config;

    this.appName = this._config.label || this.environment.defaultAppName || 'SUMARiS';
    this.logo = this._config.largeLogo || this._config.smallLogo || undefined;
    this.description = config.name;
    this.isWeb = this.platform.isWeb();
    this.canRegister = this._config.getPropertyAsBoolean(CORE_CONFIG_OPTIONS.REGISTRATION_ENABLE);
    this.legalInformation = [
      {
        title: 'HOME.PRIVACY_POLICY',
        path: this._config.getProperty(CORE_CONFIG_OPTIONS.PRIVACY_POLICY_URL) ?? (this.environment.privacyPolicyUrl as string),
      },
      {
        title: 'HOME.TERMS_OF_USE',
        path: this._config.getProperty(CORE_CONFIG_OPTIONS.TERMS_OF_USE_URL) ?? (this.environment.termsOfUseUrl as string),
      },
    ].filter((item) => isNotNilOrBlank(item.path));
    this.showLegalInformation = isNotEmptyArray(this.legalInformation);

    const partners = (this._config.partners || []).filter((p) => p && p.logo);
    this.$partners.next(partners);
    this.showPartnerBanner = partners.length > 0;
    // TODO load install links

    // If not already set, compute the background image
    if (!this.contentStyle) {
      const backgroundImages = (config.backgroundImages || [])
        // Filter on not nil, because can occur if detected has not exists (see config-service.js)
        .filter(
          (img) =>
            isNotNil(img) &&
            // If offline, filter on local dataURL image
            (!this.offline || !img.startsWith('http'))
        );

      // Background image found: select one randomly
      if (backgroundImages.length) {
        const { image, credits } = getRandomImageWithCredit(backgroundImages);
        this.contentStyle = image ? { 'background-image': `url(${image})` } : {};
        this.contentCredits = credits || '';
      }

      // Use background color
      else {
        const primaryColor = config.getProperty(CORE_CONFIG_OPTIONS.COLOR_PRIMARY) || 'var(--ion-color-primary)';
        this.contentStyle = (primaryColor && { 'background-color': primaryColor }) || {};
      }
    }

    this.refreshButtons();
    this.markForCheck();

    // If first load, hide the loading indicator
    this.loading = false;
  }

  protected onSettingsChanged(settings: LocalSettings) {
    if (settings.pageHistory !== this.pageHistory) {
      console.debug('[home] Page history loaded');
      this.pageHistory = settings.pageHistory || [];
    }
  }

  protected onLogin(account: Account) {
    if (!account) return; // Skip
    //console.debug('[home] Logged account: ', account);
    this.isLogin = true;
    this.accountName = accountToString(account);
    this.refreshButtons(account);
    this.markForCheck();
  }

  protected onLogout() {
    this.isLogin = false;
    this.accountName = '';
    this.pageHistory = [];
    this.refreshButtons();
    this.markForCheck();
  }

  protected refreshButtons(account?: Account) {
    if (!this._config) return; // Skip (waiting config to be loaded)
    if (this._debug) console.debug('[home] Refreshing buttons...');

    account = account || this.accountService.account;
    const filteredButtons = (this.buttons || [])
      .filter((item) =>
        MenuItems.checkIfVisible(item, account, this._config, {
          isLogin: this.isLogin,
          debug: this._debug,
          logPrefix: '[home]',
        })
      )
      .map((item) => {
        // Replace title using properties
        if (isNotNilOrBlank(item.titleProperty) && this._config) {
          const title = this._config.properties[item.titleProperty];
          if (title) return { ...item, title }; // Create a copy, to keep the original item.title
        }
        return item;
      });

    this.$filteredButtons.next(filteredButtons);
  }

  async openMarkdownModal(event?: Event, item: IMenuItem = null) {
    if (isNilOrBlank(item?.path)) return;

    // Resolve relative path
    let url = item.path;
    if (this.network.peer && (url.startsWith('./') || url.startsWith('/'))) {
      url = Peer.path(this.network.peer, url);
    }

    const pathFilename = UriUtils.getFilename(url);
    const isMarkdownFile = pathFilename.endsWith('.md');

    // Open as markdown
    if (isMarkdownFile) {
      await AppMarkdownModal.show(this.modalCtrl, {
        title: this.translate.instant(item.title, item.titleArgs),
        src: url,
        enableNavigationHistory: false,
        canPrint: !this.mobile,
      });
    } else {
      // Open as external link
      await this.platform.open(url);
    }
  }

  protected async toggleDarkMode() {
    const enable = !this.settings.isDarkMode;
    await this.settings.apply({ darkMode: enable, autoDarkMode: false });
  }

  protected async removePageHistory(path: string) {
    await this.settings.removePageHistory(path);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
