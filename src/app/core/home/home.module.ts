import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { HomePage } from './home';
import { AppAuthModule } from '../auth/auth.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { AppInstallUpgradeCardModule } from '../install/install-upgrade-card.module';
import { RxStateModule } from '../../shared/rx-state/rx-state.module';
import { SharedMarkdownModule } from '../../shared/markdown/markdown.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    TranslateModule.forChild(),
    RxStateModule,

    // App modules
    AppAuthModule,
    AppInstallUpgradeCardModule,
    SharedMarkdownModule,
  ],

  declarations: [HomePage],
  exports: [RouterModule, TranslateModule, HomePage],
})
export class AppHomePageModule {}
