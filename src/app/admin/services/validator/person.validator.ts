import { Injectable } from '@angular/core';
import { UntypedFormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { Person, PersonUtils } from '../../../core/services/model/person.model';
import { SharedValidators } from '../../../shared/validator/validators';
import { StatusIds } from '../../../core/services/model/model.enum';
import { AccountService } from '../../../core/services/account.service';
import { FormFieldDefinition } from '../../../shared/form/field.model';
import { AppValidatorService } from '../../../core/services/validator/base.validator.class';
import { TranslateService } from '@ngx-translate/core';

export interface PersonValidatorOptions {
  withAvatar?: boolean;
  withUsername?: boolean;
  withUsernameExtranet?: boolean;
}

@Injectable({ providedIn: 'root' })
export class PersonValidatorService<
  T extends Person = Person,
  O extends PersonValidatorOptions = PersonValidatorOptions,
> extends AppValidatorService<T> {
  constructor(
    formBuilder: UntypedFormBuilder,
    translate: TranslateService,
    protected accountService: AccountService
  ) {
    super(formBuilder, translate);
  }

  getFormGroupConfig(data?: T, opts?: O): { [key: string]: any } {
    opts = this.fillDefaultOptions(opts);

    const formConfig: { [key: string]: any[] } = {
      id: [''],
      updateDate: [''],
      creationDate: [''],
      lastName: [data?.lastName || null, Validators.compose([Validators.required, Validators.minLength(2)])],
      firstName: [data?.firstName || null, Validators.compose([Validators.required, Validators.minLength(2)])],
      email: [data?.email || null, Validators.compose([Validators.required, Validators.email])],
      mainProfile: [data?.mainProfile || PersonUtils.getMainProfile(data?.profiles) || 'GUEST', Validators.required],
      statusId: [data?.statusId || StatusIds.TEMPORARY, Validators.required],
      pubkey: [data?.pubkey || null, Validators.compose([Validators.required, SharedValidators.pubkey])],
    };

    if (opts.withAvatar) {
      formConfig.avatar = [(data && data.avatar) || null];
    }
    if (opts.withUsername) {
      formConfig.username = [(data && data.username) || null, Validators.minLength(2)];
    }
    if (opts.withUsernameExtranet) {
      formConfig.usernameExtranet = [(data && data.usernameExtranet) || null, Validators.minLength(2)];
    }

    // Add additional fields
    this.accountService.additionalFields.forEach((field) => {
      //console.debug("[register-form] Add additional field {" + field.name + "} to form", field);
      formConfig[field.key] = [(data && data[field.key]) || null, this.getValidators(field)];
    });

    return formConfig;
  }

  getValidators(field: FormFieldDefinition): ValidatorFn | ValidatorFn[] {
    const validatorFns: ValidatorFn[] = [];
    if (field.extra && field.extra.account && field.extra.account.required) {
      validatorFns.push(Validators.required);
    }
    if (field.type === 'entity') {
      validatorFns.push(SharedValidators.entity);
    }

    return validatorFns.length ? Validators.compose(validatorFns) : validatorFns.length === 1 ? validatorFns[0] : undefined;
  }

  /* -- protected methods -- */

  protected fillDefaultOptions(opts?: O): O {
    return {
      withAvatar: true,
      withUsername: true,
      withUsernameExtranet: true,
      ...opts,
    };
  }
}
