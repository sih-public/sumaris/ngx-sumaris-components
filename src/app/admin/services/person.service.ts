import { Injectable } from '@angular/core';
import { gql } from '@apollo/client/core';
import { BehaviorSubject } from 'rxjs';
import { GraphqlService } from '../../core/graphql/graphql.service';
import { NetworkService } from '../../core/services/network.service';
import { EntitiesStorage } from '../../core/services/storage/entities-storage.service';
import { Person, UserProfileLabel } from '../../core/services/model/person.model';
import { StatusIds } from '../../core/services/model/model.enum';
import { SortDirection } from '@angular/material/sort';
import { JobUtils } from '../../shared/services/job.utils';
import { EntitiesServiceLoadOptions, EntityServiceLoadOptions, LoadResult, SuggestService } from '../../shared/services/entity-service.class';
import { PlatformService } from '../../core/services/platform.service';
import { PersonFilter } from './filter/person.filter';
import { EntityUtils } from '../../core/services/model/entity.model';
import { BaseEntityGraphqlMutations, BaseEntityService } from '../../core/services/base-entity-service.class';
import { environment } from '../../../environments/environment';
import { isNotEmptyArray } from '../../shared/functions';

export const PersonFragments = {
  person: gql`
    fragment PersonFragment on PersonVO {
      id
      firstName
      lastName
      email
      pubkey
      avatar
      statusId
      updateDate
      creationDate
      profiles
      username
      usernameExtranet
      department {
        id
        label
        name
        logo
        __typename
      }
      __typename
    }
  `,
};

// Load persons query
const PersonQueries = {
  loadAll: gql`
    query Persons($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: PersonFilterVOInput) {
      data: persons(filter: $filter, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection) {
        ...PersonFragment
      }
    }
    ${PersonFragments.person}
  `,

  loadAllWithTotal: gql`
    query PersonsWithTotal($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: PersonFilterVOInput) {
      data: persons(filter: $filter, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection) {
        ...PersonFragment
      }
      total: personsCount(filter: $filter)
    }
    ${PersonFragments.person}
  `,
};

const PersonMutations: BaseEntityGraphqlMutations = {
  saveAll: gql`
    mutation savePersons($data: [PersonVOInput]) {
      data: savePersons(persons: $data) {
        ...PersonFragment
      }
    }
    ${PersonFragments.person}
  `,
  deleteAll: gql`
    mutation deletePersons($ids: [Int]) {
      deletePersons(ids: $ids)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class PersonService extends BaseEntityService<Person, PersonFilter> implements SuggestService<Person, PersonFilter> {
  constructor(
    protected graphql: GraphqlService,
    protected platform: PlatformService,
    protected network: NetworkService,
    protected entities: EntitiesStorage
  ) {
    super(graphql, platform, Person, PersonFilter, {
      queries: PersonQueries,
      mutations: PersonMutations,
      defaultSortBy: 'lastName',
      defaultSortDirection: 'asc',
    });

    // for DEV only -----
    this._debug = !environment.production;
  }

  async loadAll(
    offset: number,
    size: number,
    sortBy?: keyof Person | string,
    sortDirection?: SortDirection,
    filter?: Partial<PersonFilter>,
    opts?: EntitiesServiceLoadOptions & {
      debug?: boolean;
    }
  ): Promise<LoadResult<Person>> {
    const offline = this.network.offline && (!opts || opts.fetchPolicy !== 'network-only');
    if (offline) {
      return this.loadAllLocally(offset, size, sortBy, sortDirection, filter, opts);
    }

    return super.loadAll(offset, size, sortBy, sortDirection, filter, opts);
  }

  async loadAllLocally(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<PersonFilter>,
    opts?: EntitiesServiceLoadOptions & {
      debug?: boolean;
    }
  ): Promise<LoadResult<Person>> {
    filter = this.asFilter(filter);

    const variables = {
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || this.defaultSortBy,
      sortDirection: sortDirection || this.defaultSortDirection,
      filter: filter && filter.asFilterFn(),
    };

    const { data, total } = await this.entities.loadAll<Person>('PersonVO', variables);

    const entities = this.fromObjects(data, opts);

    const res: LoadResult<Person> = { data: entities, total };

    // Add fetch more function
    const nextOffset = (offset || 0) + entities.length;
    if (nextOffset < total) {
      res.fetchMore = () => this.loadAllLocally(nextOffset, size, sortBy, sortDirection, filter, opts);
    }

    return res;
  }

  async suggest(
    value: any,
    filter?: Partial<PersonFilter>,
    sortBy?: keyof Person | string,
    sortDirection?: SortDirection,
    opts?: EntitiesServiceLoadOptions
  ): Promise<LoadResult<Person>> {
    if (EntityUtils.isNotEmpty(value, 'id')) return { data: [value] };
    value = (typeof value === 'string' && value !== '*' && value) || undefined;
    sortBy = sortBy || filter.searchAttribute || (filter.searchAttributes && filter.searchAttributes[0]);
    return this.loadAll(
      0,
      !value ? 30 : 10,
      sortBy,
      sortDirection,
      {
        ...filter,
        searchText: value as string,
        statusIds: (filter && filter.statusIds) || [StatusIds.ENABLE, StatusIds.TEMPORARY],
        userProfiles: filter && filter.userProfiles,
      },
      {
        withTotal: true /* need by autocomplete */,
        ...opts,
      }
    );
  }

  async executeImport(
    filter: Partial<PersonFilter>,
    opts: {
      progression?: BehaviorSubject<number>;
      maxProgression?: number;
    }
  ): Promise<void> {
    const maxProgression = (opts && opts.maxProgression) || 100;
    filter = {
      ...filter,
      statusIds: [StatusIds.ENABLE, StatusIds.TEMPORARY],
      userProfiles: <UserProfileLabel[]>['SUPERVISOR', 'USER', 'GUEST'],
    };

    console.info('[person-service] Importing persons...');

    const res = await JobUtils.fetchAllPages(
      (offset, size) =>
        this.loadAll(offset, size, 'id', null, filter, {
          debug: false,
          fetchPolicy: 'network-only',
          withTotal: offset === 0, // Compute total only once
          toEntity: false,
        }),
      { progression: opts?.progression, maxProgression: maxProgression * 0.9 }
    );

    // Save result locally
    await this.entities.saveAll(res.data, { entityName: 'PersonVO', reset: true });
  }

  async loadById(id: number, opts?: EntityServiceLoadOptions): Promise<Person> {
    const { data } = await this.loadAll(0, 1, null, null, { includedIds: [id] }, { withTotal: false, ...opts });
    const source = isNotEmptyArray(data) ? data[0] : { id };
    return this.fromObject(source, opts);
  }

  async loadByPubkey(pubkey: string, opts?: EntityServiceLoadOptions): Promise<Person> {
    const { data } = await this.loadAll(0, 1, null, null, { pubkey }, { withTotal: false, ...opts });
    const source = isNotEmptyArray(data) ? data[0] : { pubkey };
    return this.fromObject(source, opts);
  }

  /* -- protected methods -- */

  protected asObject(source: Person | any): any {
    if (!source) return undefined;

    if (!(source instanceof Person)) {
      source = Person.fromObject(source);
    }
    const target = source.asObject();

    // Not known in server GraphQL schema
    delete target.mainProfile;

    target.department = source.department?.asObject();

    return target;
  }
}
