import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { AdminUsersModule } from './users/users.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,

    // Sub modules
    AdminUsersModule,
  ],
  exports: [AdminUsersModule],
})
export class AdminModule {
  constructor() {
    console.debug('[admin] Creating module');
  }
}
