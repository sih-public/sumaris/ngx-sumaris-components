import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { UsersPage } from './users';
import { SocialModule } from '../../social/social.module';
import { NgxJdenticonModule } from 'ngx-jdenticon';
import { TranslateModule } from '@ngx-translate/core';
import { SharedMaterialModule } from '../../shared/material/material.module';
import { SharedDebugModule } from '../../shared/debug/debug.module';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    NgxJdenticonModule,
    TranslateModule.forChild(),

    // App modules
    CoreModule,
    SocialModule,
    SharedMaterialModule,
    SharedDebugModule,
    MatFormFieldModule,
  ],
  declarations: [UsersPage],
  exports: [
    SocialModule,
    TranslateModule,

    // Components
    UsersPage,
  ],
})
export class AdminUsersModule {}
