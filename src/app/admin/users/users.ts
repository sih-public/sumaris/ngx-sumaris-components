import { ChangeDetectionStrategy, Component, Inject, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { Person, PRIORITIZED_AUTHORITIES } from '../../core/services/model/person.model';
import { StatusById, StatusList } from '../../core/services/model/referential.model';
import { PersonService } from '../services/person.service';
import { PersonValidatorService } from '../services/validator/person.validator';
import { AccountService } from '../../core/services/account.service';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { AppTable } from '../../core/table/table.class';
import { ValidatorService } from '@e-is/ngx-material-table';
import { FormFieldDefinition } from '../../shared/form/field.model';
import { debounceTime, distinctUntilKeyChanged, filter, tap } from 'rxjs/operators';
import { EntitiesTableDataSource } from '../../core/table/entities-table-datasource.class';
import { isNotNil } from '../../shared/functions';
import { ENVIRONMENT } from '../../../environments/environment.class';
import { PersonFilter } from '../services/filter/person.filter';
import { ConfigService } from '../../core/services/config.service';
import { CORE_CONFIG_OPTIONS } from '../../core/services/config/core.config';
import { AuthTokenType } from '../../core/services/network.types';
import { MatExpansionPanel } from '@angular/material/expansion';
import { slideUpDownAnimation } from '../../shared/material/material.animations';
import { Message } from '../../social/message/message.model';
import { MessageService } from '../../social/message/message.service';
import { RESERVED_END_COLUMNS, RESERVED_START_COLUMNS, SETTINGS_FILTER } from '../../core/table/table.model';
import { MenuService } from '../../core/menu/menu.service';

@Component({
  selector: 'app-users-table',
  templateUrl: 'users.html',
  styleUrls: ['./users.scss'],
  providers: [{ provide: ValidatorService, useExisting: PersonValidatorService }],
  animations: [slideUpDownAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersPage extends AppTable<Person, PersonFilter> implements OnInit {
  readonly canEdit: boolean;
  readonly canSendMessage: boolean;
  readonly filterForm: UntypedFormGroup;
  readonly profiles = [...PRIORITIZED_AUTHORITIES].reverse();
  readonly statusList = StatusList;
  readonly statusById = StatusById;

  additionalFields: FormFieldDefinition[];
  filterCriteriaCount = 0;

  set showUsernameColumn(value: boolean) {
    this.setShowColumn('username', value);
  }

  set showUsernameExtranetColumn(value: boolean) {
    this.setShowColumn('usernameExtranet', value);
  }

  @Input() sticky = false;
  @Input() stickyEnd = false;

  constructor(
    injector: Injector,
    formBuilder: UntypedFormBuilder,
    protected accountService: AccountService,
    protected validatorService: ValidatorService,
    protected configService: ConfigService,
    protected dataService: PersonService,
    protected messageService: MessageService,
    protected menuService: MenuService,
    @Inject(ENVIRONMENT) environment
  ) {
    super(
      injector,
      RESERVED_START_COLUMNS.concat(['avatar', 'lastName', 'firstName', 'email', 'profile', 'status', 'username', 'usernameExtranet', 'pubkey'])
        .concat(accountService.additionalFields.map((field) => field.key))
        .concat(RESERVED_END_COLUMNS),
      new EntitiesTableDataSource(Person, dataService, validatorService, {
        prependNewElements: false,
        suppressErrors: environment.production,
        saveOnlyDirtyRows: true,
      }),
      null
    );

    this.canEdit = accountService.isAdmin();
    this.canSendMessage = this.canEdit;
    this.inlineEdition = this.canEdit;
    this.confirmBeforeDelete = true;
    this.autoLoad = false; // Wait config loading
    this.i18nColumnPrefix = 'USER.';
    this.defaultSortBy = 'lastName';
    this.defaultSortDirection = 'asc';

    this.filterForm = formBuilder.group({
      searchText: [null],
      statusId: [null],
    });

    this.additionalFields = (this.accountService.additionalFields || [])
      .filter((field) => isNotNil(field.autocomplete))
      .map((field) => {
        // Make sure to get the final autocomplete config (e.g. with a suggestFn function)
        field.autocomplete = this.registerAutocompleteField(field.key, {
          ...field.autocomplete, // Copy, to be sure the original config is unchanged
        });
        return field;
      });

    // Listen page action (e.g. from sub menu item)
    this.registerSubscription(
      this.route.queryParams.pipe(distinctUntilKeyChanged('action')).subscribe((queryParams) => {
        const action = queryParams['action'] || 'none';
        switch (action) {
          case 'send':
            this.openComposeMessageModal();
            break;
          case 'none':
            break;
          default:
            console.warn('[users] Unknown action=' + action);
            break;
        }
        // Mark as consumed
        this.router.navigate(['.'], {
          relativeTo: this.route,
          queryParams: { ...queryParams, action: undefined },
          replaceUrl: true,
          state: { animated: false },
        });
      })
    );

    // For DEV only --
    this.debug = !environment.production;
  }

  @ViewChild(MatExpansionPanel, { static: true }) filterExpansionPanel: MatExpansionPanel;

  get firstUserColumn(): string {
    const columnName = this.displayedColumns[RESERVED_START_COLUMNS.length];
    // Skip avatar column, if present
    return (columnName === 'avatar' && this.displayedColumns[RESERVED_START_COLUMNS.length + 1]) || columnName;
  }

  async ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.configService.config.subscribe((config) => {
        const authTokenType = config.getProperty(CORE_CONFIG_OPTIONS.AUTH_TOKEN_TYPE) as AuthTokenType;
        console.debug('[users] AuthTokenType=' + authTokenType);
        switch (authTokenType) {
          case 'basic':
            this.setShowColumn('pubkey', false, { emitEvent: false });
            this.setShowColumn('username', true, { emitEvent: false });
            this.setShowColumn('usernameExtranet', true, { emitEvent: false });
            break;
          case 'basic-and-token':
            this.setShowColumn('pubkey', true, { emitEvent: false });
            this.setShowColumn('username', true, { emitEvent: false });
            this.setShowColumn('usernameExtranet', true, { emitEvent: false });
            break;
          case 'token':
            this.setShowColumn('pubkey', true, { emitEvent: false });
            this.setShowColumn('username', false, { emitEvent: false });
            this.setShowColumn('usernameExtranet', false, { emitEvent: false });
        }
        this.updateColumns();
        this.emitRefresh();
      })
    );

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      })
    );

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter(() => this.filterForm.valid),
          tap((json) => {
            const filter = PersonFilter.fromObject(json);
            this.filterCriteriaCount = filter.countNotEmptyCriteria();
            this.markForCheck();
            // Update the filter, without reloading the content
            this.setFilter(filter, { emitEvent: false });
          }),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap((json) => this.settings.savePageSetting(this.settingsId, json, SETTINGS_FILTER))
        )
        .subscribe()
    );

    if (this.canSendMessage && this.debug) {
      await this.menuService.addSubMenuItems(
        [
          {
            parentPath: '/admin/users',
            path: '/admin/users',
            pathParams: { action: 'expand' },
            matIcon: 'expand_more',
            title: 'USER.LIST.BTN_ACTIONS',
            action: 'expand',
          },
          {
            parentPath: '/admin/users?action=expand',
            path: '/admin/users?action=send',
            icon: 'mail',
            title: 'USER.LIST.BTN_SEND_MESSAGE',
            action: () => this.openComposeMessageModal(),
          },
        ],
        { skipIfExists: false }
      );
    }

    // Restore filter from settings, or load all
    this.restoreFilterOrLoad();
  }

  applyFilterAndClosePanel(event?: Event) {
    this.emitRefresh(event);
    this.filterExpansionPanel.close();
  }

  resetFilter(event?: Event) {
    this.totalRowCount = undefined;
    this.filterCriteriaCount = 0;
    this.markAsLoading();
    this.filterForm.reset();
    this.setFilter(null);
  }

  protected async restoreFilterOrLoad() {
    this.markAsLoading();

    console.debug('[users] Restoring filter from settings...');

    const json = this.settings.getPageSettings(this.settingsId, SETTINGS_FILTER) || {};
    const filter = PersonFilter.fromObject(json);

    this.filterForm.patchValue(json);
    this.setFilter(filter, { emitEvent: true });
  }

  clearControlValue(event: Event, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  async openComposeMessageModal(event?: Event): Promise<any> {
    // Compute recipients
    const recipients = (this.selection.selected || [])
      .map((row) => row.currentData)
      .map(Person.fromObject)
      .map((p) => {
        const target = p.asObject({ minify: true });
        target.department = p.department?.asObject();
        return target;
      });

    const recipientFilter = PersonFilter.fromObject(this.filter);
    const canRecipientFilter = !recipientFilter.isEmpty() && (this.accountService.isAdmin() || this.accountService.isSupervisor());
    return this.messageService.openComposeModal({
      suggestFn: (value, filter, sortBy, sortDirection) => this.dataService.suggest(value, filter, sortBy, sortDirection),
      canRecipientFilter,
      recipientFilterCount: this.totalRowCount,
      data: Message.fromObject({
        recipients,
        recipientFilter,
      }),
    });
  }
}
