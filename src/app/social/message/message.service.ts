import { Inject, Injectable, Optional } from '@angular/core';
import { gql } from '@apollo/client/core';
import { SocialErrorCodes } from '../social.errors';
import { GraphqlService } from '../../core/graphql/graphql.service';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BaseGraphqlService } from '../../core/services/base-graphql-service.class';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { Message, MessageFilter } from './message.model';
import { MINIFY_ENTITY_FOR_POD } from '../../core/services/model/referential.model';
import { ShowToastOptions, Toasts } from '../../shared/toast/toasts';
import { MessageModal, MessageModalOptions } from './message.modal';

const Mutations = {
  send: gql`
    mutation SendMessage($data: MessageVOInput) {
      done: sendMessage(message: $data)
    }
  `,
};

@Injectable({ providedIn: 'root' })
export class MessageService extends BaseGraphqlService<Message, MessageFilter> {
  constructor(
    protected graphql: GraphqlService,
    protected translate: TranslateService,
    protected modalCtrl: ModalController,
    protected toastController: ToastController,
    @Optional() @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(graphql, environment);

    // For DEV only
    this._debug = !environment.production;
  }

  /**
   * Send a message to recipient(s)
   *
   * @param entity
   * @param opts
   */
  async send(entity: Message, opts?: { showToast?: boolean }): Promise<boolean> {
    // Transform into json
    const data = entity.asObject(MINIFY_ENTITY_FOR_POD);

    const now = Date.now();
    if (this._debug) console.debug(`[message-service] Sending message...`);

    try {
      const { done } = await this.graphql.mutate<{ done: boolean }>({
        mutation: Mutations.send,
        variables: { data },
        error: { code: SocialErrorCodes.SEND_MESSAGE_ERROR, message: 'SOCIAL.ERROR.SEND_MESSAGE_ERROR' },
      });

      if (this._debug) console.debug(`[message-service] Send message [OK] in ${Date.now() - now}ms`);

      if (done && (!opts || opts.showToast !== false)) {
        await this.showToast({ type: 'info', message: 'SOCIAL.INFO.MESSAGE_SENT' });
      }
      return done;
    } catch (err) {
      const error = (err && err.message) || err;
      console.error(error);

      // Show error
      if (!opts || opts.showToast !== false) {
        const message = error || 'SOCIAL.ERROR.SEND_MESSAGE_ERROR';
        await this.showToast({ type: 'error', message });
      }
      return false;
    }
  }

  async openComposeModal(options: MessageModalOptions & { showToast?: boolean }): Promise<any> {
    const hasTopModal = !!(await this.modalCtrl.getTop());
    const modal = await this.modalCtrl.create({
      component: MessageModal,
      componentProps: options,
      cssClass: hasTopModal && 'stack-modal',
    });

    // Open the modal
    await modal.present();

    // On dismiss
    const { data } = await modal.onDidDismiss();
    if (!data || !(data instanceof Message)) return true; // CANCELLED

    // Send message
    return await this.send(data, { showToast: options?.showToast });
  }

  /* -- protected methods -- */

  protected async showToast(opts: ShowToastOptions) {
    return Toasts.show(this.toastController, this.translate, {
      type: 'info',
      message: 'SOCIAL.INFO.MESSAGE_SENT',
      ...opts,
    });
  }
}
