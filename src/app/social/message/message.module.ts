import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { MessageModal } from './message.modal';
import { MessageForm } from './message.form';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
  imports: [CommonModule, CoreModule, SharedModule, MatChipsModule],
  declarations: [MessageModal, MessageForm],
  exports: [MessageModal],
})
export class MessageModule {}
