import { EntityClass } from '../../core/services/model/entity.decorators';
import { Entity, EntityAsObjectOptions } from '../../core/services/model/entity.model';
import { StoreObject } from '@apollo/client/core';
import { EntityFilter } from '../../core/services/model/filter.model';
import { Person } from '../../core/services/model/person.model';
import { PersonFilter } from '../../admin/services/filter/person.filter';
import { FilterFn } from '../../shared/types';

export const MessageTypes = {
  INBOX_MESSAGE: 'INBOX_MESSAGE',
  EMAIL: 'EMAIL',
  FEED: 'FEED',
};
export const MessageTypeList = Object.freeze([
  {
    id: MessageTypes.INBOX_MESSAGE,
    icon: 'notifications',
    label: 'SOCIAL.MESSAGE.TYPE_ENUM.INBOX_MESSAGE',
  },
  {
    id: MessageTypes.EMAIL,
    icon: 'mail',
    label: 'SOCIAL.MESSAGE.TYPE_ENUM.EMAIL',
  },
  {
    id: MessageTypes.FEED,
    icon: 'paper',
    label: 'SOCIAL.MESSAGE.TYPE_ENUM.FEED',
  },
]);

// @dynamic
@EntityClass({ typename: 'MessageVO' })
export class Message extends Entity<Message> {
  static fromObject: (source: any, opts?: any) => Message;

  type: string;
  issuer: Person;
  recipients: Person[];
  recipientFilter: PersonFilter;
  subject: string;
  body: string;

  constructor() {
    super();
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.type = source.type;
    this.issuer = source.issuer && Person.fromObject(source.issuer);
    this.recipients = source.recipients && source.recipients.map(Person.fromObject);
    this.recipientFilter = (source.recipientFilter && PersonFilter.fromObject(source.recipientFilter)) || undefined;
    this.subject = source.subject;
    this.body = source.body;
  }

  asObject(opts?: EntityAsObjectOptions): StoreObject {
    const target = super.asObject(opts);
    target.recipients = this.recipients && this.recipients.map((p) => p.asObject(opts));
    target.recipientFilter = (this.recipientFilter && this.recipientFilter.asObject(opts)) || undefined;
    return target;
  }
}

// @dynamic
@EntityClass({ typename: 'MessageFilterVO' })
export class MessageFilter extends EntityFilter<MessageFilter, Message> {
  static fromObject: (source: any, opts?: any) => MessageFilter;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
  }

  protected buildFilter(): FilterFn<Message>[] {
    return super.buildFilter();
  }
}
