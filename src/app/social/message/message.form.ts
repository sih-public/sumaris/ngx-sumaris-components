import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Message, MessageTypeList, MessageTypes } from './message.model';
import { AppForm } from '../../core/form/form.class';
import { StatusIds } from '../../core/services/model/model.enum';
import { Person, PersonUtils } from '../../core/services/model/person.model';
import { SuggestFn } from '../../shared/services/entity-service.class';
import { PersonFilter } from '../../admin/services/filter/person.filter';
import { EntityUtils } from '../../core/services/model/entity.model';
import { changeCaseToUnderscore, isNotNil, toBoolean } from '../../shared/functions';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-message-form',
  templateUrl: './message.form.html',
  styleUrls: ['./message.form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageForm extends AppForm<Message> implements OnInit {
  readonly mobile: boolean;

  @Input() suggestFn: SuggestFn<Person, PersonFilter>;
  @Input() subjectMinLength = 5;
  @Input() subjectMaxLength = 255;
  @Input() bodyMaxLength = 2000;
  @Input() bodyAutoHeight = true;
  @Input() canSelectType = false;
  @Input() canRecipientFilter = false;
  @Input() recipientFilterCount = 0;

  types = MessageTypeList;

  constructor(
    injector: Injector,
    private formBuilder: UntypedFormBuilder,
    private readonly cd: ChangeDetectorRef
  ) {
    super(injector);
    this.mobile = this.settings.mobile;
  }

  ngOnInit() {
    this.setForm(
      this.formBuilder.group({
        type: [MessageTypes.INBOX_MESSAGE, Validators.required],
        recipients: [null, Validators.required],
        recipientFilter: [null],
        subject: [
          null,
          this.subjectMaxLength
            ? Validators.compose([Validators.required, Validators.minLength(this.subjectMinLength), Validators.maxLength(this.subjectMaxLength)])
            : Validators.required,
        ],
        body: [null, this.bodyMaxLength ? Validators.compose([Validators.maxLength(this.bodyMaxLength)]) : Validators.required],
      })
    );

    this.registerSubscription(
      this._form
        .get('type')
        .valueChanges.pipe(filter(isNotNil))
        .subscribe((type) => this.updateFormGroup(this._form, { type }))
    );

    // Person combo
    const personAttributes = this.settings.getFieldDisplayAttributes('person', ['lastName', 'firstName', 'department.name']);
    this.registerAutocompleteField('recipients', {
      showAllOnFocus: false,
      suggestFn: this.suggestFn,
      filter: {
        statusIds: [StatusIds.TEMPORARY, StatusIds.ENABLE],
      },
      attributes: personAttributes,
      columnNames: personAttributes.map((attr) => `USER.${changeCaseToUnderscore(attr).toUpperCase()}`),
      displayWith: PersonUtils.personToString,
      multiple: true,
      mobile: this.mobile,
    });
  }

  isSamePerson(o1: Person, o2: Person): boolean {
    return EntityUtils.equals(o1, o2, 'id');
  }

  protected updateFormGroup(formGroup: UntypedFormGroup, opts?: { type?: string; recipientRequired?: boolean }) {
    console.debug('[message-form] Updating form group...', opts);

    // Recipient validator
    const recipientsRequired = toBoolean(opts?.recipientRequired, opts?.type !== MessageTypes.FEED);
    {
      const control = formGroup.get('recipients');
      if (recipientsRequired) {
        if (!control.hasValidator(Validators.required)) {
          control.addValidators(Validators.required);
        }
        control.enable();
      } else {
        if (control.hasValidator(Validators.required)) {
          control.removeValidators(Validators.required);
        }
        control.disable();
      }
    }

    // Recipient filter validator
    const recipientFilterRequired = this.canRecipientFilter && !recipientsRequired;
    {
      const control = formGroup.get('recipientFilter');
      if (recipientFilterRequired) {
        if (!control.hasValidator(Validators.required)) {
          control.addValidators(Validators.required);
        }
        control.enable();
      } else {
        if (control.hasValidator(Validators.required)) {
          control.removeValidators(Validators.required);
        }
        control.disable();
      }
    }

    formGroup.updateValueAndValidity();
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
