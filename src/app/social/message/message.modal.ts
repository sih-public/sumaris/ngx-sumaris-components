import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { Message, MessageTypes } from './message.model';
import { Person } from '../../core/services/model/person.model';
import { SuggestFn } from '../../shared/services/entity-service.class';
import { PersonFilter } from '../../admin/services/filter/person.filter';
import { LocalSettingsService } from '../../core/services/local-settings.service';
import { MessageForm } from './message.form';
import { firstNotNilPromise } from '../../shared/observables';
import { AppFormUtils } from '../../core/form/form.utils';
import { toBoolean } from '../../shared/functions';
import { AccountService } from '../../core/services/account.service';

export interface MessageModalOptions {
  suggestFn: SuggestFn<Person, PersonFilter>;
  data?: Message;
  canSelectType?: boolean;
  canRecipientFilter?: boolean;
  recipientFilterCount?: number;
}

@Component({
  selector: 'app-message-modal',
  templateUrl: './message.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageModal implements OnInit, AfterViewInit, MessageModalOptions {
  readonly mobile: boolean;
  @Input() suggestFn: SuggestFn<Person, PersonFilter>;
  @Input() data: Message;
  @Input() canSelectType: boolean;
  @Input() canRecipientFilter: boolean;
  @Input() recipientFilterCount: number;

  @ViewChild('form', { static: true }) private form: MessageForm;

  constructor(
    protected settings: LocalSettingsService,
    protected viewCtrl: ModalController,
    protected accountService: AccountService,
    protected cd: ChangeDetectorRef
  ) {
    this.mobile = this.settings.mobile;
  }

  ngOnInit() {
    this.canSelectType = toBoolean(this.canSelectType, this.accountService.isAdmin());
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.form.markAsReady({ emitEvent: false });
      if (this.data) {
        this.data.type = this.data.type || MessageTypes.INBOX_MESSAGE;
        this.form.setValue(this.data);
      }
      this.form.markAsLoaded();
      this.form.enable();
    });
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  async doSubmit(): Promise<any> {
    if (this.form.disabled) return;
    if (!this.form.valid) {
      await AppFormUtils.waitWhilePending(this.form);

      if (this.form.invalid) {
        AppFormUtils.logFormErrors(this.form.form, '[message-modal] ');
        this.form.markAllAsTouched();
        return;
      }
    }
    this.markAsLoading();

    try {
      const data = this.form.value;

      // Disable the form
      this.form.disable();

      const entity = Message.fromObject(data);

      return this.viewCtrl.dismiss(entity);
    } catch (err) {
      this.form.error = (err && err.message) || err;
      this.markAsLoaded();

      // Enable the form
      this.form.enable();

      // Reset form error on next changes
      firstNotNilPromise(this.form.form.valueChanges).then(() => {
        this.form.error = null;
        this.markForCheck();
      });

      return;
    }
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected markAsLoading(opts?: { emitEvent?: boolean }) {
    this.form.markAsLoading(opts);
  }

  protected markAsLoaded(opts?: { emitEvent?: boolean }) {
    this.form.markAsLoaded(opts);
  }
}
