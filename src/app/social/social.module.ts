import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { UserEventModule } from './user-event/user-event.module';
import { MessageModule } from './message/message.module';
import { JobModule } from './job/job.module';
import { JobProgressionOptions } from './job/progression/job-progression.icon';

export interface SocialModuleOptions {
  showUserEventNotification: boolean;
  // todo : userEventNotificationOptions
  showJobProgression: boolean;
  jobProgressionOptions: JobProgressionOptions;
}

export const SocialModuleOptionsToken = new InjectionToken<SocialModuleOptions>('SocialModuleOptions');

@NgModule({
  imports: [CommonModule, CoreModule, SharedModule, UserEventModule, JobModule, MessageModule],
  exports: [UserEventModule, JobModule, MessageModule],
})
export class SocialModule {}
