import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  InjectionToken,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { IUserEvent, IUserEventFilter, IUserEventAction } from '../user-event.model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { IonContent, IonItem, PopoverController } from '@ionic/angular';
import { IUserEventService, APP_USER_EVENT_SERVICE } from '../user-event.service';
import { LocalSettingsService } from '../../../core/services/local-settings.service';
import { FetchMoreFn, LoadResult } from '../../../shared/services/entity-service.class';
import { isNotEmptyArray, removeDuplicatesFromArray } from '../../../shared/functions';
import { fromScrollEndEvent } from '../../../shared/events';
import { waitFor } from '../../../shared/observables';
import { SortDirection } from '@angular/material/sort';
import { debounceTime, distinctUntilChanged, filter, first, switchMap } from 'rxjs/operators';

export const APP_USER_EVENT_LIST_INFINITE_SCROLL_THRESHOLD = new InjectionToken<string>('infiniteScrollThreshold');

export interface UserEventNotificationListOptions<
  E extends IUserEvent<any> = IUserEvent<any>,
  F extends IUserEventFilter<any> = IUserEventFilter<any>,
> {
  debug?: boolean;
  titleI18n?: string;
  readEvent?: EventEmitter<E>;
  readEvents?: EventEmitter<E[]>;
  filter?: Partial<F>;
  headerActions?: IUserEventAction[];
  footerActions?: IUserEventAction[];
}

@Component({
  selector: 'app-user-event-notification-list',
  templateUrl: './user-event-notification.list.html',
  styleUrls: ['./user-event-notification.list.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEventNotificationList<E extends IUserEvent<any> = IUserEvent<any>, F extends IUserEventFilter<any> = IUserEventFilter<any>>
  implements OnInit, OnDestroy, UserEventNotificationListOptions<E, F>
{
  @Input() debug = false;
  @Input() mobile: boolean;
  @Input() titleI18n: string;
  @Input() sortBy = 'creationDate';
  @Input() sortDirection: SortDirection = 'desc';
  @Input() filter: Partial<F>;
  @Input() enableInfiniteScroll = true;
  @Input() infiniteScrollThreshold: string;
  @Input() headerActions: IUserEventAction[];
  @Input() footerActions: IUserEventAction[];

  @Input() set pageSize(value: number) {
    if (value > 0 && value !== this._$pageSize.value) this._$pageSize.next(value);
  }

  get pageSize(): number {
    return this._$pageSize.value;
  }

  @Output() readEvent: EventEmitter<E>;
  @Output() readEvents: EventEmitter<E[]>;

  @ViewChild(IonContent) ionContent: IonContent;
  @ViewChildren(IonItem) ionItems: QueryList<IonItem>;

  get canFetchMore(): boolean {
    return this.enableInfiniteScroll && !!this._fetchMoreFn;
  }

  fetchingMore = false;
  $items = new BehaviorSubject<E[]>(undefined);
  $itemCount = new BehaviorSubject<number>(undefined);

  protected _subscriptions = new Subscription();
  protected _fetchMoreFn: FetchMoreFn<LoadResult<E>>;
  protected _fetchMoreSubscription: Subscription;
  protected _$pageSize = new BehaviorSubject<number>(20);
  protected _panelElement: HTMLElement = null;

  constructor(
    protected cd: ChangeDetectorRef,
    protected popoverController: PopoverController,
    @Optional() protected settings: LocalSettingsService,
    @Optional() @Inject(APP_USER_EVENT_SERVICE) public userEventService: IUserEventService<E, IUserEventFilter<any>>,
    @Optional() @Inject(APP_USER_EVENT_LIST_INFINITE_SCROLL_THRESHOLD) public infiniteScrollThresholdFromToken: string
  ) {
    this.mobile = this.settings?.mobile || false;
    this.infiniteScrollThreshold = infiniteScrollThresholdFromToken;
  }

  ngOnInit(): void {
    // Watch all user events
    if (this.userEventService) {
      this._subscriptions.add(
        this._$pageSize
          .pipe(
            distinctUntilChanged(),
            switchMap((pageSize) => this.userEventService.watchAll(0, pageSize, this.sortBy, this.sortDirection, this.filter)),
            first()
          )
          .subscribe((result) => {
            if (this.debug) console.debug(`[user-event-notification-list] Receiving ${result.total} user events`, result.data);
            this.$items.next(result.data);
            this.$itemCount.next(result.total || result.data?.length || 0);
            this._fetchMoreFn = result.fetchMore;
            this.fetchingMore = false; // Allow to refetch more
            this.markForCheck();
          })
      );

      // Listen changes
      this._subscriptions.add(
        this.userEventService
          .listenAllChanges(this.filter)
          .pipe(filter(isNotEmptyArray))
          .subscribe((data) => {
            if (this.debug) console.debug(`[user-event-notification-list] Receiving ${data.length} new user events (by WS)`, data);
            const oldItemsCount = this.$itemCount.value || 0;
            const items = removeDuplicatesFromArray([...data, ...(this.$items.value || [])], 'id');

            // Compute delta count
            // When an existing event is update, the delta count should be = 0
            const deltaCount = items.length - oldItemsCount;

            // Update observables
            this.$items.next(items);
            if (deltaCount !== 0) this.$itemCount.next(oldItemsCount + deltaCount);
          })
      );

      if (this.enableInfiniteScroll) this.initInfiniteScroll(this.infiniteScrollThreshold);
    }
  }

  ngOnDestroy() {
    this._subscriptions.unsubscribe();
    this.$items.complete();
    this.$itemCount.complete();
  }

  click(event: Event, userEvent: E) {
    if (!userEvent) return; // Skip

    // MarkAsRead
    this.markAsRead(event, userEvent);

    // Get the default action (if any)
    const defaultAction = (userEvent?.actions || []).find((a) => a.default);

    if (defaultAction) {
      this.executeAction(event, defaultAction, userEvent);
    }
  }

  isDefaultAction(action: IUserEventAction): boolean {
    return (action && action.default) || false;
  }

  markAsRead(event: Event, userEvent: E) {
    if (userEvent?.readDate) return;
    this.readEvent?.emit(userEvent);
  }

  markAllAsRead(event: Event) {
    const unreadUserEvents = (this.$items.value || []).filter((value) => !value.readDate);
    if (!unreadUserEvents.length) return;
    this.readEvents?.emit(unreadUserEvents);
  }

  async executeAction(event: Event, action: IUserEventAction, userEvent: IUserEvent<any>) {
    if (event?.defaultPrevented) return; // Skip if prevented

    // Disable default action (.e.g avoid to call click() function)
    if (event) event.preventDefault();

    // Execute then close popover
    try {
      const res = action.executeAction(userEvent);
      this.dismiss();
      if (res instanceof Promise) await res;
    } catch (err) {
      console.error(err);
    }
  }

  dismiss() {
    this.popoverController.dismiss();
    // Reset counter (need if event has been received while popover if open)
    this.userEventService.resetCount();
  }

  async fetchMore(event?: CustomEvent & { target?: EventTarget & { complete?: () => void } }) {
    if (!this._fetchMoreFn || this.fetchingMore) return;

    console.debug('[user-event-notification-list] Fetching more notifications...', event);
    this.fetchingMore = true;
    const fetchMoreFn = this._fetchMoreFn;

    try {
      const result = await fetchMoreFn();
      let data = [...(this.$items.value || []), ...result.data];

      // Remove duplication (e.g. when new user event was added by subscription)
      data = removeDuplicatesFromArray(data, 'id');

      // fetch more changes (e.g. if watchAll observable as been refreshed)
      if (fetchMoreFn !== this._fetchMoreFn) return; // Skip

      this.$items.next(data);
      this.$itemCount.next(result.total || result.data?.length || 0);
      this._fetchMoreFn = result.fetchMore;

      // Update the page size, to keep same scroll height, after the next refresh
      //this.pageSize = Math.max(this.pageSize, data.length);
    } catch (err) {
      console.error(err);
      this._fetchMoreFn = undefined; // Forget the function
    } finally {
      this.markForCheck();
      this.fetchingMore = false;
    }
  }

  trackByFn(index: number, item: E) {
    return item.id;
  }

  protected async initInfiniteScroll(threshold?: string) {
    console.debug('[user-event-notification-list] Init infinite loop');
    if (this._fetchMoreSubscription) this._fetchMoreSubscription.unsubscribe();

    threshold = threshold || (this.mobile ? '15%' : '2%');
    await waitFor(() => !!this.ionContent, { dueTime: 250, timeout: 1000 });
    const panel = await this.getPanelElement();

    // Trigger fetch more, end end scroll reached
    this._fetchMoreSubscription = this.$items
      .pipe(
        debounceTime(250),
        filter(() => this.canFetchMore),
        switchMap(() => fromScrollEndEvent(panel, threshold, 250 /*force to check when no scrollbar - e.g. in large screen*/))
      )
      .subscribe(() => this.fetchMore());

    this._subscriptions.add(this._fetchMoreSubscription);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected async getPanelElement(): Promise<HTMLElement> {
    if (!this._panelElement) {
      this._panelElement = await this.ionContent.getScrollElement();
    }
    return this._panelElement;
  }
}
