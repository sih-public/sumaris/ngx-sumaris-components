import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Optional } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PopoverController } from '@ionic/angular';
import { APP_USER_EVENT_SERVICE, IUserEventService } from '../user-event.service';
import { UserEventNotificationList, UserEventNotificationListOptions } from './user-event-notification.list';
import { IUserEvent, IUserEventAction, IUserEventFilter } from '../user-event.model';
import { AccountService } from '../../../core/services/account.service';
import { map, tap } from 'rxjs/operators';
import { isNil, toBoolean } from '../../../shared/functions';

@Component({
  selector: 'app-user-event-notification-icon',
  templateUrl: './user-event-notification.icon.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEventNotificationIcon implements OnInit, OnDestroy {
  private _logPrefix = '[user-event-notification] ';
  private _readEvent = new EventEmitter<IUserEvent<any>>();
  private _readEvents = new EventEmitter<IUserEvent<any>[]>();

  @Input() debug = false;
  @Input() titleI18n = 'SOCIAL.USER_EVENT.NOTIFICATION.TITLE';
  @Input() disabled = false;
  @Input() filter: any = null;
  @Input() autoHide: boolean = false;
  @Input() headerActions: IUserEventAction[];
  @Input() footerActions: IUserEventAction[];

  protected visible: boolean;
  protected readonly countChanges$: Observable<number>;

  constructor(
    @Optional() @Inject(APP_USER_EVENT_SERVICE) public userEventService: IUserEventService<IUserEvent<any>, IUserEventFilter<any>>,
    protected accountService: AccountService,
    protected popoverController: PopoverController,
    private cd: ChangeDetectorRef
  ) {
    this.countChanges$ = !this.userEventService
      ? of()
      : this.userEventService.countSubject.pipe(
          tap((value) => {
            const visible = !this.autoHide || value > 0;
            this.setVisible(visible);
          }),
          map((value) => (value === 0 ? undefined : value))
          // FIXME: enable if zone lesse in timer
          //tap(_ => this.cd.detectChanges())
        );
  }

  async ngOnInit() {
    if (isNil(this.userEventService)) {
      console.warn(`${this._logPrefix}No service injected`);
      this.disabled = true;
      this.setVisible(false);
      return;
    }

    this.setVisible(toBoolean(this.visible, !this.autoHide));

    // Wait service
    await this.userEventService.ready();

    // Subscribe to read event
    this._readEvent.subscribe((item) => {
      // default: mark event as read
      this.userEventService.markAllAsRead([item]);
    });
    this._readEvents.subscribe((items) => {
      this.userEventService.markAllAsRead(items);
    });
  }

  ngOnDestroy(): void {
    if (this.debug) {
      console.debug(`${this._logPrefix}Destroying`);
    }
    this._readEvent.complete();
    this._readEvents.complete();
  }

  async showList(event: Event) {
    // Reset count
    this.userEventService.resetCount();

    const popover = await this.popoverController.create({
      component: UserEventNotificationList,
      componentProps: <UserEventNotificationListOptions>{
        debug: this.debug,
        titleI18n: this.titleI18n,
        readEvent: this._readEvent,
        readEvents: this._readEvents,
        filter: this.filter,
        headerActions: this.headerActions,
        footerActions: this.footerActions,
      },
      backdropDismiss: true,
      keyboardClose: true,
      event,
      translucent: true,
      cssClass: 'popover-large popover-notifications',
    });
    await popover.present();
  }

  protected setVisible(value: boolean) {
    if (this.visible !== value) {
      this.visible = value;
      this.cd.markForCheck();
    }
  }
}
