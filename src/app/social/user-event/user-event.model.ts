import { Moment } from 'moment';
import { Entity, IEntity } from '../../core/services/model/entity.model';
import { IEntityFilter } from '../../core/services/model/filter.model';
import { AppColors, IconRef } from '../../shared/types';

export type EventLevel = 'ERROR' | 'WARNING' | 'INFO' | 'DEBUG';

export interface IUserEvent<E extends Entity<E, ID>, ID = number, U = string, L = EventLevel> extends IEntity<E, ID> {
  issuer: U;
  recipient: U;
  type: string;
  level: L;

  updateDate: Moment;
  creationDate: Moment;
  readDate?: Moment;

  message?: string;
  content?: string | any;

  avatar?: string;
  avatarIcon?: IconRef;
  avatarJdenticon?: any;
  icon?: IconRef;

  jobId?: number;

  actions?: IUserEventAction[];
}

export interface IUserEventFilter<E extends IUserEvent<E, ID, U, L>, ID = number, U = string, L = EventLevel> extends IEntityFilter<any, E> {
  types: string[];
  levels: L[];
  issuers: U[];
  recipients: U[];
  startDate: Moment;
  includedIds: ID[];
  excludeRead: boolean;
  jobId?: number;
}

export interface IUserEventAction {
  name: string | any;
  title?: string;
  color?: AppColors;
  iconRef?: IconRef;
  default?: boolean;

  executeAction: (event: IUserEvent<any>) => any | Promise<any>;
}

/**
 * @deprecated use IUserEventAction instead
 */
export type UserEventAction = IUserEventAction;
