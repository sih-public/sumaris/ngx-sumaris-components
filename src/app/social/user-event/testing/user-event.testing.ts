import { Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { APP_USER_EVENT_SERVICE } from '../user-event.service';
import { IUserEventAction } from '../user-event.model';
import { Router } from '@angular/router';
import { Alerts } from '../../../shared/alerts';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { UserEventTestService } from './user-event.testing.service';
import { UserEventNotificationIcon } from '../notification/user-event-notification.icon';
import { Subscription, timer } from 'rxjs';
import { UserEventTest } from './user-event.testing.model';
import { DateUtils } from '../../../shared/dates';

@Component({
  selector: 'job-progression-testing',
  templateUrl: 'user-event.testing.html',
})
export class UserEventTestingPage implements OnDestroy {
  private _timerSubscription: Subscription;

  @ViewChild('notification') notification: UserEventNotificationIcon;

  get timerStarted(): boolean {
    return !!this._timerSubscription;
  }

  constructor(
    @Inject(APP_USER_EVENT_SERVICE) protected userEventService: UserEventTestService,
    protected router: Router,
    protected alertController: AlertController,
    protected translateService: TranslateService
  ) {
    userEventService.registerListener({
      accept: (_: UserEventTest) => true,
      onReceived: (userEvent: UserEventTest) => {
        this.customize(userEvent);
        return userEvent;
      },
    });
    userEventService.registerListener({
      accept: (userEvent: UserEventTest) => userEvent.level === 'DEBUG',
      onRead: (_: UserEventTest) => {
        this.showMessage('debug message read ');
      },
    });
  }

  ngOnDestroy() {
    this.stopTimer();
  }

  protected customize(userEvent: UserEventTest) {
    // Choose avatarIcon
    switch (userEvent.level) {
      case 'ERROR':
        userEvent.avatarIcon = { icon: 'close-outline', color: 'danger' };
        break;
      case 'WARNING':
        userEvent.avatarIcon = { icon: 'warning-outline', color: 'warning' };
        break;
      case 'INFO':
        userEvent.avatarIcon = { icon: 'information-circle-outline' };
        break;
      case 'DEBUG':
        userEvent.avatarIcon = { icon: 'cog', color: 'medium' };
        break;
    }

    // Choose icon
    switch (userEvent.type) {
      case 'message':
        userEvent.icon = { matIcon: 'inbox', color: 'accent' };
        break;
      case 'upload':
        userEvent.icon = { matIcon: 'file_upload' };
        break;
      case 'download':
        userEvent.icon = { matIcon: 'file_download' };
        break;
    }
  }

  addNotification() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'message',
        level: 'INFO',
        message: 'message test message test message test message test message test message test message test message test',
        creationDate: DateUtils.moment(),
      })
    );
  }

  addError() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'upload',
        level: 'ERROR',
        message: 'message error',
        creationDate: DateUtils.moment(),
      })
    );
  }

  addWarning() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'download',
        level: 'WARNING',
        message: 'message warning',
        creationDate: DateUtils.moment(),
      })
    );
  }

  addDebug() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'debug',
        level: 'DEBUG',
        message: 'message DEBUG',
        creationDate: DateUtils.moment(),
      })
    );
  }

  addNotificationWithLink() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'message',
        level: 'INFO',
        message: 'message with link',
        creationDate: DateUtils.moment(),
        actions: [
          <IUserEventAction>{
            name: 'click here',
            iconRef: {
              icon: 'arrow-forward-outline',
            },
            executeAction: (_) => {
              this.router.navigateByUrl('/');
            },
          },
        ],
      })
    );
  }

  addNotificationWithAction() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'message',
        level: 'INFO',
        message: 'message with action',
        creationDate: DateUtils.moment(),
        actions: [
          <IUserEventAction>{
            name: 'click here',
            iconRef: {
              icon: 'arrow-forward-outline',
              color: 'warning',
            },
            executeAction: (_) => {
              this.showMessage('action 1');
            },
          },
          <IUserEventAction>{
            name: 'click here 2',
            iconRef: {
              matIcon: 'settings_applications',
              color: 'secondary',
            },
            executeAction: (_) => {
              this.showMessage('action 2');
            },
          },
        ],
      })
    );
  }

  addMany(count?: number) {
    count = count || 20;
    for (let i = 0; i < count; i++) {
      this.addRandom(i);
    }
  }

  startTimer(period?: number) {
    this._timerSubscription?.unsubscribe();

    period = period || 1000;
    let counter = 0;

    this._timerSubscription = timer(period, period).subscribe(() => {
      this.addRandom(counter);
      counter++;
    });
  }

  stopTimer() {
    this._timerSubscription?.unsubscribe();
    this._timerSubscription = null;
  }

  addRandom(value: number) {
    switch (value % 7) {
      case 0:
        this.addNotification();
        break;
      case 1:
        this.addError();
        break;
      case 2:
        this.addDebug();
        break;
      case 3:
        this.addNotificationWithLink();
        break;
      case 4:
        this.addNotificationWithAction();
        break;
      case 5:
        this.addNotificationWithDefaultAction();
        break;
      case 6:
        this.replaceLastEvent();
        break;
    }
  }

  addNotificationWithDefaultAction() {
    this.userEventService.add(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'message',
        level: 'INFO',
        message: 'message with action',
        creationDate: DateUtils.moment(),
        actions: [
          <IUserEventAction>{
            default: true,
            name: 'default',
            executeAction: (_) => {
              this.showMessage('default action');
            },
          },
        ],
      })
    );
  }

  protected showMessage(text: string): Promise<void> {
    return Alerts.showError(text, this.alertController, this.translateService);
  }

  replaceLastEvent() {
    this.userEventService.replaceLast(
      UserEventTest.fromObject({
        issuer: 'system',
        recipient: 'me',
        type: 'message',
        level: 'INFO',
        message: 'REPLACED',
        creationDate: DateUtils.moment(),
      })
    );
  }
}
