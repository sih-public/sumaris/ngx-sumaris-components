import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { UserEventModule } from '../user-event.module';
import { UserEventTestingPage } from './user-event.testing';

@NgModule({
  imports: [CommonModule, TranslateModule.forChild(), SharedModule, UserEventModule],
  declarations: [UserEventTestingPage],
  exports: [UserEventTestingPage],
})
export class UserEventTestingModule {}
