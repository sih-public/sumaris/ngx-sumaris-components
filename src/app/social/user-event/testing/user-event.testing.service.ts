import { Inject, Injectable } from '@angular/core';
import { AbstractUserEventService, UserEventLoadOptions, UserEventWatchOptions } from '../user-event.service';
import { BehaviorSubject, firstValueFrom, Observable, Subject } from 'rxjs';
import { GraphqlService } from '../../../core/graphql/graphql.service';
import { AccountService } from '../../../core/services/account.service';
import { NetworkService } from '../../../core/services/network.service';
import { TranslateService } from '@ngx-translate/core';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';
import { FetchPolicy } from '@apollo/client/core';
import { LoadResult } from '../../../shared/services/entity-service.class';
import { SortDirection } from '@angular/material/sort';
import { EntityUtils } from '../../../core/services/model/entity.model';
import { map } from 'rxjs/operators';
import { UserEventTest, UserEventTestFilter } from './user-event.testing.model';
import { EntitySaveOptions } from '../../../core/services/base-entity-service.class';

@Injectable()
export class UserEventTestService extends AbstractUserEventService<UserEventTest, UserEventTestFilter> {
  private $userEvents = new BehaviorSubject<UserEventTest[]>([]);
  private $newUserEvents = new Subject<UserEventTest[]>();

  constructor(
    protected graphql: GraphqlService,
    protected accountService: AccountService,
    protected network: NetworkService,
    protected translate: TranslateService,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(graphql, accountService, network, translate, {
      queries: {
        loadAll: null,
        loadAllWithContent: null,
      },
      production: environment?.production,
    });
  }

  fromObject(source: any): UserEventTest {
    return UserEventTest.fromObject(source);
  }

  async add(entity: UserEventTest): Promise<UserEventTest> {
    entity = await this.processUserEvent(entity);
    entity.id = (this.$userEvents.value?.length || 0) + 1;
    this.$userEvents.next([entity, ...this.$userEvents.value]);
    this._countSubject.next((this._countSubject.value || 0) + 1);
    this.$newUserEvents.next([entity]);
    return entity;
  }

  async replaceLast(entity: UserEventTest) {
    entity = await this.processUserEvent(entity);
    const events = this.$userEvents.value.slice();
    const last = events.shift();
    entity.id = last.id;
    events.unshift(entity);
    this.$userEvents.next(events);
    this.$newUserEvents.next([entity]);
  }

  asFilter(filter: Partial<any>): any {
    return filter;
  }

  count(filter: any, options?: { fetchPolicy?: FetchPolicy }): Promise<number> {
    return Promise.resolve(this.$userEvents.value?.length || 0);
  }

  delete(entity: any): Promise<any> {
    return Promise.resolve(undefined);
  }

  deleteAll(data: any[], opts?: any): Promise<any> {
    return Promise.resolve(undefined);
  }

  listenAllChanges(filter: any, options?: UserEventLoadOptions<UserEventTest> & { interval?: number }): Observable<any[]> {
    return this.$newUserEvents.asObservable();
  }

  save(entity: any, options?: EntitySaveOptions<UserEventTest>): Promise<any> {
    const toSave = this.$userEvents.value?.find((value) => value.id == entity?.id);
    Object.assign(toSave, entity);

    return entity;
  }

  async saveAll(data: any[], opts?: any): Promise<any[]> {
    for (const entity of data || []) {
      await this.save(entity);
    }

    return firstValueFrom(this.$userEvents);
  }

  protected defaultRecipient(): any {
    return this.accountService.isLogin() ? this.accountService.person.pubkey : undefined;
  }

  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<any>,
    opts?: UserEventWatchOptions<UserEventTest>
  ): Observable<LoadResult<any>> {
    return this.$userEvents.pipe(
      map((userEvents) => {
        let data = (userEvents || []).slice(offset || 0, offset + size);
        const total = this.$userEvents.value?.length || 0;

        const res = <LoadResult<UserEventTest>>{ data, total };

        // Must re-order because of listenChanges result will add new event at the end
        data = EntityUtils.sort(data, sortBy, sortDirection);

        // Add fetch more
        const nextOffset = offset + data.length;
        if (nextOffset < res.total) {
          res.fetchMore = (_) =>
            this.loadAll(nextOffset, size, sortBy, sortDirection, filter, {
              ...opts,
              fetchPolicy: <FetchPolicy>'no-cache',
            } as unknown as UserEventLoadOptions<UserEventTest>);
        }

        return res;
      })
    );
  }

  loadAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<any>,
    opts?: UserEventLoadOptions<UserEventTest>
  ): Promise<LoadResult<any>> {
    let data = (this.$userEvents.value || []).slice(offset || 0, offset + size);
    const total = this.$userEvents.value?.length || 0;

    const res = <LoadResult<UserEventTest>>{ data, total };

    // Must re-order because of listenChanges result will add new event at the end
    data = EntityUtils.sort(data, sortBy, sortDirection);

    // Add fetch more
    const nextOffset = offset + data.length;
    if (nextOffset < res.total) {
      res.fetchMore = (_) =>
        this.loadAll(nextOffset, size, sortBy, sortDirection, filter, {
          ...opts,
          fetchPolicy: <FetchPolicy>'no-cache',
        } as unknown as UserEventLoadOptions<UserEventTest>);
    }

    return Promise.resolve(res);
  }
}
