// @dynamic
import { EntityClass } from '../../../core/services/model/entity.decorators';
import { Entity, EntityAsObjectOptions } from '../../../core/services/model/entity.model';
import { Moment } from 'moment';
import { FilterFn, IconRef } from '../../../shared/types';
import { fromDateISOString, toDateISOString } from '../../../shared/dates';
import { EventLevel, IUserEvent, IUserEventAction, IUserEventFilter } from '../user-event.model';
import { EntityFilter } from '../../../core/services/model/filter.model';
import { isNil, isNotEmptyArray, isNotNil } from '../../../shared/functions';
import { StoreObject } from '@apollo/client/core';

// @dynamic
@EntityClass({ typename: 'UserEventTestVO' })
export class UserEventTest extends Entity<UserEventTest> implements IUserEvent<UserEventTest> {
  static fromObject: (source: any, opts?: any) => UserEventTest;

  type: string;
  level: EventLevel;
  issuer: string;
  recipient: string;
  creationDate: Moment;
  readDate: Moment;

  message: string | undefined;
  content: string | any;
  hash: string;
  signature: string;
  readSignature: string;

  avatar?: string;
  avatarIcon?: IconRef;
  icon?: IconRef;
  jobId?: number;

  actions?: IUserEventAction[];

  constructor() {
    super(UserEventTest.TYPENAME);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.creationDate = toDateISOString(this.creationDate);
    target.readDate = toDateISOString(this.readDate);

    // Serialize content
    if (typeof target.content === 'object') {
      target.content = JSON.stringify(target.content);
    }

    delete target.avatar;
    delete target.avatarIcon;
    delete target.icon;
    delete target.actions;

    return target;
  }

  fromObject(source: any) {
    Object.assign(this, source); // Copy all properties
    super.fromObject(source);
    this.creationDate = fromDateISOString(source.creationDate);
    this.readDate = fromDateISOString(source.readDate);

    try {
      // Deserialize content
      if (typeof source.content === 'string' && source.content.startsWith('{')) {
        this.content = JSON.parse(source.content);
      }

      // Deserialize content.context
      if (this.content && typeof this.content.context === 'string' && this.content.context.startsWith('{')) {
        this.content.context = JSON.parse(this.content.context);
      }
    } catch (err) {
      console.error('Error during UserEvent deserialization', err);
    }
  }
}

// @dynamic
@EntityClass({ typename: 'UserEventTestFilterVO' })
export class UserEventTestFilter extends EntityFilter<UserEventTestFilter, UserEventTest> implements IUserEventFilter<UserEventTest> {
  static fromObject: (source: any, opts?: any) => UserEventTestFilter;

  types: string[] = [];
  levels: EventLevel[] = [];
  issuers: string[] = [];
  recipients: string[] = [];
  startDate: Moment = null;
  includedIds: number[];
  excludeRead = false;
  jobId: number = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.types = source.types || [];
    this.levels = source.levels || [];
    this.issuers = source.issuers || [];
    this.recipients = source.recipients || [];
    this.startDate = fromDateISOString(source.startDate);
    this.excludeRead = source.excludeRead || false;
    this.includedIds = source.includedIds || [];
    this.jobId = source.jobId;
  }

  protected buildFilter(): FilterFn<UserEventTest>[] {
    const filterFns = super.buildFilter();

    if (isNotEmptyArray(this.types)) {
      filterFns.push((t) => this.types.includes(t.type));
    }
    if (isNotEmptyArray(this.levels)) {
      filterFns.push((t) => this.levels.includes(t.level));
    }
    if (isNotEmptyArray(this.issuers)) {
      filterFns.push((t) => this.issuers.includes(t.issuer));
    }
    if (isNotEmptyArray(this.recipients)) {
      filterFns.push((t) => this.recipients.includes(t.recipient));
    }

    if (isNotNil(this.startDate)) {
      filterFns.push((t) => this.startDate.isSameOrBefore(t.creationDate));
    }
    if (isNotEmptyArray(this.includedIds)) {
      filterFns.push((t) => this.includedIds.includes(t.id));
    }
    if (this.excludeRead === true) {
      filterFns.push((t) => isNil(t.readSignature)); // todo or t.signature ?
    }
    if (isNotNil(this.jobId)) {
      filterFns.push((t) => this.jobId === t.jobId);
    }
    return filterFns;
  }

  asObject(opts?: EntityAsObjectOptions): StoreObject {
    const target = super.asObject(opts);
    return target;
  }
}
