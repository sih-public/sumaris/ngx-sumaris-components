import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { UserEventNotificationIcon } from './notification/user-event-notification.icon';
import { UserEventNotificationList } from './notification/user-event-notification.list';
import { AppIconModule } from '../../core/icon/icon.module';
import { NgxJdenticonModule } from 'ngx-jdenticon';
import { RxStateModule } from '../../shared/rx-state/rx-state.module';

@NgModule({
  imports: [CommonModule, SharedModule, AppIconModule, RxStateModule, NgxJdenticonModule],
  declarations: [UserEventNotificationIcon, UserEventNotificationList],
  exports: [UserEventNotificationIcon],
})
export class UserEventModule {}
