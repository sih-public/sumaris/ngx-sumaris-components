import { Directive, InjectionToken, OnDestroy } from '@angular/core';
import { FetchPolicy } from '@apollo/client/core';
import { SocialErrorCodes } from '../social.errors';
import { AccountService } from '../../core/services/account.service';
import { GraphqlService } from '../../core/graphql/graphql.service';
import { BehaviorSubject, merge, Observable, of, Subject, Subscription, timer } from 'rxjs';
import { IUserEvent, IUserEventFilter } from './user-event.model';
import { SortDirection } from '@angular/material/sort';
import {
  EntitiesServiceLoadOptions,
  EntitiesServiceWatchOptions,
  IEntitiesService,
  LoadResult,
  Page,
} from '../../shared/services/entity-service.class';
import { distinctUntilChanged, map, mergeMap } from 'rxjs/operators';
import { isEmptyArray, isNotEmptyArray, isNotNil, toNumber } from '../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import { NetworkService } from '../../core/services/network.service';
import { BaseGraphqlService, BaseGraphqlServiceOptions } from '../../core/services/base-graphql-service.class';
import { EntityUtils } from '../../core/services/model/entity.model';
import { DateUtils } from '../../shared/dates';
import { Moment } from 'moment';
import { IStartableService } from '../../shared/services/startable-service.class';
import { MINIFY_ENTITY_FOR_POD } from '../../core/services/model/referential.model';
import { DocumentNode } from 'graphql/index';
import { EntitySaveOptions, MutableWatchQueriesUpdatePolicy } from '../../core/services/base-entity-service.class';

export const APP_USER_EVENT_SERVICE = new InjectionToken<IUserEventService<any, any>>('UserEventService');

export declare interface UserEventLoadOptions<T> extends EntitiesServiceLoadOptions<T> {
  withContent?: boolean; // Default to false
}
export declare interface UserEventWatchOptions<T> extends EntitiesServiceWatchOptions<T> {
  withContent?: boolean; // Default to false
}
export interface IUserEventQueries {
  loadAll: any;
  loadAllWithContent: any;
  count?: any;
}

export interface IUserEventMutations {
  save: any;
  markAsRead?: any;
  deleteByIds: any;
}

export interface IUserEventSubscriptions {
  listenAllChanges: any;
  listenCountChanges?: any;
}

export interface IUserEventListener<E extends IUserEvent<E, ID>, ID = number> {
  accept: (E) => boolean;
  onReceived?: (E) => Promise<E> | E | undefined;
  onRead?: (E) => Promise<void> | void;
}

export interface UserEventServiceOptions<
  Q extends IUserEventQueries = IUserEventQueries,
  M extends IUserEventMutations = IUserEventMutations,
  S extends IUserEventSubscriptions = IUserEventSubscriptions,
> extends BaseGraphqlServiceOptions {
  queries: Q;
  mutations?: Partial<M>;
  subscriptions?: Partial<S>;
  watchQueriesUpdatePolicy?: MutableWatchQueriesUpdatePolicy;
}

export interface IUserEventService<
  E extends IUserEvent<E, ID>,
  F extends IUserEventFilter<E, ID>,
  ID = number,
  WO extends UserEventWatchOptions<E> = UserEventWatchOptions<E>,
  LO extends UserEventLoadOptions<E> = UserEventLoadOptions<E>,
> extends IEntitiesService<E, F, WO>,
    IStartableService {
  countSubject: Observable<number>;

  add(entity: E): Promise<E>;

  load(id: ID, opts?: LO): Promise<E>;

  count(filter: Partial<F>, options?: { fetchPolicy?: FetchPolicy }): Promise<number>;

  listenCountChanges(filter: Partial<F>, options?: Omit<LO, 'toEntity'> & { interval?: number }): Observable<number>;

  listenAllChanges(filter: Partial<F>, options?: LO & { interval?: number }): Observable<E[]>;

  save(entity: E, options?: EntitySaveOptions<E>): Promise<E>;

  delete(entity: E): Promise<any>;

  markAllAsRead(entities: E[]);

  registerListener(listener: IUserEventListener<E, ID>);

  resetCount();
}

@Directive()
export abstract class AbstractUserEventService<
    E extends IUserEvent<E, ID, any>,
    F extends IUserEventFilter<E, ID, any>,
    ID = number,
    WO extends UserEventWatchOptions<E> = UserEventWatchOptions<E>,
    LO extends UserEventLoadOptions<E> = UserEventLoadOptions<E>,
    Q extends IUserEventQueries = IUserEventQueries,
    M extends IUserEventMutations = IUserEventMutations,
    S extends IUserEventSubscriptions = IUserEventSubscriptions,
  >
  extends BaseGraphqlService<E, F, ID>
  implements IUserEventService<E, F, ID, WO, LO>, OnDestroy
{
  protected readonly queries: Q;
  protected readonly mutations: Partial<M>;
  protected readonly subscriptions: Partial<S>;
  protected readonly _countSubject = new BehaviorSubject<number>(0);
  protected readonly watchQueriesUpdatePolicy: MutableWatchQueriesUpdatePolicy;
  protected resetCountDate: Moment;
  protected listeners: IUserEventListener<E, ID>[] = [];
  private _subscriptions = new Subscription();
  private _listenSubscription: Subscription;

  get countSubject(): Subject<number> {
    return this._countSubject;
  }

  protected constructor(
    protected graphql: GraphqlService,
    protected accountService: AccountService,
    protected network: NetworkService,
    protected translate: TranslateService,
    protected options: UserEventServiceOptions<Q, M, S>
  ) {
    super(graphql, options);
    this.queries = options.queries;
    this.mutations = options.mutations || <Partial<M>>{};
    this.subscriptions = options.subscriptions || <Partial<S>>{};
    this.watchQueriesUpdatePolicy = options.watchQueriesUpdatePolicy || 'update-cache';
    this._logPrefix = '[user-event-service] ';
  }

  protected async ngOnStart(): Promise<void> {
    // Update component when refresh is need (=login events)
    this._subscriptions.add(
      merge(
        this.accountService.onLogin,
        this.accountService.onLogout,
        timer(1000) // Starts with -  first attempt if account is ready
      )
        .pipe(
          // Wait account service ready (can be restarted)
          mergeMap(() => this.accountService.ready()),
          map(() => this.accountService.isLogin()),
          distinctUntilChanged()
        )
        .subscribe((login) => {
          if (login) {
            this.onLogin();
          } else {
            this.onLogout();
          }
        })
    );
  }

  ngOnDestroy() {
    this._subscriptions.unsubscribe();
  }

  abstract asFilter(filter: Partial<F>): F;

  abstract fromObject(source: any): E;

  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<F>,
    options?: WO
  ): Observable<LoadResult<E>> {
    return this.watchPage(<Page>{ offset, size, sortBy, sortDirection }, filter, options);
  }

  watchPage(page: Page, filter?: Partial<F>, opts?: WO): Observable<LoadResult<E>> {
    let now = this._debug && Date.now();
    if (!filter) {
      filter = this.defaultFilter();
    }
    console.debug(`${this._logPrefix}Loading user events...`, filter);

    filter = this.asFilter(filter);

    // Force recipient to current issuer, if not admin and not specified
    if (isEmptyArray(filter?.recipients) || !this.accountService.isAdmin()) {
      const recipient = this.defaultRecipient();
      if (!filter?.recipients?.includes(recipient)) {
        console.warn('[user-events-service] Force user event filter.recipient=' + recipient);
        filter.recipients = [recipient];
      }
    }

    const withContent = opts?.withContent === true;

    return this.mutableWatchQuery<LoadResult<E>, { filter: any; page: Page }>({
      queryName: withContent ? 'LoadAllWithContent' : 'LoadAll',
      query: withContent ? this.queries.loadAllWithContent : this.queries.loadAll,
      variables: {
        page: {
          sortBy: 'creationDate',
          ...page,
          sortDirection: (page.sortDirection || 'desc').toUpperCase() as SortDirection,
        },
        filter: filter && filter.asPodObject(),
      },
      arrayFieldName: 'data',
      error: { code: SocialErrorCodes.LOAD_USER_EVENTS_ERROR, message: 'SOCIAL.ERROR.LOAD_USER_EVENTS_ERROR' },
      fetchPolicy: opts?.fetchPolicy || 'cache-first',
      insertFilterFn: filter && filter.asFilterFn(),
    }).pipe(
      mergeMap(async (res) => {
        let data = await Promise.all((res?.data || []).map((value) => this.processUserEvent(value)));

        if (now) {
          console.debug(`${this._logPrefix}${data.length} user events loaded in ${Date.now() - now}ms`);
          now = null;
        }

        // Must re-order because of listenAllChanges result will add new event at the end
        data = EntityUtils.sort(data, page.sortBy, page.sortDirection);

        const result = <LoadResult<E>>{
          data,
          total: toNumber(res?.total, data.length),
        };

        // Add fetch more capability, if total was fetched
        if (isNotNil(res.total)) {
          const nextOffset = (page.offset || 0) + data.length;
          if (nextOffset < result.total) {
            result.fetchMore = (_) =>
              this.loadPage({ ...page, offset: nextOffset }, filter, { ...opts, fetchPolicy: <FetchPolicy>'no-cache' } as unknown as LO);
          }
        }

        return result;
      })
    );
  }

  async loadPage(page: Page, filter?: Partial<F>, opts?: LO): Promise<LoadResult<E>> {
    const now = this._debug && Date.now();
    if (!filter) filter = this.defaultFilter();

    console.debug(`${this._logPrefix}Loading user events...`, filter);

    filter = this.asFilter(filter);

    // Force recipient to current issuer, if not admin and not specified
    if (isEmptyArray(filter?.recipients) || !this.accountService.isAdmin()) {
      const recipient = this.defaultRecipient();
      if (!filter?.recipients?.includes(recipient)) {
        console.warn('[user-events-service] Force user event filter.recipient=' + recipient);
        filter.recipients = [recipient];
      }
    }

    const withContent = opts?.withContent === true;

    const res = await this.graphql.query<LoadResult<E>>({
      query: withContent ? this.queries.loadAllWithContent : this.queries.loadAll,
      variables: {
        page: {
          sortBy: 'creationDate',
          ...page,
          sortDirection: (page.sortDirection || 'desc').toUpperCase() as SortDirection,
        },
        filter: filter && filter.asPodObject(),
      },
      error: { code: SocialErrorCodes.LOAD_USER_EVENTS_ERROR, message: 'SOCIAL.ERROR.LOAD_USER_EVENTS_ERROR' },
      fetchPolicy: opts?.fetchPolicy,
    });
    let data = await Promise.all((res?.data || []).map((value) => this.processUserEvent(value)));

    if (now) console.debug(`${this._logPrefix}${data.length} user events loaded in ${Date.now() - now}ms`);

    // Must re-order because of listenAllChanges result will add new event at the end
    data = EntityUtils.sort(data, page.sortBy, page.sortDirection);

    const result = <LoadResult<E>>{
      data,
      total: toNumber(res?.total, data.length),
    };

    // Add fetch more capability, if total was fetched
    if (isNotNil(res.total)) {
      const nextOffset = (page.offset || 0) + data.length;
      if (nextOffset < result.total) {
        result.fetchMore = (_) => this.loadPage({ ...page, offset: nextOffset }, filter, opts);
      }
    }

    return result;
  }

  async load(id: ID, opts?: LO): Promise<E> {
    const filter = this.defaultFilter();
    filter.includedIds = [id];
    const { data } = await this.loadPage({ offset: 0, size: 1 }, filter, opts);
    return data && data[0];
  }

  async add(entity: E): Promise<E> {
    entity = await this.processUserEvent(entity);

    if (!entity) return; // Rejected

    const withContent = !!entity.content;

    // Add user event locally
    if (this.watchQueriesUpdatePolicy === 'update-cache') {
      if (withContent) {
        this.insertIntoMutableCachedQueries(this.graphql.cache, {
          query: this.queries.loadAllWithContent,
          data: entity,
        });
      }
      this.insertIntoMutableCachedQueries(this.graphql.cache, {
        query: this.queries.loadAll,
        data: {
          ...entity,
          content: null,
        },
      });
    }

    // Update count
    this._countSubject.next(this._countSubject.value + 1);

    // Add id
    //console.log('TODO: add an ID to user event ?');

    return entity;
  }

  async count(filter: Partial<F>, options?: { fetchPolicy?: FetchPolicy }): Promise<number> {
    if (!this.queries?.count) {
      console.warn(`${this._logPrefix}Query 'count' not provided, skip.`);
      return undefined;
    }

    if (!filter) filter = this.defaultFilter();
    // Apply last reset date as start date
    filter.startDate = this.resetCountDate;
    filter = this.asFilter(filter);

    if (this._debug) console.debug(`${this._logPrefix}Counting user events with filter:`, filter);

    const res = await this.graphql.query<{ total: number }>({
      query: this.queries.count,
      variables: {
        filter: filter.asPodObject(),
      },
      error: { code: SocialErrorCodes.COUNT_USER_EVENT_ERROR, message: 'SOCIAL.ERROR.COUNT_USER_EVENT_ERROR' },
      fetchPolicy: options?.fetchPolicy || 'network-only',
    });
    return res.total;
  }

  listenCountChanges(filter: Partial<F>, options?: Omit<LO, 'toEntity'> & { interval?: number }): Observable<number> {
    if (!this.subscriptions?.listenCountChanges) {
      console.warn(`${this._logPrefix}Subscription query 'listenCountChanges' not provided, skip.`);
      return of();
    }

    const isDefault = !filter;
    if (isDefault) filter = this.defaultFilter();
    // Apply last reset date as start date
    filter.startDate = this.resetCountDate;
    filter.excludeRead = true;
    filter = this.asFilter(filter);

    if (this._debug) console.debug(`${this._logPrefix}[WS] Listening count changes for user events with filter:`, filter);

    return this.graphql
      .subscribe<{ total: number }, { filter: any; interval: number }>({
        query: this.subscriptions.listenCountChanges,
        fetchPolicy: options?.fetchPolicy,
        variables: { filter: filter.asPodObject(), interval: toNumber(options?.interval, 10) },
        error: {
          code: SocialErrorCodes.SUBSCRIBE_USER_EVENTS_ERROR,
          message: 'SOCIAL.ERROR.SUBSCRIBE_USER_EVENTS_ERROR',
        },
      })
      .pipe(
        map(({ total }) => {
          if (total && this._debug) console.debug(`${this._logPrefix}Received new user events count:`, total);
          // update count
          if (isDefault) this._countSubject.next(total);

          return total;
        })
      );
  }

  listenAllChanges(filter: Partial<F>, opts?: LO & { interval?: number }): Observable<E[]> {
    if (!this.subscriptions?.listenAllChanges) {
      console.warn(`${this._logPrefix}Subscription query 'listenAllChanges' not provided, skip.`);
      return of();
    }

    if (!filter) filter = this.defaultFilter();
    filter = this.asFilter(filter);

    const withContent = opts?.withContent === true;

    if (this._debug) console.debug(`${this._logPrefix}[WS] Listening changes for user events with filter:`, filter);

    return this.graphql
      .subscribe<{ data: any[] }, { filter: any; interval: number }>({
        query: this.subscriptions.listenAllChanges,
        fetchPolicy: opts?.fetchPolicy,
        variables: { filter: filter.asPodObject(), interval: toNumber(opts?.interval, 10) },
        error: {
          code: SocialErrorCodes.SUBSCRIBE_USER_EVENTS_ERROR,
          message: 'SOCIAL.ERROR.SUBSCRIBE_USER_EVENTS_ERROR',
        },
      })
      .pipe(
        mergeMap(async ({ data }) => {
          if (data && this._debug) console.debug(`${this._logPrefix}Received new user events:`, data);

          // Update cache (will update watchAll queries)
          if (isNotEmptyArray(data)) {
            this.insertIntoMutableCachedQueries(this.graphql.cache, {
              query: withContent ? this.queries.loadAllWithContent : this.queries.loadAll,
              data,
            });
          }

          // Convert to entities
          if (!opts || opts.toEntity !== false) {
            return await Promise.all((data || []).map((value) => this.processUserEvent(value)));
          } else {
            return data;
          }
        })
      );
  }

  async delete(entity: E): Promise<any> {
    if (!entity) return; // skip
    await this.deleteAll([entity]);
  }

  async deleteAll(entities: E[], opts?: any): Promise<any> {
    const ids = entities && entities.map((t) => t.id);
    if (isEmptyArray(ids)) return; // stop, if nothing else to do

    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix}Deleting events... ids:`, ids);

    await this.graphql.mutate<any>({
      mutation: this.mutations.deleteByIds,
      variables: {
        ids,
      },
      update: (cache) => {
        // Remove from cache
        if (this.watchQueriesUpdatePolicy === 'update-cache') {
          this.removeFromMutableCachedQueriesByIds(cache, {
            queries: this.getLoadQueries(),
            ids,
          });
        }

        if (this._debug) console.debug(`${this._logPrefix}Events deleted in ${Date.now() - now}ms`);
      },
    });
  }

  async save(entity: E, opts?: EntitySaveOptions<E>): Promise<E> {
    this.fillDefaultProperties(entity);

    // Transform into json
    const withContent = !!entity.content;
    const json = entity.asObject(MINIFY_ENTITY_FOR_POD);

    const now = Date.now();
    if (this._debug) console.debug(`${this._logPrefix}Saving user event...`, json);

    await this.graphql.mutate<{ data: any }>({
      mutation: this.mutations.save,
      variables: {
        data: json,
      },
      error: { code: SocialErrorCodes.SAVE_USER_EVENT_ERROR, message: 'SOCIAL.ERROR.SAVE_USER_EVENT_ERROR' },
      update: (cache, { data }, { context, variables }) => {
        // Update entity
        const savedEntity = data && data.data;
        if (savedEntity) {
          if (this._debug) console.debug(`${this._logPrefix}User event saved in ${Date.now() - now}ms`, entity);
          this.copyIdAndUpdateDate(savedEntity, entity);

          // Add to cache
          if (this.watchQueriesUpdatePolicy === 'update-cache') {
            if (withContent) {
              this.insertIntoMutableCachedQueries(cache, {
                query: this.queries.loadAllWithContent,
                data: savedEntity,
              });
            }
            this.insertIntoMutableCachedQueries(cache, {
              query: this.queries.loadAll,
              data: {
                ...savedEntity,
                content: null,
              },
            });
          }

          if (opts?.update) {
            opts.update(cache, { data }, { context, variables });
          }
        }
      },
    });

    return entity;
  }

  async saveAll(entities: E[], opts?: EntitySaveOptions<E>): Promise<E[]> {
    return Promise.all(entities.map((entity) => this.save(entity, opts)));
  }

  async markAllAsRead(entities: E[]) {
    const userEventWithoutReadListeners: E[] = [];
    for (const entity of entities || []) {
      // Get onRead listeners
      const onReadListeners = (this.listeners || []).filter((listener) => !!listener.onRead).filter((listener) => listener.accept(entity));
      if (onReadListeners.length) {
        for (const listener of onReadListeners) {
          await listener.onRead(entity);
        }
      } else {
        // Add to this list for default operation
        userEventWithoutReadListeners.push(entity);
      }
    }

    // Use default
    await this.defaultMarkAsRead(userEventWithoutReadListeners);
  }

  registerListener(listener: IUserEventListener<E, ID>) {
    if (!this.listeners) this.listeners = [];
    this.listeners.push(listener);
  }

  resetCount() {
    this._countSubject.next(undefined);
    this.resetCountDate = DateUtils.moment();
    // restart listening count changes
    this.startListenCountChanges();
  }

  /* -- protected methods -- */

  protected async onLogin() {
    // Get first count
    const count = await this.count(undefined);
    if (this._debug) console.debug(`${this._logPrefix}Receiving user events count`, count);
    // Update count
    this._countSubject.next(count);

    // Then listen changes
    this.startListenCountChanges();
  }

  protected onLogout() {
    // Reset counter
    this._countSubject.next(0);
    // Stop listening
    this.stopListenCountChanges();
  }

  protected startListenCountChanges() {
    this.stopListenCountChanges();
    this._listenSubscription = this.listenCountChanges(undefined).subscribe();
  }

  protected stopListenCountChanges() {
    this._listenSubscription?.unsubscribe();
    this._listenSubscription = undefined;
  }

  protected async processUserEvent(source: any): Promise<E> {
    // Convert json source to IUserEvent
    let target = this.fromObject(source);

    // Get listeners
    const listeners = (this.listeners || []).filter((listener) => !!listener.onReceived).filter((listener) => listener.accept(target));
    for (const listener of listeners) {
      if (target) {
        const res = listener.onReceived(target);
        target = res instanceof Promise ? await res : res;
      } else {
        if (this._debug) console.debug(`${this._logPrefix}Event has been rejected by a listener`, source);
        break;
      }
    }

    return target;
  }

  protected async defaultMarkAsRead(userEvents: E[]) {
    if (!userEvents) throw new Error(`${this._logPrefix}Invalid event to save`);
    if (!userEvents.length) return;

    // set read date
    userEvents.forEach((userEvent) => (userEvent.readDate = DateUtils.moment()));

    // split local/remote entities
    const localEntities = userEvents.filter(EntityUtils.isLocal);
    const remoteEntities = userEvents.filter(EntityUtils.isRemote);

    // save local entities
    if (localEntities.length) {
      if (this._debug) console.debug(`${this._logPrefix}Save local user event`);
      // TODO !!!!!!!!!!!!!
    }
    // mark remote entities as read
    if (remoteEntities.length) {
      if (!this.mutations?.markAsRead) {
        console.warn(`${this._logPrefix}Mutation query 'markAsRead' not provided, skip.`);
        return;
      }

      // Collect ids
      const ids = remoteEntities.map((value) => value.id);
      const now = Date.now();
      if (this._debug) console.debug(`${this._logPrefix}Mark user events as read...`, ids);

      await this.graphql.mutate<{ data: any }>({
        mutation: this.mutations.markAsRead,
        variables: {
          ids,
        },
        error: { code: SocialErrorCodes.SAVE_USER_EVENT_ERROR, message: 'SOCIAL.ERROR.SAVE_USER_EVENT_ERROR' },
      });

      if (this._debug) console.debug(`${this._logPrefix}User events marked as read in ${Date.now() - now}ms`, ids);
    }
  }

  protected defaultFilter(): F {
    // By default
    return <F>{
      recipients: [this.defaultRecipient()],
    };
  }

  protected abstract defaultRecipient(): any;

  protected fillDefaultProperties(entity: E) {}

  protected unreadEvents(events: E[]): number {
    // default calculation on unread events from a list
    if (isEmptyArray(events)) return undefined;
    const lastReadDate = events
      .filter((event) => isNotNil(event.readDate))
      .reduce((lastDate, event) => DateUtils.max(lastDate, event.readDate), undefined as Moment);
    return events.filter((event) => event.creationDate.isSameOrAfter(lastReadDate)).length;
  }

  protected copyIdAndUpdateDate(source: E, target: E) {
    EntityUtils.copyIdAndUpdateDate(source, target);
  }

  protected getLoadQueries(): DocumentNode[] {
    return [this.queries.loadAll, this.queries.loadAllWithContent].filter(isNotNil);
  }
}
