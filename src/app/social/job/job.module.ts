import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { JobProgressionIcon } from './progression/job-progression.icon';
import { JobProgressionList } from './progression/job-progression.list';
import { JobProgressionComponent } from './progression/job-progression.component';

@NgModule({
  imports: [CommonModule, SharedModule, TranslateModule.forChild()],
  declarations: [JobProgressionIcon, JobProgressionList, JobProgressionComponent],
  exports: [JobProgressionIcon, JobProgressionList, JobProgressionComponent],
})
export class JobModule {}
