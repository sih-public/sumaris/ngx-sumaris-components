import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { JobProgression } from './job-progression.model';
import { IUserEventAction } from '../../user-event/user-event.model';
import { PopoverController } from '@ionic/angular';
import { Observable } from 'rxjs';

export interface JobProgressionListOptions {
  titleI18n?: string;
  headerActions?: IUserEventAction[];
}

@Component({
  selector: 'app-job-progression-list',
  templateUrl: './job-progression.list.html',
  styleUrls: ['./job-progression.list.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobProgressionList implements JobProgressionListOptions {
  @Input() titleI18n: string;
  @Input() headerActions: IUserEventAction[];
  @Input() jobProgressions: Observable<Observable<JobProgression>[]>;

  constructor(protected popoverController: PopoverController) {}

  async executeAction(event: Event, action: IUserEventAction) {
    if (event?.defaultPrevented) return; // Skip if prevented

    // Disable default action (.e.g avoid to call click() function)
    if (event) event.preventDefault();

    // Execute then close popover
    this.popoverController.dismiss();
    try {
      const res = action.executeAction(undefined);
      if (res instanceof Promise) await res;
    } catch (err) {
      console.error(err);
    }
  }
}
