import { EntityClass } from '../../../core/services/model/entity.decorators';
import { Entity } from '../../../core/services/model/entity.model';

// @dynamic
@EntityClass({ typename: 'JobProgressionVO' })
export class JobProgression extends Entity<JobProgression> {
  static fromObject: (source: any, opts?: any) => JobProgression;

  name: string;
  message: string;
  current: number;
  total: number;

  constructor(id?: number) {
    super(JobProgression.TYPENAME);
    this.id = id;
  }

  fromObject(source: any) {
    super.fromObject(source);
    this.name = source.name;
    this.message = source.message;
    this.current = source.current;
    this.total = source.total;
  }
}
