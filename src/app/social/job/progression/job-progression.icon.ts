import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { APP_JOB_PROGRESSION_SERVICE, IJobProgressionService } from './job-progression.service';
import { JobProgression } from './job-progression.model';
import { BehaviorSubject, delay, of, Subscription, switchMap } from 'rxjs';
import { arraySize, isNil, remove, toBoolean, toNumber } from '../../../shared/functions';
import { filter, map, tap } from 'rxjs/operators';
import { PopoverController } from '@ionic/angular';
import { JobProgressionList, JobProgressionListOptions } from './job-progression.list';
import { IUserEventAction } from '../../user-event/user-event.model';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { RxState } from '@rx-angular/state';
import { ObjectMap } from '../../../shared/types';

export interface JobProgressionOptions {
  autoHide: boolean;
  autoHideDelay: number;
  autoRemove: boolean;
  autoRemoveDelay: number;
}

export interface JobProgressionIconState {
  visible: boolean;
  mode: ProgressSpinnerMode;
  value: number;
  jobProgressions: BehaviorSubject<JobProgression>[];
  jobCount: number;
}

@Component({
  selector: 'app-job-progression-icon',
  templateUrl: './job-progression.icon.html',
  styleUrls: ['./job-progression.icon.scss'],
  // providers: [RxState],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobProgressionIcon extends RxState<JobProgressionIconState> implements OnInit, OnDestroy {
  @Input() debug = false;
  @Input() titleI18n = 'SOCIAL.JOB.PROGRESSION.TITLE';
  @Input() options: JobProgressionOptions;
  @Input() autoHide: boolean;
  @Input() headerActions: IUserEventAction[];
  @Output() jobFinished = new EventEmitter<number>();

  disabled = true;
  color: ThemePalette = 'accent';
  jobProgressions$ = this.select('jobProgressions');
  jobCount$ = this.select('jobCount');
  visible$ = this.select('visible');
  mode$ = this.select('mode');
  value$ = this.select('value');

  protected autoHideDelay: number;
  protected autoRemove: boolean;
  protected autoRemoveDelay: number;

  private _listPopover: HTMLIonPopoverElement;
  private _subscriptions = new Subscription();
  private _subscriptionsById: ObjectMap<Subscription> = {};
  private _logPrefix = '[job-progression] ';

  constructor(
    @Optional() @Inject(APP_JOB_PROGRESSION_SERVICE) protected jobProgressionService: IJobProgressionService,
    protected popoverController: PopoverController,
    protected cd: ChangeDetectorRef
  ) {
    super();
  }

  get jobProgressions(): BehaviorSubject<JobProgression>[] {
    return this.get('jobProgressions');
  }

  ngOnInit() {
    if (isNil(this.jobProgressionService)) {
      console.warn(`${this._logPrefix}No service injected`);
    }
    // parse options
    this.autoHide = toBoolean(this.autoHide, this.options?.autoHide || false);
    this.set({ visible: !this.autoHide, mode: 'determinate', value: 0 });
    this.autoHideDelay = toNumber(this.options?.autoHideDelay, 1000);
    this.autoRemove = toBoolean(this.options?.autoRemove, true);
    this.autoRemoveDelay = toNumber(this.options?.autoRemoveDelay, 1000);

    if (this.jobProgressionService) {
      this.connect('jobProgressions', this.jobProgressionService.watchAll(), (state, items) => {
        const existingJobIds = state.jobProgressions?.map((job) => job.value.id) || [];
        const result = (items || []).map((item) => {
          remove(existingJobIds, (id) => id === item.id);
          // Add job to listen
          return this.startListenJob(item.id, item);
        });
        // Remove old jobs
        if (existingJobIds.length > 0) {
          existingJobIds.forEach((id) => this.stopListenJob(id, { emitEvent: false }));
        }
        return result;
      });
    }

    this.connect('jobCount', this.jobProgressions$.pipe(map((jobs) => jobs?.length || 0)));

    this.connect(
      'visible',
      this.jobCount$.pipe(
        map((jobCount) => jobCount > 0),
        switchMap((hasJob) =>
          hasJob
            ? of(true)
            : of(false).pipe(
                filter(() => this.autoHide),
                delay(this.autoHideDelay),
                tap(() => {
                  if (this._listPopover) this._listPopover.dismiss();
                })
              )
        )
      )
    );
  }

  protected startListenJob(id: number, job?: JobProgression) {
    if (isNil(this.jobProgressionService)) {
      console.warn(`${this._logPrefix}No service injected. Can't add a job`);
      return;
    }
    if (isNil(id)) {
      console.error(`${this._logPrefix}Missing required argument 'id'. Cannot listen changes !`);
      return;
    }
    let result = this.getProgression(id);
    if (result) {
      //console.warn(`${this._logPrefix}Job (id=${id}) already present`);
      return result;
    }
    if (this.debug) {
      console.debug(`${this._logPrefix}Add job id=${id}`);
    }
    // Adding empty job progression
    result = new BehaviorSubject(job || new JobProgression(id));

    this.disabled = false;

    const sub = this.jobProgressionService
      .listenChanges(id)
      .pipe(
        filter((progression) => {
          if (id !== progression.id) {
            console.error(`${this._logPrefix}Job progression (id=${progression.id}) doesn't match expected job (id=${id})`);
            return false;
          }
          return true;
        }),
        map((progression) => {
          // update current
          result.next(progression);
          if (this.debug) {
            console.debug(`${this._logPrefix}Progression updated id=${id}`, progression);
          }
          return progression;
        })
      )
      .subscribe((progression) => {
        // update value
        this.updateValue();

        // job finished
        if (progression.total > 0 && progression.current === progression.total) {
          if (this.debug) {
            console.debug(`${this._logPrefix}Finished job id=${progression.id}`);
          }
          // emit event
          this.jobFinished.emit(progression.id);
          if (this.autoRemove) {
            setTimeout(() => {
              this.stopListenJob(progression.id);
            }, this.autoRemoveDelay);
          }
        }
      });

    this._subscriptionsById[id] = sub;
    this._subscriptions.add(sub);

    return result;
  }

  protected stopListenJob(id: number, opts?: { emitEvent?: boolean }) {
    if (this.debug) {
      console.debug(`${this._logPrefix}Remove job id=${id}`);
    }
    const sub = this._subscriptionsById[id];
    if (sub) {
      this._subscriptions.remove(sub);
      sub.unsubscribe();
    }
    // Remove job from service
    this.jobProgressionService.removeJob(id);
    if (!opts?.emitEvent !== false) {
      this.updateValue();
    }
  }

  protected getProgression(id: number): BehaviorSubject<JobProgression> | undefined {
    return this.jobProgressions?.find((progression) => progression.value.id === id);
  }

  protected updateValue() {
    if (this.debug) {
      console.debug(`${this._logPrefix}Updating value`);
    }
    let value = 0;
    const nbProgression = arraySize(this.jobProgressions);
    let allIndeterminate = nbProgression > 0;

    if (nbProgression) {
      this.jobProgressions
        .map((subject) => subject.value)
        .forEach((progression) => {
          const indeterminate = progression.total === 0;
          allIndeterminate = allIndeterminate && indeterminate;
          if (!indeterminate) {
            value = value + (progression.current * 100) / progression.total;
          }
        });
    }

    this.set({ mode: allIndeterminate ? 'indeterminate' : 'determinate', value: nbProgression > 0 ? value / nbProgression : 0 });
    if (this.debug) {
      console.debug(`${this._logPrefix}Setting value=${this.get('value')}, mode=${this.get('mode')}`);
    }

    this.markForCheck();
  }

  async showList(event: Event) {
    this._listPopover = await this.popoverController.create({
      component: JobProgressionList,
      componentProps: <JobProgressionListOptions>{
        titleI18n: this.titleI18n,
        jobProgressions: this.jobProgressions$,
        headerActions: this.headerActions,
      },
      backdropDismiss: true,
      keyboardClose: true,
      event,
      translucent: true,
      cssClass: 'popover-large',
    });
    await this._listPopover.present();
    await this._listPopover.onDidDismiss();
    this._listPopover = undefined;
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  ngOnDestroy(): void {
    if (this.debug) {
      console.debug(`${this._logPrefix}Destroying`);
    }
    this._subscriptions.unsubscribe();
  }
}
