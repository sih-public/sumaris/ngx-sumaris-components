import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { JobProgression } from './job-progression.model';
import { BaseGraphqlService } from '../../../core/services/base-graphql-service.class';
import { GraphqlService } from '../../../core/graphql/graphql.service';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';
import { BehaviorSubject, Observable } from 'rxjs';
import { FetchPolicy, gql } from '@apollo/client/core';
import { isNil, removeDuplicatesFromArray, toNumber } from '../../../shared/functions';
import { map } from 'rxjs/operators';
import { SocialErrorCodes } from '../../social.errors';

export const APP_JOB_PROGRESSION_SERVICE = new InjectionToken<IJobProgressionService>('JobProgressionService');

export interface IJobProgressionService {
  watchAll(): Observable<JobProgression[]>;
  addJob(id: number, job?: JobProgression);
  removeJob(id: number);
  listenChanges(id: number, options?: { interval?: number; fetchPolicy?: FetchPolicy }): Observable<JobProgression>;
}

const JobProgressionSubscription = {
  listenChanges: gql`
    subscription UpdateJobProgression($id: Int!, $interval: Int) {
      data: updateJobProgression(id: $id, interval: $interval) {
        id
        name
        message
        current
        total
      }
    }
  `,
};

@Injectable()
export class JobProgressionService extends BaseGraphqlService<JobProgression> implements IJobProgressionService {
  private jobsSubject = new BehaviorSubject<JobProgression[]>([]);

  constructor(
    protected graphql: GraphqlService,
    @Optional() @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(graphql, environment);
    this._logPrefix = '[job-progression-service] ';
    // For DEV only
    this._debug = environment && !environment.production;
  }

  watchAll(): Observable<JobProgression[]> {
    return this.jobsSubject.asObservable();
  }

  addJob(id: number, job?: JobProgression) {
    if (isNil(id)) throw new Error('Missing required argument \'id\'');
    const exists = this.jobsSubject.value.some((j) => j.id === id);
    if (!exists) {
      job = job || new JobProgression(id);
      this.jobsSubject.next(removeDuplicatesFromArray([...this.jobsSubject.value, job], 'id'));
    }
  }

  removeJob(id: number) {
    if (isNil(id)) throw new Error('Missing required argument \'id\'');
    const jobs = this.jobsSubject.value || [];
    const index = jobs.findIndex((j) => j.id === id);
    if (index !== -1) {
      jobs.splice(index, 1);
      this.jobsSubject.next(jobs);
    }
  }

  listenChanges(
    id: number,
    options?: {
      interval?: number;
      fetchPolicy?: FetchPolicy;
    }
  ): Observable<JobProgression> {
    if (isNil(id)) throw new Error(`${this._logPrefix}Missing argument 'id'`);
    if (this._debug) console.debug(`${this._logPrefix}[WS] Listening changes for job progression {${id}}...`);

    return this.graphql
      .subscribe<{ data: any }>({
        query: JobProgressionSubscription.listenChanges,
        fetchPolicy: options?.fetchPolicy,
        variables: { id, interval: toNumber(options?.interval, 10) },
        error: { code: SocialErrorCodes.SUBSCRIBE_JOB_PROGRESSION_ERROR, message: 'SOCIAL.ERROR.SUBSCRIBE_JOB_PROGRESSION_ERROR' },
      })
      .pipe(
        map(({ data }) => {
          const progression = data && JobProgression.fromObject(data);
          if (progression && this._debug) console.debug(`${this._logPrefix}Job progression ${id} updated on server`, progression);
          return progression;
        })
      );
  }
}
