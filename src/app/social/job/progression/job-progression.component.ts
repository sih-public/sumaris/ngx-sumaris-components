import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { JobProgression } from './job-progression.model';
import { RxState } from '@rx-angular/state';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface JobProgressionState {
  name: string;
  message: string;
  value: number;
  type: 'determinate' | 'indeterminate';
}

@Component({
  selector: 'app-job-progression-component',
  templateUrl: './job-progression.component.html',
  styleUrls: ['./job-progression.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobProgressionComponent extends RxState<JobProgressionState> implements OnInit {
  @Input() showName = true;
  @Input() jobProgression$: Observable<JobProgression>;

  name$ = this.select('name');
  message$ = this.select('message');
  value$ = this.select('value');
  type$ = this.select('type');

  constructor() {
    super();
  }

  ngOnInit() {
    this.connect('name', this.jobProgression$.pipe(map((value) => value.name)));
    this.connect('message', this.jobProgression$.pipe(map((value) => value.message)));
    this.connect('value', this.jobProgression$.pipe(map((value) => (value.total > 0 ? value.current / value.total : undefined))));
    this.connect('type', this.jobProgression$.pipe(map((value) => (value.total > 0 ? 'determinate' : 'indeterminate'))));
  }
}
