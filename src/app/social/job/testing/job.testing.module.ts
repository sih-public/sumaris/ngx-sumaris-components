import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { JobProgressionTestingPage } from './job-progression.testing';
import { SharedModule } from '../../../shared/shared.module';
import { JobModule } from '../job.module';

@NgModule({
  imports: [CommonModule, TranslateModule.forChild(), SharedModule, JobModule],
  declarations: [JobProgressionTestingPage],
  exports: [JobProgressionTestingPage],
})
export class JobTestingModule {}
