import { Injectable } from '@angular/core';
import { JobProgressionService } from '../progression/job-progression.service';
import { interval, Observable } from 'rxjs';
import { JobProgression } from '../progression/job-progression.model';
import { map, take } from 'rxjs/operators';

@Injectable()
export class JobProgressionTestService extends JobProgressionService {
  listenChanges(id: number): Observable<JobProgression> {
    return interval(100).pipe(
      take(111),
      map((value) => {
        if (value <= 10) {
          return <JobProgression>{
            id,
            name: `Job #${id}`,
            current: 0,
            total: 0,
            message: `Progression pending`,
          };
        }
        value = value - 10;
        return <JobProgression>{
          id,
          name: `Job #${id}`,
          current: value,
          total: 100,
          message: `Progression ${value}/100`,
        };
      })
    );
  }
}
