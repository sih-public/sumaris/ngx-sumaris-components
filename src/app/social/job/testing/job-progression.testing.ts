import { Component, Inject, ViewChild } from '@angular/core';
import { JobProgressionIcon, JobProgressionOptions } from '../progression/job-progression.icon';
import { JobProgression } from '../progression/job-progression.model';
import { APP_JOB_PROGRESSION_SERVICE, IJobProgressionService } from '../progression/job-progression.service';

@Component({
  selector: 'job-progression-testing',
  templateUrl: 'job-progression.testing.html',
})
export class JobProgressionTestingPage {
  @ViewChild('icon') jobProgressionIcon: JobProgressionIcon;

  jobId = 0;
  options = <JobProgressionOptions>{
    autoHide: true,
  };

  constructor(@Inject(APP_JOB_PROGRESSION_SERVICE) protected jobProgressionService: IJobProgressionService) {}

  createJob() {
    const job = new JobProgression();
    job.id = ++this.jobId;
    job.name = 'test';
    job.current = 0;
    job.total = 100;
    this.jobProgressionService.addJob(job.id, job);
  }
}
