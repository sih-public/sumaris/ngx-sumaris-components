import { TestingPage } from '../shared/testing/tests.page';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { JobTestingModule } from './job/testing/job.testing.module';
import { JobProgressionTestingPage } from './job/testing/job-progression.testing';
import { UserEventTestingModule } from './user-event/testing/user-event.testing.module';
import { UserEventTestingPage } from './user-event/testing/user-event.testing';

export const SOCIAL_TESTING_PAGES: TestingPage[] = [
  { label: 'Social components', divider: true },
  { label: 'User event', page: '/testing/social/user-event' },
  { label: 'Job progression', page: '/testing/social/job-progression' },
];

const routes: Routes = [
  {
    path: 'job-progression',
    pathMatch: 'full',
    component: JobProgressionTestingPage,
  },
  {
    path: 'user-event',
    pathMatch: 'full',
    component: UserEventTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, TranslateModule.forChild(), RouterModule.forChild(routes), JobTestingModule, UserEventTestingModule],
})
export class SocialTestingModule {}
