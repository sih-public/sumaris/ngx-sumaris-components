import { APP_INITIALIZER, ModuleWithProviders, NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedMaterialModule } from './material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { DateFormatService } from './pipes/date-format.pipe';
import { DateFromNowPipe } from './pipes/date-from-now.pipe';
import { MAT_PAGINATOR_DEFAULT_OPTIONS, MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginatorI18n } from './material/paginator/material.paginator-i18n';
import { ColorPickerModule } from 'ngx-color-picker';
import { AppFormField } from './form/field.component';
import { AudioProvider } from './audio/audio';
import { CloseScrollStrategy, FullscreenOverlayContainer, Overlay, OverlayContainer } from '@angular/cdk/overlay';
import { SharedHotkeysModule } from './hotkeys/shared-hotkeys.module';
import { FileService } from './file/file.service';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MAT_AUTOCOMPLETE_DEFAULT_OPTIONS, MAT_AUTOCOMPLETE_SCROLL_STRATEGY } from '@angular/material/autocomplete';
import { MAT_SELECT_SCROLL_STRATEGY } from '@angular/material/select';
import { SharedDirectivesModule } from './directives/directives.module';
import { SharedPipesModule } from './pipes/pipes.module';
import { AppLoadingSpinner } from './form/loading-spinner';
import { DateDiffDurationPipe } from './pipes/date-diff-duration.pipe';
import { LatitudeFormatPipe, LatLongFormatPipe, LongitudeFormatPipe } from './pipes/latlong-format.pipe';
import { HighlightPipe } from './pipes/highlight.pipe';
import { NumberFormatPipe } from './pipes/number-format.pipe';
import { MarkdownModule } from 'ngx-markdown';
import { TranslateContextService } from './services/translate-context.service';
import { MatStepperIntl } from '@angular/material/stepper';
import { MatStepperI18n } from './material/stepper/material.stepper-i18n';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { UploadFilePopover } from './upload-file/upload-file-popover.component';
import { SharedTestsPage } from './testing/tests.page';
import { APP_STORAGE } from './storage/storage.utils';
import { StorageService } from './storage/storage.service';
import { SharedToolbarModule } from './toolbar/toolbar.module';
import { NgxJdenticonModule } from 'ngx-jdenticon';
import { SharedNamedFilterModule } from './named-filter/named-filter.module';
import { EnvironmentLoader } from '../../environments/environment.loader';
import { ENVIRONMENT } from '../../environments/environment.class';
import { RxStateModule } from './rx-state/rx-state.module';

export function scrollFactory(overlay: Overlay): () => CloseScrollStrategy {
  return () => overlay.scrollStrategies.close();
}

export interface SharedModuleConfig {
  loader?: Provider;
}

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
    ColorPickerModule,
    DragDropModule,
    MarkdownModule,
    RxStateModule,

    // Sub modules
    SharedMaterialModule,
    SharedDirectivesModule,
    SharedPipesModule,
    SharedHotkeysModule,
    SharedToolbarModule,
    SharedNamedFilterModule,
  ],
  declarations: [
    // Components
    AppFormField,
    AppLoadingSpinner,
    UploadFileComponent,
    UploadFilePopover,
    SharedTestsPage,
  ],
  exports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    ReactiveFormsModule,
    ColorPickerModule,
    DragDropModule,
    MarkdownModule,
    NgxJdenticonModule,
    RxStateModule,

    // Sub-modules
    SharedMaterialModule,
    SharedDirectivesModule,
    SharedPipesModule,
    SharedHotkeysModule,
    SharedToolbarModule,
    SharedNamedFilterModule,

    // Components
    AppFormField,
    AppLoadingSpinner,
    UploadFileComponent,
    UploadFilePopover,
    SharedTestsPage,
  ],
})
export class SharedModule {
  static forRoot(config?: SharedModuleConfig): ModuleWithProviders<SharedModule> {
    console.debug('[shared] Creating module (root)');

    return {
      ngModule: SharedModule,

      providers: [
        // Load environment (if a loader has been defined)
        ...(config?.loader
          ? [
              config.loader,

              {
                provide: APP_INITIALIZER,
                deps: [EnvironmentLoader],
                useFactory: (loader: EnvironmentLoader) => () => loader.load(),
                multi: true,
              },
              {
                provide: ENVIRONMENT,
                deps: [EnvironmentLoader, APP_INITIALIZER],
                useFactory: (loader: EnvironmentLoader) => loader.get(),
              },
            ]
          : []),

        // { provide: StorageService, useClass: StorageService, deps: [ENVIRONMENT]},
        { provide: APP_STORAGE, useExisting: StorageService },

        StorageService,
        AudioProvider,
        FileService,
        TranslateContextService,
        DateFormatService,

        // Export Pipes as providers
        DateFromNowPipe,
        DateDiffDurationPipe,
        LatLongFormatPipe,
        LatitudeFormatPipe,
        LongitudeFormatPipe,
        HighlightPipe,
        NumberFormatPipe,

        { provide: OverlayContainer, useClass: FullscreenOverlayContainer },
        { provide: MAT_PAGINATOR_DEFAULT_OPTIONS, useValue: { formFieldAppearance: 'fill' } },
        { provide: MatPaginatorIntl, useClass: MatPaginatorI18n, deps: [TranslateService] },
        { provide: MatStepperIntl, useClass: MatStepperI18n, deps: [TranslateService] },

        // FIXME: try to force a custom overlay for autocomplete, because of there is a bug when using inside an ionic modal
        //{ provide: Overlay, useClass: Overlay},
        { provide: MAT_AUTOCOMPLETE_SCROLL_STRATEGY, useFactory: scrollFactory, deps: [Overlay] },
        {
          provide: MAT_AUTOCOMPLETE_DEFAULT_OPTIONS,
          useValue: {
            autoActiveFirstOption: true,
          },
        },
        { provide: MAT_SELECT_SCROLL_STRATEGY, useFactory: scrollFactory, deps: [Overlay] },
      ],
    };
  }
}
