import { LogLevel } from './log-level.model';
import { Moment } from 'moment';
import { LoggingServiceConfig } from './logging-service.config';
import { fromDateISOString, toDateISOString } from '../dates';
import { AppColors } from '../types';

export interface ILoggingService {
  configure(configuration: LoggingServiceConfig): Promise<void>;

  getLogger(loggerName: string): ILogger;
}

export interface ILoggingAppender {
  append(loggerName: string, level: LogLevel, message: string);
}

export interface ILogger {
  /**
   * Logs a message at level TRACE.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  trace(methodName: string, ...params: any[]): void;
  /**
   * Logs a message at level DEBUG.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  debug(methodName: string, ...params: any[]): void;
  /**
   * Logs a message at level INFO.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  info(methodName: string, ...params: any[]): void;
  /**
   * Logs a message at level WARN.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  warn(methodName: string, ...params: any[]): void;
  /**
   * Logs a message at level ERROR.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  error(methodName: string, ...params: any[]);

  /**
   * Logs a message at level FATAL.
   *
   * @param methodName name of the method
   * @param params optional parameters to be logged; objects will be formatted as JSON
   */
  fatal(methodName: string, ...params: any[]);
}

export interface Log {
  loggerName: string;
  level: LogLevel;
  message: string;
  date: Moment;
}

export class LogUtils {
  static fromObject(source: any): Log {
    const date = fromDateISOString(source.date);
    return <Log>{
      loggerName: source.loggerName,
      date,
      level: (source.level as LogLevel) || LogLevel.INFO,
      message: source.message,
    };
  }

  static asObject(source: Log): any {
    const date = toDateISOString(source.date);
    return {
      loggerName: source.loggerName,
      date,
      level: +source.level,
      message: source.message,
    };
  }

  static fromObjects(sources: any[]): Log[] {
    return (sources || []).map(this.fromObject);
  }

  static levelToString(level: LogLevel): string {
    return LogLevel[level].toUpperCase();
  }

  static levelToColor(level: LogLevel): AppColors {
    switch (level) {
      case LogLevel.TRACE:
        return 'primary';
      case LogLevel.DEBUG:
        return 'secondary';
      case LogLevel.INFO:
        return 'tertiary';
      case LogLevel.WARN:
        return 'warning';
      case LogLevel.ERROR:
        return 'accent';
      case LogLevel.FATAL:
        return 'danger';
      default:
        return 'medium';
    }
  }
}
