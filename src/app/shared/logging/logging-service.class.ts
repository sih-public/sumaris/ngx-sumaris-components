import { ILogger, ILoggingAppender, ILoggingService, Log, LogUtils } from './logger.model';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DateUtils } from '../dates';
import { APP_STORAGE, IStorage } from '../storage/storage.utils';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { debounceTime } from 'rxjs/operators';
import { isEmptyArray, isNotEmptyArray } from '../functions';
import { LoggingServiceConfig } from './logging-service.config';
import { LogLevel } from './log-level.model';
import { StartableService } from '../services/startable-service.class';

export class Logger implements ILogger {
  constructor(
    private _name: string,
    private _appender: ILoggingAppender
  ) {}

  trace(methodName: string, ...params: any[]): void {
    this._appender.append(this._name, LogLevel.TRACE, this.toString(methodName, ...params));
  }

  debug(methodName: string, ...params: any[]): void {
    this._appender.append(this._name, LogLevel.DEBUG, this.toString(methodName, ...params));
  }

  info(methodName: string, ...params: any[]): void {
    this._appender.append(this._name, LogLevel.INFO, this.toString(methodName, ...params));
  }

  warn(methodName: string, ...params: any[]): void {
    this._appender.append(this._name, LogLevel.WARN, this.toString(methodName, ...params));
  }

  error(methodName: string, ...params: any[]) {
    this._appender.append(this._name, LogLevel.ERROR, this.toString(methodName, ...params));
  }

  fatal(methodName: string, ...params: any[]) {
    this._appender.append(this._name, LogLevel.FATAL, this.toString(methodName, ...params));
  }

  private toString(methodName, ...params: any[]) {
    return (
      `[${methodName}] ` +
      (params || [])
        .map((p) => {
          if (typeof p === 'string') return p;
          if (typeof p === 'number') return '' + p;
          return JSON.stringify(p);
        })
        .join(' ')
    );
  }
}

export const APP_LOGGING_SERVICE = new InjectionToken<ILoggingService>('LoggingService');

@Injectable()
export class LoggingService extends StartableService<void> implements ILoggingService, ILoggingAppender {
  _dataSubject = new BehaviorSubject<Log[]>([]);
  _debounceTimeMs: number;
  _storageKey: string;
  _maxMessages: number;
  _saving = false;

  constructor(
    @Inject(APP_STORAGE) private storage: IStorage,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(storage);
    this.configure(environment.logging);
  }

  async configure(config: LoggingServiceConfig) {
    await this.restart(config);
  }

  protected async ngOnStart(opts?: LoggingServiceConfig): Promise<void> {
    this._debounceTimeMs = opts?.localStorageAppender?.debounceTimeMs || 1000 /* = 1s */;
    this._maxMessages = opts?.localStorageAppender?.maxMessages || 250;
    this._storageKey = opts?.localStorageAppender?.localStorageKey || 'log';

    console.info(
      `[logging] Configuring logging... {localStorageKey: '${this._storageKey}', maxMessages: ${this._maxMessages}, savePeriodMs: ${this._debounceTimeMs}}`
    );

    // Save timer
    this.registerSubscription(
      this._dataSubject
        // Avoid to many call
        .pipe(debounceTime(this._debounceTimeMs))
        .subscribe(() => this.storeLocally())
    );
  }

  getLogger(loggerName: string): ILogger {
    return new Logger(loggerName, this);
  }

  append(loggerName: string, level: LogLevel, message: string) {
    this._dataSubject.next([...this._dataSubject.value, <Log>{ loggerName, level, message, date: DateUtils.moment() }]);
  }

  protected async storeLocally() {
    if (this._saving) return;
    this._saving = true;
    try {
      const newMessages: any[] = (this._dataSubject.value || []).map(LogUtils.asObject);

      if (isEmptyArray(newMessages)) return; // Skip if no data
      this._dataSubject.next([]); // Reset log

      const oldValue = await this.storage.get(this._storageKey);
      const oldMessages: any[] = typeof oldValue === 'string' ? JSON.parse(oldValue) : oldValue || [];
      let lastMessages = [...(oldMessages || []), ...newMessages];
      if (lastMessages.length > this._maxMessages) {
        lastMessages = lastMessages.slice(lastMessages.length - this._maxMessages);
      }

      if (isNotEmptyArray(lastMessages)) {
        console.info(`[logging] Saving ${newMessages.length} messages to storage...`);
        await this.storage.set(this._storageKey, lastMessages);
      }
    } catch (err) {
      console.error(err);
    } finally {
      this._saving = false;
    }
  }
}
