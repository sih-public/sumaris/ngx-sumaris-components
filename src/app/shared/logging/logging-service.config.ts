// Based on https://github.com/Ritzlgrmft/ionic-logging-service/blob/main/projects/ionic-logging-service/src/lib/logging-service.configuration.ts
export interface LoggingServiceConfig {
  localStorageAppender?: {
    /**
     * Key which is used to store the messages in the local storage.
     *
     * Default: 'log'.
     */
    localStorageKey: string;

    /**
     * Maximum number of log messages stored by the appender.
     *
     * Default: 250.
     */
    maxMessages?: number;

    /**
     * Threshold.
     *
     * Valid values are: ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN
     *
     * Default: WARN.
     */
    threshold?: string;

    debounceTimeMs?: number;
  };
}
