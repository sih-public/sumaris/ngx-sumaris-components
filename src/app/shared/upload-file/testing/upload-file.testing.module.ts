import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { UploadFileTestingPage } from './upload-file.testing';

const routes: Routes = [
  {
    path: 'upload-file',
    pathMatch: 'full',
    component: UploadFileTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [UploadFileTestingPage],
  exports: [RouterModule, UploadFileTestingPage],
})
export class UploadFileTestingModule {}
