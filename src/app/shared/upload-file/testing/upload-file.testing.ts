import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UploadFilePopoverOptions } from '../upload-file-popover.component';
import { Observable, Subject, timer } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { map, takeUntil } from 'rxjs/operators';
import { sleep } from '../../functions';
import { FileEvent } from '../upload-file.model';
import { FilesUtils } from '../../file/file.utils';

@Component({
  selector: 'upload-file-testing',
  templateUrl: 'upload-file.testing.html',
})
export class UploadFileTestingPage {
  constructor(private popoverController: PopoverController) {}

  async showPopover(event: Event, opts?: Partial<UploadFilePopoverOptions<any>>) {
    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      uploadFn: (file) => this.uploadFile(file),
      deleteFn: (file) => this.deleteFile(file),
      ...opts,
    });

    const filenames = (data || []).map((file) => file.response?.body?.finalName);
    console.info('Popover result: ', filenames);
  }

  /**
   * Simulate a upload function, with progression
   *
   * @param file
   */
  uploadFile(file: File): Observable<FileEvent<{ finalName: string }>> {
    const $stop = new Subject<void>();
    const loadStep = file.size / 10;
    let loaded = 0;
    return timer(0, 250).pipe(
      takeUntil($stop),
      map(() => {
        // pause in middle
        // if (loaded > 0) return { type: HttpEventType.UploadProgress, total: file.size, loaded };

        loaded += loadStep;

        // Return progression
        if (loaded < file.size) {
          return { type: HttpEventType.UploadProgress, total: file.size, loaded };
        }

        // Stop the timer
        setTimeout(() => $stop.next(), 1000);

        // Return final response
        return new HttpResponse({ body: { finalName: file.name } });
      })
    );
  }

  async deleteFile(file: File): Promise<boolean> {
    console.info('[testing] Simulate remote deletion... (waiting 2s)');
    await sleep(2000);
    return true;
  }
}
