import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UploadFileComponent } from './upload-file.component';
import { FileDeleteFn, FileUploadFn, UploadFile } from './upload-file.model';
import { isEmptyArray, isNotEmptyArray, isNotNilOrBlank } from '../functions';
import { TranslateService } from '@ngx-translate/core';

export interface UploadFilePopoverOptions<T> {
  title?: string;

  uploadFn: FileUploadFn<T>;
  deleteFn?: FileDeleteFn<any>;

  instantUpload?: boolean;
  uniqueFile?: boolean;
  fileExtension?: string;
  maxParallelUpload?: number;
}

@Component({
  selector: 'app-upload-file-popover',
  templateUrl: './upload-file-popover.component.html',
  styleUrls: ['./upload-file-popover.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploadFilePopover implements UploadFilePopoverOptions<any> {
  @ViewChild('uploader', { static: true }) uploader: UploadFileComponent;

  @Input() fileExtension: string;
  @Input() title: string;
  @Input() uniqueFile = false;
  @Input() instantUpload = false;
  @Input() uploadFn: FileUploadFn<any>;
  @Input() deleteFn: FileDeleteFn<any>;
  @Input() maxParallelUpload: number;

  importing = false;
  error: string;

  get files(): UploadFile<any>[] {
    return this.uploader.files;
  }

  get disabled() {
    return this.importing || isEmptyArray(this.files);
  }

  get valid() {
    return isNotEmptyArray(this.files);
  }

  constructor(
    protected popoverController: PopoverController,
    protected translate: TranslateService,
    protected cd: ChangeDetectorRef
  ) {}

  async onValidate(event: any): Promise<any> {
    event?.stopPropagation();

    // Avoid multiple call
    if (this.importing) return;

    console.debug('[upload-file-popover] Validate form: will upload all files...');

    try {
      this.importing = true;
      this.resetError();

      try {
        // Wait processing files finished
        await this.uploader.waitIdle();

        // upload all files
        await this.uploader.uploadFiles();

        const files: UploadFile<any>[] = (this.uploader.processedFiles || []).filter((file) => !file.deleting); // Remove deleting file (deletion in progress)

        // Nothing to upload: close popover
        if (isEmptyArray(files)) return this.cancel();

        // check errors
        const errors = files.map((file) => file.error).filter(isNotNilOrBlank);

        // If error: stop
        if (errors.length) {
          errors.forEach((error) => console.error(error));
          throw { message: 'FILE.UPLOAD.ERROR' };
        }

        // return files
        await this.popoverController.dismiss(files);
      } catch (err) {
        this.error = (err && err.message) || err;
      }
    } finally {
      this.importing = false;
      this.cd.markForCheck();
    }
  }

  cancel() {
    return this.popoverController.dismiss();
  }

  protected resetError() {
    if (this.error) {
      this.error = null;
      this.cd.markForCheck();
    }
  }
}
