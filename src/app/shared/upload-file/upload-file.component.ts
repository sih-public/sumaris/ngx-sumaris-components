import { ChangeDetectorRef, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { catchError, debounceTime, filter, finalize, first, map, switchMap, tap } from 'rxjs/operators';
import { BehaviorSubject, forkJoin, Observable, of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { FileDeleteFn, FileUploadFn, isProgressEvent, isResponseEvent, UploadFile } from './upload-file.model';
import { isNil, isNotNil } from '../functions';
import { waitFor, WaitForOptions } from '../observables';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
})
export class UploadFileComponent {
  @ViewChild('fileDropRef', { static: false }) fileDropEl: ElementRef;

  @Input() fileExtension: string;
  @Input() uniqueFile = false;
  @Input() instantUpload = false;
  @Input() uploadFn: FileUploadFn<any>;
  @Input() deleteFn: FileDeleteFn<any>;
  @Input() maxParallelUpload: number;

  files: UploadFile<any>[] = [];
  uploading = false;

  get processingFiles(): UploadFile<any>[] {
    return this.files.filter((f) => !f.deleting && isNotNil(f.progress) && f.progress < 1);
  }

  get processedFiles(): UploadFile<any>[] {
    return this.files.filter((f) => !f.deleting && f.progress === 1);
  }

  get processingFilesCount(): number {
    return this.processingFiles.length;
  }

  constructor(
    protected cd: ChangeDetectorRef,
    protected translate: TranslateService
  ) {}

  /**
   * on file drop handler
   */
  onFileDropped(event) {
    this.prepareFilesList(event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  /**
   * Delete file from files list
   *
   * @param index (File index)
   */
  async deleteFile(index: number) {
    const file = this.files[index];

    if (file.progress === 1 && this.deleteFn) {
      if (file.deleting) {
        console.warn(`[upload-file] This file already deleting`);
        return;
      }
      try {
        file.deleting = true;
        const deleted = await this.deleteFn(file);
        if (!deleted) return;
      } catch (err) {
        console.error((err && err.message) || err);
        return;
      } finally {
        file.deleting = false;
      }

      // Make sure index not changed
      index = this.files.indexOf(file);
      if (index === -1) return; // Not found (already removed ?)
    } else if (file.progress > 0) {
      console.warn(`[upload-file] Cannot remove this file while uploading`);
      return;
    }

    this.files.splice(index, 1);
    this.cd.markForCheck();
  }

  /**
   * Convert Files list to normal array list
   *
   * @param files (Files List)
   */
  prepareFilesList(files: FileList | File[]) {
    let items: File[];
    if (files instanceof FileList) {
      items = [];
      for (let i = 0; i < files.length; i++) {
        const item = files.item(i);
        items.push(item);
      }
    } else {
      items = files;
    }
    this.files.push(...items);

    this.fileDropEl.nativeElement.value = '';
    this.cd.markForCheck();

    // Start upload
    if (this.instantUpload) {
      this.uploadFiles(items);
    }
  }

  /**
   * Execute upload
   */
  async uploadFiles(files?: File[]): Promise<UploadFile<any>[]> {
    this.uploading = true;
    if (!files) {
      console.info('[upload-file] Uploading all files...');
      files = this.files;
    }

    const $processingFileCountChange = new BehaviorSubject(this.processingFilesCount);
    const $changes = new Subject<void>();
    $changes.subscribe((_) => {
      this.cd.markForCheck();
      $processingFileCountChange.next(this.processingFilesCount);
    });

    const jobs: Observable<UploadFile<any>>[] = files
      .map((f) => f as UploadFile<any>)
      .filter((file) => file && isNil(file.progress) && !file.error) // Skip if already started or in error
      .map((file, i) => {
        // Wait if need
        if (isNotNil(this.maxParallelUpload)) {
          return $processingFileCountChange.pipe(
            debounceTime(i), // call of filter() must be different for waiting job
            filter((count) => count < this.maxParallelUpload),
            map((_) => {
              // Start uploading
              if (isNil(file.progress)) file.progress = 0;
              $changes.next();
              return file;
            }),
            first()
          );
        }
        // Start uploading
        if (isNil(file.progress)) file.progress = 0;
        return of(file);
      })

      .map((file$) =>
        file$.pipe(
          filter(isNotNil),
          // Log
          tap((file) => console.debug(`[upload-file] ${file.name}: uploading...`)),

          // Start uploading
          switchMap((file) =>
            this.uploadFn(file).pipe(
              filter(isNotNil),
              map((event) => {
                // Progress event
                if (isProgressEvent(event)) {
                  if (isNotNil(event.loaded) && event.loaded >= 0) {
                    file.progress = Math.min(1, event.loaded / (event.total || 1));
                  } else {
                    file.progress = -1; // Indeterminate progression
                  }
                }

                // Response event
                else if (isResponseEvent(event)) {
                  file.progress = 1;
                  file.response = event;
                  console.debug(`[upload-file] ${file.name}: ${event.statusText || 'OK'}`, file.response);
                }

                // Other events: ignore
                else {
                  // DEBUG
                  console.debug('[upload-file] Ignoring a file event: ', event);
                }
                return file;
              }),
              catchError((err) => {
                console.error(err);
                file.error = (err && (err.message || err.error?.message)) || 'FILE.UPLOAD.ERROR';
                return of(file);
              }),
              tap(() => {
                $changes.next();
                this.cd.markForCheck();
              })
            )
          )
        )
      );

    // Wait jobs to finish
    return forkJoin(jobs)
      .pipe(
        tap((_) => $changes.complete()),
        finalize(() => (this.uploading = false))
      )
      .toPromise();
  }

  waitIdle(opts?: WaitForOptions): Promise<void> {
    return waitFor(() => this.processingFilesCount === 0, opts);
  }
}
