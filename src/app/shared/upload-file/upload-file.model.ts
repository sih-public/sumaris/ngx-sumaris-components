import { Observable } from 'rxjs';
import { HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { isNotNil } from '../functions';

export class UploadFile<T> extends File {
  progress?: number;
  deleting?: boolean;
  error?: string;

  response?: FileResponse<T> | HttpResponse<T>;
}

export type FileUploadFn<T> = (file: File) => Observable<FileEvent<T> | HttpEvent<T>>;
export type FileDeleteFn<T> = (file: UploadFile<T>) => Promise<boolean>;
export interface FileProgressEvent {
  type: HttpEventType.DownloadProgress | HttpEventType.UploadProgress;
  loaded: number;
  total?: number;
  statusText?: string;
}
export class FileResponse<T> {
  readonly body: T | null;
  readonly type = HttpEventType.Response;
  readonly status: number;
  readonly statusText: string;
  constructor(init?: { body?: T | null; statusText?: string; status?: number }) {
    this.body = isNotNil(init?.body) ? init.body : null;
    this.status = init?.status || 0;
    this.statusText = init?.statusText || 'OK';
  }
}

export declare type FileEvent<T> = HttpProgressEvent | HttpResponse<T> | FileProgressEvent | FileResponse<T>;

export function isProgressEvent(event: HttpEvent<any> | FileEvent<any>): event is HttpProgressEvent | FileProgressEvent {
  return event && (event.type === HttpEventType.UploadProgress || event.type === HttpEventType.DownloadProgress);
}
export function isResponseEvent<T>(event: HttpEvent<T> | FileEvent<T>): event is HttpResponse<T> | FileResponse<T> {
  return event && event.type === HttpEventType.Response;
}
