import { ObjectMap, Property } from '../types';
import { isNotNil, joinPropertiesPath } from '../functions';
import { InjectionToken, Injector } from '@angular/core';
import { MatAutocompleteFieldConfig } from '../material/autocomplete/material.autocomplete.config';
import { UserProfileLabel } from '../../core/services/model/person.model';
import { FloatLabelType } from '@angular/material/form-field';

export declare type DisplayFn = (obj: any) => string;

export declare type EqualsFn = (o1: any, o2: any) => boolean;

export declare type CompareFn = (o1: any, o2: any) => number;

export declare type FormFieldType =
  | 'integer'
  | 'double'
  | 'boolean'
  | 'string'
  | 'enum'
  | 'enums'
  | 'color'
  | 'peer'
  | 'entity'
  | 'entities'
  | 'date'
  | 'dateTime';
export declare type AppFloatLabelType = FloatLabelType | 'never';

export declare interface FormFieldDefinition<K = string, T = FormFieldType, E = any> {
  key: K;
  type: T;
  label: string;
  minValue?: number;
  maxValue?: number;
  maximumNumberDecimals?: number;
  defaultValue?: any;
  isTransient?: boolean; // Useful only for remote configuration
  values?: (string | Property)[] | InjectionToken<(string | Property)[]>;
  autocomplete?: MatAutocompleteFieldConfig<E>;

  disabled?: boolean;
  required?: boolean;
  extra?: {
    [key: string]: {
      disabled?: boolean;
      required: boolean;
    };
  };
  minProfile?: UserProfileLabel;

  /**
   * Use for automatic serialization/deserialization to/from string (eg. using the autocomplete suggestFn).
   * Default value: 'id'
   */
  serializeAttribute?: string;

  /**
   * Use to serialize an entity into string (see AppPropertiesUtils.***asObject())
   * @param value
   */
  serialize?: (value: E) => string;

  /**
   * Use to deserialize a string into an entity (see AppPropertiesUtils.***fromObject())
   * @param value
   */
  deserialize?: (value: string) => E | Promise<E>;
}
export declare type FormFieldDefinitionMap = ObjectMap<FormFieldDefinition>;

export class FormFieldDefinitionUtils {
  /**
   * Prepares an array of form field definitions by using the specified injector to process each definition.
   *
   * @param {Injector} injector - The injector used to process each form field definition.
   * @param {FormFieldDefinition[]} defs - An array of form field definitions to be prepared.
   * @return {FormFieldDefinition[]} An array of prepared form field definitions.
   */
  static prepareDefinitions(injector: Injector, defs: FormFieldDefinition[]): FormFieldDefinition[] {
    return defs?.map((def) => FormFieldDefinitionUtils.prepareDefinition(injector, def));
  }

  /**
   * Prepares the definition by processing its type and optionally setting the values property.
   *
   * @param {Injector} injector - The injector used to get the necessary services.
   * @param {FormFieldDefinition} def - The form field definition to be prepared.
   * @return {FormFieldDefinition} - The prepared form field definition with updated values if the type is 'enum' or 'enums'.
   */
  static prepareDefinition(injector: Injector, def: FormFieldDefinition): FormFieldDefinition {
    if (def.type === 'enum' || def.type === 'enums') {
      return {
        ...def,
        values: FormFieldDefinitionUtils.getEnumValues(injector, def.values),
      };
    }
    return def;
  }

  /**
   * Retrieves an array of Property objects from the provided enum values.
   *
   * @param {Injector} injector - The Angular injector to resolve the InjectionToken.
   * @param {(string | Property)[] | InjectionToken<(string | Property)[]>} enumValues - The enum values, which can be an array of strings, array of Property objects, or an InjectionToken that resolves to such an array.
   * @return {Property[]} An array of Property objects.
   */
  static getEnumValues(injector: Injector, enumValues: (string | Property)[] | InjectionToken<(string | Property)[]>): Property[] {
    // Resolve token
    if (enumValues instanceof InjectionToken) {
      const values = injector.get(enumValues);
      return Array.isArray(values) ? FormFieldDefinitionUtils.asPropertyArray(values) : [];
    }

    // Normalize string[] => Property[]
    return FormFieldDefinitionUtils.asPropertyArray(enumValues as (string | Property)[]);
  }

  static getDisplayValueFn(definition: FormFieldDefinition): DisplayFn {
    if (definition.autocomplete && definition.autocomplete.displayWith) {
      return (obj: any) => definition.autocomplete.displayWith(obj);
    }
    const attributes = (definition.autocomplete && definition.autocomplete.attributes) || ['label', 'name'];
    return (obj: any) => obj && joinPropertiesPath(obj, attributes);
  }

  private static asPropertyArray(values: (string | Property)[]): Property[] {
    return (Array.isArray(values) ? values : [])
      .map((v) => (typeof v === 'string' ? <Property>{ key: v, value: v } : typeof v === 'object' ? (v as Property) : null))
      .filter(isNotNil);
  }
}

export abstract class FormFieldValuesHolder {
  getAsBoolean(data: ObjectMap, definition: FormFieldDefinition): boolean {
    const value = this.get(data, definition);
    return isNotNil(value) ? value && value !== 'false' : undefined;
  }

  getAsInt(data: any, definition: FormFieldDefinition): number {
    const value = this.get(data, definition);
    return isNotNil(value) ? parseInt(value) : undefined;
  }

  getAsNumbers(data: any, definition: FormFieldDefinition): number[] {
    const value = this.get(data, definition);
    if (typeof value === 'string') return value.split(',').map(parseFloat) || undefined;
    return isNotNil(value) ? [parseFloat(value)] : undefined;
  }

  getAsStrings(data: any, definition: FormFieldDefinition): string[] {
    const value = this.get(data, definition);
    return (value && value.split(',')) || undefined;
  }

  get<T = string>(data: any, definition: FormFieldDefinition): T {
    return isNotNil(data[definition.key]) ? data[definition.key] : definition.defaultValue || undefined;
  }
}
