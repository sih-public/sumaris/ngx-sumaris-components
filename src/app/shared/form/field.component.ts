import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  numberAttribute,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl, Validators } from '@angular/forms';
import { isBlankString, isNilOrBlank, isNotNil, isNotNilBoolean, isNotNilOrBlank, joinPropertiesPath, toBoolean } from '../functions';
import { AppFloatLabelType, DisplayFn, FormFieldDefinition, FormFieldDefinitionUtils, FormFieldType } from './field.model';
import { TranslateService } from '@ngx-translate/core';
import { getColorContrast } from '../graph/colors.utils';
import { asInputElement, filterNumberInput, selectInputContentFromEvent } from '../inputs';
import { toDateISOString } from '../dates';
import { Property } from '../types';
import { ThemePalette } from '@angular/material/core';
import { PredefinedColors } from '@ionic/core';
import { MatAutocompleteField } from '../material/autocomplete/material.autocomplete';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';

const noop = () => {};

@Component({
  selector: 'app-form-field',
  styleUrls: ['./field.component.scss'],
  templateUrl: './field.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AppFormField),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppFormField implements OnInit, ControlValueAccessor {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;
  private _definition: FormFieldDefinition;

  protected _values: Property[];

  protected type: FormFieldType;
  protected numberInputStep: string;

  @Input()
  set definition(value: FormFieldDefinition) {
    if (!value || this._definition === value) return; // Skip is same or nil
    this._definition = value;
    this._onDefinitionChanged(value);
  }

  get definition(): FormFieldDefinition {
    return this._definition;
  }

  @Input({ transform: booleanAttribute }) readonly = false;
  @Input({ transform: booleanAttribute }) disabled: boolean;
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() placeholder: string;
  @Input({ transform: booleanAttribute }) compact = false;
  @Input({ transform: booleanAttribute }) required: boolean;
  @Input({ transform: booleanAttribute }) hideRequiredMarker = false;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() label: string = null;
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input({ transform: numberAttribute }) tabindex: number;
  @Input({ transform: booleanAttribute }) autofocus: boolean;

  @Input({ transform: booleanAttribute }) clearable: boolean;
  @Input() chipColor: ThemePalette | PredefinedColors = null;
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;
  @Input({ transform: booleanAttribute }) debug = false;
  @Input() panelClass: string;
  @Input() panelWidth: string;

  @Output('keyup.enter') onKeyupEnter = new EventEmitter<any>();

  get value(): any {
    return this.formControl.value;
  }

  @ViewChild('matInput') matInput: ElementRef;
  @ViewChild('autocompleteField') autocompleteField: MatAutocompleteField;

  constructor(
    protected translate: TranslateService,
    protected cd: ChangeDetectorRef,
    protected injector: Injector,
    @Optional() private formGroupDir: FormGroupDirective
  ) {}

  ngOnInit() {
    this.checkAndResolveFormControl();

    if (!this._definition) throw new Error("Missing mandatory attribute 'definition' in <app-form-field>.");
    if (typeof this._definition !== 'object') throw new Error("Invalid attribute 'definition' in <app-form-field>. Should be an object.");
    this._onDefinitionChanged(this._definition, { emitEvent: false });
  }

  writeValue(obj: any): void {
    if ((this.type === 'integer' || this.type === 'double') && Number.isNaN(obj)) {
      // DEBUG
      //if (this.debug) console.debug("WARN: trying to set NaN value, in an <app-form-field>! Reset to null");

      obj = null;
    } else if (this.type === 'boolean' && typeof obj === 'string') {
      obj = obj !== 'false';
    } else if (this.type === 'date') {
      obj = toDateISOString(obj);
    } else if (this.type === 'enums' && isNotNil(obj)) {
      let changes = false;
      if (!Array.isArray(obj)) {
        // Convert string to array
        if (typeof obj === 'string') {
          obj = obj.trim().split(/[|,]+/);
          changes = true;
        }
        // Convert object to array
        else if (typeof obj === 'object') {
          obj = [obj];
          changes = true;
        }
      }
      const updatedArray = (obj || [])
        .map((item) => {
          if (typeof item === 'string') {
            changes = true;
            return this._values.find((v) => v.key === item.trim());
          }
          return item;
        })
        .filter(isNotNil);
      if (changes) {
        obj = updatedArray;
      }
    } else if (this.type === 'entity' && isNilOrBlank(obj)) {
      obj = null;
    } else if (this.type === 'entities' && isNotNil(obj)) {
      let changes = false;

      if (!Array.isArray(obj)) {
        // Convert string to array
        if (typeof obj === 'string') {
          obj = obj.trim();
          if (obj.startsWith('[') && obj.endsWith(']')) {
            obj = JSON.parse(obj);
          } else {
            obj = obj.split(/[|,]+/);
          }
          changes = true;
        }
        // Convert object to array
        else if (typeof obj === 'object') {
          obj = [obj];
          changes = true;
        }
      }
      const updatedArray = (obj || [])
        .map((item) => {
          if (typeof item === 'string' && item.startsWith('{') && item.endsWith('}')) {
            item = JSON.parse(item);
            changes = true;
            return item;
          }
          return item;
        })
        .filter(isNotNilOrBlank);
      if (changes) {
        obj = updatedArray;
      }
    }
    if (obj !== this.formControl.value) {
      // DEBUG
      //if (this.debug) console.debug("Set <app-form-field> value: ", obj);

      this.formControl.patchValue(obj, { emitEvent: false });
      this._onChangeCallback(obj);
    }

    this.cd.markForCheck();
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (this.disabled === isDisabled) return;
    this.disabled = isDisabled;
    this.cd.markForCheck();
  }

  markAsTouched() {
    if (this.formControl.touched) {
      this.cd.markForCheck();
      this._onTouchedCallback();
    }
  }

  filterAlphanumericalInput(event: KeyboardEvent) {
    if (event.code === 'Enter' && this.onKeyupEnter.observed) {
      this.onKeyupEnter.emit(event);
      return;
    }
    // Add features (e.g. check against a pattern)
  }

  focus() {
    if (this.matInput) this.matInput.nativeElement.focus();
    else if (this.autocompleteField) this.autocompleteField.focus();
  }

  filterNumberInput = filterNumberInput;
  selectInputContentFromEvent = selectInputContentFromEvent;

  getDisplayValueFn(definition: FormFieldDefinition): DisplayFn {
    if (definition.autocomplete && definition.autocomplete.displayWith) {
      return (obj: any) => definition.autocomplete.displayWith(obj);
    }
    const attributes = (definition.autocomplete && definition.autocomplete.attributes) || ['label', 'name'];
    return (obj: any) => obj && joinPropertiesPath(obj, attributes);
  }

  getColorContrast(color: string): string | undefined {
    return isNotNilOrBlank(color) ? getColorContrast(color, true) : undefined;
  }

  /**
   * Allow to reload values, if not detected
   *
   * @param value
   */
  reloadItems(value?: any) {
    if (!this._definition) return;

    this._onDefinitionChanged(this._definition);

    if (this.autocompleteField) {
      setTimeout(() => this.autocompleteField?.reloadItems(value));
    }
  }

  /* -- protected method -- */

  protected computeNumberInputStep(definition: FormFieldDefinition): string {
    if (definition.maximumNumberDecimals > 0) {
      let step = '0.';
      if (definition.maximumNumberDecimals > 1) {
        for (let i = 0; i < definition.maximumNumberDecimals - 1; i++) {
          step += '0';
        }
      }
      step += '1';
      return step;
    } else {
      return '1';
    }
  }

  protected checkAndResolveFormControl() {
    if (this.formControl) return; // OK, there is a control

    if (this.formGroupDir) {
      // Try to find using definition.key, as formControlName
      if (!this.formControlName && this._definition.key) {
        this.formControlName = this._definition.key;
      }
      const formControlName = (this.formGroupDir.directives || []).find((d) => this.formControlName && d.name === this.formControlName);
      this.formControl = formControlName && formControlName.control;
      if (!this.formControl) {
        this.formControl = this.formGroupDir.form.get(this.formControlName) as UntypedFormControl;
      }
    }

    if (!this.formControl) {
      throw new Error("Missing attribute 'formControl' or 'formControlName' in <app-form-field> component.");
    }
  }

  protected _onDefinitionChanged(definition: FormFieldDefinition, opts?: { emitEvent?: boolean }) {
    this.type = definition.type;
    if (!this.type) throw new Error("Invalid attribute 'definition' in <app-form-field>. Missing type !");

    this.placeholder = this.placeholder || (this._definition.label && this.translate.instant(this._definition.label));
    this.required = isNotNilBoolean(this.required)
      ? this.required
      : this._definition.required || this.formControl?.validator === Validators.required || isBlankString(this.required);
    this.disabled = toBoolean(this.disabled, toBoolean(this._definition.disabled, false));

    // double
    if (this.type === 'double') {
      this.numberInputStep = this.computeNumberInputStep(definition);
    } else {
      this.numberInputStep = null;
    }

    // enum (or enums)
    if (this.type === 'enum' || this.type === 'enums') {
      // Resolve enum values
      this._values = FormFieldDefinitionUtils.getEnumValues(this.injector, definition.values);
    } else {
      this._values = [];
    }

    // Repaint
    if (!opts || opts.emitEvent !== false) {
      this.cd.markForCheck();
    }

    // Recompute tab index, because if type changed, then matInput can have changed
    this.updateTabIndex(100);
  }

  protected updateTabIndex(timeout?: number) {
    if (this.tabindex && this.tabindex !== -1) {
      setTimeout(() => {
        const element = asInputElement(this.matInput);
        if (element) {
          element.tabIndex = this.tabindex;
          this.cd.markForCheck();
        } else if (this.autocompleteField) {
          this.autocompleteField.tabindex = this.tabindex;
        }
      }, timeout || 0);
    }
  }
}
