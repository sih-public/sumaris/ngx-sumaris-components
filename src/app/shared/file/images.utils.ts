import { isNilOrBlank } from '../functions';
import { Observable, Subject } from 'rxjs';
import { FileEvent, FileResponse } from '../upload-file/upload-file.model';
import { HttpEventType } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';

export const IMAGE_DEFAULTS = {
  MAX_HEIGHT: 480,
  MAX_WIDTH: 640,
  THUMB_MAX_HEIGHT: 200,
  THUMB_MAX_WIDTH: 200,
};

export declare interface ImageResizeOptions {
  maxWidth?: number;
  maxHeight?: number;
  thumbnail?: boolean;
}

// @dynamic
export class ImagesUtils {
  /**
   * Parse a string like `<path>|<credit>` into an object `{image, credit}`
   * @param file
   **/
  static getImageAndCredit(file: string): { image: string; credits: string } {
    const parts = file?.split('|');
    return parts
      ? {
          image: parts[0],
          credits: parts[1],
        }
      : null;
  }

  /**
   * @param files
   * @deprecated Use getRandomImageWithCredit() instead
   **/
  static getRandomImage(files: string[]): string | undefined {
    const imgIndex = Math.floor(Math.random() * files.length);
    return files[imgIndex];
  }

  static getRandomImageWithCredit(files: string[]): { image: string; credits: string } {
    const imgIndex = Math.floor(Math.random() * files.length);
    return ImagesUtils.getImageAndCredit(files[imgIndex]);
  }

  static readAsDataURL(file: File, opts?: ImageResizeOptions): Observable<FileEvent<string>> {
    const $progress = new Subject<FileEvent<string>>();
    const reader = new FileReader();

    // Listen progress
    reader.onprogress = (e: ProgressEvent) => {
      const total = e.total || 1;
      const loaded = e.total || 0;
      if (loaded < total) $progress.next({ type: HttpEventType.UploadProgress, loaded, total });
    };

    // Listen end
    reader.onloadend = (_: ProgressEvent) => {
      const body = reader.result;
      if (typeof body === 'string') {
        $progress.next(new FileResponse({ body }));
        $progress.complete();
      } else {
        console.error(`[image-utils] Cannot read file '${file.name}' as text`);
        $progress.error(`Cannot read file as text`);
      }
    };

    // Listen error
    reader.onerror = (event: ProgressEvent) => {
      console.error('[image-utils] Error while reading file:', event);
      $progress.error(event);
    };

    // Start reading the file
    reader.readAsDataURL(file);

    // Resize image, if need
    if (opts) {
      return $progress.pipe(
        mergeMap(async (event) => {
          if (event instanceof FileResponse) {
            const body = await ImagesUtils.resizeBase64(event.body, opts);
            return new FileResponse({ body });
          }
          return event;
        })
      );
    }

    return $progress.asObservable();
  }

  static async readAndResizeFromUrl(fileUrl: string, opts?: ImageResizeOptions): Promise<string> {
    if (isNilOrBlank(fileUrl)) throw new Error('Illegal argument: missing fileUrl');

    // Create the temporary image element
    const img = document.createElement('img');

    // Need to avoid CORS error
    // (see https://stackoverflow.com/questions/22710627/tainted-canvases-may-not-be-exported)
    img.crossOrigin = 'anonymous';

    try {
      return await new Promise<string>((resolve, reject) => {
        img.addEventListener('load', ImagesUtils.createImageOnLoadResizeFn(resolve, reject, opts), false);
        img.addEventListener('error', reject);
        img.src = fileUrl;
      });
    } catch (err) {
      throw new Error(`Failed to load and resize image {${fileUrl}}`);
    } finally {
      img.remove();
    }
  }

  static async resizeBase64(dataUrl: string, opts?: ImageResizeOptions): Promise<string> {
    // Create the temporary image element
    const img = document.createElement('img');

    // Need to avoid CORS error
    // (see https://stackoverflow.com/questions/22710627/tainted-canvases-may-not-be-exported)
    img.crossOrigin = 'anonymous';

    try {
      return await new Promise<string>((resolve, reject) => {
        img.addEventListener('load', ImagesUtils.createImageOnLoadResizeFn(resolve, reject, opts), false);
        img.addEventListener('error', reject);
        img.src = dataUrl;
      });
    } finally {
      img.remove();
    }
  }

  protected static createImageOnLoadResizeFn(resolve, reject, opts?: ImageResizeOptions) {
    return function (event) {
      const maxWidth = opts && opts.thumbnail ? IMAGE_DEFAULTS.THUMB_MAX_WIDTH : (opts && opts.maxWidth) || IMAGE_DEFAULTS.MAX_WIDTH;
      const maxHeight = opts && opts.thumbnail ? IMAGE_DEFAULTS.THUMB_MAX_HEIGHT : (opts && opts.maxHeight) || IMAGE_DEFAULTS.MAX_HEIGHT;
      let width = event.target.width;
      let height = event.target.height;

      const canvas = document.createElement('canvas');
      let ctx;

      try {
        // Thumbnail: resize and crop (to the expected size)
        if (opts && opts.thumbnail) {
          // landscape
          if (width > height) {
            width *= maxHeight / height;
            height = maxHeight;
          }

          // portrait
          else {
            height *= maxWidth / width;
            width = maxWidth;
          }
          canvas.width = maxWidth;
          canvas.height = maxHeight;
          ctx = canvas.getContext('2d');
          const xoffset = Math.trunc((maxWidth - width) / 2 + 0.5);
          const yoffset = Math.trunc((maxHeight - height) / 2 + 0.5);
          ctx.drawImage(
            event.target,
            xoffset, // x1
            yoffset, // y1
            maxWidth + -2 * xoffset, // x2
            maxHeight + -2 * yoffset // y2
          );
        }

        // Resize, but keep the full image
        else {
          // landscape
          if (width > height) {
            if (width > maxWidth) {
              height *= maxWidth / width;
              width = maxWidth;
            }
          }

          // portrait
          else {
            if (height > maxHeight) {
              width *= maxHeight / height;
              height = maxHeight;
            }
          }

          canvas.width = width;
          canvas.height = height;
          ctx = canvas.getContext('2d');

          // Resize the whole image
          ctx.drawImage(event.target, 0, 0, canvas.width, canvas.height);
        }

        // Get image content as data URL
        const base64 = canvas.toDataURL();
        resolve(base64);
      } catch (err) {
        reject(err);
      } finally {
        canvas.remove();
      }
    };
  }
}
