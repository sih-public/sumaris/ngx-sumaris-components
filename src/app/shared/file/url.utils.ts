import { isNilOrBlank } from '../functions';

export abstract class UrlUtils {
  /**
   * Determines if a given URL is a relative URL.
   *
   * @param {string | null} href - The URL to check.
   * @return {boolean} True if the URL is relative, false otherwise.
   */
  static isRelativeUrl(href: string | null): boolean {
    if (!href) return false;
    return !href.startsWith('http:') && !href.startsWith('https:') && !href.startsWith('mailto:') && !href.startsWith('tel:');
  }

  /**
   * Determines if a given URL is an internal URL.
   *
   * @param {string | null} href - The URL to be checked. Can be a string representing the URL or null.
   * @return {boolean} Returns true if the URL is internal, otherwise false.
   */
  static isInternalUrl(href: string | null): boolean {
    if (!href) return false;
    return href.startsWith(window.location.origin) || this.isRelativeUrl(href);
  }

  static isExternalUrl(href: string | null): boolean {
    if (!href) return false;
    return !this.isInternalUrl(href);
  }

  /**
   * Removes the query string from the given URL, returning only the base URL.
   *
   * @param {string} url - The URL from which the query string will be stripped.
   * @return {string} The URL without the query string.
   */
  static stripQuery(url: string): string {
    return /[^?]*/.exec(url)[0];
  }

  /**
   * Removes the fragment and query components from a given URL string.
   *
   * @param {string} url - The URL string to process.
   * @return {string} The URL without its fragment and query components.
   */
  static stripFragmentAndQuery(url: string): string {
    if (!url) return url;
    return this.stripQuery(/[^#]*/.exec(url)[0]);
  }

  /**
   * Extracts the fragment identifier from the given URL.
   *
   * @param {string} url - The URL from which to extract the fragment.
   * @return {string} The fragment identifier of the URL, excluding the '#' symbol.
   */
  static getFragment(url: string): string {
    if (isNilOrBlank(url)) return '';

    const match = url.match(/#(.*)$/);
    return match ? match[1] : '';
  }

  static resolveRelativeUrl(baseUrl: string, relativePath: string, opts?: {absoluteByDefault: boolean}): string {
    if (!baseUrl) throw new Error("Missing 'baseUrl' argument");
    if (!relativePath) throw new Error("Missing 'relativePath' argument");
    if (!this.isRelativeUrl(relativePath)) throw new Error("Should be a relative path");

    // Detect absolute URL
    if (relativePath.startsWith('/') || (opts?.absoluteByDefault && !relativePath.startsWith('.'))) {
      baseUrl = this.getRootUrl(baseUrl);
    }

    return UrlUtils.normalizeUrl(UrlUtils.concat(baseUrl, relativePath));
  }

  /**
   * Resolves a given URL by normalizing its parts, removing unnecessary path segments like "." and "..".
   *
   * @param {string} url - The URL to be resolved. Must be a non-empty string.
   * @return {string} The resolved and cleaned URL string.
   * @throws {Error} If the provided URL is missing or undefined.
   */
  static normalizeUrl(url: string): string {
    if (!url) throw new Error("Missing 'url' argument");
    return this.cleanUrl(url)
      .split('/')
      .reduce((res, part) => {
        if (part === '.') return res;
        if (part === '..' && res.length > 0) return res.slice(0, -1);
        return res.concat(part);
      }, [])
      .join('/');
  }

  /**
   * Extracts and returns the root URL from the given complete URL string.
   *
   * @param {string} url - The complete URL string from which the root URL will be extracted.
   * @return {string} The root URL including the protocol and host (and port if present).
   * @throws {Error} Throws an error if the provided URL is missing or is in an invalid format.
   */
  static getRootUrl(url: string): string {
    if (!url) throw new Error("Missing 'url' argument");
    let [protocol, path] = url.split('//', 2);
    if (!protocol || isNilOrBlank(path)) throw new Error(`Invalid URL format: ${url}`);
    const hostAndPort = path.split('/', 2)[0];
    return `${protocol}//${hostAndPort}`;
  }

  /**
   * Retrieves the parent URL or directory path from the provided URL.
   * This method handles both relative and absolute URLs.
   *
   * @param {string} url - The URL from which to derive the parent path.
   *                       Must be a valid URL or relative path.
   * @return {string} The parent URL or directory path. Throws an error if the input is invalid.
   */
  static getParent(url: string): string {
    if (!url) throw new Error("Missing 'url' argument");
    const cleanUrl = this.stripFragmentAndQuery(url);
    if (cleanUrl.endsWith('/')) return cleanUrl; // Skip if already a parent directory
    const parentPath = cleanUrl + '/' + '..';
    if (this.isRelativeUrl(url)) {
      return this.resolveRelativeUrl(window.location.href, parentPath);
    }
    return this.normalizeUrl(parentPath);
  }

  /**
   * Cleans the provided URL by encoding it safely and replacing specific patterns.
   *
   * @param {string} href - The URL string to be cleaned.
   * @return {string|null} The cleaned and encoded URL string, or null if an error occurs.
   */
  static cleanUrl(href: string): string {
    try {
      href = encodeURI(href).replace(/%25/g, '%');
    } catch (e) {
      return null;
    }
    return href;
  }

  static addTrailingSlash(url: string): string {
    return url.endsWith('/') ? url : url + '/';
  }

  static addStartingSlash(url: string): string {
    return url.startsWith('/') ? url : '/' + url;
  }

  static removeStartingSlash(url: string): string {
    return url.startsWith('/') ? url.substring(1) : url;
  }

  static concat(basePath: string, relativePath: string): string {
    if (!basePath) return relativePath;
    if (!relativePath) return basePath;
    return this.addTrailingSlash(basePath) + this.removeStartingSlash(relativePath);
  }
}
