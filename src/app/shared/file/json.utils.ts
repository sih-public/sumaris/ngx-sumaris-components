import { FilesUtils } from './file.utils';
import { Observable } from 'rxjs';
import { FileEvent, FileResponse } from '../upload-file/upload-file.model';
import { filter, map } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { isNil, isNotNil } from '../functions';
import { stringToU8a } from '@polkadot/util';
import { TranslateService } from '@ngx-translate/core';

//@dynamic
export class JsonUtils {
  static DEFAULT_ENCODING = 'UTF-8';

  static exportToFile(
    content: any,
    opts?: {
      filename?: string;
      encoding?: string;
      type?: string;
    }
  ) {
    if (isNil(content)) return; // Skip if empty

    // Create text content
    const textContent = content === 'string' ? content : JSON.stringify(content);

    // Write to file
    FilesUtils.writeTextToFile(textContent, {
      type: 'application/json',
      filename: 'export.json',
      ...opts,
    });
  }

  static writeToFile(
    content: any,
    opts?: {
      filename?: string;
      type?: string;
    }
  ) {
    if (!content || typeof content !== 'object') return; // Skip if empty

    opts = {
      type: 'application/json',
      filename: 'export.json',
      ...opts,
    };

    // Create text content
    const arrayUt8 = stringToU8a(JSON.stringify(content));

    const blob = new Blob([arrayUt8], { type: opts.type });
    blob['lastModifiedDate'] = new Date().toISOString();
    blob['name'] = opts.filename;

    return <File>blob;
  }

  static parseFile<T = any>(file: File, opts?: { encoding?: string }): Observable<FileEvent<T>> {
    return FilesUtils.readAsText(file, opts?.encoding).pipe(
      map((e) => {
        if (e.type === HttpEventType.UploadProgress) {
          const loaded = Math.round(e.loaded * 0.8);
          return { ...e, loaded };
        } else if (e instanceof FileResponse) {
          const body = e.body;
          const data = JSON.parse(body) as T;
          return new FileResponse<T>({ body: data });
        }
      }),
      filter(isNotNil)
    );
  }

  static getLocalizedEncoding(translate: TranslateService, defaultEncoding = JsonUtils.DEFAULT_ENCODING): string {
    const key = 'FILE.JSON.ENCODING';
    const encoding = translate?.instant(key);
    if (encoding && encoding !== key) return encoding;
    return defaultEncoding;
  }
}
