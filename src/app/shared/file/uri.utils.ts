export class UriUtils {
  /**
   * Extract the filename, from an uri (using the last slash)
   *
   * @param uri
   */
  static getFilename(uri: string): string {
    if (!uri) return uri;
    let filename = uri.trim();

    // Get last part (or all string, if no '/')
    const lastSlashIndex = filename.lastIndexOf('/');
    if (lastSlashIndex !== -1 && lastSlashIndex !== uri.length - 1) {
      filename = filename.substring(lastSlashIndex + 1);
    }

    // Remove query params
    const queryParamIndex = filename.indexOf('?');
    if (queryParamIndex !== -1) {
      filename = filename.substring(0, queryParamIndex);
    }

    // Remove fragment
    const fragmentParamIndex = filename.indexOf('#');
    if (fragmentParamIndex !== -1) {
      filename = filename.substring(0, fragmentParamIndex);
    }

    return filename;
  }
}
