import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { FileEvent, FileResponse } from '../upload-file/upload-file.model';
import { FilesUtils } from './file.utils';
import { isNotNil } from '../functions';
import { TranslateService } from '@ngx-translate/core';

//@dynamic
export class CsvUtils {
  static DEFAULT_SEPARATOR = ',';
  static DEFAULT_ENCODING = 'UTF-8';

  static exportToFile(
    rows: object[],
    opts?: {
      filename?: string;
      headers?: string[];
      separator?: string;
      encoding?: string;
      type?: string;
    }
  ) {
    if (!rows || !rows.length) return; // Skip if empty

    // Create text content
    const separator = opts?.separator || ',';
    const protectCellRegexp = new RegExp('/("|' + separator + '|\n)/g');
    const keys = Object.keys(rows[0]);
    const headers = opts?.headers || keys;
    const textContent =
      // Header row
      headers.join(separator) +
      '\n' +
      // Data rows
      rows
        .map((row) =>
          keys
            .map((k) => {
              const cell = row[k] === null || row[k] === undefined ? '' : row[k];
              let cellStr: string = cell instanceof Date ? cell.toLocaleString() : cell.toString().replace(/"/g, '""');
              if (protectCellRegexp.test(cellStr)) {
                cellStr = `"${cellStr}"`;
              }
              return cellStr;
            })
            .join(separator)
        )
        .join('\n');

    // Write to file
    FilesUtils.writeTextToFile(textContent, {
      type: 'text/csv',
      filename: 'export.csv',
      ...opts,
    });
  }

  static parseFile(file: File, opts?: { separator?: string; encoding?: string }): Observable<FileEvent<string[][]>> {
    return FilesUtils.readAsText(file, opts?.encoding).pipe(
      map((e) => {
        if (e.type === HttpEventType.UploadProgress) {
          const loaded = Math.round(e.loaded * 0.8);
          return { ...e, loaded };
        } else if (e instanceof FileResponse) {
          const body = e.body;
          const data = CsvUtils.parseCSV(body, opts);
          return new FileResponse<string[][]>({ body: data });
        }
      }),
      filter(isNotNil)
    );
  }

  static parseCSV(body: string, opts?: { separator?: string; skipEmptyLine?: boolean }): string[][] {
    const separator = opts?.separator || ',';
    const skipEmptyLine = !opts || opts.skipEmptyLine !== false;

    // Protect special characters (quote and /n)
    body = body
      .replace('""', '<quote>') // Protect double quote
      .replace(/("[^\n"]+)\n\r?/gm, '"$1<br>') // Protect \n inside a quoted expression
      .replace(/\n\r/gm, '\n'); // Windows CR

    const headerAndRows = body.split('\n', 2);
    const headers = headerAndRows[0].split(separator);

    return body
      .split('\n') // split into rows
      .filter((line) => !skipEmptyLine || line.trim().length > 0) // Skip empty line
      .map((line) => {
        const cells = line
          .split(separator, headers.length) // split into cells
          .map((cell) => cell.replace(/^"([^"]*)"$/, '$1')) // Clean trailing quotes
          .map((cell) => cell.replace('<quote>', '"')) // restore protected quote
          .map((cell) => cell.replace('<br>', '\n')); // restore protected br
        return cells;
      });
  }

  static getLocalizedSeparator(translate?: TranslateService, defaultSeparator = CsvUtils.DEFAULT_SEPARATOR): string {
    const key = 'FILE.CSV.SEPARATOR';
    const separator = translate?.instant(key);
    if (separator && separator !== key) return separator;
    return defaultSeparator;
  }

  static getLocalizedEncoding(translate: TranslateService, defaultEncoding = CsvUtils.DEFAULT_ENCODING): string {
    const key = 'FILE.CSV.ENCODING';
    const encoding = translate?.instant(key);
    if (encoding && encoding !== key) return encoding;
    return defaultEncoding;
  }
}
