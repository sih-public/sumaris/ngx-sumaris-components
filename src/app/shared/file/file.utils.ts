import { FileEvent, FileResponse, UploadFile } from '../upload-file/upload-file.model';
import { UploadFilePopover, UploadFilePopoverOptions } from '../upload-file/upload-file-popover.component';

import { PopoverController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { HttpEventType } from '@angular/common/http';
import { OverlayEventDetail } from '@ionic/core';
import { isNilOrBlank } from '../functions';

export const MimeTypes = Object.freeze({
  ANDROID_APK: 'application/vnd.android.package-archive',
});

export class FilesUtils {
  static UTF8_BOM_CHAR = new Uint8Array([0xef, 0xbb, 0xbf]); // UTF-8 BOM

  static async showUploadPopover<T>(
    popoverController: PopoverController,
    event: Event,
    opts: UploadFilePopoverOptions<T>
  ): Promise<OverlayEventDetail<UploadFile<T>[]>> {
    const modal = await popoverController.create({
      component: UploadFilePopover,
      componentProps: opts,
      backdropDismiss: false,
      keyboardClose: false,
      event,
      translucent: true,
      cssClass: 'popover-large',
    });

    await modal.present();
    const { data, role } = await modal.onDidDismiss();
    return {
      role,
      data: data ? (data as UploadFile<T>[]) : undefined,
    };
  }

  static readAsText(file: File, encoding?: string): Observable<FileEvent<string>> {
    const $progress = new Subject<FileEvent<string>>();
    const reader = new FileReader();
    encoding = (encoding || 'UTF-8').toLowerCase();

    // Listen progress
    reader.onprogress = (e: ProgressEvent) => {
      const total = e.total || 1;
      const loaded = e.total || 0;
      if (loaded < total) $progress.next({ type: HttpEventType.UploadProgress, loaded, total });
    };

    // Listen end
    reader.onloadend = (_) => {
      const body = reader.result;
      if (typeof body === 'string') {
        $progress.next(new FileResponse({ body }));
        $progress.complete();
      } else {
        console.error(`Cannot read file '${file.name}' as text`);
        $progress.error(`Cannot read file as text`);
      }
    };

    // Listen error
    reader.onerror = (event: ProgressEvent) => {
      console.error('[files] Error while reading file:', event);
      $progress.error(event);
    };

    // Start reading the file
    reader.readAsText(file, encoding);

    return $progress.asObservable();
  }

  static writeTextToFile(
    content: string,
    opts?: {
      filename?: string;
      encoding?: string;
      type?: string;
    }
  ) {
    if (isNilOrBlank(content)) return; // Skip if empty

    const filename = opts?.filename || 'export.txt';
    const charset = (opts?.encoding || 'utf-8').toLowerCase();
    const type = opts?.type || 'text/plain';
    const blob = new Blob(
      charset === 'utf-8'
        ? // Add UTF-8 BOM character
          [FilesUtils.UTF8_BOM_CHAR, content]
        : // Non UTF-8 charset
          [content],
      { type: `${type};charset=${charset};` }
    );
    if ((navigator as any).msSaveBlob) {
      // IE 10+
      (navigator as any).msSaveBlob(blob, filename);
    } else {
      const link = document.createElement('a');
      if (link.download !== undefined) {
        // Browsers that support HTML5 download attribute
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  static downloadUri(uri: string, filename?: string) {
    if ((navigator as any).msLaunchUri) {
      // IE 10+
      (navigator as any).msLaunchUri(uri);
    } else {
      filename = filename || this.getFilenameFromUri(uri);
      const link = document.createElement('a');
      if (link.download !== undefined) {
        // Browsers that support HTML5 download attribute
        link.setAttribute('href', uri);
        link.setAttribute('download', filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  static getFilenameFromUri(uri: string): string {
    const lastSlashIndex = uri?.lastIndexOf('/');
    if (lastSlashIndex === -1 || lastSlashIndex === uri.length - 1) throw new Error('Invalid URI format. No slash found');
    return uri.substring(lastSlashIndex + 1);
  }
}
