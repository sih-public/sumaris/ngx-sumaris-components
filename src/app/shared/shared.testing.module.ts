import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialTestingModule, SHARED_MATERIAL_TESTING_PAGES } from './material/material.testing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ToastTestingModule } from './toast/toast.testing.module';
import { UploadFileTestingModule } from './upload-file/testing/upload-file.testing.module';
import { TestingPage } from './testing/tests.page';
import { ImageGalleryTestingModule } from './image/gallery/testing/gallery.testing.module';
import { IonicModule } from '@ionic/angular';
import { AudioTestingModule } from './audio/audio.testing.module';
import { SHARED_STORAGE_TESTING_PAGES, StorageExplorerTestingModule } from './storage/storage-explorer.testing.module';
import { MenuTestingModule } from '../core/menu/testing/menu.testing.module';
import { NamedFilterSelectorTestingModule } from './named-filter/testing/named-filter.testing.module';
import { MarkdownTestingModule } from './markdown/testing/markdown.testing.module';

export const SHARED_TESTING_PAGES: TestingPage[] = [
  ...SHARED_MATERIAL_TESTING_PAGES,

  { label: 'Shared other components', divider: true },
  { label: 'Image gallery', page: '/testing/shared/gallery' },
  { label: 'Toast', page: '/testing/shared/toast' },
  { label: 'File upload', page: '/testing/shared/upload-file' },
  { label: 'Sound & Vibration', page: '/testing/shared/audio' },
  { label: 'Menu', page: '/testing/shared/menu' },
  { label: 'NamedFilterSelector', page: '/testing/shared/name-filter-selector' },
  { label: 'Markdown', page: '/testing/shared/markdown' },

  ...SHARED_STORAGE_TESTING_PAGES,
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule.forChild(),

    // Testing sub-modules
    MaterialTestingModule,
    ToastTestingModule,
    UploadFileTestingModule,
    ImageGalleryTestingModule,
    AudioTestingModule,
    StorageExplorerTestingModule,
    MenuTestingModule,
    NamedFilterSelectorTestingModule,
    MarkdownTestingModule,
  ],
  exports: [
    // Testing sub-modules
    MaterialTestingModule,
    ToastTestingModule,
    UploadFileTestingModule,
    ImageGalleryTestingModule,
    AudioTestingModule,
    StorageExplorerTestingModule,
    MenuTestingModule,
    NamedFilterSelectorTestingModule,
    MarkdownTestingModule,
  ],
})
export class SharedTestingModule {}
