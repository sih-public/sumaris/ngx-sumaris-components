import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { ToolbarComponent } from './toolbar';
import { ModalToolbarComponent } from './modal-toolbar';
import { HammerModule } from '@angular/platform-browser';
import { SharedPipesModule } from '../pipes/pipes.module';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RxStateModule } from '../rx-state/rx-state.module';

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule, HammerModule, SharedPipesModule, RxStateModule, MatProgressBarModule, MatIconModule],
  declarations: [ToolbarComponent, ModalToolbarComponent],
  exports: [ToolbarComponent, ModalToolbarComponent],
})
export class SharedToolbarModule {}
