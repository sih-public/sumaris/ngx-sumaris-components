import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { toBoolean } from '../functions';
import { Subscription } from 'rxjs';
import { Hotkeys } from '../hotkeys/hotkeys.service';
import { IconRef } from '../types';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-modal-toolbar',
  templateUrl: 'modal-toolbar.html',
  styleUrls: ['./modal-toolbar.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalToolbarComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();

  @Input()
  modalName = 'modal-toolbar';

  @Input()
  title = '';

  @Input()
  color = 'primary';

  @Input()
  showSpinner = false;

  @Input()
  canValidate: boolean;

  @Input() validateIcon: IconRef = { icon: 'checkmark' };

  @Output()
  cancel = new EventEmitter<Event>();

  @Output()
  validate = new EventEmitter<Event>();

  constructor(private hotkeys: Hotkeys) {}

  ngOnInit() {
    this.canValidate = toBoolean(this.canValidate, this.validate.observed);

    // Ctrl+S
    this._subscription.add(
      this.hotkeys
        .addShortcut({ keys: `${this.hotkeys.defaultControlKey}.s`, description: 'COMMON.BTN_VALIDATE', elementName: this.modalName })
        .pipe(filter(() => this.canValidate))
        .subscribe((event) => this.validate.emit(event))
    );

    // Escape
    this._subscription.add(
      this.hotkeys
        .addShortcut({ keys: 'escape', description: 'COMMON.BTN_CLOSE', elementName: this.modalName })
        .subscribe((event) => this.cancel.emit(event))
    );
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
