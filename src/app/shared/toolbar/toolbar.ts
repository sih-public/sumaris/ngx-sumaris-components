import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { APP_PROGRESS_BAR_SERVICE, IProgressBarService } from '../services/progress-bar.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IonMenuToggle, IonRouterOutlet, IonSearchbar, NavController } from '@ionic/angular';
import { isNotNil, isNotNilOrBlank, toBoolean } from '../functions';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { HammerTapEvent } from '../gesture/hammer.utils';
import { HAMMER_PRESS_TIME } from '../gesture/gesture-config';
import { AppColors } from '../types';
import { SearchbarChangeEventDetail as ISearchbarSearchbarChangeEventDetail } from '@ionic/core/dist/types/components/searchbar/searchbar-interface';
import { ProgressBarMode } from '@angular/material/progress-bar';

export abstract class ToolbarToken {
  abstract onBackClick: Observable<Event>;

  abstract doBackClick(event: Event);

  abstract goBack(): Promise<boolean>;

  abstract canGoBack: boolean;
}

export const TOOLBAR_HEADER_ID = 'app-toolbar-header';

@Component({
  selector: 'app-toolbar',
  templateUrl: 'toolbar.html',
  styleUrls: ['./toolbar.scss'],
  providers: [{ provide: ToolbarToken, useExisting: ToolbarComponent }],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent implements ToolbarToken, OnInit, OnDestroy {
  private _closeTapCount = 0;
  private _validateTapCount = 0;
  private _defaultBackHref: string;
  private _backHref: string;

  protected readonly progressBarMode$ = new BehaviorSubject<ProgressBarMode>(null);
  protected showSearchBar = false;

  @Input() set progressBarMode(value: ProgressBarMode) {
    if (this.progressBarService) return; // Skip if managed by the progress service
    this.progressBarMode$.next(value);
  }

  get progressBarMode(): ProgressBarMode {
    return this.progressBarMode$.value;
  }

  @Input() title = '';
  @Input() color: AppColors = 'primary';
  @Input() class = '';
  @Input() id = TOOLBAR_HEADER_ID; // Used by

  @Input() set backHref(value: string) {
    if (value !== this._backHref) {
      this._backHref = value;
      this.canGoBack = this.canGoBack || isNotNil(value);
      this.markForCheck();
    }
  }

  get backHref(): string {
    return this._backHref;
  }

  @Input() set defaultBackHref(value: string) {
    if (value !== this._defaultBackHref) {
      this._defaultBackHref = value;
      this.canGoBack = this.canGoBack || isNotNil(value);
      this.markForCheck();
    }
  }

  get defaultBackHref(): string {
    return this._defaultBackHref;
  }

  @Input({ transform: booleanAttribute }) hasValidate = false;
  @Input({ transform: booleanAttribute }) hasClose = false;
  @Input({ transform: booleanAttribute }) hasSearch: boolean;
  @Input({ transform: booleanAttribute }) canGoBack: boolean;
  @Input({ transform: booleanAttribute }) canShowMenu = true;

  @Output() onValidate = new EventEmitter<Event>();
  @Output() onClose = new EventEmitter<Event>();
  @Output() onValidateAndClose = new EventEmitter<Event>();
  @Output() onBackClick = new EventEmitter<Event>();
  @Output() onSearch = new EventEmitter<CustomEvent<ISearchbarSearchbarChangeEventDetail>>();

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  @ViewChild('menuToggle', { static: true }) menuToggle: IonMenuToggle;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navController: NavController,
    private routerOutlet: IonRouterOutlet,
    private cd: ChangeDetectorRef,
    @Optional() @Inject(APP_PROGRESS_BAR_SERVICE) protected progressBarService: IProgressBarService
  ) {
    // Listen progress bar service mode
    if (progressBarService) {
      progressBarService.onProgressChanged
        .pipe(
          startWith(<ProgressBarMode>null), // none, by default
          debounceTime(100), // wait 100ms, to group changes
          // DEBUG
          //tap((mode) => console.debug('[toolbar] Updating progressBarMode: ' + mode))

          distinctUntilChanged()
        )
        .subscribe((value) => this.progressBarMode$.next(value));
    }
  }

  ngOnInit() {
    this.hasValidate = toBoolean(this.hasValidate, this.onValidate.observed);
    this.canGoBack = toBoolean(
      this.canGoBack,
      this.routerOutlet.canGoBack() || isNotNilOrBlank(this._backHref) || isNotNilOrBlank(this._defaultBackHref)
    );
    this.hasSearch = toBoolean(this.hasSearch, this.onSearch.observed);
  }

  ngOnDestroy() {
    this.onBackClick.complete();
    this.onBackClick.unsubscribe();
    this.onValidate.complete();
    this.onValidate.unsubscribe();
    this.onClose.complete();
    this.onClose.unsubscribe();
    this.onValidateAndClose.complete();
    this.onValidateAndClose.unsubscribe();
    this.onSearch.complete();
    this.onSearch.unsubscribe();
  }

  async toggleSearchBar() {
    this.showSearchBar = !this.showSearchBar;
    if (this.showSearchBar && this.searchbar) {
      setTimeout(() => {
        this.searchbar.setFocus();
        this.markForCheck();
      }, 300);
    }
  }

  doBackClick(event: Event) {
    this.onBackClick.emit(event);

    // Stop propagation, if need (can be cancelled by onBackClick observers)
    if (event.defaultPrevented) return;

    // Execute the back action
    this.goBack();
  }

  async goBack(): Promise<boolean> {
    console.debug('[toolbar] calling goBack()');
    if (this._backHref) {
      const replaceUrl = !this.routerOutlet.canGoBack();
      // Relative href
      if (this._backHref.startsWith('.')) {
        this.navController.setDirection('back');
        return this.router.navigate([this._backHref], { relativeTo: this.route, replaceUrl });
      } else {
        return this.navController.navigateBack(this._backHref);
      }
    } else if (this.routerOutlet.canGoBack()) {
      return this.navController.pop();
    } else if (this._defaultBackHref) {
      // Relative href
      if (this._defaultBackHref.startsWith('.')) {
        this.navController.setDirection('back');
        return this.router.navigate([this._defaultBackHref], { relativeTo: this.route, replaceUrl: true });
      } else {
        return this.navController.navigateBack(this._defaultBackHref);
      }
    } else {
      console.error("[toolbar] Cannot go back. Missing attribute 'defaultBackHref' or 'backHref'");
      return false;
    }
  }

  tapClose(event: HammerTapEvent) {
    // DEV only
    // console.debug("[toolbar] tapClose", event.tapCount);
    if (this._validateTapCount > 0) return;

    // Distinguish simple and double tap
    this._closeTapCount = event.tapCount;
    setTimeout(() => {
      // Event is obsolete (a new tap event occur)
      if (event.tapCount < this._closeTapCount) {
        // Ignore event
      }

      // If event still the last tap event: process it
      else {
        this.onClose.emit(event.srcEvent || event);

        // Reset tab count
        this._closeTapCount = 0;
      }
    }, 500);
  }

  tapValidate(event: HammerTapEvent) {
    // DEV only
    //console.debug("[toolbar] tapValidate", event.tapCount);

    if (this.onValidateAndClose.observed) {
      this.onValidate.emit(event.srcEvent || event);
    }

    // Distinguish simple and double tap
    else {
      this._validateTapCount = event.tapCount;
      setTimeout(() => {
        // Event is obsolete (a new tap event occur)
        if (event.tapCount < this._validateTapCount) {
          // Ignore event
        }

        // If event still the last tap event: process it
        else {
          if (this._validateTapCount === 1) {
            this.onValidate.emit(event.srcEvent || event);
          } else if (this._validateTapCount >= 2) {
            this.onValidateAndClose.emit(event.srcEvent || event);
          }

          // Reset tab count
          this._validateTapCount = 0;
        }
      }, HAMMER_PRESS_TIME + 10);
    }
  }

  protected ionSearchBarChanged(event: Event) {
    this.onSearch.emit(event as CustomEvent<ISearchbarSearchbarChangeEventDetail>);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
