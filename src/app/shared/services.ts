import { LoadResult } from './services/entity-service.class';
import {
  getPropertyByPathAsString,
  isNotEmptyArray,
  isNotNil,
  isNotNilObject,
  isNotNilOrBlank,
  propertiesPathComparator,
  startsWithUpperCase,
} from './functions';
import { SortDirection } from '@angular/material/sort';
import { EntityUtils } from '../core/services/model/entity.model';
import { RegExpUtils } from './regexps';

export function suggestFromArray<T = any>(
  items: T[],
  value: any,
  filter?: {
    id?: number;
    offset?: number;
    size?: number;
    searchAttribute?: string;
    searchAttributes?: string[];
    includedIds?: number[];
    excludedIds?: number[];
    skipSort?: boolean;
    anySearch?: boolean;
  },
  sortBy?: keyof T | string,
  sortDirection?: SortDirection,
  opts?: {
    offset?: number;
    size?: number;
    [key: string]: any;
  }
): LoadResult<T> {
  // Value is an array: use it
  if (Array.isArray(value)) return { data: value as T[] };
  // Value is an object: use it (wrapped in an array)
  if (isNotNilObject(value)) return { data: [value as T] };

  value = (typeof value === 'string' && value !== '*' && value.toUpperCase()) || undefined;

  const keys = (filter && ((filter.searchAttribute && [filter.searchAttribute]) || filter.searchAttributes)) || ['label'];

  if (!items) {
    return { data: [], total: 0 };
  }

  // Filter items
  let filteredItems = items;
  if (isNotNilOrBlank(value)) {
    let cleanSearchText = RegExpUtils.cleanSearchText(value as string);
    if (cleanSearchText.length > 0) {
      // If wildcard, search using a regexp
      if (filter?.anySearch || RegExpUtils.hasWildcardCharacter(cleanSearchText)) {
        const regexp = RegExpUtils.compileSearchRegexp(cleanSearchText, 'i');
        filteredItems = filteredItems.filter((v) => keys.some((key) => RegExpUtils.match(getPropertyByPathAsString(v, key), regexp)));
      }
      // Else (no wildcard), search using startsWith
      else {
        cleanSearchText = cleanSearchText.toUpperCase();
        filteredItems = filteredItems.filter((v) => keys.some((key) => startsWithUpperCase(getPropertyByPathAsString(v, key), cleanSearchText)));
      }
    }
  }

  // Included ids
  const includedIds = filter?.includedIds || (isNotNil(filter?.id) ? [filter.id] : null);
  if (isNotEmptyArray(includedIds)) {
    filteredItems = filteredItems.filter((v) => includedIds.includes(+v['id']));
  }

  // Exclude ids
  if (isNotEmptyArray(filter?.excludedIds)) {
    const excludedIds = filter.excludedIds;
    filteredItems = filteredItems.filter((v) => !excludedIds.includes(+v['id']));
  }

  // Sort
  if (!filter || filter.skipSort !== true) {
    // Make to copy input items, before sorting (if not yet already done)
    // (to keep original items unchanged)
    if (filteredItems === items) filteredItems = filteredItems.slice();

    // Sort by given attributes
    if (sortBy) {
      EntityUtils.sort(filteredItems, sortBy, sortDirection);
    }
    // Sort by each keys
    else {
      filteredItems.sort(propertiesPathComparator(keys));
    }
  }

  const total = filteredItems.length || 0;

  // Pagination (truncate if need)
  let pageData = filteredItems;
  const offset = filter?.offset ?? opts?.offset ?? 0;
  const size = filter?.size ?? opts?.size;
  if ((offset || 0) > 0) {
    pageData = pageData.slice(offset);
  }
  if (isNotNil(size) && pageData.length > filter.size) {
    pageData = pageData.slice(0, size);
  }

  const result: LoadResult<T> = {
    data: pageData,
    total,
  };

  // Add fetch more capability, if total was fetched
  const nextOffset = offset + pageData.length;
  if (nextOffset < total) {
    result.fetchMore = async (_) =>
      suggestFromArray(
        filteredItems,
        undefined /*OPTIMISATION: already filtered*/,
        {
          ...filter,
          offset: nextOffset,
          skipSort: true, // OPTIMISATION: already sorted
          excludedIds: undefined, // OPTIMISATION: already filtered
        },
        sortBy,
        sortDirection,
        opts
      );
  }

  return result;
}
