// Import the core angular services.
import { AfterContentInit, Directive, ElementRef, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { KeyboardService } from '../capacitor/keyboard';

// ----------------------------------------------------------------------------------- //
// ----------------------------------------------------------------------------------- //

const BASE_TIMER_DELAY = 100;

@Directive({
  selector: '[autofocus], input[appAutofocus]',
  inputs: ['shouldFocusElement: appAutofocus', 'timerDelay: autofocusDelay'],
})
export class AutofocusDirective implements AfterContentInit, OnChanges, OnDestroy {
  public shouldFocusElement: any;
  public timerDelay: number;

  private _timer: Subscription = null;

  // I initialize the autofocus directive.
  constructor(
    private readonly _elementRef: ElementRef,
    private readonly keyboard: KeyboardService
  ) {
    this.shouldFocusElement = '';
    this._timer = null;
    this.timerDelay = BASE_TIMER_DELAY;
  }

  // I get called once after the contents have been fully initialized.
  ngAfterContentInit() {
    // Because this directive can act on the stand-only "autofocus" attribute or
    // the more specialized "appAutofocus" property, we need to check to see if the
    // "shouldFocusElement" input property is the empty string. This will signify
    // that the focus it not being data-driven and should be performed automatically.
    if (this.shouldFocusElement === '') {
      this._startFocus();
    }
  }

  // I get called every time the input bindings are updated.
  ngOnChanges(changes: SimpleChanges) {
    // If the timer delay is being passed-in as a string (ie, someone is using
    // attribute-input syntax, not property-input syntax), let's coalesce the
    // attribute to a numeric value so that our type-annotations are consistent.
    if ((changes.autofocusDelay || changes.timerDelay) && typeof this.timerDelay !== 'number') {
      // If the coalesce fails, just fall-back to a sane value.
      if (isNaN((this.timerDelay = +this.timerDelay))) {
        this.timerDelay = BASE_TIMER_DELAY;
      }
    }

    // If the focus input is being data-driven, then we either need to start the
    // focus workflow if focus is required; or, clear any existing workflow if focus
    // is no longer required (so that we don't steal focus from another element).
    if (changes.appAutofocus || changes.shouldFocusElement) {
      if (this.shouldFocusElement) this._startFocus();
      else this._stop();
    }
  }

  // I get called once when the directive is being unmounted.
  ngOnDestroy() {
    this._stop();
  }

  /* --- private functions -- */

  // I start the timer-based workflow that will focus the current element.
  private _startFocus() {
    // Skip focus when keyboard is visible
    if (this.keyboard.visible) return;

    // If there is already a timer running for this element, just let it play out -
    // resetting it at this point will only push-out the time at which the focus is
    // applied to the element.
    if (this._timer) return;

    this._timer = timer(this.timerDelay).subscribe(() => {
      if (this._timer) {
        this._stop();
        this._elementRef.nativeElement.focus();
      }
    });
  }

  // I stop the timer-based workflow, preventing focus from taking place.
  private _stop() {
    this._timer?.unsubscribe();
    this._timer = null;
  }
}
