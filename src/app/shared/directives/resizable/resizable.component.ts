import { Component, EventEmitter, HostBinding, Input, Optional, Output } from '@angular/core';
import { MatColumnDef } from '@angular/material/table';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'th[resizable]',
  templateUrl: './resizable.template.html',
  styleUrls: ['./resizable.style.scss'],
})
export class ResizableComponent {
  @Input() resizable: boolean;
  @Output() sizeChanged = new EventEmitter<number>();

  @HostBinding('style.width')
  width: string | null = null;

  @HostBinding('style.min-width')
  minWidth: string | null = null;

  @HostBinding('style.max-width')
  maxWidth: string | null = null;

  private debug = false;

  constructor(@Optional() public readonly columnDef: MatColumnDef) {}

  onResize(width: number, opts?: { emitEvent: boolean }) {
    if (!this.resizable) return;
    if (this.debug) console.info(`resize:${width}`);
    this.width = width + 'px';
    this.minWidth = width + 'px';
    this.maxWidth = width + 'px';
    if (!opts || opts.emitEvent) {
      this.sizeChanged.emit(width);
    }
  }

  onFit(opts?: { emitEvent: boolean }) {
    if (!this.resizable) return;
    if (this.debug) console.info('fit');
    this.width = 'auto';
    this.minWidth = null;
    this.maxWidth = null;
    if (!opts || opts.emitEvent) {
      this.sizeChanged.emit(undefined);
    }
  }
}
