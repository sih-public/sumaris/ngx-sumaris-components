import { DOCUMENT } from '@angular/common';
import { Directive, ElementRef, Inject, Output } from '@angular/core';
import { bufferWhen, distinctUntilChanged, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { fromEvent, interval, Observable } from 'rxjs';

@Directive({
  selector: '[resizable]',
})
export class ResizableDirective {
  @Output()
  readonly resizable: Observable<number>;

  // fit: doubleclick
  @Output()
  readonly fit: Observable<MouseEvent[]>;

  constructor(
    @Inject(DOCUMENT) private readonly document: any,
    @Inject(ElementRef) private readonly elementRef: ElementRef<HTMLElement>
  ) {
    // resize: mousedown -> mousemove -> mouseup
    this.resizable = fromEvent<MouseEvent>(this.elementRef.nativeElement, 'mousedown').pipe(
      tap((e) => {
        e.preventDefault();
        e.stopPropagation();
      }),
      switchMap(() => {
        const { width, right } = this.elementRef.nativeElement.closest('th').getBoundingClientRect();
        return fromEvent<MouseEvent>(this.document, 'mousemove').pipe(
          map(({ clientX }) => width + clientX - right),
          distinctUntilChanged(),
          takeUntil(fromEvent(this.document, 'mouseup'))
        );
      })
    );

    // fit: doubleclick
    this.fit = fromEvent<MouseEvent>(this.elementRef.nativeElement, 'click').pipe(
      bufferWhen(() => interval(500)),
      filter((ar) => ar.length === 2)
    );
  }
}
