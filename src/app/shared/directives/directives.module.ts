import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AutofocusDirective } from './autofocus.directive';
import { NgVarDirective } from './ng-var.directive';
import { DragAndDropDirective } from './drag-and-drop.directive';
import { AutoTitleDirective } from './autotitle.directive';
import { ThrottledClickDirective } from './throttled-click.directive';
import { AutoResizeDirective } from './autoresize.directive';

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [AutofocusDirective, NgVarDirective, DragAndDropDirective, AutoTitleDirective, ThrottledClickDirective, AutoResizeDirective],
  exports: [AutofocusDirective, NgVarDirective, DragAndDropDirective, AutoTitleDirective, ThrottledClickDirective, AutoResizeDirective],
})
export class SharedDirectivesModule {}
