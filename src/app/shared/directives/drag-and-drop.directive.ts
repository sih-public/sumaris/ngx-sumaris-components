import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';
import { isNotEmptyArray } from '../functions';

interface DropEvent extends Event {
  dataTransfer?: {
    files?: File[];
  };
}

@Directive({
  selector: '[appDragAndDrop]',
})
export class DragAndDropDirective {
  @HostBinding('class.fileover') fileOver: boolean;

  @Output() fileDropped = new EventEmitter<File[]>();

  // Dragover listener
  @HostListener('dragover', ['$event'])
  onDragOver(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = true;
  }

  // Dragleave listener
  @HostListener('dragleave', ['$event'])
  public onDragLeave(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = false;
  }

  // Drop listener
  @HostListener('drop', ['$event'])
  public ondrop(event: DropEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.fileOver = false;
    const files = event.dataTransfer?.files;

    if (isNotEmptyArray(files)) {
      this.fileDropped.emit(files);
    }
  }
}
