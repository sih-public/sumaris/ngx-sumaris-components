import { AfterViewInit, Directive, ElementRef, HostListener, Input, numberAttribute } from '@angular/core';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

@Directive({
  selector: 'textarea[cdkTextareaAutosize]',
})
/**
 * Workaround to avoid CDK directive to scroll to top, when resizing
 */
export class AutoResizeDirective implements AfterViewInit {
  private get _cachedLineHeight() {
    return this.cdkDirective?.['_cachedLineHeight'] || 10;
  }

  private get _previousValue() {
    return this.cdkDirective?.['_previousValue'] || '';
  }
  private set _previousValue(value: string) {
    if (this.cdkDirective) {
      this.cdkDirective['_previousValue'] = value;
    }
  }

  @Input({ alias: 'cdkAutosizeMaxRows', transform: numberAttribute })
  maxRows: number;

  @Input({ alias: 'cdkAutosizeMinRows', transform: numberAttribute })
  minRows: number;

  constructor(
    readonly element: ElementRef<HTMLTextAreaElement>,
    readonly cdkDirective: CdkTextareaAutosize
  ) {}

  ngAfterViewInit() {}

  @HostListener('input')
  @HostListener('paste')
  @HostListener('cut')
  @HostListener('delete')
  onInput() {
    if (this._previousValue !== this.element.nativeElement.value) {
      // This will avoid CDK directive to trigger
      this._previousValue = this.element.nativeElement.value;
      // Resize
      this.doResize();
    }
  }

  private doResize() {
    const textarea = this.element.nativeElement;

    // Force to recompute the needed height
    textarea.style.height = 'auto';

    const minHeight = this.minRows ? this.minRows * this._cachedLineHeight : 0;
    const maxHeight = this.maxRows ? this.maxRows * this._cachedLineHeight : Infinity;
    const newHeight = Math.min(Math.max(textarea.scrollHeight, minHeight), maxHeight);

    // DEBUG
    //console.debug(`[autoResizeTextarea] Resizing textarea to ${newHeight}px`);

    textarea.style.height = `${newHeight}px`;

    textarea.style.overflowY = textarea.scrollHeight > maxHeight ? 'auto' : 'hidden';
  }
}
