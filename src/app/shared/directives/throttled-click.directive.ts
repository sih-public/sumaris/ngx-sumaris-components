import { Directive, EventEmitter, HostListener, Input, numberAttribute, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@Directive({
  selector: '[appPreventDoubleClick],[throttledClick]',
})
export class ThrottledClickDirective implements OnInit, OnDestroy {
  @Input({ transform: numberAttribute }) throttleTime = 500;

  @Output() throttledClick = new EventEmitter<UIEvent>();

  private clicks = new Subject<UIEvent>();
  private subscription: Subscription;

  constructor() {}

  @HostListener('click', ['$event'])
  clickEvent(event: UIEvent) {
    if (event.defaultPrevented) return;
    event.stopPropagation();
    this.clicks.next(event);
  }

  ngOnInit() {
    this.subscription = this.clicks.pipe(throttleTime(this.throttleTime)).subscribe((e) => this.emitThrottledClick(e));
  }

  emitThrottledClick(e: UIEvent) {
    this.throttledClick.emit(e);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
