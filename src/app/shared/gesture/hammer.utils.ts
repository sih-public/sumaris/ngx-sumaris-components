export declare type HammerSwipeAction = 'swipeleft' | 'swiperight';
export declare interface HammerSwipeEvent extends Event {
  type: HammerSwipeAction;
  pointerType?: 'touch' | any;
  center?: { x: number; y: number };
  distance?: number;
  velocity?: number;
  srcEvent?: Event;
}

export declare interface HammerTapEvent extends Event {
  tapCount?: number;
  srcEvent?: Event;
}
