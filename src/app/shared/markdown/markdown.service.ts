import { inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router, UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LocationStrategy } from '@angular/common';
import { MarkedRenderer } from 'ngx-markdown';
import { UrlUtils } from '../file/url.utils';
import { Subject, Subscription } from 'rxjs';
import { isNotNilOrBlank } from '../functions';
import { MarkdownUtils } from './markdown.utils';
import { PlatformService } from '../../core/services/platform.service';

@Injectable({ providedIn: 'root' })
export class MarkdownService {
  private route = inject(ActivatedRoute);
  private router = inject(Router);
  private platform = inject(PlatformService);
  private locationStrategy = inject(LocationStrategy);
  private navController = inject(NavController);

  private baseUrl: string;

  private readonly _defaultRenderer = new MarkedRenderer();
  private _markedRenderer: MarkedRenderer;

  get renderer(): MarkedRenderer {
    return this._markedRenderer;
  }

  set renderer(value: MarkedRenderer) {
    this.setRenderer(value);
  }

  onClickMarkdownUrl = new Subject<string>();

  constructor() {
    this.setRenderer(new MarkedRenderer());
  }

  setBaseUrl(baseUrl: string) {
    console.debug('[markdown-service] Base URL: ' + baseUrl);
    this.baseUrl = baseUrl;
  }

  resetBaseUrl() {
    this.setBaseUrl(null);
  }

  getUrlTree(url: string): UrlTree {
    url = UrlUtils.normalizeUrl(url);
    const urlPath = UrlUtils.stripFragmentAndQuery(url) || UrlUtils.stripFragmentAndQuery(this.router.url);
    const parsedUrl = this.router.parseUrl(url);
    const fragment = parsedUrl.fragment;
    const queryParams = parsedUrl.queryParams;
    return this.router.createUrlTree([urlPath], { relativeTo: this.route, fragment, queryParams });
  }

  navigate(url: string, replaceUrl = false) {
    const urlTree = this.getUrlTree(url);
    this.router.navigated = false;
    this.navController.navigateForward(urlTree, { replaceUrl });
  }

  listenClickEvents(nativeElement: HTMLElement): Subscription {
    console.debug('[markdown-service] Start listening click events...');
    const subscription = new Subscription();
    const listener = (event: Event) => this.click(event, nativeElement);
    const links = nativeElement.querySelectorAll('a');
    links.forEach((link) => {
      // DEBUG
      //console.debug('[markdown-service] Adding click listener to', link);
      link.addEventListener('click', listener);
      subscription.add(() => link.removeEventListener('click', listener));
    });

    // DEBUG
    subscription.add(() => console.debug('[markdown-service] Stop listening click events'));

    return subscription;
  }

  /**
   * Transform a relative URL to its absolute representation according to current router state.
   * @param url Relative URL path.
   * @return Absolute URL based on the current route.
   */
  normalizeUrl(url: string): string {
    if (UrlUtils.isExternalUrl(url)) {
      return url;
    }
    if (this.baseUrl && UrlUtils.isRelativeUrl(url)) {
      return MarkdownUtils.resolveRelativeUrl(this.baseUrl, url);
    }

    const urlTree = this.getUrlTree(url);
    const serializedUrl = this.router.serializeUrl(urlTree);
    return this.locationStrategy.prepareExternalUrl(serializedUrl);
  }

  scrollToAnchor(containerElement: HTMLElement, anchorId: string) {
    if (!containerElement || !anchorId) {
      console.warn("Missing 'containerElement' or 'fragment' argument.");
      return;
    }

    console.debug('[markdown-anchor] Scrolling to anchor #' + anchorId);

    // Find the target element with the specified fragment ID
    let targetElement = containerElement.querySelector(`#${anchorId}`);

    // If not found, retry fix a fixed anchor id. This can occur when ToC has been generated for the maven-markdown-plugin
    if (!targetElement) {
      targetElement = containerElement.querySelector(`#${MarkdownUtils.fixAnchor(anchorId)}`);
    }

    if (targetElement) {
      // Scroll smoothly to the target element
      targetElement.scrollIntoView({
        behavior: 'smooth', // Smooth scrolling behavior
        block: 'start', // Align the target element to the top of the container
      });
    } else {
      console.warn(`No element found with the ID "${anchorId}" inside the container.`);
    }
  }

  /* -- protected function -- */

  protected setRenderer(renderer: MarkedRenderer) {
    console.debug('[markdown-service] Set renderer', renderer);
    renderer.link = (href, title, text) => this.link(href, title, text);
    renderer.image = (href, title, text) => this.image(href, title, text);
    renderer.heading = (text, level, raw) => this.heading(text, level, raw);
    this._markedRenderer = renderer;
  }

  protected async click(event: Event, containerElement: HTMLElement): Promise<boolean> {
    console.debug('[markdown-service] Processing click event...', event);

    if (!(event.target instanceof HTMLAnchorElement) && !(event.target?.['tagName'] === 'A')) {
      console.warn('[markdown-service] Invalid click event target. Should an anchor <a>', event.target);
      return;
    }

    const element = event.target as HTMLAnchorElement;

    let href = element.getAttribute('href');
    const fragment = UrlUtils.getFragment(href);

    // Fragment
    if (href?.startsWith('#') && isNotNilOrBlank(fragment)) {
      event.preventDefault();
      this.scrollToAnchor(containerElement, fragment);
      return true;
    }

    const routerLink = element.getAttribute('routerLink');
    if (isNotNilOrBlank(routerLink)) {
      event.preventDefault();
      return this.navController.navigateForward(routerLink);
    }

    // Resolve relative URL, if baseUrl has been set
    if (this.baseUrl && UrlUtils.isRelativeUrl(href)) {
      if (MarkdownUtils.isMarkdownFile(href) && this.onClickMarkdownUrl.observed) {
        event.preventDefault();
        this.onClickMarkdownUrl.next(href);
        return true;
      }

      // Resolve URL, then open using the platform
      href = MarkdownUtils.resolveRelativeUrl(this.baseUrl, href);
    }

    // Resolve internal URL as an app route
    if (UrlUtils.isInternalUrl(href)) {
      const routePath = this.normalizeUrl(href.startsWith('/') ? href : `/${href}`);
      const urlTree = this.getUrlTree(routePath);
      event.preventDefault();
      this.router.navigated = false;
      // Opening route
      return this.navController.navigateForward(urlTree);
    }

    // External URL: open using the platform
    event.preventDefault();
    await this.platform.open(href);
    return true;
  }

  protected link(href: string | null, title: string | null, text: string): string {
    // Resolve router links
    if (UrlUtils.isInternalUrl(href) && !href.startsWith('#') && !this.baseUrl && !MarkdownUtils.isMarkdownFile(href)) {
      const externalUrl = this.normalizeUrl(href);
      return `<a href="${externalUrl}" routerLink="${href}" title="${title || ''}">${text}</a>`;
    }
    return `<a href="${href || ''}" title="${title || ''}">${text}</a>`;
  }

  protected image(href: string, title: string | null, text: string): string {
    let url = href;
    let fallbackUrl: string;
    if (UrlUtils.isRelativeUrl(href) && this.baseUrl) {
      // Resolve relative path
      url = MarkdownUtils.resolveRelativeUrl(this.baseUrl, href);

      // Compute a fallback URL
      fallbackUrl = MarkdownUtils.resolveRelativeUrl(this.baseUrl, href, { absoluteByDefault: true });
      fallbackUrl = fallbackUrl === url ? null : fallbackUrl;

      // DEBUG
      console.debug(`[markdown-service] Replacing image: ${href} -> ${url}`);

      if (fallbackUrl) {
        const fallbackHtml = `<img src="${fallbackUrl}" title="${title || ''}" alt="${text}" onerror="this.style.display='none';" />`;
        return `<img src="${url}" title="${title || ''}" alt="${text}" onerror="this.style.display='none';" />` + fallbackHtml;
      } else {
        return this._defaultRenderer.image(url, title, text);
      }
    }
    return this._defaultRenderer.image(url, title, text);
  }

  protected heading(text: string, level: number, raw: string): string {
    // If NOT the h1 (document title)
    const anchor = MarkdownUtils.textToAnchor(text);
    if (isNotNilOrBlank(anchor)) {
      const pageBreak = level < 2;
      console.debug(`[markdown-service] Heading ${level} '${text}' with anchor ${anchor}`);
      const link = `<a id="${anchor}" class="anchor ${pageBreak ? 'page-break' : ''}" aria-hidden="true" href="#${anchor}"></a>`;
      return this._defaultRenderer.heading(link + text, level, raw);
    }
    return this._defaultRenderer.heading(text, level, raw);
  }
}
