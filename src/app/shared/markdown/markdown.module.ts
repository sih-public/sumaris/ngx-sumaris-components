// function that returns `MarkedOptions` with renderer override
import { MarkdownModule, MARKED_OPTIONS, MarkedOptions } from 'ngx-markdown';
import { ModuleWithProviders, NgModule, SecurityContext } from '@angular/core';
import { MarkdownService } from './markdown.service';
import { MarkdownDirective } from './markdown.directive';
import { HttpClient } from '@angular/common/http';
import { AppMarkdownModal } from './markdown.modal';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedToolbarModule } from '../toolbar/toolbar.module';
import { CommonModule } from '@angular/common';
import { AppMarkdownContent } from './markdown.component';
import { SharedDebugModule } from '../debug/debug.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule.forChild(),
    MarkdownModule.forChild(),

    // App module
    SharedToolbarModule,
    SharedDebugModule,
  ],
  declarations: [
    // Component
    AppMarkdownContent,
    AppMarkdownModal,
    // Directive
    MarkdownDirective,
  ],
  exports: [TranslateModule, MarkdownModule, AppMarkdownContent, MarkdownDirective],
})
export class SharedMarkdownModule {
  static forRoot(): ModuleWithProviders<SharedMarkdownModule> {
    console.debug('[markdown] Creating module (root)');

    return {
      ngModule: SharedMarkdownModule,
      providers: [
        MarkdownService,
        MarkdownModule.forRoot({
          loader: HttpClient, // Allow to load using [src]
          sanitize: SecurityContext.NONE,
          markedOptions: {
            provide: MARKED_OPTIONS,
            deps: [MarkdownService],
            useFactory: (markdownService: MarkdownService) => {
              console.debug('[markdown] Creating marked options');
              return <MarkedOptions>{
                renderer: markdownService.renderer,
                gfm: true,
                breaks: false,
                pedantic: false,
                smartLists: true,
                smartypants: false,
              };
            },
          },
        }).providers,
      ],
    };
  }
}
