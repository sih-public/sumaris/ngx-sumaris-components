import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MarkdownTestPage } from './markdown.test';
import { FormsModule } from '@angular/forms';
import { SharedMarkdownModule } from '../markdown.module';

const routes: Routes = [
  {
    path: 'markdown',
    pathMatch: 'full',
    component: MarkdownTestPage,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule, FormsModule, SharedMarkdownModule],
  declarations: [MarkdownTestPage],
  exports: [RouterModule, MarkdownTestPage],
})
export class MarkdownTestingModule {}
