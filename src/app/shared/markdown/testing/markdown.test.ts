import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { AppMarkdownModal } from '../markdown.modal';
import { ModalController } from '@ionic/angular';
import { Environment, ENVIRONMENT } from '../../../../environments/environment.class';

@Component({
  selector: 'app-markdown-test',
  templateUrl: './markdown.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MarkdownTestPage {
  protected markdownModalTitle = 'Markdown modal';
  protected debug: boolean;

  // URL from a starting markdown
  //protected markdownUrl = 'http://localhost:3000/user-manual/index.md';
  protected markdownUrl = 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/raw/master/projects/pifil/man/quick_start_guide_fr.md';
  //protected markdownUrl = 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/raw/master/README.md';
  //protected markdownUrl = 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/raw/master/model/index.md';
  //protected markdownUrl = 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/raw/master/model/administration/index.md';
  //protected markdownUrl ='https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/raw/master/projects/activity-calendar/spe/collecte_de_donnees.md';
  //protected markdownUrl ='https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/raw/develop/doc/privacy_policy.md';

  constructor(
    private modalCtrl: ModalController,
    private cd: ChangeDetectorRef,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    this.debug = !environment.production;
  }

  onSubmit(event: Event, newUrl: string | number) {
    this.markdownUrl = '' + newUrl;
    this.cd.markForCheck();
  }

  updateInputUrl(url: string) {
    this.markdownUrl = url;
    this.cd.markForCheck();
  }

  openHelpModal(event: Event) {
    return AppMarkdownModal.show(this.modalCtrl, {
      title: this.markdownModalTitle,
      src: this.markdownUrl,
      canPrint: true,
    });
  }
}
