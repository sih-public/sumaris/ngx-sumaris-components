import { UriUtils } from '../file/uri.utils';
import { UrlUtils } from '../file/url.utils';

export class MarkdownUtils {
  static readonly HTML_ENTITIES_MAP: { [entity: string]: string } = {
    '&#39;': "'", // Single quote
    '&quot;': '"', // Double quote
    '&amp;': '&', // Ampersand
    '&lt;': '<', // Less-than
    '&gt;': '>', // Greater-than
    '&nbsp;': ' ', // Non-breaking space
    // Add more entities here if needed
  };

  /**
   * Converts the given text into a valid anchor-compatible format.
   *
   * @param {string} text - The input text to be transformed into an anchor.
   * @return {string} The formatted string suitable for use as an anchor.
   */
  static textToAnchor(text: string): string {
    if (!text) return '';
    return (
      text
        .toLowerCase()
        .trim()
        // Replace entities
        .replace(/&#?[a-z0-9]+;/g, (entity) => this.HTML_ENTITIES_MAP[entity] || '')
        .replace(/['"&<>=\\()]+/g, '')
        .replace(/\s/g, '-')
        .replace(/^-|-$/g, '-')
    );
  }

  /**
   * Adjusts the given anchor ID by replacing underscores with hyphens and converting to lowercase.
   *
   * @param {string} anchorId - The anchor ID string to be fixed.
   * @return {string} The modified anchor ID with underscores replaced by hyphens and converted to lowercase, or the original anchor ID if it is falsy.
   */
  static fixAnchor(anchorId: string): string {
    if (!anchorId) return anchorId;
    return anchorId.replace(/_/g, '-').toLowerCase();
  }

  static isMarkdownFile(url: string) {
    if (!url) return false;
    const cleanUrl = UrlUtils.stripFragmentAndQuery(url);
    const filename = UriUtils.getFilename(cleanUrl);
    return filename.endsWith('.md');
  }

  static resolveRelativeUrl(baseUrl: string, relativePath: string, opts?: { absoluteByDefault: boolean }) {
    if (!baseUrl) throw new Error("Argument 'baseUrl' is missing");
    if (!relativePath) throw new Error("Argument 'relativePath' is missing");
    if (!UrlUtils.isRelativeUrl(relativePath)) throw new Error('Should be a relative path');

    // Detect absolute URL
    if (relativePath.startsWith('/') || (opts?.absoluteByDefault && !relativePath.startsWith('.'))) {
      baseUrl = this.getRootUrl(baseUrl);
      relativePath = relativePath.startsWith('/') ? relativePath.substring(1) : relativePath;
    }

    return UrlUtils.normalizeUrl(UrlUtils.concat(baseUrl, relativePath));
  }

  static isGitlabUrl(url: string) {
    if (!url) return false;
    return url.startsWith('https://gitlab.') || url.includes('/-/raw/') || url.includes('/-/blob/');
  }

  static fixGitlabUrlToRaw(url: string) {
    if (!this.isGitlabUrl(url)) return url;
    return url.replace('/-/blob/', '/-/raw/').replace('/-/tree/', '/-/raw/');
  }

  static getRootUrl(url: string) {
    if (this.isGitlabUrl(url)) {
      console.debug('[markdown] Gitlab URL detected: ' + url);
      url = url.replace('/-/blob/', '/-/raw/');
      if (url.includes('/-/raw/')) {
        const index = url.indexOf('/-/raw/') + '/-/raw/'.length;
        const rootPath = url.substring(0, index);
        const branch = url.substring(index).split('/', 2)[0];
        return rootPath + branch;
      }
    }
    return UrlUtils.getRootUrl(url);
  }
}
