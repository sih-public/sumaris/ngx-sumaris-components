import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  inject,
  Input,
  OnDestroy,
  Optional,
  Output,
} from '@angular/core';
import { LocalSettingsService } from '../../core/services/local-settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NetworkService } from '../../core/services/network.service';
import { HttpErrorResponse } from '@angular/common/http';
import { slideDownAnimation } from '../material/material.animations';
import { MarkdownService } from './markdown.service';
import { Subscription } from 'rxjs';
import { UrlUtils } from '../file/url.utils';
import { isNotEmptyArray, isNotNilOrBlank } from '../functions';
import { NavController, ToastController } from '@ionic/angular';
import { MarkdownUtils } from './markdown.utils';
import { ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { PrintService } from '../print/print.service';

export interface AppMarkdownOptions {
  data?: string;
  src?: string;
  showError?: boolean;
}

@Component({
  selector: 'app-markdown-content',
  templateUrl: './markdown.component.html',
  styleUrls: ['./markdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideDownAnimation],
})
export class AppMarkdownContent implements AppMarkdownOptions, OnDestroy {
  private _printId: number;
  private _clickSubscription: Subscription;

  protected navController = inject(NavController);
  protected settings = inject(LocalSettingsService);
  protected translate = inject(TranslateService);
  protected network = inject(NetworkService);
  protected markdownService = inject(MarkdownService);
  protected toastController = inject(ToastController);
  protected printService = inject(PrintService);

  protected loading = true;
  protected error: string = undefined;

  protected baseUrl: string;

  protected _data: string;
  protected _src: string;
  protected _fallbackSrc: string;
  protected _anchor: string;

  protected _navHistory: { src?: string; fallbackSrc?: string; data?: string; anchor?: string }[] = [];

  @Input() set data(value: string) {
    this.setData(value);
  }
  get data() {
    return this._data;
  }

  @Input() set src(value: string) {
    this.setSrc(value);
  }
  get src() {
    return this._src;
  }

  @Input() set anchor(value: string) {
    this._anchor = value;
  }
  get anchor() {
    return this._anchor;
  }

  @Input({ transform: booleanAttribute }) debug: boolean;
  @Input({ transform: booleanAttribute }) showError = true;
  @Input({ transform: booleanAttribute }) animated = true;
  @Input({ transform: booleanAttribute }) enableNavigationHistory = true;
  @Input({ transform: booleanAttribute }) updateRouteLocation = false;

  @Output() markdownUrlChange = new EventEmitter<string>();

  constructor(
    protected cd: ChangeDetectorRef,
    protected element: ElementRef<HTMLElement>,
    @Inject(DOCUMENT) private _document: Document,
    @Optional() protected route: ActivatedRoute // Modal editor give 'null'
  ) {
    this._printId = this.printService.nextJobId();
    this.markAsLoading({ emitEvent: false });

    // TODO: for DEV only
    //this.debug = !environment.production;
  }

  ngOnDestroy() {
    this._clickSubscription?.unsubscribe();
    this.markdownService.resetBaseUrl();
  }

  goBack(event?: Event) {
    console.debug('[markdown] Going back');
    if (isNotEmptyArray(this._navHistory)) {
      const { src, fallbackSrc, data, anchor } = this._navHistory.pop();
      if (src) this.setSrc(src, { fallbackSrc, anchor });
      else this.setData(data);
    } else {
      this.navController.back();
    }
  }

  get canGoBack() {
    return isNotEmptyArray(this._navHistory);
  }

  async print() {
    if (this.loading) return; // skip
    console.debug('[markdown] Print...');

    if (!this.element) {
      console.error('Element to print not found. Please check the HTML template.');
      return;
    }

    this.markAsLoading({ emitEvent: false });

    try {
      // Print
      await this.printService.printElement(this.element.nativeElement, {
        id: this._printId,

        // Print as portrait, by default
        style: `@media print {
          @page {
            size: portrait;
            margin: 1.1cm;
          }
          markdown hr {
            page-break-before: always;
            border: none;
            margin: 0;
            padding: 0;
          }
          markdown img {
            max-width: 100%;
            height: auto;
            page-break-inside: avoid;
          }
        }`,
      });
    } finally {
      this.markAsLoaded({ emitEvent: false });
    }
  }

  markAsLoaded(opts?: { emitEvent?: boolean }) {
    this.loading = false;

    if (opts?.emitEvent !== false) {
      this.printService.markAsLoaded(this._printId);
      this.cd.markForCheck();
    }
  }

  markAsLoading(opts?: { emitEvent?: boolean }) {
    this.loading = true;

    if (opts?.emitEvent !== false) {
      this.printService.markAsLoading(this._printId);

      this.cd.markForCheck();
    }
  }

  setError(error: string | Error) {
    this.error = error?.['message'] || error || null;
    this.cd.markForCheck();
  }

  resetError() {
    this.error = null;
    this.cd.markForCheck();
  }

  /* -- protected functions -- */

  protected setData(value: string, opts?: { anchor?: string; addToNavigationHistory?: boolean }) {
    const anchor = opts?.anchor || null;

    if (this._data !== value || this._anchor !== anchor) {
      if (value) {
        // Save current state to navigation history
        if (opts?.addToNavigationHistory) {
          this._navHistory.push({
            src: this._src,
            fallbackSrc: this._fallbackSrc,
            data: this._data,
            anchor: this._anchor,
          });
        }

        this._data = value;
        this._anchor = anchor;

        // Clear src
        this._src = null;
        this._fallbackSrc = null;

        this.resetError();
        this.markAsLoaded();
      } else {
        this._data = null;
      }
      this.cd.markForCheck();
    }
  }

  protected setSrc(src: string, opts?: { fallbackSrc?: string; anchor?: string; addToNavigationHistory?: boolean }) {
    // Clean URL (remove fragment and query)
    const anchor = opts?.anchor || UrlUtils.getFragment(src);
    src = UrlUtils.stripFragmentAndQuery(src);
    src = MarkdownUtils.isGitlabUrl(src) ? MarkdownUtils.fixGitlabUrlToRaw(src) : src;

    let fallbackSrc = opts?.fallbackSrc ? UrlUtils.stripFragmentAndQuery(opts.fallbackSrc) : null;
    fallbackSrc = MarkdownUtils.isGitlabUrl(fallbackSrc) ? MarkdownUtils.fixGitlabUrlToRaw(fallbackSrc) : fallbackSrc;

    if (this._src !== src || this._fallbackSrc !== fallbackSrc || this._anchor !== anchor) {
      const url = src ? src + (isNotNilOrBlank(anchor) ? `#${anchor}` : '') : null;
      if (url) console.debug(`[markdown] Loading ${url}...`);

      // Give baseUrl to the service, to be able to resolve relative path - e.g. images)
      if (src && UrlUtils.isExternalUrl(src)) {
        this.baseUrl = UrlUtils.getParent(src);
        this.markdownService.setBaseUrl(this.baseUrl);
      }

      if (src) {
        // Save current state to navigation history
        if (opts?.addToNavigationHistory) {
          this._navHistory.push({
            src: this._src,
            fallbackSrc: this._fallbackSrc,
            data: this._data,
            anchor: this._anchor,
          });
        }

        this._src = src;
        this._fallbackSrc = fallbackSrc;
        this._anchor = anchor;

        // Clear data
        this._data = null;

        // Reset error
        this.resetError();
        this.markAsLoading();
      } else {
        this._src = null;
        this._fallbackSrc = null;
        this._anchor = null;
        this.cd.markForCheck();
      }
    }
  }

  protected onReady() {
    const url = this._src ? this._src + (isNotNilOrBlank(this._anchor) ? `#${this._anchor}` : '') : null;

    if (url) {
      console.debug(`[markdown] Ready - ${url}`);
      // Clear fallback (no more need)
      if (this._fallbackSrc) this._fallbackSrc = null;

      // Emit change
      this.markdownUrlChange.emit(url);
    }

    this.startListenClickEvents();

    this.markAsLoaded();

    // Scroll to anchor
    if (isNotNilOrBlank(this._anchor)) {
      this.markdownService.scrollToAnchor(this.element.nativeElement, this._anchor);
    }
  }

  protected onLoadError(error?: string | Error) {
    // Translate error
    if (error instanceof HttpErrorResponse && this.network.offline) {
      console.error(`[markdown] Error loading markdown URL {${this.src}} (offline: true): `, error);
      this.setError('NETWORK.INFO.OFFLINE');
    } else {
      // Retry with fallback URL
      if (this._fallbackSrc) {
        console.debug(`[markdown] Error loading markdown URL {${this.src}} - fallback to URL ${this._fallbackSrc}`);
        this.setSrc(this._fallbackSrc, { anchor: this._anchor });
        return;
      }

      console.error(`[markdown] Error loading markdown URL {${this.src}} (offline: true): `, error);
      this.setError('ERROR.LOAD_DATA_ERROR');
    }

    this.markAsLoaded();
  }

  protected startListenClickEvents() {
    this._clickSubscription?.unsubscribe();
    this._clickSubscription = new Subscription();

    // Listen for click link to Markdown URL
    this._clickSubscription.add(
      this.markdownService.onClickMarkdownUrl.subscribe((href) => {
        console.debug('[markdown] Received click event, on a markdown URL', href);

        let fragment = UrlUtils.getFragment(href);
        let url = UrlUtils.stripFragmentAndQuery(href);
        let fallbackUrl = null;

        // Resolve relative path
        if (UrlUtils.isRelativeUrl(href) && this.baseUrl) {
          // Get absolute url
          url = MarkdownUtils.resolveRelativeUrl(this.baseUrl, href);

          // Create a fallback url, in relative path was wrong
          fallbackUrl = MarkdownUtils.resolveRelativeUrl(this.baseUrl, href, { absoluteByDefault: true });
          fallbackUrl = url === fallbackUrl ? null : fallbackUrl;
        }

        // Opening the new URL
        this.setSrc(url, { fallbackSrc: fallbackUrl, anchor: fragment, addToNavigationHistory: this.enableNavigationHistory });
      })
    );
  }
}
