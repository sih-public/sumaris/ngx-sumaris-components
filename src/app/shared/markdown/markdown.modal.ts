import { booleanAttribute, ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PlatformService } from '../../core/services/platform.service';
import { LocalSettingsService } from '../../core/services/local-settings.service';
import { TranslateService } from '@ngx-translate/core';
import { NetworkService } from '../../core/services/network.service';
import { AppMarkdownContent } from './markdown.component';

export interface AppMarkdownModalOptions {
  title?: string;
  data?: string;
  src?: string;
  showError?: boolean;
  cssClass?: string;
  canPrint?: boolean;
  enableNavigationHistory?: boolean;

  debug?: boolean;
}

@Component({
  selector: 'app-markdown-modal',
  templateUrl: './markdown.modal.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppMarkdownModal implements AppMarkdownModalOptions {
  @Input() title: string;
  @Input({ transform: booleanAttribute }) showError = true;
  @Input({ transform: booleanAttribute }) canPrint = false;
  @Input() data: string;
  @Input() src: string;
  @Input({ transform: booleanAttribute }) enableNavigationHistory = true;
  @Input({ transform: booleanAttribute }) debug: boolean;

  @ViewChild('content') content: AppMarkdownContent;

  static async show(modalCtrl: ModalController, options: AppMarkdownModalOptions) {
    const modal = await modalCtrl.create({
      component: AppMarkdownModal,
      componentProps: options,
      keyboardClose: true,
      cssClass: options?.cssClass || 'modal-large',
    });

    await modal.present();

    return modal.onWillDismiss();
  }

  constructor(
    protected injector: Injector,
    protected viewCtrl: ModalController,
    protected platform: PlatformService,
    protected settings: LocalSettingsService,
    protected translate: TranslateService,
    protected network: NetworkService,
    protected cd: ChangeDetectorRef
  ) {
    // TODO: for DEV only
    //this.debug = !environment.production;
  }

  close(_?: Event) {
    return this.viewCtrl.dismiss();
  }

  goBack(event: Event, content: AppMarkdownContent) {
    if (content?.canGoBack) {
      content.goBack(event);
      this.cd.markForCheck();
    } else {
      this.close(event);
    }
  }

  protected markAsLoaded() {
    this.content?.markAsLoaded();
  }

  protected markAsLoading() {
    this.content?.markAsLoading();
  }

  protected setError(err: string | Error) {
    this.content?.setError(err);
  }

  protected resetError() {
    this.content?.resetError();
  }
}
