import { Directive, ElementRef, HostListener, OnDestroy } from '@angular/core';
import { MarkdownService } from './markdown.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: 'markdown,[markdown]',
})
export class MarkdownDirective implements OnDestroy {
  private _subscription = new Subscription();

  constructor(
    private service: MarkdownService,
    private element: ElementRef<HTMLElement>
  ) {
    // DEBUG
    //console.debug('[markdown] Creating markdown directive');
  }

  @HostListener('ready')
  public processAnchors() {
    // Listening click events
    this._subscription.add(this.service.listenClickEvents(this.element.nativeElement));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
