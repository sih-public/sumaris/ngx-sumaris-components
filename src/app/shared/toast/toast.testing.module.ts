import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ToastTestingPage } from './toast.testing';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: 'toast',
    pathMatch: 'full',
    component: ToastTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), RouterModule.forChild(routes), FormsModule],
  declarations: [ToastTestingPage],
  exports: [RouterModule, ToastTestingPage],
})
export class ToastTestingModule {}
