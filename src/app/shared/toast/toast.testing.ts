import { Component, Injector } from '@angular/core';
import { ShowToastOptions, Toasts } from './toasts';

@Component({
  selector: 'toast-testing',
  templateUrl: 'toast.testing.html',
})
export class ToastTestingPage {
  protected options: ShowToastOptions = {
    showCloseButton: false,
    duration: -1, // 5000, // 5s
    header: undefined,
    message: 'This is a message text !',
    positionAnchor: 'header',
    position: 'top',
  };

  constructor(private injector: Injector) {}

  showToast(opts?: ShowToastOptions) {
    return Toasts.showSimple(this.injector, {
      ...this.options,
      position: this.options.position === ('auto' as any) ? null : this.options.position,
      ...opts,
    });
  }

  showInfo(opts?: ShowToastOptions) {
    return this.showToast({
      type: 'info',
      ...opts,
    });
  }

  showWarning(opts?: ShowToastOptions) {
    return this.showToast({
      type: 'warning',
      ...opts,
    });
  }

  showError(opts?: ShowToastOptions) {
    return this.showToast({
      type: 'error',
      ...opts,
    });
  }

  toggleCloseButton() {
    this.options.showCloseButton = !this.options.showCloseButton;
  }
}
