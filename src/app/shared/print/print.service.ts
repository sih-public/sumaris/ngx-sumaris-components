import { ShowToastOptions, Toasts } from '../toast/toasts';
import { OverlayEventDetail } from '@ionic/core';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { StorageService } from '../storage/storage.service';
import { isNil, isNilOrBlank, isNotNil, isNotNilOrBlank } from '../functions';
import { waitForTrue } from '../observables';
import { mergeMap, timer } from 'rxjs';
import { isChrome } from '../platforms';

export interface PrintOptions {
  id?: string | number;
  title?: string;
  showToast?: boolean;
  toastMessage?: string;
  style?: string;
}

export const PRINT_LOADING_STORAGE_KEY_PREFIX = 'print-loading-';
export const PRINT_ID_QUERY_PARAM = 'print-id';

@Injectable({ providedIn: 'root' })
export class PrintService {
  private _printId = 0;

  constructor(
    protected toastController: ToastController,
    protected translate: TranslateService,
    protected storage: StorageService,
    @Inject(DOCUMENT) protected document: Document
  ) {}

  nextJobId(): number {
    return ++this._printId;
  }

  /**
   * Extracts and returns the job ID from a given URL.
   *
   * @param {string} [url] - The URL from which to extract the job ID. If not provided, `window.location.href` will be used.
   * @return {number | undefined} The extracted job ID as a number, or `undefined` if the job ID is not present or invalid.
   */
  getJobId(url?: string): string | undefined {
    url = url || window.location.href;
    const matches = url.match(/print-id=(\d+)/);
    return matches?.[1] || undefined;
  }

  /**
   * Determines whether the provided URL or the current page's URL contains
   * a specific query parameter, indicating it's a printing-related URL.
   *
   * @param {string} [url] - The optional URL to check. If not provided, the current page's URL is used.
   * @param {string} [queryParam='print-id'] - The query parameter to look for, defaulting to 'print-id'.
   * @return {boolean} Returns true if the query parameter is found in the URL; otherwise, false.
   */
  isPrintingUrl(url?: string, queryParam: string = 'print-id'): boolean {
    let query: string;
    if (isNotNilOrBlank(url)) query = new URL(url).search;
    query = query || window.location.search || '?';
    return query.indexOf(queryParam) !== -1;
  }

  async printElement(element: HTMLElement, opts?: PrintOptions): Promise<number | string> {
    if (!element) throw new Error("Missing required 'element' argument");

    return this.printHTML(element.innerHTML, opts);
  }

  async printHTML(html: string, opts?: PrintOptions): Promise<number | string> {
    if (!html) throw new Error("Missing required 'html' argument");

    // Create iframe
    const iframe = this.createPrintIframeByHtml(html, opts);

    return this.printIframe(iframe, opts);
  }

  /**
   * Prints the content of a URL by rendering it in an iframe.
   *
   * @param {string} url The URL of the content to print. This parameter is required.
   * @param {Omit<PrintOptions, 'style'>} [opts] Optional settings for printing, excluding the 'style' option.
   * @return {Promise<number>} A promise that resolves to the ID of the print job.
   */
  async printUrl(url: string, opts?: Omit<PrintOptions, 'style'>): Promise<number | string> {
    if (isNilOrBlank(url)) throw new Error("Missing required 'url' argument");

    const jobId = opts?.id ?? this.nextJobId();

    // Create iframe
    const iframe = this.createPrintIframeByUrl(url, { id: jobId });

    // Launch the print job
    return this.printIframe(iframe, opts);
  }

  /**
   * Prints the content of the specified iframe element.
   *
   * @param {HTMLIFrameElement} iframe - The iframe element whose content needs to be printed. Must be a valid and loaded iframe element.
   * @param {PrintOptions} [opts] - Optional parameters for the print operation, which may include options for toast messages and job configuration.
   * @return {Promise<number>} A promise that resolves to the unique ID of the print job.
   * @throws Will throw an error if the required 'iframe' argument is missing or invalid.
   */
  async printIframe(iframe: HTMLIFrameElement, opts?: PrintOptions): Promise<number | string> {
    if (!iframe) throw new Error("Missing required 'iframe' argument");

    const jobId = opts?.id ?? this.nextJobId();
    const toastId = `printing-${jobId}`;
    const showLoadingToast = opts?.showToast !== false;
    if (showLoadingToast)
      await this.showToast({
        id: toastId,
        message: opts?.toastMessage || 'COMMON.PLEASE_WAIT',
        duration: 0,
      });

    try {
      // Wait end of iframe load
      await this.waitIdle(iframe, { id: jobId });

      // Print (and wait end of print)
      await this.print(iframe);

      return jobId;
    } catch (err) {
      console.error('[print-service] Failed to create hidden iframe', err);
    } finally {
      iframe.remove();
      if (showLoadingToast) await this.toastController.dismiss(null, null, toastId);
    }
  }

  async markAsLoading(id: string | number) {
    console.debug(`[print-service] Mark print job #${id} as loading`);
    await this.storage.set(PRINT_LOADING_STORAGE_KEY_PREFIX + id, '1');
  }

  async markAsLoaded(id: string | number) {
    console.debug(`[print-service] Mark print job #${id} as loaded`);
    await this.storage.remove(PRINT_LOADING_STORAGE_KEY_PREFIX + id);
  }

  async isLoaded(id: string | number): Promise<boolean> {
    const result = await this.storage.get(PRINT_LOADING_STORAGE_KEY_PREFIX + id);
    return isNil(result);
  }

  async isLoading(id: string | number): Promise<boolean> {
    return !(await this.isLoaded(id));
  }

  /* -- protected functions -- */

  protected createPrintIframeByUrl(url: string, opts?: { id?: number | string }): HTMLIFrameElement {
    // Make sure to add the job id inside the URL (e.g. to allow getJobIdFromUrl() to retrieve it)
    if (isNotNil(opts?.id)) {
      const newUrl = new URL(url);
      newUrl.searchParams.set(PRINT_ID_QUERY_PARAM, '' + opts.id);
      url = newUrl.href;
    }

    // Create a iframe with the given url
    const iframe = this.document.createElement('iframe');
    iframe.classList.add('cdk-visually-hidden');
    iframe.style.width = '100%';
    iframe.style.height = '100%';
    this.document.body.appendChild(iframe);
    iframe.src = url;
    return iframe;
  }

  protected createPrintIframeByHtml(html: string, opts?: { title?: string; inlineStyle?: string }): HTMLIFrameElement {
    if (!html) throw new Error("Missing required 'html' argument");

    const lang = this.document.documentElement?.lang || 'en';

    // Create an empty iframe
    const iframe = this.document.createElement('iframe');
    iframe.classList.add('cdk-visually-hidden');
    iframe.style.width = '100%';
    iframe.style.height = '100%';
    this.document.body.appendChild(iframe);

    // Write iframe content
    const iframeDocument = iframe.contentDocument || iframe.contentWindow?.document;
    if (iframeDocument) {
      iframeDocument.open();
      iframeDocument.write(`
        <!DOCTYPE html>
        <html lang="${lang}" class="print-pdf">
          <head>
            <title>${opts?.title || ''}</title>
            <style>
            @media print {
              body {
                font-family: 'Roboto', Helvetica, Arial, sans-serif;
                margin: 0;
                padding: 0;
                overflow: hidden;
              }
              img {
                max-width: 100%;
                height: auto;
                page-break-inside: avoid;
              }
            }
            ${opts?.inlineStyle || ''}
          </style>
          </head>
          <body>
            ${html}
          </body>
        </html>
      `);
      iframeDocument.close();
    }

    // Set iframe style
    // FIXME - Ignore style injection, because it failed in Firefox (e.g. on user manual) and Chrome (e.g. in privacy policy)
    //         Nothing appear in the print dialog
    //this.applyDocumentStyleToIframe(iframe);

    return iframe;
  }

  /**
   * Applies the styles from the provided document, including external stylesheets and inline styles,
   * to the specified iframe's document.
   *
   * @param {HTMLIFrameElement} iframe - The iframe to which the styles are applied.
   * @return {void} Does not return a value.
   */
  protected applyDocumentStyleToIframe(iframe: HTMLIFrameElement): void {
    const iframeDocument = iframe.contentDocument || iframe.contentWindow?.document;
    if (iframeDocument) {
      // Récupérer tous les styles (liens CSS)
      const links = Array.from(this.document.querySelectorAll('style,link[rel="stylesheet"]'));
      links.forEach((element) => {
        // Insert inline style
        if (element.tagName === 'STYLE') {
          const newStyle = iframeDocument.createElement('style');
          newStyle.innerHTML = element.innerHTML;
          iframeDocument.head.appendChild(newStyle);

          console.debug('[print-service] Add style to iframe', newStyle);
        }

        // Insert link to stylesheet file
        else if (element.tagName === 'LINK' && element.getAttribute('href')) {
          const newLink = iframeDocument.createElement('link');
          newLink.rel = 'stylesheet';
          newLink.href = element.getAttribute('href');
          iframeDocument.head.appendChild(newLink);

          console.debug('[print-service] Add stylesheet to iframe: ', newLink.href);
        }
      });
    }
  }

  protected async waitIdle(iframe: HTMLIFrameElement, opts?: { id?: number | string; timeout?: number }): Promise<void> {
    // Wait job finished
    const printId = opts?.id ?? this._printId;
    await waitForTrue(timer(500, 500).pipe(mergeMap(() => this.isLoaded(printId))), { timeout: opts?.timeout });

    // Wait end of iframe load
    await new Promise<void>((resolve, reject) => {
      const iframeWindow = iframe.contentWindow?.window || iframe.contentWindow;
      if (iframeWindow.document.readyState === 'complete') {
        resolve();
      } else {
        // Stop if timeout reached
        if (opts?.timeout > 0) {
          setTimeout(() => reject('timeout'), opts.timeout);
        }

        const readyListener = () => {
          if (iframeWindow.document.readyState === 'complete') {
            resolve();

            iframeWindow.document.removeEventListener('readystatechange', readyListener);
          }
        };
        iframeWindow.document.addEventListener('readystatechange', readyListener);
      }
    });
  }

  /**
   * Triggers the print functionality of the given iframe and resolves or rejects based on the print completion or timeout.
   *
   * @param {HTMLIFrameElement} iframe - The iframe element to be printed.
   * @param {{timeout?: number}} [opts] - An optional configuration object.
   * @param {number} [opts.timeout] - The maximum time in milliseconds to wait for the print to complete before rejecting the promise.
   * @return {Promise<void>} A promise that resolves after the print is completed or rejects if a timeout occurs.
   */
  protected print(iframe: HTMLIFrameElement, opts?: { timeout?: number }): Promise<void> {
    return new Promise((resolve, reject) => {
      const iframeWindow = iframe.contentWindow?.window || iframe.contentWindow;

      // Stop if timeout reached
      if (opts?.timeout > 0) {
        setTimeout(() => reject('timeout'), opts.timeout);
      }

      // On Chrome
      if (isChrome(window)) {
        // Wait after print event, to resolve the promise
        const afterPrintListener = () => {
          console.debug('[print-service] After print');

          resolve();

          // Remove the listener
          iframeWindow.removeEventListener('afterprint', afterPrintListener);
        };
        iframeWindow.addEventListener('afterprint', afterPrintListener);
        iframe.contentWindow.window.print();
      }

      // Otherwise (e.g. Firefox)
      else {
        iframe.contentWindow.window.print();
        resolve();
      }
    });
  }

  private async showToast<T = any>(opts: ShowToastOptions): Promise<OverlayEventDetail<T>> {
    if (!this.toastController) throw new Error("Missing toastController in component's constructor");
    return await Toasts.show(this.toastController, this.translate, opts);
  }
}
