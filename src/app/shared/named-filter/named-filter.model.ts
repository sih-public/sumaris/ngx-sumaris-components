import { Entity, IEntity } from '../../core/services/model/entity.model';
import { EntityFilter, IEntityFilter } from '../../core/services/model/filter.model';
import { EntityClass } from '../../core/services/model/entity.decorators';

export interface INamedFilter<T extends INamedFilter<T>> extends IEntity<T, number> {
  name: string;
  entityName: string;
  content: any;
  recorderPersonId: number;
  recorderDepartmentId: number;
}

@EntityClass({ typename: 'NamedFilterVO' })
export class NamedFilter<T extends NamedFilter<any> = NamedFilter<any>> extends Entity<T, number> implements INamedFilter<T> {
  static fromObject: (source: any, opts?: any) => NamedFilter;

  name: string;
  entityName: string;
  content: any;
  recorderPersonId: number;
  recorderDepartmentId: number;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.name = source.name;
    this.entityName = source.entityName;
    this.recorderPersonId = source.recorderPersonId;
    this.recorderDepartmentId = source.recorderDepartmentId;
    try {
      this.content = typeof source?.content === 'string' ? JSON.parse(source?.content) : source?.content || undefined;
    } catch (e) {
      console.error(`[named-filter] can't parse content json`, source);
      throw e;
    }
  }
}

export interface INamedFilterFilter<F extends INamedFilterFilter<any, T>, T extends INamedFilter<T>> extends IEntityFilter<F, T> {
  searchText: string;
  entityName: string;
  recorderPersonId: number;
  recorderDepartmentId: number;
}

@EntityClass({ typename: 'NamedFilterFilterVO' })
export class NamedFilterFilter<F extends NamedFilterFilter<any, T> = NamedFilterFilter<any, any>, T extends NamedFilter<any> = NamedFilter<any>>
  extends EntityFilter<F, T>
  implements INamedFilterFilter<F, T>
{
  static fromObject: (source: any, opts?: any) => NamedFilterFilter;

  searchText: string = null;
  entityName: string = null;
  recorderPersonId: number = null;
  recorderDepartmentId: number = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.searchText = source.searchText;
    this.entityName = source.entityName;
    this.recorderPersonId = source.recorderPersonId;
    this.recorderDepartmentId = source.recorderDepartmentId;
  }
}
