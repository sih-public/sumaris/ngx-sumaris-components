import { Directive, InjectionToken } from '@angular/core';
import { EntityUtils } from '../../core/services/model/entity.model';
import {
  EntitiesServiceLoadOptions,
  EntityServiceWatchOptions,
  IEntitiesService,
  IEntityService,
  LoadResult,
  SuggestService,
} from '../services/entity-service.class';
import { GraphqlService } from '../../core/graphql/graphql.service';
import { BaseEntityService, BaseEntityServiceOptions } from '../../core/services/base-entity-service.class';
import { PlatformService } from '../../core/services/platform.service';
import { AccountService } from '../../core/services/account.service';
import { isNil } from '../functions';
import { INamedFilter, INamedFilterFilter, NamedFilter, NamedFilterFilter } from './named-filter.model';

export const APP_NAMED_FILTER_SERVICE = new InjectionToken<INamedFilterService<any, any, any, any>>('namedFilterService');

export interface INamedFilterService<
  T extends INamedFilter<T>,
  F extends INamedFilterFilter<F, T>,
  WO extends NamedFilterWatchOptions,
  LO extends NamedFilterLoadOptions,
> extends IEntitiesService<T, F, WO>,
    IEntityService<T, number, LO>,
    SuggestService<T, F> {}

export interface NamedFilterWatchOptions extends EntityServiceWatchOptions {
  withContent?: boolean;
}

export interface NamedFilterLoadOptions extends EntitiesServiceLoadOptions {
  withContent?: boolean;
}

// @dynamic
@Directive()
export abstract class AbstractNamedFilterService<
    T extends NamedFilter<T>,
    F extends NamedFilterFilter<F, T>,
    WO extends NamedFilterWatchOptions = NamedFilterWatchOptions,
    LO extends NamedFilterLoadOptions = NamedFilterLoadOptions,
  >
  extends BaseEntityService<T, F, number>
  implements INamedFilterService<T, F, WO, LO>
{
  protected constructor(
    protected graphql: GraphqlService,
    protected platform: PlatformService,
    protected account: AccountService,
    protected dataType: new () => T,
    protected filterType: new () => F,
    protected options: BaseEntityServiceOptions<T, number>
  ) {
    super(graphql, platform, dataType, filterType, {
      defaultSortBy: 'name',
      defaultSortDirection: 'asc',
      ...options,
    });
    this._logPrefix = '[named-filter-service] ';
  }

  async suggest(value: any, filter: Partial<F>): Promise<LoadResult<T>> {
    if (EntityUtils.isNotEmpty(value, 'id')) return { data: [value] };
    value = (typeof value === 'string' && value !== '*' && value) || undefined;
    return this.loadAll(0, !value ? 30 : 10, 'name', 'asc', this.asSuggestFilter(value, filter), <EntitiesServiceLoadOptions>{
      withTotal: true,
    });
  }

  asSuggestFilter(searchText: string, filter?: Partial<F>): F {
    return <F>{
      ...filter,
      searchText,
    };
  }

  asFilter(source: any): F {
    const filter = super.asFilter(source);

    // Default filter on current user
    if (this.account.isLogin() && isNil(filter.recorderPersonId) && isNil(filter.recorderDepartmentId))
      filter.recorderPersonId = this.account.person.id;

    return filter;
  }

  protected fillDefaultProperties(source: T) {
    super.fillDefaultProperties(source);

    // Default recorder
    if (isNil(source.recorderPersonId) && isNil(source.recorderDepartmentId)) source.recorderPersonId = this.account.person.id;

    // Stringify content
    if (typeof source.content === 'object') {
      source.content = JSON.stringify(source.content);
    }
  }
}
