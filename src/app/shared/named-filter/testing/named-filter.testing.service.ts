import { Injectable } from '@angular/core';
import { NamedFilter, NamedFilterFilter } from '../named-filter.model';
import { AbstractNamedFilterService, NamedFilterLoadOptions, NamedFilterWatchOptions } from '../named-filter.service';
import {
  AccountService,
  EntitiesServiceLoadOptions,
  EntitySaveOptions,
  GraphqlService,
  LoadResult,
  PlatformService,
  isNil,
  isNotNilString,
} from 'public_api';
import { SortDirection } from '@angular/material/sort';

@Injectable()
export class DummyNamedFilterService extends AbstractNamedFilterService<
  NamedFilter,
  NamedFilterFilter,
  NamedFilterWatchOptions,
  NamedFilterLoadOptions
> {
  private _dummyData: {
    id: number;
    name: string;
    entityName: string;
    content: string;
    recorderPersonId: number;
    recorderDepartmentId: number;
  }[] = [
    {
      id: 1,
      name: 'Filter B',
      entityName: 'DummyEntity',
      content: '{"filterA": "","filterB": "B value", "filterC": ""}',
      recorderPersonId: 1,
      recorderDepartmentId: 1,
    },
  ];

  constructor(
    protected graphql: GraphqlService,
    protected platform: PlatformService,
    protected accountService: AccountService
  ) {
    super(graphql, platform, accountService, NamedFilter, NamedFilterFilter, { queries: null, mutations: null });
  }

  async load(id: number, opts?: EntitiesServiceLoadOptions): Promise<NamedFilter<NamedFilter<any>>> {
    return NamedFilter.fromObject(this._dummyData.find((data) => data.id === id));
  }

  async loadAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<NamedFilterFilter<NamedFilterFilter<any, any>, NamedFilter<any>>>,
    opts?: EntitiesServiceLoadOptions & { debug?: boolean }
  ): Promise<LoadResult<NamedFilter<NamedFilter<any>>>> {
    const data = this._dummyData.map((data) => NamedFilter.fromObject(data));
    const result = isNotNilString(filter?.searchText) ? data.filter((data) => data.name.startsWith(filter.searchText)) : data;
    return {
      data: result,
      total: result.length,
    };
  }

  async save(entity: NamedFilter<NamedFilter<any>>, opts?: EntitySaveOptions): Promise<NamedFilter<NamedFilter<any>>> {
    if (isNil(entity?.id)) entity.id = this._computeNextId();
    this._dummyData = [...this._dummyData.filter((data) => data.id !== entity.id), entity].sort((a, b) => a.id - b.id);
    return entity;
  }

  async delete(entity: NamedFilter<NamedFilter<any>>, opts?: any): Promise<any> {
    this._dummyData = this._dummyData.filter((data) => data.id !== entity.id);
  }

  private _computeNextId(): number {
    const ids = this._dummyData.map((data) => data.id);
    return Math.max(...ids) + 1;
  }
}
