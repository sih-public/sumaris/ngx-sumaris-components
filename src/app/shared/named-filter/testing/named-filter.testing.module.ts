import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared.module';
import { NamedFilterSelectorTestingPage } from './named-filter-selector.testing';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'name-filter-selector',
    pathMatch: 'full',
    component: NamedFilterSelectorTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [NamedFilterSelectorTestingPage],
  exports: [RouterModule, NamedFilterSelectorTestingPage],
})
export class NamedFilterSelectorTestingModule {}
