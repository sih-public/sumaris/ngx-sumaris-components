import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatAutocompleteFieldConfig } from '../../material/autocomplete/material.autocomplete.config';
import { NamedFilter } from '../named-filter.model';
import { LocalSettingsService } from '../../../core/services/local-settings.service';

class DummyFilter {
  filterA: string;
  filterB: string;
  filterC: string;
}

@Component({
  selector: 'app-name-filter-selector-testing',
  templateUrl: './named-filter-selector.testing.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamedFilterSelectorTestingPage implements OnDestroy {
  private _subscription = new Subscription();
  protected _filterForm: UntypedFormGroup;
  protected mobile: boolean;

  protected namedFilterContentProvider = (): object => this.filterForm.value;
  protected namedFilterAutocompleteConfig: MatAutocompleteFieldConfig = {
    showAllOnFocus: true,
  };

  @Input()
  set filterForm(value: UntypedFormGroup) {
    this.setFilterForm(value);
  }

  get filterForm(): UntypedFormGroup {
    return this._filterForm;
  }
  constructor(
    protected cd: ChangeDetectorRef,
    protected settings: LocalSettingsService,
    formBuilder: UntypedFormBuilder
  ) {
    this._filterForm = formBuilder.group({
      filterA: [null],
      filterB: [null],
      filterC: [null],
    });
    this.mobile = settings.mobile;
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  setFilterForm(filterForm: UntypedFormGroup) {
    if (this._filterForm !== filterForm) {
      this._filterForm = filterForm;
      this._subscription.add(this._filterForm.statusChanges.subscribe((_) => this.markForCheck()));
    }
  }

  getFilterValue(): any {
    return this._filterForm.value;
  }

  protected markForCheck() {
    this.cd.detectChanges();
  }

  protected filterImportCallback = async (namedFilter: NamedFilter): Promise<NamedFilter> => {
    delete namedFilter.id;
    delete namedFilter.updateDate;
    return namedFilter;
  };

  async setFilter(filter: Partial<DummyFilter>, opts?: { emitEvent: boolean }) {
    filter = filter || {};
    this.filterForm.patchValue(filter, { emitEvent: false });
    this.markForCheck();
  }
}
