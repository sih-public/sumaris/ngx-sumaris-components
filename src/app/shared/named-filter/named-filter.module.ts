import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { NamedFilterSelector } from './named-filter-selector.component';
import { SharedMatAutocompleteModule } from '../material/autocomplete/material.autocomplete.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedMaterialModule } from '../material/material.module';

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), SharedMatAutocompleteModule, ReactiveFormsModule, SharedMaterialModule],
  declarations: [NamedFilterSelector],
  exports: [NamedFilterSelector],
})
export class SharedNamedFilterModule {}
