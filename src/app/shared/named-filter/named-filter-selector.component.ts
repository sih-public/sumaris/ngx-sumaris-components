import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { PopoverController, ToastController } from '@ionic/angular';
import { AppForm } from '../../core/form/form.class';
import { FilesUtils } from '../file/file.utils';
import { isEmptyArray, isNil, isNilOrBlank, isNilOrNaN, isNotNil, isNotNilOrBlank } from '../functions';
import { MatAutocompleteField } from '../material/autocomplete/material.autocomplete';
import { MatAutocompleteFieldConfig } from '../material/autocomplete/material.autocomplete.config';
import { Toasts } from '../toast/toasts';
import { INamedFilter, INamedFilterFilter, NamedFilter } from './named-filter.model';
import { APP_NAMED_FILTER_SERVICE, INamedFilterService, NamedFilterLoadOptions, NamedFilterWatchOptions } from './named-filter.service';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';

export type NamedFilterSelectorButtonsPosition = 'matSuffix' | 'after';

@Component({
  selector: 'app-named-filter-selector',
  styleUrls: ['./named-filter-selector.component.scss'],
  templateUrl: './named-filter-selector.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamedFilterSelector extends AppForm<any> implements OnInit {
  protected readonly logPrefix = '[named-filter-selector]';

  private _entityName: string;
  private _deleteDisabled = false;

  @Input() mobile: boolean;
  @Input() set entityName(entityName: string) {
    if (!entityName || this._entityName === entityName) return;
    this._entityName = entityName;
    // Update autocomplete filter
    this.autocompleteField.filter = { ...this.autocompleteField.filter, ...this.getFilter() };
  }

  get entityName(): string {
    return this._entityName;
  }

  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input() filterContentProvider: () => any;
  @Input() filterImportCallback: (namedFilter: NamedFilter) => Promise<NamedFilter>;
  @Input() filterFormDirty: boolean;
  @Input() showButtons = true;
  @Input() exportFileNamePrefix = 'filter';
  @Input() autocompleteConfig: MatAutocompleteFieldConfig;
  @Input() dropButtonTitle: string;
  @Input() clearButtonTitle: string;
  @Input() detectChangeOnSelectFilter: boolean = false; // Else is on value change
  @Input() buttonsPosition: NamedFilterSelectorButtonsPosition;

  @Input() set disabled(_disabled: boolean) {
    if (_disabled) this.disable();
    else this.enable();
  }
  get disabled(): boolean {
    return !this._enabled;
  }

  get isNew(): boolean {
    return isNilOrNaN(this.value?.namedFilter?.id);
  }

  get saveDisabled(): boolean {
    return !this.dirty || isNilOrBlank(this.value.namedFilter);
  }

  get deleteDisabled(): boolean {
    return this._deleteDisabled || this.isNew;
  }

  get dirty(): boolean {
    return super.dirty || this.filterFormDirty;
  }

  @Output() filterSelected = new EventEmitter<any>();
  @Output() filterDeleted = new EventEmitter<NamedFilter>();
  @Output() filterCleared = new EventEmitter<void>();

  @ViewChild(MatAutocompleteField, { static: true }) autocompleteField: MatAutocompleteField;

  constructor(
    protected injector: Injector,
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef,
    protected toastController: ToastController,
    protected popoverController: PopoverController,
    @Inject(APP_NAMED_FILTER_SERVICE)
    protected dataService: INamedFilterService<INamedFilter<any>, INamedFilterFilter<any, any>, NamedFilterWatchOptions, NamedFilterLoadOptions>
  ) {
    super(injector, formBuilder.group({ namedFilter: [null] }));
  }

  ngOnInit(): void {
    this.mobile = this.mobile ?? this.settings.mobile;
    this.buttonsPosition = this.buttonsPosition ?? (this.mobile ? 'after' : 'matSuffix');

    this.registerAutocompleteField('namedFilter', {
      suggestFn: (value, filter) => this.dataService.suggest(value, filter),
      attributes: ['name'],
      filter: this.getFilter(),
      applyImplicitValue: false,
      ...this.autocompleteConfig,
    });

    if (this.detectChangeOnSelectFilter) {
      this.registerSubscription(this.autocompleteField.selectionChange.subscribe((event) => this.onFilterChanges(event.value)));
    } else {
      this.registerSubscription(
        this.form.controls.namedFilter.valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe((value) => this.onFilterChanges(value))
      );
    }
  }

  getFilter(): any {
    return {
      entityName: this.entityName,
    };
  }

  markForCheck() {
    this.cd.markForCheck();
  }

  async save(event: UIEvent) {
    if (event?.defaultPrevented) return;
    event?.preventDefault();
    event?.stopPropagation();

    const filterContent = this.filterContentProvider();

    if (this.debug) console.debug(`${this.logPrefix} save named filter`, filterContent);

    if (isNilOrBlank(filterContent)) return;

    const currentSelected = this.value.namedFilter;
    let result: NamedFilter;

    if (typeof currentSelected === 'string') {
      result = NamedFilter.fromObject({
        name: currentSelected,
        entityName: this.entityName,
        content: filterContent,
      });
    } else {
      result = NamedFilter.fromObject({
        ...currentSelected,
        content: filterContent,
      });
    }

    const savedEntity = await this.dataService.save(result);
    this.setControlValue(savedEntity);
    this.autocompleteField.reloadItems();
    this.markAsPristine();
  }

  async delete(event?: UIEvent) {
    if (event?.defaultPrevented) return;
    event?.preventDefault();
    event?.stopPropagation();

    const data = this.value.namedFilter;

    if (this.debug) console.debug(`${this.logPrefix} delete named filter`, data);

    if (typeof data === 'object') {
      await this.dataService.delete(data);
      this.reset();
      this.autocompleteField.reloadItems();
      this.autocompleteField.blurred.emit();
      this.filterDeleted.emit(data);
      Toasts.show(this.toastController, this.translate, {
        type: 'info',
        message: 'COMMON.NAMED_FILTER.DELETED',
      });
    }
  }

  onMoreOptionsClick(event: Event) {
    event.preventDefault();
    event.stopPropagation(); // Avoid field focus gain
  }

  async export(event?: UIEvent) {
    if (event?.defaultPrevented) return;
    event?.preventDefault();

    const id = this.autocompleteField.value?.id;

    if (this.debug) console.debug(`${this.logPrefix} export named filter`, id);

    if (!id) return;

    const data = await this.dataService.load(id, { withContent: true });

    if (data) {
      FilesUtils.writeTextToFile(JSON.stringify(data, null, 2), {
        type: 'application/json',
        filename: `${this.exportFileNamePrefix}-${data.entityName}-${data.name}.json`,
      });
    } else {
      Toasts.show(this.toastController, this.translate, {
        type: 'error',
        message: 'COMMON.NAMED_FILTER.NOT_FOUND',
      });
    }
  }

  async import(event: Event) {
    if (event?.defaultPrevented) return;
    event?.preventDefault();

    const result = await FilesUtils.showUploadPopover(this.popoverController, event, {
      uploadFn: (file) => FilesUtils.readAsText(file),
      title: 'COMMON.NAMED_FILTER.IMPORT',
      fileExtension: '.json',
      uniqueFile: true,
      instantUpload: true,
    });
    if (!result || isEmptyArray(result.data)) return;
    try {
      let data = JSON.parse(result.data[0].response.body);
      if (this.debug) console.debug(`${this.logPrefix} import named filter`, data);
      if (this.filterImportCallback) {
        data = await this.filterImportCallback(data);
        if (isNil(data)) {
          return; // Abort import if callback returns null or undefined
        }
      }
      this.setControlValue(data);
      this.filterSelected.emit(data.content);
      this.markAsDirty();
    } catch (e) {
      console.error(e);
      Toasts.show(this.toastController, this.translate, {
        type: 'error',
        message: 'COMMON.NAMED_FILTER.IMPORT_FAILED',
      });
    }
  }

  protected async onFilterChanges(value: NamedFilter | string) {
    if (this.debug) console.debug(`${this.logPrefix} filter change`, value);

    // Emit cleared event if bound
    if (isNil(value) && this.filterCleared.observed) {
      this.filterCleared.emit();
    }

    if (typeof value === 'object' && value?.id && isNotNil(value.id)) {
      try {
        const data = await this.dataService.load(value.id, { withContent: true });
        this._deleteDisabled = !this.dataService.canUserWrite(data);

        const filter = isNotNilOrBlank(data?.content) ? data.content : null;
        if (filter) {
          this.filterSelected.emit(filter);
          this.markAsPristine();
        }
      } catch (e) {
        Toasts.show(this.toastController, this.translate, {
          type: 'error',
          message: 'COMMON.NAMED_FILTER.LOAD_FAILED',
        });
      }
    }
  }

  protected setControlValue(value: NamedFilter) {
    this.setValue({ namedFilter: value });
    this.autocompleteField.blurred.emit();
  }
}
