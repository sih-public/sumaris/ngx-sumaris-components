import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, RouteReuseStrategy, RouterModule } from '@angular/router';
import { IonicRouteStrategy } from '@ionic/angular';
import { ComponentDirtyGuard } from './guard/component-dirty.guard';
import { isNil, isNotNil } from './functions';

@Injectable()
export class CustomReuseStrategy extends IonicRouteStrategy {
  /**
   * Force to reuse the route when:
   *  - path change from [/new] -> [/:id]
   *  - or path change from [/new] -> [/new?id=:id]
   *
   * @param future
   * @param curr
   */
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    const reuse = super.shouldReuseRoute(future, curr);

    // Is sam route config
    if (!reuse && future.routeConfig && future.routeConfig === curr.routeConfig) {
      // Read the parameter name, where id is stored
      const pathIdParam = (future.routeConfig.data && future.routeConfig.data.pathIdParam) || 'id';

      // Read the current route id
      const currId =
        curr.params[pathIdParam] === 'new'
          ? curr.queryParams[pathIdParam] || curr.queryParams['id'] || 'new' // e.g. path like '/new/?id=<ID>'
          : curr.params[pathIdParam]; // e.g. path like '/<ID>'

      // Read the future route id
      const futureId =
        future.params[pathIdParam] === 'new'
          ? future.queryParams[pathIdParam] || future.queryParams['id'] || 'new' // e.g. path like '/new/?id=<ID>'
          : future.params[pathIdParam]; // e.g. path like '/<ID>'

      // Reuse if same ID, using a futureId, if exists
      if (isNotNil(futureId)) {
        // Allow reuse when changing from /new to /new?id=:id
        if (
          currId === 'new' &&
          isNil(curr.queryParams[pathIdParam]) &&
          future.params[pathIdParam] === 'new' &&
          isNotNil(future.queryParams[pathIdParam])
        ) {
          return true;
        }

        // Allow reuse same id, expect when expected '/new'
        return futureId !== 'new' && futureId === currId;
      }
    }

    return reuse;
  }
}

@NgModule({
  imports: [RouterModule],
  providers: [
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    { provide: ComponentDirtyGuard, useClass: ComponentDirtyGuard },
  ],
})
export class SharedRoutingModule {
  constructor() {
    console.debug('[shared-routing] Creating module...');
  }
}
