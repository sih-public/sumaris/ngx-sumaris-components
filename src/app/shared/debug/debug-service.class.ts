import { InjectionToken } from '@angular/core';

export const APP_DEBUG_DATA_SERVICE = new InjectionToken<IDebugDataService>('DebugDataService');

export interface IDebugDataService {
  sendDebugData: (data: any) => Promise<void>;
}
