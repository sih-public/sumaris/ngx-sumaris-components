import { NgModule } from '@angular/core';
import { DebugComponent } from './debug.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MatIcon, MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [CommonModule, IonicModule, MatExpansionModule, MatIconModule, TranslateModule.forChild(), MatExpansionModule],
  declarations: [DebugComponent],
  exports: [DebugComponent, TranslateModule],
})
export class SharedDebugModule {}
