import { Component, Inject, Input, Optional, ViewChild } from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';

@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss'],
})
export class DebugComponent {
  @Input() titlePrefix = 'COMMON.DEBUG.TITLE';
  @Input() title = '';
  @Input() enable: boolean = null;
  @Input() expanded = false;

  @ViewChild('expansionPanel') expansionPanel: MatExpansionPanel;

  constructor(@Inject(ENVIRONMENT) @Optional() environment: Environment) {
    this.enable = !environment || !environment.production;
  }

  toggle() {
    this.expansionPanel.toggle();
  }
}
