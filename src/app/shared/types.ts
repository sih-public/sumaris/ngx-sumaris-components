import { ThemePalette } from '@angular/material/core';
import { PredefinedColors } from '@ionic/core';
import { Predicate } from '@angular/core';

export declare type KeyType = string | number;
export declare type KeyValueType<T> = { [key in KeyType]: T };

export declare type KeysEnum<T> = { [P in keyof Required<T>]: boolean };

export declare interface ObjectMap<T = any> {
  [key: string]: T;
}

export declare interface ObjectMapEntry<T = any> {
  key: string;
  value?: T;
}

export declare type PropertyMap<T = string> = ObjectMap<T>;

/**
 * Same as PropertyMap
 * @deprecated Use PropertyMap instead
 */
export declare type PropertiesMap<T = string> = PropertyMap<T>;

export declare type Property<T = string> = ObjectMapEntry<T>;

export declare type PropertiesArray = Property[];

export declare type AppColors = PredefinedColors | ThemePalette | string;

export declare interface IconRef {
  icon?: string; // An ion-icon name
  matIcon?: string; // A mat icon
  matSvgIcon?: string; // A mat SVG icon
  color?: AppColors;
}

export declare type FilterFn<T> = Predicate<T>;
export declare type FilterFnFactory<T, F> = (filter: F) => FilterFn<T>;

export declare type Function<P, R> = (value: P) => R;
export declare type BiFunction<P1, P2, R> = (v1: P1, v2: P2) => R;
