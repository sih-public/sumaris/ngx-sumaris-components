import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TestingPage } from '../testing/tests.page';

export const SHARED_STORAGE_TESTING_PAGES: TestingPage[] = [{ label: 'Storage explorer', page: '/testing/shared/storage' }];

const routes: Routes = [
  {
    path: 'storage',
    loadChildren: () => import('./storage-explorer.testing-routing.module').then((m) => m.StorageExplorerTestingRoutingModule),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StorageExplorerTestingModule {}
