import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, InjectionToken, Input, OnInit, Optional, ViewChild } from '@angular/core';
import { AlertController, IonModal, ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { isNotNilOrBlank, removeDuplicatesFromArray, sleep, toBoolean } from '../functions';
import { APP_STORAGE, IStorage } from './storage.utils';
import { APP_DEBUG_DATA_SERVICE, IDebugDataService } from '../debug/debug-service.class';
import { TranslateService } from '@ngx-translate/core';
import { Toasts } from '../toast/toasts';
import { JsonUtils } from '../file/json.utils';
import { Environment, ENVIRONMENT } from '../../../environments/environment.class';
import { Log, LogUtils } from '../logging/logger.model';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DATE_ISO_PATTERN } from '../constants';
import { LogLevel } from '../logging/log-level.model';
import { DateUtils } from '../dates';
import { Alerts } from '../alerts';
import { FilesUtils } from '../file/file.utils';
import { PlatformService } from '../../core/services/platform.service';

interface StorageItemInfo {
  key: string;
  type: 'array' | 'object' | 'string' | 'number';
  total: number;
}

export const APP_STORAGE_EXPLORER_PROTECTED_KEYS = new InjectionToken<string[]>('StorageExplorerProtectedKeys');

@Component({
  selector: 'app-storage-explorer',
  templateUrl: './storage-explorer.component.html',
  styleUrls: ['./storage-explorer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StorageExplorerComponent implements OnInit {
  private _logPrefix = '[storage-explorer] ';
  protected $loading = new BehaviorSubject<boolean>(true);
  protected $items = new BehaviorSubject<StorageItemInfo[]>([]);
  protected _logKey: string;

  protected data: any;
  protected logs: Log[];
  protected key: string;

  @Input() mobile: boolean;
  @Input() showSendButton: boolean;

  @ViewChild('sendDataModal') sendDataModal: IonModal;
  @ViewChild('viewDataModal') viewDataModal: IonModal;

  constructor(
    protected platform: PlatformService,
    protected toastController: ToastController,
    protected alertCtrl: AlertController,
    protected translate: TranslateService,
    protected dateAdapter: MomentDateAdapter,
    protected cd: ChangeDetectorRef,
    @Inject(APP_STORAGE) protected readonly storage: IStorage,
    @Inject(ENVIRONMENT) protected readonly environment: Environment,
    @Optional() @Inject(APP_DEBUG_DATA_SERVICE) protected readonly debugDataService: IDebugDataService,
    @Optional() @Inject(APP_STORAGE_EXPLORER_PROTECTED_KEYS) private readonly _protectedKeys: string[]
  ) {
    this._protectedKeys = removeDuplicatesFromArray([...(this._protectedKeys || []), 'seckey', 'token']);
    this._logKey = environment.logging?.localStorageAppender?.localStorageKey || 'log';
  }

  ngOnInit() {
    this.showSendButton = toBoolean(this.showSendButton, true) && !!this.debugDataService;
    this.mobile = toBoolean(this.mobile, this.platform.is('mobile'));

    this.storage.ready().then(() => this.start());
  }

  async sendDataClick(event: Event, key: string) {
    if (!this.debugDataService) {
      console.warn(`${this._logPrefix}Cannot send debug data: missing provider for 'APP_DEBUG_DATA_SERVICE'`);
      return;
    }
    this.sendDataModal.present();

    const { data, role } = await this.sendDataModal.onDidDismiss();
    if (role === 'send') {
      this.sendData(key);
    }
  }

  async viewDataClick(event: Event, key: string) {
    if (!key) return; // skip

    try {
      this.key = key;
      const data = await this.storage.get(key);
      if (this._logKey === key) {
        const logs = Array.isArray(data) ? data : JSON.parse(data);
        this.logs = LogUtils.fromObjects(logs);
      } else {
        this.data = this.toString(key, data);
      }

      this.viewDataModal.present();

      await this.viewDataModal.onWillDismiss();
    } finally {
      // Forget the data
      this.data = null;
      this.logs = null;
      this.key = null;
    }
  }

  async sendData(key: string) {
    if (!key || !this.debugDataService) return; // Skip

    const rawValue = await this.storage.get(key);
    try {
      await this.debugDataService.sendDebugData(rawValue);
    } catch (e) {
      await Toasts.show(this.toastController, this.translate, {
        type: 'error',
        message: e.message,
      });
      return;
    }

    // Confirm message was sent
    await Toasts.show(this.toastController, this.translate, {
      type: 'info',
      message: this.translate.instant('SOCIAL.INFO.MESSAGE_SENT'),
    });
  }

  async downloadAsJson(key: string) {
    if (!key) return; // skip
    const data = await this.storage.get(key);
    const content = this.toString(key, data);

    if (key === this._logKey) {
      FilesUtils.writeTextToFile(content, { filename: `${key}.txt` });
    } else {
      JsonUtils.exportToFile(content, { filename: `${key}.json` });
    }
  }

  async copyToClipboard(key: string) {
    if (!key) return; // skip
    const data = await this.storage.get(key);

    try {
      const text = this.toString(key, data);

      // Copy to clipboard
      await this.platform.copyToClipboard(text, {message: 'INFO.COPY_SUCCEED'});

    } catch (error) {
      console.error('Error copying text to clipboard', error);
    }
  }

  protected async start() {
    const items = [];
    this.storage.forEach((value, key) => {
      if (this._protectedKeys.includes(key)) return; // Skip protected keys

      if (Array.isArray(value)) {
        items.push({
          key,
          type: 'array',
          total: value.length,
        });
      } else if (typeof value === 'object') {
        let total;
        if (value?.type && value?.type === 'object') {
          const nestedValue = JSON.parse(value.value);
          total = Array.isArray(nestedValue) ? nestedValue.length : Object.keys(nestedValue).length > 0 ? 1 : 0;
        } else {
          total = Object.keys(value).length > 0 ? 1 : 0;
        }
        items.push({
          key,
          type: 'object',
          total,
        });
      } else {
        const type = typeof value === 'number' ? 'number' : 'string';
        items.push({
          key,
          type,
          total: isNotNilOrBlank(value) ? 1 : 0,
        });
      }

      this.$items.next(items);
    });

    this.$loading.next(false);
  }

  protected async refresh() {
    if (this.$loading.value) return; // SKip if loading

    this.$loading.next(true);
    await this.start();
  }

  protected toString(key: string, data: any, html?: boolean): string {
    if (key === this._logKey) {
      let dateTimePattern = this.translate.instant('COMMON.DATE_TIME_SECONDS_PATTERN');
      if (dateTimePattern === 'COMMON.DATE_TIME_SECONDS_PATTERN') {
        dateTimePattern = DATE_ISO_PATTERN;
      }
      const logs = Array.isArray(data) ? data : JSON.parse(data);
      return this.toLogs(data)
        .map((log) =>
          [this.dateAdapter.format(log.date, dateTimePattern), LogUtils.levelToString(log.level), log.loggerName + '\t', log.message].join('\t')
        )
        .join('\n');
    }

    // Force settings to be indent
    if (key === 'settings') {
      data = typeof data === 'object' ? data : JSON.parse(data);
    }

    // Default case
    return typeof data === 'string' ? data : JSON.stringify(data, null, 2);
  }

  protected toLogs(data: any): Log[] {
    try {
      const logs = Array.isArray(data) ? data : data ? JSON.parse(data) : [];
      return LogUtils.fromObjects(logs);
    } catch (err) {
      console.error(err);
      return [
        {
          loggerName: 'storage-explorer',
          level: LogLevel.FATAL,
          message: err?.message || JSON.stringify(err),
          date: DateUtils.moment(),
        },
      ];
    }
  }

  protected async clear(key: string) {
    const confirmed = await Alerts.askActionConfirmation(this.alertCtrl, this.translate, true);
    if (!confirmed) return;

    this.logs = [];
    this.cd.markForCheck();

    await sleep(650);

    await this.storage.remove(key);

    this.viewDataModal.dismiss();

    this.refresh();
  }

  protected trackByFn(index: number, item: StorageItemInfo): any {
    return item.key;
  }

  protected levelToColor(level: LogLevel): string {
    return `var(--ion-color-${LogUtils.levelToColor(level)})`;
  }
  protected levelToString = LogUtils.levelToString;
}
