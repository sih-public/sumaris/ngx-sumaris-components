import { Inject, Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { IStorage, StorageDrivers } from './storage.utils';
import { StartableService } from '../services/startable-service.class';
import { Platform } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { ENVIRONMENT, Environment } from '../../../environments/environment.class';
import * as cordovaSQLiteDriver from 'localforage-cordovasqlitedriver';

@Injectable({
  providedIn: 'root',
  deps: [ENVIRONMENT],
})
export class StorageService extends StartableService<Storage> implements IStorage<Storage> {
  get driver(): string | undefined {
    return this._data?.driver;
  }

  constructor(
    platform: Platform,
    private storage: Storage,
    @Inject(ENVIRONMENT) private environment: Environment
  ) {
    super(platform);
    this._debug = !environment.production;
    this.start();
  }

  protected async ngOnStart(): Promise<Storage> {
    try {
      console.debug(`[storage] Starting... {driverOrder: ${environment.storage?.driverOrder}}`);

      // Define Cordova SQLLite driver
      await this.storage.defineDriver(cordovaSQLiteDriver);

      // Create the storage instance
      const storage = await this.storage.create();

      // Migrate from old storage
      await this.migrate(storage);

      console.info(`[storage] Started using driver: ${storage?.driver}`);
      return storage;
    } catch (err) {
      console.error('[storage] Cannot create storage: ' + (err?.message || err), err);
    }
  }

  protected async migrate(storage: Storage) {
    const canMigrate = storage?.driver === StorageDrivers.SQLLite;

    if (!canMigrate) return; // Skip

    //
    // const oldForage = storage.createInstance({
    //   name: storage.config().name,
    //   storeName: storage.config().storeName,
    //   driver: [storage.INDEXEDDB, storage.WEBSQL]
    // });
    //
    // const keys = await oldForage.keys();
    //
    // // No data in the odl storage: skip migration
    // if (isEmptyArray(keys)) {
    //   // Drop the old instance (not need anymore)
    //   console.debug(`[platform] Old storage is empty: dropping unused instance {name: '${storage.config().name}', driver: '${oldForage.driver()}'}`);
    //   await oldForage.dropInstance();
    // }
    // // IF some data stored in the OLD storage: start migration
    // else {
    //
    //   const now = Date.now();
    //   console.info(`[platform] Starting storage migration...`);
    //
    //   const toast = await this.showToast({message: 'INFO.DATA_MIGRATION_STARTED', type: 'info', duration: 30000});
    //   try {
    //     await StorageUtils.copy(oldForage, storage, {keys, deleteAfterCopy: false});
    //
    //     const duration = Date.now() - now;
    //     setTimeout(() => toast.dismiss(),  Math.max(2000 - duration, 0));
    //     await this.showToast({message: 'INFO.DATA_MIGRATION_SUCCEED',
    //       type: 'info',
    //       showCloseButton: true
    //     });
    //     console.info('[platform] Starting storage migration [OK]');
    //
    //     // Drop the old instance
    //     console.info(`[platform] Drop old storage {name: '${storage.config().name}', driver: '${oldForage.driver()}'}`);
    //     await oldForage.dropInstance();
    //   }
    //   catch (err) {
    //     console.error(err && err.message || err, err);
    //     await toast.dismiss();
    //     await this.showToast({message: 'ERROR.DATA_MIGRATION_FAILED',
    //       type: 'error',
    //       showCloseButton: true
    //     });
    //   }
    //}
  }

  async set(key: string, value: any) {
    if (!this.started) await this.ready();
    return this._data.set(key, value);
  }

  async get(key: string): Promise<any> {
    if (!this.started) await this.ready();
    return this._data.get(key);
  }

  async remove(key: string) {
    if (!this.started) await this.ready();
    return this._data.remove(key);
  }

  async keys(): Promise<string[]> {
    if (!this.started) await this.ready();
    return this._data.keys();
  }

  async clear() {
    if (!this.started) await this.ready();
    await this._data.clear();
  }

  async forEach(iteratorCallback: (value: any, key: string, iterationNumber: number) => any): Promise<void> {
    if (!this.started) await this.ready();
    return this._data.forEach(iteratorCallback);
  }
}
