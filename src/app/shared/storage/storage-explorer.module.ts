import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageExplorerComponent } from './storage-explorer.component';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { SharedPipesModule } from '../pipes/pipes.module';
import { SharedMaterialModule } from '../material/material.module';
import { RxStateModule } from '../rx-state/rx-state.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    TranslateModule.forChild(),
    RxStateModule,

    // Other shared modules
    SharedPipesModule,
    SharedMaterialModule,
  ],
  declarations: [StorageExplorerComponent],
  exports: [StorageExplorerComponent],
})
export class StorageExplorerModule {}
