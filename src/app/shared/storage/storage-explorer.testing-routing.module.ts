import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StorageExplorerModule } from './storage-explorer.module';
import { StorageExplorerComponent } from './storage-explorer.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: StorageExplorerComponent,
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), StorageExplorerModule],
  exports: [RouterModule],
})
export class StorageExplorerTestingRoutingModule {}
