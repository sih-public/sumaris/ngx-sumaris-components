import { Geolocation, Position } from '@capacitor/geolocation';
import { PlatformService } from '../../core/services/platform.service';
import { Platform } from '@ionic/angular';
import { isNil } from '../functions';

export class IPosition {
  latitude: number;
  longitude: number;
}

export abstract class GeolocationUtils {
  static isNotNilAndValid(position: IPosition, opts?: { debug?: boolean }) {
    if (!position || isNil(position.latitude) || isNil(position.longitude)) return false;

    // Invalid lat/lon
    if (position.latitude < -90 || position.latitude > 90 || position.longitude < -180 || position.longitude > 180) {
      // /!\ Log in console, because should never occur
      if (opts?.debug) console.warn('Invalid lat/long position:  ', position);
      return false;
    }

    // OK: valid
    return true;
  }

  static isNilOrInvalid(position: IPosition, opts?: { debug?: boolean }) {
    return !GeolocationUtils.isNotNilAndValid(position, opts);
  }

  static computeDistanceInMiles(position1: IPosition, position2: IPosition): number {
    // Invalid position(s): skip
    if (GeolocationUtils.isNilOrInvalid(position1, { debug: true }) || GeolocationUtils.isNilOrInvalid(position2, { debug: true })) return;

    const latitude1Rad = (Math.PI * position1.latitude) / 180;
    const longitude1Rad = (Math.PI * position1.longitude) / 180;
    const latitude2Rad = (Math.PI * position2.latitude) / 180;
    const longitude2Rad = (Math.PI * position2.longitude) / 180;

    let distance =
      2 *
      6371 *
      Math.asin(
        Math.sqrt(
          Math.pow(Math.sin((latitude1Rad - latitude2Rad) / 2), 2) +
            Math.cos(latitude1Rad) * Math.cos(latitude2Rad) * Math.pow(Math.sin((longitude2Rad - longitude1Rad) / 2), 2)
        )
      );
    distance = Math.round((distance / 1.852 + Number.EPSILON) * 100) / 100;
    return distance;
  }

  /**
   * Get the position by geo loc sensor
   */
  static async getCurrentPosition(platform: PlatformService | Platform, options?: PositionOptions): Promise<IPosition> {
    // Use Capacitor plugin
    if (platform.is('capacitor')) {
      try {
        console.info(`[geolocation-utils] Get current geo location, using Capacitor...(timeout: ${options?.timeout})`);
        const pos: Position = await Geolocation.getCurrentPosition(options);
        return <IPosition>{
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude,
        };
      } catch (err) {
        console.error(`[geolocation-utils] Cannot get current geo location, using Capacitor: ${err?.message || ''}`);

        // Detect some error, to add code
        // FIXME - Capacitor use hardcoded strings! (see https://github.com/ionic-team/capacitor-plugins)
        const message = ((err?.message || '') as string).toLowerCase();

        // Permission denied
        if (message.includes('location permission was denied') || message.includes('location disabled')) {
          throw { ...err, code: GeolocationPositionError.PERMISSION_DENIED };
        }

        // Timeout
        if (message.includes('timeout expired')) {
          throw { ...err, code: GeolocationPositionError.TIMEOUT };
        }
        throw err;
      }
    }

    // Or fallback to navigator
    console.info('[geolocation-utils] Get current geo location, using browser...');
    return new Promise<IPosition>((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (res) => {
          resolve(<IPosition>{
            latitude: res.coords.latitude,
            longitude: res.coords.longitude,
          });
        },
        (err) => {
          let message = err?.message || err;
          message = typeof message === 'string' ? message : JSON.stringify(message);
          console.error('[geolocation-utils] Cannot get current geo location, using browser:' + message);
          reject(err);
        },
        options
      );
    });
  }
}
