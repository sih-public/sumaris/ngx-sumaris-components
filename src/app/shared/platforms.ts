/* ---
 * Source: https://github.com/ionic-team/ionic-framework/blob/b0d53ca73619585671d8cf4dc24e47f826495a0a/core/src/utils/platform.ts
 * --- */

const _cache = {
  userAgent: null,
  mobile: null,
  cordova: null,
  capacitor: null,
};

export function matchMedia(win: Window, query: string): boolean {
  return win.matchMedia(query).matches;
}

export function getUserAgent(win: Window): string {
  if (!_cache.userAgent) {
    _cache.userAgent = win.navigator.userAgent || win.navigator.vendor || (window as any).opera;
  }
  return _cache.userAgent;
}

export function testUserAgent(win: Window, expr: RegExp) {
  const userAgent: string = getUserAgent(win);
  return expr.test(userAgent);
}

export function isWindows(win?: Window) {
  return testUserAgent(win, /windows/i);
}

export function isFirefox(win?: Window) {
  return testUserAgent(win, / firefox\//i);
}

export function isSafari(win?: Window) {
  return !isChrome(win) /*Exclude chromium*/ && testUserAgent(win, / safari\//i);
}

export function isChrome(win?: Window) {
  return testUserAgent(win, / chrome\//i);
}

export function isEdge(win?: Window) {
  return testUserAgent(win, /edge/i);
}

export function isTouchUi(win?: Window) {
  return matchMedia(win, '(any-pointer:coarse)');
}

export function isIpad(win: Window) {
  // iOS 12 and below
  if (testUserAgent(win, /iPad/i)) {
    return true;
  }

  // iOS 13+
  if (testUserAgent(win, /Macintosh/i) && isTouchUi(win)) {
    return true;
  }

  return false;
}

export function isMacOS(win: Window) {
  // iOS (but not iPad)
  return testUserAgent(win, /Macintosh/i) && !isTouchUi(win);
}

export function isIOS(win: Window) {
  return testUserAgent(win, /iPhone|iPod/i) || isIpad(win);
}

export function isAndroid(win: Window) {
  return testUserAgent(win, /android|sink/i);
}

export function isCapacitor(win: any): boolean {
  if (_cache.capacitor === null || _cache.capacitor === undefined) {
    _cache.capacitor = !!(win['Capacitor'] || win['capacitor']);
  }
  return _cache.capacitor;
}

export function isMobile(win: Window) {
  if (_cache.mobile === null || _cache.mobile === undefined) {
    _cache.mobile =
      isAndroid(win) ||
      (isIOS(window) && isTouchUi(win)) ||
      // Workaround, for MS Windows touch screen (detected as mobile, but forced as desktop)
      (isTouchUi(win) && !isWindows(win));
  }
  return _cache.mobile;
}
