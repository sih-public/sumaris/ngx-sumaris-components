import { Injectable, Optional } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AudioManagement } from '@ionic-native/audio-management/ngx';
import { isNil, sleep, toBoolean } from '../functions';
import { StartableService } from '../services/startable-service.class';
import { Haptics } from '@capacitor/haptics';
import { NativeAudio } from '@capacitor-community/native-audio';
import { Capacitor } from '@capacitor/core';

export type AudioType = 'html5' | 'native';
export interface Sound {
  assetId: string; // unique identifier of the file
  assetPath: string; //  relative path of the file or absolute url (file://)
  volume?: number; // Sound volume
  isUrl?: boolean; // pass true if assetPath is a `file://` url
  time?: number; // sound seek time
  audioChannelNum?: number; // number of audio channels
  vibration?: number | number[]; // Should vibrate ? set duration
}

const SYSTEM_SOUNDS: Sound[] = [
  { assetId: 'startup', assetPath: 'assets/audio/unfa-ping.mp3', vibration: [300, 250, 750] },
  { assetId: 'beep-confirm', assetPath: 'assets/audio/beep-confirm.mp3', vibration: 250 },
  { assetId: 'beep-error', assetPath: 'assets/audio/beep-error.mp3', vibration: 1000 },
  { assetId: 'beep-notification', assetPath: 'assets/audio/beep-notification.mp3', vibration: [450, 150, 450] },
];

const DEFAULT_AUDIO_MODE_RETURN: AudioManagement.AudioModeReturn = {
  audioMode: AudioManagement.AudioMode.NORMAL,
  label: 'Normal',
};

function audioModeToString(mode: AudioManagement.AudioMode) {
  switch (mode) {
    case AudioManagement.AudioMode.SILENT:
      return 'Silent';
    case AudioManagement.AudioMode.NORMAL:
      return 'Normal';
    case AudioManagement.AudioMode.VIBRATE:
      return 'Vibrate';
  }
}

@Injectable({ providedIn: 'root' })
export class AudioProvider extends StartableService<void> {
  private _audioType: AudioType;
  private _audioMode: AudioManagement.AudioMode = AudioManagement.AudioMode.NORMAL;
  private _enableAudioManagment = false;
  private _assetPathPrefix = '';

  private _preloadedSounds: { [id: string]: Sound } = {};
  private _htmlAudioCache: { [id: string]: HTMLAudioElement } = {};

  constructor(
    protected platform: Platform,
    @Optional() private audioManagement: AudioManagement
  ) {
    super(platform);
    console.info(`[audio] Create audio provider`);
  }

  protected async ngOnStart(): Promise<void> {
    const cordova = this.platform.is('cordova');
    this._assetPathPrefix = cordova ? 'public/' : '';
    this._audioType = cordova && Capacitor.isPluginAvailable('NativeAudio') ? 'native' : 'html5';
    this._enableAudioManagment = cordova && this.audioManagement && this.platform.is('android');
    console.info(`[audio] Starting audio provider {type: ${this._audioType}, management: ${this._enableAudioManagment}}...`);

    // Listen audio mode changed
    if (this._enableAudioManagment) {
      await this.readAudioMode();
    }

    // Pre-loading system sounds
    console.debug('[audio] Preloading audio sounds...');
    let errorCount = 0;
    await Promise.all(
      SYSTEM_SOUNDS.map((s) => {
        // Disable vibration in HTML 5 mode
        if (this._audioType === 'html5') s.vibration = undefined;
        return this.preload(s).catch((_) => errorCount++);
      })
    );

    if (errorCount > 0) {
      console.warn('[audio] Preloading audio sounds [OK] with ${errorCount} errors during preload.');
    } else {
      console.debug('[audio] Preloading audio sounds [OK]');
    }
  }

  playBeepConfirm(): Promise<any> {
    return this.play('beep-confirm', {
      // Vibrate only if in vibration mode
      vibrate: this._audioMode === AudioManagement.AudioMode.VIBRATE,
    });
  }

  playBeepError(): Promise<any> {
    return this.play('beep-error', {
      // Always vibrate (will skip vibration if silent mode)
      vibrate: true,
    });
  }

  async playBeepNotification(): Promise<any> {
    await this.play('beep-notification', {
      vibrate: this._audioMode === AudioManagement.AudioMode.VIBRATE,
    });
  }

  async playStartupSound(): Promise<any> {
    return this.play('startup');
  }

  async preload(sound: Sound) {
    if (!sound?.assetId) return; // Skip
    if (this._preloadedSounds[sound.assetId]) return; // Already preloaded
    if (isNil(this._audioType)) await this.ready();

    const assetPath = (this._assetPathPrefix || '') + sound.assetPath;
    console.info(`[audio] Preloading sound '${sound.assetId}', from file '${assetPath}'...`);

    try {
      if (this._audioType === 'native') {
        this._preloadedSounds[sound.assetId] = sound;
        await NativeAudio.preload({
          audioChannelNum: 1,
          isUrl: false,
          ...sound,
          assetPath,
        });
      } else {
        this._htmlAudioCache[sound.assetId] = new Audio(assetPath);
        this._preloadedSounds[sound.assetId] = sound;
      }

      // Add to map
      console.info(`[audio] Preloading sound '${sound.assetId}' [OK]`);
    } catch (err) {
      const errorMsg = err?.message || err;
      if (errorMsg === 'Audio Asset already exists') {
        console.info(`[audio] Preloading sound '${sound.assetId}': Skipped (${errorMsg})`);
      } else {
        this._preloadedSounds[sound.assetId] = null;
        console.error(`[audio] Unable to preload sound '${sound.assetId}', from file '${assetPath}': ${errorMsg}\n${err?.originalStack || ''}`);
        throw err;
      }
    }
  }

  async play(assetId: string, opts?: { vibrate?: boolean; vibration?: number | number[] }) {
    // Make sure provider is ready
    if (!this.started) await this.ready();

    const sound = this._preloadedSounds[assetId];
    if (!sound) {
      throw Error(`Unable to find sound '${assetId}'. Please call preload() before playing a sound.`);
    }

    // If silent: skip
    if (this._audioMode === AudioManagement.AudioMode.SILENT) return;

    const promises: Promise<any>[] = [];

    // Normal mode = can play the sound
    if (this._audioMode === AudioManagement.AudioMode.NORMAL) {
      // Use native audio
      if (this._audioType === 'native') {
        promises.push(
          NativeAudio.play({
            assetId: sound.assetId,
            time: sound.time || 0,
          })
        );
      }
      // Or HTML 5 audio
      else {
        const audio = this._htmlAudioCache[assetId] || new Audio(sound.assetPath);
        promises.push(audio.play());
      }
    }

    // Vibrate mode: by default make vibration is enable
    else if (isNil(sound.vibration) && isNil(opts && opts.vibrate)) {
      opts = opts || {};
      opts.vibrate = true;
    }

    // Do vibration (if sound as default vibration, or if user ask for it)
    if (toBoolean(opts?.vibrate, !!sound.vibration)) {
      promises.push(this.vibrate((opts && opts.vibration) || sound.vibration));
    }

    return (promises.length === 1 ? promises[0] : Promise.all(promises)).catch((err) => {
      console.error(`[audio] Error while playing audio sound '${assetId}': ${(err && err.message) || err}`, err);
    });
  }

  async unload(assetId: string) {
    if (isNil(this._audioType)) await this.ready();

    // Remove from map
    this._preloadedSounds[assetId] = undefined;

    // Native unload
    if (this._audioType === 'native') {
      try {
        await NativeAudio.unload({ assetId });
      } catch (err) {
        console.error(`[audio] Unable to unload audio '${assetId}': ${(err && err.message) || err}`, err);
      }
    }
    // HTML5 audio unload
    else {
      this._htmlAudioCache[assetId].remove();
      this._htmlAudioCache[assetId] = undefined;
    }
  }

  /**
   *
   * @param duration Duration of the vibration in milliseconds.
   */
  async vibrate(duration?: number | number[]) {
    if (!this.started) await this.ready();
    if (Array.isArray(duration)) {
      if (this._debug) console.debug('[audio] Vibrate: ' + JSON.stringify(duration));
      for (let i = 0; i < duration.length; i++) {
        const time = duration[i];
        const silent = i % 2 !== 0;
        if (silent) {
          if (this._debug) console.debug('[audio] silent during ' + time);
          await sleep(time);
        } else {
          if (this._debug) console.debug('[audio] vibrate during ' + time);
          await Promise.all([Haptics.vibrate({ duration: time }), sleep(time)]);
        }
      }
    } else {
      if (this._debug) console.debug('[audio] vibrate during ' + duration);
      await Haptics.vibrate({ duration });
    }
  }

  async setAudioMode(mode: AudioManagement.AudioMode): Promise<void> {
    if (!this._enableAudioManagment) {
      console.debug(`[audio] Cannot change audio mode to {${audioModeToString(mode)}} - skipping`);
      return;
    }

    try {
      console.info(`[audio] Changing audio mode to {${audioModeToString(mode)}}`);
      await this.audioManagement.setAudioMode(mode);
      this._audioMode = mode;
    } catch (err) {
      console.error(`[audio] Error while changing audio mode to {${audioModeToString(mode)}}: ${err?.message || err}`, err);
      // Continue
    }
  }

  /* -- protected methods  -- */

  protected async readAudioMode() {
    let value = await this.audioManagement.getAudioMode();
    value = {
      ...DEFAULT_AUDIO_MODE_RETURN,
      ...value,
    };
    console.debug(`[audio] Detected audio mode {label: '${value.label}', audioMode: ${value.audioMode}}`);
    this._audioMode = value.audioMode;
  }
}
