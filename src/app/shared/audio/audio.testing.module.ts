import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AudioTestingPage } from './audio.testing';

const routes: Routes = [
  {
    path: 'audio',
    pathMatch: 'full',
    component: AudioTestingPage,
  },
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [AudioTestingPage],
  exports: [RouterModule, AudioTestingPage],
})
export class AudioTestingModule {}
