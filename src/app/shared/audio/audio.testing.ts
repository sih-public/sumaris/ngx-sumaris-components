import { Component } from '@angular/core';
import { AudioProvider } from './audio';

@Component({
  selector: 'audio-testing',
  templateUrl: 'audio.testing.html',
})
export class AudioTestingPage {
  constructor(private audio: AudioProvider) {}

  playBeepConfirm() {
    return this.audio.playBeepConfirm();
  }
  playBeepError() {
    return this.audio.playBeepError();
  }
  playStartupSound() {
    return this.audio.playStartupSound();
  }
  playBeepNotification() {
    return this.audio.playBeepNotification();
  }

  vibrate(duration?: number | number[]) {
    return this.audio.vibrate(duration || [1000, 500, 1000, 500, 1000]);
  }
}
