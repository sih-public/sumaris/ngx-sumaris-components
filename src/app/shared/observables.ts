import { Observable, timer } from 'rxjs';
import { filter, first, map, switchMap, takeUntil } from 'rxjs/operators';
import { isNotNil } from './functions';
import { Predicate } from '@angular/core';

export declare interface FirstOptions {
  timeout?: number;
  stop?: Observable<any>;
  stopError?: boolean | string | Error;
}

export declare interface WaitForOptions extends FirstOptions {
  dueTime?: number;
  checkPeriod?: number;
}

export function filterNotNil<T = any>(obs: Observable<T>): Observable<T> {
  return obs.pipe(filter(isNotNil));
}

export function filterTrue(obs: Observable<boolean>): Observable<boolean> {
  return obs.pipe(filter((v) => v === true));
}
export function filterFalse(obs: Observable<boolean>, opts?: FirstOptions): Observable<boolean> {
  return obs.pipe(filter((v) => v === false));
}

function decorateWithTakeUntil<T = any>(obs: Observable<T>, opts?: FirstOptions): Observable<T> {
  // Set take until notifier
  let takeUntil$: Observable<any> = opts?.stop || (opts?.timeout && timer(opts.timeout));

  if (!takeUntil$) return obs; // Skip

  // When stop, throw an error (useful when used with a toPromise())
  if (opts.stopError) {
    let error: Error;
    if (opts.stopError === true) {
      error = new Error(opts.timeout ? `Timeout ${opts.timeout}ms` : `stop`);
    } else if (typeof opts.stopError === 'string') {
      error = new Error(opts.stopError);
    } else {
      error = opts.stopError;
    }
    takeUntil$ = takeUntil$.pipe(
      map(() => {
        throw error;
      })
    );
  }

  return obs.pipe(takeUntil(takeUntil$));
}

export function firstNotNil<T = any>(obs: Observable<T>, opts?: FirstOptions): Observable<T> {
  return decorateWithTakeUntil(obs.pipe(first(isNotNil)), opts);
}
export function firstTrue<T = void>(obs: Observable<boolean>, opts?: FirstOptions, result?: T): Observable<T> {
  return decorateWithTakeUntil(
    obs.pipe(
      first((v) => v === true),
      map((_) => result) // Convert to T
    ),
    opts
  );
}
export function firstFalse<T = void>(obs: Observable<boolean>, opts?: FirstOptions, result?: T): Observable<T> {
  return decorateWithTakeUntil(
    obs.pipe(
      first((v) => v === false),
      map((_) => result as T) // Convert to void
    ),
    opts
  );
}
export function firstTruePromise(obs: Observable<boolean>, opts?: FirstOptions): Promise<void> {
  return firstTrue(obs, { stopError: true, ...opts }).toPromise();
}
export function firstFalsePromise(obs: Observable<boolean>, opts?: FirstOptions): Promise<void> {
  return firstFalse(obs, { stopError: true, ...opts }).toPromise();
}
export function firstNotNilPromise<T = any>(obs: Observable<T>, opts?: FirstOptions): Promise<T> {
  return firstNotNil(obs, { stopError: true, ...opts }).toPromise();
}
export function chainPromises<T = any>(defers: (() => Promise<any>)[]): Promise<T[]> {
  return (defers || []).reduce((previous: Promise<any> | null, defer) => {
    // First job
    if (!previous) {
      return (
        defer()
          // Init the final result array, with the first result
          .then((jobRes) => [jobRes])
      );
    }
    // Other jobs
    return previous.then((finalResult) =>
      defer()
        // Add job result to final result array
        .then((jobRes) => finalResult.concat(jobRes))
    );
  }, null);
}

/**
 * Wait form a predicate return true. This need to implement AppFormUtils.waitWhilePending(), AppFormUtils.waitIdle()
 *
 * @param predicate
 * @param opts
 */
export async function waitFor(predicate: Predicate<void>, opts?: WaitForOptions): Promise<void> {
  if (predicate()) return Promise.resolve();

  const period = (opts && opts.checkPeriod) || 300;
  const dueTime = (opts && opts.dueTime) || period;
  const wait$: Observable<boolean> = timer(dueTime, period).pipe(
    // For DEBUG :
    //tap(() => console.debug("Waiting form idle...", form)),
    filter((_) => predicate()),
    map((_) => true)
  );

  await firstNotNilPromise(wait$, opts);
}

export function waitForTrue(observable: Observable<boolean>, opts?: WaitForOptions): Promise<void> {
  opts = { stopError: true, ...opts };
  // dueTime (without timeout)
  if (opts && opts.dueTime > 0) {
    observable = timer(opts.dueTime).pipe(switchMap(() => observable));
  }
  return firstTrue(observable, opts).toPromise();
}

export function waitForFalse(observable: Observable<boolean>, opts?: WaitForOptions): Promise<void> {
  return waitForTrue(
    // Inverse the logic
    observable.pipe(map((v) => v === false)),
    opts
  );
}
