import { KeysEnum, KeyValueType } from './types';
import { DEFAULT_JOIN_PROPERTIES_SEPARATOR } from './constants';
import { RegExpUtils } from './regexps';

export function isNil<T>(obj: T | null | undefined): boolean {
  return obj === undefined || obj === null;
}
export function isNilOrBlank<T>(obj: T | null | undefined): boolean {
  return obj === undefined || obj === null || (typeof obj === 'string' && obj.trim() === '');
}
export function isNotNil<T>(obj: T | null | undefined): boolean {
  return obj !== undefined && obj !== null;
}
export function isNotNilOrBlank<T>(obj: T | null | undefined): boolean {
  return obj !== undefined && obj !== null && (typeof obj !== 'string' || obj.trim() !== '');
}
export function isNotNilOrNaN<T>(obj: T | null | undefined): boolean {
  return obj !== undefined && obj !== null && (typeof obj !== 'number' || !isNaN(obj));
}
export function isNilOrNaN<T>(obj: T | null | undefined): boolean {
  return obj === undefined || obj === null || (typeof obj === 'number' && isNaN(obj));
}
export function isNotEmptyArray<T>(obj: T[] | null | undefined): boolean {
  return obj !== undefined && obj !== null && obj.length > 0;
}
export function firstArrayValue<T>(obj: T[] | null | undefined): T | undefined {
  return isNotEmptyArray(obj) ? obj[0] : undefined;
}
export function lastArrayValue<T>(obj: T[] | null | undefined): T | undefined {
  return isNotEmptyArray(obj) ? obj[obj.length - 1] : undefined;
}
export function isEmptyArray<T>(obj: T[] | null | undefined): boolean {
  return obj === undefined || obj === null || !obj.length;
}
export function arrayResize<T>(array: T[] | null | undefined, size: number, defaultValue?: T): T[] {
  if (size < 0) throw new Error("Argument 'size' must be a positive number");
  if (!array) return newArray(size, defaultValue);

  // Reduce size
  if (array.length > size) {
    return array.slice(0, size);
  }
  // Increase size
  else if (array.length < size) {
    return [...array, ...newArray(size - array.length, defaultValue)];
  }

  return array;
}
export function newArray<T>(size: number, defaultValue?: T): T[] {
  if (size < 0) throw new Error("Argument 'size' must be a positive number");
  return new Array(size).fill(defaultValue);
}
export function isNotNilBoolean(obj: any | null | undefined): obj is boolean {
  return (obj !== undefined && obj !== null && typeof obj === 'boolean') || false;
}
export function isNotNilString(obj: any | null | undefined): obj is string {
  return (obj !== undefined && obj !== null && typeof obj === 'string') || false;
}
export function isNotNilObject(obj: any | null | undefined): obj is object {
  return (obj !== undefined && obj !== null && typeof obj === 'object') || false;
}
export function isBlankString(obj: any | null | undefined): obj is string {
  return typeof obj === 'string' && obj.trim().length === 0;
}
export function notNilOrDefault<T>(obj: T | null | undefined, defaultValue: T): T {
  return obj !== undefined && obj !== null ? obj : defaultValue;
}
export function arraySize<T>(obj: T[] | null | undefined): number {
  return (isNotEmptyArray(obj) && obj.length) || 0;
}

/**
 * Remove duplicated value, in array. Will compute a unique key (from given properties, or given function).
 * If propertiesOrKeyFn is nil or undefined, will use all object properties
 *
 * @param values
 * @param propertiesOrKeyFn
 */
export function arrayDistinct<T = any>(values: T[], propertiesOrKeyFn?: string | string[] | ((obj: T) => string)): T[] {
  if (isEmptyArray(values)) return values;
  const properties: string[] = (!propertiesOrKeyFn && Object.keys(values[0])) || (Array.isArray(propertiesOrKeyFn) && propertiesOrKeyFn);
  const getKey: (obj: T) => string =
    (typeof propertiesOrKeyFn === 'function' && propertiesOrKeyFn) ||
    (typeof propertiesOrKeyFn === 'string' && ((obj) => getPropertyByPathAsString(obj, propertiesOrKeyFn))) ||
    (isEmptyArray(properties) && ((obj) => obj?.toString())) ||
    ((obj) => joinPropertiesPath(obj, properties, '|'));
  const existingIds = new Set<string>();
  return values.reduce((res, item) => {
    const uniqueKey = getKey(item);
    if (existingIds.has(uniqueKey)) return res; // Skip if duplicate
    existingIds.add(uniqueKey);
    return res.concat(item);
  }, []);
}
export function nullIfUndefined<T>(obj: T | null | undefined): T | null {
  return obj === undefined ? null : obj;
}
export function nullIfNilOrBlank<T>(obj: T | null | undefined): T | null {
  return isNilOrBlank(obj) ? null : obj;
}
export function undefinedIfNull<T>(obj: T | null | undefined): T | undefined {
  return obj === null ? undefined : obj;
}
export function trimEmptyToNull<T>(str: string | null | undefined): string | null {
  const value = (str && str.trim()) || undefined;
  return (value && value.length && value) || null;
}
export function toBoolean(obj: boolean | null | undefined | string, defaultValue?: boolean): boolean {
  return obj !== undefined && obj !== null ? (obj !== 'false' ? !!obj : false) : defaultValue;
}
export function toNumber(obj: number | null | undefined | string, defaultValue?: number): number {
  return isNotNilOrNaN(obj) ? +obj : defaultValue;
}
export function toFloat(obj: string | null | undefined, defaultValue?: number): number | null {
  return obj !== undefined && obj !== null ? parseFloat(obj) : defaultValue;
}
export function toInt(obj: string | null | undefined, defaultValue?: number): number | null {
  return obj !== undefined && obj !== null ? parseInt(obj, 0) : defaultValue;
}
export function toNotNil<T = any>(obj: T, defaultValue?: T): any | null {
  return obj !== undefined && obj !== null ? obj : defaultValue;
}
export function removeDuplicatesFromArray<T>(obj: T[] | null | undefined, property?: string): T[] | null | undefined {
  if (isEmptyArray(obj)) return obj;
  if (property) {
    return obj.filter((item, i, array) => array.findIndex((t) => t && item && t[property] === item[property]) === i);
  }
  return obj.filter((item, i, array) => array.findIndex((t) => t && item && t === item) === i);
}

export function startsWithUpperCase(input: string, search: string): boolean {
  return input?.toUpperCase().startsWith(search) || false;
}

/**
 * @deprecated Use Regexps.matchUpperCase instead
 */
export const matchUpperCase = RegExpUtils.matchUpperCase;

/**
 * Remove trailing slash if any. Examples :
 * - '/test/' -> '/test'
 * - '/' -> undefined
 */
export function noTrailingSlash(path: string): string {
  if (!path || path.trim() === '/') return undefined;
  if (path.trim().lastIndexOf('/') === path.length - 1) return path.substring(0, path.length - 1);
  return path;
}

export function replaceAll(value: string, searchString: any, replacement): string | undefined {
  while (value && value.indexOf(searchString) !== -1) {
    value = value.replace(searchString, replacement);
  }
  return value;
}

export function escapeRegExp(value: string) {
  // eslint-disable-next-line no-useless-escape
  return value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

export function removeEnd(value: string, end: string): string {
  return value?.endsWith(end || '') ? value.substr(0, value.length - end.length) : value;
}

/**
 * Replace case change by an underscore (.e.g 'myString' becomes 'my_string')
 *
 * @param value
 */
export function changeCaseToUnderscore(value: string): string {
  if (isNilOrBlank(value)) return value;
  return value.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
}

/**
 * Replace underscore by a change case (.e.g 'MY_STRING' becomes 'MyString')
 *
 * @param value
 */
export function underscoreToChangeCase(value: string): string {
  if (isNilOrBlank(value)) return value;
  return value.toLowerCase().split('_').map(capitalizeFirstLetter).join();
}

export function removeDiacritics(text: string): string {
  return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

export function suggestFromStringArray(values: string[], searchText: any, opts?: { anySearch?: boolean }): string[] {
  searchText = (typeof searchText === 'string' && searchText !== '*' && searchText) || undefined;
  let cleanSearchText = RegExpUtils.cleanSearchText(searchText as string);
  if (!cleanSearchText?.length) return values;

  // If wildcard, search using regexp
  if (opts?.anySearch || RegExpUtils.hasWildcardCharacter(cleanSearchText)) {
    const regexp = RegExpUtils.compileSearchRegexp(cleanSearchText, 'gi');
    return values.filter((v) => RegExpUtils.match(v, regexp));
  } else {
    // If no wildcard, search using startsWith
    cleanSearchText = cleanSearchText.toUpperCase();
    return values.filter((v) => startsWithUpperCase(v, cleanSearchText));
  }
}

export function joinPropertiesPath<T = any>(obj: T, properties: string[], separator?: string): string | undefined {
  if (!obj) throw new Error('Could not display an undefined entity.');
  if (isEmptyArray(properties)) throw new Error("Missing or empty 'properties' argument");
  return properties
    .map((path) => getPropertyByPath(obj, path))
    .filter(isNotNilOrBlank)
    .join(separator || DEFAULT_JOIN_PROPERTIES_SEPARATOR);
}

export function joinProperties<T = any, K extends keyof T = any>(obj: T, keys: K[], separator?: string): string | undefined {
  if (!obj) throw new Error('Could not display an undefined entity.');
  return keys
    .map((key) => getProperty(obj, key))
    .filter(isNotNilOrBlank)
    .join(separator || DEFAULT_JOIN_PROPERTIES_SEPARATOR);
}

export function propertyPathComparator<T = any>(path: string | string[]): (a: T, b: T) => number {
  return (a: T, b: T) => {
    const valueA = getPropertyByPath(a, path);
    const valueB = getPropertyByPath(b, path);
    return valueA === valueB ? 0 : valueA > valueB ? 1 : -1;
  };
}

export function propertyComparator<T = any, K extends keyof T = any>(key: K, defaultValue?: any): (a: T, b: T) => number {
  return (a: T, b: T) => {
    const valueA = a[key] !== undefined ? a[key] : defaultValue;
    const valueB = b[key] ? b[key] : defaultValue;
    return valueA === valueB ? 0 : valueA > valueB ? 1 : -1;
  };
}

export function propertiesPathComparator<T = any>(keys: string[], defaultValues?: any[]): (a: T, b: T) => number {
  if (!keys || !keys.length || (defaultValues && keys.length > defaultValues.length)) {
    throw new Error("Invalid arguments: missing 'keys' or array 'defaultValues' has a bad length");
  }
  return (a: T, b: T) =>
    keys
      .map((key, index) => {
        const valueA = getPropertyByPath(a, key, defaultValues && defaultValues[index]);
        const valueB = getPropertyByPath(b, key, defaultValues && defaultValues[index]);
        return valueA === valueB ? 0 : valueA > valueB ? 1 : -1;
      })
      // Stop if not equals, otherwise continue with the next key
      .find((res) => res !== 0) || 0;
}

export function sort<T>(array: T[], attribute?: string, opts?: Intl.CollatorOptions): T[] {
  if (isEmptyArray(array)) return array;
  const compareFn: (a: any, b: any) => number = opts ? new Intl.Collator(undefined, opts).compare : (v1, v2) => (v1 === v2 ? 0 : v1 > v2 ? 1 : -1);
  return array
    .slice() // copy
    .sort(attribute ? (a, b) => compareFn(a[attribute], b[attribute]) : compareFn);
}
const INTEGER_REGEXP = /^[-]?\d+$/;
export function isInt(value: string): boolean {
  return isNotNil(value) && INTEGER_REGEXP.test(value);
}
const NUMBER_REGEXP = /^[-]?\d+(\.\d+)?$/;
export function isNumber(value: string): boolean {
  return isNotNil(value) && NUMBER_REGEXP.test(value);
}

const NUMBER_RANGE_REGEXP = /^(\d+-\d+)|([><=]*\d+)$/;
export function isNumberRange(value: string): boolean {
  return isNotNil(value) && NUMBER_RANGE_REGEXP.test(value);
}

export function getPropertyByPath(obj: any, path: string | Array<string | number>, defaultValue?: any): any {
  if (isNil(obj)) return defaultValue;
  // Path is an array: convert path to string
  if (Array.isArray(path)) {
    return getPropertyByPath(obj, path.join('.'), defaultValue);
  }
  if (isNilOrBlank(path)) return obj;
  if (path.indexOf('[') !== -1) {
    path = path.replace(/\[([^\[\]]*)\]/, '.$1.');
  }
  const i = path.indexOf('.');
  // Simple property path
  if (i === -1) {
    const res = obj[path];
    return isNotNil(res) || isNil(defaultValue) ? res : defaultValue;
  }
  // Complex property path
  const optional = path.charAt(i - 1) === '?';
  const key = path.substring(0, optional ? i - 1 : i);
  if (isNil(obj[key])) return defaultValue;
  if (typeof obj[key] === 'object') {
    // Object or Array
    // Recursive call
    return getPropertyByPath(obj[key], path.substring(i + 1));
  }
  if (optional) return defaultValue;
  throw new Error(`Invalid property path: '${key}' is not an valid object.`);
}

export function getProperty<T = any, K extends keyof T = any>(obj: T, key: K): T[K] {
  if (isNil(obj)) return undefined;
  return obj[key]; // Inferred type is T[K]
}

export function getPropertyByPathAsString(obj: any, path: string | string[]): string | undefined {
  const res = getPropertyByPath(obj, path);
  return typeof res === 'string' ? res : '' + res;
}

/**
 * Update an object's property, using a path (like '<property_1>.<sub_property_1>')
 * @param obj
 * @param path
 * @param value
 * @return the given obj
 */
export function setPropertyByPath<T = any>(obj: T, path: string | string[], value: any): T {
  // Path is an array: convert path to string
  if (Array.isArray(path)) {
    return setPropertyByPath(obj, path.join('.'), value);
  }
  if (isNil(obj)) return obj; // Skip
  const i = path.indexOf('.');
  // Simple property path
  if (i === -1) {
    obj[path] = value;
    return obj;
  }
  const key = path.substring(0, i);
  if (isNil(obj[key])) return undefined;
  if (obj[key] && typeof obj[key] === 'object') {
    return setPropertyByPath(obj[key], path.substring(i + 1), value);
  }
  throw new Error(`Invalid form path: '${key}' is not an valid object.`);
}

export function sleep(ms: number): Promise<void> {
  if (ms <= 0) return Promise.resolve();
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function isPromise<T>(value: T | Promise<T>): value is Promise<T> {
  return typeof value?.['then'] === 'function' && typeof value?.['catch'] === 'function';
}

/**
 * Rounds a numeric value to two decimal places.
 *
 * @param {number | undefined | null} value - The value to be rounded. Can be a number, undefined, or null.
 * @return {number} The rounded value with two decimal places if the input value is a valid number; otherwise, returns the input value.
 */
export function round(value: number | undefined | null): number {
  if (isNotNilOrNaN(value)) {
    return Math.round((value + Number.EPSILON) * 100) / 100;
  }
  return value;
}

export function equalsOrNil(value1: any, value2: any) {
  return value1 === value2 || (isNil(value1) && isNil(value2));
}

/**
 * remove elements from an array
 *
 * @return the removed elements
 * @param array the array on which elements will be removed
 * @param predicate the selection predicate
 */
export function removeAll<T>(array: T[], predicate: (value: T) => boolean): T[] {
  let nbRemove = 0;
  return array
    .slice()
    .map((value, index) => {
      if (predicate(value)) {
        nbRemove++; // glitch counter to avoid index gap
        return array.splice(index - nbRemove + 1, 1)[0];
      }
      return null;
    })
    .filter(isNotNil);
}

/**
 * remove the first element from an array
 *
 * @return the removed element
 * @param array the array on which the element will be removed
 * @param predicate the selection predicate
 */
export function remove<T>(array: T[], predicate: (value: T) => boolean): T {
  return array.slice().find((value, index) => {
    if (predicate(value)) {
      array.splice(index, 1);
      return true;
    }
    return false;
  });
}

export function capitalizeFirstLetter(value: string) {
  if (!value || value.length === 0) return value;
  return value.charAt(0).toUpperCase() + value.substring(1);
}
export function uncapitalizeFirstLetter(value: string) {
  if (!value || value.length === 0) return value;
  return value.charAt(0).toLowerCase() + value.substring(1);
}
export function booleanToString(value: any, opts: { yesLabel: string; noLabel: string } = { yesLabel: 'COMMON.YES', noLabel: 'COMMON.NO' }) {
  if (isNil(value)) return undefined;
  return value === 1 || value === true || value === 'true' ? opts.yesLabel : opts.noLabel;
}

export function splitByProperty<T, M extends KeyValueType<T> = KeyValueType<T>>(array: T[] | readonly T[], propertyName: keyof T | string): M {
  return (array || []).reduce((res, value) => {
    const key = (value as unknown as any)?.[propertyName];
    if (isNotNil(key)) {
      res[key] = value;
    }
    return res;
  }, {}) as M;
}

export function splitById<T, M extends KeyValueType<T> = KeyValueType<T>>(array: T[] | readonly T[]): M {
  return (array || []).reduce(
    (res, value) => {
      const key = value?.['id'];
      if (isNotNil(key)) {
        res[key] = value;
      }
      return res;
    },
    <M>{}
  );
}

/**
 * Split an array, into a map of array, group by property
 */
export function collectByProperty<T>(values: T[], propertyName: keyof T): KeyValueType<T[]> {
  return (values || []).reduce(
    (res, item) => {
      const key = item && item[propertyName];
      if (typeof key === 'number' || typeof key === 'string') {
        res[key] = res[key] || [];
        res[key].push(item);
      }
      return res;
    },
    <KeyValueType<T[]>>{}
  );
}

/**
 * Determines if two objects or two values are equivalent.
 *
 * Two objects or values are considered equivalent if at least one of the following is true:
 *
 * * Both objects or values pass `===` comparison.
 * * Both objects or values are of the same type and all of their properties are equal by
 *   comparing them with `equals`.
 *
 * @param o1 Object or value to compare.
 * @param o2 Object or value to compare.
 * @returns true if arguments are equal.
 */
export function equals(o1: any, o2: any): boolean {
  if (o1 === o2) return true;
  if (o1 === null || o2 === null) return false;
  if (o1 !== o1 && o2 !== o2) return true; // NaN === NaN
  const t1 = typeof o1;
  const t2 = typeof o2;
  let length: number;
  let key: any;
  let keySet: any;
  if (t1 === t2 && t1 === 'object') {
    if (Array.isArray(o1)) {
      if (!Array.isArray(o2)) return false;
      if ((length = o1.length) === o2.length) {
        for (key = 0; key < length; key++) {
          if (!equals(o1[key], o2[key])) return false;
        }
        return true;
      }
    } else {
      if (Array.isArray(o2)) {
        return false;
      }
      keySet = Object.create(null);
      // eslint-disable-next-line guard-for-in
      for (key in o1) {
        if (!equals(o1[key], o2[key])) {
          return false;
        }
        keySet[key] = true;
      }
      for (key in o2) {
        if (!(key in keySet) && typeof o2[key] !== 'undefined') {
          return false;
        }
      }
      return true;
    }
  }
  return false;
}

// @dynamic
export class Beans {
  /**
   * Copy a source object, by including only properties of the given dataType.
   * IMPORTANT: extra properties that are NOT in the targetClass are NOT copied.
   *
   * @param source The source object to copy
   * @param dataType the class to use as target class
   * @param keys The keys to copy. If empty, will copy only NOT optional properties from the dataType
   */
  static copy<T>(source: T, dataType: new () => T, keys?: KeysEnum<T>): T {
    if (isNil(source)) return source;
    const target = new dataType();
    Object.keys(keys || target).forEach((key) => {
      target[key] = source[key];
    });
    return target;
  }

  /**
   * Says if an object all all properties to nil
   */
  static isEmpty<T>(
    data: T,
    keys?: KeysEnum<T> | string[],
    opts?: {
      blankStringLikeEmpty?: boolean;
    }
  ): boolean {
    return (
      isNil(data) ||
      (Array.isArray(keys) ? keys : Object.keys(keys || data))
        // Find index of the first NOT nil value
        .findIndex((key) => {
          const value = data[key];
          if (value && typeof value === 'object') return !Beans.isEmpty(value, null, opts); // Loop
          if (opts && opts.blankStringLikeEmpty === true && typeof value === 'string') return isNotNilOrBlank(value);
          return isNotNil(value);
        }) === -1
    );
  }

  static collectById<T>(array: T[]): KeyValueType<T[]> {
    return collectByProperty(array, 'id' as keyof T);
  }

  static collectByProperty = collectByProperty;
}

export function findParentWithClass(elem: HTMLElement, className: string): HTMLElement {
  if (!className) return undefined;
  let currentElement = elem?.parentElement;
  while (currentElement) {
    if (currentElement.classList.contains(className)) return currentElement;
    currentElement = currentElement.parentElement;
  }
  return undefined;
}

export function intersectArrays<T = any>(values: T[][]): T[] {
  if (isEmptyArray(values)) return [];

  // Utilise la méthode reduce pour obtenir l'intersection des tableaux
  return values.reduce((acc, curr) => acc.filter((x) => curr.includes(x)), values[0].slice());
}

export function noHtml(value: string): string {
  if (value && typeof value === 'string') {
    // Use regular expression to remove all HTML tags
    return value.replace(/<[^>]*>.*?<\/[^>]*>|<[^>]+>/g, '');
  } else {
    return value;
  }
}
