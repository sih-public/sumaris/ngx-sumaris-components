import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { firstValueFrom, Observable, timeout } from 'rxjs';

export class HttpUtils {
  static async getText(
    http: HttpClient,
    uri: string,
    opts?: {
      nocache?: boolean;
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      params?:
        | HttpParams
        | {
            [param: string]: string | string[];
          };
      reportProgress?: boolean;
      responseType?: 'text';
      withCredentials?: boolean;
    }
  ): Promise<string> {
    // Add headers
    opts = opts || { responseType: 'text' };

    // Force no cache
    if (opts.nocache === true) {
      opts.headers = opts.headers instanceof HttpHeaders ? opts.headers : new HttpHeaders(opts.headers);
      opts.headers.append('Cache-Control', 'no-cache').append('Pragma', 'no-cache');
    }

    // Use web http client
    try {
      return await firstValueFrom(http.get(uri, { ...opts, responseType: 'text' }));
    } catch (err) {
      if (err && err.message) {
        console.error(`[network] Error on get request ${uri}: ${err.message}`, err);
      } else {
        console.error(`[network] Error on get request ${uri}: ${err && err.statusText}`, err);
      }
      throw { code: err.status, message: 'ERROR.UNKNOWN_NETWORK_ERROR' };
    }
  }

  static async getJson<T>(
    http: HttpClient,
    uri: string,
    opts?: {
      nocache?: boolean;
      timeout?: number;
      headers?:
        | HttpHeaders
        | {
            [header: string]: string | string[];
          };
      params?:
        | HttpParams
        | {
            [param: string]: string | string[];
          };
      reportProgress?: boolean;
      responseType?: 'json';
      withCredentials?: boolean;
    }
  ): Promise<T> {
    // Add headers
    opts = opts || {};

    // Force no cache
    if (opts.nocache === true) {
      opts.headers = opts.headers instanceof HttpHeaders ? opts.headers : new HttpHeaders(opts.headers);
      opts.headers.append('Cache-Control', 'no-cache').append('Pragma', 'no-cache');
    }

    // Use web http client
    try {
      let result: Observable<T>;
      if (opts.timeout) {
        result = http.get<T>(uri, { ...opts, responseType: 'json' }).pipe(timeout(opts?.timeout));
      } else {
        result = http.get<T>(uri, { ...opts, responseType: 'json' });
      }
      return await firstValueFrom(result);
    } catch (err) {
      if (err && err.message) {
        console.error(`[network] Error on get request ${uri}: ${err.message}`, err);
      } else {
        console.error(`[network] Error on get request ${uri}: ${err && err.statusText}`, err);
      }
      throw err;
    }
  }
}
