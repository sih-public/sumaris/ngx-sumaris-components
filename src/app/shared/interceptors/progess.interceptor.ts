import { catchError, tap } from 'rxjs/operators';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APP_PROGRESS_BAR_SERVICE, IProgressBarService } from '../services/progress-bar.service';
import { Inject } from '@angular/core';

export class ProgressInterceptor implements HttpInterceptor {
  constructor(@Inject(APP_PROGRESS_BAR_SERVICE) private progressBarService: IProgressBarService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.progressBarService.increase();
    return next.handle(req).pipe(
      tap((event) => {
        if (event instanceof HttpResponse) {
          this.progressBarService.decrease();
        }
      }),
      catchError((err, event) => {
        this.progressBarService.decrease();
        throw err;
      })
    );
  }
}
