import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedTestsPage, TestingPage } from '../testing/tests.page';
import { SharedMaterialModule } from './material.module';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { DateTimeTestPage } from './datetime/testing/mat-date-time.test';
import { DateTestPage } from './datetime/testing/mat-date.test';
import { SharedPipesModule } from '../pipes/pipes.module';
import { ObservableTestPage } from '../testing/observable.test';
import { DateShortTestPage } from './datetime/testing/mat-dateshort.test';
import { AutocompleteTestPage } from './autocomplete/testing/autocomplete.test';
import { LatLongTestPage } from './latlong/testing/latlong.test';
import { SwipeTestPage } from './swipe/testing/swipe.test';
import { ChipsTestPage } from './chips/testing/chips.test';
import { MatBadgeTestPage } from './badge/badge.test';
import { SharedBadgeModule } from './badge/badge.module';
import { DurationTestPage } from './duration/testing/mat-duration.test';
import { MatCommonTestPage } from './testing/common.test';
import { BooleanTestPage } from './boolean/testing/boolean.test.page';
import { TextFormTestingPage } from './text/testing/text-form.testing';
import { MaskitoTestPage } from '../testing/maskito.test';
import { MaskitoDirective } from '@maskito/angular';
import { FormFieldCustomControlExample } from './test/test-component';
import { SharedMatLatLongModule } from './latlong/material.latlong.module';

export const SHARED_MATERIAL_TESTING_PAGES: TestingPage[] = [
  { label: 'Shared Material components', divider: true },
  { label: 'Common field', page: '/testing/shared/common' },
  { label: 'Date/Time field', page: '/testing/shared/datetime' },
  { label: 'Date field', page: '/testing/shared/date' },
  { label: 'Date short field', page: '/testing/shared/dateshort' },
  { label: 'Duration field', page: '/testing/shared/duration' },
  { label: 'Autocomplete field', page: '/testing/shared/autocomplete' },
  { label: 'Boolean field', page: '/testing/shared/boolean' },
  { label: 'Lat/Long field', page: '/testing/shared/latlong' },
  { label: 'Custom field example', page: '/testing/shared/custom' },
  //{ label: 'Numeric pad component', page: '/testing/shared/numpad' },
  { label: 'Text form', page: '/testing/shared/text-form' },

  { label: 'Swipe component', page: '/testing/shared/swipe' },
  { label: 'Chips component', page: '/testing/shared/chips' },

  { label: 'Shared directives', divider: true },
  { label: 'Badge directive', page: '/testing/shared/badge' },

  { label: 'Shared utils', divider: true },
  { label: 'Observable', page: '/testing/shared/observable' },
  { label: 'Maskito', page: '/testing/shared/maskito' },
];

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SharedTestsPage,
  },
  {
    path: 'common',
    pathMatch: 'full',
    component: MatCommonTestPage,
  },
  {
    path: 'datetime',
    pathMatch: 'full',
    component: DateTimeTestPage,
  },
  {
    path: 'date',
    pathMatch: 'full',
    component: DateTestPage,
  },
  {
    path: 'dateshort',
    pathMatch: 'full',
    component: DateShortTestPage,
  },
  {
    path: 'duration',
    pathMatch: 'full',
    component: DurationTestPage,
  },
  {
    path: 'autocomplete',
    pathMatch: 'full',
    component: AutocompleteTestPage,
  },
  {
    path: 'boolean',
    pathMatch: 'full',
    component: BooleanTestPage,
  },
  {
    path: 'latlong',
    pathMatch: 'full',
    component: LatLongTestPage,
  },
  {
    path: 'custom',
    pathMatch: 'full',
    component: FormFieldCustomControlExample,
  },
  // {
  //   path: 'numpad',
  //   pathMatch: 'full',
  //   component: NumpadTestPage
  // },
  {
    path: 'text-form',
    pathMatch: 'full',
    component: TextFormTestingPage,
  },
  {
    path: 'swipe',
    pathMatch: 'full',
    component: SwipeTestPage,
  },
  {
    path: 'chips',
    pathMatch: 'full',
    component: ChipsTestPage,
  },
  {
    path: 'badge',
    pathMatch: 'full',
    component: MatBadgeTestPage,
  },
  {
    path: 'observable',
    pathMatch: 'full',
    component: ObservableTestPage,
  },
  {
    path: 'maskito',
    pathMatch: 'full',
    component: MaskitoTestPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedPipesModule,
    SharedBadgeModule,
    FormsModule,
    MaskitoDirective,
    SharedMatLatLongModule,
  ],
  declarations: [
    MatCommonTestPage,
    DateTimeTestPage,
    DateTestPage,
    DateShortTestPage,
    DurationTestPage,
    AutocompleteTestPage,
    BooleanTestPage,
    LatLongTestPage,
    //NumpadTestPage,
    TextFormTestingPage,
    SwipeTestPage,
    ChipsTestPage,
    MatBadgeTestPage,
    ObservableTestPage,
    MaskitoTestPage,
  ],
  exports: [
    SharedMaterialModule,
    RouterModule,
    MatCommonTestPage,
    DateTimeTestPage,
    DateTestPage,
    DateShortTestPage,
    DurationTestPage,
    AutocompleteTestPage,
    BooleanTestPage,
    LatLongTestPage,
    //NumpadTestPage,
    TextFormTestingPage,
    SwipeTestPage,
    ChipsTestPage,
    MatBadgeTestPage,
    ObservableTestPage,
    MaskitoTestPage,
  ],
})
export class MaterialTestingModule {}
