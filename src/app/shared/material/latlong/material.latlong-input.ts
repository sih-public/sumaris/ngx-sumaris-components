import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NgControl, Validators } from '@angular/forms';
import { isNil, isNilOrBlank, isNotNil, isNotNilOrBlank } from '../../functions';
import {
  computeDecimalDegrees,
  computeDecimalPart,
  ILatLongData,
  LAT_LONG_PATTERN_MAX_DECIMALS,
  LatLongPattern,
  LatLongSign,
  LatLongSignValue,
  LatLongType,
  splitDegreesToDDArray,
  splitDegreesToDDMMArray,
  splitDegreesToDDMMSSArray,
} from './latlong.utils';
import { MaskitoOptions } from '@maskito/core';
import { MAT_FORM_FIELD, MatFormField, MatFormFieldControl } from '@angular/material/form-field';
import { Subject, Subscription } from 'rxjs';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';
import { maskitoNumberOptionsGenerator } from '@maskito/kit';

@Component({
  selector: 'mat-latlong-input',
  templateUrl: './material.latlong-input.html',
  styleUrl: './material.latlong-input.scss',
  providers: [{ provide: MatFormFieldControl, useExisting: MatLatLongFieldInput }],
  host: {
    '[class.label-floating]': 'shouldLabelFloat',
    '[id]': 'id',
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatLatLongFieldInput implements MatFormFieldControl<number>, ControlValueAccessor, OnInit, OnDestroy {
  static nextId = 0;
  @ViewChild('degrees', { read: ElementRef }) degreesInput: ElementRef;
  @ViewChild('minutes', { read: ElementRef }) minutesInput: ElementRef;
  @ViewChild('seconds', { read: ElementRef }) secondsInput: ElementRef;
  @ViewChild('sign', { read: ElementRef }) signInput: ElementRef;

  protected formGroup = inject(FormBuilder).group({
    degrees: ['', [Validators.required]],
    minutes: [''],
    seconds: [''],
    sign: [<LatLongSignValue>null],
  });
  stateChanges = new Subject<void>();
  focused = false;
  touched = false;
  controlType = 'mat-latlong-input';
  id = `mat-latlong-input-${MatLatLongFieldInput.nextId++}`;
  onChange = (_: any) => {};
  onTouched = () => {};

  get empty() {
    if (this.readonly) {
      return isNil(this.ngControl.value);
    } else {
      const {
        value: { degrees, minutes, seconds },
      } = this.formGroup;
      return !degrees && !minutes && !seconds;
    }
  }

  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input('aria-describedby') userAriaDescribedBy: string;
  placeholder: string; // Not used

  get required(): boolean {
    return this._required;
  }

  @Input()
  set required(value: BooleanInput) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  private _required = false;

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set disabled(value: BooleanInput) {
    this._disabled = coerceBooleanProperty(value);
    if (this._disabled) {
      this.formGroup.disable();
    } else {
      this.formGroup.enable();
    }
    this.stateChanges.next();
  }

  private _disabled = false;

  get value(): number | null {
    if (this.formGroup.valid) {
      const parts = this.formGroup.value;
      const degrees = this.safeParseFloat(parts.degrees);
      const minutes = this.safeParseFloat(parts.minutes);
      const seconds = this.safeParseFloat(parts.seconds);
      const sign = parts.sign;
      return this.formGroup.valid
        ? isNil(degrees) || this.pattern === 'DD'
          ? degrees
          : computeDecimalDegrees(degrees, minutes, seconds, sign, this.maxDecimals)
        : null;
    }
    return null;
  }

  @Input()
  set value(value: number | null) {
    const result = this.computeLatLongFormGroupValue(value);
    // DEBUG
    //console.debug(`${this.logPrefix} Setting value`, value, result);

    this.formGroup.setValue({
      degrees: result.degrees?.toString() || null,
      minutes: result.minutes?.toString() || null,
      seconds: result.seconds?.toString() || null,
      sign: result.sign,
    });
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return this.formGroup.invalid && this.touched;
  }

  @Input() type: LatLongType;
  @Input('latLongPattern') pattern: LatLongPattern;
  @Input() defaultSign: LatLongSign;
  @Input() maxDecimals: number;
  @Input() readonly = false;

  degreesSymbolUnit = '°';
  minutesSymbolUnit = "'";
  secondsSymbolUnit = '"';
  showMinutes = false;
  showSeconds = false;
  showSign = false;
  degreesMask: MaskitoOptions;
  minutesMask: MaskitoOptions;
  secondsMask: MaskitoOptions;
  degreesPlaceholder: string;
  minutesPlaceholder: string;
  secondsPlaceholder: string;
  lastInputMaxDecimals = LAT_LONG_PATTERN_MAX_DECIMALS;

  private logPrefix = '[mat-latlong-input]';
  private _subscription = new Subscription();
  private cd = inject(ChangeDetectorRef);

  constructor(
    private _elementRef: ElementRef<HTMLElement>,
    @Optional() @Inject(MAT_FORM_FIELD) public _formField: MatFormField,
    @Optional() @Self() public ngControl: NgControl
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    this.type = this.type || 'latitude';
    this.pattern = this.pattern || 'DDMM';
    this.showSign = this.pattern !== 'DD';

    switch (this.pattern) {
      case 'DD':
        this.lastInputMaxDecimals = 7;
        this.degreesPlaceholder = this.type === 'latitude' ? 'COMMON.LAT_LONG.DD_DDDDDDD_PLACEHOLDER' : 'COMMON.LAT_LONG.DDD_DDDDDDD_PLACEHOLDER';
        this.degreesMask = maskitoNumberOptionsGenerator({
          precision: this.lastInputMaxDecimals,
          min: this.type === 'latitude' ? -90 : -180,
          max: this.type === 'latitude' ? 90 : 180,
          minusSign: '-',
        });
        break;
      case 'DDMM':
        this.lastInputMaxDecimals = 3;
        this.showMinutes = true;
        this.degreesPlaceholder = this.type === 'latitude' ? 'COMMON.LAT_LONG.DD_PLACEHOLDER' : 'COMMON.LAT_LONG.DDD_PLACEHOLDER';
        this.minutesPlaceholder = 'COMMON.LAT_LONG.MM_MMM_PLACEHOLDER';
        this.degreesMask = maskitoNumberOptionsGenerator({
          precision: 0,
          min: 0,
          max: this.type === 'latitude' ? 90 : 180,
        });
        this.minutesMask = maskitoNumberOptionsGenerator({
          precision: this.lastInputMaxDecimals,
          min: 0,
          max: 60 - computeDecimalPart(1, this.lastInputMaxDecimals),
        });
        break;
      default: // Default is DDMMSS
        this.lastInputMaxDecimals = 2;
        this.showMinutes = true;
        this.showSeconds = true;
        this.degreesPlaceholder = this.type === 'latitude' ? 'COMMON.LAT_LONG.DD_PLACEHOLDER' : 'COMMON.LAT_LONG.DDD_PLACEHOLDER';
        this.minutesPlaceholder = 'COMMON.LAT_LONG.MM_PLACEHOLDER';
        this.secondsPlaceholder = 'COMMON.LAT_LONG.SS_SS_PLACEHOLDER';
        this.degreesMask = maskitoNumberOptionsGenerator({
          precision: 0,
          min: 0,
          max: this.type === 'latitude' ? 90 : 180,
        });
        this.minutesMask = maskitoNumberOptionsGenerator({
          precision: 0,
          min: 0,
          max: 59,
        });
        this.secondsMask = maskitoNumberOptionsGenerator({
          precision: this.lastInputMaxDecimals,
          min: 0,
          max: 60 - computeDecimalPart(1, this.lastInputMaxDecimals),
        });
        break;
    }

    if (this.showMinutes) {
      this.formGroup.controls.minutes.addValidators(Validators.required);
    }
    if (this.showSeconds) {
      this.formGroup.controls.seconds.addValidators(Validators.required);
    }
    if (this.showSign) {
      this.formGroup.controls.sign.addValidators(Validators.required);
    }

    if (this.readonly) {
      this._subscription.add(this.ngControl.valueChanges.subscribe(() => this.cd.markForCheck()));
    }
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this._subscription.unsubscribe();
  }

  onFocusIn(event: FocusEvent) {
    if (!this.focused) {
      this.focused = true;
      this.stateChanges.next();
    }
  }

  onFocusOut(event: FocusEvent) {
    if (!this._elementRef.nativeElement.contains(event.relatedTarget as Element)) {
      this.touched = true;
      this.focused = false;
      this.onTouched();
      this.stateChanges.next();
    }
  }

  handleInput(controlName: string, nextControlName?: string): void {
    // Can autofocus next control ?
    let focusNext = false;
    switch (controlName) {
      case 'degrees': {
        const degrees = this.safeParseFloat(this.formGroup.value.degrees);
        if (this.type === 'latitude' && (degrees === 0 || degrees > 9)) {
          focusNext = true;
        } else if (this.type === 'longitude' && (degrees === 0 || degrees > 18)) {
          focusNext = true;
        }
        break;
      }
      case 'minutes': {
        const minutes = this.safeParseFloat(this.formGroup.value.minutes);
        if (minutes === 0 || minutes > 6) {
          focusNext = true;
        }
        break;
      }
      case 'seconds': {
        // focusNext = true;
        break;
      }
    }

    if (focusNext) {
      this.autoFocusNext(controlName, nextControlName);
    }
    this.onChange(this.value);
  }

  autoFocusNext(controlName: string, nextControlName?: string): void {
    if (!this.formGroup.controls[controlName].errors && nextControlName) {
      this.getElementByName(nextControlName)?.focus();
    }
  }

  autoFocusPrev(controlName: string, prevControlName: string): void {
    if (isNilOrBlank(this.formGroup.controls[controlName].value)) {
      this.getElementByName(prevControlName)?.focus();
    }
  }

  getElementByName(name: string): HTMLElement | undefined {
    switch (name) {
      case 'degrees':
        return this.degreesInput?.nativeElement;
      case 'minutes':
        return this.minutesInput?.nativeElement;
      case 'seconds':
        return this.secondsInput?.nativeElement;
      case 'sign':
        return this.signInput?.nativeElement;
      default:
        throw new Error(`${this.logPrefix} No HTMLElement with name '${name}'`);
    }
  }

  setDescribedByIds(ids: string[]) {
    const controlElement = this._elementRef.nativeElement.querySelector('.mat-latlong-input-container')!;
    controlElement?.setAttribute('aria-describedby', ids.join(' '));
  }

  onContainerClick() {
    // console.debug(`${this.logPrefix} DEBUG onContainerClick`);
    if (this.readonly) return;
    if (this.empty) {
      this.degreesInput.nativeElement.focus();
    } else if (isNilOrBlank(this.formGroup.value.minutes)) {
      this.minutesInput.nativeElement.focus();
    } else if (isNilOrBlank(this.formGroup.value.seconds)) {
      this.secondsInput.nativeElement.focus();
    } else if (isNil(this.formGroup.value.sign)) {
      this.signInput.nativeElement.focus();
    }
  }

  writeValue(value: number | null): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private computeLatLongFormGroupValue(rawValue: number | string): ILatLongData {
    const isLongitude = this.type === 'longitude';
    const value = typeof rawValue === 'string' ? this.safeParseFloat(rawValue.replace(/,/g, '.')) : rawValue;
    const result: ILatLongData = {
      degrees: null,
      minutes: null,
      seconds: null,
      sign: isNotNil(value)
        ? value < 0
          ? -1
          : 1
        : // Use default sign, if any
          this.defaultSign
          ? this.defaultSign === '-'
            ? -1
            : 1
          : null,
    };

    if (value === 0) {
      result.degrees = 0;
    } else if (isNotNil(value)) {
      // Parse options
      const parseOptions = {
        longitude: isLongitude,
        pattern: this.pattern,
        maxDecimals: this.lastInputMaxDecimals,
        fixLatLong: true,
      };

      if (this.pattern === 'DD') {
        const splitValue = splitDegreesToDDArray(value, parseOptions).slice(1) as number[]; // remove sign;
        result.seconds = 0;
        result.minutes = 0;
        result.degrees = (splitValue[0] + computeDecimalPart(splitValue[1] ?? 0, this.lastInputMaxDecimals)) * (result.sign || 1);
      } else if (this.pattern === 'DDMM') {
        const splitValue = splitDegreesToDDMMArray(value, parseOptions);
        result.seconds = 0;
        result.minutes = Math.abs((splitValue[1] ?? 0) + computeDecimalPart(splitValue[2] ?? 0, this.lastInputMaxDecimals));
        result.degrees = Math.abs(!result.minutes && isNilOrBlank(splitValue[0]) ? 0 : (splitValue[0] ?? 0));
      } else {
        // Default // DDMMSS
        const splitValue = splitDegreesToDDMMSSArray(value, parseOptions);
        result.seconds = Math.abs((splitValue[2] ?? 0) + computeDecimalPart(splitValue[3] ?? 0, this.lastInputMaxDecimals));
        result.minutes = !result.seconds && !splitValue[1] ? 0 : Math.abs(splitValue[1] ?? 0);
        result.degrees = !result.seconds && !result.minutes && isNilOrBlank(splitValue[0]) ? 0 : Math.abs(splitValue[0] ?? 0);
      }
    }

    return result;
  }

  private safeParseFloat(value: string): number {
    return isNotNilOrBlank(value) ? parseFloat(value) : undefined;
  }
}
