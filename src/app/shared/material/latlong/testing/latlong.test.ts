import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { SharedValidators } from '../../../validator/validators';
import { GeolocationUtils } from '../../../geolocation/geolocation.utils';
import { PlatformService } from '../../../../core/services/platform.service';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-latlong-test',
  templateUrl: './latlong.test.html',
})
export class LatLongTestPage implements OnInit {
  form: UntypedFormGroup;

  geoPositionMessage: string;

  mode: 'mobile' | 'desktop' | 'dev' | 'temp' = 'mobile';
  selectedFormat = 'DDMM';
  appearance: MatFormFieldAppearance = 'fill';

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected platform: PlatformService
  ) {
    this.form = formBuilder.group({
      empty: formBuilder.group({
        latitude: [null, SharedValidators.latitude],
        longitude: [null, SharedValidators.longitude],
      }),
      emptyRequired: formBuilder.group({
        latitude: [null, Validators.compose([Validators.required, SharedValidators.latitude])],
        longitude: [null, Validators.compose([Validators.required, SharedValidators.longitude])],
      }),
      enable: formBuilder.group({
        latitude: [null, SharedValidators.latitude],
        longitude: [null, SharedValidators.longitude],
      }),
      disable: formBuilder.group({
        latitude: [null, SharedValidators.latitude],
        longitude: [null, SharedValidators.longitude],
      }),
    });

    const disableFormGroup = this.form.get('disable');
    disableFormGroup.disable();

    // Copy enable value to disable form
    this.form.get('enable').valueChanges.subscribe((value) => disableFormGroup.setValue(value));
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
    }
    setTimeout(() => this.loadData(), 250);
  }

  // Load the form with data
  async loadData() {
    const data = {
      empty: {
        latitude: null,
        longitude: null,
      },
      emptyRequired: {
        latitude: null,
        longitude: null,
      },
      enable: {
        latitude: 50.1,
        longitude: -2,
      },
      disable: {
        latitude: 50.1,
        longitude: -2,
      },
    };

    this.form.setValue(data);
  }

  doSubmit(event: Event) {
    console.debug('Validate form: ', this.form.value);
  }

  async geoPosition(event: Event, controlName?: string) {
    console.debug('Click on geoLocation button', event);

    event.preventDefault();
    event.stopPropagation();

    let logMessage: string;
    const now = Date.now();

    try {
      const position = await GeolocationUtils.getCurrentPosition(this.platform, {
        maximumAge: 80000 /*80s*/,
        timeout: 40000 /*40s*/,
        enableHighAccuracy: false,
      });

      this.form.get(controlName || 'empty').patchValue(position);

      logMessage = `INFO (geolocation): OK in ${Date.now() - now}ms`;
    } catch (err) {
      logMessage = err?.message || err;
      const duration = Date.now() - now;
      if (typeof logMessage === 'object') {
        logMessage = JSON.stringify(logMessage);
      }
      logMessage = `ERROR (geolocation): ${logMessage} - after ${duration}ms`;
    }

    this.geoPositionMessage = logMessage;

    // Clean the log message, if same
    setTimeout(() => {
      if (this.geoPositionMessage === logMessage) {
        this.geoPositionMessage = null;
      }
    }, 5000);
  }

  toggleAppearance(event: MatCheckboxChange) {
    this.appearance = event.checked ? 'outline' : 'fill';
  }

  stringify = JSON.stringify;

  /* -- protected methods -- */
}
