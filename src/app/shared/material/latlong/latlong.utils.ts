import {
  DEFAULT_PLACEHOLDER_CHAR,
  EMPTY_PLACEHOLDER_CHAR,
  EMPTY_PLACEHOLDER_CHAR_REGEXP_GLOBAL,
  PLUS_PLACEHOLDER_CHAR_REGEXP_GLOBAL,
  SPACE_PLACEHOLDER_CHAR_REGEXP_GLOBAL,
} from '../../constants';
import { getInputSelectionRangesFromMask } from '../../inputs';
import { isNil } from '../../functions';

export declare type LatLongType = 'latitude' | 'longitude';
export declare type LatLongPattern = 'DDMMSS' | 'DDMM' | 'DD';
export const LAT_LONG_PATTERNS: LatLongPattern[] = ['DDMMSS', 'DDMM', 'DD'];

const D = /\d|\s/;
const D5 = /[0-5]|\s/;
const M = D;
const M5 = D5;
const S = D;
const S5 = D5;
const SIGN = /[+-]/;

export declare type LatLongSign = '+' | '-';
export declare type LatLongSignValue = 1 | -1;

export interface ILatLongData {
  degrees: number;
  minutes: number;
  seconds: number;
  sign: LatLongSignValue;
}

export const MASKS = {
  latitude: {
    DDMMSS: [D, D, '°', ' ', M5, M, "'", ' ', S5, S, '.', S, S, '"'],
    DDMM: [D, D, '°', ' ', M5, M, '.', M, M, M, "'"],
    DD: [SIGN, ' ', D, D, '.', D, D, D, D, D, D, D, '°'],
  },
  longitude: {
    DDMMSS: [D, D, D, '°', ' ', M5, M, "'", ' ', S5, S, '.', S, S, '"'],
    DDMM: [D, D, D, '°', ' ', M5, M, '.', M, M, M, "'"],
    DD: [SIGN, D, D, D, '.', D, D, D, D, D, D, D, '°'],
  },
};
export const MASK_RANGES = {
  latitude: {
    DDMMSS: getInputSelectionRangesFromMask(MASKS.latitude.DDMMSS),
    DDMM: getInputSelectionRangesFromMask(MASKS.latitude.DDMM),
    DD: getInputSelectionRangesFromMask(MASKS.latitude.DD),
  },
  longitude: {
    DDMMSS: getInputSelectionRangesFromMask(MASKS.longitude.DDMMSS),
    DDMM: getInputSelectionRangesFromMask(MASKS.longitude.DDMM),
    DD: getInputSelectionRangesFromMask(MASKS.longitude.DD),
  },
};

export declare class LatLongFormatOptions {
  pattern: LatLongPattern;
  maxDecimals?: number;
  placeholderChar?: string;
  hideSign?: boolean;
  fixLatLong?: boolean;
}

export const LAT_LONG_PATTERN_MAX_DECIMALS = 3;
export const LAT_LONG_VALUE_MAX_DECIMALS = 7;

const DEFAULT_OPTIONS = <LatLongFormatOptions>{
  pattern: 'DDMM',
  maxDecimals: LAT_LONG_PATTERN_MAX_DECIMALS,
  placeholderChar: DEFAULT_PLACEHOLDER_CHAR,
};
const DEFAULT_LATITUDE_OPTIONS: LatLongFormatOptions & { longitude: boolean } = {
  ...DEFAULT_OPTIONS,
  longitude: false,
};
const DEFAULT_LONGITUDE_OPTIONS: LatLongFormatOptions & { longitude: boolean } = {
  ...DEFAULT_OPTIONS,
  longitude: true,
};

export function computeDecimalPart(value: number, nbOfDecimals: number): number {
  return value * 10 ** (nbOfDecimals * -1);
}

export function formatLatLong(value: number, type: LatLongType, opts?: Partial<LatLongFormatOptions>) {
  switch (type) {
    case 'latitude':
      return formatLatitude(value, opts);
    case 'longitude':
      return formatLongitude(value, opts);
  }
}

export function formatLatitude(value: number | null, opts?: Partial<LatLongFormatOptions>): string {
  if (value === undefined || value === null) return null;

  const finalOpts = {
    ...DEFAULT_LATITUDE_OPTIONS,
    fixLatLong: true,
    ...opts,
  };

  let fieldsValues: any[];
  switch (opts.pattern) {
    case 'DDMMSS':
      fieldsValues = splitDegreesToDDMMSSArray(value, finalOpts);
      break;
    case 'DDMM':
      fieldsValues = splitDegreesToDDMMArray(value, finalOpts);
      break;
    default: // DD is the default
      fieldsValues = splitDegreesToDDArray(value, finalOpts);
  }

  return fieldsValues.length > 0 ? formatFieldsArrayToString(fieldsValues, finalOpts) : '';
}

export function formatLongitude(value: number | null, opts?: Partial<LatLongFormatOptions>): string {
  if (value === undefined || value === null) return null;

  const finalOpts = {
    ...DEFAULT_LONGITUDE_OPTIONS,
    fixLatLong: true,
    ...opts,
  };

  let parts: any[];
  switch (opts.pattern) {
    case 'DDMMSS':
      parts = splitDegreesToDDMMSSArray(value, finalOpts);
      break;
    case 'DDMM':
      parts = splitDegreesToDDMMArray(value, finalOpts);
      break;
    default: // DD is the default
      parts = splitDegreesToDDArray(value, finalOpts);
  }

  return parts.length > 0 ? formatFieldsArrayToString(parts, finalOpts) : '';
}

export function splitDegreesToDDArray(value: number, opts: LatLongFormatOptions & { longitude: boolean }): (number | string)[] {
  const negative = value < 0;
  if (negative) value *= -1;

  if (opts.fixLatLong) {
    value = fixValue(value, opts.longitude);
  }

  const sign = negative ? '-' : '+';
  const degreesFields = roundFloat(value, opts.maxDecimals).toString().split('.');
  const degrees = parseInt(degreesFields[0]);
  if (degreesFields.length > 1) {
    const degreesTeens = parseInt(degreesFields[1]) * 10 ** (opts.maxDecimals - degreesFields[1].length);
    return [sign, degrees, degreesTeens];
  } else {
    if (degreesFields[0] === `1e-${opts.maxDecimals}`) {
      return [sign, 0, 1 / (parseFloat(degreesFields[0]) * 10 ** opts.maxDecimals)];
    } else {
      return [sign, degrees];
    }
  }
}

export function splitDegreesToDDMMArray(value: number, opts: LatLongFormatOptions & { longitude: boolean }): number[] {
  const negative = value < 0;
  value = Math.abs(value);

  if (opts.fixLatLong) {
    value = fixValue(value, opts.longitude);
  }

  let degrees: number = (negative ? -1 : 1) * Math.trunc(value);
  const minutesFields = roundFloat((value - degrees) * 60, opts.maxDecimals)
    .toString()
    .split('.');

  let minutes = parseInt(minutesFields[0]);
  let minutesTeens: number;
  if (minutesFields.length > 1) {
    minutesTeens = parseInt(minutesFields[1]);
    if (minutesFields[1].length === 1) minutesTeens *= 100;
    else if (minutesFields[1].length === 2) minutesTeens *= 10;
  } else {
    minutesTeens = 0;
  }

  while (minutes >= 60) {
    minutes -= 60;
    degrees += 1;
  }

  // The reduce remove all right zeros
  const result = [minutesTeens, minutes, degrees]
    .reduce((acc, val) => {
      if (acc.length > 0 || val > 0) {
        acc.push(val);
      }
      return acc;
    }, [])
    .reverse();

  // Re-add sign on degrees
  if (negative) result[0] = -1 * result[0];

  return result;
}

export function splitDegreesToDDMMSSArray(value: number, opts: LatLongFormatOptions & { longitude: boolean }): number[] {
  const negative = value < 0;
  value = Math.abs(value);

  if (opts.fixLatLong) {
    value = fixValue(value, opts.longitude);
  }

  let degrees: number = Math.trunc(value);
  let minutes: number | string = Math.trunc((value - degrees) * 60);
  const secondFields: string[] = roundFloat(((value - degrees) * 60 - minutes) * 60, opts.maxDecimals)
    .toString()
    .split('.');
  let seconds: number = parseInt(secondFields[0]);
  const miliseconds: number = secondFields.length > 1 ? parseInt(secondFields[1]) * (secondFields[1].length === 1 ? 10 : 1) : 0;

  while (seconds >= 60) {
    seconds -= 60;
    minutes += 1;
  }
  while (minutes >= 60) {
    minutes -= 60;
    degrees += 1;
  }

  // The reduce remove all right zeros
  const result = [miliseconds, seconds, minutes, degrees]
    .reduce((acc, val) => {
      if (acc.length > 0 || val > 0) {
        acc.push(val);
      }
      return acc;
    }, [])
    .reverse();

  // Re-add sign on degrees
  if (negative) result[0] = -1 * result[0];

  return result;
}

function fixValue(value: number, longitude: boolean) {
  // Fix longitude when outside [-180, 180]
  if (longitude && (value > 180 || value < -180)) {
    value %= 180;
  }
  // Fix latitude when outside [-90, 90]
  else if (!longitude && (value > 90 || value < -90)) {
    value %= 90;
  }
  return value;
}

function formatFieldsArrayToString(values: (string | number)[], opts: LatLongFormatOptions & { longitude: boolean }): string {
  const pattern = opts.pattern || 'DD';
  const mask = MASKS[opts.longitude ? 'longitude' : 'latitude'][pattern];
  const inputSelectionRanges = MASK_RANGES[opts.longitude ? 'longitude' : 'latitude'][pattern];

  const negative = values[0].toString().slice(0, 1) === '-';
  if (typeof values[0] === 'number') {
    // Normalize degrees to be OK when convert to string
    values[0] = Math.abs(values[0]);
  }

  let result = '';
  const placeholderChar = opts.placeholderChar;
  inputSelectionRanges.forEach((range, idx) => {
    // "start" and "end" are used to get symbol chars from mask placeholder
    // (between valuables characters)
    const last = idx === inputSelectionRanges.length - 1;
    const start = range[1] + 1;
    const end = last ? mask.length : inputSelectionRanges[idx + 1][0];
    const size = range[1] - range[0] + 1;
    const symbol = mask.slice(start, end).join('');
    if (idx < values.length) {
      let value = values[idx].toString();
      value = value.padStart(size, last ? '0' : placeholderChar);
      if (last && placeholderChar === '') {
        value = value.replace(/0*$/, placeholderChar);
      }
      result += value + symbol;
    } else {
      // '' as fillChar = don't fill
      if (placeholderChar !== '') {
        const value = ''.toString().padStart(size, placeholderChar);
        result += value + symbol;
      }
      // Avoid last char dot, replace it with real last symbol
      // 01.00" ->
      //      OK:  1"
      //      KO:  1.
      if (last && result.slice(-1) === '.') {
        result = result.slice(0, -1);
        if (symbol) {
          result += symbol;
        }
      }
    }
  });

  let sign = '';
  if (!opts?.hideSign && opts.pattern !== 'DD') {
    if (opts.longitude) {
      sign = negative ? ' W' : ' E';
    } else {
      sign = negative ? ' S' : ' N';
    }
  } else if (opts.hideSign && opts.pattern === 'DD') {
    result = result.replace(/^\+/, ' ');
  }

  return result + sign;
}

// 36°57'9" N  = 36.9525000
// 10°4'21" W = -10.0725000
export function parseLatitudeOrLongitude(input: string, pattern: string, maxDecimals?: number, placeholderChar?: string): number | null {
  placeholderChar = placeholderChar || DEFAULT_PLACEHOLDER_CHAR;
  let inputFix = input.trim();
  if (placeholderChar === EMPTY_PLACEHOLDER_CHAR) {
    inputFix = inputFix
      // Replace empty char placeholder
      .replace(EMPTY_PLACEHOLDER_CHAR_REGEXP_GLOBAL, '')
      // Remove space and '+' (= trim on each parts)
      .replace(SPACE_PLACEHOLDER_CHAR_REGEXP_GLOBAL, '')
      .replace(PLUS_PLACEHOLDER_CHAR_REGEXP_GLOBAL, '');
  }
  // Remove all placeholder (= trim on each parts)
  else {
    inputFix = inputFix.replace(new RegExp(`[ +${placeholderChar}]+`, 'g'), '');
  }

  //DEBUG console.debug("Parsing lat= " + inputFix);
  const parts = inputFix.split(/[^-\d\w.,]+/);
  let degrees = parseFloat(parts[0].replace(/,/g, '.'));
  if (isNaN(degrees)) {
    console.debug('parseLatitudeOrLongitude ' + input + ' -> Invalid degrees (NaN). Parts found:', parts);
    return NaN;
  }

  if (pattern === 'DD') {
    return roundFloat(degrees, maxDecimals);
  }

  const minutes = ((pattern === 'DDMMSS' || pattern === 'DDMM') && parts[1] && parseFloat(parts[1].replace(/,/g, '.'))) || 0;
  const seconds = (pattern === 'DDMMSS' && parts[2] && parseFloat(parts[2].replace(/,/g, '.'))) || 0;
  const direction = (pattern === 'DDMMSS' && parts[3]) || (pattern === 'DDMM' && parts[2]) || undefined;
  const sign = direction && (direction === 's' || direction === 'S' || direction === 'w' || direction === 'W') ? -1 : 1;

  return computeDecimalDegrees(degrees, minutes, seconds, sign, maxDecimals);
}

export function computeDecimalDegrees(degrees: number, minutes: number, seconds: number, sign: -1 | 1, maxDecimals?: number) {
  if (isNil(degrees)) return null;
  degrees = (sign || 1) * (degrees + (minutes || 0) / 60 + (seconds || 0) / (60 * 60));
  return roundFloat(degrees, maxDecimals);
}

function roundFloat(input: number, maxDecimals?: number): number {
  if (maxDecimals !== undefined && maxDecimals >= 0) {
    // Utilisation de toFixed pour gérer les problèmes de précision
    const str = input.toFixed(maxDecimals);
    return parseFloat(str);
  }
  return input;
}
