import {
  AfterViewInit,
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Inject,
  Input,
  numberAttribute,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldAppearance, MatFormFieldDefaultOptions, SubscriptSizing } from '@angular/material/form-field';
import { AppFloatLabelType } from '../../form/field.model';
import { isMobile } from '../../platforms';
import { SharedValidators } from '../../validator/validators';
import { MAT_FORM_FIELD_DEFAULT_APPEARANCE, MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING } from '../material.config';
import { LAT_LONG_VALUE_MAX_DECIMALS, LatLongPattern, LatLongSign, LatLongType } from './latlong.utils';
import { MatLatLongFieldInput } from './material.latlong-input';
import { isNotNil } from '../../functions';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'mat-latlong-field',
  templateUrl: './material.latlong.html',
  styleUrls: ['./material.latlong.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MatLatLongField),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatLatLongField implements ControlValueAccessor, OnInit, AfterViewInit, OnDestroy {
  private _subscription = new Subscription();
  private _appearance: MatFormFieldAppearance | null = null;
  private _subscriptSizing: SubscriptSizing | null = null;
  private _readonly = false;
  private _tabindex: number;
  private readonly _defaultAppearance: MatFormFieldAppearance;
  private readonly _defaultSubscriptSizing: SubscriptSizing;

  protected i18nErrorKeys = SharedValidators.I18N_ERROR_KEYS;

  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() type: LatLongType;
  @Input('latLongPattern') pattern: LatLongPattern;
  @Input() maxDecimals: number = LAT_LONG_VALUE_MAX_DECIMALS;
  @Input() required = false;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() placeholder: string;
  @Input() defaultSign: LatLongSign;
  @Input() autofocus = false;
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input('class') classList: string;

  @Input({ transform: numberAttribute }) set tabindex(value: number) {
    if (this._tabindex !== value) {
      this._tabindex = value;
    }
  }

  get tabindex(): number {
    return this._tabindex;
  }

  @Input()
  set appearance(value: MatFormFieldAppearance) {
    this._appearance = value;
  }

  get appearance(): MatFormFieldAppearance {
    return this._appearance || this._defaultAppearance;
  }

  @Input() set readonly(value: boolean) {
    this._readonly = value;
    this.markForCheck();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  @Input()
  set subscriptSizing(value: SubscriptSizing) {
    this._subscriptSizing = value;
  }

  get subscriptSizing(): SubscriptSizing {
    return this._subscriptSizing || this._defaultSubscriptSizing;
  }

  get disabled(): any {
    return this.readonly || this.formControl.disabled;
  }

  @ViewChild('input') input: MatLatLongFieldInput;
  pendingValue: any;
  pendingValueSet = false;
  pendingDisabledState: boolean;
  pendingOnChange: (_: any) => unknown;
  pendingOnTouched: () => unknown;

  constructor(
    private cd: ChangeDetectorRef,
    @Optional() private formGroupDir: FormGroupDirective,
    @Inject(MAT_FORM_FIELD_DEFAULT_OPTIONS) defaultOptions?: MatFormFieldDefaultOptions
  ) {
    this._defaultSubscriptSizing = defaultOptions?.subscriptSizing || MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING;
    this._defaultAppearance = defaultOptions?.appearance || MAT_FORM_FIELD_DEFAULT_APPEARANCE;
  }

  // Delegate control vale accessor
  writeValue(value: any): void {
    if (this.input) {
      this.input.writeValue(value);
      this.markForCheck();
    } else {
      this.pendingValue = value;
      this.pendingValueSet = true;
    }
  }
  registerOnChange(fn: any): void {
    if (this.input) {
      this.input.registerOnChange(fn);
    } else {
      this.pendingOnChange = fn;
    }
  }
  registerOnTouched(fn: any): void {
    if (this.input) {
      this.input.registerOnTouched(fn);
    } else {
      this.pendingOnTouched = fn;
    }
  }
  setDisabledState?(isDisabled: boolean): void {
    if (this.input) {
      this.input.setDisabledState(isDisabled);
    } else {
      this.pendingDisabledState = isDisabled;
    }
  }

  ngOnInit() {
    this.formControl =
      this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-latlong-field>.");

    this.mobile = this.mobile ?? isMobile(window);

    // Listen status changes (when done outside the component  - e.g. when setErrors() is calling on the formControl)
    this._subscription.add(
      this.formControl.statusChanges
        .pipe(
          filter(() => !this.readonly) // Skip
        )
        .subscribe(() => this.markForCheck())
    );
  }

  ngAfterViewInit() {
    if (!this.readonly) {
      if (this.pendingOnChange) {
        this.input.registerOnChange(this.pendingOnChange);
      }
      if (this.pendingOnTouched) {
        this.input.registerOnTouched(this.pendingOnTouched);
      }
      if (isNotNil(this.pendingDisabledState)) {
        this.input.setDisabledState(this.pendingDisabledState);
      }
      if (this.pendingValueSet) {
        this.input.writeValue(this.pendingValue);
      }
    }
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  clearValue(event?: Event) {
    event?.stopPropagation();
    this.formControl.setValue(null);
  }

  /* -- private method -- */

  private markForCheck() {
    this.cd.markForCheck();
  }
}
