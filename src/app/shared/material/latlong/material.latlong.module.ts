import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskitoDirective } from '@maskito/angular';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { TranslateModule } from '@ngx-translate/core';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { MatLatLongField } from './material.latlong';
import { MatLatLongFieldInput } from './material.latlong-input';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedDirectivesModule,
    SharedPipesModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MaskitoDirective,
    MatIconModule,
    MatSelectModule,
    TranslateModule.forChild(),
  ],
  declarations: [MatLatLongField, MatLatLongFieldInput],
  exports: [MatLatLongField, MatLatLongFieldInput],
})
export class SharedMatLatLongModule {}
