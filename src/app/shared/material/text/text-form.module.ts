import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { TextForm } from './text-form.component';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { CommonModule, NgIf } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    TranslateModule.forChild(),
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedPipesModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    SharedDirectivesModule,
  ],

  declarations: [
    // Components
    TextForm,
  ],
  exports: [
    TranslateModule,
    // Components
    TextForm,
  ],
})
export class SharedTextFormModule {}
