import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  numberAttribute,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
  UntypedFormBuilder,
  UntypedFormControl,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { AppForm } from '../../../core/form/form.class';
import { isNotNil } from '../../functions';
import { focusInput } from '../../inputs';
import { AppFloatLabelType } from '../../form/field.model';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';

const noop = () => {};

@Component({
  selector: 'app-text-form',
  templateUrl: './text-form.component.html',
  styleUrls: ['./text-form.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextForm),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextForm extends AppForm<{ text: string }> implements OnInit, ControlValueAccessor {
  @Input({ transform: booleanAttribute }) showError = true;
  @Input() placeholder: string;
  @Input({ transform: booleanAttribute }) multiline = true;

  @Input({ transform: booleanAttribute }) autoSize = false;
  @Input({ transform: numberAttribute }) autoSizeMinRows: number;
  @Input({ transform: numberAttribute }) autoSizeMaxRows: number;
  @Input({ transform: numberAttribute }) maxLength = 2000;
  @Input({ transform: booleanAttribute }) autofocus = false;
  @Input() validator: ValidatorFn = null;
  @Input({ transform: booleanAttribute }) standalone = false;
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;

  /**
   * @deprecated Use autoSize instead
   */
  @Input({ transform: booleanAttribute }) set autoHeight(value: boolean) {
    this.autoSize = value;
  }
  get autoHeight(): boolean {
    return this.autoSize;
  }

  @Output() textAreaChanges = new EventEmitter<string>();

  @ViewChild('textArea') textArea: ElementRef;
  @ViewChild('textInput') textInput: ElementRef;
  @ViewChild(CdkTextareaAutosize) autosizeDirective: CdkTextareaAutosize;

  textControl: UntypedFormControl;

  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;

  constructor(
    protected injector: Injector,
    protected cd: ChangeDetectorRef,
    protected formBuilder: UntypedFormBuilder,
    @Optional() private formGroupDir: FormGroupDirective
  ) {
    super(injector);
  }

  ngOnInit() {
    if (!this.standalone) {
      this.formControl =
        this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
      if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-date-field>.");
    }

    // Compute validators
    const validators = isNotNil(this.maxLength) && this.maxLength > 0 ? [Validators.maxLength(this.maxLength)] : [];
    if (this.validator) validators.push(this.validator);

    // Create the form
    const form = this.formBuilder.group({
      text: [null, validators.length ? Validators.compose(validators) : null],
    });
    this.setForm(form);

    this.textControl = form.get('text') as UntypedFormControl;
    this.registerSubscription(
      this.textControl.valueChanges.subscribe((value) => {
        if (this.standalone) {
          this.textAreaChanges.emit(value);
        } else {
          this.textFormValueChanged(value);
        }
      })
    );

    super.ngOnInit();
  }

  containerResize() {
    this.autosizeDirective?.resizeToFitContent(true);
  }

  focusInput() {
    if (this.textArea) focusInput(this.textArea);
    if (this.textInput) focusInput(this.textInput);
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.textControl.disable({ emitEvent: false });
    } else {
      this.textControl.enable({ emitEvent: false });
    }
    this.markForCheck();
  }

  writeValue(value: any): void {
    this.textControl.patchValue(value);
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected checkIfTouched(opts?: { emitEvent?: boolean }) {
    if (this.textControl.touched) {
      this._onTouchedCallback();

      if (!opts || opts.emitEvent !== false) {
        this.markForCheck();
      }
    }
  }

  private textFormValueChanged(value: string): void {
    if (this.formControl.value !== value) {
      this._onChangeCallback(value);
      this.checkIfTouched();
    }
  }
}
