import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { TextForm } from '../text-form.component';

@Component({
  selector: 'text-form-testing',
  templateUrl: 'text-form.testing.html',
})
export class TextFormTestingPage implements AfterViewInit {
  @ViewChild('standaloneTextForm1') standaloneTextForm1: TextForm;
  @ViewChild('standaloneTextForm2') standaloneTextForm2: TextForm;

  form: UntypedFormGroup;

  protected readonly Validators = Validators;
  stringify = JSON.stringify;

  constructor(protected formBuilder: UntypedFormBuilder) {
    this.form = formBuilder.group({
      empty: [null],
      required: [null, Validators.required],
      multiline: [null],
    });
  }

  ngAfterViewInit(): void {
    this.standaloneTextForm1.enable();
    this.standaloneTextForm2.enable();
  }
}
