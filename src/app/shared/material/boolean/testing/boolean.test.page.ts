import { AfterViewInit, ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { AppForm } from '../../../../core/form/form.class';
import { UntypedFormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-boolean-test-page',
  templateUrl: './boolean.test.page.html',
})
export class BooleanTestPage extends AppForm<any> implements OnInit, AfterViewInit {
  stringify = JSON.stringify;
  mode: 'radio' | 'checkbox' | 'button' = 'radio';
  styles: string[] = ['radio', 'checkbox', 'button'];

  constructor(
    protected injector: Injector,
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    super(injector);
    this.setForm(
      this.formBuilder.group({
        disabledEmpty: [null],
        disabledFalse: [false],
        disabledTrue: [true],
        enabledEmpty: [null],
        enabledEmptyRequired: [null, Validators.required],
        enabledFalse: [false],
        enabledTrue: [true],
      })
    );
  }

  ngAfterViewInit(): void {
    this.form.get('enabledEmpty').enable();
    this.form.get('enabledFalse').enable();
    this.form.get('enabledTrue').enable();
    this.form.get('enabledEmptyRequired').enable();
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
      this.cd.detectChanges();
      this.markForCheck();
    }
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }

  protected clearControlValue(path: string, event?: Event) {
    event?.preventDefault();
    this.form.get(path)?.setValue(null);
  }
}
