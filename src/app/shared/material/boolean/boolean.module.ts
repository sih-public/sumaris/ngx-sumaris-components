import { NgModule } from '@angular/core';
import { MatCommonModule } from '@angular/material/core';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { MatBooleanField } from './material.boolean';
import { MatIcon } from '@angular/material/icon';
import { MatIconButton } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedPipesModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    TranslateModule.forChild(),
    SharedDirectivesModule,
    MatIcon,
    MatIconButton,
  ],
  exports: [MatBooleanField],
  declarations: [MatBooleanField],
})
export class SharedMatBooleanModule {}
