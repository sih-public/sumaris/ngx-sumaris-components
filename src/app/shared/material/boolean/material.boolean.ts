import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  numberAttribute,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Provider,
  ViewChild,
} from '@angular/core';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { MatRadioButton, MatRadioChange } from '@angular/material/radio';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';
import { InputElement } from '../../inputs';
import { isNil, isNotNil, toNumber } from '../../functions';
import { AppFloatLabelType } from '../../form/field.model';
import { Subscription } from 'rxjs';

const DEFAULT_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MatBooleanField),
  multi: true,
};

const noop = () => {};

@Component({
  selector: 'mat-boolean-field',
  templateUrl: './material.boolean.html',
  styleUrls: ['./material.boolean.scss'],
  providers: [DEFAULT_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatBooleanField implements OnInit, ControlValueAccessor, InputElement, OnDestroy {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;
  private readonly _subscription = new Subscription();
  private _writing = false;

  protected _value: boolean;
  protected _tabindex: number;
  protected _showRadio = false; // Will be changed when focus in
  protected _focused = false;

  @ViewChild('fakeInput') fakeInput: ElementRef;
  @ViewChild('yesRadioButton') yesRadioButton: MatRadioButton;
  @ViewChild('noRadioButton') noRadioButton: MatRadioButton;
  @ViewChild('checkboxButton') checkboxButton: MatCheckbox;

  @Input({ transform: booleanAttribute }) disabled = false;
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() placeholder: string;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input({ transform: booleanAttribute }) readonly = false;
  @Input({ transform: booleanAttribute }) required = false;
  @Input({ transform: booleanAttribute }) compact = false;
  @Input() style: 'radio' | 'checkbox' | 'button';
  @Input({ transform: numberAttribute }) buttonsColCount: number;
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;
  @Input() yesLabel = 'COMMON.YES';
  @Input() noLabel = 'COMMON.NO';
  @Input({ transform: booleanAttribute }) showButtonIcons = true;
  @Input() yesIcon = 'checkmark';
  @Input() noIcon = 'close';
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input() labelPosition: 'after' | 'before' = 'after';

  @Input({ transform: numberAttribute }) set tabindex(value: number) {
    if (this._tabindex !== value) {
      this._tabindex = value;
      setTimeout(() => this.updateTabIndex());
    }
  }

  get tabindex(): number {
    return this._tabindex;
  }

  @Input({ transform: booleanAttribute })
  set showRadio(v: any) {
    if (v !== this._showRadio) {
      this._showRadio = v;
      this.updateFakeInput();
    }
  }

  get showRadio(): any {
    return (
      this._showRadio ||
      this._focused ||
      this.floatLabel === 'always' ||
      this.floatLabel === 'never' ||
      this.style === 'checkbox' ||
      isNotNil(this._value)
    );
  }

  @Input()
  set value(v: any) {
    if (v !== this._value) {
      this._value = v;
      this._onChangeCallback(v);
    }
  }

  get value(): any {
    return this._value;
  }
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix,@angular-eslint/no-output-rename
  @Output('keyup.enter') keyupEnter = new EventEmitter<any>();
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output('focus') focussed = new EventEmitter<FocusEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output('blur') blurred = new EventEmitter<FocusEvent>();

  constructor(
    private cd: ChangeDetectorRef,
    protected el: ElementRef<HTMLElement>,
    @Optional() private formGroupDir: FormGroupDirective
  ) {}

  ngOnInit() {
    this.formControl =
      this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-boolean-field>.");

    this.style = this.style || (this.compact ? 'checkbox' : 'radio');

    if (this.style === 'button') {
      this.buttonsColCount = toNumber(this.buttonsColCount, 2);
    }

    this._subscription.add(this.formControl.valueChanges.subscribe((value) => this.writeValue(value)));

    this.updateFakeInput();
    this.updateTabIndex();
  }

  ngOnDestroy() {
    this.blurred.complete();
  }

  writeValue(value: any, event?: Event): void {
    if (this._writing) return;

    // DEBUG
    //console.debug('[mat-boolean-field] Writing value', value);

    this._writing = true;
    if (value !== this._value || value !== this.formControl.value) {
      this._value = value;

      if (value !== this.formControl.value) {
        this.formControl.patchValue(value, { emitEvent: false });
        this._onChangeCallback(value);
      }

      if (this.yesRadioButton) {
        this.yesRadioButton.checked = value === true;
        this.noRadioButton.checked = value === false;
      } else if (this.checkboxButton) {
        if (isNil(value)) {
          this.checkboxButton.indeterminate = true;
        } else {
          this.checkboxButton.checked = value;
        }
      }

      setTimeout(() => {
        this.updateFakeInput();
        this.updateTabIndex();
      });
    }
    this._writing = false;

    if (this.style === 'button') {
      if (event) this.keyupEnter.emit(event);
    }

    this.markForCheck();
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  protected onFocusFakeInput(event: FocusEvent) {
    if (!this.showRadio) {
      event.preventDefault();

      // Hide the fake input
      this.fakeInput?.nativeElement.classList.add('cdk-visually-hidden');
    }

    // Focus on first button
    this.focus();
  }

  protected onBlurInput(event: FocusEvent) {
    const internalEvent = this.inputsElements.some((el) => el === event.relatedTarget);
    if (!internalEvent && this._focused) {
      this._focused = false;
      this.updateFakeInput();
      this.updateTabIndex();
      this.blurred.emit(event);

      this._checkIfTouched();
    }
  }

  focus() {
    if (this._focused) return; // Already focused

    this._focused = true;
    this.markForCheck();

    setTimeout(() => {
      this.updateFakeInput();
      this.updateTabIndex();
    });
  }

  protected onRadioValueChanged(event: MatRadioChange): void {
    if (this._writing) return; // Skip if call by self
    this._writing = true;
    this.emitChange(event.value);
    this._writing = false;
  }

  protected onCheckboxValueChanged(event: MatCheckboxChange): void {
    if (this._writing) return; // Skip if call by self
    this._writing = true;
    this.emitChange(event.checked);
    this._writing = false;

    this.focus();
  }

  toggleValue(event?: Event): void {
    event?.preventDefault();

    if (this._writing) return; // Skip if call by self
    this._writing = true;
    this._value = !this._value;
    if (this.yesRadioButton) {
      this.yesRadioButton.checked = this._value;
      this.noRadioButton.checked = !this._value;
    } else if (this.checkboxButton) {
      this.checkboxButton.checked = this._value;
    }
    this._checkIfTouched();
    this._onChangeCallback(this._value);
    this._writing = false;
  }

  /* -- private method -- */

  private emitChange(value: boolean) {
    if (this.formControl.value !== value) {
      this._value = value;

      this.formControl.setValue(value, { emitEvent: false });

      // Changes comes from inside function: use the callback
      this._onChangeCallback(value);

      // Check if need to update controls
      this._checkIfTouched();
    }
  }

  private _checkIfTouched() {
    if (this.formControl.touched) {
      this.markForCheck();
      this._onTouchedCallback();
    }
  }

  /**
   * Show/Hide the fake input
   */
  private updateFakeInput() {
    if (this.showRadio || this.style === 'button') {
      this.fakeInput?.nativeElement.classList.add('cdk-visually-hidden');
    } else {
      this.fakeInput?.nativeElement.classList.remove('cdk-visually-hidden');
    }

    this.markForCheck();
  }

  /**
   * This is a special case, because, this component has a temporary component displayed before the first focus event
   */
  private updateTabIndex() {
    if (isNil(this._tabindex)) return;

    if (this.fakeInput) {
      this.fakeInput.nativeElement.tabIndex = this._tabindex;
    }

    this.markForCheck();
  }

  private markForCheck() {
    this.cd.markForCheck();
  }

  clearValue(event?: Event) {
    this.writeValue(null);
    event?.stopPropagation();
  }

  protected get radioButtonElements(): HTMLElement[] {
    return this.yesRadioButton ? [this.yesRadioButton, this.noRadioButton].map((b) => b._inputElement.nativeElement) : [];
  }

  protected get inputsElements(): HTMLElement[] {
    return this.radioButtonElements
      .concat(this.checkboxButton ? [this.checkboxButton._inputElement.nativeElement] : [])
      .concat(this.fakeInput ? [this.fakeInput.nativeElement] : []);
  }
}
