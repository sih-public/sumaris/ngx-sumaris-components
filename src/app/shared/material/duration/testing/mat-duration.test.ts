import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { AppFormUtils } from '../../../../core/form/form.utils';
import 'moment-timezone';

@Component({
  selector: 'app-duration-test',
  templateUrl: './mat-duration.test.html',
})
export class DurationTestPage implements OnInit {
  form: UntypedFormGroup;
  mode: 'desktop' | 'temp' = 'desktop';

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    this.form = formBuilder.group({
      empty: [null],
      enable: [null, Validators.required],
      disable: [null, Validators.required],
      readonly: [null],
    });

    const disableControl = this.form.get('disable');
    disableControl.disable();

    // Copy the 'enable' value, into the disable control
    this.form.get('enable').valueChanges.subscribe((value) => disableControl.setValue(value));
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
    }
  }

  // Load the form with data
  async loadData() {
    const data = {
      empty: null, // toDateISOString(now.clone().add(2, 'hours')),
      enable: 1.5,
      disable: 3.25,
      readonly: 5,
    };

    this.form.setValue(data);

    this.log('[test-page] Data loaded: ' + JSON.stringify(data));

    this.form
      .get('empty')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°1: ' + JSON.stringify(value)));

    this.form
      .get('enable')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°2: ' + JSON.stringify(value)));
  }

  doSubmit(event) {
    this.form.markAllAsTouched();
    this.log('[test-page] Form content: ' + JSON.stringify(this.form.value));
    this.log('[test-page] Form status: ' + this.form.status);
    if (this.form.invalid) {
      this.log('[test-page] Form errors: ' + JSON.stringify(this.form.errors));

      // DEBUG
      AppFormUtils.logFormErrors(this.form);
    }
  }

  log(message: string) {
    console.debug(message);
  }
  stringify = JSON.stringify;

  /* -- protected methods -- */
}
