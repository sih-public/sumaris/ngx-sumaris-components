import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { BadgeDirective } from './badge.directive';
import { MatBadgeModule } from '@angular/material/badge';

@NgModule({
  imports: [CommonModule, IonicModule, MatBadgeModule],
  exports: [BadgeDirective],
  declarations: [BadgeDirective],
})
export class SharedBadgeModule {}
