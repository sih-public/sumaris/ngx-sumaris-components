import {
  AfterViewInit,
  ANIMATION_MODULE_TYPE,
  Directive,
  ElementRef,
  HostBinding,
  Inject,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Renderer2,
  SimpleChanges,
} from '@angular/core';
import { MatBadge, MatBadgePosition, MatBadgeSize } from '@angular/material/badge';
import { AppColors } from '../../types';
import { AriaDescriber } from '@angular/cdk/a11y';
import { BooleanInput } from '@angular/cdk/coercion';
import { isNotNilOrBlank } from '../../functions';

export declare type MatBadgeFill = 'clear' | 'solid';
const EMPTY_CHAR = 0x00a0;
const SIZE_TO_PX = {
  large: 28,
  medium: 22,
  small: 16,
};
@Directive({
  selector: '[appBadge], [appBadgeIcon], [appBadgeMatIcon]',
})
export class BadgeDirective extends MatBadge implements AfterViewInit, OnChanges, OnDestroy, OnInit {
  @HostBinding('class.mat-badge') true;
  @HostBinding('class.mat-badge-overlap') get isOverlapHost() {
    return this.overlap;
  }
  @HostBinding('class.mat-badge-above') get isAboveHost() {
    return this.isAbove();
  }
  @HostBinding('class.mat-badge-below') get isBelowHost() {
    return !this.isAbove();
  }
  @HostBinding('class.mat-badge-before') get isBeforeHost() {
    return !this.isAfter();
  }
  @HostBinding('class.mat-badge-after') get isAfterHost() {
    return this.isAfter();
  }
  @HostBinding('class.mat-badge-small') get isSmallHost() {
    return this.size === 'small';
  }
  @HostBinding('class.mat-badge-medium') get isMediumHost() {
    return this.size === 'medium';
  }
  @HostBinding('class.mat-badge-large') get isLargeHost() {
    return this.size === 'large';
  }
  @HostBinding('class.mat-badge-hidden') get isHiddenHost() {
    return this.hidden || !this.content;
  }
  @HostBinding('class.mat-badge-disabled') get isDisabledHost() {
    return this.disabled;
  }

  @Input() set appBadge(value: string | number) {
    this.content = isNotNilOrBlank(value) ? value : EMPTY_CHAR;
  }
  get appBadge(): string | number {
    return this.content;
  }

  @Input() set appBadgeDisabled(value: boolean) {
    this.disabled = value;
  }
  get appBadgeDisabled(): boolean {
    return this.disabled;
  }
  @Input() set appBadgeSize(value: MatBadgeSize) {
    this.size = value;
  }
  get appBadgeSize(): MatBadgeSize {
    return this.size;
  }

  @Input() appBadgeColor: AppColors;
  @Input() appBadgeFill: MatBadgeFill;
  @Input() appBadgeMatIcon: string;
  @Input() appBadgeIcon: string;
  @Input() appBadgeHidden: BooleanInput;
  @Input() appBadgeOverlap: BooleanInput;
  @Input() appBadgePosition: MatBadgePosition;

  private _html: string;

  constructor(
    _ngZone: NgZone,
    protected el: ElementRef<HTMLElement>,
    _ariaDescriber: AriaDescriber,
    _renderer: Renderer2,
    @Optional() @Inject(ANIMATION_MODULE_TYPE) _animationMode?: string | undefined
  ) {
    super(_ngZone, el, _ariaDescriber, _renderer, _animationMode);
    this.content = EMPTY_CHAR; // Will be invisible
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    this.updateView();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.appBadgeColor ||
      changes.appBadgeFill ||
      changes.appBadgeMatIcon ||
      changes.appBadgeIcon ||
      changes.appBadgeHidden ||
      changes.appBadgeOverlap ||
      changes.appBadgePosition ||
      changes.appBadge ||
      changes.appBadgeSize ||
      changes.appBadgeDisabled
    ) {
      this.updateView();
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  private updateView() {
    const badge = this.getBadgeElement();
    if (!badge) return;

    // Fix color
    if (!this.appBadgeColor) this.appBadgeColor = 'primary';
    if (this.appBadgeColor === 'warn') this.appBadgeColor = 'warning';

    const size = SIZE_TO_PX[this.size];
    const fontSize = `var(--mat-badge-legacy-${this.size}-size-container-size,${size}px)`;
    let hidden = this.appBadgeHidden;
    if (!hidden) {
      let html = '';
      if (this.appBadgeMatIcon) {
        html = `<i class="material-icons" style="font-size: ${fontSize};">${this.appBadgeMatIcon}</i>`;
      } else if (this.appBadgeIcon) {
        html = `<ion-icon style="font-size: ${fontSize}; pointer-events: none;" name="${this.appBadgeIcon}"></ion-icon>`;
      } else if (isNotNilOrBlank(this.content) && this.content !== EMPTY_CHAR) {
        html = '' + this.content;
      } else {
        hidden = true;
      }
      if (!hidden && this._html !== html) {
        this._html = html;
        badge.innerHTML = html;
      }
    }

    // Update style
    // fill: 'clear'
    if (this.appBadgeFill === 'clear') {
      badge.style.color = `var(--ion-color-${this.appBadgeColor})`;
      badge.style.background = 'transparent';
    }
    // fill: 'solid' (default value)
    else {
      badge.style.background = `var(--ion-color-${this.appBadgeColor})`;
      badge.style.color = `var(--ion-color-${this.appBadgeColor}-contrast)`;
    }

    if (hidden) {
      badge.style.display = 'none';
    } else {
      if (badge.style.display !== 'inline-block') badge.style.display = 'inline-block';
      if (badge.style.position !== 'absolute') badge.style.position = 'absolute';

      /*
      // WORKAROUND to force position of the badge, when inside a <mat-button> (e.g. on a <mat-label>)
      const overlap = this.overlap;

      // Right/left position
      if (this.isAfter() ) {
        const parentWidth = badge.parentElement.getBoundingClientRect().width;
        if (parentWidth > 0) {
          const left = `${parentWidth - (overlap ? size / 2 : 0)}px`;
          if (badge.style.left !== left) badge.style.left = left;
          if (badge.style.right !== '') badge.style.right = '';
        }
        else {
          const right = `${-1 * (overlap ? size / 2 : size)}px`;
          if (badge.style.right !== right) badge.style.right = right;
          if (badge.style.left !== '') badge.style.left = '';
        }
      }

      // Top/bottom position
      if (this.isAbove()) {
        const parentHeight = badge.parentElement.getBoundingClientRect().height;
        if (parentHeight > 0) {
          const bottom = `${parentHeight - (overlap ? (size / 2) : size)}px`;
          if (badge.style.bottom !== bottom) badge.style.bottom = bottom;
          if (badge.style.top !== '') badge.style.top = '';
        }
        else {
          const top = `${-1 * (overlap ? (size / 2) : size)}px`;
          if (badge.style.top !== top) badge.style.top = top;
          if (badge.style.bottom !== '') badge.style.bottom = '';
        }
      }*/
    }
  }
}
