import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MatBadgeFill } from './badge.directive';
import { MatBadgeSize } from '@angular/material/badge';
import { AppColors } from '../../types';

@Component({
  selector: 'mat-badge-test',
  templateUrl: './badge.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatBadgeTestPage {
  userText = 'A custom string';
  matBadgeContent = '!';

  $icon = new BehaviorSubject<string>('alert');
  $size = new BehaviorSubject<MatBadgeSize>('small');
  $fill = new BehaviorSubject<MatBadgeFill>('solid');
  $color = new BehaviorSubject<AppColors>('danger');
  $hidden = new BehaviorSubject<boolean>(false);
  $overlap = new BehaviorSubject<boolean>(false);

  constructor(private cd: ChangeDetectorRef) {}

  onInputChanged(event: any) {
    this.userText = event.target.value;
    this.cd.markForCheck();
  }

  onMatBadgeContentChanged(event: any) {
    this.matBadgeContent = event.target.value;
    this.cd.markForCheck();
  }
}
