import { isNotNil, joinPropertiesPath } from '../../functions';
import { DEFAULT_JOIN_ARRAY_VALUES_SEPARATOR } from '../../constants';
import { DisplayFn } from '../../form/field.model';

export class MatAutocompleteFieldUtils {
  static createDisplayFn(displayAttributes: string[], multiple = false, propertySeparator?: string, itemSeparator?: string): DisplayFn {
    // if multiple (manage array)
    if (multiple) {
      return (value) =>
        Array.isArray(value)
          ? (value || []).map((obj) => obj && joinPropertiesPath(obj, displayAttributes)).join(itemSeparator || DEFAULT_JOIN_ARRAY_VALUES_SEPARATOR)
          : value && joinPropertiesPath(value, displayAttributes, propertySeparator);
    }
    // Join properties
    return (obj) => obj && joinPropertiesPath(obj, displayAttributes);
  }

  static defaultEquals(o1: any, o2: any) {
    return isNotNil(o1?.id) && o1.id === o2?.id;
  }
}
