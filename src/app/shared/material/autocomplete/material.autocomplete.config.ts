import { SuggestFn, SuggestService } from '../../services/entity-service.class';
import { MatAutocompleteFieldUtils } from './material.autocomplete.utils';
import { Observable } from 'rxjs';
import { DisplayFn, EqualsFn } from '../../form/field.model';

export declare interface MatAutocompleteFieldConfig<T = any, F = any> {
  attributes?: string[];
  suggestFn?: SuggestFn<T, F>;
  filter?: Partial<F>;
  items?: Observable<T[]> | T[];
  columnSizes?: (number | 'auto' | undefined)[];
  columnNames?: (string | undefined)[];
  multiple?: boolean;
  displayWith?: DisplayFn;
  displayWithSeparator?: string; // Use to split searchText before searching
  splitSearchText?: boolean; // Split searchText before searching? (will use the first value)
  trimSearchText?: boolean; // Trim searchText before searching?
  clearInvalidSearchTextOnBlur?: boolean; // Clear invalid text, on blur (false by default)
  equals?: EqualsFn; // Used by <mat-chips-field>
  showAllOnFocus?: boolean;
  showPanelOnFocus?: boolean;
  reloadItemsOnFocus?: boolean;
  mobile?: boolean;
  debounceTime?: number;
  suggestLengthThreshold?: number;
  highlightAccent?: boolean;
  applyImplicitValue?: boolean;

  /**
   * Should select input content (desktop only)
   */
  selectInputContentOnFocus?: boolean;

  /**
   * Add a delay (ms) before trying to select input content (desktop only)
   */
  selectInputContentOnFocusDelay?: number;

  /**
   * If true, any invalid value entered in the input field will be cleared when the input loses focus.
   * This helps in preventing invalid data to be displayed on the field
   */
  clearInvalidValueOnBlur?: boolean;

  /**
   * Determines whether the implicit value is preview in the input text field or not (desktop only).
   *
   * If not specified, the default behavior is to not preview implicit values (`false`).
   *
   * @type {boolean}
   */
  previewImplicitValue?: boolean;

  /**
   * Class to apply to the `div.mat-mdc-select-panel`
   */
  panelClass?: string;

  /**
   * Allow to fix panel width (e.g. '500px' or '100vw')
   */
  panelWidth?: string;

  /**
   * @deprecated use `panelClass` instead
   */
  class?: string;
}

export declare interface MatAutocompleteFieldAddOptions<T = any, F = any> extends Partial<MatAutocompleteFieldConfig<T, F>> {
  service?: SuggestService<T, F>;
}

export class MatAutocompleteConfigHolder {
  fields: {
    [key: string]: MatAutocompleteFieldConfig;
  } = {};

  getUserAttributes: (fieldName: string, defaultAttributes?: string[]) => string[];

  constructor(
    private options?: {
      getUserAttributes: (fieldName: string, defaultAttributes?: string[]) => string[];
    }
  ) {
    // Store the function from options (e.g. get from user settings)
    // or create a default function
    this.getUserAttributes = options?.getUserAttributes || ((fieldName, defaultAttributes) => defaultAttributes || ['label', 'name']);
  }

  add<T = any, F = any>(fieldName: string, options?: MatAutocompleteFieldAddOptions<T, F>): MatAutocompleteFieldConfig<T, F> {
    if (!fieldName) {
      throw new Error('Unable to add config, with name: ' + (fieldName || 'undefined'));
    }
    options = options || <MatAutocompleteFieldAddOptions>{};
    const suggestFn: SuggestFn<T, F> = options.suggestFn || (options.service && ((v, f) => options.service.suggest(v, f))) || undefined;
    const attributes = this.getUserAttributes(fieldName, options.attributes) || ['label', 'name'];
    // Keep original the function from options, when exists
    const attributesOrFn = attributes.map((a, index) => (typeof a === 'function' && options.attributes[index]) || a);
    const filter: Partial<F> = {
      searchAttribute: attributes.length === 1 ? attributes[0] : undefined,
      searchAttributes: attributes.length > 1 ? attributes : undefined,
      ...options.filter,
    };
    const displayWith = options.displayWith || MatAutocompleteFieldUtils.createDisplayFn(attributes, options.multiple);
    const equals = options.equals || MatAutocompleteFieldUtils.defaultEquals;

    const config: MatAutocompleteFieldConfig = {
      ...options,
      attributes: attributesOrFn,
      suggestFn,
      filter,
      displayWith,
      equals,
    };

    this.fields[fieldName] = config;
    return config;
  }

  get<T = any>(fieldName: string): MatAutocompleteFieldConfig<T> {
    if (!fieldName) {
      throw new Error('Unable to add config, with name: ' + (fieldName || 'undefined'));
    }
    return this.fields[fieldName] || (this.add(fieldName) as MatAutocompleteFieldConfig<T>);
  }
}
