import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { SharedFormArrayValidators, SharedValidators } from '../../../validator/validators';
import { MatAutocompleteField } from '../material.autocomplete';
import { isNil, isNotNil, sleep } from '../../../functions';
import { BehaviorSubject, Subscription, timer } from 'rxjs';
import { LoadResult } from '../../../services/entity-service.class';
import { suggestFromArray } from '../../../services';
import { MatAutocompleteConfigHolder } from '../material.autocomplete.config';
import { IonModal } from '@ionic/angular';

class EntityTestClass {
  id: number;
  label: string;
  name: string;
}

const FAKE_ENTITIES = [
  { id: 1, label: 'AAA', name: 'Item A', description: 'Very long description A', comments: 'Very very long comments. again for A' },
  { id: 2, label: 'BBB', name: 'Item B', description: 'Very long description B', comments: 'Very very long comments. again for B' },
  { id: 3, label: 'CCC', name: 'Item C', description: 'Very long description C', comments: 'Very very long comments. again for C' },
  { id: 4, label: 'DDD', name: 'Item D', description: 'Very long description D', comments: 'Very very long comments. again for D' },
  { id: 5, label: 'EEE', name: 'Item E', description: 'Very long description E', comments: 'Very very long comments. again for E' },
];

function deepCopy(values?: EntityTestClass[], size?: number): EntityTestClass[] {
  if (isNil(size)) {
    return (values || FAKE_ENTITIES).map((entity) => Object.assign({}, entity));
  }

  let result = [];
  let i = 1;
  while (result.length < size) {
    result = result.concat(
      (values || FAKE_ENTITIES).map((entity) => {
        const label = entity.label + i;
        const name = entity.name + ' #' + i;
        return { ...entity, id: i++, label, name };
      })
    );
  }
  return result.slice(0, size);
}

@Component({
  selector: 'app-autocomplete-test',
  templateUrl: './autocomplete.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutocompleteTestPage implements OnInit, AfterViewInit {
  private _items = deepCopy(FAKE_ENTITIES, 100);
  private _$items = new BehaviorSubject<EntityTestClass[]>(undefined);

  form: UntypedFormGroup;
  autocompleteFields = new MatAutocompleteConfigHolder();
  memoryHide = false;
  memoryMobile = true;
  memoryAutocompleteFieldName = 'entity-$items';
  memoryTimer: Subscription;

  mode: 'mobile' | 'desktop' | 'memory' | 'temp' = 'desktop';

  @ViewChild('farEntityField') farEntityField: MatAutocompleteField;
  @ViewChild(IonModal) ionModal: IonModal;

  constructor(
    protected formBuilder: UntypedFormBuilder,
    private cd: ChangeDetectorRef
  ) {
    this.form = formBuilder.group({
      emptyEntity: [null, SharedValidators.entity],
      entity: [null, SharedValidators.entity],
      missingEntity: [null, SharedValidators.entity],
      disableEntity: [null, SharedValidators.entity],
      entities: [null, SharedFormArrayValidators.requiredArrayMinLength(1)],
      farEntity: [null, SharedValidators.entity],
      withButton: [null, SharedValidators.entity],
    });

    this.form.get('disableEntity').disable();
  }

  ngOnInit() {
    this.mode = this.mode || 'mobile';

    // From suggest
    this.autocompleteFields.add('entity-suggestFn', {
      suggestFn: (value, filter) => this.suggest(value, filter),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // From items array
    this.autocompleteFields.add('entity-items', {
      items: this._items.slice(),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // From $items observable
    this.autocompleteFields.add('entity-$items', {
      items: this._$items,
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // From $items with filter
    this.autocompleteFields.add('entity-items-filter', {
      service: {
        suggest: (value, filter) => this.suggest(value, filter),
      },
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // From items
    this.autocompleteFields.add('entity-items-large', {
      items: FAKE_ENTITIES.slice(),
      attributes: ['label', 'name', 'description', 'comments'],
      displayWith: this.entityToString,
    });

    // Far entity, in the list
    this.autocompleteFields.add('entity-far', {
      suggestFn: (value, filter) => this.suggest(value, filter),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // For multiple value
    this.autocompleteFields.add('entities-items-filter', {
      service: {
        suggest: (value, filter) => this.suggest(value, filter),
      },
      attributes: ['label', 'name'],
      multiple: true,
    });

    // With button mode values
    this.autocompleteFields.add('entity-with-button', {
      service: {
        suggest: (value, filter) => this.suggest(value, filter),
      },
      attributes: ['label', 'name'],
    });
  }

  ngAfterViewInit() {
    if (this.farEntityField) {
      setTimeout(() => {
        if (this.farEntityField) {
          this.farEntityField.suggestLengthThreshold = 2;
          this.farEntityField.reloadItems();
        }
      }, 500);
    }

    setTimeout(() => this.loadData(), 1000);
  }

  // Load the form with data
  async loadData() {
    const data = {
      emptyEntity: null,

      entity: deepCopy(FAKE_ENTITIES)[0], // First

      farEntity: deepCopy(FAKE_ENTITIES)[2],

      // This item is NOT in the items list => i should be displayed anyway
      missingEntity: {
        id: -1,
        label: '??',
        name: 'Missing item',
      },

      disableEntity: deepCopy(FAKE_ENTITIES)[2],

      entities: deepCopy(FAKE_ENTITIES),

      withButton: deepCopy(FAKE_ENTITIES)[0],
    };

    this.form.setValue(data);

    // Load observables
    setTimeout(() => this.loadItems(), 1000);
  }

  async loadItems() {
    this._$items.next(this._items.slice());
  }

  entityToString(item: any) {
    return [(item && item.label) || undefined, (item && item.name) || undefined].filter(isNotNil).join(' - ');
  }

  entitiesToString(items: any[]) {
    return (items || []).map(this.entityToString).join(', ');
  }

  async suggest(value: any, filter?: any): Promise<LoadResult<EntityTestClass>> {
    if (isNotNil(value?.id)) return { data: [value] };

    filter = { offset: 0, size: 30, ...filter };
    const res = suggestFromArray(this._items, value, {
      ...filter,
      searchAttributes: ['label', 'name'],
    });

    // Simulate a size = 30
    const total = res.total || res.data.length;

    const nextOffset = filter.offset + res.data.length;
    if (nextOffset < total) {
      filter = { ...filter, offset: nextOffset };
      return {
        total,
        data: res.data,
        fetchMore: async (variables) => {
          await sleep(1000); // Simulate access to server
          return this.suggest(value, { ...filter, ...variables });
        },
      };
    }

    return res;
  }

  doSubmit(_) {
    console.debug('Validate form: ', this.form.value);
  }

  equals(o1: EntityTestClass, o2: EntityTestClass): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
      this.stopMemoryTimer();
    }
  }

  startMemoryTimer() {
    this.memoryTimer = timer(0, 50).subscribe(() => {
      this.memoryHide = !this.memoryHide;
      this.cd.markForCheck();
    });
  }

  stopMemoryTimer() {
    this.memoryTimer?.unsubscribe();
    this.memoryTimer = null;
    this.memoryHide = false;
  }

  updateFilter(autocompleteField: MatAutocompleteField, fieldName: string) {
    const filter: { searchAttribute?: string } = this.autocompleteFields.get(fieldName).filter || {};
    filter.searchAttribute = !filter || filter.searchAttribute !== 'name' ? 'name' : 'label';
    autocompleteField.reloadItems();
  }

  /* -- protected methods -- */

  stringify(value: any) {
    return JSON.stringify(value);
  }
}
