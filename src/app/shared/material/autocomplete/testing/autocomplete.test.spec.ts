import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppModule } from '../../../../app.module';
import { SharedMatAutocompleteModule } from '../material.autocomplete.module';
import { AutocompleteTestPage } from './autocomplete.test';
import { TranslateModule } from '@ngx-translate/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../../../shared.module';

describe('MatAutocompleteField', () => {
  let component: AutocompleteTestPage;
  let fixture: ComponentFixture<AutocompleteTestPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [TranslateModule.forRoot(), NoopAnimationsModule, SharedModule, SharedMatAutocompleteModule],
      declarations: [AutocompleteTestPage],
      providers: [AppModule],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
