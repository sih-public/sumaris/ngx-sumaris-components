import {
  AfterViewInit,
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  numberAttribute,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Renderer2,
  RendererStyleFlags2,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl, Validators } from '@angular/forms';
import { BehaviorSubject, isObservable, merge, Observable, Subject, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, startWith, switchMap, takeUntil, tap, throttleTime } from 'rxjs/operators';
import { FetchMoreFn, LoadResult, SuggestFn } from '../../services/entity-service.class';
import {
  changeCaseToUnderscore,
  findParentWithClass,
  isBlankString,
  isNil,
  isNilOrBlank,
  isNotNil,
  isNotNilBoolean,
  isNotNilObject,
  isNotNilOrBlank,
  isNotNilString,
  toNumber,
} from '../../functions';
import { focusInput, InputElement, selectInputContent, selectInputRange } from '../../inputs';
import { firstNotNilPromise } from '../../observables';
import { AppFloatLabelType, DisplayFn, EqualsFn } from '../../form/field.model';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { fromScrollEndEvent } from '../../events';
import { IonSearchbar, ModalController } from '@ionic/angular';
import { isMobile } from '../../platforms';
import { suggestFromArray } from '../../services';
import { SearchbarChangeEventDetail } from '@ionic/core/dist/types/components/searchbar/searchbar-interface';
import { MatAutocompleteFieldUtils } from './material.autocomplete.utils';
import { MatAutocompleteFieldConfig } from './material.autocomplete.config';
import { DOCUMENT } from '@angular/common';

import { DEFAULT_JOIN_PROPERTIES_SEPARATOR } from '../../constants';
import { RegExpUtils } from '../../regexps';

const noop = () => {};
export interface MatAutocompleteFieldSelectChange {
  source: MatSelect | MatAutocomplete;
  value: any;
}

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'mat-autocomplete-field',
  styleUrls: ['./material.autocomplete.scss'],
  templateUrl: './material.autocomplete.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MatAutocompleteField),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatAutocompleteField implements OnInit, AfterViewInit, OnDestroy, InputElement, ControlValueAccessor {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;

  private _onFetchMoreCallback: FetchMoreFn<LoadResult<any>>;
  private _implicitValue: any;
  private _subscription = new Subscription();
  private _itemsSubscription: Subscription;
  private _openedSubscription: Subscription;
  private _$filter = new BehaviorSubject<any>(undefined);
  private _itemCount: number;
  private _suggestExecutionId: number = 0; // Identifier of the suggest job
  private _reload$ = new Subject<any>();
  private _destroy$ = new Subject<void>();

  protected _readonly = false;
  protected _tabindex: number;
  protected _$inputItems = new BehaviorSubject<any[]>(undefined);
  protected _$filteredItems = new BehaviorSubject<any[]>(undefined);
  protected _selectedItem: any;
  protected _displayValue = '';
  protected _moreItemsCount: number;
  protected _fetchMore$ = new EventEmitter<Event>();
  protected _defaultPanelWidth: string = null;
  protected _focused = false;

  @Input() equals: EqualsFn;
  @Input() logPrefix = '[mat-autocomplete] ';
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string = null;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() label: string = null;
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input() placeholder: string;
  @Input() suggestFn: SuggestFn<any, any>;
  @Input({ transform: booleanAttribute }) required = false;
  @Input({ transform: booleanAttribute }) hideRequiredMarker = false;
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input({ transform: numberAttribute }) debounceTime: number = null;
  @Input() displaySeparator: string | null;
  @Input() displayWith: DisplayFn | null;
  @Input() displayAttributes: string[];
  @Input() displayColumnSizes: (number | 'auto' | undefined)[];
  @Input() displayColumnNames: string[];
  @Input({ transform: booleanAttribute }) highlightAccent: boolean;
  @Input({ transform: booleanAttribute }) showAllOnFocus: boolean;
  @Input({ transform: booleanAttribute }) showPanelOnFocus: boolean;
  @Input({ transform: booleanAttribute }) reloadItemsOnFocus: boolean;
  @Input({ transform: booleanAttribute }) clearInvalidValueOnBlur: boolean;
  @Input({ transform: booleanAttribute }) autofocus = false;
  @Input() config: MatAutocompleteFieldConfig;
  @Input() i18nPrefix = 'REFERENTIAL.';
  @Input() noResultMessage = 'COMMON.NO_RESULT';
  @Input() panelClass: string;
  @Input() panelWidth: string;
  @Input({ transform: booleanAttribute }) disableRipple = false;
  @Input() matAutocompletePosition: 'auto' | 'above' | 'below';
  @Input({ transform: booleanAttribute }) multiple: boolean;
  @Input() fetchMoreThreshold = '15%';
  @Input({ transform: numberAttribute }) suggestLengthThreshold: number = null;
  @Input({ transform: booleanAttribute }) showLoadingSpinner = true;
  @Input({ transform: booleanAttribute }) debug = false;
  @Input({ transform: booleanAttribute }) showSearchBar = true;
  @Input({ transform: booleanAttribute }) stickySearchBar = false;
  @Input() applyImplicitValue: boolean | ((value: any) => boolean);
  @Input() dropButtonTitle: string;
  @Input() clearButtonTitle: string;
  @Input({ transform: booleanAttribute }) trimSearchText: boolean;
  @Input({ transform: booleanAttribute }) splitSearchText: boolean;
  @Input({ transform: booleanAttribute }) selectInputContentOnFocus: boolean;
  @Input({ transform: numberAttribute }) selectInputContentOnFocusDelay: number;
  @Input({ transform: booleanAttribute }) previewImplicitValue: boolean;

  /**
   * @deprecated Use panelClass instead
   */
  @Input({ alias: 'class' }) set classList(value: string) {
    this.panelClass = value;
  }

  get classList(): string {
    return this.panelClass;
  }

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('click') clicked = new EventEmitter<MouseEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('blur') blurred = new EventEmitter<FocusEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('focus') focused = new EventEmitter<FocusEvent>();
  @Output() dropButtonClick = new EventEmitter<Event>(true);
  @Output('keydown.escape') keydownEscape = new EventEmitter<Event>();
  @Output('keydown.backspace') keydownBackspace = new EventEmitter<Event>();
  @Output('keyup.enter') keyupEnter = new EventEmitter<Event>();

  @Output('selectionChange') selectionChange = new EventEmitter<MatAutocompleteFieldSelectChange>();

  @ViewChild('matSelect') matSelect: MatSelect;
  @ViewChild('ionSearchbar') ionSearchbar: IonSearchbar;
  @ViewChild('matInputText') matInputText: ElementRef;
  @ViewChild(MatAutocomplete) autocomplete: MatAutocomplete;
  @ViewChild(MatAutocompleteTrigger) autocompleteTrigger: MatAutocompleteTrigger;

  constructor(
    protected injector: Injector,
    protected el: ElementRef<HTMLElement>,
    protected cd: ChangeDetectorRef,
    protected modalController: ModalController,
    protected renderer: Renderer2,
    @Optional() private formGroupDir: FormGroupDirective
  ) {}

  get itemCount(): number {
    return this._itemCount;
  }

  get canFetchMore(): boolean {
    return this._onFetchMoreCallback && this._moreItemsCount > 0;
  }

  @Input() set filter(value: any) {
    if (value !== this._$filter.value) {
      // DEBUG
      //console.debug(this.logPrefix + " Setting filter:", value);
      this._$filter.next(value);
    }
  }

  get filter(): any {
    return this._$filter.value;
  }

  @Input({ transform: booleanAttribute }) set readonly(value: boolean) {
    this._readonly = value;
    this.markForCheck();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  @Input() set tabindex(value: number) {
    this._tabindex = value;
    this.markForCheck();
  }

  get tabindex(): number {
    return this._tabindex;
  }

  @Input() set items(value: Observable<any[]> | any[]) {
    // Remove previous subscription on items, (if exits)
    if (this._itemsSubscription) {
      console.warn(this.logPrefix + ' Items received twice !');
      this._subscription.remove(this._itemsSubscription);
      this._itemsSubscription.unsubscribe();
    }

    if (isObservable(value)) {
      this._itemsSubscription = value.subscribe((v) => this._$inputItems.next(v));
      this._subscription.add(this._itemsSubscription);
    } else {
      if (value !== this._$inputItems.value) {
        this._$inputItems.next(value as any[]);
      }
    }
  }

  get items(): Observable<any[]> | any[] {
    return this._$inputItems;
  }

  get value(): any {
    return this.formControl.value;
  }

  get disabled(): boolean {
    return this._readonly || this.formControl.disabled;
  }

  get enabled(): boolean {
    return !this._readonly && this.formControl.enabled;
  }

  get loading(): boolean {
    return isNil(this._$filteredItems.value);
  }

  ngOnInit() {
    this.formControl =
      this.formControl ?? (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-autocomplete-field>.");

    // Configuration from config object
    if (this.config) {
      this.suggestFn = this.suggestFn ?? this.config.suggestFn;
      if (!this.suggestFn && this.config.items) {
        this.items = this.config.items;
      }
      this.filter = this.filter ?? this.config.filter;
      this.displayAttributes = this.displayAttributes ?? this.config.attributes;
      this.displayColumnSizes = this.displayColumnSizes ?? this.config.columnSizes;
      this.displayColumnNames = this.displayColumnNames ?? this.config.columnNames;
      this.displaySeparator = this.displaySeparator ?? this.config.displayWithSeparator;
      this.displayWith = this.displayWith ?? this.config.displayWith;
      this.splitSearchText = this.splitSearchText ?? this.config.splitSearchText;
      this.trimSearchText = this.trimSearchText ?? this.config.trimSearchText;
      this.equals = this.equals ?? this.config.equals;
      this.multiple = this.multiple ?? this.config.multiple;
      this.mobile = this.mobile ?? this.config.mobile;
      this.suggestLengthThreshold = this.suggestLengthThreshold ?? this.config.suggestLengthThreshold;
      this.showAllOnFocus = this.showAllOnFocus ?? this.config.showAllOnFocus;
      this.showPanelOnFocus = this.showPanelOnFocus ?? this.config.showPanelOnFocus;
      this.reloadItemsOnFocus = this.reloadItemsOnFocus ?? this.config.reloadItemsOnFocus;
      this.panelClass = this.panelClass ?? this.config.panelClass ?? this.config.class;
      this.panelWidth = this.panelWidth ?? this.config.panelWidth;
      this.debounceTime = this.debounceTime ?? this.config.debounceTime;
      this.highlightAccent = this.highlightAccent ?? this.config.highlightAccent;
      this.applyImplicitValue = this.applyImplicitValue ?? this.config.applyImplicitValue;
      this.selectInputContentOnFocus = this.selectInputContentOnFocus ?? this.config.selectInputContentOnFocus;
      this.selectInputContentOnFocusDelay = this.selectInputContentOnFocusDelay ?? this.config.selectInputContentOnFocusDelay;
      this.clearInvalidValueOnBlur = this.clearInvalidValueOnBlur ?? this.config.clearInvalidValueOnBlur;
      this.previewImplicitValue = this.previewImplicitValue ?? this.config.previewImplicitValue;
    }

    // Default values
    this.required = isNotNilBoolean(this.required)
      ? this.required
      : this.formControl.validator === Validators.required || isBlankString(this.required);
    this.multiple = this.multiple ?? false;
    this.mobile = this.mobile ?? isMobile(window);
    this.displayAttributes = this.displayAttributes ?? this.filter?.attributes ?? ['label', 'name'];
    this.displaySeparator = this.displaySeparator ?? DEFAULT_JOIN_PROPERTIES_SEPARATOR;
    this.displayWith = this.displayWith || MatAutocompleteFieldUtils.createDisplayFn(this.displayAttributes, this.multiple, this.displaySeparator);
    this.splitSearchText = this.splitSearchText ?? true;
    this.trimSearchText = this.trimSearchText ?? !this.mobile; // True by default on desktop, but false in mobile (not need in the searchbar)
    this.equals = this.equals || MatAutocompleteFieldUtils.defaultEquals;

    this.displayColumnSizes =
      this.displayColumnSizes ||
      // if only column: auto size
      (this.displayAttributes.length === 1 && [undefined]) ||
      // Computed column size:
      // - if label then col size = 2
      // - if rankOrder then col size = 1
      // - ese => auto size
      this.displayAttributes.map((attr) => attr && (attr.endsWith('label') ? 3 : attr.endsWith('rankOrder') ? 2 : undefined));
    this.displayColumnNames = this.displayAttributes.map(
      (attr, index) => (this.displayColumnNames && this.displayColumnNames[index]) || this.i18nPrefix + changeCaseToUnderscore(attr).toUpperCase()
    );

    // No more need, because setting the panelWidth will force a width, instead of a min-width
    /*const panelClasses = this.panelClass?.split(/\s+/);
    this.panelWidth =
      this.panelWidth ??
      (panelClasses &&
        ((panelClasses.includes('min-width-medium') && '300px') ||
          (panelClasses.includes('min-width-large') && '400px') ||
          (panelClasses.includes('min-width-xlarge') && '450px') ||
          (panelClasses.includes('min-width-80vw') && '80vw') ||
          ((panelClasses.includes('full-width') || panelClasses.includes('full-size')) && '100vw')));*/

    this.suggestLengthThreshold = this.suggestLengthThreshold ?? 0;
    this.showAllOnFocus = this.showAllOnFocus ?? (this.mobile && this.suggestLengthThreshold <= 0);
    this.showPanelOnFocus = this.showPanelOnFocus ?? true;
    this.reloadItemsOnFocus = this.reloadItemsOnFocus ?? false;
    this.debounceTime = this.debounceTime ?? (this.mobile ? 650 : 250);
    this.highlightAccent = this.highlightAccent ?? false;
    this.matAutocompletePosition = this.matAutocompletePosition ?? 'auto';
    this.applyImplicitValue = this.applyImplicitValue ?? !this.mobile;
    this.selectInputContentOnFocus = this.selectInputContentOnFocus ?? false;
    this.selectInputContentOnFocusDelay = this.selectInputContentOnFocusDelay ?? 0;
    this.clearInvalidValueOnBlur = this.clearInvalidValueOnBlur ?? false;
    this.previewImplicitValue = this.previewImplicitValue ?? false;

    // No suggestFn: filter on the given items
    if (!this.suggestFn) {
      const suggestFromArrayFn: SuggestFn<any, any> = async (value, filter) =>
        // DEBUG
        //if (this.debug) console.debug(this.logPrefix + ' Calling suggestFromArray with value=', value);

        suggestFromArray(this._$inputItems.value, value, {
          searchAttributes: this.displayAttributes,
          ...filter,
        });

      // Wait (once) that items are loaded, then call suggest from array fn
      this.suggestFn = async (value, filter) => {
        if (isNil(this._$inputItems.value)) {
          // DEBUG
          //if (this.debug) console.debug(this.logPrefix + " Waiting items to be set...");

          await firstNotNilPromise(this._$inputItems, { stop: this._destroy$ });

          // DEBUG
          //if (this.debug) console.debug(this.logPrefix + " Received items:", this.inputItems$.value);
        }
        this.suggestFn = suggestFromArrayFn;
        return this.suggestFn(value, filter); // Loop
      };
    }

    // Send value to suggest when :
    // - is nil and '*', and no min length threshold
    // - is not nil and not a string type (e.g. object)
    // - is not nil and is a string, and has enough character
    const thresholdFilterFn = (value: any) =>
      this.suggestLengthThreshold <= 0 ||
      (isNotNil(value) && (!(typeof value === 'string') || (value !== '*' && value.trim().length >= this.suggestLengthThreshold)));

    const filteredItemsChanges = merge(
      // Reload event (no distinctUntilChanged() pipe, to force reload even if same value)
      this._reload$.pipe(
        filter(() => this.enabled),
        filter(thresholdFilterFn),
        tap(() => this.showLoadingSpinner && this.markAsLoading())
      ),

      // Search text (no distinctUntilChanged() because of the debounceTime() pipe)
      this.formControl.valueChanges.pipe(
        filter((value) => this.enabled && isNotNilString(value)),
        // Mark as loading (to show loading spinner) - as soon as possible, and before debounceTime
        tap(() => this.showLoadingSpinner && this.markAsLoading()),
        // Filter if not enough character
        filter((value) => {
          if ((value?.length || 0) < this.suggestLengthThreshold) {
            // Update display value, to enable the message "not enough characters"
            this._displayValue = value;
            return false;
          }
          return true;
        }),
        // Wait a debounce time
        debounceTime(this.debounceTime)
        // DEBUG
        //,tap((value) => this.debug && console.debug(this.logPrefix + ' Search text changes:', value))
      ),

      // Object set: use it (no distinctUntilChanged() pipe need)
      this.formControl.valueChanges.pipe(
        startWith<any>(this.formControl.value),
        filter((value) => isNotNilObject(value)),
        tap((value) => {
          // DEBUG
          //if (this.debug) console.debug(this.logPrefix + ' Received control value changes:', value);

          // Update the display value (if need)
          const displayValue = this.displayWith(value) || '';
          if (this._displayValue !== displayValue) {
            this._displayValue = displayValue;

            // Apply to the input[type=text] field
            this.setInputTextValue(this._displayValue);
            this.markForCheck();
          }
        }),

        // If multiple: do not recompute items (only for the first time, when loading)
        // when formControl value changes
        filter(() => this.loading || (!this.multiple && !this.showAllOnFocus))
      ),

      // Merge some observables, with a common distinctUntilChanged() pipe
      merge(
        // On dropdown button click (no debounceTime)
        this.dropButtonClick.pipe(filter((event) => !event || !event.defaultPrevented)),
        // Focus or click => Load all (no debounceTime)
        merge(this.focused, this.clicked)
          // Avoid too many call, e.g. when focused and clicked
          .pipe(throttleTime(100)),
        // DEBUG
        //.pipe(tap((_) => this.debug && console.debug(this.logPrefix + ' Received focused or clicked event'))),
        // Input items or filter changes
        merge(this._$inputItems, this._$filter).pipe(
          filter(() => !this.loading)
          // DEBUG
          //tap((_) => this.debug && console.debug(this.logPrefix + ' Received $inputItems or filter event'))
        )
      ).pipe(
        filter(() => this.enabled),
        map(() => (this.multiple || this.showAllOnFocus ? '*' : this.formControl.value)),
        // Distinguish changes
        distinctUntilChanged((v1, v2) =>
          // If loading (not loaded yet), simulate that value changed to force load
          this.loading ? false : v1 === v2 || this.equals(v1, v2)
        ),
        // Make sure there is enough characters
        filter(thresholdFilterFn),
        // DEBUG
        tap((value) => this.debug && console.debug(this.logPrefix + ' Received update event: ', value))
      )
    ).pipe(
      // Suggest values
      mergeMap((value) => this.suggest(value, this.filter)),

      // Ignore undefined result (e.g. when a newer suggest call is running)
      filter(isNotNil),

      // DEBUG
      //tap(items => this.debug && console.debug(this.logPrefix + ' Received from suggest: ', items)),

      // Store implicit value (will use it onBlur if not other value selected)
      tap((items) => !(this.mobile || this.multiple) && this.updateImplicitValue(items))
    );

    // Fetch more events
    const fetchMoreItemsChanges = this._fetchMore$.pipe(
      tap((event) => event?.preventDefault()), // Avoid to close the mat-select
      filter(() => this.canFetchMore),
      mergeMap(() => this.fetchMore()),

      // DEBUG
      //tap(moreItems => console.debug(this.logPrefix + ' Received from fetch More: ', moreItems)),

      // Concat to existing items
      map((moreItems) => (this._$filteredItems.value || []).concat(moreItems))
    );

    // Update filtered items
    this._subscription.add(
      merge(
        this._$inputItems,

        // Update items events
        filteredItemsChanges,

        // Fetch more events
        fetchMoreItemsChanges
      ).subscribe((items) => {
        // Make sure control value is inside (mat-select)
        // (otherwise current value will visually disappear)
        if (items) {
          const value = this.formControl.value;
          this._selectedItem = null;

          // If form value is missing: append to list
          if (isNotNil(value)) {
            if (this.multiple && Array.isArray(value)) {
              items = items.concat(value.filter((v) => items.findIndex((item) => this.equals(item, v)) === -1));
            } else {
              if (typeof value === 'object') {
                const index = items.findIndex((item) => this.equals(item, value));
                if (index === -1) {
                  // Append current value to the list, if missing (mat-select only)
                  if (this.mobile || this.multiple) {
                    items = items.concat(value);
                  }
                } else {
                  this._selectedItem = items[index];
                }
              }
            }
          }
        }

        // DEBUG
        //if (this.debug) console.debug(this.logPrefix + ' Received new filtered items', items);

        // Emit items
        this._$filteredItems.next(items);
      })
    );

    // Applying implicit value, on blur
    this._subscription.add(
      this.blurred
        .pipe(
          // Skip if multiple (no implicit value) or disable
          filter(() => !this.multiple && !this.mobile && this.enabled),
          // DEBUG
          //,tap(_ => this.debug && console.debug(this.logPrefix + ' Received blurred event')),
          map(() => this.formControl.value)
        )
        .subscribe((value) => {
          let invalidateFilteredItems = false;
          let moveCaretToBeginning = false;

          // DEBUG
          //if (this.debug) console.debug(this.logPrefix + ' Applying implicit value on blur', value);

          // When leave component without object, use implicit value if :
          // - an explicit value
          // - field is not empty (user fill something)
          // - OR field empty but is required
          if (isNotNil(this._implicitValue) && ((this.required && isNilOrBlank(value)) || (isNotNilOrBlank(value) && typeof value !== 'object'))) {
            this.writeValue(this._implicitValue);
            this.formControl.markAsPending({ emitEvent: false, onlySelf: true });
            this.formControl.updateValueAndValidity({ emitEvent: false, onlySelf: true });
            invalidateFilteredItems = this.showAllOnFocus || this.reloadItemsOnFocus;
            moveCaretToBeginning = true; // Move caret to the beginning (fix issue IMAGINE-469)
          }
          // No value and no implicit value
          else if (isNilOrBlank(value)) {
            this.writeValue(null);
            this.formControl.markAsPending({ emitEvent: false, onlySelf: true });
            this.formControl.updateValueAndValidity({ emitEvent: false, onlySelf: true });
          }
          // Has value, but no implicit value
          else {
            // Value is a valid object
            if (isNotNilObject(value)) {
              invalidateFilteredItems = this.showAllOnFocus || this.reloadItemsOnFocus;
              moveCaretToBeginning = true; // Move caret to the beginning (fix issue IMAGINE-469)
            }
            // Invalid valid value: clear search text
            else if (this.clearInvalidValueOnBlur && typeof value === 'string') {
              this.writeValue(null);
              this.formControl.markAsPending({ emitEvent: false, onlySelf: true });
              this.formControl.updateValueAndValidity({ emitEvent: false, onlySelf: true });
              invalidateFilteredItems = true;
            }
          }

          this._implicitValue = null; // reset the implicit value
          this.closePanel(); // Close panel

          // Invalidate loaded items
          if (invalidateFilteredItems) {
            this.markAsLoading(); // Should force next focus to reload items
          }

          // Move caret to the beginning (fix issue IMAGINE-469)
          if (moveCaretToBeginning) {
            // DEBUG
            //if (this.debug) console.debug(this.logPrefix + ' Moving caret to the beginning, on blur');
            selectInputRange(this.matInputText?.nativeElement, 0, 0);
          }
        })
    );

    // Listen form control status change
    this._subscription.add(
      this.formControl.statusChanges
        .pipe(distinctUntilChanged())
        // Update component state
        .subscribe(() => this.checkIfTouched())
    );

    this._subscription.add(
      this.keydownEscape.subscribe((event) => {
        if (this.isOpen) {
          // Avoid form or page to manage escape event
          event?.stopPropagation();
          event?.preventDefault();

          // DEBUG
          //console.debug(this.logPrefix + " Closing the panel (keydown.escape)");

          this.closePanel();
        }
      })
    );

    // Select input content, on focus or press enter
    if (this.selectInputContentOnFocus && !this.mobile && !this.multiple) {
      this._subscription.add(
        merge(
          this.focused.pipe(
            filter((event) => !event.defaultPrevented)
            //tap((event) => event.preventDefault())
          ),
          this.keyupEnter.pipe(filter(() => isNotNilObject(this.formControl.value)))
        )
          .pipe(debounceTime(this.selectInputContentOnFocusDelay))
          .subscribe(() => {
            if (this.debug) console.debug(this.logPrefix + ' Selecting input content (focused)');
            selectInputContent(this.matInputText?.nativeElement);
          })
      );
    }

    if (this.reloadItemsOnFocus) {
      this._subscription.add(
        this.focused.pipe(filter((event) => !this.loading && !event.defaultPrevented)).subscribe(() => {
          if (this.debug) console.debug(this.logPrefix + ' Reloading items (focused)');
          this.reloadItems();
        })
      );
    }
  }

  ngAfterViewInit() {
    this._fixMatSelectPositionAndSearchbar();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
    this._subscription.unsubscribe();
    this._itemsSubscription?.unsubscribe();
    this._openedSubscription?.unsubscribe();
    this._$inputItems.complete();
    this._$inputItems.unsubscribe();
    this._$filteredItems.complete();
    this._$filteredItems.unsubscribe();
    this._reload$.complete();
    this._$filter.complete();

    this.dropButtonClick.complete();
    this.clicked.complete();
    this.blurred.complete();
    this.focused.complete();
    this.keydownEscape.complete();
    this.keyupEnter.complete();
    this._fetchMore$.complete();

    this._implicitValue = undefined;
    this.formControl = null;
    this.displayWith = null;
    this.equals = null;
    this.suggestFn = null;
    this._$filteredItems = null;
    this.config = null;
    this.panelClass = null;

    this._onChangeCallback = null;
    this._onTouchedCallback = null;
    this._onFetchMoreCallback = null;

    // Destroy the 'autocomplete' instance, to avoid memory leak
    this.autocomplete?.ngOnDestroy();
  }

  /**
   * Allow to reload content. Useful when filter has been changed but not detected
   */
  reloadItems(value?: any) {
    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + " Asking to reloadItems()");
    // Force a refresh
    this._reload$.next(value || (this.showAllOnFocus && this.suggestLengthThreshold <= 0 ? '*' : this.formControl.value));

    // Clear the search bar (if hidden)
    if (this.mobile && !this.matSelect.panelOpen && this.ionSearchbar) {
      this.ionSearchbar.value = null;
    }
  }

  writeValue(value: any): void {
    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Writing value: ', value);

    if (value !== this.formControl.value) {
      this.formControl.setValue(value, { emitEvent: false });
      this._displayValue = this.displayWith(value);

      this._onChangeCallback(value);

      // Check if need to update controls
      this.checkIfTouched();
    }
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled && this.autocomplete?.isOpen) {
      this.closePanel();
    }
    this.cd.markForCheck();
  }

  focus() {
    if (this.matSelect) {
      this.matSelect.focus();
    } else {
      focusInput(this.matInputText);
    }
  }

  filterInputTextFocusEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) {
      if (this.debug) console.debug(this.logPrefix + 'calling filterInputTextFocusEvent - event.defaultPrevented');
      return false;
    }

    // Ignore event from mat-option
    if (event.relatedTarget instanceof HTMLElement && event.relatedTarget.tagName === 'MAT-OPTION') {
      event.preventDefault();
      if (event.stopPropagation) event.stopPropagation();
      event.returnValue = false;

      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + ' Cancelling focus event');

      return false;
    }

    // Already focused - fix issue sumaris-app#837 (2 panels opened at the same time)
    if (this._focused) return false;

    this._focused = true;

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Select input content');
    const hasContent = isNotNilOrBlank((event.target as any)?.value);

    // If combo is empty, or if has content but should force to show panel on focus
    if (!hasContent || this.showPanelOnFocus || this.showAllOnFocus) {
      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + ' Emit focus event');

      // Need a delay to prevent early focus event, just after a blur event (see issue sumaris-app#837 - 2 panels opened)
      setTimeout(() => {
        if (!this._focused) return; // Skip if blur event received

        this.focused.emit(event);
      }, 10);

      return true;
    }
    return false;
  }

  protected filterInputTextBlurEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) return;

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' filterInputTextBlurEvent', event);

    // Ignore event from mat-option
    if (
      (event.relatedTarget instanceof HTMLElement && event.relatedTarget.tagName === 'MAT-OPTION') ||
      // Or autocomplete panel is open because event.relatedTarget is null (cf. https://github.com/angular/components/issues/24182)
      this.autocomplete?.isOpen
    ) {
      event.preventDefault();
      if (event.stopPropagation) event.stopPropagation();
      event.returnValue = false;

      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + " Cancelling blur event");

      return;
    }

    // Already blurred: skip
    if (!this._focused) return;

    this._focused = false;

    // Need a delay to prevent early blur event without triggering formChange event
    setTimeout(() => {
      if (this._focused) return; // Skip if focus again

      this.blurred.emit(event);
    }, 100);
  }

  protected ionSearchBarChanged(event: Event) {
    if (!event || event.defaultPrevented || !this.matSelect.panelOpen) return;
    event.preventDefault();
    event.stopPropagation();

    // Get the value to search
    const value = (event as CustomEvent<SearchbarChangeEventDetail>)?.detail.value;

    // DEBUG
    //console.debug(this.logPrefix + " Received <ion-searchbar> changes", value);

    // Update the display value (in the visible form control)
    if (!this.multiple) {
      this._displayValue = value;
    }

    // Mark component as loading
    this.markAsLoading();

    // Reload items
    this.reloadItems(value);

    // TODO Find a way to avoid mat-select to give focus to an option, when typing
    /*if (this.multiple) {
      this.matSelect._elementRef;
    }*/
  }

  filterMatSelectFocusEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) return;
    // DEBUG
    //console.debug(this.logPrefix + ' Received <mat-select> focus event', event);

    // Emit event
    this.focused.emit(event);
  }

  filterMatSelectBlurEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) return;

    // DEBUG
    // console.debug(this.logPrefix + " Received <mat-select> blur event", event);

    if (
      event.relatedTarget instanceof HTMLElement &&
      // Ignore event from mat-option
      (event.relatedTarget.tagName === 'MAT-OPTION' ||
        (event.relatedTarget.tagName === 'DIV' && event.relatedTarget.classList.contains('mat-mdc-select-panel')) || // New MDC tag and class name (equivalent to event.relatedTarget.tagName === 'MAT-OPTION')
        (event.relatedTarget.tagName === 'INPUT' && event.relatedTarget.classList.contains('searchbar-input')))
    ) {
      event.preventDefault();
      if (event.stopPropagation) event.stopPropagation();
      event.returnValue = false;
      // DEBUG
      // console.debug(this.logPrefix + " Cancelling <mat-select> blur event");
      return false;
    }

    if (this._focused) {
      this._focused = false;

      this.blurred.emit(event);
    }

    return true;
  }

  clearValue(event?: Event) {
    this.closePanel();
    this.writeValue(null);
    event?.stopPropagation();
    if (this.ionSearchbar) {
      this.ionSearchbar.value = null;
    }
    this.markAsLoading();
  }

  _initAutocompleteInfiniteScroll(threshold?: string) {
    if (!this.autocomplete) return; // Skip

    // DEBUG
    //console.debug(this.logPrefix + ' Init autocomplete scroll listener');

    if (this._openedSubscription) {
      this._openedSubscription.unsubscribe();
      this._subscription.remove(this._openedSubscription);
    }

    const opened$: Observable<any> = this.autocomplete._isOpen ? timer(0) : this.autocomplete.opened.pipe(debounceTime(200));
    const scrollEnd$ = opened$.pipe(
      map(() => this.getAutocompletePanel()),
      filter(isNotNil),
      switchMap((panel) => fromScrollEndEvent(panel, threshold)),
      takeUntil(this.autocomplete.closed)
    );

    // Trigger fetch more, end end scroll reached
    this._openedSubscription = scrollEnd$.subscribe(() => this._fetchMore$.emit());

    // Register subscription
    this._subscription.add(this._openedSubscription);
  }

  _initMatSelectInfiniteScroll(threshold?: string) {
    if (!this.matSelect) return; // Skip

    // DEBUG
    //console.debug(this.logPrefix + ' Init mat-select scroll listener');

    if (this._openedSubscription) {
      this._openedSubscription.unsubscribe();
      this._subscription.remove(this._openedSubscription);
    }

    const opened$: Observable<any> = this.matSelect.panelOpen ? timer(0) : this.matSelect._openedStream.pipe(debounceTime(200));
    const scrollEnd$ = opened$.pipe(
      map(() => this.getMatSelectPanel()),
      filter(isNotNil),
      switchMap((panel) => fromScrollEndEvent(panel, threshold)),
      takeUntil(this.matSelect._closedStream)
    );

    // Trigger fetch more, end end scroll reached
    this._openedSubscription = scrollEnd$.pipe(filter(() => this._moreItemsCount > 0)).subscribe(() => this._fetchMore$.emit());

    // Register subscription
    this._subscription.add(this._openedSubscription);
  }

  get isOpen(): boolean {
    return this.autocomplete?.isOpen || this.matSelect?.panelOpen || false;
  }

  closePanel() {
    if (this.autocomplete?.isOpen) {
      this.autocompleteTrigger?.closePanel();
    } else if (this.matSelect?.panelOpen) {
      this.matSelect?.close();
    }
  }

  /* -- protected method -- */

  protected markAsLoading() {
    if (this._$filteredItems.value) {
      if (this.debug) console.debug(this.logPrefix + ' Marking as loading');

      this._$filteredItems.next(undefined);
      this._itemCount = undefined;
      this._implicitValue = undefined;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
      this._selectedItem = null;
      this.markForCheck();
    }
  }

  protected onSelectionChange(event: MatSelectChange) {
    this.selectionChange.emit({ source: event.source, value: event.value });
  }

  protected onOptionSelected(event: MatAutocompleteSelectedEvent) {
    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Option selected', event);
    this.selectionChange.emit({ source: event.source, value: event.option.value });
  }

  protected toggleShowPanel(event: Event) {
    if (this.isOpen) {
      event?.preventDefault();
      event?.stopPropagation();
      this.closePanel();
    } else {
      this.dropButtonClick.emit(event);
    }
  }

  /* -- private method -- */

  private async suggest(value: any, filter: any | undefined): Promise<any[] | undefined> {
    // Compute a new execution id
    const id = ++this._suggestExecutionId;

    // Split string value, if allow (.e.g 'AAA - Item AAA' => 'AAA')
    if (!this.mobile && typeof value === 'string') {
      // Split search text (and keep only the first part)
      if (this.splitSearchText && this.displaySeparator) {
        const firstSeparatorIndex = value.indexOf(this.displaySeparator);
        if (firstSeparatorIndex !== -1) {
          value = value.substring(0, firstSeparatorIndex);
        }
      }

      // Trim search text
      if (this.trimSearchText) {
        value = value.trim();
      }
    }

    // Call suggestion function
    let res = await this.suggestFn(value, filter);

    // Ignore the current result, if a newer execution has been launched
    if (id !== this._suggestExecutionId) return undefined;

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Filtered items by suggestFn:', value, res);

    if (!res) {
      this._itemCount = 0;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
      res = [];
    } else if (Array.isArray(res)) {
      res = res as any[];
      this._itemCount = res?.length || 0;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
    } else {
      const { data, total, fetchMore } = res as LoadResult<any>;
      this._itemCount = toNumber(total, (data && data.length) || 0);
      this._onFetchMoreCallback = fetchMore;
      this._moreItemsCount = (fetchMore && this._itemCount - data.length) || 0;
      res = data;
    }

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Filtered items by suggestFn:', value, res);

    return res;
  }

  private async fetchMore(): Promise<any[]> {
    if (!this._onFetchMoreCallback) return [];

    // Save then remove fetch function (to avoid multi-call)
    const fetchMoreFn = this._onFetchMoreCallback;
    this._onFetchMoreCallback = undefined;

    const { data, total, fetchMore } = await fetchMoreFn();
    this._itemCount = total || this._itemCount;
    this._onFetchMoreCallback = fetchMore;
    this._moreItemsCount = Math.max(0, this._moreItemsCount - data.length); // Borne min = 0
    return data;
  }

  private updateImplicitValue(items: any[]) {
    // Store implicit value (will use it onBlur if not other value selected)
    if (!this.multiple && items?.length === 1) {
      const implicitValue = items[0];
      const applyImplicitValue = typeof this.applyImplicitValue === 'function' ? this.applyImplicitValue(implicitValue) : this.applyImplicitValue;
      if (applyImplicitValue) {
        this._implicitValue = implicitValue;

        // Preview the implicit in the input text (and select the added part)
        if (this.previewImplicitValue && this.matInputText?.nativeElement) {
          const searchText = this.matInputText.nativeElement.value;

          // Complete the search text, with the full implicit value applied
          if (!RegExpUtils.hasWildcardCharacter(searchText)) {
            const displayValue = this.displayWith(implicitValue);
            if (displayValue.toUpperCase().startsWith(searchText.toUpperCase())) {
              // Select the additional text to match the implicit value
              this.setInputTextValue(displayValue, searchText.length, displayValue.length, 'backward');
            }
          }
        }

        return;
      }
      // Continue : reste implicit value
    }

    this._implicitValue = undefined;
  }

  private checkIfTouched() {
    if (this.formControl.touched) {
      this.cd.markForCheck();
      this._onTouchedCallback();
    }
  }

  private getAutocompletePanel(): HTMLElement {
    const ele = this.autocomplete?.options.last?._getHostElement().parentElement;
    if (!ele) {
      // DEBUG
      console.warn(this.logPrefix + ' Unable to find the autocomplete div...');
    }
    return ele;
  }

  private getMatSelectPanel(): HTMLElement {
    const ele = this.matSelect.options.last?._getHostElement().parentElement;
    if (!ele) {
      // DEBUG
      console.warn(this.logPrefix + ' Unable to find the mat-select container...');
    }
    return ele;
  }

  private markForCheck() {
    this.cd.markForCheck();
  }

  private _fixMatSelectPositionAndSearchbar() {
    if (!this.matSelect) return;

    // Hide the panel (will be show when ready)
    this.matSelect.panelClass = 'hidden';

    let searchBarSubscription: Subscription;
    this._subscription.add(
      this.matSelect.openedChange.subscribe(async (open) => {
        if (open) {
          const panelElement = this.matSelect.panel.nativeElement as HTMLElement;
          const overlayPane = findParentWithClass(panelElement, 'cdk-overlay-pane');

          // Fix panel position, when overlay has an transformX (added when keyboard is shown)
          this._fixMatSelectPanelPosition(overlayPane);

          // Fix overlay, for searchbar
          if (this.showSearchBar) {
            searchBarSubscription = await this._fixSearchbarOverlay(panelElement, overlayPane);
            if (searchBarSubscription) this._subscription.add(searchBarSubscription);
          }

          // Show the panel
          this.renderer.removeClass(panelElement, 'hidden');
          this.matSelect.panelClass = this.panelClass;
        }
        // ON close
        else {
          // Unsubscribe search bar workaround
          if (searchBarSubscription) {
            searchBarSubscription.unsubscribe();
            this._subscription.remove(searchBarSubscription);
            searchBarSubscription = null;

            // Hide panel again
            this.matSelect.panelClass = 'hidden';
          }
        }
      })
    );
  }

  private _fixMatSelectPanelPosition(overlayPane: HTMLElement) {
    if (!overlayPane) return;

    // Remove the unwanted default translateX
    this.renderer.setStyle(
      overlayPane,
      'transform',
      overlayPane.style.transform
        .split(' ')
        .filter((prop) => !prop.match(/^translateX/))
        .join(' ')
    );

    // Fix panel width to field width (it was not forced by @Input panelWidth)
    if (!this.panelWidth) {
      if (this.debug) console.debug(`${this.logPrefix} Fixing select panel width to the field size`);
      const width = `${this.el.nativeElement.offsetWidth - 1}px`;
      if (this._defaultPanelWidth !== width) {
        this._defaultPanelWidth = width;
        this.renderer.setStyle(overlayPane, 'width', width);
      }
    }

    // Fix panel's left, to avoid overflow on the right
    const left = overlayPane.style.left;
    if (isNotNilOrBlank(left) && !left.includes('calc(') && left.endsWith('px')) {
      if (this.debug) console.debug(`${this.logPrefix} Fixing select panel left, to avoid overflow on the right`);
      const width = `${overlayPane.clientWidth}px`;
      const fixLeft = `calc(-${width} + min(${left} + ${width}, -32px + 100vw))`;
      this.renderer.setStyle(overlayPane, 'left', fixLeft, RendererStyleFlags2.Important);
    }

    // Fix panel's top, to avoid overflow on the bottom
    const top = overlayPane.style.top;
    if (isNotNilOrBlank(top) && !top.includes('calc(') && top.endsWith('px')) {
      if (this.debug) console.debug(`${this.logPrefix} Fixing select panel top, to avoid overflow on the bottom`);
      const height = `${overlayPane.clientHeight || 275}px`;
      const fixTop = `calc(-${height} + min(${top} + ${height}, -32px + 100vh))`;
      this.renderer.setStyle(overlayPane, 'top', fixTop, RendererStyleFlags2.Important);
    }
  }

  /**
   * Hack : if we are in a IonModal, it's necessary to move the CdkOverlay container
   *     in the IonModal to might keep focus in IonSearchbar.
   *
   * @param panelElement
   * @param overlayPane
   * @private
   */
  private async _fixSearchbarOverlay(panelElement: HTMLElement, overlayPane: HTMLElement): Promise<Subscription | undefined> {
    if (!this.matSelect || !this.ionSearchbar) return;

    const modalElement = await this.modalController.getTop();
    if (!modalElement) return; // No modal: skip

    // Get the CDK overlay container
    const overlayContainer = findParentWithClass(panelElement, 'cdk-overlay-container');
    if (!overlayContainer) return;
    const originalParent = overlayContainer.parentElement;

    // DEBUG
    //if (this.debug)
    console.debug(`${this.logPrefix}Moving the "cdk-overlay" container from the "${originalParent?.tagName || 'body'}" to the "ion-modal" container`);

    this.renderer.appendChild(modalElement, overlayContainer);

    const _document = this.injector.get(DOCUMENT);
    const shiftLeft = (_document.defaultView.innerWidth - modalElement.firstElementChild.getBoundingClientRect().width) / 2;
    const shiftTop = (_document.defaultView.innerHeight - modalElement.firstElementChild.getBoundingClientRect().height) / 2;

    this.renderer.setStyle(overlayPane, 'margin-top', `-${shiftTop}px`);
    this.renderer.setStyle(overlayPane, 'margin-left', `-${shiftLeft}px`);

    // Return the reverse function, to replace the container in body
    return new Subscription(() => {
      // DEBUG
      //if (this.debug)
      console.debug(`${this.logPrefix} moving the "cdk-overlay" container from the "ion-modal" to the "body" container`);

      this.renderer.appendChild(originalParent /*this.document.body*/, overlayContainer);
    });
  }

  private setInputTextValue(value: string, selectionStartIndex?: number, selectionEndIndex?: number, direction?: 'backward' | 'forward') {
    // Apply to the input[type=text] field
    if (this.matInputText?.nativeElement) {
      this.matInputText.nativeElement.value = value ?? '';
      if (isNotNil(selectionStartIndex)) {
        setTimeout(() => {
          selectInputRange(this.matInputText.nativeElement, selectionStartIndex, selectionEndIndex, direction);
        });
      }
    }
  }

  protected onBackspace(event: Event): void {
    const input = event?.target as HTMLInputElement;

    // If preview enabled, remove selection when press backspace (by default only the selection will be deleted)
    if (this.previewImplicitValue && input) {
      const selectionStart = input.selectionStart;
      const selectionEnd = input.selectionEnd;
      let value = input.value;

      // Make sure to remove the last typed character (by default only the selection will be deleted)
      if (this._implicitValue && selectionStart > 0 && selectionStart < selectionEnd && value.length === selectionEnd) {
        // DEBUG
        //if (this.debug) console.debug(`${this.logPrefix} Backspace pressed, additional text from the implicit value`);

        event.preventDefault();

        // Define the new value by removing the text in the adjusted range
        const newCaretPosition = selectionStart - 1;
        value = newCaretPosition > 0 ? value.slice(0, newCaretPosition) : '';

        // clean implicit value
        this._implicitValue = null;

        // Apply the new value and update the selection
        this.setInputTextValue(value, newCaretPosition, selectionEnd);

        // Mark component as loading
        this.markAsLoading();

        // Reload items
        this.reloadItems(value);
      }
    }

    // Emit
    this.keydownBackspace.emit(event);
  }
}
