import { NgModule, Type } from '@angular/core';
import { CdkTableModule } from '@angular/cdk/table';

import { A11yModule } from '@angular/cdk/a11y';
import { OverlayModule } from '@angular/cdk/overlay';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCommonModule, MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatBadgeModule } from '@angular/material/badge';
import { SharedMatDurationModule } from './duration/duration.module';
import { SharedMatBooleanModule } from './boolean/boolean.module';
import { MatDividerModule } from '@angular/material/divider';
import { SharedBadgeModule } from './badge/badge.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedMatAutocompleteModule } from './autocomplete/material.autocomplete.module';
import { SharedMatDateTimeModule } from './datetime/datetime.module';
import { SharedMatSwipeModule } from './swipe/swipe.module';
import { SharedMatChipsModule } from './chips/chips.module';
import { SharedTextFormModule } from './text/text-form.module';
import { SharedMatLatLongModule } from './latlong/material.latlong.module';
//import {SharedMatNumpadModule} from './numpad/numpad.module';

const modules: Array<Type<any> | any[]> = [
  // Angular material components
  MatCommonModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatInputModule,
  CdkTableModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatToolbarModule,
  MatDialogModule,
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatCardModule,
  MatTabsModule,
  MatListModule,
  MatStepperModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatRadioModule,
  MatBadgeModule,
  MatSlideToggleModule,
  A11yModule, // Used for focus trap
  OverlayModule,
  ScrollingModule,
  MatRippleModule,
  MatDividerModule,
  MatTooltipModule,
  // Custom components
  SharedMatAutocompleteModule,
  SharedMatLatLongModule,
  SharedMatDateTimeModule,
  SharedMatDurationModule,
  SharedMatBooleanModule,
  //SharedMatNumpadModule,
  SharedMatSwipeModule,
  SharedMatChipsModule,
  SharedBadgeModule,
  SharedTextFormModule,
];

@NgModule({
  imports: modules,
  exports: modules,
})
export class SharedMaterialModule {}
