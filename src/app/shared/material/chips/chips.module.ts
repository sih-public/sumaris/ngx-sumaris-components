import { NgModule } from '@angular/core';
import { MatChipsField } from './material.chips';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { MatCommonModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { RxStateModule } from '../../rx-state/rx-state.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedPipesModule,
    SharedDirectivesModule,
    MatCommonModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatChipsModule,
    TranslateModule.forChild(),
    RxStateModule,
  ],
  declarations: [MatChipsField],
  exports: [MatChipsField],
})
export class SharedMatChipsModule {}
