import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { SharedValidators } from '../../../validator/validators';
import { isNil, isNotNil, sleep } from '../../../functions';
import { LoadResult } from '../../../services/entity-service.class';
import { EntityUtils } from '../../../../core/services/model/entity.model';
import { suggestFromArray } from '../../../services';
import { MatAutocompleteConfigHolder } from '../../autocomplete/material.autocomplete.config';
import { MatFormFieldAppearance } from '@angular/material/form-field';

class EntityTestClass {
  id: number;
  label: string;
  name: string;
}

const FAKE_ENTITIES: EntityTestClass[] = [
  { id: 1, label: 'AAA', name: 'Item A' },
  { id: 2, label: 'BBB', name: 'Item B' },
  { id: 3, label: 'CCC', name: 'Item C' },
  { id: 4, label: 'DDD', name: 'Item D' },
  { id: 5, label: 'EEE', name: 'Item E' },
  { id: 6, label: 'FFF', name: 'Item F' },
  { id: 7, label: 'GGG', name: 'Item G' },
];

function deepCopy(values?: EntityTestClass[], size?: number): EntityTestClass[] {
  if (isNil(size)) {
    return (values || FAKE_ENTITIES).map((entity) => Object.assign({}, entity));
  }

  let result = [];
  let i = 1;
  while (result.length < size) {
    result = result.concat(
      (values || FAKE_ENTITIES).map((entity) => {
        const name = entity.name + ' #' + i;
        return { ...entity, id: i++, name };
      })
    );
  }
  return result.slice(0, size);
}

@Component({
  selector: 'app-chips-test',
  templateUrl: './chips.test.html',
})
export class ChipsTestPage implements OnInit {
  private _items = deepCopy(FAKE_ENTITIES, 100);
  private _$items = new BehaviorSubject<EntityTestClass[]>(undefined);

  form: UntypedFormGroup;
  autocompleteFields = new MatAutocompleteConfigHolder();
  appearance: MatFormFieldAppearance = 'fill';
  mobile = true;

  constructor(protected formBuilder: UntypedFormBuilder) {
    this.form = formBuilder.group({
      entity: [null, SharedValidators.entity],
      entityInitial: [null, SharedValidators.entity],
      disableEntity: [null, SharedValidators.entity],
      farEntity: [null, SharedValidators.entity],
    });

    this.form.get('disableEntity').disable();
  }

  ngOnInit() {
    // From suggest
    this.autocompleteFields.add('entity-suggestFn', {
      suggestFn: (value, filter) => this.suggest(value, filter),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
      equals: (o1, o2) => EntityUtils.compare(o1, o2, 1, 'id') === 0,
    });

    // From items
    this.autocompleteFields.add('entity-items', {
      items: this._items.slice(),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
      equals: (o1, o2) => EntityUtils.compare(o1, o2, 1, 'id') === 0,
    });

    // Far entity, in the list
    this.autocompleteFields.add('entity-far', {
      suggestFn: (value, filter) => this.suggest(value, filter),
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    // From $items observable
    this.autocompleteFields.add('entity-$items', {
      items: this._$items,
      attributes: ['label', 'name'],
      displayWith: this.entityToString,
    });

    this.loadData();
  }

  // Load the form with data
  async loadData() {
    const items = deepCopy(FAKE_ENTITIES);
    const data = {
      entity: items.slice(0, 1), // Select the first

      entityInitial: items.slice(1, 2),

      disableEntity: items.slice(1, 2),
    };

    this.form.setValue(data);

    // Load observables
    setTimeout(() => this.loadItems(), 1000);
  }

  async loadItems() {
    this._$items.next(this._items.slice());
  }

  entityToString(item: any) {
    return [(item && item.label) || undefined, (item && item.name) || undefined].filter(isNotNil).join(' - ');
  }

  async suggest(value: any, filter?: any): Promise<LoadResult<EntityTestClass>> {
    filter = { offset: 0, size: 30, ...filter };
    const res = suggestFromArray(this._items, value, {
      ...filter,
      searchAttributes: ['label', 'name'],
    });

    // Simulate a size = 30
    const total = res.total || res.data.length;

    const nextOffset = filter.offset + res.data.length;
    if (nextOffset < total) {
      filter = { ...filter, offset: nextOffset };
      return {
        total,
        data: res.data,
        fetchMore: async (variables) => {
          await sleep(1000); // Simulate access to server
          return this.suggest(value, { ...filter, ...variables });
        },
      };
    }

    return res;
  }

  doSubmit(_) {
    console.debug('Validate form: ', this.form.value);
  }

  equals(o1: EntityTestClass, o2: EntityTestClass): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

  /* -- protected methods -- */

  stringify(value: any) {
    return JSON.stringify(value);
  }
}
