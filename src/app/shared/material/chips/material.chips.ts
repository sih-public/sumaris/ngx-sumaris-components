import {
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  numberAttribute,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { BehaviorSubject, isObservable, merge, Observable, Subject, Subscription, timer } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap, takeUntil, tap } from 'rxjs/operators';
import { focusInput, InputElement, selectInputContentFromEvent, selectInputRange } from '../../inputs';
import { FetchMoreFn, LoadResult, SuggestFn } from '../../services/entity-service.class';
import { AppFloatLabelType, DisplayFn, EqualsFn } from '../../form/field.model';
import {
  changeCaseToUnderscore,
  isEmptyArray,
  isNil,
  isNilOrBlank,
  isNotNil,
  isNotNilOrBlank,
  isNotNilString,
  toBoolean,
  toNumber,
} from '../../functions';
import { firstNotNilPromise } from '../../observables';
import { fromScrollEndEvent } from '../../events';
import { ThemePalette } from '@angular/material/core';
import { suggestFromArray } from '../../services';
import { MatAutocompleteFieldConfig } from '../autocomplete/material.autocomplete.config';
import { MatAutocompleteFieldUtils } from '../autocomplete/material.autocomplete.utils';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';
import { MatAutocomplete, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { isMobile } from '../../platforms';
import { DEFAULT_JOIN_PROPERTIES_SEPARATOR } from '../../constants';

const noop = () => {};

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'mat-chips-field',
  styleUrls: ['./material.chips.scss'],
  templateUrl: './material.chips.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MatChipsField),
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatChipsField implements OnInit, OnDestroy, InputElement, ControlValueAccessor {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;

  private _onFetchMoreCallback: FetchMoreFn<LoadResult<any>>;
  private _implicitValue: any;
  private _subscription = new Subscription();
  private _itemsSubscription: Subscription;
  private _openedSubscription: Subscription;
  private _$filter = new BehaviorSubject<any>(undefined);
  private _itemCount: number;
  private _suggestExecutionId: number = 0; // Identifier of the suggest job
  private _reload$ = new Subject<any>();

  protected _tabindex: number;
  protected _$inputItems = new BehaviorSubject<any[]>(undefined);
  protected _$filteredItems = new BehaviorSubject<any[]>(undefined);
  protected _inputControl = new UntypedFormControl();
  protected _moreItemsCount: number;
  protected _fetchMore$ = new EventEmitter<Event>();

  @Input() equals: EqualsFn;
  @Input() logPrefix = '[mat-chips] ';
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string = null;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input() placeholder: string;
  @Input() suggestFn: SuggestFn<any, any>;
  @Input({ transform: booleanAttribute }) required = false;
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input({ transform: booleanAttribute }) readonly = false;
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input({ transform: numberAttribute }) debounceTime: number = null;

  @Input() displaySeparator: string | null;
  @Input() displayWith: DisplayFn | null;
  @Input() displayAttributes: string[];
  @Input() displayColumnSizes: (number | 'auto' | undefined)[];
  @Input() displayColumnNames: string[];
  @Input({ transform: booleanAttribute }) highlightAccent: boolean;
  @Input({ transform: booleanAttribute }) showAllOnFocus: boolean;
  @Input({ transform: booleanAttribute }) showPanelOnFocus: boolean;
  @Input({ transform: booleanAttribute }) autofocus = false;
  @Input() config: MatAutocompleteFieldConfig;
  @Input() i18nPrefix = 'REFERENTIAL.';
  @Input() noResultMessage = 'COMMON.NO_RESULT';
  @Input() panelClass: string;
  @Input() panelWidth: string;
  @Input({ transform: booleanAttribute }) disableRipple = false;
  @Input() matAutocompletePosition: 'auto' | 'above' | 'below';
  @Input() itemSize = '48px';
  @Input() fetchMoreThreshold = '15%';
  @Input({ transform: numberAttribute }) suggestLengthThreshold: number = null;
  @Input({ transform: booleanAttribute }) showLoadingSpinner = true;
  @Input() chipColor: ThemePalette;
  @Input({ transform: booleanAttribute }) debug = false;
  @Input() applyImplicitValue: boolean | ((value: any) => boolean) = false; // /!\ false by default (not like autocomplete)
  @Input() dropButtonTitle: string;
  @Input() clearButtonTitle: string;
  @Input({ transform: booleanAttribute }) trimSearchText: boolean;

  /**
   * @deprecated Use panelClass instead
   */
  @Input({ alias: 'class' }) set classList(value: string) {
    this.panelClass = value;
  }
  get classList(): string {
    return this.panelClass;
  }

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('click') clicked = new EventEmitter<MouseEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('blur') blurred = new EventEmitter<FocusEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('focus') focused = new EventEmitter<FocusEvent>();
  @Output() dropButtonClick = new EventEmitter<Event>(true);
  @Output('keydown.escape') keydownEscape = new EventEmitter<Event>();
  @Output('keyup.enter') keyupEnter = new EventEmitter<Event>();

  @ViewChild('matInputText') matInputText: ElementRef;
  @ViewChild(MatAutocomplete) autocomplete: MatAutocomplete;
  @ViewChild(MatAutocompleteTrigger) autocompleteTrigger: MatAutocompleteTrigger;

  constructor(
    protected cd: ChangeDetectorRef,
    @Optional() private formGroupDir: FormGroupDirective
  ) {}

  get itemCount(): number {
    return this._itemCount;
  }

  get canFetchMore(): boolean {
    return this._onFetchMoreCallback && this._moreItemsCount > 0;
  }

  @Input() set filter(value: any) {
    if (value !== this._$filter.value) {
      // DEBUG
      //console.debug(this.logPrefix + " Setting filter:", value);
      this._$filter.next(value);
    }
  }

  get filter(): any {
    return this._$filter.value;
  }

  @Input() set tabindex(value: number) {
    this._tabindex = value;
    this.markForCheck();
  }

  get tabindex(): number {
    return this._tabindex;
  }

  @Input() set items(value: Observable<any[]> | any[]) {
    // Remove previous subscription on items, (if exits)
    if (this._itemsSubscription) {
      console.warn(this.logPrefix + ' Items received twice !');
      this._subscription.remove(this._itemsSubscription);
      this._itemsSubscription.unsubscribe();
    }

    if (isObservable(value)) {
      this._itemsSubscription = value.subscribe((v) => this._$inputItems.next(v));
      this._subscription.add(this._itemsSubscription);
    } else {
      if (value !== this._$inputItems.value) {
        this._$inputItems.next(value as any[]);
      }
    }
  }

  get items(): Observable<any[]> | any[] {
    return this._$inputItems;
  }

  get value(): any[] {
    return this.formControl.value || [];
  }

  get disabled(): boolean {
    return this.readonly || this.formControl?.disabled || false;
  }

  get enabled(): boolean {
    return (!this.readonly && this.formControl?.enabled) || false;
  }

  get loading(): boolean {
    return isNil(this._$filteredItems.value);
  }

  ngOnInit() {
    this.formControl =
      this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-chips-field>.");

    // Configuration from config object
    if (this.config) {
      this.suggestFn = this.suggestFn || this.config.suggestFn;
      if (!this.suggestFn && this.config.items) {
        this.items = this.config.items;
      }
      this.filter = this.filter || this.config.filter;
      this.displayAttributes = this.displayAttributes ?? this.config.attributes;
      this.displayColumnSizes = this.displayColumnSizes ?? this.config.columnSizes;
      this.displayColumnNames = this.displayColumnNames ?? this.config.columnNames;
      this.displaySeparator = this.displaySeparator ?? this.config.displayWithSeparator;
      this.displayWith = this.displayWith ?? this.config.displayWith;
      this.trimSearchText = this.trimSearchText ?? this.config.trimSearchText;
      this.equals = this.equals ?? this.config.equals;
      this.mobile = this.mobile ?? this.config.mobile;
      this.suggestLengthThreshold = this.suggestLengthThreshold ?? this.config.suggestLengthThreshold;
      this.showAllOnFocus = this.showAllOnFocus ?? this.config.showAllOnFocus;
      this.showPanelOnFocus = this.showPanelOnFocus ?? this.config.showPanelOnFocus;
      this.panelClass = this.panelClass ?? this.config.panelClass ?? this.config.class;
      this.panelWidth = this.panelWidth ?? this.config.panelWidth;
      this.debounceTime = toNumber(this.debounceTime, this.config.debounceTime);
      this.highlightAccent = toBoolean(this.highlightAccent, toBoolean(this.config.highlightAccent));
    }

    // Default values
    this.mobile = this.mobile ?? isMobile(window);
    this.displayAttributes = this.displayAttributes ?? this.filter?.attributes ?? ['label', 'name'];
    this.displaySeparator = this.displaySeparator ?? DEFAULT_JOIN_PROPERTIES_SEPARATOR;
    this.displayWith = this.displayWith ?? MatAutocompleteFieldUtils.createDisplayFn(this.displayAttributes, false, this.displaySeparator);
    this.trimSearchText = this.trimSearchText ?? true;
    this.equals = this.equals ?? MatAutocompleteFieldUtils.defaultEquals;

    this.displayColumnSizes =
      this.displayColumnSizes ??
      // if only column: auto size
      ((this.displayAttributes.length === 1 && [undefined]) ||
        // Computed column size:
        // - if label then col size = 2
        // - if rankOrder then col size = 1
        // - ese => auto size
        this.displayAttributes.map((attr) => attr && (attr.endsWith('label') ? 3 : attr.endsWith('rankOrder') ? 2 : undefined)));
    this.displayColumnNames = this.displayAttributes.map(
      (attr, index) => (this.displayColumnNames && this.displayColumnNames[index]) || this.i18nPrefix + changeCaseToUnderscore(attr).toUpperCase()
    );
    const panelClasses = this.panelClass?.split(/\s/);
    this.panelWidth =
      this.panelWidth ??
      (panelClasses &&
        ((panelClasses.includes('min-width-medium') && '300px') ||
          (panelClasses.includes('min-width-large') && '400px') ||
          (panelClasses.includes('min-width-xlarge') && '450px') ||
          (panelClasses.includes('min-width-80vw') && '80vw') ||
          ((panelClasses.includes('full-width') || panelClasses.includes('full-size')) && '100vw')));
    this.suggestLengthThreshold = this.suggestLengthThreshold ?? 0;
    this.showAllOnFocus = this.showAllOnFocus ?? this.suggestLengthThreshold <= 0;
    this.showPanelOnFocus = this.showPanelOnFocus ?? true;
    this.debounceTime = this.debounceTime ?? (this.mobile ? 650 : 250);
    this.highlightAccent = this.highlightAccent ?? false;
    this.matAutocompletePosition = this.matAutocompletePosition ?? (this.mobile ? 'above' : 'auto'); // On mobile, set position to above (because of bottom keyboard)

    // No suggestFn: filter on the given items
    if (!this.suggestFn) {
      const suggestFromArrayFn: SuggestFn<any, any> = async (value, filter) =>
        // DEBUG
        //if (this.debug) console.debug(this.logPrefix + ' Calling suggestFromArray with value=', value);

        suggestFromArray(this._$inputItems.value, value, {
          searchAttributes: this.displayAttributes,
          ...filter,
        });
      // Wait (once) that items are loaded, then call suggest from array fn
      this.suggestFn = async (value, filter) => {
        if (isNil(this._$inputItems.value)) {
          // DEBUG
          //if (this.debug) console.debug('[mat-chips] Waiting items to be set...');

          await firstNotNilPromise(this._$inputItems);

          // DEBUG
          //if (this.debug) console.debug('[mat-chips] Received items:', this.$inputItems.value);
        }
        this.suggestFn = suggestFromArrayFn;
        return this.suggestFn(value, filter); // Loop
      };
    }

    // Send value to suggest when :
    // - is nil and '*', and no min length threshold
    // - is not nil and not a string type (e.g. object)
    // - is not nil and is a string, and has enough character
    const thresholdFilterFn = (value) =>
      this.suggestLengthThreshold <= 0 ||
      (isNotNil(value) && (!(typeof value === 'string') || (value !== '*' && value.trim().length >= this.suggestLengthThreshold)));

    const filteredItemsChanges = merge(
      // Reload event (no distinctUntilChanged() pipe, to force reload even if same value)
      this._reload$.pipe(
        filter(() => this.enabled),
        filter(thresholdFilterFn),
        tap((_) => this.showLoadingSpinner && this.markAsLoading())
      ),

      // Search text (no distinctUntilChanged() because of the debounceTime() pipe)
      this._inputControl.valueChanges.pipe(
        filter((value) => this.enabled && isNotNilString(value)),
        // Mark as loading (to show loading spinner) - as soon as possible, and before debounceTime
        tap((_) => this.showLoadingSpinner && this.markAsLoading()),
        // Filter if not enough character
        filter((value) => (value?.length || 0) >= this.suggestLengthThreshold),
        // Wait a debounce time
        debounceTime(this.debounceTime)
        // DEBUG
        //,tap((value) => this.debug && console.debug(this.logPrefix + ' Search text changes:', value))
      ),

      // Merge some observables, with a common distinctUntilChanged() pipe
      merge(
        // On dropdown button click (no debounceTime)
        this.dropButtonClick.pipe(
          filter((event) => !event || !event.defaultPrevented)
          // DEBUG
          //,tap(value => this.debug && console.debug(this.logPrefix + ' Received an control object value: ', value))
        ),
        // Focus or click => Load all (no debounceTime)
        merge(this.focused, this.clicked),
        // DEBUG
        //.pipe(tap((_) => this.debug && console.debug(this.logPrefix + ' Received focused or clicked event'))),
        // Input items or filter changes
        merge(this._$inputItems, this._$filter).pipe(
          filter((_) => !this.loading)
          // DEBUG
          //,tap((_) => this.debug && console.debug(this.logPrefix + ' Received $inputItems or filter event'))
        )
      ).pipe(
        filter((_) => this.enabled),
        map((_) => (this.showAllOnFocus && isNilOrBlank(this._inputControl.value) ? '*' : this._inputControl.value)),
        // Distinguish changes
        distinctUntilChanged((v1, v2) =>
          // If loading (not loaded yet), simulate that value changed to force load
          this.loading ? false : v1 === v2 || this.equals(v1, v2)
        ),
        // Make sure there is enough characters
        filter(thresholdFilterFn)
        // DEBUG
        //,tap((value) => this.debug && console.debug(this.logPrefix + ' Received update event: ', value))
      )
    ).pipe(
      // Suggest values
      mergeMap((value) => this.suggest(value, this.filter)),

      // Ignore undefined result (e.g. when a newer suggest call is running)
      filter(isNotNil),

      // DEBUG
      //tap(items => this.debug && console.debug(this.logPrefix + ' Received from suggest: ', items)),

      // Store implicit value (will use it onBlur if not other value selected)
      tap((items) => this.updateImplicitValue(items))
    );

    // Fetch more events
    const fetchMoreItemsChanges = this._fetchMore$.pipe(
      tap((event) => event?.preventDefault()), // Avoid to close the mat-select
      filter(() => this.canFetchMore),
      mergeMap(() => this.fetchMore()),

      // DEBUG
      //tap(moreItems => console.debug(this.logPrefix + ' Received from fetch More: ', moreItems)),

      // Concat to existing items
      map((moreItems) => (this._$filteredItems.value || []).concat(moreItems))
    );

    // Update filtered items
    this._subscription.add(
      merge(
        this._$inputItems,

        // Update items events
        filteredItemsChanges,

        // Fetch more events
        fetchMoreItemsChanges
      )
        // Emit items
        .subscribe((items) => this._$filteredItems.next(items))
    );

    // Applying implicit value, on blur
    this._subscription.add(
      this.blurred
        .pipe(
          debounceTime(150),
          // Skip if multiple (no implicit value) or disable
          filter((_) => this.enabled),

          map((_) => this._inputControl.value)
          // DEBUG
          //,tap(value => this.debug && console.debug(this.logPrefix + ' Received blurred event: ', value)),
        )
        .subscribe((value) => {
          // When leave component without object, use implicit value if :
          // - an explicit value
          // - field is not empty (user fill something)
          // - OR field empty but is required
          if (
            this._implicitValue &&
            ((this.required && isEmptyArray(this.formControl.value)) || (isNotNilOrBlank(value) && typeof value !== 'object'))
          ) {
            this.add(this._implicitValue);
            this.checkIfTouched();
          } else {
            // Reset search text
            this.matInputText.nativeElement.value = '';
            this._inputControl.setValue('');
            this.checkIfTouched();
            this.markAsLoading();
          }

          this._implicitValue = null; // reset the implicit value
          this.closePanel(); // Clone panel
        })
    );

    // Listen form control status change
    this._subscription.add(
      this.formControl.statusChanges
        .pipe(distinctUntilChanged())
        // Update component state
        .subscribe(() => this.checkIfTouched())
    );

    this._subscription.add(
      this.keydownEscape.subscribe((event) => {
        if (this.isOpen) {
          // Avoid form or page to manage escape event
          event?.stopPropagation();
          event?.preventDefault();

          // DEBUG
          //console.debug(this.logPrefix + " Closing the panel (keydown.escape)");

          this.closePanel();
        }
      })
    );
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    if (this._itemsSubscription) {
      this._itemsSubscription.unsubscribe();
    }
    if (this._openedSubscription) {
      this._openedSubscription.unsubscribe();
    }
    this._$inputItems.complete();
    this._$inputItems.unsubscribe();
    this._$filteredItems.complete();
    this._$filteredItems.unsubscribe();
    this._reload$.complete();
    this._$filter.complete();

    this.dropButtonClick.complete();
    this.clicked.complete();
    this.blurred.complete();
    this.focused.complete();
    this.keydownEscape.complete();
    this.keyupEnter.complete();
    this._fetchMore$.complete();

    this._implicitValue = undefined;
    this.formControl = null;
    this.displayWith = null;
    this.equals = null;
    this.suggestFn = null;
    this._$filteredItems = null;
    this.config = null;
    this.panelClass = null;

    this._onChangeCallback = null;
    this._onTouchedCallback = null;
    this._onFetchMoreCallback = null;

    // Destroy the 'autocomplete' instance, to avoid memory leak
    if (this.autocomplete) {
      this.autocomplete.ngOnDestroy();
    }
  }

  /**
   * Add item to chips
   *
   * @param item
   */
  add(item: any) {
    if (this.debug) console.debug(`${this.logPrefix}select`, item);
    this.writeValue([...this.value, item]);
    this.matInputText.nativeElement.value = '';
    this._inputControl.setValue('');
  }

  /**
   * Remove item from chips
   *
   * @param item
   */
  remove(item: any) {
    this.closePanel();
    const value = this.value;
    const index = value ? value.indexOf(item) : -1;
    if (index !== -1) {
      const newValue = value.slice();
      newValue.splice(index, 1);
      this.writeValue(newValue);
      this.markAsLoading();
    }
  }

  /**
   * Allow to reload content. Useful when filter has been changed but not detected
   */
  reloadItems(value?: any) {
    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + " Asking to reloadItems()");
    // Force a refresh
    this._reload$.next(value || (this.showAllOnFocus && this.suggestLengthThreshold <= 0 ? '*' : this.formControl.value));
  }

  writeValue(value: any[]): void {
    // DEBUG
    // if (this.debug) console.debug(this.logPrefix + ' Writing value: ', value);

    if (value && !Array.isArray(value)) {
      value = [value];
    }
    if (value !== this.formControl.value) {
      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + ' Writing value: ', value);

      this.formControl.patchValue(value, { emitEvent: false });
      this._onChangeCallback(value);
    } else {
      this.markForCheck();
    }
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled && this.autocomplete?.isOpen) {
      this.closePanel();
    }
    this.cd.markForCheck();
  }

  focus() {
    focusInput(this.matInputText);
  }

  filterInputTextFocusEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) return;

    // Ignore event from mat-option
    if (event.relatedTarget instanceof HTMLElement && event.relatedTarget.tagName === 'MAT-OPTION') {
      // FIXME: Never happens anymore but no problem because blur event has been fixed
      event.preventDefault();
      if (event.stopPropagation) event.stopPropagation();
      event.returnValue = false;

      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + ' Cancelling focus event');

      return false;
    }

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Select input content');
    const hasContent = selectInputContentFromEvent(event);

    if (hasContent) {
      // Move caret to the beginning (fix issue IMAGINE-469)
      selectInputRange(event.target as any, 0, 0);
    } else if (this.showPanelOnFocus && this.showAllOnFocus) {
      // If combo is empty, or if has content but should force to show panel on focus
      // DEBUG
      //if (this.debug) console.debug(this.logPrefix + ' Emit focus event');

      this.focused.emit(event);
      return true;
    }
    return false;
  }

  protected filterInputTextBlurEvent(event: FocusEvent) {
    if (!event || event.defaultPrevented) return;

    // Emit blur event
    this.blurred.emit(event);
  }

  filterChipsFocusEvent(event: FocusEvent) {
    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Focus in chips list', event);

    // Ignore event in button

    this.matInputText.nativeElement.focus();
  }

  clearValue(event: Event) {
    this.writeValue(null);
    event.stopPropagation();
    this.markAsLoading();
  }

  _initAutocompleteInfiniteScroll(threshold?: string) {
    if (!this.autocomplete) return; // Skip

    // DEBUG
    //console.debug(this.logPrefix + ' Init autocomplete scroll listener');

    if (this._openedSubscription) {
      this._openedSubscription.unsubscribe();
      this._subscription.remove(this._openedSubscription);
    }

    const opened$: Observable<any> = this.autocomplete._isOpen ? timer(0) : this.autocomplete.opened.pipe(debounceTime(200));
    const scrollEnd$ = opened$.pipe(
      map((_) => this.getAutocompletePanel()),
      filter(isNotNil),
      switchMap((panel) => fromScrollEndEvent(panel, threshold)),
      takeUntil(this.autocomplete.closed)
    );

    // Trigger fetch more, end end scroll reached
    this._openedSubscription = scrollEnd$.subscribe(() => this._fetchMore$.emit());

    // Register subscription
    this._subscription.add(this._openedSubscription);
  }

  get isOpen(): boolean {
    return this.autocomplete?.isOpen || false;
  }

  closePanel() {
    if (this.autocomplete?.isOpen) {
      this.autocompleteTrigger?.closePanel();
      return;
    }
  }

  protected toggleShowPanel(event: Event) {
    if (this.isOpen) {
      event?.preventDefault();
      event?.stopPropagation();
      this.closePanel();
    } else {
      this.dropButtonClick.emit(event);
    }
  }

  /* -- private method -- */

  private async suggest(value: any, filter?: any): Promise<any[] | undefined> {
    // Compute a new execution id
    const id = ++this._suggestExecutionId;

    // Trim string value before searching
    if (this.trimSearchText && typeof value === 'string') {
      value = value.trim();
    }

    // Call suggestion function
    let res = await this.suggestFn(value, filter);

    // Ignore the current result, if a newer execution has been launched
    if (id !== this._suggestExecutionId) return undefined;

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Filtered items by suggestFn:', value, res);

    if (!res) {
      this._itemCount = 0;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
      res = [];
    } else if (Array.isArray(res)) {
      res = res as any[];
      this._itemCount = res?.length || 0;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
    } else {
      const { data, total, fetchMore } = res as LoadResult<any>;
      this._itemCount = toNumber(total, (data && data.length) || 0);
      this._onFetchMoreCallback = fetchMore;
      this._moreItemsCount = (fetchMore && this._itemCount - data.length) || 0;
      res = data;
    }

    // Filter out existing items
    const existingItems = this.value;
    const filteredData = (res || []).filter((item1) => !existingItems?.some((item2) => this.equals(item1, item2)));
    const removedItemCount = (res?.length || 0) - filteredData.length;

    // Decrease the item count, with delta
    this._itemCount -= removedItemCount;

    // DEBUG
    //if (this.debug) console.debug(this.logPrefix + ' Filtered items by suggestFn:', value, filteredData);

    return filteredData as any[];
  }

  private async fetchMore(): Promise<any[]> {
    if (!this._onFetchMoreCallback) return [];

    // Save then remove fetch function (to avoid multi-call)
    const fetchMoreFn = this._onFetchMoreCallback;
    this._onFetchMoreCallback = undefined;

    const { data, total, fetchMore } = await fetchMoreFn();
    this._itemCount = (total || this._itemCount) - (this.value?.length || 0);
    this._onFetchMoreCallback = fetchMore;
    this._moreItemsCount = Math.max(0, this._moreItemsCount - data.length); // Borne min = 0
    return data;
  }

  private updateImplicitValue(items: any[]) {
    // Store implicit value (will use it onBlur if not other value selected)
    if (items?.length === 1) {
      const applyImplicitValue = typeof this.applyImplicitValue === 'function' ? this.applyImplicitValue(items[0]) : this.applyImplicitValue;
      this._implicitValue = applyImplicitValue ? items[0] : undefined;
    } else {
      this._implicitValue = undefined;
    }
  }

  private checkIfTouched() {
    if (this.formControl.touched) {
      this.cd.markForCheck();
      this._onTouchedCallback();
    }
  }

  private getAutocompletePanel(): HTMLElement {
    const ele = this.autocomplete?.options.last?._getHostElement().parentElement;
    if (!ele) {
      // DEBUG
      console.warn(this.logPrefix + ' Unable to find the autocomplete div...');
    }
    return ele;
  }

  private markForCheck() {
    this.cd.markForCheck();
  }

  private markAsLoading() {
    if (this._$filteredItems.value) {
      this._$filteredItems.next(undefined);
      this._itemCount = undefined;
      this._implicitValue = undefined;
      this._onFetchMoreCallback = undefined;
      this._moreItemsCount = 0;
      this.markForCheck();
    }
  }
}
