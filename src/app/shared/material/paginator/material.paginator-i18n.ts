import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';

@Injectable({ providedIn: 'root' })
export class MatPaginatorI18n extends MatPaginatorIntl {
  ofLabel: string;

  constructor(private translate: TranslateService) {
    super();

    this.translate
      .get([
        'COMMON.PAGINATOR.ITEMS_PER_PAGE',
        'COMMON.PAGINATOR.NEXT_PAGE',
        'COMMON.PAGINATOR.PREVIOUS_PAGE',
        'COMMON.PAGINATOR.OF',
        'COMMON.PAGINATOR.FIRST_PAGE',
        'COMMON.PAGINATOR.LAST_PAGE',
      ])
      .subscribe((translations: { [key: string]: string }) => {
        console.info('[MatPaginatorI18n] Update pagination translations');
        this.itemsPerPageLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.ITEMS_PER_PAGE', '');
        this.nextPageLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.NEXT_PAGE', '>');
        this.previousPageLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.PREVIOUS_PAGE', '<');
        this.ofLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.OF', '/');
        this.firstPageLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.FIRST_PAGE', '|<');
        this.lastPageLabel = this._getTranslationValue(translations, 'COMMON.PAGINATOR.LAST_PAGE', '>|');
        this.changes.next();
      });
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return '0 ' + this.ofLabel + ' ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' ' + this.ofLabel + ' ' + length;
  };

  private _getTranslationValue(translations: any, key: string, defaultValue: string): string {
    return translations[key] !== key ? translations[key] : defaultValue;
  }
}
