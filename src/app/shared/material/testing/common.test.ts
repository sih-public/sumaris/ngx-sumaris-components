import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AppFormUtils } from '../../../core/form/form.utils';
import { debounceTime } from 'rxjs/operators';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { SharedFormGroupValidators, SharedValidators } from '../../validator/validators';
import { isMobile } from '../../platforms';
import { DateUtils, toDateISOString } from '../../dates';
import { StatusList } from '../../../core/services/model/referential.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'mat-common-test',
  templateUrl: './common.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatCommonTestPage implements OnInit, OnDestroy {
  showLogPanel = true;
  logContent = '';

  form: UntypedFormGroup;

  statusList = StatusList;

  private subscription = new Subscription();

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    this.form = formBuilder.group(
      {
        empty: [null],
        emptyRequired: [null, Validators.required],
        enable: [null, Validators.required],
        disable: [null, Validators.required],
        readonly: [null],
        select: [null, Validators.required],
        boolean: [null],
        date: [null, Validators.compose([Validators.required, SharedValidators.validDate])],
      },
      {
        validators: SharedFormGroupValidators.dateRange('empty', 'enable'),
      }
    );

    const disableControl = this.form.get('disable');
    disableControl.disable();

    // Copy the 'enable' value, into the disable control
    this.subscription.add(this.form.get('enable').valueChanges.subscribe((value) => disableControl.setValue(value)));

    this.showLogPanel = this.showLogPanel || isMobile(window);
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // Load the form with data
  async loadData() {
    const now = DateUtils.moment();

    const data = {
      empty: null, // toDateISOString(now.clone().add(2, 'hours')),
      emptyRequired: null,
      enable: toDateISOString(now),
      disable: now.clone(),
      readonly: now.clone(),
      select: null,
      boolean: null,
      date: null,
    };

    this.form.setValue(data);

    this.log('[test-page] Data loaded: ' + JSON.stringify(data));

    this.subscription.add(
      this.form
        .get('empty')
        .valueChanges.pipe(debounceTime(300))
        .subscribe((value) => this.log('[test-page] Value n°1: ' + JSON.stringify(value)))
    );

    this.subscription.add(
      this.form
        .get('enable')
        .valueChanges.pipe(debounceTime(300))
        .subscribe((value) => this.log('[test-page] Value n°2: ' + JSON.stringify(value)))
    );
  }

  doSubmit(_) {
    this.form.markAllAsTouched();
    this.log('[test-page] Form content: ' + JSON.stringify(this.form.value));
    this.log('[test-page] Form status: ' + this.form.status);
    if (this.form.invalid) {
      this.log('[test-page] Form errors: ' + JSON.stringify(this.form.errors));

      // DEBUG
      AppFormUtils.logFormErrors(this.form);
    }
  }

  log(message: string) {
    console.debug(message);

    if (this.showLogPanel) {
      this.logContent += message + '<br/>';
      this.cd.markForCheck();
    }
  }

  clearLogPanel() {
    this.logContent = '';
    this.cd.markForCheck();
  }

  stringify = JSON.stringify;

  /* -- protected methods -- */
}
