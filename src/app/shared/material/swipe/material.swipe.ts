import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  Provider,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { BehaviorSubject, isObservable, noop, Observable, Subscription } from 'rxjs';
import { isNil, isNotNil, joinPropertiesPath } from '../../functions';
import { InputElement } from '../../inputs';
import { AppFloatLabelType, DisplayFn, EqualsFn } from '../../form/field.model';
import { firstTruePromise } from '../../observables';
import { isMobile } from '../../platforms';
import { Swiper } from 'swiper/types';
import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';
import { MatButton } from '@angular/material/button';

const DEFAULT_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MatSwipeField),
  multi: true,
};

@Component({
  selector: 'mat-swipe-field',
  styleUrls: ['./material.swipe.scss'],
  templateUrl: './material.swipe.html',
  providers: [DEFAULT_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatSwipeField implements OnInit, InputElement, OnDestroy, ControlValueAccessor {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;
  private _value: any;
  private _subscription = new Subscription();
  private _itemsSubscription: Subscription;
  private _writing = false;
  private _tabindex: number;
  private _swiper: Swiper;
  protected previousDisabled: boolean;
  protected nextDisabled: boolean;
  protected showSlides = false;
  protected $items = new BehaviorSubject<any[]>(undefined);
  protected $loaded = new BehaviorSubject<boolean>(false);

  @Input() logPrefix = '[mat-swipe-field]';
  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() floatLabel: AppFloatLabelType;
  @Input() appearance: MatFormFieldAppearance;
  @Input() subscriptSizing: SubscriptSizing;
  @Input() placeholder: string;
  @Input() debug = false;
  @Input() required = false;
  @Input() mobile: boolean;
  @Input() clearable = false;
  @Input() equals: EqualsFn | null;
  @Input() displayWith: DisplayFn | null;
  @Input() displayAttributes: string[];
  @Input() appAutofocus: boolean;
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('class') classList: string;

  @Input() set tabindex(value: number) {
    if (this._tabindex !== value) {
      this._tabindex = value;
      setTimeout(() => this.updateTabIndex());
    }
  }

  get tabindex(): number {
    return this._tabindex;
  }

  @Input() set items(value: Observable<any[]> | any[]) {
    // Remove previous subscription on items, (if exits)
    if (this._itemsSubscription) {
      console.warn(`${this.logPrefix} Items received twice !`);
      this._subscription.remove(this._itemsSubscription);
      this._itemsSubscription.unsubscribe();
    }

    if (isObservable(value)) {
      this._itemsSubscription = value.subscribe((v) => this.loadItems(v));
      this._subscription.add(this._itemsSubscription);
    } else {
      if (value !== this.$items.getValue()) {
        this.loadItems(value as any[]);
      }
    }
  }

  get items(): Observable<any[]> | any[] {
    return this.$items.getValue();
  }

  get itemCount(): number {
    return (this.$items.getValue() || []).length;
  }

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('click') onClick = new EventEmitter<MouseEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('blur') blurred = new EventEmitter<FocusEvent>();
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output('focus') focused = new EventEmitter<FocusEvent>();

  @ViewChild('fakeInput') fakeInput: ElementRef;
  @ViewChild('swiper', { static: true }) swiperRef: ElementRef | undefined;
  @ViewChild('prevButton') prevButton: MatButton;
  @ViewChild('nextButton') nextButton: MatButton;

  get value(): any {
    return this._value;
  }

  get disabled(): any {
    return this.formControl.disabled;
  }

  get enabled(): any {
    return this.formControl.enabled;
  }

  protected getSwiper(): Swiper {
    this._swiper = this.swiperRef?.nativeElement?.swiper || this._swiper;
    return this._swiper;
  }

  constructor(
    protected cd: ChangeDetectorRef,
    @Optional() private formGroupDir: FormGroupDirective
  ) {}

  ngOnInit() {
    this._swiper = this.swiperRef.nativeElement.swiper as Swiper;
    this.formControl =
      this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-swipe-field>.");

    // Default values
    this.displayAttributes = this.displayAttributes || ['label', 'name'];
    this.displayWith = this.displayWith || ((obj) => obj && joinPropertiesPath(obj, this.displayAttributes));
    this.equals = this.equals || ((o1: any, o2: any) => o1 && o2 && o1.id === o2.id);
    this.mobile = isNotNil(this.mobile) ? this.mobile : isMobile(window);
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    this._value = undefined;
    this.$items.complete();
  }

  _onFocusFakeInput(event: FocusEvent) {
    event.preventDefault();

    // Hide the fake input
    if (this.fakeInput) {
      this.fakeInput.nativeElement.classList.add('hidden');
      this.fakeInput.nativeElement.tabIndex = -1;
    }

    // Focus on first button
    this.focus();
  }

  focus() {
    // show slides component
    this.showSlides = true;

    setTimeout(() => {
      if (this.prevButton) {
        this.prevButton.focus();
      }
      this.updateTabIndex();
    });

    if (isNil(this._value)) {
      // Slide to first and affect value
      this.slideTo(0);
      this.slideChanged();
    }

    this.markForCheck();
  }

  /**
   * This is a special case, because, this component has a temporary component displayed before the first focus event
   */
  private updateTabIndex() {
    if (isNil(this._tabindex) || this._tabindex === -1) return;

    if (this.fakeInput) {
      if (this.showSlides) {
        this.fakeInput.nativeElement.classList.add('hidden');
        this.fakeInput.nativeElement.tabIndex = -1;
      } else {
        this.fakeInput.nativeElement.classList.remove('hidden');
        this.fakeInput.nativeElement.tabIndex = this._tabindex;
      }
    }
    if (this.prevButton && this.nextButton) {
      this.prevButton._elementRef.nativeElement.tabIndex = this.showSlides ? this._tabindex : -1;
      this.nextButton._elementRef.nativeElement.tabIndex = this.showSlides ? this._tabindex + 1 : -1;
    }
    this.markForCheck();
  }

  writeValue(value: any): void {
    if (this._writing) return;

    this._writing = true;
    if (!this.equals(value, this._value)) {
      this._value = value;
      this.showSlides = isNotNil(this._value);
      this.updateSlides();
      setTimeout(() => this.updateTabIndex());
    }
    this._writing = false;

    this.markForCheck();
  }

  clearValue(event: Event) {
    event.stopPropagation();
    this._writing = true;
    if (this.debug) console.debug(`${this.logPrefix} clear value`);
    this._value = undefined;
    this.formControl.patchValue(null, { emitEvent: false });
    this.showSlides = false;
    setTimeout(() => {
      this.updateTabIndex();
      this.slideTo(0);
      setTimeout(() => {
        this._writing = false;
      }, 100);
    });
    this._onChangeCallback(null);
    this.markForCheck();
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.lockSwipes(isDisabled);
    this.updateButtons();
    this.cd.markForCheck();
  }

  previous() {
    const swiper = this.getSwiper();
    swiper.slidePrev();
    this.updateButtons();
  }

  next() {
    const swiper = this.getSwiper();
    swiper.slideNext();
    this.updateButtons();
  }

  reachStart(reached: boolean) {
    this.previousDisabled = reached;
  }

  reachEnd(reached: boolean) {
    this.nextDisabled = reached;
  }

  swiperLoaded() {
    this._swiper = this.swiperRef.nativeElement.swiper;
    this.$loaded.next(true);
  }

  slideChanged() {
    const activeIndex = this._swiper.activeIndex;
    if (this._writing || isNil(activeIndex)) return;

    this._writing = true;
    const value = (this.$items.getValue() || [])[activeIndex] || undefined;
    if (this.debug) console.debug(`${this.logPrefix} slide changed ActiveIndex:${activeIndex}, new value:${value}`);
    this.formControl.patchValue(value, { emitEvent: false });
    this._writing = false;
    this.checkIfTouched();
    this._onChangeCallback(value);
  }

  /* -- protected method -- */

  private lockSwipes(lock: boolean) {
    const swiper = this.getSwiper();
    swiper.allowSlideNext = !lock;
    swiper.allowSlidePrev = !lock;
    swiper.allowTouchMove = !lock;
    swiper.update();
  }

  private loadItems(items: any[]) {
    this.$loaded.next(false);
    this.$items.next(items);
    this.ready().then(() => this._swiper.update());
  }

  private checkIfTouched() {
    if (this.formControl.touched) {
      this.markForCheck();
      this._onTouchedCallback();
    }
  }

  private markForCheck() {
    this.cd.markForCheck();
  }

  private updateSlides() {
    if (this.showSlides) {
      const itemIndex = (this.$items.getValue() || []).findIndex((value) => this.equals(value, this._value));
      if (itemIndex >= 0) {
        this.slideTo(itemIndex);
      } else {
        console.warn(`${this.logPrefix} Item not found`, this._value);
        this.slideTo(0);
      }
    }
  }

  private slideTo(index: number) {
    const swiper = this.getSwiper();
    swiper.slideTo(index, 0, false);
    swiper.update();
    this.updateButtons();
  }

  private updateButtons() {
    const swiper = this.getSwiper();
    const activeIndex = swiper.activeIndex;
    if (this.debug) console.debug(`${this.logPrefix} update buttons ActiveIndex:${activeIndex}`);
    this.previousDisabled = activeIndex === 0 || this.disabled;
    this.nextDisabled = activeIndex === this.itemCount - 1 || this.disabled;
    this.markForCheck();
  }

  async ready(): Promise<void> {
    // Wait loaded
    if (this.$loaded.getValue() === false) {
      if (this.debug) console.debug(`${this.logPrefix} waiting slides to be ready...`);
      await firstTruePromise(this.$loaded);
    }
  }
}
