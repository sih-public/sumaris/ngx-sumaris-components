import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { MatSwipeField } from './material.swipe';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SharedPipesModule } from '../../pipes/pipes.module';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    IonicModule,
    MatButtonModule,
    MatIconModule,
    TranslateModule.forChild(),
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedPipesModule,
  ],
  exports: [MatSwipeField],
  declarations: [MatSwipeField],
})
export class SharedMatSwipeModule {}
