import { MatFormFieldAppearance, SubscriptSizing } from '@angular/material/form-field';

export const MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING: SubscriptSizing = 'fixed';

export const MAT_FORM_FIELD_DEFAULT_APPEARANCE: MatFormFieldAppearance = 'fill';
