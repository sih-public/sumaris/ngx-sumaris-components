import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { SharedFormGroupValidators, SharedValidators } from '../../../validator/validators';
import { AppFormUtils } from '../../../../core/form/form.utils';
import { DateUtils, toDateISOString } from '../../../dates';
import 'moment-timezone';
import { isMobile } from '../../../platforms';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-data-test',
  templateUrl: './mat-date.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTestPage implements OnInit {
  showLogPanel = true;
  logContent = '';

  memoryHide = false;
  memoryMobile = true;
  memoryTimer: Subscription;
  timezone = 'Indian/Mahe';

  form: UntypedFormGroup;
  mode: 'mobile' | 'desktop' | 'memory' | 'temp' = 'mobile';

  constructor(
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    this.form = formBuilder.group(
      {
        empty: [null, SharedValidators.validDate],
        emptyRequired: [null, Validators.compose([Validators.required, SharedValidators.validDate])],
        enable: [null, Validators.required],
        disable: [null, Validators.required],
        readonly: [null],
        timezone: [null],
        hint: [null],
      },
      {
        validators: SharedFormGroupValidators.dateRange('empty', 'enable'),
      }
    );

    const disableControl = this.form.get('disable');
    disableControl.disable();

    // Copy the 'enable' value, into the disable control
    this.form.get('enable').valueChanges.subscribe((value) => disableControl.setValue(value));

    this.showLogPanel = this.showLogPanel || isMobile(window);
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
      this.stopMemoryTimer();
    }
  }

  // Load the form with data
  async loadData() {
    const now = DateUtils.moment();

    const nowAtMahe = now.clone().tz(this.timezone).startOf('day');

    const data = {
      empty: null, // toDateISOString(now.clone().add(2, 'hours')),
      emptyRequired: null,
      enable: toDateISOString(now),
      disable: now.clone(),
      readonly: now.clone(),
      timezone: nowAtMahe,
      hint: now.clone(),
    };

    this.form.setValue(data);

    this.log('[test-page] Data loaded: ' + JSON.stringify(data));

    this.form
      .get('empty')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°1: ' + JSON.stringify(value)));

    this.form
      .get('enable')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°2: ' + JSON.stringify(value)));

    this.form
      .get('timezone')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value with timezone: ' + JSON.stringify(value)));
  }

  doSubmit(event) {
    this.form.markAllAsTouched();
    this.log('[test-page] Form content: ' + JSON.stringify(this.form.value));
    this.log('[test-page] Form status: ' + this.form.status);
    if (this.form.invalid) {
      this.log('[test-page] Form errors: ' + JSON.stringify(this.form.errors));

      // DEBUG
      AppFormUtils.logFormErrors(this.form);
    }
  }

  log(message: string) {
    console.debug(message);

    if (this.showLogPanel) {
      this.logContent += message + '<br/>';
      this.cd.markForCheck();
    }
  }

  clearLogPanel() {
    this.logContent = '';
    this.cd.markForCheck();
  }

  startMemoryTimer() {
    this.memoryTimer = timer(50, 50).subscribe(() => {
      this.memoryHide = !this.memoryHide;
      this.cd.markForCheck();
    });
  }

  stopMemoryTimer() {
    this.memoryTimer?.unsubscribe();
    this.memoryTimer = null;
    this.memoryHide = false;
  }

  stringify = JSON.stringify;

  /* -- protected methods -- */
}
