import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { debounceTime } from 'rxjs/operators';
import { SharedFormGroupValidators } from '../../../validator/validators';
import { AppFormUtils } from '../../../../core/form/form.utils';
import { DateUtils, MOMENT_NO_TIME_PROPERTY, toDateISOString } from '../../../dates';
import { isMobile } from '../../../platforms';
import { Subscription, timer } from 'rxjs';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-data-time-test',
  templateUrl: './mat-date-time.test.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTimeTestPage implements OnInit {
  showLogPanel = true;
  logContent = '';

  memoryHide = false;
  memoryMobile = true;
  memoryTimer: Subscription;

  form: UntypedFormGroup;
  mode: 'mobile' | 'desktop' | 'memory' | 'temp' = 'mobile';
  appearance: MatFormFieldAppearance = 'fill';

  noTimeProperty = MOMENT_NO_TIME_PROPERTY;

  constructor(
    protected platform: Platform,
    protected formBuilder: UntypedFormBuilder,
    protected cd: ChangeDetectorRef
  ) {
    this.form = formBuilder.group(
      {
        empty: [null],
        emptyRequired: [null, Validators.required],
        enable: [null, Validators.required],
        disable: [null, Validators.required],
        noTime: [null, Validators.required],
        readonly: [null],
        startDateTime: [null, Validators.required],
        endDateTime: [null, Validators.required],
        hint: [null],
      },
      {
        validators: SharedFormGroupValidators.dateRange('empty', 'enable'),
      }
    );

    const disableControl = this.form.get('disable');
    disableControl.disable();

    // Copy the 'enable' value, into the disable control
    this.form.get('enable').valueChanges.subscribe((value) => disableControl.setValue(value));

    this.showLogPanel = this.showLogPanel || isMobile(window);
  }

  ngOnInit() {
    setTimeout(() => this.loadData(), 250);
  }

  toggleMode(value) {
    if (this.mode !== value) {
      this.mode = value;
      this.stopMemoryTimer();
    }
  }

  toggleAppearance(event: MatCheckboxChange) {
    this.appearance = event.checked ? 'outline' : 'fill';
  }

  // Load the form with data
  async loadData() {
    const now = DateUtils.moment();
    const data = {
      empty: null, // toDateISOString(now.clone().add(2, 'hours'))
      emptyRequired: null,
      enable: toDateISOString(now),
      disable: now,
      noTime: DateUtils.markNoTime(DateUtils.resetTime(now)),
      readonly: now,
      startDateTime: toDateISOString(now),
      endDateTime: toDateISOString(now.add(1, 'd')),
      hint: null,
    };

    this.form.setValue(data);

    this.log('[test-page] Data loaded: ' + JSON.stringify(data));

    this.form
      .get('empty')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°1: ' + JSON.stringify(value)));

    this.form
      .get('enable')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°2: ' + JSON.stringify(value)));

    this.form
      .get('noTime')
      .valueChanges.pipe(debounceTime(300))
      .subscribe((value) => this.log('[test-page] Value n°3: ' + JSON.stringify(value)));
  }

  doSubmit(event?: Event) {
    this.form.markAllAsTouched();
    this.log('[test-page] Form content: ' + JSON.stringify(this.form.value));
    this.log('[test-page] Form status: ' + this.form.status);
    if (this.form.invalid) {
      this.log('[test-page] Form errors: ' + JSON.stringify(this.form.errors));

      // DEBUG
      AppFormUtils.logFormErrors(this.form);
    }
  }

  log(message: string) {
    console.debug(message);

    if (this.showLogPanel) {
      this.logContent += message + '<br/>';
      this.cd.markForCheck();
    }
  }

  clearLogPanel() {
    this.logContent = '';
    this.cd.markForCheck();
  }

  startMemoryTimer() {
    this.memoryTimer = timer(50, 50).subscribe(() => {
      this.memoryHide = !this.memoryHide;
      this.cd.markForCheck();
    });
  }

  stopMemoryTimer() {
    this.memoryTimer?.unsubscribe();
    this.memoryTimer = null;
    this.memoryHide = false;
  }

  stringify = JSON.stringify;

  /* -- protected methods -- */
}
