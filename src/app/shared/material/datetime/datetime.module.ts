import { NgModule } from '@angular/core';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDate } from './material.date';
import { MatDateTime } from './material.datetime';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatDateShort } from './material.dateshort';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MaskitoDirective } from '@maskito/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedPipesModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MaskitoDirective,
    TranslateModule.forChild(),
    SharedDirectivesModule,
    NgxMatTimepickerModule,
  ],
  exports: [MatIconModule, MatDate, MatDateTime, MatDateShort],
  declarations: [MatDate, MatDateTime, MatDateShort],
})
export class SharedMatDateTimeModule {}
