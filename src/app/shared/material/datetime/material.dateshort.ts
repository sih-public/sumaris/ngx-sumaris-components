import {
  AfterViewInit,
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Provider,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { isMoment, Moment } from 'moment';
import { KEYBOARD_HIDE_DELAY_MS } from '../../constants';
import { SharedValidators } from '../../validator/validators';
import { isBlankString, isNil, isNotNil, isNotNilBoolean, sleep } from '../../functions';
import { InputElement, setTabIndex } from '../../inputs';
import { Subscription } from 'rxjs';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldAppearance, MatFormFieldDefaultOptions, SubscriptSizing } from '@angular/material/form-field';
import { DateFilterFn, MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { isFocusableElement } from '../../focusable';
import { isMobile } from '../../platforms';
import { Keyboard } from '@capacitor/keyboard';
import { filter, skip } from 'rxjs/operators';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateUtils, fromDateISOString, toDateISOString } from '../../dates';
import { AppFloatLabelType } from '../../form/field.model';
import { MaskitoOptions } from '@maskito/core';
import { MaskitoDateMode, maskitoDateOptionsGenerator, maskitoWithPlaceholder } from '@maskito/kit';
import { MAT_FORM_FIELD_DEFAULT_APPEARANCE, MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING } from '../material.config';

const DEFAULT_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MatDateShort),
  multi: true,
};

const noop = () => {};

const I18N_KEYS = ['COMMON.DATE_YEAR_PATTERN', 'COMMON.DATE_YEAR_PLACEHOLDER'];

@Component({
  selector: 'mat-date-short-field',
  templateUrl: './material.dateshort.html',
  styleUrls: ['./material.dateshort.scss'],
  providers: [DEFAULT_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatDateShort implements OnInit, ControlValueAccessor, InputElement, AfterViewInit, OnDestroy {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;
  private readonly _subscription = new Subscription();
  private _writing = true;
  private _disabling = false;
  private _tabindex: number;
  private _readonly = false;
  private _appearance: MatFormFieldAppearance | null = null;
  private _subscriptSizing: SubscriptSizing | null = null;
  private readonly _defaultAppearance: MatFormFieldAppearance;
  private readonly _defaultSubscriptSizing: SubscriptSizing;

  protected textControl: UntypedFormControl;
  protected yearPattern: string;
  protected yearPlaceholder: string;
  protected locale: string;
  protected maskitoOptions: MaskitoOptions;

  @Input() formControl: UntypedFormControl;
  @Input() formControlName: string;
  @Input() placeholder: string;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input({ transform: booleanAttribute }) compact = false;
  @Input({ transform: booleanAttribute }) autofocus = false;
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input({ transform: booleanAttribute }) required: boolean;
  @Input() startDate: Moment | null = null;
  @Input() datePickerFilter: DateFilterFn<Moment>;

  @Input()
  set appearance(value: MatFormFieldAppearance) {
    this._appearance = value;
  }
  get appearance(): MatFormFieldAppearance {
    return this._appearance || this._defaultAppearance;
  }

  @Input()
  set subscriptSizing(value: SubscriptSizing) {
    this._subscriptSizing = value;
  }
  get subscriptSizing(): SubscriptSizing {
    return this._subscriptSizing || this._defaultSubscriptSizing;
  }

  @Input() set readonly(value: boolean) {
    this._readonly = value;
    this.markForCheck();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  @Input() set tabindex(value: number) {
    if (this._tabindex !== value) {
      this._tabindex = value;
      if (!this._writing) this.updateTabIndex();
    }
  }

  get tabindex(): number {
    return this._tabindex;
  }

  get value(): any {
    return this.formControl.value;
  }

  get disabled(): boolean {
    return this.formControl.disabled;
  }

  @ViewChild('datePicker') datePicker!: MatDatepicker<Moment>;
  @ViewChild('matInput', { static: false }) _matInput: ElementRef;

  constructor(
    private dateAdapter: MomentDateAdapter,
    private translate: TranslateService,
    private formBuilder: UntypedFormBuilder,
    private cd: ChangeDetectorRef,
    @Optional() private formGroupDir: FormGroupDirective,
    @Inject(MAT_FORM_FIELD_DEFAULT_OPTIONS) defaultOptions?: MatFormFieldDefaultOptions
  ) {
    this.locale = (translate.currentLang || translate.defaultLang).substring(0, 2);
    this._defaultSubscriptSizing = defaultOptions?.subscriptSizing || MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING;
    this._defaultAppearance = defaultOptions?.appearance || MAT_FORM_FIELD_DEFAULT_APPEARANCE;
  }

  ngOnInit() {
    this.formControl =
      this.formControl || (this.formControlName && this.formGroupDir && (this.formGroupDir.form.get(this.formControlName) as UntypedFormControl));
    if (!this.formControl) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-date-short-field>.");

    this.mobile = isNotNil(this.mobile) ? this.mobile : isMobile(window);
    this.required = isNotNilBoolean(this.required)
      ? this.required
      : this.formControl.hasValidator(Validators.required) || isBlankString(this.required);

    // Add 'validDate' validator (when existing validator are null or required, to be sure to keep it)
    if (!this.formControl.validator || this.formControl.validator === Validators.required) {
      this.formControl.setValidators(this.required ? [Validators.required, SharedValidators.validDate] : SharedValidators.validDate);
    } else {
      this.formControl.setValidators(
        this.required
          ? [this.formControl.validator, Validators.required, SharedValidators.validDate]
          : [this.formControl.validator, SharedValidators.validDate]
      );
    }

    // Create the text control
    this.textControl = this.formBuilder.control(
      null,
      // Important: should copy errors from the model formControl
      () => this.formControl.errors
    );

    // Get patterns to display date
    this.updateTranslations(this.translate.instant(I18N_KEYS));
    this._subscription.add(
      this.translate
        .get(I18N_KEYS)
        .pipe(skip(1))
        .subscribe((translations) => this.updateTranslations(translations))
    );

    this._subscription.add(this.textControl.valueChanges.subscribe((value) => this.onFormChange(value)));

    // Listen status changes (when done outside the component  - e.g. when setErrors() is calling on the formControl)
    this._subscription.add(
      this.formControl.statusChanges
        .pipe(
          filter((_) => !this.readonly && !this._writing && !this._disabling) // Skip
        )
        .subscribe(() => {
          this.textControl.updateValueAndValidity({ emitEvent: false });
          if (this.formControl.touched && !this.textControl.touched) {
            this.textControl.markAsTouched();
          }
          this.markForCheck();
        })
    );

    this._writing = false;
  }

  ngAfterViewInit() {
    this.updateTabIndex();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  writeValue(value: any): void {
    if (this._writing) return; // Skip
    this._writing = true;

    // DEBUG
    //console.debug("[mat-date] writeValue() with:", value);

    // Convert into date
    // Important: clone, because startOf will update the existing date
    const date = isMoment(value) ? value.clone() : fromDateISOString(value);

    if (!date || !date.isValid()) {
      this.textControl.patchValue(null, { emitEvent: false });
      if (this.formControl.value) {
        this.formControl.patchValue(null, { emitEvent: false });
        this._onChangeCallback(null);
      }
    } else {
      // Reset day
      const year = date.startOf('year');
      const yearStr = this.dateAdapter.format(year, this.yearPattern);

      if (this.textControl.value !== yearStr) {
        // Update text control
        this.textControl.patchValue(yearStr, { emitEvent: false });
      }
    }

    this._writing = false;
    this.markForCheck();
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (this._disabling) return;

    this._disabling = true;
    if (isDisabled) {
      this.textControl.disable({ emitEvent: false });
    } else {
      this.textControl.enable({ emitEvent: false });
    }
    this._disabling = false;

    this.markForCheck();
  }

  openDatePicker() {
    if (!this.datePicker) return;

    if (!this.datePicker.opened) {
      // DEBUG
      //console.debug('[mat-date-short] Opening date picker...');

      this.datePicker.open();
    }
  }

  focus() {
    if (!this._matInput) return;

    setTimeout(() => {
      if (isFocusableElement(this._matInput.nativeElement)) {
        this._matInput.nativeElement.focus();
      }
    });
  }

  clear(event?: Event) {
    this._preventEvent(event);

    this.textControl.patchValue(null, { emitEvent: false });
    this.formControl.setValue(null, { emitEvent: false });
    this._onChangeCallback(null);
    this.markAsTouched();
    this.markAsDirty();
  }

  /* -- protected methods -- */

  protected _onDatePickerChange(event: MatDatepickerInputEvent<Moment>): void {
    // Make sure event is valid
    if (!event || (event.value !== null && !isMoment(event.value))) {
      console.warn('Invalid MatDatepicker event. Skipping', event);
      return; // Skip
    }

    let value = event.value;

    // No value, but mobile mode: use the current year
    if (!value && this.mobile) {
      value = DateUtils.moment().locale(this.locale).startOf('year').utc(true);
      this.formControl.setValue(toDateISOString(value), {
        emitEvent: false, // Will call writeValue()
      });
      return;
    }

    const date = value?.clone().locale(this.locale).startOf('year').utc(true);
    const dateStr = (date && this.dateAdapter.format(date, this.yearPattern)) || null;

    if (this.textControl.value !== dateStr) {
      // DEBUG
      //console.debug('[mat-date-time] Emit new value: ' + dateStr);

      this.textControl.setValue(dateStr, {
        emitEvent: true, // Will call onFormChange
      });
    }
  }

  protected _openDatePickerIfMobile(event: Event) {
    if (!this.mobile || event?.defaultPrevented || this.datePicker?.opened || this.disabled) return;

    //console.debug('[mat-date-short] Preparing to open date picker... ');

    this._preventEvent(event);

    // Open the picker
    this.openDatePicker();
  }

  protected _checkIfTouched() {
    if (this.textControl.touched) {
      this._onTouchedCallback();

      this.markForCheck();
    }
  }

  // select only year
  protected _closeDatePicker(value: Moment) {
    this.datePicker?.close();

    // Get the model value
    const isoDate = toDateISOString(value);

    if (this.formControl.value !== isoDate) {
      this.formControl.setValue(isoDate);
    }
  }

  protected _preventEvent(event: Event) {
    if (!event) return;
    event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
    if (event.stopImmediatePropagation) event.stopImmediatePropagation();
  }

  /* -- private methods -- */

  private updateTranslations(translations: { [key: string]: string }) {
    const datePattern = translations['COMMON.DATE_YEAR_PATTERN'] !== 'COMMON.DATE_YEAR_PATTERN' ? translations['COMMON.DATE_YEAR_PATTERN'] : 'YYYY';
    this.updatePattern(datePattern);

    const yearPlaceholder =
      translations['COMMON.DATE_YEAR_PLACEHOLDER'] !== 'COMMON.DATE_YEAR_PLACEHOLDER'
        ? translations['COMMON.DATE_YEAR_PLACEHOLDER']
        : datePattern.toLowerCase();
    this.updateMaskOptions(yearPlaceholder);
  }

  private updatePattern(pattern: string) {
    if (this.yearPattern !== pattern) {
      this.yearPattern = pattern;
      this.markForCheck();
    }
  }

  private updateMaskOptions(yearPlaceholder: string) {
    if (this.yearPlaceholder !== yearPlaceholder) {
      this.yearPlaceholder = yearPlaceholder;

      const dateOptions = maskitoDateOptionsGenerator({
        mode: this.yearPattern.toLowerCase() as MaskitoDateMode,
      });

      const {
        plugins, // plugins keeps caret inside actual value and remove placeholder on blur
        ...placeholderOptions
        // pass 'true' as second argument to add plugin to hide placeholder when input is not focused
      } = maskitoWithPlaceholder(this.yearPlaceholder, true);

      this.maskitoOptions = {
        ...dateOptions,
        plugins: plugins.concat(dateOptions.plugins || []),
        preprocessors: [
          // Always put it BEFORE all other preprocessors
          ...placeholderOptions.preprocessors,
          ...dateOptions.preprocessors,
        ],
        postprocessors: [
          ...dateOptions.postprocessors,
          // Always put it AFTER all other postprocessors
          ...placeholderOptions.postprocessors,
        ],
      } as Required<MaskitoOptions>;

      this.markForCheck();
    }
  }

  private onFormChange(value: string): void {
    if (this._writing) return; // Skip if call by self
    this._writing = true;

    if (value === this.yearPlaceholder) {
      value = undefined;
    }

    // Parse year string
    let date = (value && this.dateAdapter.parse(value, this.yearPattern)) || null;

    // Reset time
    date = date && date.locale(this.locale).startOf('year');

    // Get the model value
    const dateStr = date && date.isValid() ? toDateISOString(date) : date;
    // Set model value
    if (this.formControl.value !== dateStr) {
      // DEBUG
      //console.debug('[mat-date-time] Emit new value: ' + dateStr);

      if (date?.isValid()) {
        // Update formControl value (will update datePicker also)
        this.formControl.setValue(dateStr, { emitEvent: false });
      }

      // Changes comes from inside function: use the callback
      this._onChangeCallback(dateStr);

      // Check if need to update controls
      this._checkIfTouched();

      // Force textControl validity
      this.textControl.updateValueAndValidity();
    }

    this._writing = false;
    this.markForCheck();
  }

  private async waitKeyboardHide(waitKeyboardDelay: boolean) {
    try {
      // Force keyboard to be hide
      await Keyboard.hide();

      // Wait an additional delay, to be sure the keyboard has been totally hidden
      await sleep(KEYBOARD_HIDE_DELAY_MS);
    } catch (e) {
      console.error(e);
    }
  }

  private updateTabIndex() {
    if (isNil(this._tabindex) || this._tabindex === -1) return; // skip

    // Focus to input
    setTimeout(() => {
      setTabIndex(this._matInput, this._tabindex);
      this.markForCheck();
    });
  }

  private markAsTouched(opts?: { onlySelf?: boolean }) {
    this.textControl.markAsTouched(opts);
    this._onTouchedCallback();
    this.markForCheck();
  }

  private markAsDirty(opts?: any) {
    this.formControl.markAsDirty(opts);
  }

  private markForCheck() {
    this.cd.markForCheck();
  }
}
