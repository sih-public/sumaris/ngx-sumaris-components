import {
  AfterViewInit,
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Provider,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldAppearance, MatFormFieldDefaultOptions, SubscriptSizing } from '@angular/material/form-field';
import { ControlValueAccessor, FormGroupDirective, NG_VALUE_ACCESSOR, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { isMoment, Moment } from 'moment';
import { SharedValidators } from '../../validator/validators';
import { filter, skip } from 'rxjs/operators';
import { InputElement, setTabIndex } from '../../inputs';
import { isFocusableElement } from '../../focusable';
import { merge, Subscription } from 'rxjs';
import { DateFilterFn, MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { isBlankString, isNil, isNotNil, isNotNilBoolean, isNotNilOrBlank, nullIfNilOrBlank } from '../../functions';
import { DateUtils, fromDateISOString, toDateISOString } from '../../dates';
import { isMobile } from '../../platforms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppFloatLabelType } from '../../form/field.model';
import { MaskitoOptions } from '@maskito/core';
import { MaskitoDateMode, maskitoDateOptionsGenerator, MaskitoTimeMode, maskitoTimeOptionsGenerator, maskitoWithPlaceholder } from '@maskito/kit';
import { MAT_FORM_FIELD_DEFAULT_APPEARANCE, MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING } from '../material.config';

const DEFAULT_VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => MatDateTime),
  multi: true,
};

const I18N_KEYS = ['COMMON.DATE_PATTERN', 'COMMON.TIME_PATTERN', 'COMMON.DATE_TIME_PATTERN', 'COMMON.DATE_PLACEHOLDER', 'COMMON.TIME_PLACEHOLDER'];

const noop = () => {};

declare interface NgxTimePicker {
  open();
  close();
}

@Component({
  selector: 'mat-date-time-field',
  templateUrl: './material.datetime.html',
  styleUrls: ['./material.datetime.scss'],
  providers: [DEFAULT_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatDateTime implements OnInit, AfterViewInit, OnDestroy, ControlValueAccessor, InputElement {
  private _onChangeCallback: (_: any) => void = noop;
  private _onTouchedCallback: () => void = noop;
  private _subscription = new Subscription();
  private _controlSubscription: Subscription;
  private _writing = true;
  private _disabling = false;
  private _tabindex: number;
  private _readonly = false;
  private _controlName: string;
  private _required: boolean;
  private _appearance: MatFormFieldAppearance | null = null;
  private _subscriptSizing: SubscriptSizing | null = null;
  private readonly _defaultAppearance: MatFormFieldAppearance;
  private readonly _defaultSubscriptSizing: SubscriptSizing;

  protected _formControl: UntypedFormControl;
  protected dateControl: UntypedFormControl;
  protected timeControl: UntypedFormControl;
  protected displayPattern: string;
  protected datePattern: string;
  protected timePattern: string;
  protected locale: string;
  protected maskitoDateOptions: MaskitoOptions;
  protected maskitoTimeOptions: MaskitoOptions;
  protected datePlaceholder: string;
  protected timePlaceholder: string;

  @Input() placeholder: string;
  @Input() floatLabel: AppFloatLabelType = 'auto';
  @Input({ transform: booleanAttribute }) mobile: boolean;
  @Input({ transform: booleanAttribute }) compact = false;
  @Input({ transform: booleanAttribute }) autofocus = false;
  @Input({ transform: booleanAttribute }) clearable = false;
  @Input() startDate: Moment | null = null;
  @Input() datePickerFilter: DateFilterFn<Moment>;
  @Input({ transform: booleanAttribute }) allowNoTime = false;

  @Input()
  set appearance(value: MatFormFieldAppearance) {
    this._appearance = value;
  }
  get appearance(): MatFormFieldAppearance {
    return this._appearance || this._defaultAppearance;
  }

  @Input()
  set subscriptSizing(value: SubscriptSizing) {
    this._subscriptSizing = value;
  }
  get subscriptSizing(): SubscriptSizing {
    return this._subscriptSizing || this._defaultSubscriptSizing;
  }

  @Input() set formControl(value: UntypedFormControl) {
    this._setFormControl(value);
  }
  get formControl(): UntypedFormControl {
    return this._formControl;
  }

  @Input() set formControlName(value: string) {
    this._setFormControlName(value);
  }
  get formControlName(): string {
    return this._controlName;
  }

  @Input() set required(value: boolean | any) {
    if (this._required === value) return; // SKip

    this._required = value;
    if (this._formControl) {
      this._updateControlValidators(this._formControl, value);
    }
  }
  get required(): boolean | any {
    return this._required;
  }

  get requiredTime(): boolean | any {
    return this._required && !this.allowNoTime;
  }

  @Input() set readonly(value: boolean) {
    this._readonly = value;
    this.markForCheck();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  @Input() set tabindex(value: number) {
    if (this._tabindex !== value) {
      this._tabindex = value;
      if (!this._writing) this.updateTabIndex();
    }
  }

  get tabindex(): number {
    return this._tabindex;
  }

  get value(): any {
    return this.formControl.value;
  }

  get disabled(): boolean {
    return this.formControl.disabled;
  }

  @ViewChild('datePicker') datePicker: MatDatepicker<Moment>;
  @ViewChild('timePicker') timePicker: NgxTimePicker;
  @ViewChildren('matInput') _matInputs: QueryList<ElementRef>;

  constructor(
    private dateAdapter: MomentDateAdapter,
    private translate: TranslateService,
    private formBuilder: UntypedFormBuilder,
    private cd: ChangeDetectorRef,
    @Optional() private formGroupDir: FormGroupDirective,
    @Inject(MAT_FORM_FIELD_DEFAULT_OPTIONS) defaultOptions?: MatFormFieldDefaultOptions
  ) {
    this.locale = (translate.currentLang || translate.defaultLang).substring(0, 2);
    this._defaultSubscriptSizing = defaultOptions?.subscriptSizing || MAT_FORM_FIELD_DEFAULT_SUBSCRIPT_SIZING;
    this._defaultAppearance = defaultOptions?.appearance || MAT_FORM_FIELD_DEFAULT_APPEARANCE;
  }

  ngOnInit() {
    const control = this._formControl || (this._controlName && (this.formGroupDir?.form.get(this._controlName) as UntypedFormControl));
    if (!control) throw new Error("Missing mandatory attribute 'formControl' or 'formControlName' in <mat-date-time-field>.");

    // Default values
    this.mobile = isNotNil(this.mobile) ? this.mobile : isMobile(window);
    this._required = isNotNilBoolean(this._required) ? this._required : control.hasValidator(Validators.required) || isBlankString(this._required);

    this._setFormControl(control);

    // Create the date control
    this.dateControl = this.formBuilder.control(null);

    // Create the time control
    this.timeControl = this.formBuilder.control(null);

    // Get patterns to display date and date+time
    this.updateTranslations(this.translate.instant(I18N_KEYS));
    this._subscription.add(
      this.translate
        .get(I18N_KEYS)
        .pipe(skip(1))
        .subscribe((translations) => this.updateTranslations(translations))
    );

    this._subscription.add(
      merge(this.dateControl.valueChanges, this.timeControl.valueChanges).subscribe(() =>
        this.onFormChange(this.dateControl.value, this.timeControl.value)
      )
    );

    this._writing = false;
  }

  ngAfterViewInit() {
    this.updateTabIndex();
    // Enable animations now. This ensures we don't animate on initial render.
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
    this._controlSubscription?.unsubscribe();
  }

  writeValue(value: any): void {
    if (this._writing) return; // Skip
    this._writing = true;

    // Convert into date
    const moment = isMoment(value) ? value : fromDateISOString(value);

    // DEBUG
    //console.debug("[mat-date-time] writeValue() with:", value);

    if (!moment || !moment.isValid()) {
      this.dateControl.patchValue(null, { emitEvent: false });
      this.timeControl.patchValue(null, { emitEvent: false });
    } else {
      const date = moment
        // Important: clone, because startOf() will update the existing date
        .clone()
        // Reset time
        .startOf('day');
      const dateStr = this.dateAdapter.format(date, this.datePattern);

      // Format time
      let timeStr: string;
      if (DateUtils.isNoTime(moment)) {
        // Clear time
        timeStr = null;
      } else {
        // - Format hh
        let hour: number | string = moment.hour();
        hour = +hour < 10 ? '0' + hour : hour;
        // - Format mm
        let minutes: number | string = moment.minutes();
        minutes = +minutes < 10 ? '0' + minutes : minutes;
        timeStr = `${hour}:${minutes}`;
      }

      // Update controls
      this.dateControl.patchValue(dateStr, { emitEvent: false });
      this.timeControl.patchValue(timeStr, { emitEvent: false });
    }

    this._writing = false;
    this.markForCheck();
  }

  registerOnChange(fn: any): void {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (this._disabling) return; // Skip
    this._disabling = true;

    if (isDisabled) {
      this.dateControl.disable({ emitEvent: false });
      this.timeControl.disable({ emitEvent: false });
    } else {
      this.dateControl.enable({ emitEvent: false });
      this.timeControl.enable({ emitEvent: false });
    }

    this._disabling = false;
    this.markForCheck();
  }

  openDatePicker(event?: Event, datePicker?: MatDatepicker<any>) {
    datePicker = datePicker || this.datePicker;
    if (datePicker) {
      this._preventEvent(event);

      if (!datePicker.opened) {
        datePicker.open();
      }
    }
  }

  openTimePicker(event?: Event) {
    if (!this.timePicker || this.disabled) return; // Skip

    this._preventEvent(event);

    this.timePicker.open();
  }

  focus() {
    const matInput = this._matInputs.first;
    if (!matInput) return;

    setTimeout(() => {
      if (isFocusableElement(matInput.nativeElement)) {
        matInput.nativeElement.focus();
      }
    });
  }

  clear(event?: Event) {
    this._preventEvent(event);

    this._formControl.setValue(null);
    this.markAsTouched();
    this.markAsDirty();
  }

  /* -- protected functions -- */

  protected _setFormControlName(controlName: string) {
    if (!controlName || this._controlName === controlName) return; // Skip

    this._controlName = controlName;

    const control = this.formGroupDir?.form?.get(controlName) as UntypedFormControl;
    if (control) this._setFormControl(control);
  }

  protected _setFormControl(control: UntypedFormControl) {
    if (!control) return; // Skip

    if (this._formControl !== control) {
      this._formControl = control;

      // Listen status changes (when done outside the component  - e.g. when setErrors() is calling on the formControl)
      this._controlSubscription?.unsubscribe();
      this._controlSubscription = control.statusChanges
        .pipe(
          filter((_) => !this._readonly && !this._writing && !this._disabling) // Skip
        )
        .subscribe((_) => {
          // DEBUG
          //console.debug('[mat-date-time] updateValueAndValidity of sub controls');

          this.dateControl.updateValueAndValidity({ emitEvent: false });
          this.timeControl.updateValueAndValidity({ emitEvent: false });
          if (this._formControl.touched) {
            if (!this.dateControl.touched) this.dateControl.markAsTouched();
            if (!this.timeControl.touched) this.timeControl.markAsTouched();
          }

          //console.debug('[mat-date-time] timeControl.error ?', this.timeControl.errors);
          this.markForCheck();
        });
    }

    // Update validators
    this._updateControlValidators(control, this._required);
  }

  protected _updateControlValidators(control: UntypedFormControl, required: boolean) {
    if (!control) return;

    // Add 'validDate' validator
    {
      const validator = this.allowNoTime ? SharedValidators.validDateAllowNoTime : SharedValidators.validDate;
      const inverseValidator = this.allowNoTime ? SharedValidators.validDate : SharedValidators.validDateAllowNoTime;
      if (!control.hasValidator(validator)) {
        control.addValidators(validator);
      }
      if (control.hasValidator(inverseValidator)) {
        control.removeValidators(inverseValidator);
      }
    }

    // Add required validator (if need)
    if (required) {
      if (!control.hasValidator(Validators.required)) {
        control.addValidators(Validators.required);
      }
    } else {
      if (control.hasValidator(Validators.required)) {
        control.removeValidators(Validators.required);
      }
    }
  }

  protected _onDatePickerChange(event: MatDatepickerInputEvent<Moment>): void {
    //console.debug(`[${this.constructor.name}._onDatePickerChange]`);

    // Make sure event is valid
    if (!event || (event.value !== null && !isMoment(event.value))) {
      console.warn('Invalid MatDatepicker event. Skipping', event);
      return; // Skip
    }

    let moment = event.value;

    // No value, but mobile mode: use the current date
    if (!moment && this.mobile) {
      this._writing = true;
      moment = DateUtils.resetTime(DateUtils.moment().locale(this.locale));
      this._formControl.setValue(moment, { emitEvent: false });
      this.markForCheck();
      this._writing = false;
    }

    // Convert to usable date, then convert to date pattern (e.g DD/MM/YYYY)
    const date = moment
      ?.locale(this.locale) // set locale
      .hour(0)
      .minute(0)
      .seconds(0)
      .millisecond(0) // Reset hour
      .utc(true);
    const dateStr = (date && this.dateAdapter.format(date, this.datePattern)) || null;

    // Update the date control
    if (this.dateControl.value !== dateStr) {
      // DEBUG
      // console.debug("[mat-date-time] onDatePickerChange() new value:", dateStr);

      this.dateControl.setValue(dateStr, {
        emitEvent: true, // Will call onFormChange
      });
    }

    if (this.mobile) {
      // Loop if invalid
      if (!event.value && this.required) {
        if (this.required) this._openDatePickerIfMobile();
      } else {
        this._openTimePickerIfMobile();
      }
    }
    this._writing = false;
  }

  protected _onTimePickerChange(timeStr: string) {
    // Update the time control, if need
    if (this.timeControl.value !== timeStr) {
      // DEBUG
      // console.debug("[mat-date-time] onTimePickerChange() new value:", timeStr);

      this.timeControl.setValue(timeStr, {
        emitEvent: true, // Will call onFormChange
      });
    }
  }

  protected _openDatePickerIfMobile(event?: Event, datePicker?: MatDatepicker<any>) {
    if (!this.mobile || event?.defaultPrevented || this.disabled) return;

    this._preventEvent(event);

    // Open the picker
    this.openDatePicker(null, datePicker);
  }

  protected _openTimePickerIfMobile(event?: Event) {
    if (!this.mobile || event?.defaultPrevented || this.disabled) return;

    // Open the picker
    this.openTimePicker(event);
  }

  protected _onBlur() {
    if (this.dateControl.touched || this.timeControl.touched) {
      const dateStr = this.dateControl.value;

      // Auto fill data time/in case of partial input
      if (isNotNilOrBlank(dateStr)) {
        const timeStr = this.timeControl.value;
        const date: Moment = this.dateAdapter.parse(dateStr, this.datePattern);
        let validDate = true;
        if (isNotNilOrBlank(timeStr)) {
          const hourParts = (timeStr || '').split(':');
          const hour = parseInt(hourParts[0] || 0);
          const minutes = parseInt(hourParts[1] || 0);
          date.locale(this.locale).hour(hour).minute(minutes).seconds(0).millisecond(0);
        }
        // No time, and not required: mark
        else {
          DateUtils.markNoTime(date);
          validDate = this.allowNoTime;
        }

        this.writeValue(date);

        if (validDate) {
          this._formControl.setValue(date, { emitEvent: false });
          this.formControl.updateValueAndValidity();
        } else {
          // Mark control as invalid, because time is missing
          this._formControl.markAsPending({ onlySelf: true });
          this._formControl.setErrors({
            validDate: true,
            ...this._formControl.errors,
          });
        }
      }
    }

    // Check if touched
    this._checkIfTouched();
  }

  protected _checkIfTouched() {
    if (this.dateControl.touched || this.timeControl.touched) {
      this._onTouchedCallback();

      this.markForCheck();
    }
  }

  protected _preventEvent(event?: Event) {
    if (!event) return;
    event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
    if (event.stopImmediatePropagation) event.stopImmediatePropagation();
  }

  /* -- private method -- */

  private updateTranslations(translations: { [key: string]: string }) {
    this.datePattern = translations['COMMON.DATE_PATTERN'] !== 'COMMON.DATE_PATTERN' ? translations['COMMON.DATE_PATTERN'] : 'DD/MM/YYYY';
    this.timePattern = translations['COMMON.TIME_PATTERN'] !== 'COMMON.TIME_PATTERN' ? translations['COMMON.TIME_PATTERN'] : 'HH:MM';

    const displayPattern =
      translations['COMMON.DATE_TIME_PATTERN'] !== 'COMMON.DATE_TIME_PATTERN' ? translations['COMMON.DATE_TIME_PATTERN'] : 'L LT';
    this.updateDisplayPattern(displayPattern);

    const datePlaceholder =
      translations['COMMON.DATE_PLACEHOLDER'] !== 'COMMON.DATE_PLACEHOLDER'
        ? translations['COMMON.DATE_PLACEHOLDER']
        : this.datePattern?.toLowerCase();
    this.updateDateMaskOptions(datePlaceholder);

    const timePlaceholder =
      translations['COMMON.TIME_PLACEHOLDER'] !== 'COMMON.TIME_PLACEHOLDER'
        ? translations['COMMON.TIME_PLACEHOLDER']
        : this.timePattern?.toLowerCase();
    this.updateTimeMaskOptions(timePlaceholder);
  }

  private updateDisplayPattern(displayPattern: string) {
    if (this.displayPattern !== displayPattern) {
      this.displayPattern = displayPattern;
      this.markForCheck();
    }
  }

  private updateDateMaskOptions(datePlaceholder: string) {
    if (this.datePlaceholder !== datePlaceholder) {
      this.datePlaceholder = datePlaceholder;

      const dateOptions = maskitoDateOptionsGenerator({
        mode: this.datePattern.toLowerCase() as MaskitoDateMode,
        separator: '/',
      });

      const {
        plugins, // plugins keeps caret inside actual value and remove placeholder on blur
        ...placeholderOptions
        // pass 'true' as second argument to add plugin to hide placeholder when input is not focused
      } = maskitoWithPlaceholder(this.datePlaceholder, true);

      this.maskitoDateOptions = {
        ...dateOptions,
        plugins: plugins.concat(dateOptions.plugins || []),
        preprocessors: [
          // Always put it BEFORE all other preprocessors
          ...placeholderOptions.preprocessors,
          ...dateOptions.preprocessors,
        ],
        postprocessors: [
          ...dateOptions.postprocessors,
          // Always put it AFTER all other postprocessors
          ...placeholderOptions.postprocessors,
        ],
      } as Required<MaskitoOptions>;

      this.markForCheck();
    }
  }

  private updateTimeMaskOptions(timePlaceholder: string) {
    if (this.timePlaceholder !== timePlaceholder) {
      this.timePlaceholder = timePlaceholder;

      const timeOptions = maskitoTimeOptionsGenerator({
        mode: this.timePattern.toUpperCase() as MaskitoTimeMode,
      });

      const {
        plugins, // plugins keeps caret inside actual value and remove placeholder on blur
        ...placeholderOptions
        // pass 'true' as second argument to add plugin to hide placeholder when input is not focused
      } = maskitoWithPlaceholder(this.timePlaceholder, true);

      this.maskitoTimeOptions = {
        ...timeOptions,
        plugins: plugins.concat(timeOptions.plugins || []),
        preprocessors: [
          // Always put it BEFORE all other preprocessors
          ...placeholderOptions.preprocessors,
          ...timeOptions.preprocessors,
        ],
        postprocessors: [
          ...timeOptions.postprocessors,
          // Always put it AFTER all other postprocessors
          ...placeholderOptions.postprocessors,
        ],
      } as Required<MaskitoOptions>;

      this.markForCheck();
    }
  }

  private onFormChange(dateStr: string, timeStr: string) {
    if (this._writing) return; // Skip if call by self
    this._writing = true;

    // Clean up
    if (dateStr === this.datePlaceholder) {
      dateStr = null;
    } else {
      dateStr = nullIfNilOrBlank(dateStr);
    }
    if (timeStr === this.timePlaceholder) {
      timeStr = null;
    } else {
      timeStr = nullIfNilOrBlank(timeStr);
    }

    // DEBUG
    //console.debug(`[mat-date-time] onFormChange() - controls values: `, [dateStr, timeStr]);

    const incompleteValue = !this.allowNoTime && isNil(timeStr) !== isNil(dateStr);
    if (incompleteValue || this.dateControl.invalid || this.timeControl.invalid) {
      //console.debug(`[mat-date-time] onFormChange() error -> incompleteValue: ${incompleteValue}`);

      this._formControl.markAsPending({ onlySelf: true });
      this._formControl.setErrors({
        validDate: incompleteValue,
        ...this._formControl.errors,
        ...this.dateControl.errors,
        ...this.timeControl.errors,
      });
      if (this.dateControl.dirty || this.timeControl.dirty) {
        this._formControl.markAsDirty();
      }

      this._writing = false;
      return;
    }

    // Force '0' for empty month (otherwise moment will use the current month)
    dateStr = dateStr
      ?.replace(/[^\d/]/g, '') // Keep only digits and slashes
      .replace('//', '/0/')
      // Force '0' for empty year (otherwise moment will use the current year)
      .replace(/\/$/, '/0');

    // Parse date
    const date: Moment = (dateStr && this.dateAdapter.parse(dateStr, this.datePattern)) || null;

    // Parse time
    const hourParts = (timeStr || '').split(':');
    const hour = parseInt(hourParts[0] || '0');
    const minutes = parseInt(hourParts[1] || '0');
    const dateTime = date
      ?.locale(this.locale) // set as time as locale time
      .hour(hour)
      .minute(minutes) // Set local hour
      .seconds(0)
      .millisecond(0) // Reset seconds/millisecond
      .utc(); // Convert to UTC (avoid TZ offset in final string)

    // Set model value
    this.emitChange(dateTime, dateStr);

    this._writing = false;
    this.markForCheck();
  }

  private emitChange(date: Moment, defaultValue?: string) {
    // Convert to model value (ISO date string)
    // IMPORTANT: If date is invalid, then keep the invalid string value
    // to allow SharedValidators.validDate() to raise an error
    const controlValue = date?.isValid() ? toDateISOString(date) : defaultValue || null;

    if (this._formControl.value !== controlValue) {
      // DEBUG
      //console.debug('[mat-date-time] Emit new value: ' + controlValue);

      // Update formControl value (this is need to update datePicker also, in desktop mode)
      if (!this.mobile && date?.isValid()) {
        this.formControl.setValue(controlValue, { emitEvent: false });
      }

      // Changes comes from inside function: use the callback
      this._onChangeCallback(controlValue);

      // Check if need to update controls
      this._checkIfTouched();
    }
  }

  private updateTabIndex() {
    if (isNil(this._tabindex) || this._tabindex === -1) return; // skip

    // Focus to first input
    setTimeout(() => {
      this._matInputs.forEach((elementRef, index) => {
        setTabIndex(elementRef, this._tabindex + index);
      });
      this.markForCheck();
    });
  }

  private markAsTouched(opts?: { onlySelf?: boolean }) {
    this.dateControl.markAsTouched(opts);
    this.timeControl.markAsTouched(opts);
    this._onTouchedCallback();
    this.markForCheck();
  }

  private markAsDirty(opts?: any) {
    this._formControl.markAsDirty(opts);
  }

  private markForCheck() {
    this.cd.markForCheck();
  }
}
