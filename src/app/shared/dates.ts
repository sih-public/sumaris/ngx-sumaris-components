import * as momentImported from 'moment';
import { Duration, DurationInputArg1, DurationInputArg2, isMoment, Moment, MomentFormatSpecification, MomentInput, unitOfTime } from 'moment/moment';
import { DATE_ISO_PATTERN, DATE_MATCH_REGEXP, DATE_PATTERN } from './constants';
import { unwrapESModule } from './modules';

// // Workaround for issue https://github.com/ng-packagr/ng-packagr/issues/2215
declare type MomentFn = {
  (inp?: MomentInput, strict?: boolean): Moment;
  (inp?: MomentInput, format?: MomentFormatSpecification, strict?: boolean): Moment;
  (inp?: MomentInput, format?: MomentFormatSpecification, language?: string, strict?: boolean): Moment;
  duration: (inp?: DurationInputArg1, unit?: DurationInputArg2) => Duration;
};
const _moment = unwrapESModule<MomentFn>(momentImported);

export const DATE_UNIX_TIMESTAMP = 'X';
export const DATE_UNIX_MS_TIMESTAMP = 'x';
export const MOMENT_NO_TIME_PROPERTY = '_hasNoTime';

export class DateUtils {
  static moment = _moment;

  static toDateISOString = toDateISOString;
  static fromDateISOString = fromDateISOString;
  static toDuration = toDuration;

  static min(date1: Moment, date2: Moment): Moment {
    const d1 = fromDateISOString(date1);
    const d2 = fromDateISOString(date2);
    return d1 && (!d2 || d1.isSameOrBefore(d2)) ? d1 : d2;
  }

  static max(date1: Moment | string, date2: Moment | string): Moment {
    const d1 = fromDateISOString(date1);
    const d2 = fromDateISOString(date2);
    return d1 && (!d2 || d1.isSameOrAfter(d2)) ? d1 : d2;
  }

  static equals(date1: Moment | string, date2: Moment | string): boolean {
    return DateUtils.isSame(date1, date2);
  }

  static isSame(date1: Moment | string, date2: Moment | string, granularity?: unitOfTime.StartOf): boolean {
    const d1 = fromDateISOString(date1);
    const d2 = fromDateISOString(date2);
    return (!d1 && !d2) || (d1 && d1.isSame(d2, granularity));
  }

  static compare(date1: Moment | string, date2: Moment | string, granularity?: unitOfTime.StartOf): number {
    const d1 = fromDateISOString(date1);
    const d2 = fromDateISOString(date2);
    if ((!d1 && !d2) || d1?.isSame(d2, granularity)) return 0; // Equals
    if (d1?.isAfter(d2, granularity)) return 1; // After
    return -1; // Before
  }

  /**
   * Calculates the absolute difference between two dates in the specified units.
   *
   * @param {Moment | string} date1 - The first date to compare.
   * @param {Moment | string} date2 - The second date to compare.
   * @param {unitOfTime.Base} [durationUnit='seconds'] - The unit of time to express the difference in (default is 'seconds').
   * @return {number} The absolute difference between the two dates expressed in the specified units.
   */
  static diffAbs(date1: Moment | string, date2: Moment | string, durationUnit: unitOfTime.Base = 'seconds'): number {
    const d1 = fromDateISOString(date1);
    const d2 = fromDateISOString(date2);
    if (!d1 || !d2) throw new Error('Missing 2 first required arguments'); // Cannot compute
    return DateUtils.moment.duration(d1.diff(d2)).abs().as(durationUnit);
  }

  /**
   * Create a copy of a date, without time fields (always return a new Moment object, or undefined).
   * Same implementation as the Java class Dates.resetTime() (see any SUMARiS like Pod)
   *
   * @param value
   * @param timezone a timezone (see https://momentjs.com/timezone/)
   * @param keepLocalTime if true, only the timezone (and offset) is updated, keeping the local time same. Consequently, it will now point to a different point in time if the offset has changed.
   */
  static resetTime(value: Moment | string, timezone?: string, keepLocalTime?: boolean): Moment | undefined {
    if (!value) return undefined;
    const date = fromDateISOString(value);
    // No timezone
    if (!timezone) {
      return date.clone().startOf('day');
    }
    // Use timezone
    return date
      .clone() // clone the original date
      .tz(timezone, keepLocalTime)
      .startOf('day');
  }

  static markNoTime(value: Moment): Moment | undefined {
    if (!value) return undefined;
    value[MOMENT_NO_TIME_PROPERTY] = true;
    return value;
  }

  static markTime(value: Moment): Moment | undefined {
    if (!value) return undefined;
    delete value[MOMENT_NO_TIME_PROPERTY];
    return value;
  }

  static isNoTime(value: Moment): boolean {
    return value?.[MOMENT_NO_TIME_PROPERTY] === true;
  }

  /**
   * Test if a date is on the given day of week
   *
   * @param date
   * @param weekday
   * @param timezone
   */
  static isAtDay(date: string | Moment, weekday: number, timezone?: string): boolean {
    let momentDate = date && fromDateISOString(date);
    if (!momentDate) return null;
    if (timezone) momentDate = momentDate.clone().tz(timezone).startOf('day');
    return momentDate.day() === weekday;
  }

  static durationToString(
    value: number,
    dayUnit: string,
    opts?: unitOfTime.DurationConstructor | { unit?: unitOfTime.DurationConstructor; seconds?: boolean } | any
  ): string {
    if (!value) return '';
    const unit: unitOfTime.DurationConstructor = opts?.unit || opts || 'hours';

    // try with moment
    const duration = toDuration(value, unit);

    const days = duration.days();
    const hours = duration.hours().toString().padStart(2, '0');
    const minutes = duration.minutes().toString().padStart(2, '0');
    const seconds = opts?.seconds ? duration.seconds().toString().padStart(2, '0') : null;

    return (days > 0 ? days.toString() + (dayUnit + ' ') : '') + hours + ':' + minutes + (seconds ? ':' + seconds : '');
  }
}

export function toDateISOString(value: any): string | undefined {
  if (!value) return undefined;

  // Already a valid ISO date time string (without timezone): use it
  if (typeof value === 'string' && value.indexOf('+') === -1 && value.lastIndexOf('Z') === value.length - 1) {
    return value;
  }
  // Make sure to have a Moment object
  const date = fromDateISOString(value);
  if (!date) return undefined;
  return DateUtils.isNoTime(date) ? date.toISOString(true /* important ! */).substring(0, 10) : date.toISOString();
}

export function fromDateISOString(value: any): Moment | undefined {
  // Already a moment object: use it
  if (!value || isMoment(value)) return value;

  if (typeof value === 'string' && !!value.match(DATE_MATCH_REGEXP)) {
    // Parse the input value, as a date only
    const dateOnly: Moment = DateUtils.moment(value, DATE_PATTERN, true);
    return DateUtils.markNoTime(dateOnly);
  }

  // Parse the input value, as a ISO date time
  const date: Moment = DateUtils.moment(value, DATE_ISO_PATTERN);
  if (date.isValid()) return date;

  // Not valid: trying to convert from unix timestamp
  if (typeof value === 'string') {
    console.warn('Wrong date format - Trying to convert from local time: ' + value);
    if (value.length === 10) {
      return DateUtils.moment(value, DATE_UNIX_TIMESTAMP);
    } else if (value.length === 13) {
      return DateUtils.moment(value, DATE_UNIX_MS_TIMESTAMP);
    }
  }

  console.warn('Unable to parse date: ' + value);
  return undefined;
}

export function fromUnixTimestamp(timeInSec: number) {
  return DateUtils.moment(timeInSec, DATE_UNIX_TIMESTAMP);
}
export function fromUnixMsTimestamp(timeInMs: number) {
  return DateUtils.moment(timeInMs, DATE_UNIX_MS_TIMESTAMP);
}

export function toDuration(value: number, unit?: unitOfTime.DurationConstructor): Duration {
  if (!value) return undefined;

  const duration = DateUtils.moment.duration(value, unit);

  // fix 990+ ms
  if (duration.milliseconds() >= 990) {
    duration.add(1000 - duration.milliseconds(), 'ms');
  }
  // fix 59 s
  if (duration.seconds() >= 59) {
    duration.add(60 - duration.seconds(), 's');
  }

  return duration;
}
