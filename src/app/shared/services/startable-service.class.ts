import { Directive, Optional } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface IStartableService<T = any> {
  started: boolean;
  start(): Promise<T>;
  stop(): Promise<void>;
  ready(): Promise<T>;
}

export function isStartableService(data: any): data is IStartableService {
  return typeof data?.['start'] === 'function' && typeof data?.['stop'] === 'function' && typeof data?.['ready'] === 'function';
}

@Directive()
export abstract class StartableService<T = any, O = any> implements IStartableService<T> {
  readonly startSubject = new Subject<T>();
  readonly stopSubject = new Subject<void>();

  protected _debug = false;
  protected _startByReadyFunction = true; // should start when calling ready() ?
  protected _data: T = null;

  private _started = false;
  private _startPromise: Promise<T> = null;
  private _startPrerequisite: () => Promise<any> = null;
  private _subscription: Subscription = null;

  protected constructor(@Optional() prerequisiteService?: { ready: () => Promise<any> }) {
    this._startPrerequisite = prerequisiteService ? () => prerequisiteService.ready() : () => Promise.resolve();
  }

  start(opts?: O): Promise<T> {
    if (this._startPromise) return this._startPromise;
    if (this._started) return Promise.resolve(this._data);

    this._startPromise = this._startPrerequisite()
      .then(() => this.ngOnStart(opts))
      .then((data) => {
        this._data = data;

        this._started = true;
        this._startPromise = undefined;

        this.startSubject.next(data);

        return data;
      })
      .catch((err) => {
        if (!this.stopped) {
          console.error('Failed to start a service: ' + ((err && err.message) || err), err);
          this._startPromise = null;
          this._started = false;
        }
        return null;
      });
    return this._startPromise;
  }

  ready(): Promise<T> {
    if (this._started) return Promise.resolve(this._data);
    if (this._startPromise) return this._startPromise;
    if (this._startByReadyFunction) return this.start();

    return this.startSubject.pipe(takeUntil(this.stopSubject)).toPromise();
  }

  async stop() {
    try {
      this.unsubscribe();
      await this.ngOnStop();
    } catch (err) {
      console.error('Failed to stop a service: ' + ((err && err.message) || err), err);
    } finally {
      this._data = null;
      this._started = false;
      this._startPromise = undefined;
      this.stopSubject.next();
    }
  }

  async restart(opts?: O): Promise<T> {
    if (this._startPromise) await this._startPromise; // Wait end of previous start, if running
    if (this._started) await this.stop(); // Then stop if started
    return this.start(opts); // Then start again
  }

  get started(): boolean {
    return this._started;
  }

  get starting(): boolean {
    return !!this._startPromise;
  }

  get stopped(): boolean {
    return !this._started && !this._startPromise;
  }

  protected registerSubscription(sub: Subscription) {
    this._subscription = this._subscription || new Subscription();
    this._subscription.add(sub);
  }

  protected unregisterSubscription(sub: Subscription): void {
    this._subscription?.remove(sub);
  }

  protected unsubscribe() {
    this._subscription?.unsubscribe();
    this._subscription = null;
  }

  protected ngOnStop(): Promise<void> | void {
    // Can be overwritten by subclasses
  }

  protected abstract ngOnStart(opts?: O): Promise<T>;
}
