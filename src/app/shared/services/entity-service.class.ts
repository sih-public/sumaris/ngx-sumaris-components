import { Observable } from 'rxjs';
import { FetchPolicy, WatchQueryFetchPolicy } from '@apollo/client/core';
import { SortDirection } from '@angular/material/sort';
import { EmptyObject } from '../../core/graphql/graphql.utils';

export declare interface Page {
  offset: number;
  size: number;
  sortBy?: string;
  sortDirection?: SortDirection;
}

export declare type FetchMoreFn<R, V = EmptyObject> = (variables?: V) => Promise<R>;

export declare interface LoadResult<T> {
  data: T[];
  total?: number;
  errors?: any[];
  fetchMore?: FetchMoreFn<LoadResult<T>>;
}

export declare type SuggestFn<T, F> = (
  value: any,
  filter?: F,
  sortBy?: string | keyof T,
  sortDirection?: SortDirection,
  opts?: {
    fetchPolicy?: FetchPolicy;
    offset?: number;
    size?: number;
    [key: string]: any;
  }
) => Promise<T[] | LoadResult<T>>;

export declare interface SuggestService<T, F> {
  suggest: SuggestFn<T, F>;
}

export declare interface EntityServiceLoadOptions<E = any> {
  fetchPolicy?: FetchPolicy;
  trash?: boolean;
  toEntity?: boolean | ((source: any, opts?: any) => E);
  query?: any;
  variables?: any;
  [key: string]: any;
}

export declare interface EntityServiceWatchOptions<T = any> {
  fetchPolicy?: WatchQueryFetchPolicy;
  trash?: boolean;
  toEntity?: boolean | ((source: any, opts?: any) => T);
  query?: any;
  variables?: any;
  [key: string]: any;
}
export declare interface EntityServiceListenChangesOptions<T = any> {
  interval?: number;
  fetchPolicy?: FetchPolicy;
  toEntity?: boolean | ((source: any) => T);
  query?: any;
  variables?: any;
  [key: string]: any;
}

export declare interface IEntityService<T, ID = any, LO = EntityServiceLoadOptions<T>> {
  load(id: ID, opts?: LO): Promise<T>;

  canUserWrite(data: T, opts?: any): boolean;

  save(data: T, opts?: any): Promise<T>;

  delete(data: T, opts?: any): Promise<any>;

  listenChanges(id: ID, opts?: any): Observable<T | undefined>;
}

export declare interface EntitiesServiceLoadOptions<T = any> extends EntityServiceLoadOptions<T> {
  withTotal?: boolean;
}

export declare interface EntitiesServiceWatchOptions<T = any> extends EntityServiceWatchOptions<T> {
  withTotal?: boolean;
}

export declare interface IEntitiesService<T, F, O extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions<T>> {
  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<F>, options?: O): Observable<LoadResult<T>>;

  // TODO
  /*watchPage(
    page: Page<T>,
    filter?: F,
    options?: O
  ): Observable<LoadResult<T>>;*/

  saveAll(data: T[], opts?: any): Promise<T[]>;

  deleteAll(data: T[], opts?: any): Promise<any>;

  asFilter(filter: Partial<F>): F;
}

export declare type LoadResultByPageFn<T> = (offset: number, size: number) => Promise<LoadResult<T>>;

export interface IEntityFullService<T, ID, F, O extends EntitiesServiceWatchOptions & EntityServiceLoadOptions>
  extends IEntityService<T, ID, O>,
    IEntitiesService<T, F, O> {}

export function mergeLoadResult<T>(res1: LoadResult<T>, res2: LoadResult<T>): LoadResult<T> {
  return {
    data: (res1.data || []).concat(...res2.data),
    total: (res1.total || res1.data?.length || 0) + (res2.total || res2.data?.length || 0),
  };
}
