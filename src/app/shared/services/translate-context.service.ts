import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { isNil, isNilOrBlank, isNotNilOrBlank } from '../functions';

@Injectable({ providedIn: 'root' })
export class TranslateContextService {
  constructor(protected translate: TranslateService) {}

  get(key: string | string[], context?: string, interpolateParams?: any): Observable<any> {
    // No context: do a normal translate
    if (!key || isNil(context)) return this.translate.get(key, interpolateParams);

    if (Array.isArray(key)) {
      if (!key.length) return of();
      // Observe the first key, then add other
      return this.get(key[0]).pipe(
        map((translation) =>
          key
            .slice(1) // Skip 1, because we already have it
            .reduce(
              (map, k) => {
                map[k] = this.instant(k, context);
                return map;
              },
              // Init with the first value
              {
                [key[0]]: translation,
              }
            )
        )
      );
    }

    // Compute a contextual i18n key, using the context as suffix
    const contextualKeys = this.contextualKeys(key, context);

    // Return the contextual translation, or default of not exists
    return this.translate.get(contextualKeys, interpolateParams).pipe(map((translations) => this.findFirstValid(contextualKeys, translations)));
  }

  instant(key: string | string[], context?: string, interpolateParams?: any): any {
    // No context: do a normal translate
    if (!key || isNilOrBlank(context)) return this.translate.instant(key, interpolateParams);

    if (Array.isArray(key)) {
      return key.reduce((map, k) => {
        map[k] = this.instant(k, context);
        return map;
      }, {});
    }

    // Compute a contextual i18n key, using the context as suffix
    const contextualKeys = this.contextualKeys(key, context);
    const translations = this.translate.instant(contextualKeys, interpolateParams);

    return this.findFirstValid(contextualKeys, translations);
  }

  /**
   * Compute contextual i18n keys, using the context as suffix.<br>
   * Suffix can be simple name (like 'aaa' - will return ['<key>.aaa']) or complexe (like 'aaa.bbb' - will return ['<key>.aaa.bbb', '<key>.aaa']
   *
   * @param key
   * @param context
   * @private
   */
  contextualKeys(key: string, context: string): string[] {
    return context
      .split('.')
      .filter(isNotNilOrBlank)
      .reduce((res, item) => {
        const inheritedContext = res.length > 0 ? res[res.length - 1] + '.' : '';
        return res.concat(inheritedContext + item);
      }, [])
      .reverse() // Start with children, ends with parent key
      .map((contextItem) => this.contextualKey(key, contextItem))
      .concat(key);
  }

  /**
   * Compute a contextual i18n key, using the context as suffix
   *
   * @param key
   * @param context
   * @private
   */
  contextualKey(key: string, context: string): string {
    // Append a dot on context if missing
    if (!context.endsWith('.')) {
      context = context + '.';
    }

    // Compute a contextual i18n key, using the context as suffix
    const parts = key.split('.');
    return parts.length === 1
      ? `${context}${key}`
      : parts
          .slice(0, parts.length - 1)
          .concat(context + parts[parts.length - 1])
          .join('.');
  }

  private findFirstValid(keys: string[], translations: { [key: string]: string }): string {
    // For eac contextual key: try to find translation, then use the first valid
    for (const key of keys) {
      if (translations[key] !== key) return translations[key];
    }
    return translations[keys[keys.length - 1]]; // Return the last keys translation
  }
}
