import { Directive, Optional } from '@angular/core';
import { BehaviorSubject, firstValueFrom, Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { IStartableService } from './startable-service.class';

/**
 * Same as StartableService, but with a dataSubject instead of a simple 'data' property
 */
@Directive()
export abstract class StartableObservableService<T = any, O = any> implements IStartableService<T> {
  readonly dataSubject = new BehaviorSubject<T>(null);
  readonly startSubject = new Subject<T>();
  readonly stopSubject = new Subject<void>();

  protected _debug = false;
  protected _startByReadyFunction = true; // should start when calling ready() ?

  private _started = false;
  private _startPromise: Promise<T> = null;
  private _startPrerequisite: () => Promise<any> = null;
  private _subscription: Subscription = null;

  protected constructor(@Optional() prerequisiteService?: { ready: () => Promise<any> }) {
    this._startPrerequisite = prerequisiteService ? () => prerequisiteService.ready() : () => Promise.resolve();
  }

  start(opts?: O): Promise<T> {
    if (this._startPromise) return this._startPromise;
    if (this._started) return Promise.resolve(this.dataSubject.value);

    this._startPromise = this._startPrerequisite()
      .then(() => this.ngOnStart(opts))
      .then((data) => {
        this._started = true;
        this._startPromise = undefined;

        // Should be done AFTER 'this._started = true', because of the 'filter' operator, inside ready() function
        this.dataSubject.next(data);
        this.startSubject.next(data);

        return data;
      })
      .catch((err) => {
        if (!this.stopped) {
          console.error('Failed to start a service: ' + ((err && err.message) || err), err);
          this._started = false;
          this._startPromise = null;
        }
        return null;
      });
    return this._startPromise;
  }

  ready(): Promise<T> {
    if (this._started) return Promise.resolve(this.dataSubject.value);
    if (this._startPromise) return this._startPromise;
    if (this._startByReadyFunction) return this.start();

    return firstValueFrom(
      this.dataSubject.pipe(
        takeUntil(this.stopSubject),
        // Wait start() to be called, to exclude the initial 'null' value
        filter((_) => this._started)
      )
    );
  }

  async stop() {
    try {
      this.unsubscribe();
      await this.ngOnStop();
    } catch (err) {
      console.error('Failed to stop a service: ' + ((err && err.message) || err), err);
    } finally {
      this._started = false;
      this._startPromise = undefined;
      this.stopSubject.next(); // Stop all running observables
      this.dataSubject.next(null); // Reset data
    }
  }

  async restart(opts?: O): Promise<T> {
    if (this._startPromise) await this._startPromise; // Wait end of previous loading
    if (this._started) await this.stop(); // Then stop if started
    return this.start(opts); // Then start again
  }

  get started(): boolean {
    return this._started;
  }

  get starting(): boolean {
    return !!this._startPromise;
  }

  get stopped(): boolean {
    return !this._started && !this._startPromise;
  }

  protected get data(): T {
    return this.dataSubject.value;
  }

  protected set data(value: T) {
    if (this.dataSubject.value !== value) {
      this.dataSubject.next(value);
    }
  }

  protected registerSubscription(sub: Subscription) {
    this._subscription = this._subscription || new Subscription();
    this._subscription.add(sub);
  }

  protected unregisterSubscription(sub: Subscription): void {
    this._subscription?.remove(sub);
  }

  protected unsubscribe() {
    this._subscription?.unsubscribe();
    this._subscription = null;
  }

  protected ngOnStop(): Promise<void> | void {
    // Can be overwritten by subclasses
  }

  protected abstract ngOnStart(opts?: O): Promise<T>;
}
