import { EventEmitter, Injectable, InjectionToken } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';

export const APP_PROGRESS_BAR_SERVICE = new InjectionToken<IProgressBarService>('ProgressBarService');

export declare type ProgressMode = ProgressBarMode;

export interface IProgressBarService {
  onProgressChanged: EventEmitter<ProgressBarMode>;
  list(): number;
  increase();
  decrease();
}

@Injectable({ providedIn: 'root' })
export class ProgressBarService implements IProgressBarService {
  private _requestsRunning = 0;

  onProgressChanged = new EventEmitter<ProgressBarMode>();

  list(): number {
    return this._requestsRunning;
  }

  increase(): void {
    this._requestsRunning++;
    if (this._requestsRunning === 1) {
      this.onProgressChanged.emit('query');
    }
  }

  decrease(): void {
    if (this._requestsRunning > 0) {
      this._requestsRunning--;
    }
    if (this._requestsRunning === 0) {
      this.onProgressChanged.emit(null);
    }
  }
}
