import { BehaviorSubject, defer, Observable } from 'rxjs';
import { isNil, isNotNil, toNumber } from '../functions';
import { LoadResult, LoadResultByPageFn } from './entity-service.class';

export type CallableWithProgressionFn<O extends CallableWithProgressionOptions> = (opts?: O) => Promise<any>;
export interface CallableWithProgressionOptions {
  maxProgression?: number;
  progression?: BehaviorSubject<number>;
  [key: string]: any;
}

// @dynamic
export class JobUtils {
  static defers<O extends CallableWithProgressionOptions>(runnableList: CallableWithProgressionFn<O>[], opts?: O): Observable<number>[] {
    return runnableList.map((runnable) => JobUtils.defer(runnable, opts));
  }

  static defer<O extends CallableWithProgressionOptions>(runnable: CallableWithProgressionFn<O>, opts?: O): Observable<number> {
    return defer<Observable<number>>(() => JobUtils.run(runnable, opts));
  }

  static run<O extends CallableWithProgressionOptions>(runnable: CallableWithProgressionFn<O>, opts: O): Observable<number> {
    const progression = new BehaviorSubject<number>(0);
    runnable({ ...opts, progression })
      .then(() => {
        if (opts?.maxProgression) progression.next(opts.maxProgression);
        progression.complete();
      })
      .catch((err) => progression.error(err));
    return progression;
  }

  static async fetchAllPages<T>(
    loadPageFn: LoadResultByPageFn<T>,
    opts?: {
      progression?: BehaviorSubject<number>;
      maxProgression?: number;
      onPageLoaded?: (pageResult: LoadResult<T>) => any;
      logPrefix?: string;
      fetchSize?: number;
    }
  ): Promise<LoadResult<T>> {
    const maxProgression = toNumber(opts?.maxProgression, 100);
    const progression = opts?.progression;
    let total;
    let pageCount;
    let progressionStep;
    let currentPageDataCount: number;
    const fetchSize = toNumber(opts?.fetchSize, 1000);
    if (fetchSize <= 0) throw new Error('Unable to fetch pages: invalid fetchSize: ' + fetchSize);
    let offset = 0;
    let data: T[] = [];
    do {
      if (opts && opts.logPrefix) console.debug(`${opts.logPrefix} Fetching page #${offset / fetchSize}...`);

      // Fetch a page
      const res = await loadPageFn(offset, fetchSize);

      // Concat to previous result
      data = data.concat(res.data);

      // Compute variables, for next loop
      currentPageDataCount = (res.data && res.data.length) || 0;
      offset += currentPageDataCount;

      // Set total count (only once)
      if (isNil(total) && isNotNil(res.total)) {
        total = res.total;
        // Compute the number of page
        pageCount = Math.round(res.total / fetchSize + 0.5); // Round to HALF_UP integer
        // Compute step, by dividing maxProgression by number of page
        if (progression) {
          progressionStep = maxProgression / pageCount;
          // DEBUG
          if (opts && opts.logPrefix)
            console.debug(
              `${opts.logPrefix} {pageCount: ${pageCount}, progress: ${progression.value?.toFixed(2)}, pageProgressStep: ${progressionStep?.toFixed(1)}}`
            );
        }
      }

      // Increment progression on each iteration (if possible)
      if (isNotNil(progressionStep)) {
        progression.next(progression.value + progressionStep);
      }

      if (opts?.onPageLoaded) opts.onPageLoaded(res);
    } while (currentPageDataCount > 0 && ((isNil(total) && currentPageDataCount === fetchSize) || (isNotNil(total) && offset < total)));

    // Complete progression to reach progressionStep value (because of round)
    if (isNotNil(progressionStep)) {
      // If total return by loadAll was bad !
      if (isNotNil(total) && data.length < total) {
        console.warn(`A function loadAll() returned a bad total (less than all page's data)! Expected ${total} but fetch ${data.length}`);
        total = data.length;
        pageCount = Math.round(data.length / fetchSize + 0.5);
      }

      // Compute final increment, using the real page count
      progressionStep = Math.max(0, maxProgression - progressionStep * pageCount);
    }

    // Or increment once (e.g. when total is not known, in received LoadResult)
    else if (progression) {
      progressionStep = maxProgression;
    }

    // DEBUG
    if (opts && opts.logPrefix)
      console.debug(
        `${opts.logPrefix} All pages fetched - {progress: ${progression.value?.toFixed(2)}, lastProgressStep: ${progressionStep?.toFixed(1)}`
      );

    // Final increment
    if (progression && progressionStep > 0) {
      progression.next(progression.value + progressionStep);
    }

    return {
      data,
      total,
    };
  }
}
