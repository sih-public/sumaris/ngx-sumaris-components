import { BehaviorSubject, merge, Observable } from 'rxjs';
import { filter, map, mergeMap, takeUntil, tap } from 'rxjs/operators';
import { isEmptyArray, isNil, isNotEmptyArray, isNotNil } from '../functions';
import { EntitiesServiceLoadOptions, EntitiesServiceWatchOptions, IEntitiesService, LoadResult } from './entity-service.class';
import { SortDirection } from '@angular/material/sort';
import { Directive, OnDestroy } from '@angular/core';
import { EntityUtils, IEntity } from '../../core/services/model/entity.model';
import { EntityFilter, EntityFilterUtils } from '../../core/services/model/filter.model';
import { firstFalsePromise, WaitForOptions } from '../observables';
import { StartableObservableService } from './startable-observable-service.class';
import { FilterFn, FilterFnFactory } from '../types';

export interface InMemoryEntitiesServiceOptions<T, F> {
  onSort?: (data: T[], sortBy?: string, sortDirection?: SortDirection) => T[];
  onLoad?: (data: T[]) => T[] | Promise<T[]>;
  onSave?: (data: T[]) => T[] | Promise<T[]>;
  equals?: (d1: T, d2: T) => boolean;

  filterFnFactory?: FilterFnFactory<T, F>;
  filterFn?: FilterFn<T>;

  sortByReplacement?: { [key: string]: string };
}

// @dynamic
@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export class InMemoryEntitiesService<
    T extends IEntity<T, ID>,
    F = any,
    ID = number,
    O extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
  >
  extends StartableObservableService<T[], T[]>
  implements IEntitiesService<T, F, O>, OnDestroy
{
  debug = false;

  private _hiddenData: T[] = null;

  readonly dirtySubject = new BehaviorSubject<boolean>(false);
  readonly savingSubject = new BehaviorSubject<boolean>(false);

  protected readonly sortByReplacement: { [key: string]: string };

  private readonly _sortFn: (data: T[], sortBy?: string, sortDirection?: SortDirection) => T[];
  private readonly _onLoad: (data: T[]) => T[] | Promise<T[]>;
  private readonly _onSaveFn: (data: T[]) => T[] | Promise<T[]>;
  private readonly _equalsFn: (d1: T, d2: T) => boolean;
  private readonly _filterFnFactory: FilterFnFactory<T, F>;

  set value(data: T[]) {
    this.setValue(data);
  }

  get value(): T[] {
    return this.dataSubject.getValue();
  }

  get saving(): boolean {
    return this.savingSubject.value;
  }

  get dirty(): boolean {
    return this.dirtySubject.value;
  }

  constructor(
    protected dataType: new () => T,
    protected filterType: new () => F,
    options?: InMemoryEntitiesServiceOptions<T, F>
  ) {
    super(null);
    options = {
      onSort: this.sort,
      ...options,
    };

    this._sortFn = options.onSort;
    this._onLoad = options.onLoad;
    this._onSaveFn = options.onSave;
    this._equalsFn = options.equals;
    this._filterFnFactory =
      options.filterFnFactory ||
      ((f) => {
        const filter = this.asFilter(f) as F;
        if (filter instanceof EntityFilter) {
          return filter.asFilterFn();
        }
        return undefined;
      });
    this._startByReadyFunction = false; // Need setValue() to be called, to start the service

    this.sortByReplacement = {
      ...options.sortByReplacement,
    };
  }

  protected async ngOnStart(data?: T[]): Promise<T[]> {
    if (!data) throw new Error('Missing required data, to start this service');
    if (this.debug) console.debug('[memory-data-service] Starting...');
    this._hiddenData = [];
    this.markAsSaved();
    this.markAsPristine();
    return data;
  }

  protected async ngOnStop(): Promise<void> {
    if (this.debug) console.debug('[memory-data-service] Stopping...');
    this._hiddenData = null;
    this.markAsSaved();
    this.markAsPristine();
  }

  ngOnDestroy() {
    this.stop();
  }

  setValue(data: T[]) {
    // If service already started
    if (this.started) {
      // Update the data, if changed
      if (this.dataSubject.value !== data) {
        this._hiddenData = [];
        this.dataSubject.next(data);
      }

      // Reset dirty and saving state
      // (Fix ObsBio issue on samples table : still dirty after setting the same value again)
      this.markAsSaved();
      this.markAsPristine();
    }
    // If service not started yet, then start it, using given data
    else {
      this.start(data);
    }
  }

  async loadAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: F,
    opts?: EntitiesServiceLoadOptions & { updateHiddenData?: boolean }
  ): Promise<LoadResult<T>> {
    offset = offset >= 0 ? offset : 0;
    size = size >= 0 ? size : -1;

    const originalData = this.dataSubject.value;
    if (isNil(originalData)) {
      console.warn('[memory-data-service] Cannot load all: no value set. Will return empty result');
    }

    try {
      // Wait service is ready
      await this.ready();

      // Wait while busy (e.g. when saving, or deleting)
      await this.waitIdle({ stopError: false /*avoid error when stopped*/ });
    } catch (err) {
      // Should be a stop error: log and continue
      if (!this.stopped) console.error('Unexpected error, while waiting :', err);
    }

    // Make sure service is not stopped
    if (this.stopped) return undefined;

    const excludedDataByFilter: T[] = [];
    let excludedDataByPagination: T[];

    try {
      if (isEmptyArray(originalData)) {
        return { data: [], total: 0 };
      }

      // Apply sort
      let data = this._sortFn(originalData, sortBy, sortDirection);

      if (this._onLoad) {
        data = await this._onLoad(data);
      }

      // Apply filter (will update hiddenData)
      data = this.filter(data, filter, excludedDataByFilter);

      // Compute the total length
      const total = data.length;

      // If page size=0 (e.g. only need total)
      if (size === 0) {
        excludedDataByPagination = data;
        return { data: [], total };
      }

      // Slice in a page (using offset and size)
      if (offset > 0) {
        // Offset after the end: no result
        if (offset >= data.length) {
          excludedDataByPagination = data;
          data = [];
        } else {
          excludedDataByPagination =
            size > 0 && offset + size < data.length
              ? // Slice using limit to size
                data.slice(0, offset).concat(data.slice(offset + size))
              : // Slice without limit
                data.slice(0, offset);

          data =
            size > 0 && offset + size < data.length
              ? // Slice using limit to size
                data.slice(offset, offset + size)
              : // Slice without limit
                data.slice(offset);
        }
      }

      // Apply a limit
      else if (size > 0) {
        excludedDataByPagination = data.slice(size);
        data = data.slice(0, size);
      }

      // No limit:
      else if (size === -1) {
        // Keep all data
      }

      // /!\ If already observed, then always create a copy of the original array
      // Because datasource will only update if the array changed
      if (data === originalData) {
        data = data.slice();
      }

      const res: LoadResult<T> = { data, total };

      const nextOffset = size > 0 ? (offset || 0) + size : total;
      if (nextOffset < total) {
        res.fetchMore = async () => {
          const res = await this.loadAll(nextOffset, size, sortBy, sortDirection, filter, { ...opts, updateHiddenData: false });
          // Update hidden data (remove new fetched item)
          if (res.data?.length) {
            this._hiddenData = this._hiddenData?.filter((item) => !res.data.includes(item));
          }
          return res;
        };
      }

      return res;
    } finally {
      // Remember hidden data
      if (opts?.updateHiddenData !== false) {
        this._hiddenData = !excludedDataByPagination ? excludedDataByFilter : excludedDataByFilter.concat(...excludedDataByPagination);
      }
    }
  }

  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filterData?: F, options?: any): Observable<LoadResult<T>> {
    offset = offset >= 0 ? offset : 0;
    size = size >= 0 ? size : -1;

    return this.dataSubject.pipe(
      // Warn if waiting value to be set
      tap((data) => this.debug && !data && console.debug('[memory-data-service] Waiting value to be set...')),
      filter(isNotNil),
      mergeMap((_) => this.loadAll(offset, size, sortBy, sortDirection, filterData, options)),
      filter(isNotNil), // Skip no result (e.g. when stopped)
      takeUntil(this.stopSubject)
    );
  }

  async saveAll(data: T[], options?: any): Promise<T[]> {
    if (!this.dataSubject.value) throw new Error('[memory-service] Could not save, because value not set');

    this.markAsSaving();

    try {
      // Restore hidden data
      if (isNotEmptyArray(this._hiddenData)) data = data.concat(...this._hiddenData);

      if (this._onSaveFn) {
        const res = this._onSaveFn(data);
        data = res instanceof Promise ? await res : res;
      }

      this.dataSubject.next(data);
      this.markAsDirty();
      return data;
    } finally {
      this.markAsSaved();
    }
  }

  async deleteAll(dataToRemove: T[], options?: any): Promise<any> {
    let data = this.dataSubject.value;
    if (!data) throw new Error('[memory-service] Could not delete, because value not set');

    this.markAsSaving();

    try {
      // Remove deleted item, from data
      const updatedData = data.filter((entity) => {
        const removed = dataToRemove.findIndex((entityToRemove) => this.equals(entityToRemove, entity)) !== -1;
        return !removed;
      });
      const deleteCount = data.length - updatedData.length;
      if (deleteCount > 0) {
        data = updatedData;
        this.dataSubject.next(data);
        this.markAsDirty();
      }
    } finally {
      this.markAsSaved();
    }
  }

  sort(data: T[], sortBy?: string, sortDirection?: SortDirection): T[] {
    // Make sure to fill sortBy BEFORE checking in the replacement map
    sortBy = sortBy || 'id';
    // Replace sortBy, using the replacement map
    sortBy = this.sortByReplacement[sortBy] || sortBy;

    // Execute the sort
    return EntityUtils.sort(data, sortBy, sortDirection);
  }

  asFilter(source: Partial<F>): F {
    return EntityFilterUtils.fromObject(source, this.filterType);
  }

  addSortByReplacement(source: string, target: string) {
    this.sortByReplacement[source] = target;
  }

  equals(d1: T, d2: T): boolean {
    if (this._equalsFn) return this._equalsFn(d1, d2);
    return d1 && d1.equals ? d1.equals(d2) : EntityUtils.equals(d1, d2, 'id');
  }

  /**
   * Count data stored by the service (without hidden data).
   *
   * @param opts
   */
  // get visibleCount(): number {
  //   return (this.dataSubject.value?.length || 0);
  // }

  /**
   * Count data stored by the service (without hidden data).
   *
   * @param opts
   * @deprecated TODO Please rename this getter into visibleCount (and create count = hiddenCount + visibleCount)
   */
  get count(): number {
    return this.dataSubject.value?.length || 0;
  }

  get hiddenCount(): number {
    return this._hiddenData?.length || 0;
  }

  async waitIdle(opts?: WaitForOptions) {
    await this.waitWhileSaving(opts);
  }

  markAsDirty() {
    if (!this.dirtySubject.value) {
      this.dirtySubject.next(true);
    }
  }

  markAsPristine() {
    if (this.dirtySubject.value) {
      this.dirtySubject.next(false);
    }
  }

  /* -- protected methods -- */

  protected filter(data: T[], _filter: F, hiddenData: T[]): T[] {
    // if filter is DataFilter instance, use its test function
    const filterFn = this._filterFnFactory && this._filterFnFactory(_filter);
    if (filterFn) {
      const filteredData = [];

      data.forEach((value) => {
        if (filterFn(value)) filteredData.push(value);
        else hiddenData.push(value);
      });

      return filteredData;
    }

    // default, no filter
    return data;
  }

  protected connect(): Observable<LoadResult<T>> {
    return this.dataSubject.pipe(
      map((data) => {
        data = data || [];
        const total = data.length;
        return { data, total };
      })
    );
  }

  protected async waitWhileSaving(opts?: WaitForOptions): Promise<void> {
    if (this.saving) {
      return firstFalsePromise(this.savingSubject, {
        stop: opts?.stop || merge(this.stopSubject, this.dataSubject), // Stop if destroyed or new data Received
        stopError: false, // Avoid error when stopped
        ...opts,
      });
    }
  }

  protected markAsSaving() {
    if (!this.savingSubject.value) {
      this.savingSubject.next(true);
    }
  }

  protected markAsSaved() {
    if (this.savingSubject.value) {
      this.savingSubject.next(false);
    }
  }
}
