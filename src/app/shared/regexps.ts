export abstract class RegExpUtils {
  private static _cachedSearchRegexps: { [key: string]: RegExp } = {};
  private static _cachedSearchTexts: string[] = [];

  private static MAX_CACHE_SIZE = 100;
  private static SPECIAL_CHARACTERS_REGEXP = /[.\[\](){}\\$^|+*?]/g;
  private static SPECIAL_CHARACTERS_REPLACEMENT_MAP = {
    // Escape some characters
    '.': '[.]', // Escape point
    '(': '\\(', // Escape parenthesis
    ')': '\\)', // Escape parenthesis
    '[': '\\[', // Escape character class opening bracket
    ']': '\\]', // Escape character class closing bracket
    '{': '\\{', // Escape quantifier opening brace
    '}': '\\}', // Escape quantifier closing brace
    '\\': '\\\\', // Escape backslash itself
    $: '\\$', // Escape dollar sign
    '^': '\\^', // Escape caret
    '|': '\\|', // Escape pipe (alternation)
    '+': '\\+', // Escape plus

    // Transform wildcard characters to regexp
    '?': '.', // Exactly one character
    '*': '.*', // Zero or more character
  };

  /**
   * Cleans the provided search text by trimming whitespace and removing or condensing
   * wildcard characters based on the specified options.
   * (e.g. 'a**' becomes 'a', '*' becomes '', etc.)
   *
   * @param {string} searchText - The search text to be cleaned.
   * @param {Object} [opts] - Optional settings for cleaning the search text.
   * @param {boolean} [opts.anySearch] - If true, the search text is cleaned for any match. If false, search as a prefix
   * @return {string} - The cleaned search text.
   */
  static cleanSearchText(searchText: string, opts: { trim?: boolean } = { trim: true }): string {
    if (!searchText?.length) return searchText;

    // DEBUG
    //console.debug(`[regexp-utils] Cleaning searchText '${searchText}'`);

    searchText = searchText.replace(/[*]{2,}/g, '*'); // Replace many '*' into only one

    // Trim whitespace
    if (opts?.trim !== false) {
      searchText = searchText.trim();
    }

    // Remove trailing wildcard characters
    searchText = searchText.replace(/^[*]+$/g, '');

    // Clean any match expression
    if (searchText === '*') return '';

    return searchText;
  }

  /**
   * Compiles a regular expression from the provided search text, using a static cache to store
   * previously compiled regular expressions for faster retrieval.
   *
   * @param {string} searchText - The text to compile into a regular expression.
   * @param flags
   * @return {RegExp} - The compiled regular expression.
   */
  static compileSearchRegexp(searchText: string, flags = 'gi'): RegExp {
    if (!searchText?.length) return null;

    // Use cached regexp if exists
    const cacheKey = searchText + '|' + flags;
    const cachedRegexp = this._cachedSearchRegexps[cacheKey];
    if (cachedRegexp) return cachedRegexp;

    const searchRegexpText = searchText.replace(this.SPECIAL_CHARACTERS_REGEXP, (match) => this.SPECIAL_CHARACTERS_REPLACEMENT_MAP[match] || match);

    // DEBUG
    //console.debug(`[regexp-utils] Computing new regexp '${searchRegexpText}' from searchText '${searchText}'`);

    const regexp = new RegExp(`[ ]*${searchRegexpText}`, flags);

    // If cache is full: remove older cache element
    if (this._cachedSearchTexts.length >= this.MAX_CACHE_SIZE) {
      // DEBUG
      //console.debug('[regexp-utils] Removing older cached regexp');

      // Clear half cache's elements
      while (this._cachedSearchTexts.length > this.MAX_CACHE_SIZE / 2) {
        const cacheKeyToEvict = this._cachedSearchTexts.shift();
        delete this._cachedSearchRegexps[cacheKeyToEvict];
      }
    }

    // Add to static cache
    this._cachedSearchRegexps[cacheKey] = regexp;
    this._cachedSearchTexts.push(cacheKey);

    return regexp;
  }

  static match(input: string, regexp: string | RegExp): boolean {
    return !!input?.match(regexp) || false;
  }

  static matchUpperCase(input: string, regexp: string | RegExp): boolean {
    return !!input?.toUpperCase().match(regexp) || false;
  }

  static hasWildcardCharacter(input: string): boolean {
    return (input && (input.includes('*') || input.includes('?'))) || false;
  }
}
