import { Injectable } from '@angular/core';
import { Capacitor, PluginListenerHandle } from '@capacitor/core';
import { CapacitorPlugins } from './plugins';
import { Keyboard } from '@capacitor/keyboard';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class KeyboardService {
  private _enable = false;
  private _visibleSubject = new BehaviorSubject<boolean>(false);
  private _handlers: PluginListenerHandle[];

  get visibleSubject(): Observable<boolean> {
    return this._visibleSubject.asObservable();
  }

  get visible(): boolean {
    return this._visibleSubject.value;
  }
  get enable(): boolean {
    return this._enable;
  }

  constructor(private platform: Platform) {
    platform.ready().then(() => this.init());
  }

  private async init() {
    this._enable = this.platform.is('capacitor') && Capacitor.isPluginAvailable(CapacitorPlugins.Keyboard);
    if (this._enable) {
      this._handlers = await Promise.all([
        Keyboard.addListener('keyboardWillShow', () => this._visibleSubject.next(true)),
        Keyboard.addListener('keyboardDidHide', () => this._visibleSubject.next(false)),
      ]);
    }
  }
}
