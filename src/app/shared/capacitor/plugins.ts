import { Capacitor } from '@capacitor/core';

export declare type CapacitorPluginType = 'Camera' | 'StatusBar' | 'Keyboard' | 'BarcodeScanner';

export class CapacitorPlugins {
  static readonly Camera = 'Camera';
  static readonly StatusBar = 'StatusBar';
  static readonly Keyboard = 'Keyboard';
  //static readonly BarcodeScanner = 'BarcodeScanner';

  static isAvailable = Capacitor.isPluginAvailable;
}
