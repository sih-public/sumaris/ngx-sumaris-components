import { AbstractControl, UntypedFormArray, UntypedFormControl, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { isMoment } from 'moment';
import { isEmptyArray, isNotEmptyArray, isNotNil, isNotNilOrNaN, nullIfUndefined } from './functions';
import { toDateISOString } from './dates';
import { waitFor, WaitForOptions } from './observables';
import { Predicate } from '@angular/core';

import { AppFormArray } from '../core/form/array/form-array';

export interface FormErrors {
  [key: string]: ValidationErrors;
}

/**
 * Fill a form using a source entity
 *
 * @param target
 * @param entity
 * @param opts
 */
export function copyEntity2Form(entity: any, target: UntypedFormGroup, opts?: { emitEvent?: boolean; onlySelf?: boolean }) {
  const json = getFormValueFromEntity(entity, target);
  target.patchValue(json, opts);
}

export function getFormValueFromEntity(entity: any | undefined, form: UntypedFormGroup): { [key: string]: any } {
  return adaptValueToControl(entity, form) as { [key: string]: any };
}

/**
 * Transform an entity into a simple object, compatible with the given form
 *
 * @param source an entity (or subentity)
 * @param control
 * @param path
 */
export function adaptValueToControl(source: any | undefined, control: AbstractControl, path?: string): any {
  const pathPrefix = path ? path + '.' : '';

  // Form group
  if (control instanceof UntypedFormGroup) {
    const result = {};
    // eslint-disable-next-line guard-for-in
    for (const key in control.controls) {
      result[key] = adaptValueToControl(source && source[key], control.controls[key], pathPrefix + key);
    }
    return result;
  }

  // Array
  if (control instanceof UntypedFormArray) {
    // Split, if many values in a string
    if (typeof source === 'string') {
      source = source.split('|');
    }
    // Skip if value is not an array
    if (!Array.isArray(source)) {
      return [];
    }

    // Resizable array
    if (control instanceof AppFormArray) {
      const exampleControl = control.createControl();
      return source.map((item, index) => adaptValueToControl(item, exampleControl, pathPrefix + '#' + index)) as any[];
    }

    // Legacy array
    else if (control.length > 0) {
      const firstControl = control.at(0);
      // Use the first form group, as model
      let result = source.map((item, index) => adaptValueToControl(item, firstControl, pathPrefix + '#' + index)) as any[];

      // Truncate if too many values
      if (result.length > control.length) {
        if (firstControl instanceof UntypedFormControl) {
          for (let i = control.length; i < result.length; i++) {
            control.push(new UntypedFormControl(null, firstControl.validator));
          }
        } else {
          console.warn(`WARN: please resize the FormArray '${path || ''}' to the same length of the input array`);
          result = result.slice(0, control.length);
        }
      }

      // Add values if not enought
      else if (result.length < control.length) {
        //console.warn(`WARN: Adding null value to array values`);
        for (let i = result.length; i < control.length; i++) {
          result.push(null);
        }
      }
      return result;
    }
    // Skip if unable to find a control in the array
    else {
      if (isNotEmptyArray(source)) console.warn(`WARN: please resize the FormArray '${path}' to the same length of the input array`);
      return [];
    }
  }

  // Form control
  if (control instanceof UntypedFormControl) {
    // Date
    if (isMoment(source)) {
      return toDateISOString(source);
    }
    // Any other control: replace undefined by null value
    // because Undefined is not authorized as control value
    else {
      return nullIfUndefined(source);
    }
  }
}

export function logFormErrors(control: AbstractControl, logPrefix?: string, path?: string) {
  if (!control || control.valid) return;
  logPrefix = logPrefix || '';
  // Form group
  if (control instanceof UntypedFormGroup) {
    if (!path) console.warn(`${logPrefix} Form errors:`);
    if (control.errors) {
      Object.keys(control.errors).forEach((error) => console.warn(`${logPrefix} -> ${path || ''} (${error})`));
    }
    if (control.controls) {
      Object.keys(control.controls).forEach(
        (child) => logFormErrors(control.controls[child], logPrefix, path ? `${path}.${child}` : child) // Recursive call
      );
    }
  }
  // Form array
  else if (control instanceof UntypedFormArray) {
    if (control.errors) {
      Object.keys(control.errors).forEach((error) => console.warn(`${logPrefix} -> ${path || ''} (${error})`));
    }
    control.controls.forEach((child, index) => {
      logFormErrors(child, logPrefix, path ? `${path}.${index}` : `${index}`); // Recursive call
    });
  }
  // Other control's errors
  else if (control.errors) {
    Object.keys(control.errors).forEach((error) => console.warn(`${logPrefix} -> ${path || ''} (${error})`));
  }
}

export function getFormErrors(
  control: AbstractControl,
  opts?: {
    controlName?: string;
    result?: FormErrors;
    recursive?: boolean;
  }
): FormErrors {
  if (!control || control.valid) return undefined;

  opts = opts || {};
  opts.result = opts.result || {};

  // Form group
  if (control instanceof UntypedFormGroup) {
    // Copy errors
    if (control.errors) {
      if (opts.controlName) {
        opts.result[opts.controlName] = {
          ...control.errors,
        };
      } else {
        opts.result = {
          ...opts.result,
          ...control.errors,
        };
      }
    }

    if (!opts || opts.recursive !== false) {
      // Loop on children controls
      for (const key in control.controls) {
        const child = control.controls[key];
        if (child?.enabled) {
          getFormErrors(child, {
            ...opts,
            controlName: opts.controlName ? [opts.controlName, key].join('.') : key,
            result: opts.result, // Make sure to keep the same result object
          });
        }
      }
    }
  }

  // Form array
  else if (control instanceof UntypedFormArray) {
    // Copy errors
    if (control.errors) {
      if (opts.controlName) {
        opts.result[opts.controlName] = {
          ...control.errors,
        };
      } else {
        opts.result = {
          ...opts.result,
          ...control.errors,
        };
      }
    }

    control.controls.forEach((child, index) => {
      getFormErrors(child, {
        ...opts,
        controlName: (opts.controlName || '') + '.' + index,
        result: opts.result, // Make sure to keep the same result object
      });
    });
  }

  // Other type of control (e.g simple control)
  else if (control.errors) {
    if (opts.controlName) {
      opts.result[opts.controlName] = {
        ...control.errors,
      };
    } else {
      opts.result = {
        ...opts.result,
        ...control.errors,
      };
    }
  }
  return opts.result;
}

export function getControlFromPath(form: UntypedFormGroup, path: string): AbstractControl {
  const i = path.indexOf('.');
  if (i === -1) {
    return form.controls[path];
  }
  const key = path.substring(0, i);
  if (form.controls[key] instanceof UntypedFormGroup) {
    return getControlFromPath(form.controls[key] as UntypedFormGroup, path.substring(i + 1));
  }
  throw new Error(`Invalid form path: '${key}' should be a form group.`);
}

export function disableControls(form: UntypedFormGroup, paths: string[], opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }) {
  (paths || []).forEach((path) => {
    const control = getControlFromPath(form, path);
    if (control) disableControl(control, opts);
  });
}
export function disableAndClearControls(
  form: UntypedFormGroup,
  paths: string[],
  opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }
) {
  (paths || []).forEach((path) => {
    const control = getControlFromPath(form, path);
    if (control) disableAndClearControl(control, opts);
  });
}
export function enableControls(form: UntypedFormGroup, paths: string[], opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }) {
  (paths || []).forEach((path) => {
    const control = getControlFromPath(form, path);
    if (control) enableControl(control, opts);
  });
}

export function setControlsEnabled(
  form: UntypedFormGroup,
  enabled: boolean,
  paths: string[],
  opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }
) {
  (paths || []).forEach((path) => {
    const control = getControlFromPath(form, path);
    if (control) setControlEnabled(control, enabled, opts);
  });
}

export function enableControl(control: AbstractControl, opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }) {
  setControlEnabled(control, true, opts);
}

export function disableControl(control: AbstractControl, opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }) {
  setControlEnabled(control, false, { required: false, ...opts });
}

export function disableAndClearControl(control: AbstractControl, opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }) {
  const wasDisabled = control.disabled;

  // Apply disable
  setControlEnabled(control, false, { required: false, ...opts });

  // Important, should not use reset() to mark the control as dirty.
  // Otherwise, optimization using dirty (.e.g MeasurementForm) will not be notified by this changed
  if (isNotNil(control.value)) {
    if (wasDisabled) {
      control.reset(null, opts);
    } else {
      control.setValue(null, opts);
    }
  }
}

export function setControlEnabled(
  control: AbstractControl,
  enabled: boolean,
  opts?: { onlySelf?: boolean; emitEvent?: boolean; required?: boolean }
) {
  if (!control) return; // SKip if no control
  if (enabled) {
    if (isNotNil(opts?.required) && opts.required && !control.hasValidator(Validators.required)) {
      control.addValidators(Validators.required);
    }
    control.enable(opts);
  } else {
    control.disable(opts);
    if (isNotNil(opts?.required) && !opts.required && control.hasValidator(Validators.required)) {
      control.removeValidators(Validators.required);
    }
  }
}

export function addValueInArray(
  arrayControl: UntypedFormArray,
  createControl: (value?: any) => AbstractControl,
  equals: (v1: any, v2: any) => boolean,
  isEmpty: (value: any) => boolean,
  value: any,
  options?: { emitEvent: boolean; insertAt?: number; allowManyNullValues?: boolean; allowDuplicateValue?: boolean }
): boolean {
  const disabled = arrayControl.disabled;

  let hasChanged = false;
  let index = -1;
  let isEmptyValue = isEmpty(value);

  // Search if value already exists
  if (!isEmptyValue && options?.allowDuplicateValue !== true) {
    index = (arrayControl.value || []).findIndex((v) => equals(value, v));
  }

  // If value not exists, but last value is empty: reuse last value
  if (index === -1 && options?.allowManyNullValues !== true && arrayControl.length > 0) {
    const lastValue = arrayControl.at(arrayControl.length - 1).value;
    if (isEmpty(lastValue)) {
      index = arrayControl.length - 1;
    }
  }

  // Replace the existing value
  if (index !== -1) {
    if (!isEmptyValue) {
      arrayControl.at(index).patchValue(value, options);
      hasChanged = true;
    }
  } else {
    const control = createControl(value);

    // Apply parent disabled state, before to push it into the array
    // This is need to avoid parent form to be enabled
    if (disabled && control.enabled) control.disable({ emitEvent: false });
    else if (!disabled && control.disabled) control.enable({ emitEvent: false });

    if (isNotNilOrNaN(options?.insertAt)) {
      arrayControl.insert(options.insertAt, control, options);
    } else {
      arrayControl.push(control, options);
    }
    hasChanged = true;
  }

  if (hasChanged) {
    if (!options || options.emitEvent !== false) {
      // Mark array control dirty
      if (!isEmptyValue) {
        arrayControl.markAsDirty();
      }
    }
  }

  return hasChanged;
}

/**
 * Set an array using given default values. Each default value will be pass to the 'createControl()' function
 *
 * @param arrayControl
 * @param createControl
 * @param defaultValues
 * @param options
 */
export function initArrayControlsFromValues(
  arrayControl: UntypedFormArray,
  createControl: (value?: any) => AbstractControl,
  defaultValues: any[],
  options?: { emitEvent?: boolean }
): boolean {
  if (arrayControl.length === 0 && (!defaultValues || defaultValues.length === 0)) return false; // No changes need

  const disabled = arrayControl.disabled;

  while (arrayControl.length > 0) {
    arrayControl.removeAt(arrayControl.length - 1, options);
  }

  (defaultValues || []).forEach((value) => {
    const control = createControl(value);

    // Apply parent disabled state, before to push it into the array
    // This is need to avoid parent form to be enabled
    if (disabled && control.enabled) control.disable({ emitEvent: false });
    else if (!disabled && control.disabled) control.enable({ emitEvent: false });

    arrayControl.push(control, options);
  });

  return true; // Has some changes
}

export function resizeArray(
  arrayControl: UntypedFormArray,
  createControl: () => AbstractControl,
  length: number,
  options?: { emitEvent?: boolean }
): boolean {
  if (arrayControl.length === length) return false; // No changes need

  const disabled = arrayControl.disabled;

  // Reduce size
  while (arrayControl.length > length) {
    arrayControl.removeAt(arrayControl.length - 1, options);
  }

  // Increase size
  while (arrayControl.length < length) {
    const control = createControl();

    // Apply parent disabled state, before to push it into the array
    // This is need to avoid parent form to be enabled, after calling resizeArray()
    if (disabled && control.enabled) control.disable({ emitEvent: false });
    else if (!disabled && control.disabled) control.enable({ emitEvent: false });

    arrayControl.push(control, options);
  }

  return true; // Has some changes
}

export function removeValueInArray(arrayControl: UntypedFormArray, isEmpty: (value: any) => boolean, index: number): boolean {
  arrayControl.removeAt(index);
  arrayControl.markAsDirty();
  return true;
}

export function clearValueInArray(arrayControl: UntypedFormArray, isEmpty: (value: any) => boolean, index: number): boolean {
  const control = arrayControl.at(index);
  if (isEmpty(control.value)) return false; // skip (not need to clear)

  if (control instanceof UntypedFormGroup) {
    copyEntity2Form({}, control);
  } else if (control instanceof UntypedFormArray) {
    control.setValue([]);
  } else {
    control.setValue(null);
  }
  arrayControl.markAsDirty();
  return true;
}

export function markAllAsTouched(control: AbstractControl, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
  if (!control) return;
  if (control instanceof UntypedFormGroup) {
    markFormGroupAsTouched(control, { ...opts, onlySelf: true }); // recursive call
  } else if (control instanceof UntypedFormArray) {
    control.markAsTouched({ onlySelf: true });
    (control.controls || []).forEach((c) => markControlAsTouched(c, { ...opts, onlySelf: true })); // recursive call
  } else {
    control.markAsTouched({ onlySelf: true });
    control.updateValueAndValidity({ ...opts, onlySelf: true });
  }
}

export function markFormGroupAsTouched(form: UntypedFormGroup, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
  if (!form) return;
  form.markAsTouched(opts);
  Object.keys(form.controls)
    .map((key) => form.controls[key])
    .filter((control) => control.enabled)
    .forEach((control) => markControlAsTouched(control, opts));
  form.updateValueAndValidity({ ...opts, onlySelf: true });
}

export function markControlAsTouched(control: AbstractControl, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
  if (!control) return;
  if (control instanceof UntypedFormGroup) {
    markAllAsTouched(control, { ...opts, onlySelf: true }); // recursive call
  } else if (control instanceof UntypedFormArray) {
    (control.controls || []).forEach((c) => markControlAsTouched(c, { ...opts, onlySelf: true })); // recursive call
  } else {
    control.markAsTouched({ onlySelf: true });
    control.updateValueAndValidity({ ...opts, onlySelf: true });
  }
}

export function updateValueAndValidity(form: UntypedFormGroup, opts?: { onlySelf?: boolean; emitEvent?: boolean }) {
  if (!form) return;
  form.updateValueAndValidity(opts);
  Object.keys(form.controls)
    .map((key) => form.controls[key])
    .filter((control) => control.enabled)
    .forEach((control) => {
      if (control instanceof UntypedFormGroup) {
        updateValueAndValidity(control, { ...opts, onlySelf: true }); // recursive call
      } else {
        control.updateValueAndValidity({ ...opts, onlySelf: true });
      }
    });
}

export function markAsUntouched(form: UntypedFormGroup, opts?: { onlySelf?: boolean }) {
  if (!form) return;
  form.markAsUntouched(opts);
  Object.getOwnPropertyNames(form.controls).forEach((key) => {
    const control = form.get(key);
    if (control instanceof UntypedFormGroup) {
      markAsUntouched(control, { onlySelf: true }); // recursive call
    } else {
      control.markAsUntouched({ onlySelf: true });
      control.setErrors(null);
    }
  });
}

/**
 * Wait end of async validation (not pending). This need to implement {pending: boolean}
 * return false
 */
export function waitWhilePending<T extends { pending: boolean }>(form: T, opts?: WaitForOptions): Promise<void> {
  const predicate: Predicate<void> = () => form.pending !== true;
  return waitFor(predicate, opts);
}

/**
 * Wait form is idle (not loading). This need to implement {loading: boolean}
 *
 * @param form
 * @param opts
 */
export function waitIdle<T extends { loading: boolean }>(form: T, opts?: WaitForOptions): Promise<void> {
  const predicate: Predicate<void> = () => form.loading !== true;
  return waitFor(predicate, opts);
}

export function filterFormErrorsByPrefix(errors: FormErrors, ...prefixes: string[]): FormErrors {
  if (isEmptyArray(prefixes)) return errors;
  return filterFormErrorsByPath(errors, (path) => prefixes?.some((prefix) => path.startsWith(prefix)));
}

export function filterFormErrorsByPath(errors: FormErrors, filter: Predicate<string>): FormErrors {
  if (typeof filter !== 'function') return errors;
  return filterFormErrors(errors, ([path, _]) => filter(path));
}

export function filterFormErrors(errors: FormErrors, filter: Predicate<[string, any]>): FormErrors {
  if (typeof filter !== 'function') return errors;
  return Object.entries(errors)
    .filter(filter)
    .reduce(
      (res, [path, error]) => {
        // Init error map
        res = res || {};
        // Add current error
        res[path] = error;
        return res;
      },
      <FormErrors>undefined
    );
}
