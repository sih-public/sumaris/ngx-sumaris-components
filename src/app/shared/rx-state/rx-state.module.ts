import { NgModule } from '@angular/core';
import { RxFor } from '@rx-angular/template/for';
import { RxIf } from '@rx-angular/template/if';
import { RxPush } from '@rx-angular/template/push';
import { RxLet } from '@rx-angular/template/let';

@NgModule({
  imports: [RxPush, RxFor, RxIf, RxLet],
  exports: [RxPush, RxFor, RxIf, RxLet],
})
export class RxStateModule {}
