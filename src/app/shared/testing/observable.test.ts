import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { firstNotNilPromise, FirstOptions, WaitForOptions, waitForTrue } from '../observables';
import { Observable, Subject } from 'rxjs';
import { isNotNil } from '../functions';
import { tap } from 'rxjs/operators';
import { DateUtils, toDateISOString } from '../dates';

@Component({
  selector: 'app-observable-test',
  templateUrl: './observable.test.html',
  styleUrls: ['./observable.test.scss'],
})
export class ObservableTestPage implements OnDestroy {
  _stopSubject = new Subject<void>();

  $value = new Subject<any>();
  runningTaskCount = 0;
  taskId = 0;
  options: Partial<FirstOptions> = {
    stop: this._stopSubject,
  };

  @ViewChild('result') resultDiv!: ElementRef;

  constructor() {}

  ngOnDestroy() {
    this._stopSubject.next();
  }

  setOptions(opts: any) {
    this.options = {
      ...this.options,
      ...opts,
    };
    this.log('Options changes to: ', { ...this.options, stop: this.options?.stop ? '$stop' : 'null' });
  }

  setOptionsError(errorMessage) {
    this.setOptions({ stopError: new Error(errorMessage) });
  }

  async firstNotNilPromise(opts?: FirstOptions) {
    const logPrefix = `[firstNotNilPromise() #${++this.taskId}] `;
    await this.execute(firstNotNilPromise, opts, logPrefix);
  }

  async waitForTrue(opts?: WaitForOptions) {
    const logPrefix = `[waitForTrue() #${++this.taskId}] `;
    await this.execute(waitForTrue, opts, logPrefix);
  }

  async execute<T, O extends WaitForOptions = WaitForOptions>(fn: (o: Observable<any>, opts: O) => Promise<T>, opts?: O, logPrefix?: string) {
    opts = {
      ...this.options,
      stop: this._stopSubject,
      ...opts,
    };
    logPrefix = logPrefix || `[#${++this.taskId}] `;

    const value$ = this.$value.pipe(tap((value) => this.log(logPrefix + `Received value: ${value}`)));

    try {
      this.runningTaskCount++;
      this.log(logPrefix + 'Waiting...');

      // Execute the async function
      const result = await fn(value$, opts);

      // Dump result
      if (isNotNil(result)) this.log(logPrefix + '[OK] with result:', result);
      else this.log(logPrefix + '[OK]');
    } catch (err) {
      const errorMsg = logPrefix + 'Error: ' + (err?.message || '');
      console.error(errorMsg, err);
      this.log(errorMsg + (err?.originalStack ? `<pre>${err.originalStack}</pre>` : ''));
    } finally {
      this.runningTaskCount--;
    }
  }

  emit(value: any) {
    this.$value.next(value);
  }

  stop() {
    this._stopSubject.next();
  }

  log(message: string, ...args: any) {
    let fullMessage = `${toDateISOString(DateUtils.moment())} - ${message}`;
    if (args?.length) {
      fullMessage += ' - <small>' + args.map(JSON.stringify).join(', ') + '</small>';
    }
    this.appendLog(fullMessage);
  }

  appendLog(message: string) {
    this.resultDiv.nativeElement.innerHTML += message + '<br/>';
  }
}
