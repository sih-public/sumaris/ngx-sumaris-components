import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'app-maskito-test',
  templateUrl: './maskito.test.html',
})
export class MaskitoTestPage {
  form: UntypedFormGroup;

  labelMask: (string | RegExp)[] = [
    /\d/,
    /\d/,
    ' ',
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    /^[a-zA-Z]$/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
  ];

  constructor(protected formBuilder: UntypedFormBuilder) {
    this.form = formBuilder.group({
      strategy: [null],
    });
  }
}
