import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { SelectionChange, SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { equals } from '../functions';

@Pipe({
  name: 'abstract-selection',
})
export abstract class AbstractSelectionModelPipe<T, R, O = void> implements PipeTransform, OnDestroy {
  protected _result: R = undefined;

  private _lastSelection: SelectionModel<any> | null = null;
  private _lastValue: T;
  private _lastOpts: O;
  protected _shouldSubscribeToChanges = true;
  private _onSelectionChanges: Subscription | undefined;

  protected constructor(protected _ref: ChangeDetectorRef) {}

  transform(selection: SelectionModel<any>, value?: T, opts?: O): R {
    if (!selection) {
      this._dispose();
      return undefined;
    }

    // if we ask another time for the same form and opts, return the last value
    if (selection === this._lastSelection && value === this._lastValue && equals(opts, this._lastOpts)) {
      return this._result;
    }

    // store the query, in case it changes
    this._lastSelection = selection;

    // store the params, in case they change
    this._lastValue = value;
    this._lastOpts = opts;

    // set the result
    this._result = this._transform(selection, value, opts);

    // if there is already a subscription, clean it
    this._dispose();

    // subscribe to valueChanges event
    if (this._shouldSubscribeToChanges) {
      this._onSelectionChanges = this._subscribe(selection, value, opts);
    }

    return this._result;
  }

  ngOnDestroy(): void {
    this._dispose();
  }

  protected _subscribe(selection: SelectionModel<T>, value: T, opts?: O): Subscription {
    // subscribe to valueChanges event
    return selection.changed.pipe(filter((selectionChange) => this._filterSelectionChange(selectionChange, value, opts))).subscribe((_) => {
      const result = this._transform(selection, value);
      if (this._result !== result) {
        this._result = result;
        this._ref.markForCheck();
      }
    });
  }

  /**
   * Clean any existing subscription to change events
   */
  protected _dispose(): void {
    this._onSelectionChanges?.unsubscribe();
    this._onSelectionChanges = undefined;
  }

  protected abstract _transform(selection: SelectionModel<T>, value?: T, opts?: O): R;

  protected _filterSelectionChange(selectionChange: SelectionChange<any>, value?: T, opts?: O): boolean {
    return true;
  }
}

@Pipe({
  name: 'isSelected',
  pure: false,
})
export class IsSelectedPipe extends AbstractSelectionModelPipe<any, boolean> implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }
  protected _transform(selection: SelectionModel<any>, value: any): boolean {
    return selection.isSelected(value);
  }

  protected _filterSelectionChange(selectionChange: SelectionChange<any>, value: any) {
    return selectionChange?.added?.includes(value) || selectionChange?.removed?.includes(value) || false;
  }
}

@Pipe({
  name: 'isNotEmptySelection',
  pure: false,
})
export class IsNotEmptySelectionPipe extends AbstractSelectionModelPipe<void, boolean> implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }
  protected _transform(selection: SelectionModel<any>): boolean {
    return selection.hasValue();
  }
}

@Pipe({
  name: 'isEmptySelection',
  pure: false,
})
export class IsEmptySelectionPipe extends AbstractSelectionModelPipe<void, boolean> {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }
  protected _transform(selection: SelectionModel<any>): boolean {
    return selection.isEmpty();
  }
}

@Pipe({
  name: 'selectionLength',
  pure: false,
})
export class SelectionLengthPipe extends AbstractSelectionModelPipe<void, number> implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
  }
  protected _transform(selection: SelectionModel<any>): number {
    return selection.selected.length;
  }
}

@Pipe({
  name: 'isMultipleSelection',
  pure: false,
})
export class IsMultipleSelectionPipe extends AbstractSelectionModelPipe<void, boolean> implements PipeTransform, OnDestroy {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
    this._shouldSubscribeToChanges = false;
  }
  protected _transform(selection: SelectionModel<any>): boolean {
    return selection.isMultipleSelection();
  }
}

@Pipe({
  name: 'isSingleSelection',
  pure: false,
})
export class IsSingleSelectionPipe extends AbstractSelectionModelPipe<void, boolean> {
  constructor(_ref: ChangeDetectorRef) {
    super(_ref);
    this._shouldSubscribeToChanges = false;
  }
  protected _transform(selection: SelectionModel<any>): boolean {
    return !selection.isMultipleSelection();
  }
}
