import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { DateUtils } from '../dates';
import { unitOfTime } from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'duration',
})
@Injectable({ providedIn: 'root' })
export class DurationPipe implements PipeTransform {
  protected dayUnit: string;

  constructor(translate: TranslateService) {
    this.dayUnit = translate.instant('COMMON.DAY_UNIT');
  }

  /**
   * Format a diff between two dates.
   *
   * @param value
   * @param opts
   * @param opts.unit allow to convert into a particular unit. If not defined, result will be like `x days HH:MM`
   * @param opts.seconds Should display or ignore the seconds ?
   **/
  transform(value: number, opts?: unitOfTime.DurationConstructor | { unit?: unitOfTime.DurationConstructor; seconds?: boolean } | any): string {
    return DateUtils.durationToString(value, this.dayUnit, opts);
  }
}
