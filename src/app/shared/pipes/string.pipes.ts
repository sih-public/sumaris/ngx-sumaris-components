import { Pipe, PipeTransform } from '@angular/core';
import { capitalizeFirstLetter, isNil, isNilOrBlank, isNilOrNaN, isNotNil, isNotNilOrBlank, isNotNilOrNaN } from '../functions';
import { DisplayFn } from '../form/field.model';

@Pipe({
  name: 'isNotNilOrBlank',
})
export class IsNotNilOrBlankPipe implements PipeTransform {
  transform(value: string): boolean {
    return isNotNilOrBlank(value);
  }
}

@Pipe({
  name: 'isNilOrBlank',
})
export class IsNilOrBlankPipe implements PipeTransform {
  transform(value: string): boolean {
    return isNilOrBlank(value);
  }
}

@Pipe({
  name: 'isNilOrNaN',
})
export class IsNilOrNaNPipe implements PipeTransform {
  transform(value: number): boolean {
    return isNilOrNaN(value);
  }
}

@Pipe({
  name: 'isNotNilOrNaN',
})
export class IsNotNilOrNaNPipe implements PipeTransform {
  transform(value: number): boolean {
    return isNotNilOrNaN(value);
  }
}

@Pipe({
  name: 'isNotNil',
})
export class IsNotNilPipe implements PipeTransform {
  transform(value: any): boolean {
    return isNotNil(value);
  }
}

@Pipe({
  name: 'isNil',
})
export class IsNilPipe implements PipeTransform {
  transform(value: any): boolean {
    return isNil(value);
  }
}

@Pipe({
  name: 'toString',
})
export class ToStringPipe implements PipeTransform {
  transform(value: number | object, displayWith?: DisplayFn): string {
    if (displayWith) return displayWith(value);
    return value !== null && value !== undefined ? value.toString() : '';
  }
}

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(value: string): string {
    return capitalizeFirstLetter(value);
  }
}

@Pipe({
  name: 'strLength',
})
export class StrLengthPipe implements PipeTransform {
  transform(value: string): number {
    return (value && value.length) || 0;
  }
}

@Pipe({
  name: 'strIncludes',
})
export class StrIncludesPipe implements PipeTransform {
  transform(value: string, searchString: string, position?: number): boolean {
    return value?.includes(searchString, position) || false;
  }
}

@Pipe({
  name: 'replace',
})
export class StrReplacePipe implements PipeTransform {
  transform(input: string, searchValue: string | RegExp, replacement: string): any {
    if (typeof input !== 'string' || isNilOrBlank(searchValue) || isNil(replacement)) {
      return input;
    }

    return input.replace(searchValue, replacement);
  }
}

@Pipe({
  name: 'truncText',
})
export class TruncTextPipe implements PipeTransform {
  transform(input: string, size = 60, ellipsis = true, ellipsisText = '...'): any {
    if (!(input?.length > 3) || input.length <= size) return input;
    return input.substring(0, size - 3) + (ellipsis ? ellipsisText : '');
  }
}

@Pipe({
  name: 'truncHtml',
})
export class TruncHtmlPipe implements PipeTransform {
  transform(input: string, size = 500, ellipsis = true, ellipsisText = ' (...)'): any {
    if (!(input?.length > 3) || input.length <= size) return input;
    const endIndex = Math.max(input.lastIndexOf(' ', size), input.lastIndexOf('<', size));
    return input.substring(0, endIndex) + (ellipsis ? ellipsisText : '');
  }
}
