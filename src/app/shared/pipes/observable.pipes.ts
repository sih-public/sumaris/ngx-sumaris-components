import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { firstFalse, FirstOptions, firstTrue } from '../observables';
import { first } from 'rxjs/operators';

@Pipe({
  name: 'firstTrue',
})
export class FirstTruePipe implements PipeTransform {
  transform(value: Observable<boolean>, opts?: FirstOptions): Observable<boolean> {
    return firstTrue(value, opts, true);
  }
}

@Pipe({
  name: 'firstFalse',
})
export class FirstFalsePipe implements PipeTransform {
  transform(value: Observable<boolean>, opts?: FirstOptions): Observable<boolean> {
    return firstFalse(value, opts, false);
  }
}

@Pipe({
  name: 'first',
})
export class FirstPipe implements PipeTransform {
  transform<T = any>(value: Observable<T>): Observable<T> {
    return value.pipe(first());
  }
}
