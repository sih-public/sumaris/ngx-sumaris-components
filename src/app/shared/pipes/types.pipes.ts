import { Pipe, PipeTransform, Predicate } from '@angular/core';
import { isObservable, Observable } from 'rxjs';
import { FloatLabelType } from '@angular/material/form-field';
import { AppFloatLabelType } from '../form/field.model';
import { toBoolean } from '../functions';

@Pipe({
  name: 'asAny',
})
export class AsAnyPipe implements PipeTransform {
  transform<T = any>(value: unknown | any): T | null {
    if (value === null || value === undefined) return null;
    return value as T;
  }
}

@Pipe({
  name: 'asArray',
})
export class AsArrayPipe implements PipeTransform {
  transform<T = any>(value: unknown): T[] | null {
    return Array.isArray(value) ? (value as any[]) : null;
  }
}

@Pipe({
  name: 'asObservable',
})
export class AsObservablePipe implements PipeTransform {
  transform<T = any>(value: unknown): Observable<T> | null {
    return isObservable(value) ? (value as Observable<T>) : null;
  }
}

@Pipe({
  name: 'asFloatLabelType',
})
export class AsFloatLabelTypePipe implements PipeTransform {
  transform(value: AppFloatLabelType): FloatLabelType | null {
    return value === 'never' ? null : value;
  }
}

@Pipe({
  name: 'asBoolean',
})
export class AsBooleanPipe implements PipeTransform {
  transform(value: any, opts?: boolean | Predicate<any>): boolean {
    if (typeof opts === 'function') return opts(value);
    return toBoolean(value, opts);
  }
}
