import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { TranslateContextService } from '../services/translate-context.service';
import { changeCaseToUnderscore } from '../functions';

@Injectable()
@Pipe({
  name: 'translateContext',
})
export class TranslateContextPipe implements PipeTransform {
  constructor(private translate: TranslateContextService) {}

  transform(query: string, context?: string, interpolateParams?: any): string {
    if (!query || !query.length) return query;
    return this.translate.instant(query, context, interpolateParams);
  }
}

@Pipe({
  name: 'translatable',
})
@Injectable()
export class TranslatablePipe implements PipeTransform {
  transform(value: string): string {
    // transform a string to a i18n compatible string (ex: QualitativeValue -> QUALITATIVE_VALUE)
    return changeCaseToUnderscore(value)?.toUpperCase();
  }
}
