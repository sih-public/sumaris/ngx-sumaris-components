import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapGet',
})
@Injectable({ providedIn: 'root' })
export class MapGetPipe implements PipeTransform {
  transform<T = any, R = any>(val: T, args: keyof T | string | number | { key: keyof T | string | number }): R {
    if (!val) return null;
    const key = (args && typeof args === 'object' ? args.key : args) as any;
    if (key === null || key === undefined) return null;
    return val[key] as R;
  }
}

@Pipe({
  name: 'mapKeys',
})
@Injectable({ providedIn: 'root' })
export class MapKeysPipe implements PipeTransform {
  transform(map: any): any[] {
    if (!map) return null;
    return Object.keys(map);
  }
}

@Pipe({
  name: 'mapValues',
})
@Injectable({ providedIn: 'root' })
export class MapValuesPipe implements PipeTransform {
  transform<K = any, V = any>(map: any | Map<K, V>): V[] {
    if (!map) return null;
    return Object.values(map);
  }
}
