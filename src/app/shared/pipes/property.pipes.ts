import { inject, Injectable, Pipe, PipeTransform } from '@angular/core';
import { booleanToString, getPropertyByPath, isNil, isNotNil, joinPropertiesPath } from '../functions';
import { DateFormatService } from './date-format.pipe';
import { FormFieldDefinition } from '../form/field.model';
import { TranslateService } from '@ngx-translate/core';
import { DateUtils } from '../dates';

@Pipe({ name: 'propertyGet' })
export class PropertyGetPipe implements PipeTransform {
  transform(obj: any, args: string | Array<string | number> | { key: string; defaultValue?: any }): any {
    return getPropertyByPath(
      obj,
      // Path
      args && (typeof args === 'string' || Array.isArray(args) ? args : args.key),
      // Default value
      args && (args as any)?.defaultValue
    );
  }
}

abstract class AbstractValueFormatPipe {
  private _i18nYes: string;
  private _i18nNo: string;

  private dateFormat = inject(DateFormatService);
  private translate = inject(TranslateService);

  get i18nYes(): string {
    if (!this._i18nYes) {
      this._i18nYes = this.translate.instant('COMMON.YES');
    }
    return this._i18nYes;
  }

  get i18nNo(): string {
    if (!this._i18nNo) {
      this._i18nNo = this.translate.instant('COMMON.NO');
    }
    return this._i18nNo;
  }

  protected valueToString(value: any, definition?: Partial<FormFieldDefinition>): string {
    if (isNil(value)) return value;
    const type = definition?.type || typeof value;
    switch (type) {
      case 'date':
        const date = DateUtils.fromDateISOString(value);
        return this.dateFormat.transform(date) as string;
      case 'dateTime':
        const dateTime = DateUtils.fromDateISOString(value);
        return this.dateFormat.transform(dateTime, { time: true }) as string;
      case 'enum': {
        // DEBUG
        // console.debug('formatProperty by definition (type='enum', key=' +definition.key+ '):', value );
        const key = value.key ?? value;
        const item = ((definition?.values as any[]) || [value]).find((item) => (isNotNil(item?.key) ? item.key : item) === key);
        return item?.value ?? item ?? value;
      }
      case 'object':
      case 'entity': {
        if (typeof definition?.autocomplete?.displayWith === 'function') {
          return definition.autocomplete.displayWith(value);
        }
        return joinPropertiesPath(value, definition?.autocomplete?.attributes || ['label', 'name']) || undefined;
      }
      case 'entities':
      case 'enums': {
        const values = Array.isArray(value) ? value : [value];
        return values.map((val) => this.valueToString(val, { type: 'enum' })).join(', ');
      }
      case 'boolean':
        return booleanToString(value, { yesLabel: this.i18nYes, noLabel: this.i18nNo });
      case 'integer':
      case 'double':
      case 'string':
      default:
        return value;
    }
  }
}

@Pipe({
  name: 'propertyFormat',
})
@Injectable({ providedIn: 'root' })
export class PropertyFormatPipe extends AbstractValueFormatPipe implements PipeTransform {
  transform(obj: any, args: string | Array<string | number> | FormFieldDefinition): string | Promise<string> {
    if (!obj) return '';
    const definition = typeof args === 'object' ? (args as FormFieldDefinition) : undefined;
    const path = definition ? definition.key : (args as (string | number)[]);
    const value = getPropertyByPath(obj, path);
    return this.valueToString(value, definition);
  }
}

@Pipe({
  name: 'valueFormat',
})
@Injectable({ providedIn: 'root' })
export class ValueFormatPipe extends AbstractValueFormatPipe implements PipeTransform {
  transform(value: any, definition: FormFieldDefinition): string | Promise<string> {
    return this.valueToString(value, definition);
  }
}

@Pipe({
  name: 'booleanFormat',
})
export class BooleanFormatPipe extends AbstractValueFormatPipe implements PipeTransform {
  transform(value: any, opts?: { yesLabel: string; noLabel: string }): string {
    if (isNil(value)) return value;
    if (!opts) opts = { yesLabel: this.i18nYes, noLabel: this.i18nNo };
    return booleanToString(value, opts);
  }
}
