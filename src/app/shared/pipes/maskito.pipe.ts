import { Pipe, PipeTransform } from '@angular/core';
import { isNilOrBlank, isNotNil } from '../functions';
import { MaskitoMask, MaskitoOptions, MaskitoPlugin } from '@maskito/core';
import { maskitoEventHandler, maskitoWithPlaceholder } from '@maskito/kit';
import { getCaretPosition, getInputRangeFromCaretIndex, getInputSelectionRangesFromMask, selectInputRange } from '../inputs';

export type MaskitoPlaceholderPipeOptions = { defaultPrefix?: string; noDefaults?: boolean } & Partial<MaskitoOptions>;

@Pipe({
  name: 'maskitoPlaceholder',
})
export class MaskitoPlaceholderPipe implements PipeTransform {
  transform(placeholder: string, mask: MaskitoMask, opts?: MaskitoPlaceholderPipeOptions): MaskitoOptions {
    if (isNilOrBlank(placeholder)) return { mask };

    const plugins = [];
    const preprocessors = [];
    const postprocessors = [];

    if (!opts?.noDefaults) {
      const placeholderOptions = maskitoWithPlaceholder(placeholder, true);
      plugins.push(...placeholderOptions.plugins);
      preprocessors.push(...placeholderOptions.preprocessors);
      postprocessors.push(...placeholderOptions.postprocessors);
    }
    plugins.push(...(opts?.plugins || []));
    preprocessors.push(...(opts?.preprocessors || []));
    postprocessors.push(...(opts?.postprocessors || []));

    if (isNotNil(opts?.defaultPrefix)) {
      plugins.push(maskitoPrefixPlugin(opts.defaultPrefix, placeholder));
    }
    plugins.push(maskitoAutoSelectByMaskPattern());

    return {
      overwriteMode: opts?.overwriteMode || 'replace',
      plugins,
      preprocessors,
      postprocessors,
      mask,
    };
  }
}

export function maskitoPrefixPlugin(prefix: string, placeholder?: string): MaskitoPlugin {
  return maskitoEventHandler('focus', (element: HTMLInputElement | HTMLTextAreaElement, options: Required<MaskitoOptions>) => {
    if (element.value === '') {
      element.value = prefix;
      element.dispatchEvent(new Event('input'));
    } else if (element.value === placeholder) {
      element.value = prefix + placeholder.substring(prefix.length);
      element.dispatchEvent(new Event('input'));
    }
  });
}

export function maskitoAutoSelectByMaskPattern(): MaskitoPlugin {
  return maskitoEventHandler('click', (element: HTMLInputElement | HTMLTextAreaElement, options: Required<MaskitoOptions>) => {
    if (options?.mask instanceof Array) {
      const caretIndex = getCaretPosition(element);
      const maskPatternInputRanges = getInputSelectionRangesFromMask(options.mask);
      const selectedInputRange = getInputRangeFromCaretIndex(caretIndex, maskPatternInputRanges);
      if (isNotNil(selectedInputRange)) {
        selectInputRange(element, selectedInputRange[0], selectedInputRange[1] + 1);
      }
    }
  });
}
