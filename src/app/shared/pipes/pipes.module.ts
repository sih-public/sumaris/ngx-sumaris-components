import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { DateFormatPipe } from './date-format.pipe';
import { DateDiffDurationPipe } from './date-diff-duration.pipe';
import { DateFromNowPipe } from './date-from-now.pipe';
import { LatitudeFormatPipe, LatLongFormatPipe, LongitudeFormatPipe } from './latlong-format.pipe';
import { NumberFormatPipe } from './number-format.pipe';
import { HighlightPipe } from './highlight.pipe';
import { FileSizePipe } from './file-size.pipe';
import { DurationPipe } from './duration.pipe';
import { EvenPipe, MathAbsPipe, OddPipe } from './math.pipes';
import {
  ArrayDistinctPipe,
  ArrayFilterPipe,
  ArrayFirstPipe,
  ArrayIncludesPipe,
  ArrayJoinPipe,
  ArrayLastPipe,
  ArrayLengthPipe,
  ArrayPluckPipe,
  EmptyArrayPipe,
  NotEmptyArrayPipe,
} from './arrays.pipe';
import { MapGetPipe, MapKeysPipe, MapValuesPipe } from './maps.pipe';
import { TranslatablePipe, TranslateContextPipe } from './translate-context.pipe';
import { NgInitDirective } from './ng-init.pipe';
import {
  FormErrorPipe,
  FormErrorTranslatePipe,
  FormGetArrayPipe,
  FormGetControlPipe,
  FormGetGroupPipe,
  FormGetPipe,
  FormGetValuePipe,
} from './form.pipes';
import { BooleanFormatPipe, PropertyFormatPipe, PropertyGetPipe, ValueFormatPipe } from './property.pipes';
import { MatColorPipe } from './colors.pipe';
import { AsAnyPipe, AsArrayPipe, AsBooleanPipe, AsFloatLabelTypePipe, AsObservablePipe } from './types.pipes';
import { MaskitoPlaceholderPipe } from './maskito.pipe';
import {
  IsEmptySelectionPipe,
  IsMultipleSelectionPipe,
  IsNotEmptySelectionPipe,
  IsSelectedPipe,
  IsSingleSelectionPipe,
  SelectionLengthPipe,
} from './selection.pipes';
import { BadgeNumberPipe } from './badge.pipes';
import {
  CapitalizePipe,
  IsNilOrBlankPipe,
  IsNilOrNaNPipe,
  IsNilPipe,
  IsNotNilOrBlankPipe,
  IsNotNilOrNaNPipe,
  IsNotNilPipe,
  StrIncludesPipe,
  StrLengthPipe,
  StrReplacePipe,
  ToStringPipe,
  TruncHtmlPipe,
  TruncTextPipe,
} from './string.pipes';
import { NoHtmlPipe, SafeHtmlPipe, SafeStylePipe } from './html.pipes';
import { DisplayWithPipe } from './display-with.pipe';
import { IsValidDatePipe } from './dates.pipe';
import { FirstFalsePipe, FirstPipe, FirstTruePipe } from './observable.pipes';

const components = [
  PropertyGetPipe,
  PropertyFormatPipe,
  ValueFormatPipe,
  DateFormatPipe,
  DateDiffDurationPipe,
  DurationPipe,
  DateFromNowPipe,
  LatLongFormatPipe,
  LatitudeFormatPipe,
  LongitudeFormatPipe,
  HighlightPipe,
  NumberFormatPipe,
  FileSizePipe,
  MathAbsPipe,
  OddPipe,
  EvenPipe,
  NotEmptyArrayPipe,
  EmptyArrayPipe,
  ArrayLengthPipe,
  ArrayFirstPipe,
  ArrayLastPipe,
  ArrayPluckPipe,
  ArrayIncludesPipe,
  ArrayFilterPipe,
  ArrayJoinPipe,
  ArrayDistinctPipe,
  MapGetPipe,
  MapKeysPipe,
  MapValuesPipe,
  IsNilOrBlankPipe,
  IsNotNilOrBlankPipe,
  IsNilOrNaNPipe,
  IsNotNilOrNaNPipe,
  IsNotNilPipe,
  IsNilPipe,
  IsValidDatePipe,
  ToStringPipe,
  CapitalizePipe,
  StrLengthPipe,
  StrIncludesPipe,
  StrReplacePipe,
  TruncTextPipe,
  TruncHtmlPipe,
  BooleanFormatPipe,
  TranslateContextPipe,
  TranslatablePipe,
  NgInitDirective,
  FormErrorPipe,
  FormErrorTranslatePipe,
  FormGetPipe,
  FormGetControlPipe,
  FormGetArrayPipe,
  FormGetGroupPipe,
  FormGetValuePipe,
  MatColorPipe,
  AsAnyPipe,
  AsArrayPipe,
  AsObservablePipe,
  AsFloatLabelTypePipe,
  MaskitoPlaceholderPipe,
  IsSelectedPipe,
  IsNotEmptySelectionPipe,
  IsEmptySelectionPipe,
  SelectionLengthPipe,
  IsMultipleSelectionPipe,
  IsSingleSelectionPipe,
  BadgeNumberPipe,
  AsBooleanPipe,
  SafeHtmlPipe,
  SafeStylePipe,
  NoHtmlPipe,
  DisplayWithPipe,
  FirstTruePipe,
  FirstFalsePipe,
  FirstPipe,
];

@NgModule({
  imports: [CommonModule, IonicModule, TranslateModule],
  declarations: components,
  exports: components,
})
export class SharedPipesModule {}
