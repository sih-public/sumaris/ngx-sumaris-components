import { Pipe, PipeTransform } from '@angular/core';
import { Moment } from 'moment';
import { DateUtils, fromDateISOString } from '../dates';

@Pipe({
  name: 'isValidDate',
})
export class IsValidDatePipe implements PipeTransform {
  transform(value: string | Moment, opts?: { requiredTime?: boolean }): boolean {
    const date = fromDateISOString(value);
    return date?.isValid() && (!opts?.requiredTime || !DateUtils.isNoTime(date));
  }
}
