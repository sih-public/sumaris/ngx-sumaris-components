import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { removeDiacritics, toBoolean } from '../functions';
import { RegExpUtils } from '../regexps';

@Pipe({
  name: 'highlight',
})
@Injectable({ providedIn: 'root' })
export class HighlightPipe implements PipeTransform {
  // Replace search text by bold representation
  transform(value: any, args?: string | { search: string; withAccent?: boolean }): string {
    if (typeof value !== 'string' || !args) return value;
    let searchText = typeof args === 'string' ? args : args.search;
    const withAccent = typeof args !== 'string' ? toBoolean(args?.withAccent, false) : false;

    // Skip if :
    // - not a string (e.g. an object)
    // - or blank string
    if (typeof searchText !== 'string') return value;

    // Remove unnecessary wildcard (e.g. 'a**' becomes 'a')
    searchText = RegExpUtils.cleanSearchText(searchText);
    if (!searchText?.length) return value;

    if (withAccent) {
      // Remove all accent characters to allow versatile comparison
      const cleanedSearchText = removeDiacritics(searchText.toUpperCase());
      const cleanValue = removeDiacritics(value.toUpperCase());
      const startIndex = cleanValue.indexOf(cleanedSearchText);
      if (startIndex === -1) return value; // no match
      const endIndex = startIndex + cleanedSearchText.length;
      return value.substring(0, startIndex) + '<b>' + value.substring(startIndex, endIndex) + '</b>' + value.substring(endIndex);
    }

    // Default behaviour, using regexp
    else {
      const regexp = RegExpUtils.compileSearchRegexp(searchText, 'i');
      return value.replace(regexp, '<b>$&</b>');
    }
  }
}
