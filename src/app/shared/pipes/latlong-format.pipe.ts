import { Pipe, PipeTransform } from '@angular/core';
import { formatLatitude, formatLongitude, LatLongFormatOptions } from '../material/latlong/latlong.utils';

@Pipe({
  name: 'latitudeFormat',
})
export class LatitudeFormatPipe implements PipeTransform {
  transform = formatLatitude;
}

@Pipe({
  name: 'longitudeFormat',
})
export class LongitudeFormatPipe implements PipeTransform {
  transform = formatLongitude;
}

@Pipe({
  name: 'latLongFormat',
})
/**
 * @deprecated use 'latitudeFormat' or 'longitudeFormat' instead
 */
export class LatLongFormatPipe implements PipeTransform {
  transform(value: number, opts?: Partial<LatLongFormatOptions> & { type: 'latitude' | 'longitude' }): string | Promise<string> {
    return (!opts || opts?.type === 'latitude' ? formatLatitude : formatLongitude)(value, opts);
  }
}
