import { Pipe, PipeTransform } from '@angular/core';
import { getPropertyByPath, isNotNil, removeDuplicatesFromArray } from '../functions';

@Pipe({
  name: 'isNotEmptyArray',
})
export class NotEmptyArrayPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[]): boolean {
    if (!Array.isArray(val)) return false;
    return val.length > 0;
  }
}

@Pipe({
  name: 'isEmptyArray',
})
export class EmptyArrayPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[]): boolean {
    if (!Array.isArray(val)) return true;
    return val.length === 0;
  }
}

@Pipe({
  name: 'isArrayLength',
})
export class ArrayLengthPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[], args?: { greaterThan?: number; equals?: number; lessThan?: number }): boolean {
    args = args || {};
    const size = !Array.isArray(val) ? 0 : val.length;
    if (isNotNil(args.lessThan)) {
      return size < args.lessThan;
    }
    if (isNotNil(args.greaterThan)) {
      return size > args.greaterThan;
    }
    if (isNotNil(args.equals)) {
      return size === args.equals;
    }
    return false;
  }
}

@Pipe({
  name: 'arrayFirst',
})
export class ArrayFirstPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[]): T | null {
    if (!Array.isArray(val)) return null;
    return val.length > 0 ? val[0] : null;
  }
}

@Pipe({
  name: 'arrayLast',
})
export class ArrayLastPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[]): T | null {
    if (!Array.isArray(val)) return null;
    return val.length > 0 ? val[val.length - 1] : null;
  }
}

@Pipe({
  name: 'arrayPluck',
})
export class ArrayPluckPipe implements PipeTransform {
  /**
   *
   * @param val
   * @param opts property to get, or options
   */
  transform<T = any>(val: unknown | T[], opts: string | string[] | { property: string | string[]; omitNil?: boolean }): any[] {
    if (!Array.isArray(val) || !opts) return null;
    const property = typeof opts === 'string' || Array.isArray(opts) ? (opts as string | string[]) : opts.property;
    return opts['omitNil'] !== true
      ? val.map((value) => getPropertyByPath(value, property))
      : val.map((value) => getPropertyByPath(value, property)).filter(isNotNil);
  }
}

@Pipe({
  name: 'arrayIncludes',
})
export class ArrayIncludesPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[], searchElement: T, fromIndex?: number): boolean {
    if (!Array.isArray(val)) return false;
    return (val && val.includes(searchElement, fromIndex)) || false;
  }
}

@Pipe({
  name: 'arrayFilter',
})
export class ArrayFilterPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[], filterFn: (T) => boolean): T[] | null {
    if (!Array.isArray(val)) return null;
    return val.filter(filterFn);
  }
}

@Pipe({
  name: 'arrayJoin',
})
export class ArrayJoinPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[], separator: string): string {
    if (!Array.isArray(val)) return '';
    return val.join(separator) || '';
  }
}

@Pipe({
  name: 'arrayDistinct',
})
export class ArrayDistinctPipe implements PipeTransform {
  transform<T = any>(val: unknown | T[], opts?: string | { property: string; omitNil?: boolean }): any[] {
    if (!Array.isArray(val)) return null;
    const property = typeof opts === 'string' ? (opts as string) : opts?.property;
    return opts?.['omitNil'] !== true ? removeDuplicatesFromArray(val, property) : removeDuplicatesFromArray(val, property).filter(isNotNil);
  }
}
