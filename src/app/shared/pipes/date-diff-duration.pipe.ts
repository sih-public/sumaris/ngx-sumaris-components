import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Moment } from 'moment';
import { DateAdapter } from '@angular/material/core';
import { DATE_ISO_PATTERN } from '../constants';
import { TranslateService } from '@ngx-translate/core';
import { DateUtils } from '../dates';
import { unitOfTime } from 'moment/moment';

@Pipe({
  name: 'dateDiffDuration',
})
@Injectable({ providedIn: 'root' })
export class DateDiffDurationPipe implements PipeTransform {
  protected dayUnit: string;

  constructor(
    private dateAdapter: DateAdapter<Moment>,
    translate: TranslateService
  ) {
    this.dayUnit = translate.instant('COMMON.DAY_UNIT');
    translate.get('COMMON.DAY_UNIT').subscribe((dayUnit) => (this.dayUnit = dayUnit));
  }

  transform(
    value: { startValue: string | Moment; endValue: string | Moment },
    opts?: { seconds?: boolean; unit?: unitOfTime.Base }
  ): string | Promise<string> {
    if (!value.startValue || !value.endValue) return '';

    const startDate = this.dateAdapter.parse(value.startValue, DATE_ISO_PATTERN);
    const endDate = this.dateAdapter.parse(value.endValue, DATE_ISO_PATTERN);

    return this.format(startDate, endDate, opts);
  }

  format(startDate: Moment, endDate: Moment, opts: { seconds?: boolean; unit?: unitOfTime.Base }): string {
    const duration = DateUtils.moment.duration(endDate.diff(startDate));

    // Convert to the expected unit, if any
    if (opts?.unit) return '' + duration.as(opts.unit);

    const withSeconds = opts?.seconds;
    if (!withSeconds && duration.asMinutes() < 0) return '';

    const timeDuration = DateUtils.moment(0).hour(duration.hours()).minute(duration.minutes());
    if (withSeconds) {
      timeDuration.second(duration.seconds());
    }

    const days = Math.floor(duration.asDays());
    return (days > 0 ? days.toString() + (this.dayUnit + ' ') : '') + timeDuration.format(withSeconds ? 'HH:mm:ss' : 'HH:mm');
  }
}
