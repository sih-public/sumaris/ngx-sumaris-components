import { Injectable, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { isMoment, Moment } from 'moment';
import { DATE_ISO_PATTERN } from '../constants';
import { TranslateService } from '@ngx-translate/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Subscription } from 'rxjs';

abstract class AbstractDateFormat {
  private datePattern: string;
  private dateTimePattern: string;
  private dateTimeSecondsPattern: string;

  protected constructor(
    protected dateAdapter: MomentDateAdapter,
    protected translate: TranslateService
  ) {
    const translations = translate.instant(['COMMON.DATE_PATTERN', 'COMMON.DATE_TIME_PATTERN', 'COMMON.DATE_TIME_SECONDS_PATTERN']);
    this._updateTranslations(translations);
  }

  transform(value: string | Moment | Date, args?: { pattern?: string; time?: boolean; seconds?: boolean }): string {
    const pattern =
      (args && args.pattern) || (args && args.time ? (args.seconds ? this.dateTimeSecondsPattern : this.dateTimePattern) : this.datePattern);
    // Keep original moment object, if possible (to avoid a conversion)
    const date: Moment = isMoment(value) ? value : this.dateAdapter.parse(value, DATE_ISO_PATTERN);
    return (date && this.dateAdapter.format(date, pattern)) || '';
  }

  protected _updateTranslations(translations: any) {
    this.datePattern = this._getTranslationValue(translations, 'COMMON.DATE_PATTERN', 'DD/MM/YYYY');
    this.dateTimePattern = this._getTranslationValue(translations, 'COMMON.DATE_TIME_PATTERN', 'DD/MM/YYYY HH:mm');
    this.dateTimeSecondsPattern = this._getTranslationValue(translations, 'COMMON.DATE_TIME_SECONDS_PATTERN', 'DD/MM/YYYY HH:mm:ss');
  }

  protected _getTranslationValue(translations: any, key: string, defaultValue: string): string {
    return translations[key] !== key ? translations[key] : defaultValue;
  }
}

@Pipe({
  name: 'dateFormat',
})
export class DateFormatPipe extends AbstractDateFormat implements PipeTransform {
  constructor(dateAdapter: MomentDateAdapter, translate: TranslateService) {
    super(dateAdapter, translate);
  }
}

@Injectable({ providedIn: 'root' })
export class DateFormatService extends AbstractDateFormat implements OnDestroy {
  private _subscription: Subscription;

  constructor(dateAdapter: MomentDateAdapter, translate: TranslateService) {
    super(dateAdapter, translate);

    // Subscribe to changes (e.g. when platform not ready yet)
    this._subscription = translate
      .get(['COMMON.DATE_PATTERN', 'COMMON.DATE_TIME_PATTERN', 'COMMON.DATE_TIME_SECONDS_PATTERN'])
      .subscribe((translations) => this._updateTranslations(translations));
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  format(date: Moment, pattern: any): string {
    return this.dateAdapter.format(date, pattern);
  }

  parse(value: string, parseFormat: any): Moment {
    return isMoment(value) ? value : this.dateAdapter.parse(value, parseFormat || DATE_ISO_PATTERN);
  }
}
