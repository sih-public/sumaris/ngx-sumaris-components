import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'badgeNumber',
})
export class BadgeNumberPipe implements PipeTransform {
  transform(val: number, max = 99): string {
    if (val !== undefined && val !== null) {
      if (val > max) {
        return max.toFixed(0) + '+';
      }
      return val.toFixed(0);
    } else {
      return '';
    }
  }
}
