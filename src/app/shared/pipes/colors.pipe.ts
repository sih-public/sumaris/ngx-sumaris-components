import { Pipe, PipeTransform } from '@angular/core';
import { AppColors } from '../types';
import { ThemePalette } from '@angular/material/core';

@Pipe({
  name: 'matColor',
})
export class MatColorPipe implements PipeTransform {
  transform(color: AppColors): ThemePalette | undefined {
    if (!color) return undefined;
    switch (color) {
      // Redirect 'secondary' and 'tertiary' to primary (not exists in material theme)
      case 'secondary':
      case 'tertiary':
        return 'primary';
      case 'warning':
        return 'warn';
      case 'medium':
        return undefined; // Use default
      default:
        return color as ThemePalette;
    }
  }
}
