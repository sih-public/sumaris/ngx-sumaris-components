import { Pipe, PipeTransform } from '@angular/core';
import { noHtml } from '../functions';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'noHtml',
})
/**
 * Remove all HTML tags, from an input string
 */
export class NoHtmlPipe implements PipeTransform {
  transform(value: string): string {
    return noHtml(value);
  }
}

@Pipe({
  name: 'safeHtml',
})
/**
 * SafeHtmlPipe is a custom Angular pipe designed to sanitize and transform
 * potentially unsafe HTML content into safe HTML. It leverages the noHtml
 * function to strip away any HTML tags from the input string, ensuring that
 * the output string is safe for display in the frontend without risking
 * security vulnerabilities like Cross-Site Scripting (XSS) attacks.
 *
 * This pipe can be used when there is a need to render user-generated or
 * dynamically fetched HTML content safely within an Angular application.
 *
 * Methods:
 * - transform: Accepts a string as input and returns a sanitized version
 *              of the string with HTML tags removed.
 */
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {}
  transform(value: string): SafeHtml {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Pipe({
  name: 'safeStyle',
})
export class SafeStylePipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {}
  transform(value: string): SafeHtml {
    return this.sanitized.bypassSecurityTrustStyle(value);
  }
}
