import {
  AfterContentChecked,
  booleanAttribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { TableDataSource, TableElement } from '@e-is/ngx-material-table';
import { ENVIRONMENT, Environment } from '../../../../environments/environment.class';
import { ImageService } from '../image.service';
import { Image } from '../image.model';
import { AlertController, IonicSlides, IonModal, IonPopover, Platform } from '@ionic/angular';
import { isMobile } from '../../platforms';
import { fadeInAnimation, fadeInOutAnimation } from '../../material/material.animations';
import { isEmptyArray, toBoolean } from '../../functions';
import { CollectionViewer, isDataSource, ListRange } from '@angular/cdk/collections';
import { takeUntil } from 'rxjs/operators';
import { Alerts } from '../../alerts';
import { TranslateService } from '@ngx-translate/core';
import { AppColors } from '../../types';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { createPromiseEventEmitter, emitPromiseEvent } from '../../events';
import { PredefinedColors } from '@ionic/core';
import { Swiper } from 'swiper/types';

export declare type GalleryMode = 'mosaic' | 'list';

const GalleryModeColSize = Object.freeze({
  mosaic: 4,
  list: 12,
});

export interface ISwiper extends Swiper {
  activeIndex: number;
  imagesLoaded: number;
}

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss'],
  animations: [fadeInAnimation, fadeInOutAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppImageGalleryComponent<T extends Image> implements OnInit, OnDestroy, CollectionViewer, AfterContentChecked {
  protected _destroySubject = new Subject<void>();
  protected _renderChangeSubscription: Subscription;
  protected _subscription = new Subscription();
  protected _dataSource: TableDataSource<T>;
  protected _data$ = new BehaviorSubject<TableElement<T>[] | ReadonlyArray<TableElement<T>>>(null);

  protected _colSize: number;
  protected _selectedRowIndex: number;
  protected _presentingElement: Element = null;
  protected _titleForm: FormGroup;
  protected _titleAutofocus = false;
  protected _slidesSubscription: Subscription;
  protected _swiper: Swiper;

  protected swiperModules = [IonicSlides];
  protected zoomActive = false;
  protected zoomScale = 1;

  viewChange: BehaviorSubject<ListRange>;

  @Input() cardColor: string | PredefinedColors;
  @Input({ transform: booleanAttribute }) debug: boolean = null;
  @Input({ transform: booleanAttribute }) disabled = false;
  @Input({ transform: booleanAttribute }) readOnly = false;
  @Input({ transform: booleanAttribute }) mobile: boolean = null;
  @Input() mode: GalleryMode = null;
  @Input() confirmBeforeDelete = true;
  @Input({ transform: booleanAttribute }) showToolbar = true;
  @Input({ transform: booleanAttribute }) showFabButton: boolean;
  @Input({ transform: booleanAttribute }) showTitle = true;
  @Input({ transform: booleanAttribute }) showAddTextButton = false;
  @Input({ transform: booleanAttribute }) showAddCardButton = true;
  @Input() addButtonColor: AppColors = 'tertiary';
  @Input() addButtonText: string = 'COMMON.BTN_ADD';

  @Input() set dataSource(dataSource: TableDataSource<T, any, any, any>) {
    if (this._dataSource !== dataSource) {
      this._switchDataSource(dataSource);
    }
  }
  get dataSource(): TableDataSource<T, any, any, any> {
    return this._dataSource;
  }
  get enabled() {
    return !this.disabled;
  }

  get rows(): TableElement<T>[] | ReadonlyArray<TableElement<T>> {
    return this._data$.value;
  }

  get rowCount() {
    return this.rows?.length || 0;
  }

  get activeIndex(): number {
    return this.getSwiper()?.activeIndex || 0;
  }

  @ViewChild('zoomModal') zoomModal: IonModal;
  @ViewChild('titlePopover') titlePopover: IonPopover;
  @ViewChild('modalSwiper') swiperRef: ElementRef;

  @Output() onBeforeDeleteRows = createPromiseEventEmitter<boolean, { rows: TableElement<T>[] }>();
  @Output() onAfterAddRows = new EventEmitter<TableElement<T>[]>();
  @Output() onAfterEditRow = new EventEmitter<TableElement<T>>();

  constructor(
    protected image: ImageService,
    protected platform: Platform,
    protected alterCtrl: AlertController,
    protected translate: TranslateService,
    protected cd: ChangeDetectorRef,
    @Optional() @Inject(ENVIRONMENT) environment?: Environment
  ) {
    this.viewChange = new BehaviorSubject({ start: 0, end: Number.MAX_VALUE });

    // DEV log
    this.debug = environment && !environment.production;
  }

  ngOnInit() {
    this.mobile = toBoolean(this.mobile, isMobile(window));
    this.mode = this.mode ?? (this.mobile ? 'list' : 'mosaic');
    this._colSize = GalleryModeColSize[this.mode];
    this.showToolbar = toBoolean(this.showToolbar, !this.mobile);
    this.showAddCardButton = toBoolean(this.showAddCardButton, this.mobile);
    this.showFabButton = toBoolean(this.showFabButton, this.mobile && !this.showAddCardButton);

    // Get modal presenting element (iOS only)
    this._presentingElement = this.mobile ? document.querySelector('.ion-page') : null;
  }

  ngAfterContentChecked() {
    if (this.dataSource && !this._renderChangeSubscription) {
      this._observeRenderChanges();
    }
  }

  ngOnDestroy() {
    this._destroySubject.next();
    this._destroySubject.complete();

    this._subscription.unsubscribe();
    this._slidesSubscription?.unsubscribe();

    if (isDataSource(this.dataSource)) {
      this.dataSource.disconnect(this);
    }
  }

  async add(event?: Event): Promise<void> {
    const images = await this.image.add(event);

    if (isEmptyArray(images)) return;

    console.info(`[image-gallery] Adding new ${images.length} image(s)`, images);

    const rows: TableElement<T>[] = [];
    for (const image of images) {
      const row = await this._dataSource.createNew();
      row.currentData = image as unknown as T;
      rows.push(row);
      const added = await row.confirmEditCreate();
      if (!added) {
        console.warn('[image-gallery] Cannot confirm new image!');
        break;
      }
    }

    this.onAfterAddRows.emit(rows);
  }

  async delete(event: Event, row: TableElement<T>, opts?: { interactive?: boolean }): Promise<boolean> {
    if (event?.defaultPrevented || this.readOnly) return false;
    event?.preventDefault();
    event?.stopPropagation();

    // Check using emitter
    const confirmed = await this.canDeleteRows([row]);
    if (!confirmed) return false;

    let deleted: boolean;
    if (row.id === -1) {
      console.info('[image-gallery] Removing new image');
      deleted = await row.cancelOrDelete();
    } else {
      console.info('[image-gallery] Removing image (at #' + row.id + ')');
      deleted = await row.delete();
    }

    return deleted;
  }

  toggleViewMode(_?: Event) {
    switch (this.mode) {
      case 'list':
        this.mode = 'mosaic';
        break;
      default:
        this.mode = 'list';
    }
    this._colSize = GalleryModeColSize[this.mode];
    this.markForCheck();
  }

  trackByFn(index: number, row: TableElement<T>) {
    return row.id;
  }

  async editTitle(event: MouseEvent, row: TableElement<T>, cardToolbar: any) {
    if (event?.defaultPrevented) return;
    event?.preventDefault();
    event?.stopPropagation();
    event?.stopImmediatePropagation();

    this.showCardToolbar(cardToolbar);

    const titleControl = new FormControl(row.currentData.title, Validators.compose([Validators.required, Validators.maxLength(255)]));
    this._titleForm = new FormGroup({ title: titleControl });
    this._titleAutofocus = this.mobile && this.enabled && !this.readOnly;
    this.cd.detectChanges();

    await this.titlePopover.present();

    const { data, role } = await this.titlePopover.onDidDismiss();

    if (!role || role !== 'CANCEL') {
      console.debug('[image-gallery] User set title: ' + data);
      row.currentData.title = data;
      this.markForCheck();
      this.onAfterEditRow.emit(row);
    }

    // Hide the toolbar, because the focus have been given to the event's origin (the edit button)
    setTimeout(() => this.hideCardToolbar(cardToolbar), 750);
  }

  protected showCardToolbar(cardToolbar: any) {
    const el = cardToolbar['el'] as HTMLElement;
    el.classList.add('focused');
  }

  protected hideCardToolbar(cardToolbar: any) {
    const el = cardToolbar['el'] as HTMLElement;
    el.classList.remove('focused');
  }

  protected _switchDataSource(dataSource: TableDataSource<T>) {
    if (isDataSource(this._dataSource)) {
      this._dataSource.disconnect(this);
    }
    this._dataSource = dataSource;
  }

  protected _observeRenderChanges() {
    // If no data source has been set, there is nothing to observe for changes.
    if (!this.dataSource) {
      return;
    }
    let dataStream: Observable<TableElement<T>[] | ReadonlyArray<TableElement<T>>>;
    if (isDataSource(this.dataSource)) {
      dataStream = this.dataSource.connect(this);
    }
    if (dataStream === undefined) {
      throw this.getTableUnknownDataSourceError();
    }
    this._renderChangeSubscription?.unsubscribe();
    this._renderChangeSubscription = dataStream.pipe(takeUntil(this._destroySubject)).subscribe((data) => {
      console.debug('[image-gallery] Received new rows', data);
      this._data$.next(data);
    });
  }

  protected getTableUnknownDataSourceError() {
    return Error(`Provided data source did not match an array, Observable, or DataSource`);
  }

  protected async clickRow(row: TableElement<T>, index: number) {
    this._selectedRowIndex = index;
    await this.zoomModal.present();
    await this.zoomModal.onDidDismiss();
  }

  protected activeSlideIndex = 0;

  protected getSwiper(swiperElement?: any): Swiper {
    this._swiper = (swiperElement || this.swiperRef?.nativeElement)?.swiper || this._swiper;
    return this._swiper;
  }

  protected onSlideChange(swiperElement: any) {
    const swiper = this.getSwiper(swiperElement);
    this.activeSlideIndex = swiper.activeIndex;
    this.markForCheck();
  }

  protected initModalSlides(swiperElement: any) {
    if (this._selectedRowIndex > 0) {
      if (this.debug) console.debug(`[image-gallery] Initializing modal, selectedRowIndex=${this._selectedRowIndex}`);
      // Use a setTimeout otherwise the swiper cannot slide
      setTimeout(() => {
        const swiper = this.getSwiper(swiperElement);
        swiper.slideTo(this._selectedRowIndex, 0);
      });
    }
  }

  protected async zoom(swiperElement: any, zoomIn: boolean) {
    if (this.debug) console.debug('[image-gallery] zoom ' + (zoomIn ? 'in' : 'out'), swiperElement);
    const swiper = this.getSwiper(swiperElement);
    const zoom = swiper.zoom;
    if (zoomIn) zoom.in();
    else zoom.out();
  }

  protected async deleteFromModal(event, swiperElement: any): Promise<boolean> {
    const swiper = this.getSwiper(swiperElement);
    const currentRow = this.rows.find((row, index) => index === swiper.activeIndex);
    if (!currentRow) return false; // Row not found

    const deleted = await this.delete(event, currentRow);

    if (this.rows.length === 0) {
      await this.zoomModal.dismiss();
    } else {
      // Reset the zoom
      swiper.zoom.out();
      swiper.update();
      this.markForCheck();
    }
    return deleted;
  }

  protected async slidePrev(swiperElement: any) {
    const swiper = this.getSwiper(swiperElement);
    const activeIndex = swiper.activeIndex;
    if (activeIndex === 0) {
      // Loop to last
      await swiper.slideTo(this.rows.length - 1, 0 /*no transition, to avoid a "revert" effect*/);
    } else {
      await swiper.slidePrev();
    }
  }

  protected async slideNext(swiperElement: any) {
    const swiper = this.getSwiper(swiperElement);
    const activeIndex = swiper.activeIndex;
    if (activeIndex === this.rows.length - 1) {
      // Loop to first
      await swiper.slideTo(0, 0 /*no transition, to avoid a "revert" effect*/);
    } else {
      await swiper.slideNext();
    }
  }

  protected async canDeleteRows(rows: TableElement<T>[], opts?: { interactive?: boolean }): Promise<boolean> {
    // Check using emitter
    if (this.onBeforeDeleteRows.observed) {
      try {
        const canDelete = await emitPromiseEvent(this.onBeforeDeleteRows, 'canDelete', {
          detail: { rows },
        });
        if (!canDelete) return false;
      } catch (err) {
        if (err === 'CANCELLED') return false; // User cancel
        console.error('Error while checking if can delete rows', err);
        throw err;
      }
    }

    if (this.confirmBeforeDelete) {
      const confirmed = await Alerts.askDeleteConfirmation(this.alterCtrl, this.translate, event);
      if (!confirmed) return false; // Cancelled
    }

    return true;
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}
