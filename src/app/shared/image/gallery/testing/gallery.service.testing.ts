import { Injectable } from '@angular/core';

import { SortDirection } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { StartableService } from '../../../services/startable-service.class';
import { EntitiesServiceWatchOptions, IEntitiesService, LoadResult } from '../../../services/entity-service.class';
import { Platform } from '@ionic/angular';
import { isNil, isNotNil, removeDuplicatesFromArray } from '../../../functions';
import { Entity, EntityUtils } from '../../../../core/services/model/entity.model';
import { EntityClass } from '../../../../core/services/model/entity.decorators';
import { Image } from '../../image.model';
import { EntityFilter } from '../../../../core/services/model/filter.model';

// @dynamic
@EntityClass({ typename: 'ImageAttachmentVO' })
export class ImageAttachment extends Entity<ImageAttachment> implements Image {
  static fromObject: (source: any, opts?: any) => ImageAttachment;

  dataUrl: string = null;
  url: string = null;
  title: string = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.url = source.url;
    this.dataUrl = source.dataUrl;
    this.title = source.title;
  }
}

// @dynamic
@EntityClass({ typename: 'ImageAttachmentFilterVO' })
export class ImageAttachmentFilter extends EntityFilter<ImageAttachmentFilter, ImageAttachment> {
  static fromObject: (source: any, opts?: any) => ImageAttachmentFilter;
}

@Injectable()
export class ImageAttachmentService extends StartableService implements IEntitiesService<ImageAttachment, ImageAttachmentFilter> {
  $images = new BehaviorSubject<Partial<ImageAttachment>[]>([
    ImageAttachment.fromObject({ id: 0, url: 'https://test.sumaris.net/assets/img/bg/ray-1.jpg', title: 'ray #1' }),
    ImageAttachment.fromObject({ id: 1, url: 'https://test.sumaris.net/assets/img/bg/ray-2.jpg', title: 'ray #2' }),
  ]);

  constructor(protected platform: Platform) {
    super(platform);
    console.debug('[gallery-testing-service] Create service');
  }

  protected ngOnStart(): Promise<any> {
    return Promise.resolve(undefined);
  }

  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: ImageAttachmentFilter,
    opts?: EntitiesServiceWatchOptions & { query?: any }
  ): Observable<LoadResult<ImageAttachment>> {
    console.debug('[gallery-testing-service] Watching images...');
    return this.$images.pipe(
      map((images) => {
        let data = (images || []).map(ImageAttachment.fromObject);

        data = EntityUtils.sort(data, sortBy, sortDirection);

        console.debug(`[gallery-testing-service] Loaded ${data.length} images...`);

        return { data, total: data.length };
      })
    );
  }

  async saveAll(entities: ImageAttachment[]): Promise<any[]> {
    console.debug('[gallery-service-testing] Saving images...' + JSON.stringify(entities));
    if (!entities) return [];

    const existingImages = this.$images.value;
    let currentId = existingImages.reduce((res, image) => Math.max(res, +image?.id || 0), 0);
    entities
      .filter((item) => isNil(item.id))
      .forEach((item) => {
        item.id = ++currentId;
      });
    this.$images.next(removeDuplicatesFromArray([...existingImages, ...entities], 'id'));
  }

  async deleteAll(entities: ImageAttachment[]): Promise<any> {
    const deletedIds = (entities || []).map((item) => item.id).filter(isNotNil);
    const filteredData = this.$images.value.filter((item) => !deletedIds.includes(item.id));
    this.$images.next(filteredData);
  }

  asFilter(filter: any) {
    return ImageAttachmentFilter.fromObject(filter);
  }
}
