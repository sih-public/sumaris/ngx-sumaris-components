import { EntityClass } from '../../../../core/services/model/entity.decorators';
import { Entity } from '../../../../core/services/model/entity.model';
import { EntityFilter } from '../../../../core/services/model/filter.model';
import { Image } from '../../image.model';

@EntityClass({ typename: 'ImageAttachmentVO' })
export class ImageAttachment extends Entity<ImageAttachment> implements Image {
  static fromObject: (source: any, opts?: any) => ImageAttachment;

  dataUrl: string = null;
  url: string = null;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.url = source.url;
    this.dataUrl = source.dataUrl;
  }
}

@EntityClass({ typename: 'ImageAttachmentFilterVO' })
export class ImageAttachmentFilter extends EntityFilter<ImageAttachmentFilter, ImageAttachment> {
  static fromObject: (source: any, opts?: any) => ImageAttachmentFilter;
}
