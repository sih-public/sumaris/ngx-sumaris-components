import { Component, OnDestroy, OnInit } from '@angular/core';
import { ImageAttachment, ImageAttachmentService } from './gallery.service.testing';
import { EntitiesTableDataSource } from '../../../../core/table/entities-table-datasource.class';
import { Subscription } from 'rxjs';
import { ImageAttachmentFilter } from './gallegry.model.testing';

@Component({
  selector: 'app-gallery-test-page',
  templateUrl: './gallery.testing.html',
  providers: [{ provide: ImageAttachmentService, useClass: ImageAttachmentService }],
})
export class GalleryTestPage implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  readonly dataSource: EntitiesTableDataSource<ImageAttachment, ImageAttachmentFilter>;

  constructor(protected dataService: ImageAttachmentService) {
    this.dataSource = new EntitiesTableDataSource<ImageAttachment, ImageAttachmentFilter>(ImageAttachment, this.dataService, null, {
      saveBeforeDelete: true,
      saveOnlyDirtyRows: false,
    });
  }

  ngOnInit() {
    this._subscription.add(this.dataSource.watchAll(0, 100, null, null, null).subscribe());
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  save() {
    this.dataSource.save();
  }
}
