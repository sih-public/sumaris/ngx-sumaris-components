import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule, Routes } from '@angular/router';
import { GalleryTestPage } from './gallery.testing';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ImageGalleryModule } from '../image-gallery.module';
import { SharedDebugModule } from '../../../debug/debug.module';

const routes: Routes = [
  {
    path: 'gallery',
    pathMatch: 'full',
    component: GalleryTestPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ImageGalleryModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    SharedDebugModule,
  ],
  declarations: [
    // Components
    GalleryTestPage,
  ],
  exports: [
    RouterModule,

    // Components
    GalleryTestPage,
  ],
})
export class ImageGalleryTestingModule {}
