import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AppImageGalleryComponent } from './image-gallery.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { SharedPipesModule } from '../../pipes/pipes.module';
import { SharedMaterialModule } from '../../material/material.module';
import { SharedDirectivesModule } from '../../directives/directives.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RxStateModule } from '../../rx-state/rx-state.module';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    SharedMaterialModule,
    SharedPipesModule,
    SharedDirectivesModule,
    TranslateModule.forChild(),
    RxStateModule,
  ],
  declarations: [
    // Components
    AppImageGalleryComponent,
  ],
  exports: [
    IonicModule,
    TranslateModule,
    // Components
    AppImageGalleryComponent,
  ],
})
export class ImageGalleryModule {}
