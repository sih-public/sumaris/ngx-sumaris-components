import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ImageModule } from './image.module';
import { CommonModule } from '@angular/common';
import { ImageGalleryTestingModule } from './gallery/testing/gallery.testing.module';

@NgModule({
  imports: [CommonModule, ImageModule, ImageGalleryTestingModule],
  exports: [
    RouterModule,

    // Sub modules
    ImageGalleryTestingModule,
  ],
})
export class ImageTestingModule {}
