export interface Image {
  dataUrl: string;
  url: string;
  title?: string;
}
