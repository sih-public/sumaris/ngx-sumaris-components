import { Inject, Injectable, Optional } from '@angular/core';
import { ActionSheetController, PopoverController } from '@ionic/angular';

import { TranslateService } from '@ngx-translate/core';
import { StartableService } from '../services/startable-service.class';
import { ENVIRONMENT, Environment } from '../../../environments/environment.class';
import { PlatformService } from '../../core/services/platform.service';
import { LocalSettingsService } from '../../core/services/local-settings.service';
import { FilesUtils } from '../file/file.utils';
import { ImagesUtils } from '../file/images.utils';
import { isEmptyArray } from '../functions';

import { Image } from './image.model';
import { Capacitor } from '@capacitor/core';
import { Camera, CameraResultType } from '@capacitor/camera';

@Injectable({ providedIn: 'root' })
export class ImageService extends StartableService {
  private defaultMaxHeight = 1024;
  private defaultMaxWidth = 1024;

  constructor(
    protected platform: PlatformService,
    protected settings: LocalSettingsService,
    protected translate: TranslateService,
    protected popoverController: PopoverController,
    //              protected camera: Camera,
    protected actionSheetController: ActionSheetController,
    @Inject(ENVIRONMENT) @Optional() protected environment?: Environment
  ) {
    super(platform);
    this._debug = !environment.production;
    if (this._debug) console.debug(`[image] Creating service`);
  }

  protected async ngOnStart(): Promise<any> {
    try {
      const camera = Capacitor.isPluginAvailable('Camera');
      console.info(`[image] Starting... {camera: ${camera}}`);
    } catch (err) {
      console.error('Cannot check Camera plugin: ' + (err?.message || err));
    }
  }

  async add(event?: Event, opts?: { maxHeight?: number; mawWidth?: number; quality?: number }): Promise<Image[] | undefined> {
    if (!this.started) await this.ready();

    // If desktop: open upload popover
    if (!this.platform.isCordova()) {
      return this.addUploadedImages(event, opts);
    }

    console.info(`[image-service] Adding image...`);

    try {
      const data = await Camera.getPhoto({
        height: this.defaultMaxHeight,
        width: this.defaultMaxWidth,
        promptLabelCancel: this.translate.instant('COMMON.BTN_CANCEL'),
        promptLabelPicture: this.translate.instant('IMAGE.BTN_CAMERA_SOURCE'),
        resultType: CameraResultType.DataUrl,
        ...opts,
      });
      if (!data) return undefined;

      return [
        <Image>{
          dataUrl: data.dataUrl,
        },
      ];
    } catch (err) {
      console.info(`[image-service] Error while adding image: ${err?.message || err} -> ${err?.originalStack || ''}`);
      return undefined;
    }
  }

  protected async addUploadedImages(event?: Event, opts?: { maxHeight?: number; mawWidth?: number; quality?: number }): Promise<Image[] | undefined> {
    const { data } = await FilesUtils.showUploadPopover(this.popoverController, event, {
      uniqueFile: true,
      fileExtension: '.png, .jpg',
      instantUpload: true,
      uploadFn: (file) =>
        ImagesUtils.readAsDataURL(file, {
          maxWidth: this.defaultMaxWidth,
          maxHeight: this.defaultMaxHeight,
          ...opts,
        }),
    });

    if (isEmptyArray(data)) return; // No files uploaded: skip
    return (data || [])
      .map((file) => file.response.body) // Get response body = data as base64
      .map((dataUrl) => <Image>{ dataUrl });
  }
}
