import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageGalleryModule } from './gallery/image-gallery.module';

@NgModule({
  imports: [CommonModule, ImageGalleryModule],
  exports: [
    // Sub module
    ImageGalleryModule,
  ],
})
export class ImageModule {}
