import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './hotkeys-dialog.component.html',
  styleUrls: ['./hotkeys-dialog.component.scss'],
})
export class HotkeysDialogComponent {
  hotkeys: any[];

  constructor(@Inject(MAT_DIALOG_DATA) protected data: any) {
    this.hotkeys = Array.from(this.data);
  }
}
