import { Inject, inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { delay, Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { HotkeysDialogComponent } from './dialog/hotkeys-dialog.component';
import { isNotNilOrBlank } from '../functions';
import { ModalController, Platform, PopoverController } from '@ionic/angular';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { first } from 'rxjs/operators';
import { isMacOS } from '../platforms';

interface Options {
  element: any;
  elementName: string;
  description: string | undefined;
  keys: string;
  preventDefault: boolean;
}

export interface HotKeysConfig {
  /**
   * Shortcut to open the hotkeys help modal
   */
  openHelpModalShortcut: string;

  /**
   * Enable the hotkeys help modal
   */
  enableHelpModal: boolean;
}

export const APP_HOTKEYS_CONFIG = new InjectionToken<Partial<HotKeysConfig>>('HotKeysConfig');

@Injectable({
  providedIn: 'root',
})
export class Hotkeys {
  private _debug = false;
  private _id: number = 0;
  private _hotkeys = new Map<number, Options>();
  private _document = inject(DOCUMENT);
  private _defaults: Partial<Options> = {
    element: this._document,
    preventDefault: true,
  };
  private _dialogRef: MatDialogRef<any>;
  private _config: Partial<HotKeysConfig>;

  readonly defaultControlKey: string;
  readonly defaultControlKeyName: string;

  get isHelpModalOpened() {
    return !!this._dialogRef;
  }

  constructor(
    platform: Platform,
    private eventManager: EventManager,
    private dialog: MatDialog,
    private modalController: ModalController,
    private popoverController: PopoverController,
    @Optional() @Inject(APP_HOTKEYS_CONFIG) config?: Partial<HotKeysConfig>
  ) {
    this._config = {
      enableHelpModal: true,
      openHelpModalShortcut: 'F1',
      ...config,
    };

    // Add shortcut to open help modal
    if (this._config.enableHelpModal) {
      if (this._debug) console.debug(`[hotkeys] Starting hotkeys service... Press ${this._config.openHelpModalShortcut} to get help modal.`);

      this.addShortcut({ keys: this._config.openHelpModalShortcut }).subscribe(() => this.toggleHelpModal());
    }

    const isMac = isMacOS(window);
    this.defaultControlKey = isMac ? 'meta' : 'control';
    this.defaultControlKeyName = isMac ? '⌘' : 'Ctrl';

    // For DEV only
    //this._debug = !environment.production;
  }

  addShortcut(options: Partial<Options>): Observable<KeyboardEvent> {
    const merged = <Options>{ ...this._defaults, ...options };
    if (!merged.keys) throw new Error("Missing 'keys' or 'description' attribute");
    const event = `keydown.${merged.keys}`;
    const id = this._id++;

    this._hotkeys.set(id, merged);

    if (isNotNilOrBlank(merged.description)) {
      if (this._debug) console.debug(`[hotkeys] Add shortcut {${options.keys}}: ${merged.description}`);
    } else {
      if (this._debug) console.debug(`[hotkeys] Add shortcut {${options.keys}}`);
    }

    return new Observable((observer) => {
      const handler = async (e: KeyboardEvent) => {
        // Get top component from ModalController
        const top = (await this.modalController.getTop()) || (await this.popoverController.getTop());
        // If modal present, check its component name against hotkey element name (fix #181)
        if (top && top.component['name'] !== merged.elementName) {
          if (merged.preventDefault) e.preventDefault();
          return;
        }
        if (e instanceof KeyboardEvent && e.repeat) return; // skip when repeated
        if (merged.preventDefault) e.preventDefault();
        if (this._debug) console.debug(`[hotkeys] Shortcut {${options.keys}} detected...`);
        observer.next(e);
      };

      const dispose = this.eventManager.addEventListener(merged.element, event, handler);

      return () => {
        dispose();
        this._hotkeys.delete(id);
      };
    });
  }

  toggleHelpModal() {
    if (this.isHelpModalOpened) {
      this.closeHelpModal();
    } else {
      this.openHelpModal();
    }
  }

  openHelpModal() {
    if (this.isHelpModalOpened) return; // Skip if already opened

    // Get keys with a not blank description
    const hotkeys = Array.from(this._hotkeys.values()).reduce((map, item: Options) => {
      if (isNotNilOrBlank(item?.description)) {
        map.set(item.keys, item.description);
      }
      return map;
    }, new Map());

    if (hotkeys.size === 0) return; // Nothing to display

    this._dialogRef = this.dialog.open(HotkeysDialogComponent, {
      width: '500px',
      data: hotkeys,
    });
    this._dialogRef
      .afterClosed()
      .pipe(
        first(),
        // Avoid too many call (e.g. in Safari)
        delay(250)
      )
      .subscribe(() => {
        // Forget the dialog ref
        this._dialogRef = null;
        console.debug('[hotkeys] Closing help modal');
      });
  }

  closeHelpModal() {
    this._dialogRef?.close(true);
  }
}
