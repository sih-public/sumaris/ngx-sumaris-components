import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

export class Alerts {
  /**
   * Ask the user to save before leaving. If return undefined: user has cancelled
   * <p>
   *   from a routed page, prefer using ComponentDirtyGuard
   *
   * @param alertCtrl
   * @param translate
   * @param event
   */
  static async askSaveBeforeLeave(alertCtrl: AlertController, translate: TranslateService, event?: Event): Promise<boolean | undefined> {
    let confirm = false;
    let cancel = false;
    const translations = translate.instant([
      'COMMON.BTN_SAVE',
      'COMMON.BTN_CANCEL',
      'COMMON.BTN_ABORT_CHANGES',
      'CONFIRM.SAVE',
      'CONFIRM.ALERT_HEADER',
    ]);
    const alert = await alertCtrl.create({
      header: translations['CONFIRM.ALERT_HEADER'],
      message: translations['CONFIRM.SAVE'],
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            cancel = true;
          },
        },
        {
          text: translations['COMMON.BTN_ABORT_CHANGES'],
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: translations['COMMON.BTN_SAVE'],
          handler: () => {
            confirm = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    if (confirm) return true; // = Do save and leave

    if (cancel) {
      // Stop the event
      if (event) event.preventDefault();

      return undefined; // User cancelled
    }

    return false; // Leave without saving
  }

  /**
   * Ask the user to conform an action. If return undefined: user has cancelled
   *
   * @param alertCtrl
   * @param translate
   * @param immediate is action has an immediate effect ?
   * @param event
   * @param interpolateParams
   */
  static async askActionConfirmation(
    alertCtrl: AlertController,
    translate: TranslateService,
    immediate?: boolean,
    event?: Event,
    interpolateParams?: any
  ): Promise<boolean | undefined> {
    const messageKey = immediate === true ? 'CONFIRM.ACTION_IMMEDIATE' : 'CONFIRM.ACTION';
    return Alerts.askConfirmation(messageKey, alertCtrl, translate, event, interpolateParams);
  }

  /**
   * Ask the user to confirm. If return undefined: user has cancelled
   *
   * @pram messageKey i18n message key
   * @param messageKey
   * @param alertCtrl
   * @param translate
   * @param event
   * @param interpolateParams
   */
  static async askConfirmation(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: Event,
    interpolateParams?: any
  ): Promise<boolean | undefined> {
    if (!alertCtrl || !translate) throw new Error("Missing required argument 'alertCtrl' or 'translate'");
    let confirm = false;
    let cancel = false;
    const translations = translate.instant(['COMMON.BTN_YES_CONTINUE', 'COMMON.BTN_CANCEL', messageKey, 'CONFIRM.ALERT_HEADER'], interpolateParams);
    const alert = await alertCtrl.create({
      header: translations['CONFIRM.ALERT_HEADER'],
      message: translations[messageKey],
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            cancel = true;
          },
        },
        {
          text: translations['COMMON.BTN_YES_CONTINUE'],
          handler: () => {
            confirm = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    if (confirm) return true; // = Do save and leave

    if (cancel) {
      // Stop the event
      if (event) event.preventDefault();

      return undefined; // User cancelled
    }

    return false; // Leave without saving
  }

  /**
   * Ask the user to save before leaving. If return undefined: user has cancelled
   *
   * @param alertCtrl
   * @param translate
   * @param event
   * @param interpolateParams
   */
  static async askDeleteConfirmation(
    alertCtrl: AlertController,
    translate: TranslateService,
    event?: Event,
    interpolateParams?: any
  ): Promise<boolean | undefined> {
    let confirm = false;
    let cancel = false;
    const translations = translate.instant(
      ['COMMON.BTN_YES_DELETE', 'COMMON.BTN_CANCEL', 'CONFIRM.DELETE', 'CONFIRM.ALERT_HEADER'],
      interpolateParams
    );
    const alert = await alertCtrl.create({
      header: translations['CONFIRM.ALERT_HEADER'],
      message: translations['CONFIRM.DELETE'],
      buttons: [
        {
          text: translations['COMMON.BTN_CANCEL'],
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            cancel = true;
          },
        },
        {
          text: translations['COMMON.BTN_YES_DELETE'],
          handler: () => {
            confirm = true; // update upper value
          },
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();

    if (confirm) return true; // = Do save and leave

    if (cancel) {
      // Stop the event
      if (event) event.preventDefault();

      return undefined; // User cancelled
    }

    return false; // Leave without saving
  }

  static async askSaveBeforeAction(
    alertCtrl: AlertController,
    translate: TranslateService,
    opts?: { valid?: boolean; validMessageI18n?: string; invalidMessageI18n?: string },
    interpolateParams?: any
  ): Promise<{ confirmed: boolean; save: boolean } | undefined> {
    let save = false;
    let confirmed = false;
    let alert: HTMLIonAlertElement;
    const validMessageI18n = opts?.validMessageI18n || 'CONFIRM.SAVE_BEFORE_CONTINUE';
    const invalidMessageI18n = opts?.invalidMessageI18n || 'CONFIRM.CANCEL_CHANGES';

    // Ask user before save
    if (opts?.valid) {
      const translations = translate.instant(
        ['COMMON.BTN_CANCEL', 'COMMON.BTN_SAVE', 'COMMON.BTN_ABORT_CHANGES', 'CONFIRM.ALERT_HEADER', validMessageI18n],
        interpolateParams
      );
      alert = await alertCtrl.create({
        header: translations['CONFIRM.ALERT_HEADER'],
        message: translations[validMessageI18n],
        buttons: [
          {
            text: translations['COMMON.BTN_CANCEL'],
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {},
          },
          {
            text: translations['COMMON.BTN_ABORT_CHANGES'],
            cssClass: 'secondary',
            handler: () => {
              confirmed = true;
            },
          },
          {
            text: translations['COMMON.BTN_SAVE'],
            handler: () => {
              save = true;
              confirmed = true;
            },
          },
        ],
      });
    } else {
      const translations = translate.instant(
        ['COMMON.BTN_ABORT_CHANGES', 'COMMON.BTN_CANCEL', 'CONFIRM.ALERT_HEADER', invalidMessageI18n],
        interpolateParams
      );

      alert = await alertCtrl.create({
        header: translations['CONFIRM.ALERT_HEADER'],
        message: translations[invalidMessageI18n],
        buttons: [
          {
            text: translations['COMMON.BTN_ABORT_CHANGES'],
            cssClass: 'secondary',
            handler: () => {
              confirmed = true;
            },
          },
          {
            text: translations['COMMON.BTN_CANCEL'],
            role: 'cancel',
            handler: () => {},
          },
        ],
      });
    }

    await alert.present();
    await alert.onDidDismiss();

    return { confirmed, save };
  }

  static async showError(
    messageKey: string,
    alertCtrl: AlertController,
    translate: TranslateService,
    opts?: {
      titleKey?: string;
    },
    interpolateParams?: any
  ) {
    if (!messageKey || !alertCtrl || !translate) throw new Error("Missing a required argument ('messageKey', 'alertCtrl' or 'translate')");
    const titleKey = (opts && opts.titleKey) || 'ERROR.ALERT_HEADER';
    const translations = translate.instant(['COMMON.BTN_OK', messageKey, titleKey], interpolateParams);
    const alert = await alertCtrl.create({
      header: translations[titleKey],
      message: translations[messageKey],
      buttons: [
        {
          text: translations['COMMON.BTN_OK'],
          role: 'cancel',
        },
      ],
    });
    await alert.present();
    await alert.onDidDismiss();
  }
}
