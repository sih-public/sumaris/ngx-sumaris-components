export const DATE_ISO_PATTERN = 'YYYY-MM-DDTHH:mm:ss.SSSZ';
export const DATE_PATTERN = 'YYYY-MM-DD';
export const DATE_MATCH_REGEXP = new RegExp(/^\d+[-]\d+[-]\d+$/g);
export const DEFAULT_PLACEHOLDER_CHAR = '\u005F'; // underscore
export const PUBKEY_REGEXP = /^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$/;
export const SPACE_PLACEHOLDER_CHAR = '\u2000';
export const SPACE_PLACEHOLDER_CHAR_REGEXP_GLOBAL = /\u2000/g;

// See https://unicode-search.net/unicode-namesearch.pl?term=ZERO
export const EMPTY_PLACEHOLDER_CHAR = '\uFEFF'; // ZERO WIDTH NO-BREAK SPACE
export const EMPTY_PLACEHOLDER_CHAR_REGEXP_GLOBAL = /\uFEFF/g; // ZERO WIDTH NO-BREAK SPACE

export const PLUS_PLACEHOLDER_CHAR_REGEXP_GLOBAL = /[+]/g;

export const KEYBOARD_HIDE_DELAY_MS = 250; // 1s

// Separator to join properties (see `joinPropertiesPath()` or `referentialToString()` )
export const DEFAULT_JOIN_PROPERTIES_SEPARATOR: string = ' - ';
export const DEFAULT_JOIN_ARRAY_VALUES_SEPARATOR: string = ', ';
