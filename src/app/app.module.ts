import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, provideExperimentalZonelessChangeDetection } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { HTTP_INTERCEPTORS, HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { APP_LOCAL_SETTINGS, APP_LOCAL_SETTINGS_OPTIONS, LocalSettingsOptions } from './core/services/local-settings.service';
import { APP_LOCALES, LocalSettings } from './core/services/model/settings.model';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_CONFIG_OPTIONS } from './core/services/config.service';
import { APP_HOME_BUTTONS } from './core/home/home';
import { CORE_CONFIG_OPTIONS } from './core/services/config/core.config';
import { IonicModule } from '@ionic/angular';
import { CacheModule } from 'ionic-cache';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule } from './shared/shared.module';
import { APP_LOCAL_STORAGE_TYPE_POLICIES } from './core/services/storage/entities-storage.service';
import { AppGestureConfig } from './shared/gesture/gesture-config';
import { APP_GRAPHQL_TYPE_POLICIES } from './core/graphql/graphql.service';
import { DATE_ISO_PATTERN } from './shared/constants';
import { JDENTICON_CONFIG } from 'ngx-jdenticon';
import { APP_ABOUT_DEVELOPERS, APP_ABOUT_PARTNERS } from './core/about/about.modal';
import { Department } from './core/services/model/department.model';
import { CORE_TESTING_PAGES } from './core/core.testing.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { environment } from '../environments/environment';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SHARED_TESTING_PAGES } from './shared/shared.testing.module';
import { APP_TESTING_PAGES } from './shared/testing/tests.page';
import { SOCIAL_TESTING_PAGES } from './social/social.testing.module';
import { APP_MENU_OPTIONS } from './core/menu/menu.service';
import { IMenuItem, MenuOptions } from './core/menu/menu.model';
import { APP_USER_EVENT_SERVICE } from './social/user-event/user-event.service';
import { UserEventTestService } from './social/user-event/testing/user-event.testing.service';
import { UserEventModule } from './social/user-event/user-event.module';
import { JobModule } from './social/job/job.module';
import { APP_JOB_PROGRESSION_SERVICE } from './social/job/progression/job-progression.service';
import { JobProgressionTestService } from './social/job/testing/job-progression.testing.service';
import { IonicStorageModule } from '@ionic/storage-angular';
import { ApolloModule } from 'apollo-angular';
import { AudioManagement } from '@ionic-native/audio-management/ngx';
import { StorageService } from './shared/storage/storage.service';
import { APP_STORAGE } from './shared/storage/storage.utils';
import { Downloader } from '@ionic-native/downloader/ngx';
import { APP_PROGRESS_BAR_SERVICE, ProgressBarService } from './shared/services/progress-bar.service';
import { ProgressInterceptor } from './shared/interceptors/progess.interceptor';
import { APP_USER_SETTINGS_OPTIONS, APP_USER_TOKEN_SCOPES, UserSettingsOptions } from './core/services/account.service';
import { TokenScope } from './core/services/model/token.model';
import { APP_SETTINGS_MENU_ITEMS } from './core/settings/settings.page';
import { APP_LOGGING_SERVICE, LoggingService } from './shared/logging/logging-service.class';
import { isAndroid, isCapacitor, isIOS, isMobile } from './shared/platforms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MAT_TABS_CONFIG, MatTabsConfig } from '@angular/material/tabs';
import { EnvironmentHttpLoader, EnvironmentLoader } from '../environments/environment.loader';
import { APP_NAMED_FILTER_SERVICE } from './shared/named-filter/named-filter.service';
import { DummyNamedFilterService } from './shared/named-filter/testing/named-filter.testing.service';
import { APP_HOTKEYS_CONFIG, HotKeysConfig } from './shared/hotkeys/hotkeys.service';
import { TEST_USER_SETTINGS_OPTIONS } from './core/services/testing/user-settings.config';
import { SharedMarkdownModule } from './shared/markdown/markdown.module';

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApolloModule,
    IonicModule.forRoot({
      innerHTMLTemplatesEnabled: true,
      platform: {
        mobile: isMobile,
        capacitor: isCapacitor,
        android: isAndroid,
        ios: isIOS,
      },
    }),
    CacheModule.forRoot({
      keyPrefix: '', // For compatibility
      ...environment.cache,
    }),
    IonicStorageModule.forRoot({
      name: 'ngx-sumaris-components', // default
      ...environment.storage,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (httpClient: HttpClient) => new TranslateHttpLoader(httpClient, './assets/i18n/', `.json`),
        deps: [HttpClient],
      },
    }),
    SharedMarkdownModule.forRoot(),
    // Need for tap event, in app-toolbar
    HammerModule,
    // functional modules
    SharedModule.forRoot({
      loader: {
        provide: EnvironmentLoader,
        useFactory: (httpClient: HttpClient) => new EnvironmentHttpLoader(httpClient, environment),
        deps: [HttpClient],
      },
    }),
    CoreModule.forRoot(),
    AppRoutingModule,
    UserEventModule,
    JobModule,
  ],
  providers: [
    // Cordova plugins
    AudioManagement,
    Downloader,

    // Angular material config
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: <MatFormFieldDefaultOptions>{
        appearance: 'fill',
        // TODO : Test if it suitable
        // appearance: 'outline',
        //subscriptSizing: 'dynamic',
      },
    },
    {
      provide: MAT_TABS_CONFIG,
      useValue: <MatTabsConfig>{
        stretchTabs: false,
      },
    },
    // FIXME - Workaround find at : https://github.com/angular/components/issues/26000#issuecomment-1563107933
    // {
    //   provide: MAT_SELECT_CONFIG,
    //   useValue: <MatSelectConfig>{
    //     overlayPanelClass: 'mat-select-panel-fit-content',
    //   },
    // },
    //{provide: APP_BASE_HREF, useValue: (environment.baseUrl || '/')},
    {
      provide: APP_BASE_HREF,
      useFactory: () => {
        try {
          return document.getElementsByTagName('base')[0].href;
        } catch (err) {
          console.error(err);
          return environment.baseUrl || '/';
        }
      },
    },
    { provide: APP_STORAGE, useExisting: StorageService },
    //{ provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: APP_PROGRESS_BAR_SERVICE, useClass: ProgressBarService },
    { provide: HTTP_INTERCEPTORS, useClass: ProgressInterceptor, multi: true, deps: [APP_PROGRESS_BAR_SERVICE] },
    {
      provide: APP_LOCALES,
      useValue: [
        {
          key: 'fr',
          value: 'Français',
          country: 'fr',
        },
        {
          key: 'en',
          value: 'English (UK)',
          country: 'gb',
        },
        {
          key: 'en-US',
          value: 'English (US)',
          country: 'us',
        },
      ],
    },
    { provide: MAT_DATE_LOCALE, useValue: environment.defaultLocale || 'en' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: DATE_ISO_PATTERN,
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
      useValue: { strict: false },
    },
    { provide: MomentDateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS] },
    { provide: DateAdapter, useExisting: MomentDateAdapter },
    // Configure hammer gesture
    { provide: HAMMER_GESTURE_CONFIG, useClass: AppGestureConfig },
    // Settings default values
    {
      provide: APP_LOCAL_SETTINGS,
      useValue: <Partial<LocalSettings>>{
        pageHistoryMaxSize: 3,
      },
    },
    // Local setting options
    {
      provide: APP_LOCAL_SETTINGS_OPTIONS,
      useValue: <Partial<LocalSettingsOptions>>{
        serializeAsString: true,
        options: {
          // Dynamic options are now inserted by app.component
          //...TEST_LOCAL_SETTINGS_OPTIONS,
        }, // options definition
      },
    },
    // User options (comment out to enable user properties in AccountPage)
    {
      provide: APP_USER_SETTINGS_OPTIONS,
      useValue: <UserSettingsOptions>{
        options: {
          ...TEST_USER_SETTINGS_OPTIONS,
        },
      },
    },
    // Token Scopes
    {
      provide: APP_USER_TOKEN_SCOPES,
      useValue: <TokenScope[]>[
        {
          id: 'read_api',
          flag: 1 << 0,
          name: 'ACCOUNT.TOKENS.SCOPES.READ',
        },
        {
          id: 'write_api',
          flag: 1 << 1,
          name: 'ACCOUNT.TOKENS.SCOPES.WRITE',
        },
        {
          id: 'admin_api',
          flag: 1 << 2,
          name: 'ACCOUNT.TOKENS.SCOPES.ADMIN',
        },
      ],
    },
    // Config options definition (Core + trip)
    { provide: APP_CONFIG_OPTIONS, useValue: CORE_CONFIG_OPTIONS },
    // Menu options
    {
      provide: APP_MENU_OPTIONS,
      useValue: <MenuOptions>{
        enableSubMenus: true,
        enableSubMenuIcon: true,
      },
    },
    // Home buttons
    { provide: APP_HOME_BUTTONS, useValue: [] },
    // Settings menu options
    {
      provide: APP_SETTINGS_MENU_ITEMS,
      useValue: <IMenuItem[]>[
        { title: 'MENU.TESTING', path: '/testing', icon: 'code', color: 'danger', ifProperty: 'sumaris.testing.enable', profile: 'SUPERVISOR' },
      ],
    },
    // Entities Apollo cache options
    { provide: APP_GRAPHQL_TYPE_POLICIES, useValue: {} },
    // Entities storage options
    { provide: APP_LOCAL_STORAGE_TYPE_POLICIES, useValue: {} },
    // Logging service
    { provide: APP_LOGGING_SERVICE, useClass: LoggingService },
    // User event service (for test only - app defined their own service)
    { provide: APP_USER_EVENT_SERVICE, useClass: UserEventTestService },
    // Job progression service (test)
    { provide: APP_JOB_PROGRESSION_SERVICE, useClass: JobProgressionTestService },
    // About developers
    {
      provide: APP_ABOUT_DEVELOPERS,
      useValue: <Partial<Department>[]>[
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png', label: 'Environmental Information Systems' },
      ],
    },
    // About partners
    {
      provide: APP_ABOUT_PARTNERS,
      useValue: <Partial<Department>[]>[
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
        { siteUrl: 'https://www.e-is.pro', logo: 'assets/img/logo/logo-eis_50px.png' },
        { siteUrl: 'https://www.sumaris.net', logo: 'assets/img/logo/logo-sumaris.png' },
        { siteUrl: 'https://www.sumaris.net', logo: 'assets/img/logo/logo-sumaris.png' },
        { siteUrl: 'https://www.sumaris.net', logo: 'assets/img/logo/logo-sumaris.png' },
        { siteUrl: 'https://www.sumaris.net', logo: 'assets/img/logo/logo-sumaris.png' },
        { siteUrl: 'https://www.sumaris.net', logo: 'assets/img/logo/logo-sumaris.png' },
      ],
    },
    // Testing pages
    {
      provide: APP_TESTING_PAGES,
      useValue: [...SHARED_TESTING_PAGES, ...CORE_TESTING_PAGES, ...SOCIAL_TESTING_PAGES],
    },
    // Custom identicon style
    // https://jdenticon.com/icon-designer.html?config=4451860010ff320028501e5a
    {
      provide: JDENTICON_CONFIG,
      useValue: {
        lightness: {
          color: [0.26, 0.8],
          grayscale: [0.3, 0.9],
        },
        saturation: {
          color: 0.5,
          grayscale: 0.46,
        },
        backColor: '#0000',
      },
    },
    {
      provide: APP_NAMED_FILTER_SERVICE,
      useClass: DummyNamedFilterService,
    },
    {
      provide: APP_HOTKEYS_CONFIG,
      useValue: <HotKeysConfig>{
        openHelpModalShortcut: 'F1',
        enableHelpModal: true,
      },
    },

    // Http client
    provideHttpClient(withInterceptorsFromDi()),

    // Zone less (experimental)
    provideExperimentalZonelessChangeDetection(),

    // Event replay
    // TODO enable when testing the SSR - Server Side Rendering)
    //provideClientHydration(withEventReplay()),
  ],
})
export class AppModule {
  constructor() {
    console.debug('[app] Creating module');
  }
}
