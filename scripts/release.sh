#!/bin/bash
# Get to the root project
if [[ "_" == "_${PROJECT_DIR}" ]]; then
  SCRIPT_DIR=$(dirname "$0")
  PROJECT_DIR=$(cd "${SCRIPT_DIR}/.." && pwd)
  export PROJECT_DIR
fi;

cd "${PROJECT_DIR}" || exit 1

# Preparing environment
# TODO check if env-global can be run with JDK17
#. "${PROJECT_DIR}"/scripts/env-global.sh
#[[ $? -ne 0 ]] && exit 1

# Read parameters
version=$1
release_description=$2


# Check parameters
if [[ ! $version =~ ^[0-9]+.[0-9]+.[0-9]+(-(alpha|beta|rc)[0-9]+)?$ ]]; then
  echo "Wrong version format"
  echo "Usage:"
  echo " > $0 <version> <release_description>"
  echo "with:"
  echo " - version: x.y.z"
  echo " - release_description: a optional comment on release"
  exit 1
fi

if [[ "_$release_description" == "_" ]]; then
  release_description="Release $version"
fi

# Control if there is no uncommitted files
if [[ -n $(git status -s) ]]
then
  echo "Please commit changes before running release"
#  exit 1
fi

# Control that the script is run on `dev` branch
branch_origin="develop"
if [[ "$version" =~ ^2\.6(\.[0-9]+)?(-(alpha|beta|rc)[0-9]+)?$ ]]; then
    branch_origin="v2.6.x"
fi
if [[ "$version" =~ ^2\.9(\.[0-9]+)?(-(alpha|beta|rc)[0-9]+)?$ ]]; then
    branch_origin="v2.9.x"
fi
resumeRelease=0
branch=$(git rev-parse --abbrev-ref HEAD)
if [[ ! "$branch" == "$branch_origin" ]]; then
  if [[ "$branch" == "release/$version" ]]; then
    echo "Resuming release ..."
    resumeRelease=1
  else
    echo ">> This script must be run under \`$branch_origin\` or \`release/$version\` branch (actual branch: $branch)"
    exit 1
  fi
fi

### Get current version (package.json)
package=$(node -p "require('./package.json').name")
if [[ "$(uname)" == "Darwin" ]]; then
  current=$(awk -F'"' '/"version": "/ {print $4}' package.json | head -n 1)
else
  current=$(grep -oP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)[0-9]+)?" package.json | grep -m 1 -oP "\d+.\d+.\d+(-(alpha|beta|rc)[0-9]+)?")
fi
if [[ "_$current" == "_" ]]; then
  echo ">> Unable to read the current version in 'package.json'. Please check version format is: x.y.z (x and y should be an integer)."
  exit 1;
fi

echo "----------------------------------"
if [[ $resumeRelease = 0 ]]
then
  echo "- Starting release..."
else
  echo "- Resuming release..."
fi
echo "----------------------------------"
if [[ $resumeRelease = 0 ]]
then
  echo "- Current version: $current"
fi
echo "- Release version: $version"
echo "----------------------------------"

# Check version already published
if [[ "$(npm view "$package"@"$version" version 2>/dev/null)" == "$version" ]]; then
  echo "Version $version already published"
  exit 1
fi

if [[ $resumeRelease == 0 ]]; then
  read -r -p "Is these new versions correct ? [y/N] " response
  [[ ! "$response" =~ ^(yes|y|Y)$ ]] && exit 1
  # Create release branch
  git checkout -b release/"$version"
  [[ $? -ne 0 ]] && exit 1
fi

if [[ "$(uname)" == "Darwin" ]]; then
  # Changer la version dans 'package.json' et 'config.xml'
  sed -i '' "s/version\": \"$current\"/version\": \"$version\"/g" package.json

  # Récupérer la version actuelle depuis 'src/assets/manifest.json'
  currentManifestJsonVersion=$(awk -F'"' '/"version": "/ {print $4}' src/assets/manifest.json | head -n 1)

  # Changer la version dans 'src/assets/manifest.json'
  sed -i '' "s/version\": \"$currentManifestJsonVersion\"/version\": \"$version\"/g" src/assets/manifest.json
else
  # Change the version in files: 'package.json' and 'config.xml'
  sed -i "s/version\": \"$current\"/version\": \"$version\"/g" package.json
  # Change version in file: 'src/assets/manifest.json'
  currentManifestJsonVersion=$(grep -oP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?\"" src/assets/manifest.json | grep -oP "\d+.\d+.\d+(-(alpha|beta|rc)[0-9]*)?")
  sed -i "s/version\": \"$currentManifestJsonVersion\"/version\": \"$version\"/g" src/assets/manifest.json
fi

echo "----------------------------------"
echo "- Build..."
echo "----------------------------------"
npm run packagr
[[ $? -ne 0 ]] && exit 1

echo "----------------------------------"
echo "- Publish..."
echo "----------------------------------"
cd dist || exit 1
npm publish
[[ $? -ne 0 ]] && exit 1

echo "----------------------------------"
echo "- Finish release..."
echo "----------------------------------"

# Git process for release (ISO gitflow)
cd..
git commit -a -m "Prepare release $version"
# -- Merge to master
if [[ "$branch_origin" == "develop" ]]; then
  git checkout master
  git reset --hard origin/master
  git merge --no-ff --no-edit -m "Release $version" "release/$version"
  git tag -a "$version" -m "$release_description"
  git push origin master
fi

# -- Merge to the origin branch
git checkout $branch_origin
git merge --no-ff --no-edit -m "[skip ci] Release $version" "release/$version"
if [[ "$branch_origin" == "develop" ]]; then
  git tag -a "$version" -m "$release_description"
fi
git push origin $branch_origin
git push --tags
git branch -D "release/$version"

echo "----------------------------------"
echo "- Release OK"
echo "----------------------------------"
