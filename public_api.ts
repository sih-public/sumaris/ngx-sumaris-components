// Environment

import { MatLatLongFieldInput } from './src/app/shared/material/latlong/material.latlong-input';

export * from './src/environments/environment.class';
export * from './src/environments/environment.loader';

// Shared
export * from './src/app/shared/constants';
export * from './src/app/shared/shared.module';
export { SharedRoutingModule } from './src/app/shared/shared-routing.module';
export { RxStateModule } from './src/app/shared/rx-state/rx-state.module';
export * from './src/app/shared/rx-state/rx-state.decorators';

// Shared material
export * from './src/app/shared/material/material.module';
export * from './src/app/shared/material/material.config';
export * from './src/app/shared/material/material.animations';
export * from './src/app/shared/material/boolean/boolean.module';
export { MatBooleanField } from './src/app/shared/material/boolean/material.boolean';
export * from './src/app/shared/material/autocomplete/material.autocomplete.module';
export * from './src/app/shared/material/autocomplete/material.autocomplete';
export * from './src/app/shared/material/autocomplete/material.autocomplete.config';
export * from './src/app/shared/material/autocomplete/material.autocomplete.utils';
export * from './src/app/shared/material/chips/chips.module';
export { MatChipsField } from './src/app/shared/material/chips/material.chips';
export * from './src/app/shared/material/paginator/material.paginator-i18n';
export * from './src/app/shared/material/stepper/material.stepper-i18n';
export * from './src/app/shared/material/datetime/datetime.module';
export { MatDateShort } from './src/app/shared/material/datetime/material.dateshort';
export { MatDateTime } from './src/app/shared/material/datetime/material.datetime';
export { MatDate } from './src/app/shared/material/datetime/material.date';
export { MatLatLongField } from './src/app/shared/material/latlong/material.latlong';
export { MatLatLongFieldInput } from './src/app/shared/material/latlong/material.latlong-input';
export * from './src/app/shared/material/latlong/material.latlong.module';
export * from './src/app/shared/material/duration/duration.module';
export { MatDuration } from './src/app/shared/material/duration/material.duration';
// export * from './src/app/shared/material/numpad/numpad.module';
// export * from './src/app/shared/material/numpad/numpad.directive';
// export * from './src/app/shared/material/numpad/numpad.append-to-input.directive';
// export * from './src/app/shared/material/numpad/numpad.container';
// export * from './src/app/shared/material/numpad/numpad.content';
// export * from './src/app/shared/material/numpad/numpad.component';
export * from './src/app/shared/material/swipe/swipe.module';
export * from './src/app/shared/material/swipe/material.swipe';
export * from './src/app/shared/material/badge/badge.module';
export * from './src/app/shared/material/badge/badge.directive';
export * from './src/app/shared/material/text/text-form.component';
export * from './src/app/shared/material/text/text-form.module';

// Shared components
export * from './src/app/shared/inputs';
export { AppFormField } from './src/app/shared/form/field.component';
export * from './src/app/shared/form/loading-spinner';
export * from './src/app/shared/form/field.model';
export * from './src/app/shared/toolbar/toolbar';
export * from './src/app/shared/toolbar/modal-toolbar';
export * from './src/app/shared/toolbar/toolbar.module';
export * from './src/app/shared/interceptors/progess.interceptor';
export * from './src/app/shared/toast/toasts';
export * from './src/app/shared/guard/component-dirty.guard';
export * from './src/app/shared/upload-file/upload-file.model';
export * from './src/app/shared/upload-file/upload-file-popover.component';
export * from './src/app/shared/upload-file/upload-file.component';
export * from './src/app/shared/image/gallery/image-gallery.component';
export * from './src/app/shared/image/gallery/image-gallery.module';
export * from './src/app/shared/image/image.model';
export * from './src/app/shared/image/image.module';
export * from './src/app/shared/storage/storage-explorer.component';
export * from './src/app/shared/storage/storage-explorer.module';
export * from './src/app/shared/logging/log-level.model';
export * from './src/app/shared/logging/logger.model';
export * from './src/app/shared/logging/logging-service.class';
export * from './src/app/shared/logging/logging-service.config';
export * from './src/app/shared/logging/logging-service.module';

// Shared directives
export * from './src/app/shared/directives/directives.module';
export * from './src/app/shared/directives/autofocus.directive';
export * from './src/app/shared/directives/ng-var.directive';
export * from './src/app/shared/directives/drag-and-drop.directive';
export * from './src/app/shared/directives/autotitle.directive';
export * from './src/app/shared/directives/throttled-click.directive';
export * from './src/app/shared/directives/resizable/resizable.directive';
export * from './src/app/shared/directives/resizable/resizable.component';
export * from './src/app/shared/directives/resizable/resizable.module';
export * from './src/app/shared/directives/autoresize.directive';

// Shared pipes
export * from './src/app/shared/pipes/pipes.module';
export * from './src/app/shared/pipes/date-format.pipe';
export * from './src/app/shared/pipes/date-from-now.pipe';
export * from './src/app/shared/pipes/date-diff-duration.pipe';
export * from './src/app/shared/pipes/dates.pipe';
export * from './src/app/shared/pipes/latlong-format.pipe';
export * from './src/app/shared/pipes/highlight.pipe';
export * from './src/app/shared/pipes/number-format.pipe';
export * from './src/app/shared/pipes/duration.pipe';
export * from './src/app/shared/pipes/file-size.pipe';
export * from './src/app/shared/pipes/property.pipes';
export * from './src/app/shared/pipes/math.pipes';
export * from './src/app/shared/pipes/arrays.pipe';
export * from './src/app/shared/pipes/maps.pipe';
export * from './src/app/shared/pipes/string.pipes';
export * from './src/app/shared/pipes/observable.pipes';
export * from './src/app/shared/pipes/translate-context.pipe';
export * from './src/app/shared/pipes/ng-init.pipe';
export * from './src/app/shared/pipes/form.pipes';
export * from './src/app/shared/pipes/colors.pipe';
export * from './src/app/shared/pipes/types.pipes';
export * from './src/app/shared/pipes/maskito.pipe';
export * from './src/app/shared/pipes/selection.pipes';
export * from './src/app/shared/pipes/badge.pipes';
export * from './src/app/shared/pipes/html.pipes';
export * from './src/app/shared/pipes/display-with.pipe';

// Shared services
export * from './src/app/shared/services';
export * from './src/app/shared/audio/audio';
export * from './src/app/shared/file/file.service';
export * from './src/app/shared/image/image.service';
export * from './src/app/shared/services/startable-service.class';
export * from './src/app/shared/services/entity-service.class';
export * from './src/app/shared/services/memory-entity-service.class';
export * from './src/app/shared/services/progress-bar.service';
export * from './src/app/shared/services/translate-context.service';
export * from './src/app/shared/services/job.utils';
export * from './src/app/shared/storage/storage.service';
export * from './src/app/shared/storage/storage.utils';
export * from './src/app/shared/storage/storage-explorer.component';
export * from './src/app/shared/storage/storage-explorer.module';
export * from './src/app/shared/validator/form-error-adapter.class';
export * from './src/app/shared/debug/debug-service.class';

// Shared other
export * from './src/app/shared/types';
export * from './src/app/shared/dates';
export * from './src/app/shared/functions';
export * from './src/app/shared/regexps';
export * from './src/app/shared/services';
export * from './src/app/shared/forms';
export * from './src/app/shared/observables';
export * from './src/app/shared/events';
export * from './src/app/shared/alerts';
export * from './src/app/shared/platforms';
export * from './src/app/shared/geolocation/geolocation.utils';
export * from './src/app/shared/version/versions';
export * from './src/app/shared/file/file.utils';
export * from './src/app/shared/file/csv.utils';
export * from './src/app/shared/file/images.utils';
export * from './src/app/shared/file/json.utils';
export * from './src/app/shared/file/uri.utils';
export * from './src/app/shared/file/url.utils';
export * from './src/app/shared/hotkeys/shared-hotkeys.module';
export * from './src/app/shared/hotkeys/hotkeys.service';
export * from './src/app/shared/hotkeys/dialog/hotkeys-dialog.component';
export {
  Color,
  ColorName,
  ColorScale,
  ColorScaleLegend,
  ColorScaleOptions,
  ColorGradientOptions,
  ColorScaleLegendItem,
} from './src/app/shared/graph/graph-colors';
export * from './src/app/shared/graph/colors.utils';
export * from './src/app/shared/gesture/gesture-config';
export * from './src/app/shared/gesture/hammer.utils';
export * from './src/app/shared/validator/validators';
export * from './src/app/shared/validator/form-error-adapter.class';
export * from './src/app/shared/material/latlong/latlong.utils';
export * from './src/app/shared/debug/debug.module';
export * from './src/app/shared/debug/debug.component';
export * from './src/app/shared/named-filter/named-filter.module';
export * from './src/app/shared/named-filter/named-filter.model';
export * from './src/app/shared/named-filter/named-filter.service';
export * from './src/app/shared/named-filter/named-filter-selector.component';
export * from './src/app/shared/markdown/markdown.module';
export * from './src/app/shared/markdown/markdown.utils';
export * from './src/app/shared/markdown/markdown.service';
export * from './src/app/shared/markdown/markdown.directive';
export * from './src/app/shared/markdown/markdown.component';
export * from './src/app/shared/markdown/markdown.modal';
export * from './src/app/shared/print/print.service';

// Core
export * from './src/app/core/core.module';

// Core model
export * from './src/app/core/services/model/account.model';
export * from './src/app/core/services/model/token.model';
export * from './src/app/core/services/model/config.model';
export * from './src/app/core/services/model/department.model';
export * from './src/app/core/services/model/entity.model';
export * from './src/app/core/services/model/tree-item-entity.model';
export * from './src/app/core/services/model/filter.model';
export * from './src/app/core/services/model/history.model';
export * from './src/app/core/services/model/model.enum';
export * from './src/app/core/services/model/peer.model';
export * from './src/app/core/services/model/person.model';
export {
  IReferentialRef,
  ReferentialRef,
  BaseReferential,
  Referential,
  ReferentialUtils,
  ReferentialAsObjectOptions,
  referentialToString,
  referentialsToString,
  IStatus,
  StatusList,
  StatusById,
  MINIFY_ENTITY_FOR_LOCAL_STORAGE,
  MINIFY_ENTITY_FOR_POD,
} from './src/app/core/services/model/referential.model';
export * from './src/app/core/services/model/settings.model';

// GraphQL
export * from './src/app/core/graphql/graphql.module';
export * from './src/app/core/graphql/graphql.service';

// Core pipes
export * from './src/app/core/services/pipes/account.pipes';
export * from './src/app/core/services/pipes/department-to-string.pipe';
export * from './src/app/core/services/pipes/person-to-string.pipe';
export * from './src/app/core/services/pipes/usage-mode.pipes';
export * from './src/app/core/services/pipes/referential-to-string.pipe';
export * from './src/app/core/services/pipes/pipes.module';

// Core services
export * from './src/app/core/services/base58';
export * from './src/app/core/services/errors';
export * from './src/app/core/services/platform.service';
export * from './src/app/core/services/network.service';
export * from './src/app/core/services/network.types';
export * from './src/app/core/services/network.utils';
export * from './src/app/core/services/config.service';
export { CORE_CONFIG_OPTIONS } from './src/app/core/services/config/core.config';
export * from './src/app/core/services/local-settings.service';
export * from './src/app/core/services/account.service';
export * from './src/app/core/services/crypto.service';
export * from './src/app/core/services/auth-guard.service';
export * from './src/app/core/services/base-graphql-service.class';
export * from './src/app/core/services/base-entity-service.class';
export * from './src/app/core/services/storage/entities-storage.service';
export * from './src/app/core/services/storage/entity-store.class';
export * from './src/app/core/services/validator/base.validator.class';

// Core components
export * from './src/app/core/form/form.class';
export * from './src/app/core/form/form.utils';
export * from './src/app/core/form/form-container.class';
export * from './src/app/core/form/form.module';
export * from './src/app/core/form/buttons/form-buttons-bar.component';
export * from './src/app/core/form/buttons/form-buttons-bar.module';
export * from './src/app/core/form/entity/editor.class';
export * from './src/app/core/form/entity/tab-editor.class';
export * from './src/app/core/form/entity/entity-editor.class';
export * from './src/app/core/form/entity/entity-editor-modal.class';
export * from './src/app/core/form/entity/entity-metadata.component';
export * from './src/app/core/form/entity/entity.module';
export * from './src/app/core/form/list/list.form';
export * from './src/app/core/form/list/list.module';
export * from './src/app/core/form/properties/properties.form';
export * from './src/app/core/form/properties/properties.utils';
export * from './src/app/core/form/properties/properties.table';
export * from './src/app/core/form/properties/properties.module';
export * from './src/app/core/form/text-popover/text-popover.component';
export * from './src/app/core/form/text-popover/text-popover.module';
export * from './src/app/core/form/array/form-array';
export * from './src/app/core/table/table.model';
export * from './src/app/core/table/table.class';
export * from './src/app/core/table/async-table.class';
export * from './src/app/core/table/table.utils';
export * from './src/app/core/table/table.pipes';
export * from './src/app/core/table/table.module';
export * from './src/app/core/table/memory-table.class';
export * from './src/app/core/table/entities-table-datasource.class';
export * from './src/app/core/table/entities-async-table-datasource.class';
export * from './src/app/core/table/table-select-columns.component';
export * from './src/app/core/table/column/actions-column.component';
export * from './src/app/core/table/column/nav-actions-column.component';
export * from './src/app/core/table/column/row-field.component';
export * from './src/app/core/icon/icon.component';
export * from './src/app/core/icon/icon.module';
export * from './src/app/core/home/home';
export * from './src/app/core/home/home.module';
export * from './src/app/core/settings/settings.page';
export * from './src/app/core/settings/settings.module';
export * from './src/app/core/account/account.page';
export * from './src/app/core/account/account.module';
export * from './src/app/core/account/account.page';
export * from './src/app/core/account/token.table';
export * from './src/app/core/account/new-token.form';
export * from './src/app/core/account/new-token.modal';
export * from './src/app/core/account/password/change-password.module';
export * from './src/app/core/account/password/change-password.form';
export * from './src/app/core/account/password/change-password.page';
export * from './src/app/core/auth/auth.form';
export * from './src/app/core/auth/auth.modal';
export * from './src/app/core/auth/auth.module';
export * from './src/app/core/auth/reset-password.modal';
export * from './src/app/core/register/register-confirm.page';
export * from './src/app/core/register/register.form';
export * from './src/app/core/register/register.modal';
export * from './src/app/core/register/register.module';
export * from './src/app/core/about/about.modal';
export * from './src/app/core/about/about.module';
export * from './src/app/core/peer/select-peer.modal';
export * from './src/app/core/peer/select-peer.module';
export * from './src/app/core/install/install-upgrade-card.component';
export * from './src/app/core/install/install-upgrade-card.module';
export * from './src/app/core/menu/menu.component';
export * from './src/app/core/menu/menu.model';
export * from './src/app/core/menu/menu.module';
export * from './src/app/core/menu/menu.service';
export * from './src/app/core/menu/sub-menu-tab.directive';
export * from './src/app/core/offline/update-offline-mode-card.component';
export * from './src/app/core/offline/update-offline-mode-card.module';

// Core decorator
export { EntityClass, EntityClasses } from './src/app/core/services/model/entity.decorators';

// Social
export * from './src/app/social/social.module';
export * from './src/app/social/social.errors';
export * from './src/app/social/user-event/user-event.module';
export * from './src/app/social/user-event/user-event.model';
export * from './src/app/social/user-event/user-event.service';
export * from './src/app/social/user-event/notification/user-event-notification.icon';
export * from './src/app/social/user-event/notification/user-event-notification.list';
export * from './src/app/social/job/job.module';
export * from './src/app/social/job/progression/job-progression.service';
export * from './src/app/social/job/progression/job-progression.model';
export * from './src/app/social/job/progression/job-progression.icon';
export * from './src/app/social/job/progression/job-progression.list';
export * from './src/app/social/job/progression/job-progression.component';
export * from './src/app/social/message/message.module';
export * from './src/app/social/message/message.model';
export * from './src/app/social/message/message.service';
export * from './src/app/social/message/message.form';
export * from './src/app/social/message/message.modal';

// Admin
export * from './src/app/admin/admin.module';
export { AdminRoutingModule } from './src/app/admin/admin-routing.module';
export * from './src/app/admin/services/filter/person.filter';
export * from './src/app/admin/services/validator/person.validator';
export * from './src/app/admin/services/person.service';
export * from './src/app/admin/users/users.module';
export * from './src/app/admin/users/users';

// Shared tests page
export * from './src/app/shared/testing/tests.page';

// ------------------------------------------------------------------------
// Testing module and components
//
// TODO: to remove when ngx-components will be well tested
// (or export into a sub package, like '@sumaris-net/ngx-components/testing')
// ------------------------------------------------------------------------

// Shared material test
export * from './src/app/shared/material/material.testing.module';
export * from './src/app/shared/material/autocomplete/testing/autocomplete.test';
export * from './src/app/shared/material/latlong/testing/latlong.test';
export * from './src/app/shared/material/swipe/testing/swipe.test';
export * from './src/app/shared/material/datetime/testing/mat-date-time.test';
export * from './src/app/shared/material/datetime/testing/mat-date.test';
export * from './src/app/shared/material/datetime/testing/mat-dateshort.test';
export * from './src/app/shared/material/chips/testing/chips.test';
//export * from './src/app/shared/material/numpad/testing/numpad.test';
export * from './src/app/shared/material/text/testing/text-form.testing';
export * from './src/app/shared/testing/observable.test';
export * from './src/app/shared/material/badge/badge.test';
export * from './src/app/shared/material/testing/common.test';
export * from './src/app/shared/material/boolean/testing/boolean.test.page';
export * from './src/app/shared/material/duration/testing/mat-duration.test';
export * from './src/app/shared/testing/maskito.test';

// Shared other test
export * from './src/app/shared/shared.testing.module';
export * from './src/app/shared/upload-file/testing/upload-file.testing';
export * from './src/app/shared/upload-file/testing/upload-file.testing.module';
export * from './src/app/shared/toast/toast.testing';
export * from './src/app/shared/toast/toast.testing.module';
export * from './src/app/shared/image/gallery/testing/gallery.service.testing';
export * from './src/app/shared/image/gallery/testing/gallery.testing';
export * from './src/app/shared/image/gallery/testing/gallery.testing.module';
export * from './src/app/shared/storage/storage-explorer.testing.module';
export * from './src/app/shared/storage/storage-explorer.testing-routing.module';
export * from './src/app/shared/audio/audio.testing';
export * from './src/app/shared/audio/audio.testing.module';
export * from './src/app/shared/markdown/testing/markdown.test';
export * from './src/app/shared/markdown/testing/markdown.testing.module';

// Core testing
export * from './src/app/core/core.testing.module';
export * from './src/app/core/services/testing/referential-filter.model';
export * from './src/app/core/services/testing/referential.validator';
export * from './src/app/core/table/testing/table.testing.module';
export * from './src/app/core/table/testing/table.testing';
export * from './src/app/core/table/testing/table2.testing';
export * from './src/app/core/form/text-popover/testing/text-popover.testing.module';
export * from './src/app/core/form/text-popover/testing/text-popover.testing';
export * from './src/app/core/form/properties/testing/properties-form.test';
export * from './src/app/core/form/properties/testing/properties-form.testing.module';
export * from './src/app/core/menu/testing/menu.testing';
export * from './src/app/core/menu/testing/menu.testing.module';
export * from './src/app/core/menu/testing/menu-other.testing';
export * from './src/app/core/form/array/testing/form-array.test';
export * from './src/app/core/form/array/testing/form-array-test.module';
export * from './src/app/shared/named-filter/testing/named-filter-selector.testing';
export * from './src/app/shared/named-filter/testing/named-filter.testing.module';

// Social testing
export * from './src/app/social/social.testing.module';
export * from './src/app/social/user-event/testing/user-event.testing.module';
export * from './src/app/social/user-event/testing/user-event.testing';
export * from './src/app/social/user-event/testing/user-event.testing.service';
export * from './src/app/social/job/testing/job.testing.module';
export * from './src/app/social/job/testing/job-progression.testing';
export * from './src/app/social/job/testing/job-progression.testing.service';
export * from './src/app/core/table/testing/table-validator.service';
