module.exports = {
  packages: {
    'apollo-link-queue': {
      ignorableDeepImportMatchers: [
        /@apollo\//
      ]
    }
  }
};
